# Change list for REFCODES.ORG artifacts' version 1.10.2

> This change list has been auto-generated on `triton.local` by `steiner` with `changelist-all.sh` on the 2019-01-13 at 19:29:00.

## Change list &lt;refcodes-licensing&gt; (version 1.10.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-licensing/src/refcodes-licensing-1.10.2/pom.xml) 

## Change list &lt;refcodes-parent&gt; (version 1.10.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-parent/src/refcodes-parent-1.10.2/pom.xml) 

## Change list &lt;refcodes-time&gt; (version 1.10.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-time/src/refcodes-time-1.10.2/pom.xml) 

## Change list &lt;refcodes-mixin&gt; (version 1.10.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-mixin/src/refcodes-mixin-1.10.2/pom.xml) 

## Change list &lt;refcodes-data&gt; (version 1.10.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data/src/refcodes-data-1.10.2/pom.xml) 

## Change list &lt;refcodes-exception&gt; (version 1.10.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-exception/src/refcodes-exception-1.10.2/pom.xml) 

## Change list &lt;refcodes-factory&gt; (version 1.10.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-factory/src/refcodes-factory-1.10.2/pom.xml) 

## Change list &lt;refcodes-factory-alt&gt; (version 1.10.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-factory-alt/src/refcodes-factory-alt-1.10.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-factory-alt/src/refcodes-factory-alt-1.10.2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-factory-alt/src/refcodes-factory-alt-1.10.2/refcodes-factory-alt-spring/pom.xml) 

## Change list &lt;refcodes-controlflow&gt; (version 1.10.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-controlflow/src/refcodes-controlflow-1.10.2/pom.xml) 

## Change list &lt;refcodes-numerical&gt; (version 1.10.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-numerical/src/refcodes-numerical-1.10.2/pom.xml) 

## Change list &lt;refcodes-generator&gt; (version 1.10.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-generator/src/refcodes-generator-1.10.2/pom.xml) 

## Change list &lt;refcodes-matcher&gt; (version 1.10.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-matcher/src/refcodes-matcher-1.10.2/pom.xml) 

## Change list &lt;refcodes-structure&gt; (version 1.10.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-1.10.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-1.10.2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`CanonicalMap.java`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-1.10.2/src/main/java/org/refcodes/structure/CanonicalMap.java) (see Javadoc at [`CanonicalMap.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-structure/1.10.2/org/refcodes/structure/CanonicalMap.html))
* \[<span style="color:green">MODIFIED</span>\] [`PathMap.java`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-1.10.2/src/main/java/org/refcodes/structure/PathMap.java) (see Javadoc at [`PathMap.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-structure/1.10.2/org/refcodes/structure/PathMap.html))
* \[<span style="color:green">MODIFIED</span>\] [`PropertiesAccessorMixin.java`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-1.10.2/src/main/java/org/refcodes/structure/PropertiesAccessorMixin.java) (see Javadoc at [`PropertiesAccessorMixin.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-structure/1.10.2/org/refcodes/structure/PropertiesAccessorMixin.html))
* \[<span style="color:green">MODIFIED</span>\] [`PathMapArrayTest.java`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-1.10.2/src/test/java/org/refcodes/structure/PathMapArrayTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`PathMapDirTest.java`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-1.10.2/src/test/java/org/refcodes/structure/PathMapDirTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`PathMapTest.java`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-1.10.2/src/test/java/org/refcodes/structure/PathMapTest.java) 

## Change list &lt;refcodes-runtime&gt; (version 1.10.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-1.10.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-1.10.2/README.md) 

## Change list &lt;refcodes-component&gt; (version 1.10.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-1.10.2/pom.xml) 

## Change list &lt;refcodes-data-ext&gt; (version 1.10.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.10.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.10.2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.10.2/refcodes-data-ext-boulderdash/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.10.2/refcodes-data-ext-checkers/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.10.2/refcodes-data-ext-checkers/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.10.2/refcodes-data-ext-chess/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.10.2/refcodes-data-ext-chess/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.10.2/refcodes-data-ext-corporate/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.10.2/refcodes-data-ext-corporate/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.10.2/refcodes-data-ext-symbols/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.10.2/refcodes-data-ext-symbols/README.md) 

## Change list &lt;refcodes-graphical&gt; (version 1.10.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-graphical/src/refcodes-graphical-1.10.2/pom.xml) 

## Change list &lt;refcodes-textual&gt; (version 1.10.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-1.10.2/pom.xml) 

## Change list &lt;refcodes-criteria&gt; (version 1.10.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-1.10.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-1.10.2/README.md) 

## Change list &lt;refcodes-tabular&gt; (version 1.10.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-tabular/src/refcodes-tabular-1.10.2/pom.xml) 

## Change list &lt;refcodes-observer&gt; (version 1.10.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-observer/src/refcodes-observer-1.10.2/pom.xml) 

## Change list &lt;refcodes-command&gt; (version 1.10.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-command/src/refcodes-command-1.10.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-command/src/refcodes-command-1.10.2/README.md) 

## Change list &lt;refcodes-cli&gt; (version 1.10.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-1.10.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-1.10.2/README.md) 

## Change list &lt;refcodes-io&gt; (version 1.10.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-io/src/refcodes-io-1.10.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-io/src/refcodes-io-1.10.2/README.md) 

## Change list &lt;refcodes-codec&gt; (version 1.10.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-1.10.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-1.10.2/README.md) 

## Change list &lt;refcodes-component-ext&gt; (version 1.10.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.10.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.10.2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.10.2/refcodes-component-ext-observer/pom.xml) 

## Change list &lt;refcodes-properties&gt; (version 1.10.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.10.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.10.2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`package-info.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.10.2/src/main/java/org/refcodes/configuration/package-info.java) 
* \[<span style="color:green">MODIFIED</span>\] [`ProfileProperties.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.10.2/src/main/java/org/refcodes/configuration/ProfileProperties.java) (see Javadoc at [`ProfileProperties.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties/1.10.2/org/refcodes/configuration/ProfileProperties.html))
* \[<span style="color:green">MODIFIED</span>\] [`Properties.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.10.2/src/main/java/org/refcodes/configuration/Properties.java) (see Javadoc at [`Properties.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties/1.10.2/org/refcodes/configuration/Properties.html))
* \[<span style="color:green">MODIFIED</span>\] [`ResourceProperties.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.10.2/src/main/java/org/refcodes/configuration/ResourceProperties.java) (see Javadoc at [`ResourceProperties.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties/1.10.2/org/refcodes/configuration/ResourceProperties.html))
* \[<span style="color:green">MODIFIED</span>\] [`StrictProperties.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.10.2/src/main/java/org/refcodes/configuration/StrictProperties.java) (see Javadoc at [`StrictProperties.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties/1.10.2/org/refcodes/configuration/StrictProperties.html))
* \[<span style="color:green">MODIFIED</span>\] [`TomlPropertiesBuilder.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.10.2/src/main/java/org/refcodes/configuration/TomlPropertiesBuilder.java) (see Javadoc at [`TomlPropertiesBuilder.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties/1.10.2/org/refcodes/configuration/TomlPropertiesBuilder.html))

## Change list &lt;refcodes-security&gt; (version 1.10.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security/src/refcodes-security-1.10.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security/src/refcodes-security-1.10.2/README.md) 

## Change list &lt;refcodes-security-alt&gt; (version 1.10.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-1.10.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-1.10.2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-1.10.2/refcodes-security-alt-chaos/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-1.10.2/refcodes-security-alt-chaos/README.md) 

## Change list &lt;refcodes-security-ext&gt; (version 1.10.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-1.10.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-1.10.2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-1.10.2/refcodes-security-ext-chaos/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-1.10.2/refcodes-security-ext-chaos/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-1.10.2/refcodes-security-ext-spring/pom.xml) 

## Change list &lt;refcodes-properties-ext&gt; (version 1.10.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.10.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.10.2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.10.2/refcodes-properties-ext-cli/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.10.2/refcodes-properties-ext-cli/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.10.2/refcodes-properties-ext-obfuscation/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.10.2/refcodes-properties-ext-obfuscation/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`AbstractObfuscationResourcePropertiesBuilderDecorator.java`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.10.2/refcodes-properties-ext-obfuscation/src/main/java/org/refcodes/configuration/ext/obfuscation/AbstractObfuscationResourcePropertiesBuilderDecorator.java) (see Javadoc at [`AbstractObfuscationResourcePropertiesBuilderDecorator.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties-ext-obfuscation/1.10.2/org/refcodes/configuration/ext/obfuscation/AbstractObfuscationResourcePropertiesBuilderDecorator.html))
* \[<span style="color:green">MODIFIED</span>\] [`ObfuscationProperties.java`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.10.2/refcodes-properties-ext-obfuscation/src/main/java/org/refcodes/configuration/ext/obfuscation/ObfuscationProperties.java) (see Javadoc at [`ObfuscationProperties.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties-ext-obfuscation/1.10.2/org/refcodes/configuration/ext/obfuscation/ObfuscationProperties.html))
* \[<span style="color:green">MODIFIED</span>\] [`ObfuscationResourcePropertiesBuilderDecorator.java`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.10.2/refcodes-properties-ext-obfuscation/src/main/java/org/refcodes/configuration/ext/obfuscation/ObfuscationResourcePropertiesBuilderDecorator.java) (see Javadoc at [`ObfuscationResourcePropertiesBuilderDecorator.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties-ext-obfuscation/1.10.2/org/refcodes/configuration/ext/obfuscation/ObfuscationResourcePropertiesBuilderDecorator.html))
* \[<span style="color:green">MODIFIED</span>\] [`ObfuscationResourceProperties.java`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.10.2/refcodes-properties-ext-obfuscation/src/main/java/org/refcodes/configuration/ext/obfuscation/ObfuscationResourceProperties.java) (see Javadoc at [`ObfuscationResourceProperties.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties-ext-obfuscation/1.10.2/org/refcodes/configuration/ext/obfuscation/ObfuscationResourceProperties.html))
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.10.2/refcodes-properties-ext-observer/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.10.2/refcodes-properties-ext-observer/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`ObservableProperties.java`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.10.2/refcodes-properties-ext-observer/src/main/java/org/refcodes/configuration/ext/observer/ObservableProperties.java) (see Javadoc at [`ObservableProperties.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties-ext-observer/1.10.2/org/refcodes/configuration/ext/observer/ObservableProperties.html))
* \[<span style="color:green">MODIFIED</span>\] [`ObservableResouceProperties.java`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.10.2/refcodes-properties-ext-observer/src/main/java/org/refcodes/configuration/ext/observer/ObservableResouceProperties.java) (see Javadoc at [`ObservableResouceProperties.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties-ext-observer/1.10.2/org/refcodes/configuration/ext/observer/ObservableResouceProperties.html))
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.10.2/refcodes-properties-ext-runtime/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.10.2/refcodes-properties-ext-runtime/README.md) 

## Change list &lt;refcodes-logger&gt; (version 1.10.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger/src/refcodes-logger-1.10.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger/src/refcodes-logger-1.10.2/README.md) 

## Change list &lt;refcodes-logger-alt&gt; (version 1.10.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.10.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.10.2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.10.2/refcodes-logger-alt-async/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.10.2/refcodes-logger-alt-async/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.10.2/refcodes-logger-alt-console/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.10.2/refcodes-logger-alt-console/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.10.2/refcodes-logger-alt-io/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.10.2/refcodes-logger-alt-io/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.10.2/refcodes-logger-alt-simpledb/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.10.2/refcodes-logger-alt-slf4j/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.10.2/refcodes-logger-alt-spring/pom.xml) 

## Change list &lt;refcodes-logger-ext&gt; (version 1.10.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-1.10.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-1.10.2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-1.10.2/refcodes-logger-ext-slf4j/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-1.10.2/refcodes-logger-ext-slf4j/README.md) 

## Change list &lt;refcodes-graphical-ext&gt; (version 1.10.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-1.10.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-1.10.2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-1.10.2/refcodes-graphical-ext-javafx/pom.xml) 

## Change list &lt;refcodes-checkerboard&gt; (version 1.10.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-1.10.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-1.10.2/README.md) 

## Change list &lt;refcodes-checkerboard-alt&gt; (version 1.10.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-alt/src/refcodes-checkerboard-alt-1.10.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-alt/src/refcodes-checkerboard-alt-1.10.2/refcodes-checkerboard-alt-javafx/pom.xml) 

## Change list &lt;refcodes-checkerboard-ext&gt; (version 1.10.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-ext/src/refcodes-checkerboard-ext-1.10.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-checkerboard-ext/src/refcodes-checkerboard-ext-1.10.2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-ext/src/refcodes-checkerboard-ext-1.10.2/refcodes-checkerboard-ext-javafx-boulderdash/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-ext/src/refcodes-checkerboard-ext-1.10.2/refcodes-checkerboard-ext-javafx-chess/pom.xml) 

## Change list &lt;refcodes-boulderdash&gt; (version 1.10.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-boulderdash/src/refcodes-boulderdash-1.10.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-boulderdash/src/refcodes-boulderdash-1.10.2/README.md) 

## Change list &lt;refcodes-net&gt; (version 1.10.2)

* \[<span style="color:blue">ADDED</span>\] [`RedirectDepthAccessor.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.10.2/src/main/java/org/refcodes/net/RedirectDepthAccessor.java) (see Javadoc at [`RedirectDepthAccessor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/1.10.2/org/refcodes/net/RedirectDepthAccessor.html))
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.10.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`ContentType.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.10.2/src/main/java/org/refcodes/net/ContentType.java) (see Javadoc at [`ContentType.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/1.10.2/org/refcodes/net/ContentType.html))
* \[<span style="color:green">MODIFIED</span>\] [`HeaderField.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.10.2/src/main/java/org/refcodes/net/HeaderField.java) (see Javadoc at [`HeaderField.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/1.10.2/org/refcodes/net/HeaderField.html))
* \[<span style="color:green">MODIFIED</span>\] [`HttpBodyMap.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.10.2/src/main/java/org/refcodes/net/HttpBodyMap.java) (see Javadoc at [`HttpBodyMap.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/1.10.2/org/refcodes/net/HttpBodyMap.html))
* \[<span style="color:green">MODIFIED</span>\] [`HttpClientRequestImpl.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.10.2/src/main/java/org/refcodes/net/HttpClientRequestImpl.java) (see Javadoc at [`HttpClientRequestImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/1.10.2/org/refcodes/net/HttpClientRequestImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`HttpClientRequest.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.10.2/src/main/java/org/refcodes/net/HttpClientRequest.java) (see Javadoc at [`HttpClientRequest.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/1.10.2/org/refcodes/net/HttpClientRequest.html))
* \[<span style="color:green">MODIFIED</span>\] [`HttpClientResponseImpl.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.10.2/src/main/java/org/refcodes/net/HttpClientResponseImpl.java) (see Javadoc at [`HttpClientResponseImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/1.10.2/org/refcodes/net/HttpClientResponseImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`HttpRequest.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.10.2/src/main/java/org/refcodes/net/HttpRequest.java) (see Javadoc at [`HttpRequest.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/1.10.2/org/refcodes/net/HttpRequest.html))
* \[<span style="color:green">MODIFIED</span>\] [`HttpServerRequestImpl.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.10.2/src/main/java/org/refcodes/net/HttpServerRequestImpl.java) (see Javadoc at [`HttpServerRequestImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/1.10.2/org/refcodes/net/HttpServerRequestImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`HttpServerResponseImpl.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.10.2/src/main/java/org/refcodes/net/HttpServerResponseImpl.java) (see Javadoc at [`HttpServerResponseImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/1.10.2/org/refcodes/net/HttpServerResponseImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`HttpStatusCode.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.10.2/src/main/java/org/refcodes/net/HttpStatusCode.java) (see Javadoc at [`HttpStatusCode.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/1.10.2/org/refcodes/net/HttpStatusCode.html))
* \[<span style="color:green">MODIFIED</span>\] [`ResponseHeaderFields.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.10.2/src/main/java/org/refcodes/net/ResponseHeaderFields.java) (see Javadoc at [`ResponseHeaderFields.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/1.10.2/org/refcodes/net/ResponseHeaderFields.html))

## Change list &lt;refcodes-rest&gt; (version 1.10.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.10.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.10.2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`AbstractHttpRestClientDecorator.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.10.2/src/main/java/org/refcodes/rest/AbstractHttpRestClientDecorator.java) (see Javadoc at [`AbstractHttpRestClientDecorator.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/1.10.2/org/refcodes/rest/AbstractHttpRestClientDecorator.html))
* \[<span style="color:green">MODIFIED</span>\] [`AbstractRestClient.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.10.2/src/main/java/org/refcodes/rest/AbstractRestClient.java) (see Javadoc at [`AbstractRestClient.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/1.10.2/org/refcodes/rest/AbstractRestClient.html))
* \[<span style="color:green">MODIFIED</span>\] [`HttpRestClientImpl.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.10.2/src/main/java/org/refcodes/rest/HttpRestClientImpl.java) (see Javadoc at [`HttpRestClientImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/1.10.2/org/refcodes/rest/HttpRestClientImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`RestCallerBuilderImpl.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.10.2/src/main/java/org/refcodes/rest/RestCallerBuilderImpl.java) (see Javadoc at [`RestCallerBuilderImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/1.10.2/org/refcodes/rest/RestCallerBuilderImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`RestCallerBuilder.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.10.2/src/main/java/org/refcodes/rest/RestCallerBuilder.java) (see Javadoc at [`RestCallerBuilder.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/1.10.2/org/refcodes/rest/RestCallerBuilder.html))
* \[<span style="color:green">MODIFIED</span>\] [`RestRequestBuilderImpl.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.10.2/src/main/java/org/refcodes/rest/RestRequestBuilderImpl.java) (see Javadoc at [`RestRequestBuilderImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/1.10.2/org/refcodes/rest/RestRequestBuilderImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`RestRequestBuilder.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.10.2/src/main/java/org/refcodes/rest/RestRequestBuilder.java) (see Javadoc at [`RestRequestBuilder.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/1.10.2/org/refcodes/rest/RestRequestBuilder.html))
* \[<span style="color:green">MODIFIED</span>\] [`RestRequestClient.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.10.2/src/main/java/org/refcodes/rest/RestRequestClient.java) (see Javadoc at [`RestRequestClient.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/1.10.2/org/refcodes/rest/RestRequestClient.html))
* \[<span style="color:green">MODIFIED</span>\] [`HttpRestClientTest.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.10.2/src/test/java/org/refcodes/rest/HttpRestClientTest.java) 

## Change list &lt;refcodes-daemon&gt; (version 1.10.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-daemon/src/refcodes-daemon-1.10.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-daemon/src/refcodes-daemon-1.10.2/README.md) 

## Change list &lt;refcodes-eventbus&gt; (version 1.10.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-eventbus/src/refcodes-eventbus-1.10.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-eventbus/src/refcodes-eventbus-1.10.2/README.md) 

## Change list &lt;refcodes-eventbus-ext&gt; (version 1.10.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-1.10.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-1.10.2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-1.10.2/refcodes-eventbus-ext-application/pom.xml) 

## Change list &lt;refcodes-filesystem&gt; (version 1.10.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-filesystem/src/refcodes-filesystem-1.10.2/pom.xml) 

## Change list &lt;refcodes-filesystem-alt&gt; (version 1.10.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-filesystem-alt/src/refcodes-filesystem-alt-1.10.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-filesystem-alt/src/refcodes-filesystem-alt-1.10.2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-filesystem-alt/src/refcodes-filesystem-alt-1.10.2/refcodes-filesystem-alt-s3/pom.xml) 

## Change list &lt;refcodes-forwardsecrecy&gt; (version 1.10.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy/src/refcodes-forwardsecrecy-1.10.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy/src/refcodes-forwardsecrecy-1.10.2/README.md) 

## Change list &lt;refcodes-forwardsecrecy-alt&gt; (version 1.10.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-1.10.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-1.10.2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-1.10.2/refcodes-forwardsecrecy-alt-filesystem/pom.xml) 

## Change list &lt;refcodes-io-ext&gt; (version 1.10.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-1.10.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-1.10.2/refcodes-io-ext-observable/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-1.10.2/refcodes-io-ext-observable/README.md) 

## Change list &lt;refcodes-interceptor&gt; (version 1.10.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-interceptor/src/refcodes-interceptor-1.10.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-interceptor/src/refcodes-interceptor-1.10.2/README.md) 

## Change list &lt;refcodes-jobbus&gt; (version 1.10.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-jobbus/src/refcodes-jobbus-1.10.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-jobbus/src/refcodes-jobbus-1.10.2/README.md) 

## Change list &lt;refcodes-rest-ext&gt; (version 1.10.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-1.10.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-1.10.2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-1.10.2/refcodes-rest-ext-eureka/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-1.10.2/refcodes-rest-ext-eureka/README.md) 

## Change list &lt;refcodes-remoting&gt; (version 1.10.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-remoting/src/refcodes-remoting-1.10.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-remoting/src/refcodes-remoting-1.10.2/README.md) 

## Change list &lt;refcodes-remoting-ext&gt; (version 1.10.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-1.10.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-1.10.2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-1.10.2/refcodes-remoting-ext-observer/pom.xml) 

## Change list &lt;refcodes-servicebus&gt; (version 1.10.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-servicebus/src/refcodes-servicebus-1.10.2/pom.xml) 

## Change list &lt;refcodes-servicebus-alt&gt; (version 1.10.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-servicebus-alt/src/refcodes-servicebus-alt-1.10.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-servicebus-alt/src/refcodes-servicebus-alt-1.10.2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-servicebus-alt/src/refcodes-servicebus-alt-1.10.2/refcodes-servicebus-alt-spring/pom.xml) 

## Change list &lt;refcodes-tabular-alt&gt; (version 1.10.2)

* \[<span style="color:blue">ADDED</span>\] [`#Untitled-1#`](https://bitbucket.org/refcodes/refcodes-tabular-alt/src/refcodes-tabular-alt-1.10.2/#Untitled-1#) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-tabular-alt/src/refcodes-tabular-alt-1.10.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-tabular-alt/src/refcodes-tabular-alt-1.10.2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-tabular-alt/src/refcodes-tabular-alt-1.10.2/refcodes-tabular-alt-forwardsecrecy/pom.xml) 
