# Change list for REFCODES.ORG artifacts' version 1.1.6

> This change list has been auto-generated on `triton` by `steiner` with with `changelist-all.sh` on the 2017-05-15 at 21:32:31.

## Change list &lt;refcodes-licensing&gt; (version 1.1.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-licensing/src/refcodes-licensing-1.1.6/pom.xml) 

## Change list &lt;refcodes-parent&gt; (version 1.1.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-parent/src/refcodes-parent-1.1.6/pom.xml) 

## Change list &lt;refcodes-time&gt; (version 1.1.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-time/src/refcodes-time-1.1.6/pom.xml) 

## Change list &lt;refcodes-exception&gt; (version 1.1.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-exception/src/refcodes-exception-1.1.6/pom.xml) 

## Change list &lt;refcodes-mixin&gt; (version 1.1.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-mixin/src/refcodes-mixin-1.1.6/pom.xml) 

## Change list &lt;refcodes-data&gt; (version 1.1.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data/src/refcodes-data-1.1.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`CharSet.java`](https://bitbucket.org/refcodes/refcodes-data/src/refcodes-data-1.1.6/src/main/java/org/refcodes/data/CharSet.java) (see Javadoc at [`CharSet.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-data/1.1.6/org/refcodes/data/CharSet.html))
* \[<span style="color:green">MODIFIED</span>\] [`Delimiter.java`](https://bitbucket.org/refcodes/refcodes-data/src/refcodes-data-1.1.6/src/main/java/org/refcodes/data/Delimiter.java) (see Javadoc at [`Delimiter.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-data/1.1.6/org/refcodes/data/Delimiter.html))
* \[<span style="color:green">MODIFIED</span>\] [`Protocol.java`](https://bitbucket.org/refcodes/refcodes-data/src/refcodes-data-1.1.6/src/main/java/org/refcodes/data/Protocol.java) (see Javadoc at [`Protocol.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-data/1.1.6/org/refcodes/data/Protocol.html))

## Change list &lt;refcodes-structure&gt; (version 1.1.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-1.1.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`PropertyImpl.java`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-1.1.6/src/main/java/org/refcodes/structure/PropertyImpl.java) (see Javadoc at [`PropertyImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-structure/1.1.6/org/refcodes/structure/PropertyImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`Property.java`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-1.1.6/src/main/java/org/refcodes/structure/Property.java) (see Javadoc at [`Property.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-structure/1.1.6/org/refcodes/structure/Property.html))

## Change list &lt;refcodes-controlflow&gt; (version 1.1.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-controlflow/src/refcodes-controlflow-1.1.6/pom.xml) 

## Change list &lt;refcodes-numerical&gt; (version 1.1.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-numerical/src/refcodes-numerical-1.1.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`NumberBaseBuilderImpl.java`](https://bitbucket.org/refcodes/refcodes-numerical/src/refcodes-numerical-1.1.6/src/main/java/org/refcodes/numerical/NumberBaseBuilderImpl.java) (see Javadoc at [`NumberBaseBuilderImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-numerical/1.1.6/org/refcodes/numerical/NumberBaseBuilderImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`NumberBaseBuilderTest.java`](https://bitbucket.org/refcodes/refcodes-numerical/src/refcodes-numerical-1.1.6/src/test/java/org/refcodes/numerical/NumberBaseBuilderTest.java) 

## Change list &lt;refcodes-generator&gt; (version 1.1.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-generator/src/refcodes-generator-1.1.6/pom.xml) 

## Change list &lt;refcodes-collection&gt; (version 1.1.6)

* \[<span style="color:red">DELETED</span>\] `PropertiesAccessor.java`
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-collection/src/refcodes-collection-1.1.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`PropertiesImpl.java`](https://bitbucket.org/refcodes/refcodes-collection/src/refcodes-collection-1.1.6/src/main/java/org/refcodes/collection/PropertiesImpl.java) (see Javadoc at [`PropertiesImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-collection/1.1.6/org/refcodes/collection/PropertiesImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`Properties.java`](https://bitbucket.org/refcodes/refcodes-collection/src/refcodes-collection-1.1.6/src/main/java/org/refcodes/collection/Properties.java) (see Javadoc at [`Properties.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-collection/1.1.6/org/refcodes/collection/Properties.html))

## Change list &lt;refcodes-runtime&gt; (version 1.1.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-1.1.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`Correlation.java`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-1.1.6/src/main/java/org/refcodes/runtime/Correlation.java) (see Javadoc at [`Correlation.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-runtime/1.1.6/org/refcodes/runtime/Correlation.html))
* \[<span style="color:green">MODIFIED</span>\] [`EnvironmentUtility.java`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-1.1.6/src/main/java/org/refcodes/runtime/EnvironmentUtility.java) (see Javadoc at [`EnvironmentUtility.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-runtime/1.1.6/org/refcodes/runtime/EnvironmentUtility.html))
* \[<span style="color:green">MODIFIED</span>\] [`RuntimePropertiesImpl.java`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-1.1.6/src/main/java/org/refcodes/runtime/RuntimePropertiesImpl.java) (see Javadoc at [`RuntimePropertiesImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-runtime/1.1.6/org/refcodes/runtime/RuntimePropertiesImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`RuntimeUtility.java`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-1.1.6/src/main/java/org/refcodes/runtime/RuntimeUtility.java) (see Javadoc at [`RuntimeUtility.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-runtime/1.1.6/org/refcodes/runtime/RuntimeUtility.html))
* \[<span style="color:green">MODIFIED</span>\] [`RuntimeUtilityTest.java`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-1.1.6/src/test/java/org/refcodes/runtime/RuntimeUtilityTest.java) 

## Change list &lt;refcodes-component&gt; (version 1.1.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-1.1.6/pom.xml) 

## Change list &lt;refcodes-interceptor&gt; (version 1.1.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-interceptor/src/refcodes-interceptor-1.1.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-interceptor/src/refcodes-interceptor-1.1.6/README.md) 

## Change list &lt;refcodes-factory&gt; (version 1.1.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-factory/src/refcodes-factory-1.1.6/pom.xml) 

## Change list &lt;refcodes-data-ext&gt; (version 1.1.6)

* \[<span style="color:blue">ADDED</span>\] [`BoulderDashCaveMapFactory.java`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.1.6/refcodes-data-ext-boulderdash/src/main/java/org/refcodes/data/ext/boulderdash/BoulderDashCaveMapFactory.java) (see Javadoc at [`BoulderDashCaveMapFactory.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-data-ext-boulderdash/1.1.6/org/refcodes/data/ext/boulderdash/BoulderDashCaveMapFactory.html))
* \[<span style="color:blue">ADDED</span>\] [`BoulderDashCaveMap.java`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.1.6/refcodes-data-ext-boulderdash/src/main/java/org/refcodes/data/ext/boulderdash/BoulderDashCaveMap.java) (see Javadoc at [`BoulderDashCaveMap.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-data-ext-boulderdash/1.1.6/org/refcodes/data/ext/boulderdash/BoulderDashCaveMap.html))
* \[<span style="color:blue">ADDED</span>\] [`BoulderDashStatus.java`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.1.6/refcodes-data-ext-boulderdash/src/main/java/org/refcodes/data/ext/boulderdash/BoulderDashStatus.java) (see Javadoc at [`BoulderDashStatus.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-data-ext-boulderdash/1.1.6/org/refcodes/data/ext/boulderdash/BoulderDashStatus.html))
* \[<span style="color:blue">ADDED</span>\] [`BoulderDashCaveMapFactoryImpl.java`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.1.6/refcodes-data-ext-boulderdash/src/main/java/org/refcodes/data/ext/boulderdash/BoulderDashCaveMapFactoryImpl.java) (see Javadoc at [`BoulderDashCaveMapFactoryImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-data-ext-boulderdash/1.1.6/org/refcodes/data/ext/boulderdash/BoulderDashCaveMapFactoryImpl.html))
* \[<span style="color:blue">ADDED</span>\] [`Cave_01_Intro.txt`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.1.6/refcodes-data-ext-boulderdash/src/main/resources/org/refcodes/data/ext/boulderdash/Cave_01_Intro.txt) 
* \[<span style="color:blue">ADDED</span>\] [`Cave_02_Rooms.txt`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.1.6/refcodes-data-ext-boulderdash/src/main/resources/org/refcodes/data/ext/boulderdash/Cave_02_Rooms.txt) 
* \[<span style="color:blue">ADDED</span>\] [`Cave_03_Maze.txt`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.1.6/refcodes-data-ext-boulderdash/src/main/resources/org/refcodes/data/ext/boulderdash/Cave_03_Maze.txt) 
* \[<span style="color:blue">ADDED</span>\] [`Cave_04_Butterflies.txt`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.1.6/refcodes-data-ext-boulderdash/src/main/resources/org/refcodes/data/ext/boulderdash/Cave_04_Butterflies.txt) 
* \[<span style="color:blue">ADDED</span>\] [`Cave_05_Guards.txt`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.1.6/refcodes-data-ext-boulderdash/src/main/resources/org/refcodes/data/ext/boulderdash/Cave_05_Guards.txt) 
* \[<span style="color:blue">ADDED</span>\] [`Cave_06_Firefly_dens.txt`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.1.6/refcodes-data-ext-boulderdash/src/main/resources/org/refcodes/data/ext/boulderdash/Cave_06_Firefly_dens.txt) 
* \[<span style="color:blue">ADDED</span>\] [`Cave_07_Amoeba.txt`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.1.6/refcodes-data-ext-boulderdash/src/main/resources/org/refcodes/data/ext/boulderdash/Cave_07_Amoeba.txt) 
* \[<span style="color:blue">ADDED</span>\] [`Cave_08_Enchanted_wall.txt`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.1.6/refcodes-data-ext-boulderdash/src/main/resources/org/refcodes/data/ext/boulderdash/Cave_08_Enchanted_wall.txt) 
* \[<span style="color:blue">ADDED</span>\] [`Cave_09_Greed.txt`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.1.6/refcodes-data-ext-boulderdash/src/main/resources/org/refcodes/data/ext/boulderdash/Cave_09_Greed.txt) 
* \[<span style="color:blue">ADDED</span>\] [`Cave_10_Tracks.txt`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.1.6/refcodes-data-ext-boulderdash/src/main/resources/org/refcodes/data/ext/boulderdash/Cave_10_Tracks.txt) 
* \[<span style="color:blue">ADDED</span>\] [`Cave_11_Crowd.txt`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.1.6/refcodes-data-ext-boulderdash/src/main/resources/org/refcodes/data/ext/boulderdash/Cave_11_Crowd.txt) 
* \[<span style="color:blue">ADDED</span>\] [`Cave_12_Walls.txt`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.1.6/refcodes-data-ext-boulderdash/src/main/resources/org/refcodes/data/ext/boulderdash/Cave_12_Walls.txt) 
* \[<span style="color:blue">ADDED</span>\] [`Cave_13_Apocalypse.txt`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.1.6/refcodes-data-ext-boulderdash/src/main/resources/org/refcodes/data/ext/boulderdash/Cave_13_Apocalypse.txt) 
* \[<span style="color:blue">ADDED</span>\] [`Cave_14_Zigzag.txt`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.1.6/refcodes-data-ext-boulderdash/src/main/resources/org/refcodes/data/ext/boulderdash/Cave_14_Zigzag.txt) 
* \[<span style="color:blue">ADDED</span>\] [`Cave_15_Funnel.txt`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.1.6/refcodes-data-ext-boulderdash/src/main/resources/org/refcodes/data/ext/boulderdash/Cave_15_Funnel.txt) 
* \[<span style="color:blue">ADDED</span>\] [`Cave_16_Enchanted_boxes.txt`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.1.6/refcodes-data-ext-boulderdash/src/main/resources/org/refcodes/data/ext/boulderdash/Cave_16_Enchanted_boxes.txt) 
* \[<span style="color:blue">ADDED</span>\] [`Cave_17_Intermission_1.txt`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.1.6/refcodes-data-ext-boulderdash/src/main/resources/org/refcodes/data/ext/boulderdash/Cave_17_Intermission_1.txt) 
* \[<span style="color:blue">ADDED</span>\] [`Cave_18_Intermission_2.txt`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.1.6/refcodes-data-ext-boulderdash/src/main/resources/org/refcodes/data/ext/boulderdash/Cave_18_Intermission_2.txt) 
* \[<span style="color:blue">ADDED</span>\] [`Cave_19_Intermission_3.txt`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.1.6/refcodes-data-ext-boulderdash/src/main/resources/org/refcodes/data/ext/boulderdash/Cave_19_Intermission_3.txt) 
* \[<span style="color:blue">ADDED</span>\] [`Cave_20_Intermission_4.txt`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.1.6/refcodes-data-ext-boulderdash/src/main/resources/org/refcodes/data/ext/boulderdash/Cave_20_Intermission_4.txt) 
* \[<span style="color:blue">ADDED</span>\] [`BoulderDashCaveMapTest.java`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.1.6/refcodes-data-ext-boulderdash/src/test/java/org/refcodes/data/ext/boulderdash/BoulderDashCaveMapTest.java) 
* \[<span style="color:blue">ADDED</span>\] [`ChessmanStatus.java`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.1.6/refcodes-data-ext-chess/src/main/java/org/refcodes/data/ext/chess/ChessmanStatus.java) (see Javadoc at [`ChessmanStatus.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-data-ext-chess/1.1.6/org/refcodes/data/ext/chess/ChessmanStatus.html))
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.1.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.1.6/refcodes-data-ext-boulderdash/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.1.6/refcodes-data-ext-boulderdash/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.1.6/refcodes-data-ext-checkers/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.1.6/refcodes-data-ext-checkers/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.1.6/refcodes-data-ext-chess/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.1.6/refcodes-data-ext-chess/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.1.6/refcodes-data-ext-corporate/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.1.6/refcodes-data-ext-symbols/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.1.6/refcodes-data-ext-symbols/README.md) 

## Change list &lt;refcodes-graphical&gt; (version 1.1.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-graphical/src/refcodes-graphical-1.1.6/pom.xml) 

## Change list &lt;refcodes-textual&gt; (version 1.1.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-1.1.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`CsvBuilderImpl.java`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-1.1.6/src/main/java/org/refcodes/textual/CsvBuilderImpl.java) (see Javadoc at [`CsvBuilderImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-textual/1.1.6/org/refcodes/textual/CsvBuilderImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`TextBlockBuilderImpl.java`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-1.1.6/src/main/java/org/refcodes/textual/TextBlockBuilderImpl.java) (see Javadoc at [`TextBlockBuilderImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-textual/1.1.6/org/refcodes/textual/TextBlockBuilderImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`RandomTextMode.java`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-1.1.6/src/main/java/org/refcodes/textual/RandomTextMode.java) (see Javadoc at [`RandomTextMode.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-textual/1.1.6/org/refcodes/textual/RandomTextMode.html))

## Change list &lt;refcodes-criteria&gt; (version 1.1.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-1.1.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-1.1.6/README.md) 

## Change list &lt;refcodes-tabular&gt; (version 1.1.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-tabular/src/refcodes-tabular-1.1.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`AbstractColumn.java`](https://bitbucket.org/refcodes/refcodes-tabular/src/refcodes-tabular-1.1.6/src/main/java/org/refcodes/tabular/AbstractColumn.java) (see Javadoc at [`AbstractColumn.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-tabular/1.1.6/org/refcodes/tabular/AbstractColumn.html))
* \[<span style="color:green">MODIFIED</span>\] [`CsvInputStreamRecordsImpl.java`](https://bitbucket.org/refcodes/refcodes-tabular/src/refcodes-tabular-1.1.6/src/main/java/org/refcodes/tabular/CsvInputStreamRecordsImpl.java) (see Javadoc at [`CsvInputStreamRecordsImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-tabular/1.1.6/org/refcodes/tabular/CsvInputStreamRecordsImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`TabularUtility.java`](https://bitbucket.org/refcodes/refcodes-tabular/src/refcodes-tabular-1.1.6/src/main/java/org/refcodes/tabular/TabularUtility.java) (see Javadoc at [`TabularUtility.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-tabular/1.1.6/org/refcodes/tabular/TabularUtility.html))

## Change list &lt;refcodes-logger&gt; (version 1.1.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger/src/refcodes-logger-1.1.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger/src/refcodes-logger-1.1.6/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`RuntimeLoggerFactoryImpl.java`](https://bitbucket.org/refcodes/refcodes-logger/src/refcodes-logger-1.1.6/src/main/java/org/refcodes/logger/RuntimeLoggerFactoryImpl.java) (see Javadoc at [`RuntimeLoggerFactoryImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-logger/1.1.6/org/refcodes/logger/RuntimeLoggerFactoryImpl.html))

## Change list &lt;refcodes-factory-alt&gt; (version 1.1.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-factory-alt/src/refcodes-factory-alt-1.1.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-factory-alt/src/refcodes-factory-alt-1.1.6/refcodes-factory-alt-spring/pom.xml) 

## Change list &lt;refcodes-logger-alt&gt; (version 1.1.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.1.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.1.6/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.1.6/refcodes-logger-alt-async/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.1.6/refcodes-logger-alt-async/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.1.6/refcodes-logger-alt-console/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.1.6/refcodes-logger-alt-console/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`ConsoleLoggerImpl.java`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.1.6/refcodes-logger-alt-console/src/main/java/org/refcodes/logger/alt/console/ConsoleLoggerImpl.java) (see Javadoc at [`ConsoleLoggerImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-logger-alt-console/1.1.6/org/refcodes/logger/alt/console/ConsoleLoggerImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`FormattedLoggerImpl.java`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.1.6/refcodes-logger-alt-console/src/main/java/org/refcodes/logger/alt/console/FormattedLoggerImpl.java) (see Javadoc at [`FormattedLoggerImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-logger-alt-console/1.1.6/org/refcodes/logger/alt/console/FormattedLoggerImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.1.6/refcodes-logger-alt-io/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.1.6/refcodes-logger-alt-io/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`IoLoggerImpl.java`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.1.6/refcodes-logger-alt-io/src/main/java/org/refcodes/logger/alt/io/IoLoggerImpl.java) (see Javadoc at [`IoLoggerImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-logger-alt-io/1.1.6/org/refcodes/logger/alt/io/IoLoggerImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.1.6/refcodes-logger-alt-simpledb/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.1.6/refcodes-logger-alt-slf4j/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`Slf4jRuntimeLoggerFactory.java`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.1.6/refcodes-logger-alt-slf4j/src/main/java/org/refcodes/logger/alt/slf4j/Slf4jRuntimeLoggerFactory.java) (see Javadoc at [`Slf4jRuntimeLoggerFactory.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-logger-alt-slf4j/1.1.6/org/refcodes/logger/alt/slf4j/Slf4jRuntimeLoggerFactory.html))
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.1.6/refcodes-logger-alt-spring/pom.xml) 

## Change list &lt;refcodes-matcher&gt; (version 1.1.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-matcher/src/refcodes-matcher-1.1.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`PathMatcherImpl.java`](https://bitbucket.org/refcodes/refcodes-matcher/src/refcodes-matcher-1.1.6/src/main/java/org/refcodes/matcher/PathMatcherImpl.java) (see Javadoc at [`PathMatcherImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-matcher/1.1.6/org/refcodes/matcher/PathMatcherImpl.html))

## Change list &lt;refcodes-observer&gt; (version 1.1.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-observer/src/refcodes-observer-1.1.6/pom.xml) 

## Change list &lt;refcodes-checkerboard&gt; (version 1.1.6)

* \[<span style="color:red">DELETED</span>\] `BoulderDashStatus.java`
* \[<span style="color:red">DELETED</span>\] `ChessmanStatus.java`
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-1.1.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-1.1.6/README.md) 

## Change list &lt;refcodes-graphical-ext&gt; (version 1.1.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-1.1.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-1.1.6/refcodes-graphical-ext-javafx/pom.xml) 

## Change list &lt;refcodes-checkerboard-alt&gt; (version 1.1.6)

* \[<span style="color:red">DELETED</span>\] `FxBoulderDashFactoryImpl.java`
* \[<span style="color:red">DELETED</span>\] `FxChessmenFactoryImpl.java`
* \[<span style="color:red">DELETED</span>\] `BoulderDashDemo.java`
* \[<span style="color:red">DELETED</span>\] `CheckerboardDemo.java`
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-alt/src/refcodes-checkerboard-alt-1.1.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-alt/src/refcodes-checkerboard-alt-1.1.6/refcodes-checkerboard-alt-javafx/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`FxBoulderDashFactory.java`](https://bitbucket.org/refcodes/refcodes-checkerboard-alt/src/refcodes-checkerboard-alt-1.1.6/refcodes-checkerboard-alt-javafx/src/main/java/org/refcodes/checkerboard/alt/javafx/FxBoulderDashFactory.java) (see Javadoc at [`FxBoulderDashFactory.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-checkerboard-alt-javafx/1.1.6/org/refcodes/checkerboard/alt/javafx/FxBoulderDashFactory.html))
* \[<span style="color:green">MODIFIED</span>\] [`FxChessmenFactory.java`](https://bitbucket.org/refcodes/refcodes-checkerboard-alt/src/refcodes-checkerboard-alt-1.1.6/refcodes-checkerboard-alt-javafx/src/main/java/org/refcodes/checkerboard/alt/javafx/FxChessmenFactory.java) (see Javadoc at [`FxChessmenFactory.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-checkerboard-alt-javafx/1.1.6/org/refcodes/checkerboard/alt/javafx/FxChessmenFactory.html))
* \[<span style="color:green">MODIFIED</span>\] [`FxCheckerboardViewerImpl.java`](https://bitbucket.org/refcodes/refcodes-checkerboard-alt/src/refcodes-checkerboard-alt-1.1.6/refcodes-checkerboard-alt-javafx/src/main/java/org/refcodes/checkerboard/alt/javafx/FxCheckerboardViewerImpl.java) (see Javadoc at [`FxCheckerboardViewerImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-checkerboard-alt-javafx/1.1.6/org/refcodes/checkerboard/alt/javafx/FxCheckerboardViewerImpl.html))

## Change list &lt;refcodes-io&gt; (version 1.1.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-io/src/refcodes-io-1.1.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`FileUtility.java`](https://bitbucket.org/refcodes/refcodes-io/src/refcodes-io-1.1.6/src/main/java/org/refcodes/io/FileUtility.java) (see Javadoc at [`FileUtility.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-io/1.1.6/org/refcodes/io/FileUtility.html))
* \[<span style="color:green">MODIFIED</span>\] [`FileUtilityTest.java`](https://bitbucket.org/refcodes/refcodes-io/src/refcodes-io-1.1.6/src/test/java/org/refcodes/io/FileUtilityTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`ZipFileStreamTest.java`](https://bitbucket.org/refcodes/refcodes-io/src/refcodes-io-1.1.6/src/test/java/org/refcodes/io/ZipFileStreamTest.java) 

## Change list &lt;refcodes-codec&gt; (version 1.1.6)

* \[<span style="color:blue">ADDED</span>\] [`ChannelSelectorAccessor.java`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-1.1.6/src/main/java/org/refcodes/codec/ChannelSelectorAccessor.java) (see Javadoc at [`ChannelSelectorAccessor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-codec/1.1.6/org/refcodes/codec/ChannelSelectorAccessor.html))
* \[<span style="color:blue">ADDED</span>\] [`ChannelSelector.java`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-1.1.6/src/main/java/org/refcodes/codec/ChannelSelector.java) (see Javadoc at [`ChannelSelector.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-codec/1.1.6/org/refcodes/codec/ChannelSelector.html))
* \[<span style="color:blue">ADDED</span>\] [`FrequencyThresholdAccessor.java`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-1.1.6/src/main/java/org/refcodes/codec/FrequencyThresholdAccessor.java) (see Javadoc at [`FrequencyThresholdAccessor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-codec/1.1.6/org/refcodes/codec/FrequencyThresholdAccessor.html))
* \[<span style="color:blue">ADDED</span>\] [`FrequencyThreshold.java`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-1.1.6/src/main/java/org/refcodes/codec/FrequencyThreshold.java) (see Javadoc at [`FrequencyThreshold.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-codec/1.1.6/org/refcodes/codec/FrequencyThreshold.html))
* \[<span style="color:blue">ADDED</span>\] [`ModemMetricsImpl.java`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-1.1.6/src/main/java/org/refcodes/codec/ModemMetricsImpl.java) (see Javadoc at [`ModemMetricsImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-codec/1.1.6/org/refcodes/codec/ModemMetricsImpl.html))
* \[<span style="color:blue">ADDED</span>\] [`ModemDecoder.java`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-1.1.6/src/main/java/org/refcodes/codec/ModemDecoder.java) (see Javadoc at [`ModemDecoder.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-codec/1.1.6/org/refcodes/codec/ModemDecoder.html))
* \[<span style="color:blue">ADDED</span>\] [`ModemEncoder.java`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-1.1.6/src/main/java/org/refcodes/codec/ModemEncoder.java) (see Javadoc at [`ModemEncoder.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-codec/1.1.6/org/refcodes/codec/ModemEncoder.html))
* \[<span style="color:blue">ADDED</span>\] [`ModemMetrics.java`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-1.1.6/src/main/java/org/refcodes/codec/ModemMetrics.java) (see Javadoc at [`ModemMetrics.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-codec/1.1.6/org/refcodes/codec/ModemMetrics.html))
* \[<span style="color:blue">ADDED</span>\] [`ModemModeAccessor.java`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-1.1.6/src/main/java/org/refcodes/codec/ModemModeAccessor.java) (see Javadoc at [`ModemModeAccessor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-codec/1.1.6/org/refcodes/codec/ModemModeAccessor.html))
* \[<span style="color:blue">ADDED</span>\] [`ModemMode.java`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-1.1.6/src/main/java/org/refcodes/codec/ModemMode.java) (see Javadoc at [`ModemMode.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-codec/1.1.6/org/refcodes/codec/ModemMode.html))
* \[<span style="color:blue">ADDED</span>\] [`ModulationFormatAccessor.java`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-1.1.6/src/main/java/org/refcodes/codec/ModulationFormatAccessor.java) (see Javadoc at [`ModulationFormatAccessor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-codec/1.1.6/org/refcodes/codec/ModulationFormatAccessor.html))
* \[<span style="color:blue">ADDED</span>\] [`ModulationFormat.java`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-1.1.6/src/main/java/org/refcodes/codec/ModulationFormat.java) (see Javadoc at [`ModulationFormat.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-codec/1.1.6/org/refcodes/codec/ModulationFormat.html))
* \[<span style="color:blue">ADDED</span>\] [`SampleRateAccessor.java`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-1.1.6/src/main/java/org/refcodes/codec/SampleRateAccessor.java) (see Javadoc at [`SampleRateAccessor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-codec/1.1.6/org/refcodes/codec/SampleRateAccessor.html))
* \[<span style="color:blue">ADDED</span>\] [`SampleRate.java`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-1.1.6/src/main/java/org/refcodes/codec/SampleRate.java) (see Javadoc at [`SampleRate.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-codec/1.1.6/org/refcodes/codec/SampleRate.html))
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-1.1.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-1.1.6/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`BaseBuilder.java`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-1.1.6/src/main/java/org/refcodes/codec/BaseBuilder.java) (see Javadoc at [`BaseBuilder.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-codec/1.1.6/org/refcodes/codec/BaseBuilder.html))
* \[<span style="color:green">MODIFIED</span>\] [`BaseMetricsConfig.java`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-1.1.6/src/main/java/org/refcodes/codec/BaseMetricsConfig.java) (see Javadoc at [`BaseMetricsConfig.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-codec/1.1.6/org/refcodes/codec/BaseMetricsConfig.html))
* \[<span style="color:green">MODIFIED</span>\] [`BaseMetrics.java`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-1.1.6/src/main/java/org/refcodes/codec/BaseMetrics.java) (see Javadoc at [`BaseMetrics.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-codec/1.1.6/org/refcodes/codec/BaseMetrics.html))
* \[<span style="color:green">MODIFIED</span>\] [`BaseBuilderImpl.java`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-1.1.6/src/main/java/org/refcodes/codec/BaseBuilderImpl.java) (see Javadoc at [`BaseBuilderImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-codec/1.1.6/org/refcodes/codec/BaseBuilderImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`BaseMetricsImpl.java`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-1.1.6/src/main/java/org/refcodes/codec/BaseMetricsImpl.java) (see Javadoc at [`BaseMetricsImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-codec/1.1.6/org/refcodes/codec/BaseMetricsImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`BaseBuilderTest.java`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-1.1.6/src/test/java/org/refcodes/codec/BaseBuilderTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`BaseInputStreamDecoderTest.java`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-1.1.6/src/test/java/org/refcodes/codec/BaseInputStreamDecoderTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`BaseOutputStreamEncoderTest.java`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-1.1.6/src/test/java/org/refcodes/codec/BaseOutputStreamEncoderTest.java) 

## Change list &lt;refcodes-command&gt; (version 1.1.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-command/src/refcodes-command-1.1.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-command/src/refcodes-command-1.1.6/README.md) 

## Change list &lt;refcodes-net&gt; (version 1.1.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.1.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`BadResponseException.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.1.6/src/main/java/org/refcodes/net/BadResponseException.java) (see Javadoc at [`BadResponseException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/1.1.6/org/refcodes/net/BadResponseException.html))
* \[<span style="color:green">MODIFIED</span>\] [`BadResponseRuntimeException.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.1.6/src/main/java/org/refcodes/net/BadResponseRuntimeException.java) (see Javadoc at [`BadResponseRuntimeException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/1.1.6/org/refcodes/net/BadResponseRuntimeException.html))
* \[<span style="color:green">MODIFIED</span>\] [`ContentEncoding.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.1.6/src/main/java/org/refcodes/net/ContentEncoding.java) (see Javadoc at [`ContentEncoding.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/1.1.6/org/refcodes/net/ContentEncoding.html))
* \[<span style="color:green">MODIFIED</span>\] [`FormFields.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.1.6/src/main/java/org/refcodes/net/FormFields.java) (see Javadoc at [`FormFields.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/1.1.6/org/refcodes/net/FormFields.html))
* \[<span style="color:green">MODIFIED</span>\] [`HeaderFields.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.1.6/src/main/java/org/refcodes/net/HeaderFields.java) (see Javadoc at [`HeaderFields.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/1.1.6/org/refcodes/net/HeaderFields.html))
* \[<span style="color:green">MODIFIED</span>\] [`HttpFields.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.1.6/src/main/java/org/refcodes/net/HttpFields.java) (see Javadoc at [`HttpFields.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/1.1.6/org/refcodes/net/HttpFields.html))
* \[<span style="color:green">MODIFIED</span>\] [`HttpResponseException.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.1.6/src/main/java/org/refcodes/net/HttpResponseException.java) (see Javadoc at [`HttpResponseException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/1.1.6/org/refcodes/net/HttpResponseException.html))
* \[<span style="color:green">MODIFIED</span>\] [`HttpResponseRuntimeException.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.1.6/src/main/java/org/refcodes/net/HttpResponseRuntimeException.java) (see Javadoc at [`HttpResponseRuntimeException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/1.1.6/org/refcodes/net/HttpResponseRuntimeException.html))
* \[<span style="color:green">MODIFIED</span>\] [`HttpStatusCode.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.1.6/src/main/java/org/refcodes/net/HttpStatusCode.java) (see Javadoc at [`HttpStatusCode.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/1.1.6/org/refcodes/net/HttpStatusCode.html))
* \[<span style="color:green">MODIFIED</span>\] [`AbstractHeaderFields.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.1.6/src/main/java/org/refcodes/net/AbstractHeaderFields.java) (see Javadoc at [`AbstractHeaderFields.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/1.1.6/org/refcodes/net/AbstractHeaderFields.html))
* \[<span style="color:green">MODIFIED</span>\] [`ApplicationJsonFactory.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.1.6/src/main/java/org/refcodes/net/ApplicationJsonFactory.java) (see Javadoc at [`ApplicationJsonFactory.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/1.1.6/org/refcodes/net/ApplicationJsonFactory.html))
* \[<span style="color:green">MODIFIED</span>\] [`ContentTypeImpl.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.1.6/src/main/java/org/refcodes/net/ContentTypeImpl.java) (see Javadoc at [`ContentTypeImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/1.1.6/org/refcodes/net/ContentTypeImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`HttpServerResponseImpl.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.1.6/src/main/java/org/refcodes/net/HttpServerResponseImpl.java) (see Javadoc at [`HttpServerResponseImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/1.1.6/org/refcodes/net/HttpServerResponseImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`MediaType.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.1.6/src/main/java/org/refcodes/net/MediaType.java) (see Javadoc at [`MediaType.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/1.1.6/org/refcodes/net/MediaType.html))
* \[<span style="color:green">MODIFIED</span>\] [`RequestCookie.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.1.6/src/main/java/org/refcodes/net/RequestCookie.java) (see Javadoc at [`RequestCookie.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/1.1.6/org/refcodes/net/RequestCookie.html))
* \[<span style="color:green">MODIFIED</span>\] [`ResponseCookie.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.1.6/src/main/java/org/refcodes/net/ResponseCookie.java) (see Javadoc at [`ResponseCookie.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/1.1.6/org/refcodes/net/ResponseCookie.html))
* \[<span style="color:green">MODIFIED</span>\] [`TopLevelType.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.1.6/src/main/java/org/refcodes/net/TopLevelType.java) (see Javadoc at [`TopLevelType.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/1.1.6/org/refcodes/net/TopLevelType.html))
* \[<span style="color:green">MODIFIED</span>\] [`RequestHeaderFieldsTest.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.1.6/src/test/java/org/refcodes/net/RequestHeaderFieldsTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`ResponseHeaderFieldsTest.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.1.6/src/test/java/org/refcodes/net/ResponseHeaderFieldsTest.java) 

## Change list &lt;refcodes-rest&gt; (version 1.1.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.1.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.1.6/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`HttpRestClient.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.1.6/src/main/java/org/refcodes/rest/HttpRestClient.java) (see Javadoc at [`HttpRestClient.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/1.1.6/org/refcodes/rest/HttpRestClient.html))
* \[<span style="color:green">MODIFIED</span>\] [`HttpRestClientSugar.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.1.6/src/main/java/org/refcodes/rest/HttpRestClientSugar.java) (see Javadoc at [`HttpRestClientSugar.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/1.1.6/org/refcodes/rest/HttpRestClientSugar.html))
* \[<span style="color:green">MODIFIED</span>\] [`AbstractRestServer.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.1.6/src/main/java/org/refcodes/rest/AbstractRestServer.java) (see Javadoc at [`AbstractRestServer.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/1.1.6/org/refcodes/rest/AbstractRestServer.html))
* \[<span style="color:green">MODIFIED</span>\] [`HttpRestClientImpl.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.1.6/src/main/java/org/refcodes/rest/HttpRestClientImpl.java) (see Javadoc at [`HttpRestClientImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/1.1.6/org/refcodes/rest/HttpRestClientImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`HttpRestServerImpl.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.1.6/src/main/java/org/refcodes/rest/HttpRestServerImpl.java) (see Javadoc at [`HttpRestServerImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/1.1.6/org/refcodes/rest/HttpRestServerImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`RestClient.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.1.6/src/main/java/org/refcodes/rest/RestClient.java) (see Javadoc at [`RestClient.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/1.1.6/org/refcodes/rest/RestClient.html))

## Change list &lt;refcodes-component-ext&gt; (version 1.1.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.1.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.1.6/refcodes-component-ext-observer/pom.xml) 

## Change list &lt;refcodes-cli&gt; (version 1.1.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-1.1.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-1.1.6/README.md) 

## Change list &lt;refcodes-security&gt; (version 1.1.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security/src/refcodes-security-1.1.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security/src/refcodes-security-1.1.6/README.md) 

## Change list &lt;refcodes-forwardsecrecy&gt; (version 1.1.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy/src/refcodes-forwardsecrecy-1.1.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy/src/refcodes-forwardsecrecy-1.1.6/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`ForwardSecrecyUtility.java`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy/src/refcodes-forwardsecrecy-1.1.6/src/main/java/org/refcodes/forwardsecrecy/ForwardSecrecyUtility.java) (see Javadoc at [`ForwardSecrecyUtility.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-forwardsecrecy/1.1.6/org/refcodes/forwardsecrecy/ForwardSecrecyUtility.html))
* \[<span style="color:green">MODIFIED</span>\] [`EncryptionProviderImpl.java`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy/src/refcodes-forwardsecrecy-1.1.6/src/main/java/org/refcodes/forwardsecrecy/EncryptionProviderImpl.java) (see Javadoc at [`EncryptionProviderImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-forwardsecrecy/1.1.6/org/refcodes/forwardsecrecy/EncryptionProviderImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`ForwardSecrecyUtilityTest.java`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy/src/refcodes-forwardsecrecy-1.1.6/src/test/java/org/refcodes/forwardsecrecy/ForwardSecrecyUtilityTest.java) 

## Change list &lt;refcodes-filesystem&gt; (version 1.1.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-filesystem/src/refcodes-filesystem-1.1.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`FileSystem.java`](https://bitbucket.org/refcodes/refcodes-filesystem/src/refcodes-filesystem-1.1.6/src/main/java/org/refcodes/filesystem/FileSystem.java) (see Javadoc at [`FileSystem.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-filesystem/1.1.6/org/refcodes/filesystem/FileSystem.html))

## Change list &lt;refcodes-forwardsecrecy-alt&gt; (version 1.1.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-1.1.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-1.1.6/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-1.1.6/refcodes-forwardsecrecy-alt-filesystem/pom.xml) 

## Change list &lt;refcodes-eventbus&gt; (version 1.1.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-eventbus/src/refcodes-eventbus-1.1.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-eventbus/src/refcodes-eventbus-1.1.6/README.md) 

## Change list &lt;refcodes-filesystem-alt&gt; (version 1.1.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-filesystem-alt/src/refcodes-filesystem-alt-1.1.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-filesystem-alt/src/refcodes-filesystem-alt-1.1.6/refcodes-filesystem-alt-s3/pom.xml) 

## Change list &lt;refcodes-io-ext&gt; (version 1.1.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-1.1.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-1.1.6/refcodes-io-ext-observable/pom.xml) 

## Change list &lt;refcodes-jobbus&gt; (version 1.1.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-jobbus/src/refcodes-jobbus-1.1.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-jobbus/src/refcodes-jobbus-1.1.6/README.md) 

## Change list &lt;refcodes-logger-ext&gt; (version 1.1.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-1.1.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-1.1.6/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-1.1.6/refcodes-logger-ext-slf4j/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-1.1.6/refcodes-logger-ext-slf4j/README.md) 

## Change list &lt;refcodes-remoting&gt; (version 1.1.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-remoting/src/refcodes-remoting-1.1.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-remoting/src/refcodes-remoting-1.1.6/README.md) 

## Change list &lt;refcodes-remoting-ext&gt; (version 1.1.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-1.1.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-1.1.6/refcodes-remoting-ext-observer/pom.xml) 

## Change list &lt;refcodes-security-alt&gt; (version 1.1.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-1.1.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-1.1.6/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-1.1.6/refcodes-security-alt-chaos/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-1.1.6/refcodes-security-alt-chaos/README.md) 

## Change list &lt;refcodes-security-ext&gt; (version 1.1.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-1.1.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-1.1.6/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-1.1.6/refcodes-security-ext-chaos/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-1.1.6/refcodes-security-ext-chaos/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-1.1.6/refcodes-security-ext-spring/pom.xml) 

## Change list &lt;refcodes-servicebus&gt; (version 1.1.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-servicebus/src/refcodes-servicebus-1.1.6/pom.xml) 

## Change list &lt;refcodes-servicebus-alt&gt; (version 1.1.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-servicebus-alt/src/refcodes-servicebus-alt-1.1.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-servicebus-alt/src/refcodes-servicebus-alt-1.1.6/refcodes-servicebus-alt-spring/pom.xml) 

## Change list &lt;refcodes-tabular-alt&gt; (version 1.1.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-tabular-alt/src/refcodes-tabular-alt-1.1.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-tabular-alt/src/refcodes-tabular-alt-1.1.6/refcodes-tabular-alt-forwardsecrecy/pom.xml) 
