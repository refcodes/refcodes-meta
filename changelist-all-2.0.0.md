# Change list for REFCODES.ORG artifacts' version 2.0.0

> This change list has been auto-generated on `triton.local` by `steiner` with `changelist-all.sh` on the 2019-02-19 at 15:02:01.

## Change list &lt;refcodes-licensing&gt; (version 2.0.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-licensing/src/refcodes-licensing-2.0.0/pom.xml) 

## Change list &lt;refcodes-parent&gt; (version 2.0.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-parent/src/refcodes-parent-2.0.0/pom.xml) 

## Change list &lt;refcodes-time&gt; (version 2.0.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-time/src/refcodes-time-2.0.0/pom.xml) 

## Change list &lt;refcodes-mixin&gt; (version 2.0.0)

* \[<span style="color:blue">ADDED</span>\] [`OffsetAccessor.java`](https://bitbucket.org/refcodes/refcodes-mixin/src/refcodes-mixin-2.0.0/src/main/java/org/refcodes/mixin/OffsetAccessor.java) (see Javadoc at [`OffsetAccessor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-mixin/2.0.0/org/refcodes/mixin/OffsetAccessor.html))
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-mixin/src/refcodes-mixin-2.0.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`ValueAccessor.java`](https://bitbucket.org/refcodes/refcodes-mixin/src/refcodes-mixin-2.0.0/src/main/java/org/refcodes/mixin/ValueAccessor.java) (see Javadoc at [`ValueAccessor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-mixin/2.0.0/org/refcodes/mixin/ValueAccessor.html))

## Change list &lt;refcodes-data&gt; (version 2.0.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data/src/refcodes-data-2.0.0/pom.xml) 

## Change list &lt;refcodes-exception&gt; (version 2.0.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-exception/src/refcodes-exception-2.0.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`UnmarshalException.java`](https://bitbucket.org/refcodes/refcodes-exception/src/refcodes-exception-2.0.0/src/main/java/org/refcodes/exception/UnmarshalException.java) (see Javadoc at [`UnmarshalException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-exception/2.0.0/org/refcodes/exception/UnmarshalException.html))

## Change list &lt;refcodes-factory&gt; (version 2.0.0)

* \[<span style="color:blue">ADDED</span>\] [`MarshalFactory.java`](https://bitbucket.org/refcodes/refcodes-factory/src/refcodes-factory-2.0.0/src/main/java/org/refcodes/factory/MarshalFactory.java) (see Javadoc at [`MarshalFactory.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-factory/2.0.0/org/refcodes/factory/MarshalFactory.html))
* \[<span style="color:blue">ADDED</span>\] [`MarshalTypeFactory.java`](https://bitbucket.org/refcodes/refcodes-factory/src/refcodes-factory-2.0.0/src/main/java/org/refcodes/factory/MarshalTypeFactory.java) (see Javadoc at [`MarshalTypeFactory.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-factory/2.0.0/org/refcodes/factory/MarshalTypeFactory.html))
* \[<span style="color:blue">ADDED</span>\] [`UnmarshalFactory.java`](https://bitbucket.org/refcodes/refcodes-factory/src/refcodes-factory-2.0.0/src/main/java/org/refcodes/factory/UnmarshalFactory.java) (see Javadoc at [`UnmarshalFactory.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-factory/2.0.0/org/refcodes/factory/UnmarshalFactory.html))
* \[<span style="color:blue">ADDED</span>\] [`UnmarshalTypeFactory.java`](https://bitbucket.org/refcodes/refcodes-factory/src/refcodes-factory-2.0.0/src/main/java/org/refcodes/factory/UnmarshalTypeFactory.java) (see Javadoc at [`UnmarshalTypeFactory.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-factory/2.0.0/org/refcodes/factory/UnmarshalTypeFactory.html))
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-factory/src/refcodes-factory-2.0.0/pom.xml) 

## Change list &lt;refcodes-factory-alt&gt; (version 2.0.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-factory-alt/src/refcodes-factory-alt-2.0.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-factory-alt/src/refcodes-factory-alt-2.0.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-factory-alt/src/refcodes-factory-alt-2.0.0/refcodes-factory-alt-spring/pom.xml) 

## Change list &lt;refcodes-controlflow&gt; (version 2.0.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-controlflow/src/refcodes-controlflow-2.0.0/pom.xml) 

## Change list &lt;refcodes-numerical&gt; (version 2.0.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-numerical/src/refcodes-numerical-2.0.0/pom.xml) 

## Change list &lt;refcodes-generator&gt; (version 2.0.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-generator/src/refcodes-generator-2.0.0/pom.xml) 

## Change list &lt;refcodes-matcher&gt; (version 2.0.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-matcher/src/refcodes-matcher-2.0.0/pom.xml) 

## Change list &lt;refcodes-structure&gt; (version 2.0.0)

* \[<span style="color:blue">ADDED</span>\] [`PathComparator.java`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-2.0.0/src/main/java/org/refcodes/structure/PathComparator.java) (see Javadoc at [`PathComparator.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-structure/2.0.0/org/refcodes/structure/PathComparator.html))
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-2.0.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-2.0.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`CanonicalMapBuilderImpl.java`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-2.0.0/src/main/java/org/refcodes/structure/CanonicalMapBuilderImpl.java) (see Javadoc at [`CanonicalMapBuilderImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-structure/2.0.0/org/refcodes/structure/CanonicalMapBuilderImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`CanonicalMapImpl.java`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-2.0.0/src/main/java/org/refcodes/structure/CanonicalMapImpl.java) (see Javadoc at [`CanonicalMapImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-structure/2.0.0/org/refcodes/structure/CanonicalMapImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`CanonicalMap.java`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-2.0.0/src/main/java/org/refcodes/structure/CanonicalMap.java) (see Javadoc at [`CanonicalMap.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-structure/2.0.0/org/refcodes/structure/CanonicalMap.html))
* \[<span style="color:green">MODIFIED</span>\] [`Keys.java`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-2.0.0/src/main/java/org/refcodes/structure/Keys.java) (see Javadoc at [`Keys.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-structure/2.0.0/org/refcodes/structure/Keys.html))
* \[<span style="color:green">MODIFIED</span>\] [`PathMapBuilderImpl.java`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-2.0.0/src/main/java/org/refcodes/structure/PathMapBuilderImpl.java) (see Javadoc at [`PathMapBuilderImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-structure/2.0.0/org/refcodes/structure/PathMapBuilderImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`PathMap.java`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-2.0.0/src/main/java/org/refcodes/structure/PathMap.java) (see Javadoc at [`PathMap.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-structure/2.0.0/org/refcodes/structure/PathMap.html))
* \[<span style="color:green">MODIFIED</span>\] [`PropertiesAccessorMixin.java`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-2.0.0/src/main/java/org/refcodes/structure/PropertiesAccessorMixin.java) (see Javadoc at [`PropertiesAccessorMixin.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-structure/2.0.0/org/refcodes/structure/PropertiesAccessorMixin.html))
* \[<span style="color:green">MODIFIED</span>\] [`CanonicalMapTest.java`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-2.0.0/src/test/java/org/refcodes/structure/CanonicalMapTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`PathMapArrayTest.java`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-2.0.0/src/test/java/org/refcodes/structure/PathMapArrayTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`PathMapPathTest.java`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-2.0.0/src/test/java/org/refcodes/structure/PathMapPathTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`PathMapQueryTest.java`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-2.0.0/src/test/java/org/refcodes/structure/PathMapQueryTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`PathMapTest.java`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-2.0.0/src/test/java/org/refcodes/structure/PathMapTest.java) 

## Change list &lt;refcodes-structure-ext&gt; (version 2.0.0)

* \[<span style="color:blue">ADDED</span>\] [`TomlCanonicalMapFactoryTest.java`](https://bitbucket.org/refcodes/refcodes-structure-ext/src/refcodes-structure-ext-2.0.0/refcodes-structure-ext-factory/src/test/java/org/refcodes/structure/ext/factory/TomlCanonicalMapFactoryTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-structure-ext/src/refcodes-structure-ext-2.0.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-structure-ext/src/refcodes-structure-ext-2.0.0/refcodes-structure-ext-factory/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`AbstractCanonicalMapFactory.java`](https://bitbucket.org/refcodes/refcodes-structure-ext/src/refcodes-structure-ext-2.0.0/refcodes-structure-ext-factory/src/main/java/org/refcodes/structure/ext/factory/AbstractCanonicalMapFactory.java) (see Javadoc at [`AbstractCanonicalMapFactory.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-structure-ext-factory/2.0.0/org/refcodes/structure/ext/factory/AbstractCanonicalMapFactory.html))
* \[<span style="color:green">MODIFIED</span>\] [`CanonicalMapFactory.java`](https://bitbucket.org/refcodes/refcodes-structure-ext/src/refcodes-structure-ext-2.0.0/refcodes-structure-ext-factory/src/main/java/org/refcodes/structure/ext/factory/CanonicalMapFactory.java) (see Javadoc at [`CanonicalMapFactory.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-structure-ext-factory/2.0.0/org/refcodes/structure/ext/factory/CanonicalMapFactory.html))
* \[<span style="color:green">MODIFIED</span>\] [`JavaCanonicalMapFactory.java`](https://bitbucket.org/refcodes/refcodes-structure-ext/src/refcodes-structure-ext-2.0.0/refcodes-structure-ext-factory/src/main/java/org/refcodes/structure/ext/factory/JavaCanonicalMapFactory.java) (see Javadoc at [`JavaCanonicalMapFactory.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-structure-ext-factory/2.0.0/org/refcodes/structure/ext/factory/JavaCanonicalMapFactory.html))
* \[<span style="color:green">MODIFIED</span>\] [`JsonCanonicalMapFactory.java`](https://bitbucket.org/refcodes/refcodes-structure-ext/src/refcodes-structure-ext-2.0.0/refcodes-structure-ext-factory/src/main/java/org/refcodes/structure/ext/factory/JsonCanonicalMapFactory.java) (see Javadoc at [`JsonCanonicalMapFactory.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-structure-ext-factory/2.0.0/org/refcodes/structure/ext/factory/JsonCanonicalMapFactory.html))
* \[<span style="color:green">MODIFIED</span>\] [`TomlCanonicalMapFactory.java`](https://bitbucket.org/refcodes/refcodes-structure-ext/src/refcodes-structure-ext-2.0.0/refcodes-structure-ext-factory/src/main/java/org/refcodes/structure/ext/factory/TomlCanonicalMapFactory.java) (see Javadoc at [`TomlCanonicalMapFactory.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-structure-ext-factory/2.0.0/org/refcodes/structure/ext/factory/TomlCanonicalMapFactory.html))
* \[<span style="color:green">MODIFIED</span>\] [`XmlCanonicalMapFactory.java`](https://bitbucket.org/refcodes/refcodes-structure-ext/src/refcodes-structure-ext-2.0.0/refcodes-structure-ext-factory/src/main/java/org/refcodes/structure/ext/factory/XmlCanonicalMapFactory.java) (see Javadoc at [`XmlCanonicalMapFactory.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-structure-ext-factory/2.0.0/org/refcodes/structure/ext/factory/XmlCanonicalMapFactory.html))
* \[<span style="color:green">MODIFIED</span>\] [`YamlCanonicalMapFactory.java`](https://bitbucket.org/refcodes/refcodes-structure-ext/src/refcodes-structure-ext-2.0.0/refcodes-structure-ext-factory/src/main/java/org/refcodes/structure/ext/factory/YamlCanonicalMapFactory.java) (see Javadoc at [`YamlCanonicalMapFactory.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-structure-ext-factory/2.0.0/org/refcodes/structure/ext/factory/YamlCanonicalMapFactory.html))
* \[<span style="color:green">MODIFIED</span>\] [`JavaCanonicalMapFactoryTest.java`](https://bitbucket.org/refcodes/refcodes-structure-ext/src/refcodes-structure-ext-2.0.0/refcodes-structure-ext-factory/src/test/java/org/refcodes/structure/ext/factory/JavaCanonicalMapFactoryTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`JsonCanonicalMapFactoryTest.java`](https://bitbucket.org/refcodes/refcodes-structure-ext/src/refcodes-structure-ext-2.0.0/refcodes-structure-ext-factory/src/test/java/org/refcodes/structure/ext/factory/JsonCanonicalMapFactoryTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`XmlCanonicalMapFactoryTest.java`](https://bitbucket.org/refcodes/refcodes-structure-ext/src/refcodes-structure-ext-2.0.0/refcodes-structure-ext-factory/src/test/java/org/refcodes/structure/ext/factory/XmlCanonicalMapFactoryTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`YamlCanonicalMapFactoryTest.java`](https://bitbucket.org/refcodes/refcodes-structure-ext/src/refcodes-structure-ext-2.0.0/refcodes-structure-ext-factory/src/test/java/org/refcodes/structure/ext/factory/YamlCanonicalMapFactoryTest.java) 

## Change list &lt;refcodes-component&gt; (version 2.0.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-2.0.0/pom.xml) 

## Change list &lt;refcodes-graphical&gt; (version 2.0.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-graphical/src/refcodes-graphical-2.0.0/pom.xml) 

## Change list &lt;refcodes-textual&gt; (version 2.0.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-2.0.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`VerboseTextBuilderImpl.java`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-2.0.0/src/main/java/org/refcodes/textual/VerboseTextBuilderImpl.java) (see Javadoc at [`VerboseTextBuilderImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-textual/2.0.0/org/refcodes/textual/VerboseTextBuilderImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`VerboseTextBuilder.java`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-2.0.0/src/main/java/org/refcodes/textual/VerboseTextBuilder.java) (see Javadoc at [`VerboseTextBuilder.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-textual/2.0.0/org/refcodes/textual/VerboseTextBuilder.html))

## Change list &lt;refcodes-criteria&gt; (version 2.0.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-2.0.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-2.0.0/README.md) 

## Change list &lt;refcodes-tabular&gt; (version 2.0.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-tabular/src/refcodes-tabular-2.0.0/pom.xml) 

## Change list &lt;refcodes-observer&gt; (version 2.0.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-observer/src/refcodes-observer-2.0.0/pom.xml) 

## Change list &lt;refcodes-command&gt; (version 2.0.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-command/src/refcodes-command-2.0.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-command/src/refcodes-command-2.0.0/README.md) 

## Change list &lt;refcodes-cli&gt; (version 2.0.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-2.0.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-2.0.0/README.md) 

## Change list &lt;refcodes-io&gt; (version 2.0.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-io/src/refcodes-io-2.0.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-io/src/refcodes-io-2.0.0/README.md) 

## Change list &lt;refcodes-codec&gt; (version 2.0.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-2.0.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-2.0.0/README.md) 

## Change list &lt;refcodes-component-ext&gt; (version 2.0.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-2.0.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-2.0.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-2.0.0/refcodes-component-ext-observer/pom.xml) 

## Change list &lt;refcodes-properties&gt; (version 2.0.0)

* \[<span style="color:blue">ADDED</span>\] [`PathComparatorTest.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-2.0.0/src/test/java/org/refcodes/configuration/PathComparatorTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-2.0.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-2.0.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`module-info.java.off`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-2.0.0/src/main/java/module-info.java.off) 
* \[<span style="color:green">MODIFIED</span>\] [`AbstractResourcePropertiesBuilder.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-2.0.0/src/main/java/org/refcodes/configuration/AbstractResourcePropertiesBuilder.java) (see Javadoc at [`AbstractResourcePropertiesBuilder.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties/2.0.0/org/refcodes/configuration/AbstractResourcePropertiesBuilder.html))
* \[<span style="color:green">MODIFIED</span>\] [`JavaPropertiesBuilder.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-2.0.0/src/main/java/org/refcodes/configuration/JavaPropertiesBuilder.java) (see Javadoc at [`JavaPropertiesBuilder.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties/2.0.0/org/refcodes/configuration/JavaPropertiesBuilder.html))
* \[<span style="color:green">MODIFIED</span>\] [`JsonPropertiesBuilder.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-2.0.0/src/main/java/org/refcodes/configuration/JsonPropertiesBuilder.java) (see Javadoc at [`JsonPropertiesBuilder.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties/2.0.0/org/refcodes/configuration/JsonPropertiesBuilder.html))
* \[<span style="color:green">MODIFIED</span>\] [`package-info.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-2.0.0/src/main/java/org/refcodes/configuration/package-info.java) 
* \[<span style="color:green">MODIFIED</span>\] [`ProfileProperties.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-2.0.0/src/main/java/org/refcodes/configuration/ProfileProperties.java) (see Javadoc at [`ProfileProperties.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties/2.0.0/org/refcodes/configuration/ProfileProperties.html))
* \[<span style="color:green">MODIFIED</span>\] [`Properties.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-2.0.0/src/main/java/org/refcodes/configuration/Properties.java) (see Javadoc at [`Properties.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties/2.0.0/org/refcodes/configuration/Properties.html))
* \[<span style="color:green">MODIFIED</span>\] [`ResourceProperties.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-2.0.0/src/main/java/org/refcodes/configuration/ResourceProperties.java) (see Javadoc at [`ResourceProperties.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties/2.0.0/org/refcodes/configuration/ResourceProperties.html))
* \[<span style="color:green">MODIFIED</span>\] [`StrictProperties.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-2.0.0/src/main/java/org/refcodes/configuration/StrictProperties.java) (see Javadoc at [`StrictProperties.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties/2.0.0/org/refcodes/configuration/StrictProperties.html))
* \[<span style="color:green">MODIFIED</span>\] [`TomlPropertiesBuilder.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-2.0.0/src/main/java/org/refcodes/configuration/TomlPropertiesBuilder.java) (see Javadoc at [`TomlPropertiesBuilder.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties/2.0.0/org/refcodes/configuration/TomlPropertiesBuilder.html))
* \[<span style="color:green">MODIFIED</span>\] [`XmlPropertiesBuilder.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-2.0.0/src/main/java/org/refcodes/configuration/XmlPropertiesBuilder.java) (see Javadoc at [`XmlPropertiesBuilder.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties/2.0.0/org/refcodes/configuration/XmlPropertiesBuilder.html))
* \[<span style="color:green">MODIFIED</span>\] [`YamlPropertiesBuilder.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-2.0.0/src/main/java/org/refcodes/configuration/YamlPropertiesBuilder.java) (see Javadoc at [`YamlPropertiesBuilder.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties/2.0.0/org/refcodes/configuration/YamlPropertiesBuilder.html))
* \[<span style="color:green">MODIFIED</span>\] [`JavaPropertiesTest.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-2.0.0/src/test/java/org/refcodes/configuration/JavaPropertiesTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`JsonPropertiesTest.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-2.0.0/src/test/java/org/refcodes/configuration/JsonPropertiesTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`XmlPropertiesTest.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-2.0.0/src/test/java/org/refcodes/configuration/XmlPropertiesTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`YamlPropertiesTest.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-2.0.0/src/test/java/org/refcodes/configuration/YamlPropertiesTest.java) 

## Change list &lt;refcodes-security&gt; (version 2.0.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security/src/refcodes-security-2.0.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security/src/refcodes-security-2.0.0/README.md) 

## Change list &lt;refcodes-security-alt&gt; (version 2.0.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-2.0.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-2.0.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-2.0.0/refcodes-security-alt-chaos/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-2.0.0/refcodes-security-alt-chaos/README.md) 

## Change list &lt;refcodes-security-ext&gt; (version 2.0.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-2.0.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-2.0.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-2.0.0/refcodes-security-ext-chaos/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-2.0.0/refcodes-security-ext-chaos/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-2.0.0/refcodes-security-ext-spring/pom.xml) 

## Change list &lt;refcodes-properties-ext&gt; (version 2.0.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-2.0.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-2.0.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-2.0.0/refcodes-properties-ext-cli/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-2.0.0/refcodes-properties-ext-cli/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-2.0.0/refcodes-properties-ext-obfuscation/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-2.0.0/refcodes-properties-ext-obfuscation/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`ObfuscationProperties.java`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-2.0.0/refcodes-properties-ext-obfuscation/src/main/java/org/refcodes/configuration/ext/obfuscation/ObfuscationProperties.java) (see Javadoc at [`ObfuscationProperties.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties-ext-obfuscation/2.0.0/org/refcodes/configuration/ext/obfuscation/ObfuscationProperties.html))
* \[<span style="color:green">MODIFIED</span>\] [`ObfuscationResourceProperties.java`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-2.0.0/refcodes-properties-ext-obfuscation/src/main/java/org/refcodes/configuration/ext/obfuscation/ObfuscationResourceProperties.java) (see Javadoc at [`ObfuscationResourceProperties.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties-ext-obfuscation/2.0.0/org/refcodes/configuration/ext/obfuscation/ObfuscationResourceProperties.html))
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-2.0.0/refcodes-properties-ext-observer/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-2.0.0/refcodes-properties-ext-observer/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`ObservableProperties.java`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-2.0.0/refcodes-properties-ext-observer/src/main/java/org/refcodes/configuration/ext/observer/ObservableProperties.java) (see Javadoc at [`ObservableProperties.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties-ext-observer/2.0.0/org/refcodes/configuration/ext/observer/ObservableProperties.html))
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-2.0.0/refcodes-properties-ext-runtime/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-2.0.0/refcodes-properties-ext-runtime/README.md) 

## Change list &lt;refcodes-logger&gt; (version 2.0.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger/src/refcodes-logger-2.0.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger/src/refcodes-logger-2.0.0/README.md) 

## Change list &lt;refcodes-logger-alt&gt; (version 2.0.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-2.0.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-2.0.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-2.0.0/refcodes-logger-alt-async/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-2.0.0/refcodes-logger-alt-async/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-2.0.0/refcodes-logger-alt-console/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-2.0.0/refcodes-logger-alt-console/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-2.0.0/refcodes-logger-alt-io/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-2.0.0/refcodes-logger-alt-io/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-2.0.0/refcodes-logger-alt-simpledb/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-2.0.0/refcodes-logger-alt-slf4j/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-2.0.0/refcodes-logger-alt-spring/pom.xml) 

## Change list &lt;refcodes-logger-ext&gt; (version 2.0.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-2.0.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-2.0.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-2.0.0/refcodes-logger-ext-slf4j/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-2.0.0/refcodes-logger-ext-slf4j/README.md) 

## Change list &lt;refcodes-graphical-ext&gt; (version 2.0.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-2.0.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-2.0.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-2.0.0/refcodes-graphical-ext-javafx/pom.xml) 

## Change list &lt;refcodes-checkerboard&gt; (version 2.0.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-2.0.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-2.0.0/README.md) 

## Change list &lt;refcodes-checkerboard-alt&gt; (version 2.0.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-alt/src/refcodes-checkerboard-alt-2.0.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-alt/src/refcodes-checkerboard-alt-2.0.0/refcodes-checkerboard-alt-javafx/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`FxCheckerboardViewerImpl.java`](https://bitbucket.org/refcodes/refcodes-checkerboard-alt/src/refcodes-checkerboard-alt-2.0.0/refcodes-checkerboard-alt-javafx/src/main/java/org/refcodes/checkerboard/alt/javafx/FxCheckerboardViewerImpl.java) (see Javadoc at [`FxCheckerboardViewerImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-checkerboard-alt-javafx/2.0.0/org/refcodes/checkerboard/alt/javafx/FxCheckerboardViewerImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`FxSpriteFactory.java`](https://bitbucket.org/refcodes/refcodes-checkerboard-alt/src/refcodes-checkerboard-alt-2.0.0/refcodes-checkerboard-alt-javafx/src/main/java/org/refcodes/checkerboard/alt/javafx/FxSpriteFactory.java) (see Javadoc at [`FxSpriteFactory.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-checkerboard-alt-javafx/2.0.0/org/refcodes/checkerboard/alt/javafx/FxSpriteFactory.html))

## Change list &lt;refcodes-checkerboard-ext&gt; (version 2.0.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-ext/src/refcodes-checkerboard-ext-2.0.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-checkerboard-ext/src/refcodes-checkerboard-ext-2.0.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-ext/src/refcodes-checkerboard-ext-2.0.0/refcodes-checkerboard-ext-javafx-boulderdash/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-ext/src/refcodes-checkerboard-ext-2.0.0/refcodes-checkerboard-ext-javafx-chess/pom.xml) 

## Change list &lt;refcodes-boulderdash&gt; (version 2.0.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-boulderdash/src/refcodes-boulderdash-2.0.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-boulderdash/src/refcodes-boulderdash-2.0.0/README.md) 

## Change list &lt;refcodes-net&gt; (version 2.0.0)

* \[<span style="color:red">DELETED</span>\] `Marshalable.java`
* \[<span style="color:red">DELETED</span>\] `Unmarshalable.java`
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-2.0.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`FormMediaTypeFactory.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-2.0.0/src/main/java/org/refcodes/net/FormMediaTypeFactory.java) (see Javadoc at [`FormMediaTypeFactory.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/2.0.0/org/refcodes/net/FormMediaTypeFactory.html))
* \[<span style="color:green">MODIFIED</span>\] [`HttpBodyMapImpl.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-2.0.0/src/main/java/org/refcodes/net/HttpBodyMapImpl.java) (see Javadoc at [`HttpBodyMapImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/2.0.0/org/refcodes/net/HttpBodyMapImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`HttpBodyMap.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-2.0.0/src/main/java/org/refcodes/net/HttpBodyMap.java) (see Javadoc at [`HttpBodyMap.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/2.0.0/org/refcodes/net/HttpBodyMap.html))
* \[<span style="color:green">MODIFIED</span>\] [`MediaTypeFactory.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-2.0.0/src/main/java/org/refcodes/net/MediaTypeFactory.java) (see Javadoc at [`MediaTypeFactory.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/2.0.0/org/refcodes/net/MediaTypeFactory.html))

## Change list &lt;refcodes-rest&gt; (version 2.0.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-2.0.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-2.0.0/README.md) 

## Change list &lt;refcodes-daemon&gt; (version 2.0.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-daemon/src/refcodes-daemon-2.0.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-daemon/src/refcodes-daemon-2.0.0/README.md) 

## Change list &lt;refcodes-eventbus&gt; (version 2.0.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-eventbus/src/refcodes-eventbus-2.0.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-eventbus/src/refcodes-eventbus-2.0.0/README.md) 

## Change list &lt;refcodes-eventbus-ext&gt; (version 2.0.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-2.0.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-2.0.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-2.0.0/refcodes-eventbus-ext-application/pom.xml) 

## Change list &lt;refcodes-filesystem&gt; (version 2.0.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-filesystem/src/refcodes-filesystem-2.0.0/pom.xml) 

## Change list &lt;refcodes-filesystem-alt&gt; (version 2.0.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-filesystem-alt/src/refcodes-filesystem-alt-2.0.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-filesystem-alt/src/refcodes-filesystem-alt-2.0.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-filesystem-alt/src/refcodes-filesystem-alt-2.0.0/refcodes-filesystem-alt-s3/pom.xml) 

## Change list &lt;refcodes-forwardsecrecy&gt; (version 2.0.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy/src/refcodes-forwardsecrecy-2.0.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy/src/refcodes-forwardsecrecy-2.0.0/README.md) 

## Change list &lt;refcodes-forwardsecrecy-alt&gt; (version 2.0.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-2.0.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-2.0.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-2.0.0/refcodes-forwardsecrecy-alt-filesystem/pom.xml) 

## Change list &lt;refcodes-io-ext&gt; (version 2.0.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-2.0.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-2.0.0/refcodes-io-ext-observable/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-2.0.0/refcodes-io-ext-observable/README.md) 

## Change list &lt;refcodes-interceptor&gt; (version 2.0.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-interceptor/src/refcodes-interceptor-2.0.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-interceptor/src/refcodes-interceptor-2.0.0/README.md) 

## Change list &lt;refcodes-jobbus&gt; (version 2.0.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-jobbus/src/refcodes-jobbus-2.0.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-jobbus/src/refcodes-jobbus-2.0.0/README.md) 

## Change list &lt;refcodes-rest-ext&gt; (version 2.0.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-2.0.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-2.0.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-2.0.0/refcodes-rest-ext-eureka/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-2.0.0/refcodes-rest-ext-eureka/README.md) 

## Change list &lt;refcodes-remoting&gt; (version 2.0.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-remoting/src/refcodes-remoting-2.0.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-remoting/src/refcodes-remoting-2.0.0/README.md) 

## Change list &lt;refcodes-remoting-ext&gt; (version 2.0.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-2.0.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-2.0.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-2.0.0/refcodes-remoting-ext-observer/pom.xml) 

## Change list &lt;refcodes-servicebus&gt; (version 2.0.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-servicebus/src/refcodes-servicebus-2.0.0/pom.xml) 

## Change list &lt;refcodes-servicebus-alt&gt; (version 2.0.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-servicebus-alt/src/refcodes-servicebus-alt-2.0.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-servicebus-alt/src/refcodes-servicebus-alt-2.0.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-servicebus-alt/src/refcodes-servicebus-alt-2.0.0/refcodes-servicebus-alt-spring/pom.xml) 

## Change list &lt;refcodes-tabular-alt&gt; (version 2.0.0)

* \[<span style="color:red">DELETED</span>\] `#Untitled-1#`
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-tabular-alt/src/refcodes-tabular-alt-2.0.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-tabular-alt/src/refcodes-tabular-alt-2.0.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-tabular-alt/src/refcodes-tabular-alt-2.0.0/refcodes-tabular-alt-forwardsecrecy/pom.xml) 
