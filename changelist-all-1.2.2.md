# Change list for REFCODES.ORG artifacts' version 1.2.2

> This change list has been auto-generated on `triton` by `steiner` with with `changelist-all.sh` on the 2018-02-14 at 11:24:48.

## Change list &lt;refcodes-licensing&gt; (version 1.2.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-licensing/src/refcodes-licensing-1.2.2/pom.xml) 

## Change list &lt;refcodes-parent&gt; (version 1.2.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-parent/src/refcodes-parent-1.2.2/pom.xml) 

## Change list &lt;refcodes-time&gt; (version 1.2.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-time/src/refcodes-time-1.2.2/pom.xml) 

## Change list &lt;refcodes-exception&gt; (version 1.2.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-exception/src/refcodes-exception-1.2.2/pom.xml) 

## Change list &lt;refcodes-mixin&gt; (version 1.2.2)

* \[<span style="color:blue">ADDED</span>\] [`InstanceIdAccessor.java`](https://bitbucket.org/refcodes/refcodes-mixin/src/refcodes-mixin-1.2.2/src/main/java/org/refcodes/mixin/InstanceIdAccessor.java) (see Javadoc at [`InstanceIdAccessor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-mixin/1.2.2/org/refcodes/mixin/InstanceIdAccessor.html))
* \[<span style="color:blue">ADDED</span>\] [`PortAccessor.java`](https://bitbucket.org/refcodes/refcodes-mixin/src/refcodes-mixin-1.2.2/src/main/java/org/refcodes/mixin/PortAccessor.java) (see Javadoc at [`PortAccessor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-mixin/1.2.2/org/refcodes/mixin/PortAccessor.html))
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-mixin/src/refcodes-mixin-1.2.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`CredentialsAccessor.java`](https://bitbucket.org/refcodes/refcodes-mixin/src/refcodes-mixin-1.2.2/src/main/java/org/refcodes/mixin/CredentialsAccessor.java) (see Javadoc at [`CredentialsAccessor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-mixin/1.2.2/org/refcodes/mixin/CredentialsAccessor.html))
* \[<span style="color:green">MODIFIED</span>\] [`Dumpable.java`](https://bitbucket.org/refcodes/refcodes-mixin/src/refcodes-mixin-1.2.2/src/main/java/org/refcodes/mixin/Dumpable.java) (see Javadoc at [`Dumpable.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-mixin/1.2.2/org/refcodes/mixin/Dumpable.html))

## Change list &lt;refcodes-data&gt; (version 1.2.2)

* \[<span style="color:blue">ADDED</span>\] [`Port.java`](https://bitbucket.org/refcodes/refcodes-data/src/refcodes-data-1.2.2/src/main/java/org/refcodes/data/Port.java) (see Javadoc at [`Port.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-data/1.2.2/org/refcodes/data/Port.html))
* \[<span style="color:blue">ADDED</span>\] [`Scheme.java`](https://bitbucket.org/refcodes/refcodes-data/src/refcodes-data-1.2.2/src/main/java/org/refcodes/data/Scheme.java) (see Javadoc at [`Scheme.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-data/1.2.2/org/refcodes/data/Scheme.html))
* \[<span style="color:blue">ADDED</span>\] [`UrlSchemeTest.java`](https://bitbucket.org/refcodes/refcodes-data/src/refcodes-data-1.2.2/src/test/java/org/refcodes/data/UrlSchemeTest.java) 
* \[<span style="color:red">DELETED</span>\] `Protocol.java`
* \[<span style="color:red">DELETED</span>\] `Proxy.java`
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data/src/refcodes-data-1.2.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`Delimiter.java`](https://bitbucket.org/refcodes/refcodes-data/src/refcodes-data-1.2.2/src/main/java/org/refcodes/data/Delimiter.java) (see Javadoc at [`Delimiter.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-data/1.2.2/org/refcodes/data/Delimiter.html))

## Change list &lt;refcodes-structure&gt; (version 1.2.2)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-1.2.2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-1.2.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`CanonicalMap.java`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-1.2.2/src/main/java/org/refcodes/structure/CanonicalMap.java) (see Javadoc at [`CanonicalMap.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-structure/1.2.2/org/refcodes/structure/CanonicalMap.html))
* \[<span style="color:green">MODIFIED</span>\] [`CanonicalMapBuilderImpl.java`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-1.2.2/src/main/java/org/refcodes/structure/CanonicalMapBuilderImpl.java) (see Javadoc at [`CanonicalMapBuilderImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-structure/1.2.2/org/refcodes/structure/CanonicalMapBuilderImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`CanonicalMapImpl.java`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-1.2.2/src/main/java/org/refcodes/structure/CanonicalMapImpl.java) (see Javadoc at [`CanonicalMapImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-structure/1.2.2/org/refcodes/structure/CanonicalMapImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`PathMap.java`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-1.2.2/src/main/java/org/refcodes/structure/PathMap.java) (see Javadoc at [`PathMap.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-structure/1.2.2/org/refcodes/structure/PathMap.html))
* \[<span style="color:green">MODIFIED</span>\] [`PathMapBuilderImpl.java`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-1.2.2/src/main/java/org/refcodes/structure/PathMapBuilderImpl.java) (see Javadoc at [`PathMapBuilderImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-structure/1.2.2/org/refcodes/structure/PathMapBuilderImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`PathMapImpl.java`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-1.2.2/src/main/java/org/refcodes/structure/PathMapImpl.java) (see Javadoc at [`PathMapImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-structure/1.2.2/org/refcodes/structure/PathMapImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`TypeUtility.java`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-1.2.2/src/main/java/org/refcodes/structure/TypeUtility.java) (see Javadoc at [`TypeUtility.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-structure/1.2.2/org/refcodes/structure/TypeUtility.html))
* \[<span style="color:green">MODIFIED</span>\] [`PathMapTest.java`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-1.2.2/src/test/java/org/refcodes/structure/PathMapTest.java) 

## Change list &lt;refcodes-controlflow&gt; (version 1.2.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-controlflow/src/refcodes-controlflow-1.2.2/pom.xml) 

## Change list &lt;refcodes-numerical&gt; (version 1.2.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-numerical/src/refcodes-numerical-1.2.2/pom.xml) 

## Change list &lt;refcodes-generator&gt; (version 1.2.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-generator/src/refcodes-generator-1.2.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`UniqueIdGeneratorImpl.java`](https://bitbucket.org/refcodes/refcodes-generator/src/refcodes-generator-1.2.2/src/main/java/org/refcodes/generator/UniqueIdGeneratorImpl.java) (see Javadoc at [`UniqueIdGeneratorImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-generator/1.2.2/org/refcodes/generator/UniqueIdGeneratorImpl.html))

## Change list &lt;refcodes-runtime&gt; (version 1.2.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-1.2.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`DumpBuilderImpl.java`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-1.2.2/src/main/java/org/refcodes/runtime/DumpBuilderImpl.java) (see Javadoc at [`DumpBuilderImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-runtime/1.2.2/org/refcodes/runtime/DumpBuilderImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`RuntimeUtility.java`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-1.2.2/src/main/java/org/refcodes/runtime/RuntimeUtility.java) (see Javadoc at [`RuntimeUtility.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-runtime/1.2.2/org/refcodes/runtime/RuntimeUtility.html))

## Change list &lt;refcodes-component&gt; (version 1.2.2)

* \[<span style="color:blue">ADDED</span>\] [`ExecutionStatus.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-1.2.2/src/main/java/org/refcodes/component/ExecutionStatus.java) (see Javadoc at [`ExecutionStatus.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/1.2.2/org/refcodes/component/ExecutionStatus.html))
* \[<span style="color:blue">ADDED</span>\] [`HealthChecker.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-1.2.2/src/main/java/org/refcodes/component/HealthChecker.java) (see Javadoc at [`HealthChecker.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/1.2.2/org/refcodes/component/HealthChecker.html))
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-1.2.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`ComponentUtility.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-1.2.2/src/main/java/org/refcodes/component/ComponentUtility.java) (see Javadoc at [`ComponentUtility.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/1.2.2/org/refcodes/component/ComponentUtility.html))
* \[<span style="color:green">MODIFIED</span>\] [`Configurable.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-1.2.2/src/main/java/org/refcodes/component/Configurable.java) (see Javadoc at [`Configurable.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/1.2.2/org/refcodes/component/Configurable.html))
* \[<span style="color:green">MODIFIED</span>\] [`ConfigurableLifeCycleAutomatonImpl.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-1.2.2/src/main/java/org/refcodes/component/ConfigurableLifeCycleAutomatonImpl.java) (see Javadoc at [`ConfigurableLifeCycleAutomatonImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/1.2.2/org/refcodes/component/ConfigurableLifeCycleAutomatonImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`ConnectionStatusAccessor.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-1.2.2/src/main/java/org/refcodes/component/ConnectionStatusAccessor.java) (see Javadoc at [`ConnectionStatusAccessor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/1.2.2/org/refcodes/component/ConnectionStatusAccessor.html))
* \[<span style="color:green">MODIFIED</span>\] [`LifeCycleAutomatonImpl.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-1.2.2/src/main/java/org/refcodes/component/LifeCycleAutomatonImpl.java) (see Javadoc at [`LifeCycleAutomatonImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/1.2.2/org/refcodes/component/LifeCycleAutomatonImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`LifeCycleStatus.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-1.2.2/src/main/java/org/refcodes/component/LifeCycleStatus.java) (see Javadoc at [`LifeCycleStatus.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/1.2.2/org/refcodes/component/LifeCycleStatus.html))
* \[<span style="color:green">MODIFIED</span>\] [`ConfigurableLifeCycleAutomatonTest.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-1.2.2/src/test/java/org/refcodes/component/ConfigurableLifeCycleAutomatonTest.java) 

## Change list &lt;refcodes-properties&gt; (version 1.2.2)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.2.2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.2.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`AbstractPropertiesDecorator.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.2.2/src/main/java/org/refcodes/configuration/AbstractPropertiesDecorator.java) (see Javadoc at [`AbstractPropertiesDecorator.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties/1.2.2/org/refcodes/configuration/AbstractPropertiesDecorator.html))
* \[<span style="color:green">MODIFIED</span>\] [`AbstractResourcePropertiesBuilder.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.2.2/src/main/java/org/refcodes/configuration/AbstractResourcePropertiesBuilder.java) (see Javadoc at [`AbstractResourcePropertiesBuilder.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties/1.2.2/org/refcodes/configuration/AbstractResourcePropertiesBuilder.html))
* \[<span style="color:green">MODIFIED</span>\] [`JavaProperties.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.2.2/src/main/java/org/refcodes/configuration/JavaProperties.java) (see Javadoc at [`JavaProperties.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties/1.2.2/org/refcodes/configuration/JavaProperties.html))
* \[<span style="color:green">MODIFIED</span>\] [`JsonProperties.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.2.2/src/main/java/org/refcodes/configuration/JsonProperties.java) (see Javadoc at [`JsonProperties.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties/1.2.2/org/refcodes/configuration/JsonProperties.html))
* \[<span style="color:green">MODIFIED</span>\] [`NormalizedPropertiesDecorator.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.2.2/src/main/java/org/refcodes/configuration/NormalizedPropertiesDecorator.java) (see Javadoc at [`NormalizedPropertiesDecorator.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties/1.2.2/org/refcodes/configuration/NormalizedPropertiesDecorator.html))
* \[<span style="color:green">MODIFIED</span>\] [`ProfilePropertiesProjection.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.2.2/src/main/java/org/refcodes/configuration/ProfilePropertiesProjection.java) (see Javadoc at [`ProfilePropertiesProjection.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties/1.2.2/org/refcodes/configuration/ProfilePropertiesProjection.html))
* \[<span style="color:green">MODIFIED</span>\] [`Properties.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.2.2/src/main/java/org/refcodes/configuration/Properties.java) (see Javadoc at [`Properties.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties/1.2.2/org/refcodes/configuration/Properties.html))
* \[<span style="color:green">MODIFIED</span>\] [`PropertiesBuilderImpl.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.2.2/src/main/java/org/refcodes/configuration/PropertiesBuilderImpl.java) (see Javadoc at [`PropertiesBuilderImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties/1.2.2/org/refcodes/configuration/PropertiesBuilderImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`PropertiesImpl.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.2.2/src/main/java/org/refcodes/configuration/PropertiesImpl.java) (see Javadoc at [`PropertiesImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties/1.2.2/org/refcodes/configuration/PropertiesImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`PropertiesPrecedenceComposite.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.2.2/src/main/java/org/refcodes/configuration/PropertiesPrecedenceComposite.java) (see Javadoc at [`PropertiesPrecedenceComposite.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties/1.2.2/org/refcodes/configuration/PropertiesPrecedenceComposite.html))
* \[<span style="color:green">MODIFIED</span>\] [`ResourceProperties.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.2.2/src/main/java/org/refcodes/configuration/ResourceProperties.java) (see Javadoc at [`ResourceProperties.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties/1.2.2/org/refcodes/configuration/ResourceProperties.html))
* \[<span style="color:green">MODIFIED</span>\] [`TomlProperties.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.2.2/src/main/java/org/refcodes/configuration/TomlProperties.java) (see Javadoc at [`TomlProperties.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties/1.2.2/org/refcodes/configuration/TomlProperties.html))
* \[<span style="color:green">MODIFIED</span>\] [`XmlProperties.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.2.2/src/main/java/org/refcodes/configuration/XmlProperties.java) (see Javadoc at [`XmlProperties.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties/1.2.2/org/refcodes/configuration/XmlProperties.html))
* \[<span style="color:green">MODIFIED</span>\] [`YamlProperties.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.2.2/src/main/java/org/refcodes/configuration/YamlProperties.java) (see Javadoc at [`YamlProperties.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties/1.2.2/org/refcodes/configuration/YamlProperties.html))
* \[<span style="color:green">MODIFIED</span>\] [`package-info.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.2.2/src/main/java/org/refcodes/configuration/package-info.java) 
* \[<span style="color:green">MODIFIED</span>\] [`ProfilePropertiesTest.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.2.2/src/test/java/org/refcodes/configuration/ProfilePropertiesTest.java) 

## Change list &lt;refcodes-interceptor&gt; (version 1.2.2)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-interceptor/src/refcodes-interceptor-1.2.2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-interceptor/src/refcodes-interceptor-1.2.2/pom.xml) 

## Change list &lt;refcodes-factory&gt; (version 1.2.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-factory/src/refcodes-factory-1.2.2/pom.xml) 

## Change list &lt;refcodes-data-ext&gt; (version 1.2.2)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.2.2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.2.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`org.eclipse.jdt.core.prefs`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.2.2/refcodes-data-ext-boulderdash/.settings/org.eclipse.jdt.core.prefs) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.2.2/refcodes-data-ext-boulderdash/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`org.eclipse.jdt.core.prefs`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.2.2/refcodes-data-ext-checkers/.settings/org.eclipse.jdt.core.prefs) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.2.2/refcodes-data-ext-checkers/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.2.2/refcodes-data-ext-checkers/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`org.eclipse.jdt.core.prefs`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.2.2/refcodes-data-ext-chess/.settings/org.eclipse.jdt.core.prefs) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.2.2/refcodes-data-ext-chess/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.2.2/refcodes-data-ext-chess/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.2.2/refcodes-data-ext-corporate/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.2.2/refcodes-data-ext-symbols/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.2.2/refcodes-data-ext-symbols/pom.xml) 

## Change list &lt;refcodes-graphical&gt; (version 1.2.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-graphical/src/refcodes-graphical-1.2.2/pom.xml) 

## Change list &lt;refcodes-textual&gt; (version 1.2.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-1.2.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`AsciiArtBuilderImpl.java`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-1.2.2/src/main/java/org/refcodes/textual/AsciiArtBuilderImpl.java) (see Javadoc at [`AsciiArtBuilderImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-textual/1.2.2/org/refcodes/textual/AsciiArtBuilderImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`TextLineBuilder.java`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-1.2.2/src/main/java/org/refcodes/textual/TextLineBuilder.java) (see Javadoc at [`TextLineBuilder.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-textual/1.2.2/org/refcodes/textual/TextLineBuilder.html))
* \[<span style="color:green">MODIFIED</span>\] [`TextLineBuilderImpl.java`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-1.2.2/src/main/java/org/refcodes/textual/TextLineBuilderImpl.java) (see Javadoc at [`TextLineBuilderImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-textual/1.2.2/org/refcodes/textual/TextLineBuilderImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`CsvBuilderTest.java`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-1.2.2/src/test/java/org/refcodes/textual/CsvBuilderTest.java) 

## Change list &lt;refcodes-criteria&gt; (version 1.2.2)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-1.2.2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-1.2.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`DoubleEvaluator.java`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-1.2.2/src/main/java/org/matheclipse/parser/client/eval/DoubleEvaluator.java) (see Javadoc at [`DoubleEvaluator.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-criteria/1.2.2/org/matheclipse/parser/client/eval/DoubleEvaluator.html))
* \[<span style="color:green">MODIFIED</span>\] [`MathUtils.java`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-1.2.2/src/main/java/org/matheclipse/parser/client/math/MathUtils.java) (see Javadoc at [`MathUtils.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-criteria/1.2.2/org/matheclipse/parser/client/math/MathUtils.html))

## Change list &lt;refcodes-tabular&gt; (version 1.2.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-tabular/src/refcodes-tabular-1.2.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`ColumnTest.java`](https://bitbucket.org/refcodes/refcodes-tabular/src/refcodes-tabular-1.2.2/src/test/java/org/refcodes/tabular/ColumnTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`CsvStreamRecordsTest.java`](https://bitbucket.org/refcodes/refcodes-tabular/src/refcodes-tabular-1.2.2/src/test/java/org/refcodes/tabular/CsvStreamRecordsTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`HeaderTest.java`](https://bitbucket.org/refcodes/refcodes-tabular/src/refcodes-tabular-1.2.2/src/test/java/org/refcodes/tabular/HeaderTest.java) 

## Change list &lt;refcodes-logger&gt; (version 1.2.2)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger/src/refcodes-logger-1.2.2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger/src/refcodes-logger-1.2.2/pom.xml) 

## Change list &lt;refcodes-factory-alt&gt; (version 1.2.2)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-factory-alt/src/refcodes-factory-alt-1.2.2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-factory-alt/src/refcodes-factory-alt-1.2.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-factory-alt/src/refcodes-factory-alt-1.2.2/refcodes-factory-alt-spring/pom.xml) 

## Change list &lt;refcodes-logger-alt&gt; (version 1.2.2)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.2.2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.2.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`org.eclipse.jdt.core.prefs`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.2.2/refcodes-logger-alt-async/.settings/org.eclipse.jdt.core.prefs) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.2.2/refcodes-logger-alt-async/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.2.2/refcodes-logger-alt-async/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.2.2/refcodes-logger-alt-console/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.2.2/refcodes-logger-alt-console/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.2.2/refcodes-logger-alt-io/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.2.2/refcodes-logger-alt-io/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.2.2/refcodes-logger-alt-simpledb/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.2.2/refcodes-logger-alt-slf4j/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.2.2/refcodes-logger-alt-spring/pom.xml) 

## Change list &lt;refcodes-matcher&gt; (version 1.2.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-matcher/src/refcodes-matcher-1.2.2/pom.xml) 

## Change list &lt;refcodes-observer&gt; (version 1.2.2)

* \[<span style="color:blue">ADDED</span>\] [`Observers.java`](https://bitbucket.org/refcodes/refcodes-observer/src/refcodes-observer-1.2.2/src/main/java/org/refcodes/observer/Observers.java) (see Javadoc at [`Observers.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-observer/1.2.2/org/refcodes/observer/Observers.html))
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-observer/src/refcodes-observer-1.2.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`AbstractObservable.java`](https://bitbucket.org/refcodes/refcodes-observer/src/refcodes-observer-1.2.2/src/main/java/org/refcodes/observer/AbstractObservable.java) (see Javadoc at [`AbstractObservable.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-observer/1.2.2/org/refcodes/observer/AbstractObservable.html))
* \[<span style="color:green">MODIFIED</span>\] [`ActionEvent.java`](https://bitbucket.org/refcodes/refcodes-observer/src/refcodes-observer-1.2.2/src/main/java/org/refcodes/observer/ActionEvent.java) (see Javadoc at [`ActionEvent.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-observer/1.2.2/org/refcodes/observer/ActionEvent.html))
* \[<span style="color:green">MODIFIED</span>\] [`Event.java`](https://bitbucket.org/refcodes/refcodes-observer/src/refcodes-observer-1.2.2/src/main/java/org/refcodes/observer/Event.java) (see Javadoc at [`Event.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-observer/1.2.2/org/refcodes/observer/Event.html))
* \[<span style="color:green">MODIFIED</span>\] [`MetaDataActionEvent.java`](https://bitbucket.org/refcodes/refcodes-observer/src/refcodes-observer-1.2.2/src/main/java/org/refcodes/observer/MetaDataActionEvent.java) (see Javadoc at [`MetaDataActionEvent.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-observer/1.2.2/org/refcodes/observer/MetaDataActionEvent.html))
* \[<span style="color:green">MODIFIED</span>\] [`MetaDataEvent.java`](https://bitbucket.org/refcodes/refcodes-observer/src/refcodes-observer-1.2.2/src/main/java/org/refcodes/observer/MetaDataEvent.java) (see Javadoc at [`MetaDataEvent.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-observer/1.2.2/org/refcodes/observer/MetaDataEvent.html))
* \[<span style="color:green">MODIFIED</span>\] [`Observable.java`](https://bitbucket.org/refcodes/refcodes-observer/src/refcodes-observer-1.2.2/src/main/java/org/refcodes/observer/Observable.java) (see Javadoc at [`Observable.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-observer/1.2.2/org/refcodes/observer/Observable.html))

## Change list &lt;refcodes-properties-ext&gt; (version 1.2.2)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.2.2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.2.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.2.2/refcodes-properties-ext-observer/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.2.2/refcodes-properties-ext-observer/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`AbstractObservablePropertiesBuilderDecorator.java`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.2.2/refcodes-properties-ext-observer/src/main/java/org/refcodes/configuration/ext/observer/AbstractObservablePropertiesBuilderDecorator.java) (see Javadoc at [`AbstractObservablePropertiesBuilderDecorator.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties-ext-observer/1.2.2/org/refcodes/configuration/ext/observer/AbstractObservablePropertiesBuilderDecorator.html))
* \[<span style="color:green">MODIFIED</span>\] [`AbstractObservableResourcePropertiesBuilderDecorator.java`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.2.2/refcodes-properties-ext-observer/src/main/java/org/refcodes/configuration/ext/observer/AbstractObservableResourcePropertiesBuilderDecorator.java) (see Javadoc at [`AbstractObservableResourcePropertiesBuilderDecorator.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties-ext-observer/1.2.2/org/refcodes/configuration/ext/observer/AbstractObservableResourcePropertiesBuilderDecorator.html))

## Change list &lt;refcodes-checkerboard&gt; (version 1.2.2)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-1.2.2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-1.2.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`BackgroundFactory.java`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-1.2.2/src/main/java/org/refcodes/checkerboard/BackgroundFactory.java) (see Javadoc at [`BackgroundFactory.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-checkerboard/1.2.2/org/refcodes/checkerboard/BackgroundFactory.html))

## Change list &lt;refcodes-graphical-ext&gt; (version 1.2.2)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-1.2.2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-1.2.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-1.2.2/refcodes-graphical-ext-javafx/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`FxRasterFactory.java`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-1.2.2/refcodes-graphical-ext-javafx/src/main/java/org/refcodes/graphical/ext/javafx/FxRasterFactory.java) (see Javadoc at [`FxRasterFactory.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-graphical-ext-javafx/1.2.2/org/refcodes/graphical/ext/javafx/FxRasterFactory.html))

## Change list &lt;refcodes-checkerboard-alt&gt; (version 1.2.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-alt/src/refcodes-checkerboard-alt-1.2.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-alt/src/refcodes-checkerboard-alt-1.2.2/refcodes-checkerboard-alt-javafx/pom.xml) 

## Change list &lt;refcodes-checkerboard-ext&gt; (version 1.2.2)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-checkerboard-ext/src/refcodes-checkerboard-ext-1.2.2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-ext/src/refcodes-checkerboard-ext-1.2.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-ext/src/refcodes-checkerboard-ext-1.2.2/refcodes-checkerboard-ext-javafx-boulderdash/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-ext/src/refcodes-checkerboard-ext-1.2.2/refcodes-checkerboard-ext-javafx-chess/pom.xml) 

## Change list &lt;refcodes-io&gt; (version 1.2.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-io/src/refcodes-io-1.2.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`FileUtility.java`](https://bitbucket.org/refcodes/refcodes-io/src/refcodes-io-1.2.2/src/main/java/org/refcodes/io/FileUtility.java) (see Javadoc at [`FileUtility.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-io/1.2.2/org/refcodes/io/FileUtility.html))
* \[<span style="color:green">MODIFIED</span>\] [`MaxConnectionsAccessor.java`](https://bitbucket.org/refcodes/refcodes-io/src/refcodes-io-1.2.2/src/main/java/org/refcodes/io/MaxConnectionsAccessor.java) (see Javadoc at [`MaxConnectionsAccessor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-io/1.2.2/org/refcodes/io/MaxConnectionsAccessor.html))

## Change list &lt;refcodes-codec&gt; (version 1.2.2)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-1.2.2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-1.2.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`ModemDecoder.java`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-1.2.2/src/main/java/org/refcodes/codec/ModemDecoder.java) (see Javadoc at [`ModemDecoder.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-codec/1.2.2/org/refcodes/codec/ModemDecoder.html))
* \[<span style="color:green">MODIFIED</span>\] [`ModemEncoder.java`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-1.2.2/src/main/java/org/refcodes/codec/ModemEncoder.java) (see Javadoc at [`ModemEncoder.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-codec/1.2.2/org/refcodes/codec/ModemEncoder.html))

## Change list &lt;refcodes-command&gt; (version 1.2.2)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-command/src/refcodes-command-1.2.2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-command/src/refcodes-command-1.2.2/pom.xml) 

## Change list &lt;refcodes-component-ext&gt; (version 1.2.2)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.2.2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.2.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.2.2/refcodes-component-ext-observer/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`ObservableLifeCycleAutomatonImpl.java`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.2.2/refcodes-component-ext-observer/src/main/java/org/refcodes/component/ext/observer/ObservableLifeCycleAutomatonImpl.java) (see Javadoc at [`ObservableLifeCycleAutomatonImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component-ext-observer/1.2.2/org/refcodes/component/ext/observer/ObservableLifeCycleAutomatonImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`ObservableLifeCycleRequestAutomatonImpl.java`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.2.2/refcodes-component-ext-observer/src/main/java/org/refcodes/component/ext/observer/ObservableLifeCycleRequestAutomatonImpl.java) (see Javadoc at [`ObservableLifeCycleRequestAutomatonImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component-ext-observer/1.2.2/org/refcodes/component/ext/observer/ObservableLifeCycleRequestAutomatonImpl.html))

## Change list &lt;refcodes-cli&gt; (version 1.2.2)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-1.2.2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-1.2.2/pom.xml) 

## Change list &lt;refcodes-security&gt; (version 1.2.2)

* \[<span style="color:blue">ADDED</span>\] [`KeyStoreDescriptor.java`](https://bitbucket.org/refcodes/refcodes-security/src/refcodes-security-1.2.2/src/main/java/org/refcodes/security/KeyStoreDescriptor.java) (see Javadoc at [`KeyStoreDescriptor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-security/1.2.2/org/refcodes/security/KeyStoreDescriptor.html))
* \[<span style="color:blue">ADDED</span>\] [`KeyStoreDescriptorAccessor.java`](https://bitbucket.org/refcodes/refcodes-security/src/refcodes-security-1.2.2/src/main/java/org/refcodes/security/KeyStoreDescriptorAccessor.java) (see Javadoc at [`KeyStoreDescriptorAccessor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-security/1.2.2/org/refcodes/security/KeyStoreDescriptorAccessor.html))
* \[<span style="color:blue">ADDED</span>\] [`KeyStoreDescriptorBuilderImpl.java`](https://bitbucket.org/refcodes/refcodes-security/src/refcodes-security-1.2.2/src/main/java/org/refcodes/security/KeyStoreDescriptorBuilderImpl.java) (see Javadoc at [`KeyStoreDescriptorBuilderImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-security/1.2.2/org/refcodes/security/KeyStoreDescriptorBuilderImpl.html))
* \[<span style="color:blue">ADDED</span>\] [`KeyStoreDescriptorImpl.java`](https://bitbucket.org/refcodes/refcodes-security/src/refcodes-security-1.2.2/src/main/java/org/refcodes/security/KeyStoreDescriptorImpl.java) (see Javadoc at [`KeyStoreDescriptorImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-security/1.2.2/org/refcodes/security/KeyStoreDescriptorImpl.html))
* \[<span style="color:blue">ADDED</span>\] [`StoreType.java`](https://bitbucket.org/refcodes/refcodes-security/src/refcodes-security-1.2.2/src/main/java/org/refcodes/security/StoreType.java) (see Javadoc at [`StoreType.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-security/1.2.2/org/refcodes/security/StoreType.html))
* \[<span style="color:blue">ADDED</span>\] [`TrustStoreDescriptor.java`](https://bitbucket.org/refcodes/refcodes-security/src/refcodes-security-1.2.2/src/main/java/org/refcodes/security/TrustStoreDescriptor.java) (see Javadoc at [`TrustStoreDescriptor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-security/1.2.2/org/refcodes/security/TrustStoreDescriptor.html))
* \[<span style="color:blue">ADDED</span>\] [`TrustStoreDescriptorAccessor.java`](https://bitbucket.org/refcodes/refcodes-security/src/refcodes-security-1.2.2/src/main/java/org/refcodes/security/TrustStoreDescriptorAccessor.java) (see Javadoc at [`TrustStoreDescriptorAccessor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-security/1.2.2/org/refcodes/security/TrustStoreDescriptorAccessor.html))
* \[<span style="color:blue">ADDED</span>\] [`TrustStoreDescriptorBuilderImpl.java`](https://bitbucket.org/refcodes/refcodes-security/src/refcodes-security-1.2.2/src/main/java/org/refcodes/security/TrustStoreDescriptorBuilderImpl.java) (see Javadoc at [`TrustStoreDescriptorBuilderImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-security/1.2.2/org/refcodes/security/TrustStoreDescriptorBuilderImpl.html))
* \[<span style="color:blue">ADDED</span>\] [`TrustStoreDescriptorImpl.java`](https://bitbucket.org/refcodes/refcodes-security/src/refcodes-security-1.2.2/src/main/java/org/refcodes/security/TrustStoreDescriptorImpl.java) (see Javadoc at [`TrustStoreDescriptorImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-security/1.2.2/org/refcodes/security/TrustStoreDescriptorImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security/src/refcodes-security-1.2.2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security/src/refcodes-security-1.2.2/pom.xml) 

## Change list &lt;refcodes-net&gt; (version 1.2.2)

* \[<span style="color:blue">ADDED</span>\] [`FragmentAccessor.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.2.2/src/main/java/org/refcodes/net/FragmentAccessor.java) (see Javadoc at [`FragmentAccessor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/1.2.2/org/refcodes/net/FragmentAccessor.html))
* \[<span style="color:blue">ADDED</span>\] [`HostAccessor.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.2.2/src/main/java/org/refcodes/net/HostAccessor.java) (see Javadoc at [`HostAccessor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/1.2.2/org/refcodes/net/HostAccessor.html))
* \[<span style="color:blue">ADDED</span>\] [`HttpClientContext.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.2.2/src/main/java/org/refcodes/net/HttpClientContext.java) (see Javadoc at [`HttpClientContext.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/1.2.2/org/refcodes/net/HttpClientContext.html))
* \[<span style="color:blue">ADDED</span>\] [`HttpClientContextBuilderImpl.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.2.2/src/main/java/org/refcodes/net/HttpClientContextBuilderImpl.java) (see Javadoc at [`HttpClientContextBuilderImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/1.2.2/org/refcodes/net/HttpClientContextBuilderImpl.html))
* \[<span style="color:blue">ADDED</span>\] [`HttpServerContext.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.2.2/src/main/java/org/refcodes/net/HttpServerContext.java) (see Javadoc at [`HttpServerContext.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/1.2.2/org/refcodes/net/HttpServerContext.html))
* \[<span style="color:blue">ADDED</span>\] [`HttpServerContextBuilderImpl.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.2.2/src/main/java/org/refcodes/net/HttpServerContextBuilderImpl.java) (see Javadoc at [`HttpServerContextBuilderImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/1.2.2/org/refcodes/net/HttpServerContextBuilderImpl.html))
* \[<span style="color:blue">ADDED</span>\] [`HttpVersionNotSupportedException.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.2.2/src/main/java/org/refcodes/net/HttpVersionNotSupportedException.java) (see Javadoc at [`HttpVersionNotSupportedException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/1.2.2/org/refcodes/net/HttpVersionNotSupportedException.html))
* \[<span style="color:blue">ADDED</span>\] [`HttpVersionNotSupportedRuntimeException.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.2.2/src/main/java/org/refcodes/net/HttpVersionNotSupportedRuntimeException.java) (see Javadoc at [`HttpVersionNotSupportedRuntimeException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/1.2.2/org/refcodes/net/HttpVersionNotSupportedRuntimeException.html))
* \[<span style="color:blue">ADDED</span>\] [`InternalClientErrorException.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.2.2/src/main/java/org/refcodes/net/InternalClientErrorException.java) (see Javadoc at [`InternalClientErrorException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/1.2.2/org/refcodes/net/InternalClientErrorException.html))
* \[<span style="color:blue">ADDED</span>\] [`InternalClientErrorRuntimeException.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.2.2/src/main/java/org/refcodes/net/InternalClientErrorRuntimeException.java) (see Javadoc at [`InternalClientErrorRuntimeException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/1.2.2/org/refcodes/net/InternalClientErrorRuntimeException.html))
* \[<span style="color:blue">ADDED</span>\] [`IpAddress.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.2.2/src/main/java/org/refcodes/net/IpAddress.java) (see Javadoc at [`IpAddress.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/1.2.2/org/refcodes/net/IpAddress.html))
* \[<span style="color:blue">ADDED</span>\] [`IpAddressAccessor.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.2.2/src/main/java/org/refcodes/net/IpAddressAccessor.java) (see Javadoc at [`IpAddressAccessor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/1.2.2/org/refcodes/net/IpAddressAccessor.html))
* \[<span style="color:blue">ADDED</span>\] [`LoadBalancingStrategy.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.2.2/src/main/java/org/refcodes/net/LoadBalancingStrategy.java) (see Javadoc at [`LoadBalancingStrategy.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/1.2.2/org/refcodes/net/LoadBalancingStrategy.html))
* \[<span style="color:blue">ADDED</span>\] [`LoadBalancingStrategyAccessor.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.2.2/src/main/java/org/refcodes/net/LoadBalancingStrategyAccessor.java) (see Javadoc at [`LoadBalancingStrategyAccessor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/1.2.2/org/refcodes/net/LoadBalancingStrategyAccessor.html))
* \[<span style="color:blue">ADDED</span>\] [`Proxy.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.2.2/src/main/java/org/refcodes/net/Proxy.java) (see Javadoc at [`Proxy.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/1.2.2/org/refcodes/net/Proxy.html))
* \[<span style="color:blue">ADDED</span>\] [`SchemeAccessor.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.2.2/src/main/java/org/refcodes/net/SchemeAccessor.java) (see Javadoc at [`SchemeAccessor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/1.2.2/org/refcodes/net/SchemeAccessor.html))
* \[<span style="color:blue">ADDED</span>\] [`TransportLayerProtocol.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.2.2/src/main/java/org/refcodes/net/TransportLayerProtocol.java) (see Javadoc at [`TransportLayerProtocol.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/1.2.2/org/refcodes/net/TransportLayerProtocol.html))
* \[<span style="color:blue">ADDED</span>\] [`UnavailableForLegalReasonsException.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.2.2/src/main/java/org/refcodes/net/UnavailableForLegalReasonsException.java) (see Javadoc at [`UnavailableForLegalReasonsException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/1.2.2/org/refcodes/net/UnavailableForLegalReasonsException.html))
* \[<span style="color:blue">ADDED</span>\] [`UnavailableForLegalReasonsRuntimeException.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.2.2/src/main/java/org/refcodes/net/UnavailableForLegalReasonsRuntimeException.java) (see Javadoc at [`UnavailableForLegalReasonsRuntimeException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/1.2.2/org/refcodes/net/UnavailableForLegalReasonsRuntimeException.html))
* \[<span style="color:blue">ADDED</span>\] [`Url.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.2.2/src/main/java/org/refcodes/net/Url.java) (see Javadoc at [`Url.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/1.2.2/org/refcodes/net/Url.html))
* \[<span style="color:blue">ADDED</span>\] [`UrlImpl.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.2.2/src/main/java/org/refcodes/net/UrlImpl.java) (see Javadoc at [`UrlImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/1.2.2/org/refcodes/net/UrlImpl.html))
* \[<span style="color:blue">ADDED</span>\] [`UrlSugar.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.2.2/src/main/java/org/refcodes/net/UrlSugar.java) (see Javadoc at [`UrlSugar.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/1.2.2/org/refcodes/net/UrlSugar.html))
* \[<span style="color:blue">ADDED</span>\] [`VirtualHostAccessor.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.2.2/src/main/java/org/refcodes/net/VirtualHostAccessor.java) (see Javadoc at [`VirtualHostAccessor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/1.2.2/org/refcodes/net/VirtualHostAccessor.html))
* \[<span style="color:blue">ADDED</span>\] [`HttpStatusCodeTest.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.2.2/src/test/java/org/refcodes/net/HttpStatusCodeTest.java) 
* \[<span style="color:blue">ADDED</span>\] [`IpAddressTest.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.2.2/src/test/java/org/refcodes/net/IpAddressTest.java) 
* \[<span style="color:blue">ADDED</span>\] [`UrlTest.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.2.2/src/test/java/org/refcodes/net/UrlTest.java) 
* \[<span style="color:red">DELETED</span>\] `Port.java`
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.2.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`ApplicationFormFactory.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.2.2/src/main/java/org/refcodes/net/ApplicationFormFactory.java) (see Javadoc at [`ApplicationFormFactory.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/1.2.2/org/refcodes/net/ApplicationFormFactory.html))
* \[<span style="color:green">MODIFIED</span>\] [`BaseUrlAccessor.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.2.2/src/main/java/org/refcodes/net/BaseUrlAccessor.java) (see Javadoc at [`BaseUrlAccessor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/1.2.2/org/refcodes/net/BaseUrlAccessor.html))
* \[<span style="color:green">MODIFIED</span>\] [`BasicAuthCredentialsImpl.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.2.2/src/main/java/org/refcodes/net/BasicAuthCredentialsImpl.java) (see Javadoc at [`BasicAuthCredentialsImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/1.2.2/org/refcodes/net/BasicAuthCredentialsImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`BasicCredentials.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.2.2/src/main/java/org/refcodes/net/BasicCredentials.java) (see Javadoc at [`BasicCredentials.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/1.2.2/org/refcodes/net/BasicCredentials.html))
* \[<span style="color:green">MODIFIED</span>\] [`BasicCredentialsImpl.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.2.2/src/main/java/org/refcodes/net/BasicCredentialsImpl.java) (see Javadoc at [`BasicCredentialsImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/1.2.2/org/refcodes/net/BasicCredentialsImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`FormFields.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.2.2/src/main/java/org/refcodes/net/FormFields.java) (see Javadoc at [`FormFields.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/1.2.2/org/refcodes/net/FormFields.html))
* \[<span style="color:green">MODIFIED</span>\] [`FormFieldsImpl.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.2.2/src/main/java/org/refcodes/net/FormFieldsImpl.java) (see Javadoc at [`FormFieldsImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/1.2.2/org/refcodes/net/FormFieldsImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`HttpBodyMap.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.2.2/src/main/java/org/refcodes/net/HttpBodyMap.java) (see Javadoc at [`HttpBodyMap.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/1.2.2/org/refcodes/net/HttpBodyMap.html))
* \[<span style="color:green">MODIFIED</span>\] [`HttpBodyMapImpl.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.2.2/src/main/java/org/refcodes/net/HttpBodyMapImpl.java) (see Javadoc at [`HttpBodyMapImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/1.2.2/org/refcodes/net/HttpBodyMapImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`HttpClientRequestImpl.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.2.2/src/main/java/org/refcodes/net/HttpClientRequestImpl.java) (see Javadoc at [`HttpClientRequestImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/1.2.2/org/refcodes/net/HttpClientRequestImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`HttpRequest.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.2.2/src/main/java/org/refcodes/net/HttpRequest.java) (see Javadoc at [`HttpRequest.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/1.2.2/org/refcodes/net/HttpRequest.html))
* \[<span style="color:green">MODIFIED</span>\] [`HttpRequestBuilder.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.2.2/src/main/java/org/refcodes/net/HttpRequestBuilder.java) (see Javadoc at [`HttpRequestBuilder.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/1.2.2/org/refcodes/net/HttpRequestBuilder.html))
* \[<span style="color:green">MODIFIED</span>\] [`HttpRequestImpl.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.2.2/src/main/java/org/refcodes/net/HttpRequestImpl.java) (see Javadoc at [`HttpRequestImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/1.2.2/org/refcodes/net/HttpRequestImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`HttpServerRequestImpl.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.2.2/src/main/java/org/refcodes/net/HttpServerRequestImpl.java) (see Javadoc at [`HttpServerRequestImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/1.2.2/org/refcodes/net/HttpServerRequestImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`HttpStatusCode.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.2.2/src/main/java/org/refcodes/net/HttpStatusCode.java) (see Javadoc at [`HttpStatusCode.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/1.2.2/org/refcodes/net/HttpStatusCode.html))
* \[<span style="color:green">MODIFIED</span>\] [`PortAlreadyBoundException.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.2.2/src/main/java/org/refcodes/net/PortAlreadyBoundException.java) (see Javadoc at [`PortAlreadyBoundException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/1.2.2/org/refcodes/net/PortAlreadyBoundException.html))
* \[<span style="color:green">MODIFIED</span>\] [`PortAlreadyBoundRuntimeException.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.2.2/src/main/java/org/refcodes/net/PortAlreadyBoundRuntimeException.java) (see Javadoc at [`PortAlreadyBoundRuntimeException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/1.2.2/org/refcodes/net/PortAlreadyBoundRuntimeException.html))
* \[<span style="color:green">MODIFIED</span>\] [`PortManagerImpl.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.2.2/src/main/java/org/refcodes/net/PortManagerImpl.java) (see Javadoc at [`PortManagerImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/1.2.2/org/refcodes/net/PortManagerImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`package-info.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.2.2/src/main/java/org/refcodes/net/package-info.java) 
* \[<span style="color:green">MODIFIED</span>\] [`refcodes-net`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.2.2/src/main/java/org/refcodes/net/refcodes-net) 
* \[<span style="color:green">MODIFIED</span>\] [`ApplicationFormFactoryTest.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.2.2/src/test/java/org/refcodes/net/ApplicationFormFactoryTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`FormFieldsTest.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.2.2/src/test/java/org/refcodes/net/FormFieldsTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`PortManagerTest.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.2.2/src/test/java/org/refcodes/net/PortManagerTest.java) 

## Change list &lt;refcodes-rest&gt; (version 1.2.2)

* \[<span style="color:blue">ADDED</span>\] [`AbstractHttpDiscoveryRestClientDecorator.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.2.2/src/main/java/org/refcodes/rest/AbstractHttpDiscoveryRestClientDecorator.java) (see Javadoc at [`AbstractHttpDiscoveryRestClientDecorator.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/1.2.2/org/refcodes/rest/AbstractHttpDiscoveryRestClientDecorator.html))
* \[<span style="color:blue">ADDED</span>\] [`AbstractHttpRegistryContextBuilder.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.2.2/src/main/java/org/refcodes/rest/AbstractHttpRegistryContextBuilder.java) (see Javadoc at [`AbstractHttpRegistryContextBuilder.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/1.2.2/org/refcodes/rest/AbstractHttpRegistryContextBuilder.html))
* \[<span style="color:blue">ADDED</span>\] [`AbstractHttpRegistryRestServerDecorator.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.2.2/src/main/java/org/refcodes/rest/AbstractHttpRegistryRestServerDecorator.java) (see Javadoc at [`AbstractHttpRegistryRestServerDecorator.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/1.2.2/org/refcodes/rest/AbstractHttpRegistryRestServerDecorator.html))
* \[<span style="color:blue">ADDED</span>\] [`AbstractHttpRestClientDecorator.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.2.2/src/main/java/org/refcodes/rest/AbstractHttpRestClientDecorator.java) (see Javadoc at [`AbstractHttpRestClientDecorator.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/1.2.2/org/refcodes/rest/AbstractHttpRestClientDecorator.html))
* \[<span style="color:blue">ADDED</span>\] [`AbstractHttpRestServerDecorator.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.2.2/src/main/java/org/refcodes/rest/AbstractHttpRestServerDecorator.java) (see Javadoc at [`AbstractHttpRestServerDecorator.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/1.2.2/org/refcodes/rest/AbstractHttpRestServerDecorator.html))
* \[<span style="color:blue">ADDED</span>\] [`HttpDiscoveryContext.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.2.2/src/main/java/org/refcodes/rest/HttpDiscoveryContext.java) (see Javadoc at [`HttpDiscoveryContext.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/1.2.2/org/refcodes/rest/HttpDiscoveryContext.html))
* \[<span style="color:blue">ADDED</span>\] [`HttpDiscoveryContextBuilderImpl.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.2.2/src/main/java/org/refcodes/rest/HttpDiscoveryContextBuilderImpl.java) (see Javadoc at [`HttpDiscoveryContextBuilderImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/1.2.2/org/refcodes/rest/HttpDiscoveryContextBuilderImpl.html))
* \[<span style="color:blue">ADDED</span>\] [`HttpDiscoveryRestClient.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.2.2/src/main/java/org/refcodes/rest/HttpDiscoveryRestClient.java) (see Javadoc at [`HttpDiscoveryRestClient.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/1.2.2/org/refcodes/rest/HttpDiscoveryRestClient.html))
* \[<span style="color:blue">ADDED</span>\] [`HttpDiscoveryUrlAccessor.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.2.2/src/main/java/org/refcodes/rest/HttpDiscoveryUrlAccessor.java) (see Javadoc at [`HttpDiscoveryUrlAccessor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/1.2.2/org/refcodes/rest/HttpDiscoveryUrlAccessor.html))
* \[<span style="color:blue">ADDED</span>\] [`HttpRegistryContext.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.2.2/src/main/java/org/refcodes/rest/HttpRegistryContext.java) (see Javadoc at [`HttpRegistryContext.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/1.2.2/org/refcodes/rest/HttpRegistryContext.html))
* \[<span style="color:blue">ADDED</span>\] [`HttpRegistryRestServer.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.2.2/src/main/java/org/refcodes/rest/HttpRegistryRestServer.java) (see Javadoc at [`HttpRegistryRestServer.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/1.2.2/org/refcodes/rest/HttpRegistryRestServer.html))
* \[<span style="color:blue">ADDED</span>\] [`HttpRegistryUrlAccessor.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.2.2/src/main/java/org/refcodes/rest/HttpRegistryUrlAccessor.java) (see Javadoc at [`HttpRegistryUrlAccessor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/1.2.2/org/refcodes/rest/HttpRegistryUrlAccessor.html))
* \[<span style="color:blue">ADDED</span>\] [`HttpServerDescriptor.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.2.2/src/main/java/org/refcodes/rest/HttpServerDescriptor.java) (see Javadoc at [`HttpServerDescriptor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/1.2.2/org/refcodes/rest/HttpServerDescriptor.html))
* \[<span style="color:blue">ADDED</span>\] [`HttpServerDescriptorAccessor.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.2.2/src/main/java/org/refcodes/rest/HttpServerDescriptorAccessor.java) (see Javadoc at [`HttpServerDescriptorAccessor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/1.2.2/org/refcodes/rest/HttpServerDescriptorAccessor.html))
* \[<span style="color:blue">ADDED</span>\] [`PingPathAccessor.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.2.2/src/main/java/org/refcodes/rest/PingPathAccessor.java) (see Javadoc at [`PingPathAccessor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/1.2.2/org/refcodes/rest/PingPathAccessor.html))
* \[<span style="color:blue">ADDED</span>\] [`PingUrlAccessor.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.2.2/src/main/java/org/refcodes/rest/PingUrlAccessor.java) (see Javadoc at [`PingUrlAccessor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/1.2.2/org/refcodes/rest/PingUrlAccessor.html))
* \[<span style="color:blue">ADDED</span>\] [`RestDeleteClient.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.2.2/src/main/java/org/refcodes/rest/RestDeleteClient.java) (see Javadoc at [`RestDeleteClient.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/1.2.2/org/refcodes/rest/RestDeleteClient.html))
* \[<span style="color:blue">ADDED</span>\] [`RestDeleteClientSugar.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.2.2/src/main/java/org/refcodes/rest/RestDeleteClientSugar.java) (see Javadoc at [`RestDeleteClientSugar.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/1.2.2/org/refcodes/rest/RestDeleteClientSugar.html))
* \[<span style="color:blue">ADDED</span>\] [`RestGetClient.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.2.2/src/main/java/org/refcodes/rest/RestGetClient.java) (see Javadoc at [`RestGetClient.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/1.2.2/org/refcodes/rest/RestGetClient.html))
* \[<span style="color:blue">ADDED</span>\] [`RestGetClientSugar.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.2.2/src/main/java/org/refcodes/rest/RestGetClientSugar.java) (see Javadoc at [`RestGetClientSugar.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/1.2.2/org/refcodes/rest/RestGetClientSugar.html))
* \[<span style="color:blue">ADDED</span>\] [`RestPostClient.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.2.2/src/main/java/org/refcodes/rest/RestPostClient.java) (see Javadoc at [`RestPostClient.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/1.2.2/org/refcodes/rest/RestPostClient.html))
* \[<span style="color:blue">ADDED</span>\] [`RestPostClientSugar.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.2.2/src/main/java/org/refcodes/rest/RestPostClientSugar.java) (see Javadoc at [`RestPostClientSugar.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/1.2.2/org/refcodes/rest/RestPostClientSugar.html))
* \[<span style="color:blue">ADDED</span>\] [`RestPutClient.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.2.2/src/main/java/org/refcodes/rest/RestPutClient.java) (see Javadoc at [`RestPutClient.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/1.2.2/org/refcodes/rest/RestPutClient.html))
* \[<span style="color:blue">ADDED</span>\] [`RestPutClientSugar.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.2.2/src/main/java/org/refcodes/rest/RestPutClientSugar.java) (see Javadoc at [`RestPutClientSugar.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/1.2.2/org/refcodes/rest/RestPutClientSugar.html))
* \[<span style="color:blue">ADDED</span>\] [`RestRequestClient.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.2.2/src/main/java/org/refcodes/rest/RestRequestClient.java) (see Javadoc at [`RestRequestClient.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/1.2.2/org/refcodes/rest/RestRequestClient.html))
* \[<span style="color:blue">ADDED</span>\] [`RestRequestClientSugar.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.2.2/src/main/java/org/refcodes/rest/RestRequestClientSugar.java) (see Javadoc at [`RestRequestClientSugar.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/1.2.2/org/refcodes/rest/RestRequestClientSugar.html))
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.2.2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.2.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`AbstractRestClient.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.2.2/src/main/java/org/refcodes/rest/AbstractRestClient.java) (see Javadoc at [`AbstractRestClient.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/1.2.2/org/refcodes/rest/AbstractRestClient.html))
* \[<span style="color:green">MODIFIED</span>\] [`AbstractRestServer.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.2.2/src/main/java/org/refcodes/rest/AbstractRestServer.java) (see Javadoc at [`AbstractRestServer.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/1.2.2/org/refcodes/rest/AbstractRestServer.html))
* \[<span style="color:green">MODIFIED</span>\] [`HttpRestClient.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.2.2/src/main/java/org/refcodes/rest/HttpRestClient.java) (see Javadoc at [`HttpRestClient.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/1.2.2/org/refcodes/rest/HttpRestClient.html))
* \[<span style="color:green">MODIFIED</span>\] [`HttpRestClientImpl.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.2.2/src/main/java/org/refcodes/rest/HttpRestClientImpl.java) (see Javadoc at [`HttpRestClientImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/1.2.2/org/refcodes/rest/HttpRestClientImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`HttpRestClientSugar.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.2.2/src/main/java/org/refcodes/rest/HttpRestClientSugar.java) (see Javadoc at [`HttpRestClientSugar.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/1.2.2/org/refcodes/rest/HttpRestClientSugar.html))
* \[<span style="color:green">MODIFIED</span>\] [`HttpRestServer.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.2.2/src/main/java/org/refcodes/rest/HttpRestServer.java) (see Javadoc at [`HttpRestServer.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/1.2.2/org/refcodes/rest/HttpRestServer.html))
* \[<span style="color:green">MODIFIED</span>\] [`HttpRestServerImpl.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.2.2/src/main/java/org/refcodes/rest/HttpRestServerImpl.java) (see Javadoc at [`HttpRestServerImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/1.2.2/org/refcodes/rest/HttpRestServerImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`HttpRestServerSugar.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.2.2/src/main/java/org/refcodes/rest/HttpRestServerSugar.java) (see Javadoc at [`HttpRestServerSugar.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/1.2.2/org/refcodes/rest/HttpRestServerSugar.html))
* \[<span style="color:green">MODIFIED</span>\] [`LoopbackRestServer.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.2.2/src/main/java/org/refcodes/rest/LoopbackRestServer.java) (see Javadoc at [`LoopbackRestServer.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/1.2.2/org/refcodes/rest/LoopbackRestServer.html))
* \[<span style="color:green">MODIFIED</span>\] [`LoopbackRestServerImpl.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.2.2/src/main/java/org/refcodes/rest/LoopbackRestServerImpl.java) (see Javadoc at [`LoopbackRestServerImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/1.2.2/org/refcodes/rest/LoopbackRestServerImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`RestCallerBuilder.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.2.2/src/main/java/org/refcodes/rest/RestCallerBuilder.java) (see Javadoc at [`RestCallerBuilder.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/1.2.2/org/refcodes/rest/RestCallerBuilder.html))
* \[<span style="color:green">MODIFIED</span>\] [`RestCallerBuilderImpl.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.2.2/src/main/java/org/refcodes/rest/RestCallerBuilderImpl.java) (see Javadoc at [`RestCallerBuilderImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/1.2.2/org/refcodes/rest/RestCallerBuilderImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`RestClient.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.2.2/src/main/java/org/refcodes/rest/RestClient.java) (see Javadoc at [`RestClient.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/1.2.2/org/refcodes/rest/RestClient.html))
* \[<span style="color:green">MODIFIED</span>\] [`RestRequest.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.2.2/src/main/java/org/refcodes/rest/RestRequest.java) (see Javadoc at [`RestRequest.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/1.2.2/org/refcodes/rest/RestRequest.html))
* \[<span style="color:green">MODIFIED</span>\] [`RestRequestBuilder.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.2.2/src/main/java/org/refcodes/rest/RestRequestBuilder.java) (see Javadoc at [`RestRequestBuilder.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/1.2.2/org/refcodes/rest/RestRequestBuilder.html))
* \[<span style="color:green">MODIFIED</span>\] [`RestRequestBuilderImpl.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.2.2/src/main/java/org/refcodes/rest/RestRequestBuilderImpl.java) (see Javadoc at [`RestRequestBuilderImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/1.2.2/org/refcodes/rest/RestRequestBuilderImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`RestRequestEventImpl.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.2.2/src/main/java/org/refcodes/rest/RestRequestEventImpl.java) (see Javadoc at [`RestRequestEventImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/1.2.2/org/refcodes/rest/RestRequestEventImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`RestServer.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.2.2/src/main/java/org/refcodes/rest/RestServer.java) (see Javadoc at [`RestServer.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/1.2.2/org/refcodes/rest/RestServer.html))
* \[<span style="color:green">MODIFIED</span>\] [`HttpRestServerTest.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.2.2/src/test/java/org/refcodes/rest/HttpRestServerTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`HttpsRestServerTest.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.2.2/src/test/java/org/refcodes/rest/HttpsRestServerTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`LoopbackRestServerTest.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.2.2/src/test/java/org/refcodes/rest/LoopbackRestServerTest.java) 

## Change list &lt;refcodes-logger-ext&gt; (version 1.2.2)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-1.2.2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-1.2.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-1.2.2/refcodes-logger-ext-slf4j/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-1.2.2/refcodes-logger-ext-slf4j/pom.xml) 

## Change list &lt;refcodes-rest-ext&gt; (version 1.2.2)

* \[<span style="color:blue">ADDED</span>\] [`AmazonMetaData.java`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-1.2.2/refcodes-rest-ext-eureka/src/main/java/org/refcodes/rest/ext/eureka/AmazonMetaData.java) (see Javadoc at [`AmazonMetaData.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest-ext-eureka/1.2.2/org/refcodes/rest/ext/eureka/AmazonMetaData.html))
* \[<span style="color:blue">ADDED</span>\] [`AmazonMetaDataAccessor.java`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-1.2.2/refcodes-rest-ext-eureka/src/main/java/org/refcodes/rest/ext/eureka/AmazonMetaDataAccessor.java) (see Javadoc at [`AmazonMetaDataAccessor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest-ext-eureka/1.2.2/org/refcodes/rest/ext/eureka/AmazonMetaDataAccessor.html))
* \[<span style="color:blue">ADDED</span>\] [`AmazonMetaDataImpl.java`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-1.2.2/refcodes-rest-ext-eureka/src/main/java/org/refcodes/rest/ext/eureka/AmazonMetaDataImpl.java) (see Javadoc at [`AmazonMetaDataImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest-ext-eureka/1.2.2/org/refcodes/rest/ext/eureka/AmazonMetaDataImpl.html))
* \[<span style="color:blue">ADDED</span>\] [`EurekaDataCenterType.java`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-1.2.2/refcodes-rest-ext-eureka/src/main/java/org/refcodes/rest/ext/eureka/EurekaDataCenterType.java) (see Javadoc at [`EurekaDataCenterType.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest-ext-eureka/1.2.2/org/refcodes/rest/ext/eureka/EurekaDataCenterType.html))
* \[<span style="color:blue">ADDED</span>\] [`EurekaDataCenterTypeAccessor.java`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-1.2.2/refcodes-rest-ext-eureka/src/main/java/org/refcodes/rest/ext/eureka/EurekaDataCenterTypeAccessor.java) (see Javadoc at [`EurekaDataCenterTypeAccessor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest-ext-eureka/1.2.2/org/refcodes/rest/ext/eureka/EurekaDataCenterTypeAccessor.html))
* \[<span style="color:blue">ADDED</span>\] [`EurekaInstanceDescriptor.java`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-1.2.2/refcodes-rest-ext-eureka/src/main/java/org/refcodes/rest/ext/eureka/EurekaInstanceDescriptor.java) (see Javadoc at [`EurekaInstanceDescriptor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest-ext-eureka/1.2.2/org/refcodes/rest/ext/eureka/EurekaInstanceDescriptor.html))
* \[<span style="color:blue">ADDED</span>\] [`EurekaInstanceDescriptorImpl.java`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-1.2.2/refcodes-rest-ext-eureka/src/main/java/org/refcodes/rest/ext/eureka/EurekaInstanceDescriptorImpl.java) (see Javadoc at [`EurekaInstanceDescriptorImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest-ext-eureka/1.2.2/org/refcodes/rest/ext/eureka/EurekaInstanceDescriptorImpl.html))
* \[<span style="color:blue">ADDED</span>\] [`EurekaLoopSleepTime.java`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-1.2.2/refcodes-rest-ext-eureka/src/main/java/org/refcodes/rest/ext/eureka/EurekaLoopSleepTime.java) (see Javadoc at [`EurekaLoopSleepTime.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest-ext-eureka/1.2.2/org/refcodes/rest/ext/eureka/EurekaLoopSleepTime.html))
* \[<span style="color:blue">ADDED</span>\] [`EurekaRestClient.java`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-1.2.2/refcodes-rest-ext-eureka/src/main/java/org/refcodes/rest/ext/eureka/EurekaRestClient.java) (see Javadoc at [`EurekaRestClient.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest-ext-eureka/1.2.2/org/refcodes/rest/ext/eureka/EurekaRestClient.html))
* \[<span style="color:blue">ADDED</span>\] [`EurekaRestClientDecorator.java`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-1.2.2/refcodes-rest-ext-eureka/src/main/java/org/refcodes/rest/ext/eureka/EurekaRestClientDecorator.java) (see Javadoc at [`EurekaRestClientDecorator.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest-ext-eureka/1.2.2/org/refcodes/rest/ext/eureka/EurekaRestClientDecorator.html))
* \[<span style="color:blue">ADDED</span>\] [`EurekaRestClientImpl.java`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-1.2.2/refcodes-rest-ext-eureka/src/main/java/org/refcodes/rest/ext/eureka/EurekaRestClientImpl.java) (see Javadoc at [`EurekaRestClientImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest-ext-eureka/1.2.2/org/refcodes/rest/ext/eureka/EurekaRestClientImpl.html))
* \[<span style="color:blue">ADDED</span>\] [`EurekaRestServer.java`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-1.2.2/refcodes-rest-ext-eureka/src/main/java/org/refcodes/rest/ext/eureka/EurekaRestServer.java) (see Javadoc at [`EurekaRestServer.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest-ext-eureka/1.2.2/org/refcodes/rest/ext/eureka/EurekaRestServer.html))
* \[<span style="color:blue">ADDED</span>\] [`EurekaRestServerDecorator.java`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-1.2.2/refcodes-rest-ext-eureka/src/main/java/org/refcodes/rest/ext/eureka/EurekaRestServerDecorator.java) (see Javadoc at [`EurekaRestServerDecorator.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest-ext-eureka/1.2.2/org/refcodes/rest/ext/eureka/EurekaRestServerDecorator.html))
* \[<span style="color:blue">ADDED</span>\] [`EurekaRestServerImpl.java`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-1.2.2/refcodes-rest-ext-eureka/src/main/java/org/refcodes/rest/ext/eureka/EurekaRestServerImpl.java) (see Javadoc at [`EurekaRestServerImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest-ext-eureka/1.2.2/org/refcodes/rest/ext/eureka/EurekaRestServerImpl.html))
* \[<span style="color:blue">ADDED</span>\] [`EurekaServerDescriptor.java`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-1.2.2/refcodes-rest-ext-eureka/src/main/java/org/refcodes/rest/ext/eureka/EurekaServerDescriptor.java) (see Javadoc at [`EurekaServerDescriptor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest-ext-eureka/1.2.2/org/refcodes/rest/ext/eureka/EurekaServerDescriptor.html))
* \[<span style="color:blue">ADDED</span>\] [`EurekaServerDescriptorImpl.java`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-1.2.2/refcodes-rest-ext-eureka/src/main/java/org/refcodes/rest/ext/eureka/EurekaServerDescriptorImpl.java) (see Javadoc at [`EurekaServerDescriptorImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest-ext-eureka/1.2.2/org/refcodes/rest/ext/eureka/EurekaServerDescriptorImpl.html))
* \[<span style="color:blue">ADDED</span>\] [`EurekaServiceStatus.java`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-1.2.2/refcodes-rest-ext-eureka/src/main/java/org/refcodes/rest/ext/eureka/EurekaServiceStatus.java) (see Javadoc at [`EurekaServiceStatus.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest-ext-eureka/1.2.2/org/refcodes/rest/ext/eureka/EurekaServiceStatus.html))
* \[<span style="color:blue">ADDED</span>\] [`EurekaServiceStatusAccessor.java`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-1.2.2/refcodes-rest-ext-eureka/src/main/java/org/refcodes/rest/ext/eureka/EurekaServiceStatusAccessor.java) (see Javadoc at [`EurekaServiceStatusAccessor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest-ext-eureka/1.2.2/org/refcodes/rest/ext/eureka/EurekaServiceStatusAccessor.html))
* \[<span style="color:blue">ADDED</span>\] [`EurekaRestClientTest.java`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-1.2.2/refcodes-rest-ext-eureka/src/test/java/org/refcodes/rest/ext/eureka/EurekaRestClientTest.java) 
* \[<span style="color:blue">ADDED</span>\] [`EurekaRestServerTest.java`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-1.2.2/refcodes-rest-ext-eureka/src/test/java/org/refcodes/rest/ext/eureka/EurekaRestServerTest.java) 
* \[<span style="color:blue">ADDED</span>\] [`EurekaServer.java`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-1.2.2/refcodes-rest-ext-eureka/src/test/java/org/refcodes/rest/ext/eureka/EurekaServer.java) 
* \[<span style="color:blue">ADDED</span>\] [`EurekaServerDescriptorTest.java`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-1.2.2/refcodes-rest-ext-eureka/src/test/java/org/refcodes/rest/ext/eureka/EurekaServerDescriptorTest.java) 
* \[<span style="color:blue">ADDED</span>\] [`application.properties`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-1.2.2/refcodes-rest-ext-eureka/src/test/resources/application.properties) 
* \[<span style="color:blue">ADDED</span>\] [`log4j.xml`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-1.2.2/refcodes-rest-ext-eureka/src/test/resources/log4j.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-1.2.2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-1.2.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-1.2.2/refcodes-rest-ext-eureka/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-1.2.2/refcodes-rest-ext-eureka/pom.xml) 

## Change list &lt;refcodes-forwardsecrecy&gt; (version 1.2.2)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy/src/refcodes-forwardsecrecy-1.2.2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy/src/refcodes-forwardsecrecy-1.2.2/pom.xml) 

## Change list &lt;refcodes-filesystem&gt; (version 1.2.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-filesystem/src/refcodes-filesystem-1.2.2/pom.xml) 

## Change list &lt;refcodes-forwardsecrecy-alt&gt; (version 1.2.2)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-1.2.2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-1.2.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-1.2.2/refcodes-forwardsecrecy-alt-filesystem/pom.xml) 

## Change list &lt;refcodes-eventbus&gt; (version 1.2.2)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-eventbus/src/refcodes-eventbus-1.2.2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-eventbus/src/refcodes-eventbus-1.2.2/pom.xml) 

## Change list &lt;refcodes-filesystem-alt&gt; (version 1.2.2)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-filesystem-alt/src/refcodes-filesystem-alt-1.2.2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-filesystem-alt/src/refcodes-filesystem-alt-1.2.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-filesystem-alt/src/refcodes-filesystem-alt-1.2.2/refcodes-filesystem-alt-s3/pom.xml) 

## Change list &lt;refcodes-io-ext&gt; (version 1.2.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-1.2.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-1.2.2/refcodes-io-ext-observable/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`ObservableBidirectionalRequestTransceiverImpl.java`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-1.2.2/refcodes-io-ext-observable/src/main/java/org/refcodes/connection/ext/observable/ObservableBidirectionalRequestTransceiverImpl.java) (see Javadoc at [`ObservableBidirectionalRequestTransceiverImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-io-ext-observable/1.2.2/org/refcodes/connection/ext/observable/ObservableBidirectionalRequestTransceiverImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`ObservableBidirectionalTransceiverImpl.java`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-1.2.2/refcodes-io-ext-observable/src/main/java/org/refcodes/connection/ext/observable/ObservableBidirectionalTransceiverImpl.java) (see Javadoc at [`ObservableBidirectionalTransceiverImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-io-ext-observable/1.2.2/org/refcodes/connection/ext/observable/ObservableBidirectionalTransceiverImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`ObservableConnectionReceiverImpl.java`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-1.2.2/refcodes-io-ext-observable/src/main/java/org/refcodes/connection/ext/observable/ObservableConnectionReceiverImpl.java) (see Javadoc at [`ObservableConnectionReceiverImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-io-ext-observable/1.2.2/org/refcodes/connection/ext/observable/ObservableConnectionReceiverImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`ObservableConnectionRequestReceiverImpl.java`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-1.2.2/refcodes-io-ext-observable/src/main/java/org/refcodes/connection/ext/observable/ObservableConnectionRequestReceiverImpl.java) (see Javadoc at [`ObservableConnectionRequestReceiverImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-io-ext-observable/1.2.2/org/refcodes/connection/ext/observable/ObservableConnectionRequestReceiverImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`ObservableConnectionRequestSenderImpl.java`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-1.2.2/refcodes-io-ext-observable/src/main/java/org/refcodes/connection/ext/observable/ObservableConnectionRequestSenderImpl.java) (see Javadoc at [`ObservableConnectionRequestSenderImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-io-ext-observable/1.2.2/org/refcodes/connection/ext/observable/ObservableConnectionRequestSenderImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`ObservableConnectionRequestTransceiverImpl.java`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-1.2.2/refcodes-io-ext-observable/src/main/java/org/refcodes/connection/ext/observable/ObservableConnectionRequestTransceiverImpl.java) (see Javadoc at [`ObservableConnectionRequestTransceiverImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-io-ext-observable/1.2.2/org/refcodes/connection/ext/observable/ObservableConnectionRequestTransceiverImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`ObservableConnectionSenderImpl.java`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-1.2.2/refcodes-io-ext-observable/src/main/java/org/refcodes/connection/ext/observable/ObservableConnectionSenderImpl.java) (see Javadoc at [`ObservableConnectionSenderImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-io-ext-observable/1.2.2/org/refcodes/connection/ext/observable/ObservableConnectionSenderImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`ObservableConnectionTransceiverImpl.java`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-1.2.2/refcodes-io-ext-observable/src/main/java/org/refcodes/connection/ext/observable/ObservableConnectionTransceiverImpl.java) (see Javadoc at [`ObservableConnectionTransceiverImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-io-ext-observable/1.2.2/org/refcodes/connection/ext/observable/ObservableConnectionTransceiverImpl.html))

## Change list &lt;refcodes-jobbus&gt; (version 1.2.2)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-jobbus/src/refcodes-jobbus-1.2.2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-jobbus/src/refcodes-jobbus-1.2.2/pom.xml) 

## Change list &lt;refcodes-daemon&gt; (version 1.2.2)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-daemon/src/refcodes-daemon-1.2.2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-daemon/src/refcodes-daemon-1.2.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`AbstractDaemon.java`](https://bitbucket.org/refcodes/refcodes-daemon/src/refcodes-daemon-1.2.2/src/main/java/org/refcodes/daemon/AbstractDaemon.java) (see Javadoc at [`AbstractDaemon.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-daemon/1.2.2/org/refcodes/daemon/AbstractDaemon.html))

## Change list &lt;refcodes-remoting&gt; (version 1.2.2)

* \[<span style="color:red">DELETED</span>\] `InstanceIdAccessor.java`
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-remoting/src/refcodes-remoting-1.2.2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-remoting/src/refcodes-remoting-1.2.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`InstanceId.java`](https://bitbucket.org/refcodes/refcodes-remoting/src/refcodes-remoting-1.2.2/src/main/java/org/refcodes/remoting/InstanceId.java) (see Javadoc at [`InstanceId.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-remoting/1.2.2/org/refcodes/remoting/InstanceId.html))
* \[<span style="color:green">MODIFIED</span>\] [`RemoteClientImpl.java`](https://bitbucket.org/refcodes/refcodes-remoting/src/refcodes-remoting-1.2.2/src/main/java/org/refcodes/remoting/RemoteClientImpl.java) (see Javadoc at [`RemoteClientImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-remoting/1.2.2/org/refcodes/remoting/RemoteClientImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`RemoteServerImpl.java`](https://bitbucket.org/refcodes/refcodes-remoting/src/refcodes-remoting-1.2.2/src/main/java/org/refcodes/remoting/RemoteServerImpl.java) (see Javadoc at [`RemoteServerImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-remoting/1.2.2/org/refcodes/remoting/RemoteServerImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`UnknownInstanceIdRuntimeException.java`](https://bitbucket.org/refcodes/refcodes-remoting/src/refcodes-remoting-1.2.2/src/main/java/org/refcodes/remoting/UnknownInstanceIdRuntimeException.java) (see Javadoc at [`UnknownInstanceIdRuntimeException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-remoting/1.2.2/org/refcodes/remoting/UnknownInstanceIdRuntimeException.html))

## Change list &lt;refcodes-remoting-ext&gt; (version 1.2.2)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-1.2.2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-1.2.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-1.2.2/refcodes-remoting-ext-observer/pom.xml) 

## Change list &lt;refcodes-security-alt&gt; (version 1.2.2)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-1.2.2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-1.2.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-1.2.2/refcodes-security-alt-chaos/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-1.2.2/refcodes-security-alt-chaos/pom.xml) 

## Change list &lt;refcodes-security-ext&gt; (version 1.2.2)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-1.2.2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-1.2.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-1.2.2/refcodes-security-ext-chaos/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-1.2.2/refcodes-security-ext-chaos/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-1.2.2/refcodes-security-ext-spring/pom.xml) 

## Change list &lt;refcodes-servicebus&gt; (version 1.2.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-servicebus/src/refcodes-servicebus-1.2.2/pom.xml) 

## Change list &lt;refcodes-servicebus-alt&gt; (version 1.2.2)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-servicebus-alt/src/refcodes-servicebus-alt-1.2.2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-servicebus-alt/src/refcodes-servicebus-alt-1.2.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-servicebus-alt/src/refcodes-servicebus-alt-1.2.2/refcodes-servicebus-alt-spring/pom.xml) 

## Change list &lt;refcodes-tabular-alt&gt; (version 1.2.2)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-tabular-alt/src/refcodes-tabular-alt-1.2.2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-tabular-alt/src/refcodes-tabular-alt-1.2.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-tabular-alt/src/refcodes-tabular-alt-1.2.2/refcodes-tabular-alt-forwardsecrecy/pom.xml) 

## Change list &lt;refcodes-boulderdash&gt; (version 1.2.2)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-boulderdash/src/refcodes-boulderdash-1.2.2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-boulderdash/src/refcodes-boulderdash-1.2.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`BoulderDashBoardImpl.java`](https://bitbucket.org/refcodes/refcodes-boulderdash/src/refcodes-boulderdash-1.2.2/src/main/java/org/refcodes/boulderdash/BoulderDashBoardImpl.java) (see Javadoc at [`BoulderDashBoardImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-boulderdash/1.2.2/org/refcodes/boulderdash/BoulderDashBoardImpl.html))
