> This change list has been auto-generated on `triton` by `steiner` with `changelist-all.sh` on the 2022-06-24 at 21:59:17.

## Change list &lt;refcodes-licensing&gt; (version 3.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-licensing/src/refcodes-licensing-3.0.4/pom.xml) 

## Change list &lt;refcodes-parent&gt; (version 3.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-parent/src/refcodes-parent-3.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-parent/src/refcodes-parent-3.0.4/README.md) 

## Change list &lt;refcodes-mixin&gt; (version 3.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-mixin/src/refcodes-mixin-3.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-mixin/src/refcodes-mixin-3.0.4/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`Schema.java`](https://bitbucket.org/refcodes/refcodes-mixin/src/refcodes-mixin-3.0.4/src/main/java/org/refcodes/mixin/Schema.java) (see Javadoc at [`Schema.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-mixin/3.0.4/org.refcodes.mixin/org/refcodes/mixin/Schema.html))

## Change list &lt;refcodes-data&gt; (version 3.0.4)

* \[<span style="color:blue">ADDED</span>\] [`RecoverySleepTime.java`](https://bitbucket.org/refcodes/refcodes-data/src/refcodes-data-3.0.4/src/main/java/org/refcodes/data/RecoverySleepTime.java) (see Javadoc at [`RecoverySleepTime.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-data/3.0.4/org.refcodes.data/org/refcodes/data/RecoverySleepTime.html))
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data/src/refcodes-data-3.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data/src/refcodes-data-3.0.4/README.md) 

## Change list &lt;refcodes-exception&gt; (version 3.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-exception/src/refcodes-exception-3.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-exception/src/refcodes-exception-3.0.4/README.md) 

## Change list &lt;refcodes-factory&gt; (version 3.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-factory/src/refcodes-factory-3.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-factory/src/refcodes-factory-3.0.4/README.md) 

## Change list &lt;refcodes-factory-alt&gt; (version 3.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-factory-alt/src/refcodes-factory-alt-3.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-factory-alt/src/refcodes-factory-alt-3.0.4/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-factory-alt/src/refcodes-factory-alt-3.0.4/refcodes-factory-alt-spring/pom.xml) 

## Change list &lt;refcodes-controlflow&gt; (version 3.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-controlflow/src/refcodes-controlflow-3.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-controlflow/src/refcodes-controlflow-3.0.4/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`RetryCounter.java`](https://bitbucket.org/refcodes/refcodes-controlflow/src/refcodes-controlflow-3.0.4/src/main/java/org/refcodes/controlflow/RetryCounter.java) (see Javadoc at [`RetryCounter.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-controlflow/3.0.4/org.refcodes.controlflow/org/refcodes/controlflow/RetryCounter.html))
* \[<span style="color:green">MODIFIED</span>\] [`RetryCounterTest.java`](https://bitbucket.org/refcodes/refcodes-controlflow/src/refcodes-controlflow-3.0.4/src/test/java/org/refcodes/controlflow/RetryCounterTest.java) 

## Change list &lt;refcodes-numerical&gt; (version 3.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-numerical/src/refcodes-numerical-3.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-numerical/src/refcodes-numerical-3.0.4/README.md) 

## Change list &lt;refcodes-generator&gt; (version 3.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-generator/src/refcodes-generator-3.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-generator/src/refcodes-generator-3.0.4/README.md) 

## Change list &lt;refcodes-matcher&gt; (version 3.0.4)

* \[<span style="color:blue">ADDED</span>\] [`package-info.java`](https://bitbucket.org/refcodes/refcodes-matcher/src/refcodes-matcher-3.0.4/src/main/java/org/refcodes/matcher/package-info.java) 
* \[<span style="color:blue">ADDED</span>\] [`MatcherExamples.java`](https://bitbucket.org/refcodes/refcodes-matcher/src/refcodes-matcher-3.0.4/src/test/java/org/refcodes/matcher/MatcherExamples.java) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-matcher/src/refcodes-matcher-3.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-matcher/src/refcodes-matcher-3.0.4/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`AbstractMatcheeMatcher.java`](https://bitbucket.org/refcodes/refcodes-matcher/src/refcodes-matcher-3.0.4/src/main/java/org/refcodes/matcher/AbstractMatcheeMatcher.java) (see Javadoc at [`AbstractMatcheeMatcher.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-matcher/3.0.4/org.refcodes.matcher/org/refcodes/matcher/AbstractMatcheeMatcher.html))
* \[<span style="color:green">MODIFIED</span>\] [`AbstractMatcherComposite.java`](https://bitbucket.org/refcodes/refcodes-matcher/src/refcodes-matcher-3.0.4/src/main/java/org/refcodes/matcher/AbstractMatcherComposite.java) (see Javadoc at [`AbstractMatcherComposite.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-matcher/3.0.4/org.refcodes.matcher/org/refcodes/matcher/AbstractMatcherComposite.html))
* \[<span style="color:green">MODIFIED</span>\] [`AbstractMatcher.java`](https://bitbucket.org/refcodes/refcodes-matcher/src/refcodes-matcher-3.0.4/src/main/java/org/refcodes/matcher/AbstractMatcher.java) (see Javadoc at [`AbstractMatcher.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-matcher/3.0.4/org.refcodes.matcher/org/refcodes/matcher/AbstractMatcher.html))
* \[<span style="color:green">MODIFIED</span>\] [`AbstractWildcardMatcher.java`](https://bitbucket.org/refcodes/refcodes-matcher/src/refcodes-matcher-3.0.4/src/main/java/org/refcodes/matcher/AbstractWildcardMatcher.java) (see Javadoc at [`AbstractWildcardMatcher.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-matcher/3.0.4/org.refcodes.matcher/org/refcodes/matcher/AbstractWildcardMatcher.html))
* \[<span style="color:green">MODIFIED</span>\] [`AndMatcher.java`](https://bitbucket.org/refcodes/refcodes-matcher/src/refcodes-matcher-3.0.4/src/main/java/org/refcodes/matcher/AndMatcher.java) (see Javadoc at [`AndMatcher.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-matcher/3.0.4/org.refcodes.matcher/org/refcodes/matcher/AndMatcher.html))
* \[<span style="color:green">MODIFIED</span>\] [`AnyMatcher.java`](https://bitbucket.org/refcodes/refcodes-matcher/src/refcodes-matcher-3.0.4/src/main/java/org/refcodes/matcher/AnyMatcher.java) (see Javadoc at [`AnyMatcher.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-matcher/3.0.4/org.refcodes.matcher/org/refcodes/matcher/AnyMatcher.html))
* \[<span style="color:green">MODIFIED</span>\] [`EqualWithMatcher.java`](https://bitbucket.org/refcodes/refcodes-matcher/src/refcodes-matcher-3.0.4/src/main/java/org/refcodes/matcher/EqualWithMatcher.java) (see Javadoc at [`EqualWithMatcher.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-matcher/3.0.4/org.refcodes.matcher/org/refcodes/matcher/EqualWithMatcher.html))
* \[<span style="color:green">MODIFIED</span>\] [`GreaterOrEqualThanMatcher.java`](https://bitbucket.org/refcodes/refcodes-matcher/src/refcodes-matcher-3.0.4/src/main/java/org/refcodes/matcher/GreaterOrEqualThanMatcher.java) (see Javadoc at [`GreaterOrEqualThanMatcher.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-matcher/3.0.4/org.refcodes.matcher/org/refcodes/matcher/GreaterOrEqualThanMatcher.html))
* \[<span style="color:green">MODIFIED</span>\] [`GreaterThanMatcher.java`](https://bitbucket.org/refcodes/refcodes-matcher/src/refcodes-matcher-3.0.4/src/main/java/org/refcodes/matcher/GreaterThanMatcher.java) (see Javadoc at [`GreaterThanMatcher.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-matcher/3.0.4/org.refcodes.matcher/org/refcodes/matcher/GreaterThanMatcher.html))
* \[<span style="color:green">MODIFIED</span>\] [`IsAssignableFromMatcher.java`](https://bitbucket.org/refcodes/refcodes-matcher/src/refcodes-matcher-3.0.4/src/main/java/org/refcodes/matcher/IsAssignableFromMatcher.java) (see Javadoc at [`IsAssignableFromMatcher.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-matcher/3.0.4/org.refcodes.matcher/org/refcodes/matcher/IsAssignableFromMatcher.html))
* \[<span style="color:green">MODIFIED</span>\] [`LessOrEqualThanMatcher.java`](https://bitbucket.org/refcodes/refcodes-matcher/src/refcodes-matcher-3.0.4/src/main/java/org/refcodes/matcher/LessOrEqualThanMatcher.java) (see Javadoc at [`LessOrEqualThanMatcher.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-matcher/3.0.4/org.refcodes.matcher/org/refcodes/matcher/LessOrEqualThanMatcher.html))
* \[<span style="color:green">MODIFIED</span>\] [`LessThanMatcher.java`](https://bitbucket.org/refcodes/refcodes-matcher/src/refcodes-matcher-3.0.4/src/main/java/org/refcodes/matcher/LessThanMatcher.java) (see Javadoc at [`LessThanMatcher.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-matcher/3.0.4/org.refcodes.matcher/org/refcodes/matcher/LessThanMatcher.html))
* \[<span style="color:green">MODIFIED</span>\] [`Matchable.java`](https://bitbucket.org/refcodes/refcodes-matcher/src/refcodes-matcher-3.0.4/src/main/java/org/refcodes/matcher/Matchable.java) (see Javadoc at [`Matchable.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-matcher/3.0.4/org.refcodes.matcher/org/refcodes/matcher/Matchable.html))
* \[<span style="color:green">MODIFIED</span>\] [`Matcher.java`](https://bitbucket.org/refcodes/refcodes-matcher/src/refcodes-matcher-3.0.4/src/main/java/org/refcodes/matcher/Matcher.java) (see Javadoc at [`Matcher.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-matcher/3.0.4/org.refcodes.matcher/org/refcodes/matcher/Matcher.html))
* \[<span style="color:green">MODIFIED</span>\] [`MatcherSchema.java`](https://bitbucket.org/refcodes/refcodes-matcher/src/refcodes-matcher-3.0.4/src/main/java/org/refcodes/matcher/MatcherSchema.java) (see Javadoc at [`MatcherSchema.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-matcher/3.0.4/org.refcodes.matcher/org/refcodes/matcher/MatcherSchema.html))
* \[<span style="color:green">MODIFIED</span>\] [`NoneMatcher.java`](https://bitbucket.org/refcodes/refcodes-matcher/src/refcodes-matcher-3.0.4/src/main/java/org/refcodes/matcher/NoneMatcher.java) (see Javadoc at [`NoneMatcher.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-matcher/3.0.4/org.refcodes.matcher/org/refcodes/matcher/NoneMatcher.html))
* \[<span style="color:green">MODIFIED</span>\] [`NotEqualWithMatcher.java`](https://bitbucket.org/refcodes/refcodes-matcher/src/refcodes-matcher-3.0.4/src/main/java/org/refcodes/matcher/NotEqualWithMatcher.java) (see Javadoc at [`NotEqualWithMatcher.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-matcher/3.0.4/org.refcodes.matcher/org/refcodes/matcher/NotEqualWithMatcher.html))
* \[<span style="color:green">MODIFIED</span>\] [`NotMatcher.java`](https://bitbucket.org/refcodes/refcodes-matcher/src/refcodes-matcher-3.0.4/src/main/java/org/refcodes/matcher/NotMatcher.java) (see Javadoc at [`NotMatcher.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-matcher/3.0.4/org.refcodes.matcher/org/refcodes/matcher/NotMatcher.html))
* \[<span style="color:green">MODIFIED</span>\] [`OrMatcher.java`](https://bitbucket.org/refcodes/refcodes-matcher/src/refcodes-matcher-3.0.4/src/main/java/org/refcodes/matcher/OrMatcher.java) (see Javadoc at [`OrMatcher.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-matcher/3.0.4/org.refcodes.matcher/org/refcodes/matcher/OrMatcher.html))

## Change list &lt;refcodes-struct&gt; (version 3.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-struct/src/refcodes-struct-3.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-struct/src/refcodes-struct-3.0.4/README.md) 

## Change list &lt;refcodes-struct-ext&gt; (version 3.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-struct-ext/src/refcodes-struct-ext-3.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-struct-ext/src/refcodes-struct-ext-3.0.4/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-struct-ext/src/refcodes-struct-ext-3.0.4/refcodes-struct-ext-factory/pom.xml) 

## Change list &lt;refcodes-runtime&gt; (version 3.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-3.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-3.0.4/README.md) 

## Change list &lt;refcodes-component&gt; (version 3.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.0.4/README.md) 

## Change list &lt;refcodes-data-ext&gt; (version 3.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-3.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-3.0.4/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-3.0.4/refcodes-data-ext-checkers/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-3.0.4/refcodes-data-ext-checkers/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-3.0.4/refcodes-data-ext-chess/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-3.0.4/refcodes-data-ext-chess/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-3.0.4/refcodes-data-ext-corporate/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-3.0.4/refcodes-data-ext-corporate/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-3.0.4/refcodes-data-ext-symbols/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-3.0.4/refcodes-data-ext-symbols/README.md) 

## Change list &lt;refcodes-graphical&gt; (version 3.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-graphical/src/refcodes-graphical-3.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-graphical/src/refcodes-graphical-3.0.4/README.md) 

## Change list &lt;refcodes-textual&gt; (version 3.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-3.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-3.0.4/README.md) 

## Change list &lt;refcodes-criteria&gt; (version 3.0.4)

* \[<span style="color:blue">ADDED</span>\] [`CriteriaSchema.java`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-3.0.4/src/main/java/org/refcodes/criteria/CriteriaSchema.java) (see Javadoc at [`CriteriaSchema.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-criteria/3.0.4/org.refcodes.criteria/org/refcodes/criteria/CriteriaSchema.html))
* \[<span style="color:blue">ADDED</span>\] [`CriteriaExamples.java`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-3.0.4/src/test/java/org/refcodes/criteria/CriteriaExamples.java) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-3.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-3.0.4/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`AbstractCriteria.java`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-3.0.4/src/main/java/org/refcodes/criteria/AbstractCriteria.java) (see Javadoc at [`AbstractCriteria.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-criteria/3.0.4/org.refcodes.criteria/org/refcodes/criteria/AbstractCriteria.html))
* \[<span style="color:green">MODIFIED</span>\] [`AbstractCriteriaLeaf.java`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-3.0.4/src/main/java/org/refcodes/criteria/AbstractCriteriaLeaf.java) (see Javadoc at [`AbstractCriteriaLeaf.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-criteria/3.0.4/org.refcodes.criteria/org/refcodes/criteria/AbstractCriteriaLeaf.html))
* \[<span style="color:green">MODIFIED</span>\] [`AbstractCriteriaNode.java`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-3.0.4/src/main/java/org/refcodes/criteria/AbstractCriteriaNode.java) (see Javadoc at [`AbstractCriteriaNode.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-criteria/3.0.4/org.refcodes.criteria/org/refcodes/criteria/AbstractCriteriaNode.html))
* \[<span style="color:green">MODIFIED</span>\] [`AbstractSingleCriteriaNode.java`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-3.0.4/src/main/java/org/refcodes/criteria/AbstractSingleCriteriaNode.java) (see Javadoc at [`AbstractSingleCriteriaNode.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-criteria/3.0.4/org.refcodes.criteria/org/refcodes/criteria/AbstractSingleCriteriaNode.html))
* \[<span style="color:green">MODIFIED</span>\] [`AndCriteria.java`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-3.0.4/src/main/java/org/refcodes/criteria/AndCriteria.java) (see Javadoc at [`AndCriteria.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-criteria/3.0.4/org.refcodes.criteria/org/refcodes/criteria/AndCriteria.html))
* \[<span style="color:green">MODIFIED</span>\] [`Criteria.java`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-3.0.4/src/main/java/org/refcodes/criteria/Criteria.java) (see Javadoc at [`Criteria.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-criteria/3.0.4/org.refcodes.criteria/org/refcodes/criteria/Criteria.html))
* \[<span style="color:green">MODIFIED</span>\] [`CriteriaSugar.java`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-3.0.4/src/main/java/org/refcodes/criteria/CriteriaSugar.java) (see Javadoc at [`CriteriaSugar.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-criteria/3.0.4/org.refcodes.criteria/org/refcodes/criteria/CriteriaSugar.html))
* \[<span style="color:green">MODIFIED</span>\] [`EqualWithCriteria.java`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-3.0.4/src/main/java/org/refcodes/criteria/EqualWithCriteria.java) (see Javadoc at [`EqualWithCriteria.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-criteria/3.0.4/org.refcodes.criteria/org/refcodes/criteria/EqualWithCriteria.html))
* \[<span style="color:green">MODIFIED</span>\] [`GreaterOrEqualThanCriteria.java`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-3.0.4/src/main/java/org/refcodes/criteria/GreaterOrEqualThanCriteria.java) (see Javadoc at [`GreaterOrEqualThanCriteria.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-criteria/3.0.4/org.refcodes.criteria/org/refcodes/criteria/GreaterOrEqualThanCriteria.html))
* \[<span style="color:green">MODIFIED</span>\] [`GreaterThanCriteria.java`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-3.0.4/src/main/java/org/refcodes/criteria/GreaterThanCriteria.java) (see Javadoc at [`GreaterThanCriteria.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-criteria/3.0.4/org.refcodes.criteria/org/refcodes/criteria/GreaterThanCriteria.html))
* \[<span style="color:green">MODIFIED</span>\] [`IntersectWithCriteria.java`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-3.0.4/src/main/java/org/refcodes/criteria/IntersectWithCriteria.java) (see Javadoc at [`IntersectWithCriteria.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-criteria/3.0.4/org.refcodes.criteria/org/refcodes/criteria/IntersectWithCriteria.html))
* \[<span style="color:green">MODIFIED</span>\] [`LessOrEqualThanCriteria.java`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-3.0.4/src/main/java/org/refcodes/criteria/LessOrEqualThanCriteria.java) (see Javadoc at [`LessOrEqualThanCriteria.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-criteria/3.0.4/org.refcodes.criteria/org/refcodes/criteria/LessOrEqualThanCriteria.html))
* \[<span style="color:green">MODIFIED</span>\] [`LessThanCriteria.java`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-3.0.4/src/main/java/org/refcodes/criteria/LessThanCriteria.java) (see Javadoc at [`LessThanCriteria.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-criteria/3.0.4/org.refcodes.criteria/org/refcodes/criteria/LessThanCriteria.html))
* \[<span style="color:green">MODIFIED</span>\] [`NotCriteria.java`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-3.0.4/src/main/java/org/refcodes/criteria/NotCriteria.java) (see Javadoc at [`NotCriteria.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-criteria/3.0.4/org.refcodes.criteria/org/refcodes/criteria/NotCriteria.html))
* \[<span style="color:green">MODIFIED</span>\] [`NotEqualWithCriteria.java`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-3.0.4/src/main/java/org/refcodes/criteria/NotEqualWithCriteria.java) (see Javadoc at [`NotEqualWithCriteria.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-criteria/3.0.4/org.refcodes.criteria/org/refcodes/criteria/NotEqualWithCriteria.html))
* \[<span style="color:green">MODIFIED</span>\] [`OrCriteria.java`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-3.0.4/src/main/java/org/refcodes/criteria/OrCriteria.java) (see Javadoc at [`OrCriteria.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-criteria/3.0.4/org.refcodes.criteria/org/refcodes/criteria/OrCriteria.html))
* \[<span style="color:green">MODIFIED</span>\] [`package-info.java`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-3.0.4/src/main/java/org/refcodes/criteria/package-info.java) 
* \[<span style="color:green">MODIFIED</span>\] [`ExpressionQueryFactoryTest.java`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-3.0.4/src/test/java/org/refcodes/criteria/ExpressionQueryFactoryTest.java) 

## Change list &lt;refcodes-io&gt; (version 3.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-io/src/refcodes-io-3.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-io/src/refcodes-io-3.0.4/README.md) 

## Change list &lt;refcodes-tabular&gt; (version 3.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-tabular/src/refcodes-tabular-3.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-tabular/src/refcodes-tabular-3.0.4/README.md) 

## Change list &lt;refcodes-observer&gt; (version 3.0.4)

* \[<span style="color:blue">ADDED</span>\] [`AbstractEventMatcher.java`](https://bitbucket.org/refcodes/refcodes-observer/src/refcodes-observer-3.0.4/src/main/java/org/refcodes/observer/AbstractEventMatcher.java) (see Javadoc at [`AbstractEventMatcher.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-observer/3.0.4/org.refcodes.observer/org/refcodes/observer/AbstractEventMatcher.html))
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-observer/src/refcodes-observer-3.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-observer/src/refcodes-observer-3.0.4/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`AbstractObservable.java`](https://bitbucket.org/refcodes/refcodes-observer/src/refcodes-observer-3.0.4/src/main/java/org/refcodes/observer/AbstractObservable.java) (see Javadoc at [`AbstractObservable.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-observer/3.0.4/org.refcodes.observer/org/refcodes/observer/AbstractObservable.html))
* \[<span style="color:green">MODIFIED</span>\] [`ActionEqualWithEventMatcher.java`](https://bitbucket.org/refcodes/refcodes-observer/src/refcodes-observer-3.0.4/src/main/java/org/refcodes/observer/ActionEqualWithEventMatcher.java) (see Javadoc at [`ActionEqualWithEventMatcher.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-observer/3.0.4/org.refcodes.observer/org/refcodes/observer/ActionEqualWithEventMatcher.html))
* \[<span style="color:green">MODIFIED</span>\] [`AliasEqualWithEventMatcher.java`](https://bitbucket.org/refcodes/refcodes-observer/src/refcodes-observer-3.0.4/src/main/java/org/refcodes/observer/AliasEqualWithEventMatcher.java) (see Javadoc at [`AliasEqualWithEventMatcher.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-observer/3.0.4/org.refcodes.observer/org/refcodes/observer/AliasEqualWithEventMatcher.html))
* \[<span style="color:green">MODIFIED</span>\] [`CatchAllEventMatcher.java`](https://bitbucket.org/refcodes/refcodes-observer/src/refcodes-observer-3.0.4/src/main/java/org/refcodes/observer/CatchAllEventMatcher.java) (see Javadoc at [`CatchAllEventMatcher.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-observer/3.0.4/org.refcodes.observer/org/refcodes/observer/CatchAllEventMatcher.html))
* \[<span style="color:green">MODIFIED</span>\] [`CatchNoneEventMatcher.java`](https://bitbucket.org/refcodes/refcodes-observer/src/refcodes-observer-3.0.4/src/main/java/org/refcodes/observer/CatchNoneEventMatcher.java) (see Javadoc at [`CatchNoneEventMatcher.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-observer/3.0.4/org.refcodes.observer/org/refcodes/observer/CatchNoneEventMatcher.html))
* \[<span style="color:green">MODIFIED</span>\] [`ChannelEqualWithEventMatcher.java`](https://bitbucket.org/refcodes/refcodes-observer/src/refcodes-observer-3.0.4/src/main/java/org/refcodes/observer/ChannelEqualWithEventMatcher.java) (see Javadoc at [`ChannelEqualWithEventMatcher.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-observer/3.0.4/org.refcodes.observer/org/refcodes/observer/ChannelEqualWithEventMatcher.html))
* \[<span style="color:green">MODIFIED</span>\] [`EventMatcherSugar.java`](https://bitbucket.org/refcodes/refcodes-observer/src/refcodes-observer-3.0.4/src/main/java/org/refcodes/observer/EventMatcherSugar.java) (see Javadoc at [`EventMatcherSugar.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-observer/3.0.4/org.refcodes.observer/org/refcodes/observer/EventMatcherSugar.html))
* \[<span style="color:green">MODIFIED</span>\] [`GenericActionPayloadMetaDataEvent.java`](https://bitbucket.org/refcodes/refcodes-observer/src/refcodes-observer-3.0.4/src/main/java/org/refcodes/observer/GenericActionPayloadMetaDataEvent.java) (see Javadoc at [`GenericActionPayloadMetaDataEvent.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-observer/3.0.4/org.refcodes.observer/org/refcodes/observer/GenericActionPayloadMetaDataEvent.html))
* \[<span style="color:green">MODIFIED</span>\] [`GroupEqualWithEventMatcher.java`](https://bitbucket.org/refcodes/refcodes-observer/src/refcodes-observer-3.0.4/src/main/java/org/refcodes/observer/GroupEqualWithEventMatcher.java) (see Javadoc at [`GroupEqualWithEventMatcher.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-observer/3.0.4/org.refcodes.observer/org/refcodes/observer/GroupEqualWithEventMatcher.html))
* \[<span style="color:green">MODIFIED</span>\] [`MetaDataActionEventImpl.java`](https://bitbucket.org/refcodes/refcodes-observer/src/refcodes-observer-3.0.4/src/main/java/org/refcodes/observer/MetaDataActionEventImpl.java) (see Javadoc at [`MetaDataActionEventImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-observer/3.0.4/org.refcodes.observer/org/refcodes/observer/MetaDataActionEventImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`Observers.java`](https://bitbucket.org/refcodes/refcodes-observer/src/refcodes-observer-3.0.4/src/main/java/org/refcodes/observer/Observers.java) (see Javadoc at [`Observers.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-observer/3.0.4/org.refcodes.observer/org/refcodes/observer/Observers.html))
* \[<span style="color:green">MODIFIED</span>\] [`PayloadEventImpl.java`](https://bitbucket.org/refcodes/refcodes-observer/src/refcodes-observer-3.0.4/src/main/java/org/refcodes/observer/PayloadEventImpl.java) (see Javadoc at [`PayloadEventImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-observer/3.0.4/org.refcodes.observer/org/refcodes/observer/PayloadEventImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`PayloadMetaDataActionEventBuilderImpl.java`](https://bitbucket.org/refcodes/refcodes-observer/src/refcodes-observer-3.0.4/src/main/java/org/refcodes/observer/PayloadMetaDataActionEventBuilderImpl.java) (see Javadoc at [`PayloadMetaDataActionEventBuilderImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-observer/3.0.4/org.refcodes.observer/org/refcodes/observer/PayloadMetaDataActionEventBuilderImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`PayloadMetaDataEventImpl.java`](https://bitbucket.org/refcodes/refcodes-observer/src/refcodes-observer-3.0.4/src/main/java/org/refcodes/observer/PayloadMetaDataEventImpl.java) (see Javadoc at [`PayloadMetaDataEventImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-observer/3.0.4/org.refcodes.observer/org/refcodes/observer/PayloadMetaDataEventImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`PublisherIsAssignableFromMatcher.java`](https://bitbucket.org/refcodes/refcodes-observer/src/refcodes-observer-3.0.4/src/main/java/org/refcodes/observer/PublisherIsAssignableFromMatcher.java) (see Javadoc at [`PublisherIsAssignableFromMatcher.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-observer/3.0.4/org.refcodes.observer/org/refcodes/observer/PublisherIsAssignableFromMatcher.html))
* \[<span style="color:green">MODIFIED</span>\] [`UniversalIdEqualWithEventMatcher.java`](https://bitbucket.org/refcodes/refcodes-observer/src/refcodes-observer-3.0.4/src/main/java/org/refcodes/observer/UniversalIdEqualWithEventMatcher.java) (see Javadoc at [`UniversalIdEqualWithEventMatcher.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-observer/3.0.4/org.refcodes.observer/org/refcodes/observer/UniversalIdEqualWithEventMatcher.html))

## Change list &lt;refcodes-command&gt; (version 3.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-command/src/refcodes-command-3.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-command/src/refcodes-command-3.0.4/README.md) 

## Change list &lt;refcodes-cli&gt; (version 3.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.0.4/README.md) 

## Change list &lt;refcodes-codec&gt; (version 3.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-3.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-3.0.4/README.md) 

## Change list &lt;refcodes-component-ext&gt; (version 3.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-3.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-3.0.4/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-3.0.4/refcodes-component-ext-observer/pom.xml) 

## Change list &lt;refcodes-properties&gt; (version 3.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-3.0.4/pom.xml) 

## Change list &lt;refcodes-security&gt; (version 3.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security/src/refcodes-security-3.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security/src/refcodes-security-3.0.4/README.md) 

## Change list &lt;refcodes-security-alt&gt; (version 3.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-3.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-3.0.4/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-3.0.4/refcodes-security-alt-chaos/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-3.0.4/refcodes-security-alt-chaos/README.md) 

## Change list &lt;refcodes-security-ext&gt; (version 3.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-3.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-3.0.4/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-3.0.4/refcodes-security-ext-chaos/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-3.0.4/refcodes-security-ext-chaos/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-3.0.4/refcodes-security-ext-spring/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-3.0.4/refcodes-security-ext-spring/README.md) 

## Change list &lt;refcodes-properties-ext&gt; (version 3.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.0.4/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.0.4/refcodes-properties-ext-cli/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.0.4/refcodes-properties-ext-cli/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.0.4/refcodes-properties-ext-obfuscation/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.0.4/refcodes-properties-ext-obfuscation/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.0.4/refcodes-properties-ext-observer/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.0.4/refcodes-properties-ext-observer/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.0.4/refcodes-properties-ext-runtime/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.0.4/refcodes-properties-ext-runtime/README.md) 

## Change list &lt;refcodes-logger&gt; (version 3.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger/src/refcodes-logger-3.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger/src/refcodes-logger-3.0.4/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`AbstractLoggerComposite.java`](https://bitbucket.org/refcodes/refcodes-logger/src/refcodes-logger-3.0.4/src/main/java/org/refcodes/logger/AbstractLoggerComposite.java) (see Javadoc at [`AbstractLoggerComposite.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-logger/3.0.4/org.refcodes.logger/org/refcodes/logger/AbstractLoggerComposite.html))
* \[<span style="color:green">MODIFIED</span>\] [`AbstractPartedQueryLogger.java`](https://bitbucket.org/refcodes/refcodes-logger/src/refcodes-logger-3.0.4/src/main/java/org/refcodes/logger/AbstractPartedQueryLogger.java) (see Javadoc at [`AbstractPartedQueryLogger.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-logger/3.0.4/org.refcodes.logger/org/refcodes/logger/AbstractPartedQueryLogger.html))

## Change list &lt;refcodes-logger-alt&gt; (version 3.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.0.4/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.0.4/refcodes-logger-alt-async/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.0.4/refcodes-logger-alt-async/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`AsyncLogger.java`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.0.4/refcodes-logger-alt-async/src/main/java/org/refcodes/logger/alt/async/AsyncLogger.java) (see Javadoc at [`AsyncLogger.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-logger-alt-async/3.0.4/org.refcodes.logger.alt.async/org/refcodes/logger/alt/async/AsyncLogger.html))
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.0.4/refcodes-logger-alt-console/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.0.4/refcodes-logger-alt-console/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.0.4/refcodes-logger-alt-io/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.0.4/refcodes-logger-alt-io/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.0.4/refcodes-logger-alt-simpledb/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.0.4/refcodes-logger-alt-simpledb/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`SimpleDbLogger.java`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.0.4/refcodes-logger-alt-simpledb/src/main/java/org/refcodes/logger/alt/simpledb/SimpleDbLogger.java) (see Javadoc at [`SimpleDbLogger.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-logger-alt-simpledb/3.0.4/org.refcodes.logger.alt.simpledb/org/refcodes/logger/alt/simpledb/SimpleDbLogger.html))
* \[<span style="color:green">MODIFIED</span>\] [`SimpleDbQueryLogger.java`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.0.4/refcodes-logger-alt-simpledb/src/main/java/org/refcodes/logger/alt/simpledb/SimpleDbQueryLogger.java) (see Javadoc at [`SimpleDbQueryLogger.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-logger-alt-simpledb/3.0.4/org.refcodes.logger.alt.simpledb/org/refcodes/logger/alt/simpledb/SimpleDbQueryLogger.html))
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.0.4/refcodes-logger-alt-slf4j/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.0.4/refcodes-logger-alt-spring/pom.xml) 

## Change list &lt;refcodes-logger-ext&gt; (version 3.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-3.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-3.0.4/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-3.0.4/refcodes-logger-ext-slf4j/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-3.0.4/refcodes-logger-ext-slf4j/README.md) 

## Change list &lt;refcodes-graphical-ext&gt; (version 3.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-3.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-3.0.4/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-3.0.4/refcodes-graphical-ext-javafx/pom.xml) 

## Change list &lt;refcodes-checkerboard&gt; (version 3.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-3.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-3.0.4/README.md) 

## Change list &lt;refcodes-checkerboard-alt&gt; (version 3.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-alt/src/refcodes-checkerboard-alt-3.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-alt/src/refcodes-checkerboard-alt-3.0.4/refcodes-checkerboard-alt-javafx/pom.xml) 

## Change list &lt;refcodes-net&gt; (version 3.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-3.0.4/pom.xml) 

## Change list &lt;refcodes-rest&gt; (version 3.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-3.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-3.0.4/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`OauthTokenHandler.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-3.0.4/src/main/java/org/refcodes/rest/OauthTokenHandler.java) (see Javadoc at [`OauthTokenHandler.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/3.0.4/org.refcodes.rest/org/refcodes/rest/OauthTokenHandler.html))
* \[<span style="color:green">MODIFIED</span>\] [`HttpRestServerTest.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-3.0.4/src/test/java/org/refcodes/rest/HttpRestServerTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`OauthTokenHandlerTest.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-3.0.4/src/test/java/org/refcodes/rest/OauthTokenHandlerTest.java) 

## Change list &lt;refcodes-hal&gt; (version 3.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-3.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-3.0.4/README.md) 

## Change list &lt;refcodes-eventbus&gt; (version 3.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-eventbus/src/refcodes-eventbus-3.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-eventbus/src/refcodes-eventbus-3.0.4/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`EventBusSugar.java`](https://bitbucket.org/refcodes/refcodes-eventbus/src/refcodes-eventbus-3.0.4/src/main/java/org/refcodes/eventbus/EventBusSugar.java) (see Javadoc at [`EventBusSugar.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-eventbus/3.0.4/org.refcodes.eventbus/org/refcodes/eventbus/EventBusSugar.html))

## Change list &lt;refcodes-eventbus-ext&gt; (version 3.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-3.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-3.0.4/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-3.0.4/refcodes-eventbus-ext-application/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`ApplicationBusSugar.java`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-3.0.4/refcodes-eventbus-ext-application/src/main/java/org/refcodes/eventbus/ext/application/ApplicationBusSugar.java) (see Javadoc at [`ApplicationBusSugar.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-eventbus-ext-application/3.0.4/org.refcodes.eventbus.ext.application/org/refcodes/eventbus/ext/application/ApplicationBusSugar.html))

## Change list &lt;refcodes-filesystem&gt; (version 3.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-filesystem/src/refcodes-filesystem-3.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-filesystem/src/refcodes-filesystem-3.0.4/README.md) 

## Change list &lt;refcodes-filesystem-alt&gt; (version 3.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-filesystem-alt/src/refcodes-filesystem-alt-3.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-filesystem-alt/src/refcodes-filesystem-alt-3.0.4/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-filesystem-alt/src/refcodes-filesystem-alt-3.0.4/refcodes-filesystem-alt-s3/pom.xml) 

## Change list &lt;refcodes-forwardsecrecy&gt; (version 3.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy/src/refcodes-forwardsecrecy-3.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy/src/refcodes-forwardsecrecy-3.0.4/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`AbstractEncryptionService.java`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy/src/refcodes-forwardsecrecy-3.0.4/src/main/java/org/refcodes/forwardsecrecy/AbstractEncryptionService.java) (see Javadoc at [`AbstractEncryptionService.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-forwardsecrecy/3.0.4/org.refcodes.forwardsecrecy/org/refcodes/forwardsecrecy/AbstractEncryptionService.html))
* \[<span style="color:green">MODIFIED</span>\] [`DecryptionProviderImpl.java`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy/src/refcodes-forwardsecrecy-3.0.4/src/main/java/org/refcodes/forwardsecrecy/DecryptionProviderImpl.java) (see Javadoc at [`DecryptionProviderImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-forwardsecrecy/3.0.4/org.refcodes.forwardsecrecy/org/refcodes/forwardsecrecy/DecryptionProviderImpl.html))

## Change list &lt;refcodes-forwardsecrecy-alt&gt; (version 3.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-3.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-3.0.4/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-3.0.4/refcodes-forwardsecrecy-alt-filesystem/pom.xml) 

## Change list &lt;refcodes-io-ext&gt; (version 3.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-3.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-3.0.4/refcodes-io-ext-observer/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-3.0.4/refcodes-io-ext-observer/README.md) 

## Change list &lt;refcodes-interceptor&gt; (version 3.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-interceptor/src/refcodes-interceptor-3.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-interceptor/src/refcodes-interceptor-3.0.4/README.md) 

## Change list &lt;refcodes-jobbus&gt; (version 3.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-jobbus/src/refcodes-jobbus-3.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-jobbus/src/refcodes-jobbus-3.0.4/README.md) 

## Change list &lt;refcodes-rest-ext&gt; (version 3.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-3.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-3.0.4/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-3.0.4/refcodes-rest-ext-eureka/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-3.0.4/refcodes-rest-ext-eureka/README.md) 

## Change list &lt;refcodes-remoting&gt; (version 3.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-remoting/src/refcodes-remoting-3.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-remoting/src/refcodes-remoting-3.0.4/README.md) 

## Change list &lt;refcodes-remoting-ext&gt; (version 3.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-3.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-3.0.4/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-3.0.4/refcodes-remoting-ext-observer/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-3.0.4/refcodes-remoting-ext-observer/README.md) 

## Change list &lt;refcodes-serial&gt; (version 3.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial/src/refcodes-serial-3.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-serial/src/refcodes-serial-3.0.4/README.md) 

## Change list &lt;refcodes-serial-alt&gt; (version 3.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial-alt/src/refcodes-serial-alt-3.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-serial-alt/src/refcodes-serial-alt-3.0.4/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial-alt/src/refcodes-serial-alt-3.0.4/refcodes-serial-alt-net/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-serial-alt/src/refcodes-serial-alt-3.0.4/refcodes-serial-alt-net/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial-alt/src/refcodes-serial-alt-3.0.4/refcodes-serial-alt-tty/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-serial-alt/src/refcodes-serial-alt-3.0.4/refcodes-serial-alt-tty/README.md) 

## Change list &lt;refcodes-serial-ext&gt; (version 3.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-3.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-3.0.4/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-3.0.4/refcodes-serial-ext-handshake/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-3.0.4/refcodes-serial-ext-handshake/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-3.0.4/refcodes-serial-ext-observer/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-3.0.4/refcodes-serial-ext-observer/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-3.0.4/refcodes-serial-ext-security/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-3.0.4/refcodes-serial-ext-security/README.md) 

## Change list &lt;refcodes-p2p&gt; (version 3.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-p2p/src/refcodes-p2p-3.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-p2p/src/refcodes-p2p-3.0.4/README.md) 

## Change list &lt;refcodes-p2p-alt&gt; (version 3.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-p2p-alt/src/refcodes-p2p-alt-3.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-p2p-alt/src/refcodes-p2p-alt-3.0.4/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-p2p-alt/src/refcodes-p2p-alt-3.0.4/refcodes-p2p-alt-rest/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-p2p-alt/src/refcodes-p2p-alt-3.0.4/refcodes-p2p-alt-serial/pom.xml) 

## Change list &lt;refcodes-p2p-ext&gt; (version 3.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-p2p-ext/src/refcodes-p2p-ext-3.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-p2p-ext/src/refcodes-p2p-ext-3.0.4/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-p2p-ext/src/refcodes-p2p-ext-3.0.4/refcodes-p2p-ext-observer/pom.xml) 

## Change list &lt;refcodes-servicebus&gt; (version 3.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-servicebus/src/refcodes-servicebus-3.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-servicebus/src/refcodes-servicebus-3.0.4/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`ServiceMatcherSugar.java`](https://bitbucket.org/refcodes/refcodes-servicebus/src/refcodes-servicebus-3.0.4/src/main/java/org/refcodes/servicebus/ServiceMatcherSugar.java) (see Javadoc at [`ServiceMatcherSugar.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-servicebus/3.0.4/org.refcodes.servicebus/org/refcodes/servicebus/ServiceMatcherSugar.html))

## Change list &lt;refcodes-servicebus-alt&gt; (version 3.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-servicebus-alt/src/refcodes-servicebus-alt-3.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-servicebus-alt/src/refcodes-servicebus-alt-3.0.4/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-servicebus-alt/src/refcodes-servicebus-alt-3.0.4/refcodes-servicebus-alt-spring/pom.xml) 

## Change list &lt;refcodes-tabular-alt&gt; (version 3.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-tabular-alt/src/refcodes-tabular-alt-3.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-tabular-alt/src/refcodes-tabular-alt-3.0.4/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-tabular-alt/src/refcodes-tabular-alt-3.0.4/refcodes-tabular-alt-forwardsecrecy/pom.xml) 

## Change list &lt;refcodes-archetype&gt; (version 3.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype/src/refcodes-archetype-3.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype/src/refcodes-archetype-3.0.4/README.md) 

## Change list &lt;refcodes-archetype-alt&gt; (version 3.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.0.4/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.0.4/refcodes-archetype-alt-c2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.0.4/refcodes-archetype-alt-c2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.0.4/refcodes-archetype-alt-cli/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.0.4/refcodes-archetype-alt-cli/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.0.4/refcodes-archetype-alt-csv/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.0.4/refcodes-archetype-alt-csv/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.0.4/refcodes-archetype-alt-eventbus/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.0.4/refcodes-archetype-alt-eventbus/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.0.4/refcodes-archetype-alt-rest/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.0.4/refcodes-archetype-alt-rest/README.md) 
