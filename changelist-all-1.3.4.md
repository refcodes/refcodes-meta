# Change list for REFCODES.ORG artifacts' version 1.3.4

> This change list has been auto-generated on `triton` by `steiner` with with `changelist-all.sh` on the 2018-10-11 at 06:46:26.

## Change list &lt;refcodes-licensing&gt; (version 1.3.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-licensing/src/refcodes-licensing-1.3.4/pom.xml) 

## Change list &lt;refcodes-parent&gt; (version 1.3.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-parent/src/refcodes-parent-1.3.4/pom.xml) 

## Change list &lt;refcodes-time&gt; (version 1.3.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-time/src/refcodes-time-1.3.4/pom.xml) 

## Change list &lt;refcodes-mixin&gt; (version 1.3.4)

* \[<span style="color:blue">ADDED</span>\] [`TrimAccessor.java`](https://bitbucket.org/refcodes/refcodes-mixin/src/refcodes-mixin-1.3.4/src/main/java/org/refcodes/mixin/TrimAccessor.java) (see Javadoc at [`TrimAccessor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-mixin/1.3.4/org/refcodes/mixin/TrimAccessor.html))
* \[<span style="color:blue">ADDED</span>\] [`ValidAccessor.java`](https://bitbucket.org/refcodes/refcodes-mixin/src/refcodes-mixin-1.3.4/src/main/java/org/refcodes/mixin/ValidAccessor.java) (see Javadoc at [`ValidAccessor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-mixin/1.3.4/org/refcodes/mixin/ValidAccessor.html))
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-mixin/src/refcodes-mixin-1.3.4/pom.xml) 

## Change list &lt;refcodes-data&gt; (version 1.3.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data/src/refcodes-data-1.3.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`IoTimeout.java`](https://bitbucket.org/refcodes/refcodes-data/src/refcodes-data-1.3.4/src/main/java/org/refcodes/data/IoTimeout.java) (see Javadoc at [`IoTimeout.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-data/1.3.4/org/refcodes/data/IoTimeout.html))

## Change list &lt;refcodes-exception&gt; (version 1.3.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-exception/src/refcodes-exception-1.3.4/pom.xml) 

## Change list &lt;refcodes-factory&gt; (version 1.3.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-factory/src/refcodes-factory-1.3.4/pom.xml) 

## Change list &lt;refcodes-factory-alt&gt; (version 1.3.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-factory-alt/src/refcodes-factory-alt-1.3.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-factory-alt/src/refcodes-factory-alt-1.3.4/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-factory-alt/src/refcodes-factory-alt-1.3.4/refcodes-factory-alt-spring/pom.xml) 

## Change list &lt;refcodes-controlflow&gt; (version 1.3.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-controlflow/src/refcodes-controlflow-1.3.4/pom.xml) 

## Change list &lt;refcodes-numerical&gt; (version 1.3.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-numerical/src/refcodes-numerical-1.3.4/pom.xml) 

## Change list &lt;refcodes-generator&gt; (version 1.3.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-generator/src/refcodes-generator-1.3.4/pom.xml) 

## Change list &lt;refcodes-structure&gt; (version 1.3.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-1.3.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-1.3.4/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`CanonicalMap.java`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-1.3.4/src/main/java/org/refcodes/structure/CanonicalMap.java) (see Javadoc at [`CanonicalMap.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-structure/1.3.4/org/refcodes/structure/CanonicalMap.html))
* \[<span style="color:green">MODIFIED</span>\] [`Keys.java`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-1.3.4/src/main/java/org/refcodes/structure/Keys.java) (see Javadoc at [`Keys.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-structure/1.3.4/org/refcodes/structure/Keys.html))
* \[<span style="color:green">MODIFIED</span>\] [`PathMapImpl.java`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-1.3.4/src/main/java/org/refcodes/structure/PathMapImpl.java) (see Javadoc at [`PathMapImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-structure/1.3.4/org/refcodes/structure/PathMapImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`PathMap.java`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-1.3.4/src/main/java/org/refcodes/structure/PathMap.java) (see Javadoc at [`PathMap.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-structure/1.3.4/org/refcodes/structure/PathMap.html))
* \[<span style="color:green">MODIFIED</span>\] [`TypeUtility.java`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-1.3.4/src/main/java/org/refcodes/structure/TypeUtility.java) (see Javadoc at [`TypeUtility.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-structure/1.3.4/org/refcodes/structure/TypeUtility.html))
* \[<span style="color:green">MODIFIED</span>\] [`PathMapTest.java`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-1.3.4/src/test/java/org/refcodes/structure/PathMapTest.java) 

## Change list &lt;refcodes-runtime&gt; (version 1.3.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-1.3.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-1.3.4/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`ProcessResult.java`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-1.3.4/src/main/java/org/refcodes/runtime/ProcessResult.java) (see Javadoc at [`ProcessResult.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-runtime/1.3.4/org/refcodes/runtime/ProcessResult.html))
* \[<span style="color:green">MODIFIED</span>\] [`RuntimeUtility.java`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-1.3.4/src/main/java/org/refcodes/runtime/RuntimeUtility.java) (see Javadoc at [`RuntimeUtility.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-runtime/1.3.4/org/refcodes/runtime/RuntimeUtility.html))

## Change list &lt;refcodes-component&gt; (version 1.3.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-1.3.4/pom.xml) 

## Change list &lt;refcodes-data-ext&gt; (version 1.3.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.3.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.3.4/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.3.4/refcodes-data-ext-boulderdash/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.3.4/refcodes-data-ext-checkers/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.3.4/refcodes-data-ext-checkers/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.3.4/refcodes-data-ext-chess/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.3.4/refcodes-data-ext-chess/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.3.4/refcodes-data-ext-corporate/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.3.4/refcodes-data-ext-symbols/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.3.4/refcodes-data-ext-symbols/README.md) 

## Change list &lt;refcodes-graphical&gt; (version 1.3.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-graphical/src/refcodes-graphical-1.3.4/pom.xml) 

## Change list &lt;refcodes-textual&gt; (version 1.3.4)

* \[<span style="color:blue">ADDED</span>\] [`CsvEscapeModeAccessor.java`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-1.3.4/src/main/java/org/refcodes/textual/CsvEscapeModeAccessor.java) (see Javadoc at [`CsvEscapeModeAccessor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-textual/1.3.4/org/refcodes/textual/CsvEscapeModeAccessor.html))
* \[<span style="color:blue">ADDED</span>\] [`CsvMixin.java`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-1.3.4/src/main/java/org/refcodes/textual/CsvMixin.java) (see Javadoc at [`CsvMixin.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-textual/1.3.4/org/refcodes/textual/CsvMixin.html))
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-1.3.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`CsvBuilderImpl.java`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-1.3.4/src/main/java/org/refcodes/textual/CsvBuilderImpl.java) (see Javadoc at [`CsvBuilderImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-textual/1.3.4/org/refcodes/textual/CsvBuilderImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`CsvBuilder.java`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-1.3.4/src/main/java/org/refcodes/textual/CsvBuilder.java) (see Javadoc at [`CsvBuilder.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-textual/1.3.4/org/refcodes/textual/CsvBuilder.html))
* \[<span style="color:green">MODIFIED</span>\] [`CsvBuilderTest.java`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-1.3.4/src/test/java/org/refcodes/textual/CsvBuilderTest.java) 

## Change list &lt;refcodes-criteria&gt; (version 1.3.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-1.3.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-1.3.4/README.md) 

## Change list &lt;refcodes-tabular&gt; (version 1.3.4)

* \[<span style="color:blue">ADDED</span>\] [`CsvRecordsReader.java`](https://bitbucket.org/refcodes/refcodes-tabular/src/refcodes-tabular-1.3.4/src/main/java/org/refcodes/tabular/CsvRecordsReader.java) (see Javadoc at [`CsvRecordsReader.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-tabular/1.3.4/org/refcodes/tabular/CsvRecordsReader.html))
* \[<span style="color:blue">ADDED</span>\] [`CsvRecordsWriter.java`](https://bitbucket.org/refcodes/refcodes-tabular/src/refcodes-tabular-1.3.4/src/main/java/org/refcodes/tabular/CsvRecordsWriter.java) (see Javadoc at [`CsvRecordsWriter.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-tabular/1.3.4/org/refcodes/tabular/CsvRecordsWriter.html))
* \[<span style="color:blue">ADDED</span>\] [`CsvStringRecordsReader.java`](https://bitbucket.org/refcodes/refcodes-tabular/src/refcodes-tabular-1.3.4/src/main/java/org/refcodes/tabular/CsvStringRecordsReader.java) (see Javadoc at [`CsvStringRecordsReader.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-tabular/1.3.4/org/refcodes/tabular/CsvStringRecordsReader.html))
* \[<span style="color:blue">ADDED</span>\] [`CsvStringRecordsWriter.java`](https://bitbucket.org/refcodes/refcodes-tabular/src/refcodes-tabular-1.3.4/src/main/java/org/refcodes/tabular/CsvStringRecordsWriter.java) (see Javadoc at [`CsvStringRecordsWriter.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-tabular/1.3.4/org/refcodes/tabular/CsvStringRecordsWriter.html))
* \[<span style="color:blue">ADDED</span>\] [`IntegerColumnFactory.java`](https://bitbucket.org/refcodes/refcodes-tabular/src/refcodes-tabular-1.3.4/src/main/java/org/refcodes/tabular/IntegerColumnFactory.java) (see Javadoc at [`IntegerColumnFactory.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-tabular/1.3.4/org/refcodes/tabular/IntegerColumnFactory.html))
* \[<span style="color:blue">ADDED</span>\] [`RecordsReader.java`](https://bitbucket.org/refcodes/refcodes-tabular/src/refcodes-tabular-1.3.4/src/main/java/org/refcodes/tabular/RecordsReader.java) (see Javadoc at [`RecordsReader.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-tabular/1.3.4/org/refcodes/tabular/RecordsReader.html))
* \[<span style="color:blue">ADDED</span>\] [`RecordsWriter.java`](https://bitbucket.org/refcodes/refcodes-tabular/src/refcodes-tabular-1.3.4/src/main/java/org/refcodes/tabular/RecordsWriter.java) (see Javadoc at [`RecordsWriter.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-tabular/1.3.4/org/refcodes/tabular/RecordsWriter.html))
* \[<span style="color:blue">ADDED</span>\] [`CsvStringReaderTest.java`](https://bitbucket.org/refcodes/refcodes-tabular/src/refcodes-tabular-1.3.4/src/test/java/org/refcodes/tabular/CsvStringReaderTest.java) 
* \[<span style="color:blue">ADDED</span>\] [`CsvStringWriterTest.java`](https://bitbucket.org/refcodes/refcodes-tabular/src/refcodes-tabular-1.3.4/src/test/java/org/refcodes/tabular/CsvStringWriterTest.java) 
* \[<span style="color:blue">ADDED</span>\] [`string_records.csv`](https://bitbucket.org/refcodes/refcodes-tabular/src/refcodes-tabular-1.3.4/src/test/resources/string_records.csv) 
* \[<span style="color:red">DELETED</span>\] `CsvFileRecordsImpl.java`
* \[<span style="color:red">DELETED</span>\] `CsvInputStreamRecordsImpl.java`
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-tabular/src/refcodes-tabular-1.3.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`AbstractColumn.java`](https://bitbucket.org/refcodes/refcodes-tabular/src/refcodes-tabular-1.3.4/src/main/java/org/refcodes/tabular/AbstractColumn.java) (see Javadoc at [`AbstractColumn.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-tabular/1.3.4/org/refcodes/tabular/AbstractColumn.html))
* \[<span style="color:green">MODIFIED</span>\] [`AbstractHeader.java`](https://bitbucket.org/refcodes/refcodes-tabular/src/refcodes-tabular-1.3.4/src/main/java/org/refcodes/tabular/AbstractHeader.java) (see Javadoc at [`AbstractHeader.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-tabular/1.3.4/org/refcodes/tabular/AbstractHeader.html))
* \[<span style="color:green">MODIFIED</span>\] [`Column.java`](https://bitbucket.org/refcodes/refcodes-tabular/src/refcodes-tabular-1.3.4/src/main/java/org/refcodes/tabular/Column.java) (see Javadoc at [`Column.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-tabular/1.3.4/org/refcodes/tabular/Column.html))
* \[<span style="color:green">MODIFIED</span>\] [`Columns.java`](https://bitbucket.org/refcodes/refcodes-tabular/src/refcodes-tabular-1.3.4/src/main/java/org/refcodes/tabular/Columns.java) (see Javadoc at [`Columns.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-tabular/1.3.4/org/refcodes/tabular/Columns.html))
* \[<span style="color:green">MODIFIED</span>\] [`FormattedColumnDecorator.java`](https://bitbucket.org/refcodes/refcodes-tabular/src/refcodes-tabular-1.3.4/src/main/java/org/refcodes/tabular/FormattedColumnDecorator.java) (see Javadoc at [`FormattedColumnDecorator.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-tabular/1.3.4/org/refcodes/tabular/FormattedColumnDecorator.html))
* \[<span style="color:green">MODIFIED</span>\] [`FormattedColumns.java`](https://bitbucket.org/refcodes/refcodes-tabular/src/refcodes-tabular-1.3.4/src/main/java/org/refcodes/tabular/FormattedColumns.java) (see Javadoc at [`FormattedColumns.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-tabular/1.3.4/org/refcodes/tabular/FormattedColumns.html))
* \[<span style="color:green">MODIFIED</span>\] [`FormattedHeader.java`](https://bitbucket.org/refcodes/refcodes-tabular/src/refcodes-tabular-1.3.4/src/main/java/org/refcodes/tabular/FormattedHeader.java) (see Javadoc at [`FormattedHeader.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-tabular/1.3.4/org/refcodes/tabular/FormattedHeader.html))
* \[<span style="color:green">MODIFIED</span>\] [`HeaderImpl.java`](https://bitbucket.org/refcodes/refcodes-tabular/src/refcodes-tabular-1.3.4/src/main/java/org/refcodes/tabular/HeaderImpl.java) (see Javadoc at [`HeaderImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-tabular/1.3.4/org/refcodes/tabular/HeaderImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`Header.java`](https://bitbucket.org/refcodes/refcodes-tabular/src/refcodes-tabular-1.3.4/src/main/java/org/refcodes/tabular/Header.java) (see Javadoc at [`Header.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-tabular/1.3.4/org/refcodes/tabular/Header.html))
* \[<span style="color:green">MODIFIED</span>\] [`RecordImpl.java`](https://bitbucket.org/refcodes/refcodes-tabular/src/refcodes-tabular-1.3.4/src/main/java/org/refcodes/tabular/RecordImpl.java) (see Javadoc at [`RecordImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-tabular/1.3.4/org/refcodes/tabular/RecordImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`Record.java`](https://bitbucket.org/refcodes/refcodes-tabular/src/refcodes-tabular-1.3.4/src/main/java/org/refcodes/tabular/Record.java) (see Javadoc at [`Record.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-tabular/1.3.4/org/refcodes/tabular/Record.html))
* \[<span style="color:green">MODIFIED</span>\] [`RecordsImpl.java`](https://bitbucket.org/refcodes/refcodes-tabular/src/refcodes-tabular-1.3.4/src/main/java/org/refcodes/tabular/RecordsImpl.java) (see Javadoc at [`RecordsImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-tabular/1.3.4/org/refcodes/tabular/RecordsImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`Records.java`](https://bitbucket.org/refcodes/refcodes-tabular/src/refcodes-tabular-1.3.4/src/main/java/org/refcodes/tabular/Records.java) (see Javadoc at [`Records.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-tabular/1.3.4/org/refcodes/tabular/Records.html))
* \[<span style="color:green">MODIFIED</span>\] [`TabularUtility.java`](https://bitbucket.org/refcodes/refcodes-tabular/src/refcodes-tabular-1.3.4/src/main/java/org/refcodes/tabular/TabularUtility.java) (see Javadoc at [`TabularUtility.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-tabular/1.3.4/org/refcodes/tabular/TabularUtility.html))
* \[<span style="color:green">MODIFIED</span>\] [`ColumnTest.java`](https://bitbucket.org/refcodes/refcodes-tabular/src/refcodes-tabular-1.3.4/src/test/java/org/refcodes/tabular/ColumnTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`HeaderTest.java`](https://bitbucket.org/refcodes/refcodes-tabular/src/refcodes-tabular-1.3.4/src/test/java/org/refcodes/tabular/HeaderTest.java) 

## Change list &lt;refcodes-matcher&gt; (version 1.3.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-matcher/src/refcodes-matcher-1.3.4/pom.xml) 

## Change list &lt;refcodes-observer&gt; (version 1.3.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-observer/src/refcodes-observer-1.3.4/pom.xml) 

## Change list &lt;refcodes-command&gt; (version 1.3.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-command/src/refcodes-command-1.3.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-command/src/refcodes-command-1.3.4/README.md) 

## Change list &lt;refcodes-cli&gt; (version 1.3.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-1.3.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-1.3.4/README.md) 

## Change list &lt;refcodes-io&gt; (version 1.3.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-io/src/refcodes-io-1.3.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-io/src/refcodes-io-1.3.4/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`FileUtility.java`](https://bitbucket.org/refcodes/refcodes-io/src/refcodes-io-1.3.4/src/main/java/org/refcodes/io/FileUtility.java) (see Javadoc at [`FileUtility.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-io/1.3.4/org/refcodes/io/FileUtility.html))
* \[<span style="color:green">MODIFIED</span>\] [`FileUtilityTest.java`](https://bitbucket.org/refcodes/refcodes-io/src/refcodes-io-1.3.4/src/test/java/org/refcodes/io/FileUtilityTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`ZipFileStreamTest.java`](https://bitbucket.org/refcodes/refcodes-io/src/refcodes-io-1.3.4/src/test/java/org/refcodes/io/ZipFileStreamTest.java) 

## Change list &lt;refcodes-codec&gt; (version 1.3.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-1.3.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-1.3.4/README.md) 

## Change list &lt;refcodes-component-ext&gt; (version 1.3.4)

* \[<span style="color:blue">ADDED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.3.4/README.md) 
* \[<span style="color:blue">ADDED</span>\] [`module-info.java.off`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.3.4/refcodes-component-ext-observer/src/main/java/module-info.java.off) 
* \[<span style="color:blue">ADDED</span>\] [`AbstractConnectionRequestEvent.java`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.3.4/refcodes-component-ext-observer/src/main/java/org/refcodes/component/ext/observer/AbstractConnectionRequestEvent.java) (see Javadoc at [`AbstractConnectionRequestEvent.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component-ext-observer/1.3.4/org/refcodes/component/ext/observer/AbstractConnectionRequestEvent.html))
* \[<span style="color:blue">ADDED</span>\] [`AbstractLifeCycleRequestEvent.java`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.3.4/refcodes-component-ext-observer/src/main/java/org/refcodes/component/ext/observer/AbstractLifeCycleRequestEvent.java) (see Javadoc at [`AbstractLifeCycleRequestEvent.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component-ext-observer/1.3.4/org/refcodes/component/ext/observer/AbstractLifeCycleRequestEvent.html))
* \[<span style="color:blue">ADDED</span>\] [`AbstractLifeCycleStatusEvent.java`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.3.4/refcodes-component-ext-observer/src/main/java/org/refcodes/component/ext/observer/AbstractLifeCycleStatusEvent.java) (see Javadoc at [`AbstractLifeCycleStatusEvent.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component-ext-observer/1.3.4/org/refcodes/component/ext/observer/AbstractLifeCycleStatusEvent.html))
* \[<span style="color:blue">ADDED</span>\] [`AbstractNetworkRequestEvent.java`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.3.4/refcodes-component-ext-observer/src/main/java/org/refcodes/component/ext/observer/AbstractNetworkRequestEvent.java) (see Javadoc at [`AbstractNetworkRequestEvent.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component-ext-observer/1.3.4/org/refcodes/component/ext/observer/AbstractNetworkRequestEvent.html))
* \[<span style="color:blue">ADDED</span>\] [`AbstractNetworkStatusEvent.java`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.3.4/refcodes-component-ext-observer/src/main/java/org/refcodes/component/ext/observer/AbstractNetworkStatusEvent.java) (see Javadoc at [`AbstractNetworkStatusEvent.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component-ext-observer/1.3.4/org/refcodes/component/ext/observer/AbstractNetworkStatusEvent.html))
* \[<span style="color:blue">ADDED</span>\] [`ClosedEvent.java`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.3.4/refcodes-component-ext-observer/src/main/java/org/refcodes/component/ext/observer/ClosedEvent.java) (see Javadoc at [`ClosedEvent.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component-ext-observer/1.3.4/org/refcodes/component/ext/observer/ClosedEvent.html))
* \[<span style="color:blue">ADDED</span>\] [`CloseEvent.java`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.3.4/refcodes-component-ext-observer/src/main/java/org/refcodes/component/ext/observer/CloseEvent.java) (see Javadoc at [`CloseEvent.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component-ext-observer/1.3.4/org/refcodes/component/ext/observer/CloseEvent.html))
* \[<span style="color:blue">ADDED</span>\] [`ConnectionObserver.java`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.3.4/refcodes-component-ext-observer/src/main/java/org/refcodes/component/ext/observer/ConnectionObserver.java) (see Javadoc at [`ConnectionObserver.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component-ext-observer/1.3.4/org/refcodes/component/ext/observer/ConnectionObserver.html))
* \[<span style="color:blue">ADDED</span>\] [`ConnectionRequestEvent.java`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.3.4/refcodes-component-ext-observer/src/main/java/org/refcodes/component/ext/observer/ConnectionRequestEvent.java) (see Javadoc at [`ConnectionRequestEvent.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component-ext-observer/1.3.4/org/refcodes/component/ext/observer/ConnectionRequestEvent.html))
* \[<span style="color:blue">ADDED</span>\] [`DestroyedEvent.java`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.3.4/refcodes-component-ext-observer/src/main/java/org/refcodes/component/ext/observer/DestroyedEvent.java) (see Javadoc at [`DestroyedEvent.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component-ext-observer/1.3.4/org/refcodes/component/ext/observer/DestroyedEvent.html))
* \[<span style="color:blue">ADDED</span>\] [`DestroyEvent.java`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.3.4/refcodes-component-ext-observer/src/main/java/org/refcodes/component/ext/observer/DestroyEvent.java) (see Javadoc at [`DestroyEvent.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component-ext-observer/1.3.4/org/refcodes/component/ext/observer/DestroyEvent.html))
* \[<span style="color:blue">ADDED</span>\] [`GenericClosedEvent.java`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.3.4/refcodes-component-ext-observer/src/main/java/org/refcodes/component/ext/observer/GenericClosedEvent.java) (see Javadoc at [`GenericClosedEvent.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component-ext-observer/1.3.4/org/refcodes/component/ext/observer/GenericClosedEvent.html))
* \[<span style="color:blue">ADDED</span>\] [`GenericCloseEvent.java`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.3.4/refcodes-component-ext-observer/src/main/java/org/refcodes/component/ext/observer/GenericCloseEvent.java) (see Javadoc at [`GenericCloseEvent.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component-ext-observer/1.3.4/org/refcodes/component/ext/observer/GenericCloseEvent.html))
* \[<span style="color:blue">ADDED</span>\] [`GenericConnectionRequestEvent.java`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.3.4/refcodes-component-ext-observer/src/main/java/org/refcodes/component/ext/observer/GenericConnectionRequestEvent.java) (see Javadoc at [`GenericConnectionRequestEvent.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component-ext-observer/1.3.4/org/refcodes/component/ext/observer/GenericConnectionRequestEvent.html))
* \[<span style="color:blue">ADDED</span>\] [`GenericDestroyedEvent.java`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.3.4/refcodes-component-ext-observer/src/main/java/org/refcodes/component/ext/observer/GenericDestroyedEvent.java) (see Javadoc at [`GenericDestroyedEvent.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component-ext-observer/1.3.4/org/refcodes/component/ext/observer/GenericDestroyedEvent.html))
* \[<span style="color:blue">ADDED</span>\] [`GenericDestroyEvent.java`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.3.4/refcodes-component-ext-observer/src/main/java/org/refcodes/component/ext/observer/GenericDestroyEvent.java) (see Javadoc at [`GenericDestroyEvent.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component-ext-observer/1.3.4/org/refcodes/component/ext/observer/GenericDestroyEvent.html))
* \[<span style="color:blue">ADDED</span>\] [`GenericInitializedEvent.java`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.3.4/refcodes-component-ext-observer/src/main/java/org/refcodes/component/ext/observer/GenericInitializedEvent.java) (see Javadoc at [`GenericInitializedEvent.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component-ext-observer/1.3.4/org/refcodes/component/ext/observer/GenericInitializedEvent.html))
* \[<span style="color:blue">ADDED</span>\] [`GenericInitializeEvent.java`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.3.4/refcodes-component-ext-observer/src/main/java/org/refcodes/component/ext/observer/GenericInitializeEvent.java) (see Javadoc at [`GenericInitializeEvent.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component-ext-observer/1.3.4/org/refcodes/component/ext/observer/GenericInitializeEvent.html))
* \[<span style="color:blue">ADDED</span>\] [`GenericLifeCycleRequestEvent.java`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.3.4/refcodes-component-ext-observer/src/main/java/org/refcodes/component/ext/observer/GenericLifeCycleRequestEvent.java) (see Javadoc at [`GenericLifeCycleRequestEvent.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component-ext-observer/1.3.4/org/refcodes/component/ext/observer/GenericLifeCycleRequestEvent.html))
* \[<span style="color:blue">ADDED</span>\] [`GenericLifeCycleRequestObserver.java`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.3.4/refcodes-component-ext-observer/src/main/java/org/refcodes/component/ext/observer/GenericLifeCycleRequestObserver.java) (see Javadoc at [`GenericLifeCycleRequestObserver.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component-ext-observer/1.3.4/org/refcodes/component/ext/observer/GenericLifeCycleRequestObserver.html))
* \[<span style="color:blue">ADDED</span>\] [`GenericLifeCycleStatusEvent.java`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.3.4/refcodes-component-ext-observer/src/main/java/org/refcodes/component/ext/observer/GenericLifeCycleStatusEvent.java) (see Javadoc at [`GenericLifeCycleStatusEvent.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component-ext-observer/1.3.4/org/refcodes/component/ext/observer/GenericLifeCycleStatusEvent.html))
* \[<span style="color:blue">ADDED</span>\] [`GenericLifeCycleStatusObserver.java`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.3.4/refcodes-component-ext-observer/src/main/java/org/refcodes/component/ext/observer/GenericLifeCycleStatusObserver.java) (see Javadoc at [`GenericLifeCycleStatusObserver.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component-ext-observer/1.3.4/org/refcodes/component/ext/observer/GenericLifeCycleStatusObserver.html))
* \[<span style="color:blue">ADDED</span>\] [`GenericNetworkRequestEvent.java`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.3.4/refcodes-component-ext-observer/src/main/java/org/refcodes/component/ext/observer/GenericNetworkRequestEvent.java) (see Javadoc at [`GenericNetworkRequestEvent.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component-ext-observer/1.3.4/org/refcodes/component/ext/observer/GenericNetworkRequestEvent.html))
* \[<span style="color:blue">ADDED</span>\] [`GenericNetworkStatusEvent.java`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.3.4/refcodes-component-ext-observer/src/main/java/org/refcodes/component/ext/observer/GenericNetworkStatusEvent.java) (see Javadoc at [`GenericNetworkStatusEvent.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component-ext-observer/1.3.4/org/refcodes/component/ext/observer/GenericNetworkStatusEvent.html))
* \[<span style="color:blue">ADDED</span>\] [`GenericOpenedEvent.java`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.3.4/refcodes-component-ext-observer/src/main/java/org/refcodes/component/ext/observer/GenericOpenedEvent.java) (see Javadoc at [`GenericOpenedEvent.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component-ext-observer/1.3.4/org/refcodes/component/ext/observer/GenericOpenedEvent.html))
* \[<span style="color:blue">ADDED</span>\] [`GenericOpenEvent.java`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.3.4/refcodes-component-ext-observer/src/main/java/org/refcodes/component/ext/observer/GenericOpenEvent.java) (see Javadoc at [`GenericOpenEvent.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component-ext-observer/1.3.4/org/refcodes/component/ext/observer/GenericOpenEvent.html))
* \[<span style="color:blue">ADDED</span>\] [`GenericPausedEvent.java`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.3.4/refcodes-component-ext-observer/src/main/java/org/refcodes/component/ext/observer/GenericPausedEvent.java) (see Javadoc at [`GenericPausedEvent.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component-ext-observer/1.3.4/org/refcodes/component/ext/observer/GenericPausedEvent.html))
* \[<span style="color:blue">ADDED</span>\] [`GenericPauseEvent.java`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.3.4/refcodes-component-ext-observer/src/main/java/org/refcodes/component/ext/observer/GenericPauseEvent.java) (see Javadoc at [`GenericPauseEvent.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component-ext-observer/1.3.4/org/refcodes/component/ext/observer/GenericPauseEvent.html))
* \[<span style="color:blue">ADDED</span>\] [`GenericResumedEvent.java`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.3.4/refcodes-component-ext-observer/src/main/java/org/refcodes/component/ext/observer/GenericResumedEvent.java) (see Javadoc at [`GenericResumedEvent.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component-ext-observer/1.3.4/org/refcodes/component/ext/observer/GenericResumedEvent.html))
* \[<span style="color:blue">ADDED</span>\] [`GenericResumeEvent.java`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.3.4/refcodes-component-ext-observer/src/main/java/org/refcodes/component/ext/observer/GenericResumeEvent.java) (see Javadoc at [`GenericResumeEvent.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component-ext-observer/1.3.4/org/refcodes/component/ext/observer/GenericResumeEvent.html))
* \[<span style="color:blue">ADDED</span>\] [`GenericStartedEvent.java`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.3.4/refcodes-component-ext-observer/src/main/java/org/refcodes/component/ext/observer/GenericStartedEvent.java) (see Javadoc at [`GenericStartedEvent.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component-ext-observer/1.3.4/org/refcodes/component/ext/observer/GenericStartedEvent.html))
* \[<span style="color:blue">ADDED</span>\] [`GenericStartEvent.java`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.3.4/refcodes-component-ext-observer/src/main/java/org/refcodes/component/ext/observer/GenericStartEvent.java) (see Javadoc at [`GenericStartEvent.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component-ext-observer/1.3.4/org/refcodes/component/ext/observer/GenericStartEvent.html))
* \[<span style="color:blue">ADDED</span>\] [`GenericStopEvent.java`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.3.4/refcodes-component-ext-observer/src/main/java/org/refcodes/component/ext/observer/GenericStopEvent.java) (see Javadoc at [`GenericStopEvent.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component-ext-observer/1.3.4/org/refcodes/component/ext/observer/GenericStopEvent.html))
* \[<span style="color:blue">ADDED</span>\] [`GenericStoppedEvent.java`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.3.4/refcodes-component-ext-observer/src/main/java/org/refcodes/component/ext/observer/GenericStoppedEvent.java) (see Javadoc at [`GenericStoppedEvent.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component-ext-observer/1.3.4/org/refcodes/component/ext/observer/GenericStoppedEvent.html))
* \[<span style="color:blue">ADDED</span>\] [`InitializedEvent.java`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.3.4/refcodes-component-ext-observer/src/main/java/org/refcodes/component/ext/observer/InitializedEvent.java) (see Javadoc at [`InitializedEvent.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component-ext-observer/1.3.4/org/refcodes/component/ext/observer/InitializedEvent.html))
* \[<span style="color:blue">ADDED</span>\] [`InitializeEvent.java`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.3.4/refcodes-component-ext-observer/src/main/java/org/refcodes/component/ext/observer/InitializeEvent.java) (see Javadoc at [`InitializeEvent.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component-ext-observer/1.3.4/org/refcodes/component/ext/observer/InitializeEvent.html))
* \[<span style="color:blue">ADDED</span>\] [`LifeCycleRequestEvent.java`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.3.4/refcodes-component-ext-observer/src/main/java/org/refcodes/component/ext/observer/LifeCycleRequestEvent.java) (see Javadoc at [`LifeCycleRequestEvent.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component-ext-observer/1.3.4/org/refcodes/component/ext/observer/LifeCycleRequestEvent.html))
* \[<span style="color:blue">ADDED</span>\] [`LifeCycleRequestObserver.java`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.3.4/refcodes-component-ext-observer/src/main/java/org/refcodes/component/ext/observer/LifeCycleRequestObserver.java) (see Javadoc at [`LifeCycleRequestObserver.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component-ext-observer/1.3.4/org/refcodes/component/ext/observer/LifeCycleRequestObserver.html))
* \[<span style="color:blue">ADDED</span>\] [`NetworkRequestEvent.java`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.3.4/refcodes-component-ext-observer/src/main/java/org/refcodes/component/ext/observer/NetworkRequestEvent.java) (see Javadoc at [`NetworkRequestEvent.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component-ext-observer/1.3.4/org/refcodes/component/ext/observer/NetworkRequestEvent.html))
* \[<span style="color:blue">ADDED</span>\] [`ObservableLifeCycleAutomatonAccessor.java`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.3.4/refcodes-component-ext-observer/src/main/java/org/refcodes/component/ext/observer/ObservableLifeCycleAutomatonAccessor.java) (see Javadoc at [`ObservableLifeCycleAutomatonAccessor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component-ext-observer/1.3.4/org/refcodes/component/ext/observer/ObservableLifeCycleAutomatonAccessor.html))
* \[<span style="color:blue">ADDED</span>\] [`ObservableLifeCycleRequestAutomatonImpl.java`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.3.4/refcodes-component-ext-observer/src/main/java/org/refcodes/component/ext/observer/ObservableLifeCycleRequestAutomatonImpl.java) (see Javadoc at [`ObservableLifeCycleRequestAutomatonImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component-ext-observer/1.3.4/org/refcodes/component/ext/observer/ObservableLifeCycleRequestAutomatonImpl.html))
* \[<span style="color:blue">ADDED</span>\] [`ObservableLifeCycleRequestAutomaton.java`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.3.4/refcodes-component-ext-observer/src/main/java/org/refcodes/component/ext/observer/ObservableLifeCycleRequestAutomaton.java) (see Javadoc at [`ObservableLifeCycleRequestAutomaton.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component-ext-observer/1.3.4/org/refcodes/component/ext/observer/ObservableLifeCycleRequestAutomaton.html))
* \[<span style="color:blue">ADDED</span>\] [`ObservableLifeCycleStatusAutomatonImpl.java`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.3.4/refcodes-component-ext-observer/src/main/java/org/refcodes/component/ext/observer/ObservableLifeCycleStatusAutomatonImpl.java) (see Javadoc at [`ObservableLifeCycleStatusAutomatonImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component-ext-observer/1.3.4/org/refcodes/component/ext/observer/ObservableLifeCycleStatusAutomatonImpl.html))
* \[<span style="color:blue">ADDED</span>\] [`ObservableLifeCycleStatusAutomaton.java`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.3.4/refcodes-component-ext-observer/src/main/java/org/refcodes/component/ext/observer/ObservableLifeCycleStatusAutomaton.java) (see Javadoc at [`ObservableLifeCycleStatusAutomaton.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component-ext-observer/1.3.4/org/refcodes/component/ext/observer/ObservableLifeCycleStatusAutomaton.html))
* \[<span style="color:blue">ADDED</span>\] [`OpenedEvent.java`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.3.4/refcodes-component-ext-observer/src/main/java/org/refcodes/component/ext/observer/OpenedEvent.java) (see Javadoc at [`OpenedEvent.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component-ext-observer/1.3.4/org/refcodes/component/ext/observer/OpenedEvent.html))
* \[<span style="color:blue">ADDED</span>\] [`OpenEvent.java`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.3.4/refcodes-component-ext-observer/src/main/java/org/refcodes/component/ext/observer/OpenEvent.java) (see Javadoc at [`OpenEvent.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component-ext-observer/1.3.4/org/refcodes/component/ext/observer/OpenEvent.html))
* \[<span style="color:blue">ADDED</span>\] [`PausedEvent.java`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.3.4/refcodes-component-ext-observer/src/main/java/org/refcodes/component/ext/observer/PausedEvent.java) (see Javadoc at [`PausedEvent.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component-ext-observer/1.3.4/org/refcodes/component/ext/observer/PausedEvent.html))
* \[<span style="color:blue">ADDED</span>\] [`PauseEvent.java`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.3.4/refcodes-component-ext-observer/src/main/java/org/refcodes/component/ext/observer/PauseEvent.java) (see Javadoc at [`PauseEvent.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component-ext-observer/1.3.4/org/refcodes/component/ext/observer/PauseEvent.html))
* \[<span style="color:blue">ADDED</span>\] [`ResumedEvent.java`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.3.4/refcodes-component-ext-observer/src/main/java/org/refcodes/component/ext/observer/ResumedEvent.java) (see Javadoc at [`ResumedEvent.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component-ext-observer/1.3.4/org/refcodes/component/ext/observer/ResumedEvent.html))
* \[<span style="color:blue">ADDED</span>\] [`ResumeEvent.java`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.3.4/refcodes-component-ext-observer/src/main/java/org/refcodes/component/ext/observer/ResumeEvent.java) (see Javadoc at [`ResumeEvent.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component-ext-observer/1.3.4/org/refcodes/component/ext/observer/ResumeEvent.html))
* \[<span style="color:blue">ADDED</span>\] [`StartedEvent.java`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.3.4/refcodes-component-ext-observer/src/main/java/org/refcodes/component/ext/observer/StartedEvent.java) (see Javadoc at [`StartedEvent.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component-ext-observer/1.3.4/org/refcodes/component/ext/observer/StartedEvent.html))
* \[<span style="color:blue">ADDED</span>\] [`StartEvent.java`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.3.4/refcodes-component-ext-observer/src/main/java/org/refcodes/component/ext/observer/StartEvent.java) (see Javadoc at [`StartEvent.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component-ext-observer/1.3.4/org/refcodes/component/ext/observer/StartEvent.html))
* \[<span style="color:blue">ADDED</span>\] [`StopEvent.java`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.3.4/refcodes-component-ext-observer/src/main/java/org/refcodes/component/ext/observer/StopEvent.java) (see Javadoc at [`StopEvent.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component-ext-observer/1.3.4/org/refcodes/component/ext/observer/StopEvent.html))
* \[<span style="color:blue">ADDED</span>\] [`StoppedEvent.java`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.3.4/refcodes-component-ext-observer/src/main/java/org/refcodes/component/ext/observer/StoppedEvent.java) (see Javadoc at [`StoppedEvent.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component-ext-observer/1.3.4/org/refcodes/component/ext/observer/StoppedEvent.html))
* \[<span style="color:blue">ADDED</span>\] [`ObservableLifeCycleAutomatonTest.java`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.3.4/refcodes-component-ext-observer/src/test/java/org/refcodes/component/ext/observer/ObservableLifeCycleAutomatonTest.java) 
* \[<span style="color:blue">ADDED</span>\] [`ObservableLifeCycleRequestAutomatonTest.java`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.3.4/refcodes-component-ext-observer/src/test/java/org/refcodes/component/ext/observer/ObservableLifeCycleRequestAutomatonTest.java) 
* \[<span style="color:red">DELETED</span>\] `LifeCycleRequest.java`
* \[<span style="color:red">DELETED</span>\] `DestroyEventImpl.java`
* \[<span style="color:red">DELETED</span>\] `InitializeEventImpl.java`
* \[<span style="color:red">DELETED</span>\] `PauseEventImpl.java`
* \[<span style="color:red">DELETED</span>\] `ResumeEventImpl.java`
* \[<span style="color:red">DELETED</span>\] `StartEventImpl.java`
* \[<span style="color:red">DELETED</span>\] `StopEventImpl.java`
* \[<span style="color:red">DELETED</span>\] `StartEvent.java`
* \[<span style="color:red">DELETED</span>\] `StopEvent.java`
* \[<span style="color:red">DELETED</span>\] `AbstractObservableLifeCycleAutomaton.java`
* \[<span style="color:red">DELETED</span>\] `ObservableLifeCycleAutomatonImpl.java`
* \[<span style="color:red">DELETED</span>\] `LifeCycleObserver.java`
* \[<span style="color:red">DELETED</span>\] `ObservableLifeCycleAutomaton.java`
* \[<span style="color:red">DELETED</span>\] `ObservableLifeCycleComponentTest.java`
* \[<span style="color:red">DELETED</span>\] `log4j.xml`
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.3.4/pom.xml) 

## Change list &lt;refcodes-properties&gt; (version 1.3.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.3.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.3.4/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`package-info.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.3.4/src/main/java/org/refcodes/configuration/package-info.java) 
* \[<span style="color:green">MODIFIED</span>\] [`ProfilePropertiesProjection.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.3.4/src/main/java/org/refcodes/configuration/ProfilePropertiesProjection.java) (see Javadoc at [`ProfilePropertiesProjection.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties/1.3.4/org/refcodes/configuration/ProfilePropertiesProjection.html))
* \[<span style="color:green">MODIFIED</span>\] [`Properties.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.3.4/src/main/java/org/refcodes/configuration/Properties.java) (see Javadoc at [`Properties.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties/1.3.4/org/refcodes/configuration/Properties.html))
* \[<span style="color:green">MODIFIED</span>\] [`PolyglotPropertiesBuilderTest.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.3.4/src/test/java/org/refcodes/configuration/PolyglotPropertiesBuilderTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`PolyglotPropertiesTest.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.3.4/src/test/java/org/refcodes/configuration/PolyglotPropertiesTest.java) 

## Change list &lt;refcodes-security&gt; (version 1.3.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security/src/refcodes-security-1.3.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security/src/refcodes-security-1.3.4/README.md) 

## Change list &lt;refcodes-security-alt&gt; (version 1.3.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-1.3.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-1.3.4/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-1.3.4/refcodes-security-alt-chaos/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-1.3.4/refcodes-security-alt-chaos/README.md) 

## Change list &lt;refcodes-security-ext&gt; (version 1.3.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-1.3.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-1.3.4/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-1.3.4/refcodes-security-ext-chaos/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-1.3.4/refcodes-security-ext-chaos/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`ChaosProviderImpl.java`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-1.3.4/refcodes-security-ext-chaos/src/main/java/org/refcodes/security/ext/chaos/ChaosProviderImpl.java) (see Javadoc at [`ChaosProviderImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-security-ext-chaos/1.3.4/org/refcodes/security/ext/chaos/ChaosProviderImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`ChaosProviderTest.java`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-1.3.4/refcodes-security-ext-chaos/src/test/java/org/refcodes/security/ext/chaos/ChaosProviderTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-1.3.4/refcodes-security-ext-spring/pom.xml) 

## Change list &lt;refcodes-properties-ext&gt; (version 1.3.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.3.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.3.4/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.3.4/refcodes-properties-ext-cli/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.3.4/refcodes-properties-ext-cli/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.3.4/refcodes-properties-ext-obfuscation/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.3.4/refcodes-properties-ext-obfuscation/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.3.4/refcodes-properties-ext-observer/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.3.4/refcodes-properties-ext-observer/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.3.4/refcodes-properties-ext-runtime/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.3.4/refcodes-properties-ext-runtime/README.md) 

## Change list &lt;refcodes-logger&gt; (version 1.3.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger/src/refcodes-logger-1.3.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger/src/refcodes-logger-1.3.4/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`LoggerField.java`](https://bitbucket.org/refcodes/refcodes-logger/src/refcodes-logger-1.3.4/src/main/java/org/refcodes/logger/LoggerField.java) (see Javadoc at [`LoggerField.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-logger/1.3.4/org/refcodes/logger/LoggerField.html))
* \[<span style="color:green">MODIFIED</span>\] [`LoggerUtility.java`](https://bitbucket.org/refcodes/refcodes-logger/src/refcodes-logger-1.3.4/src/main/java/org/refcodes/logger/LoggerUtility.java) (see Javadoc at [`LoggerUtility.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-logger/1.3.4/org/refcodes/logger/LoggerUtility.html))
* \[<span style="color:green">MODIFIED</span>\] [`package-info.java`](https://bitbucket.org/refcodes/refcodes-logger/src/refcodes-logger-1.3.4/src/main/java/org/refcodes/logger/package-info.java) 
* \[<span style="color:green">MODIFIED</span>\] [`RuntimeLoggerFactoryImpl.java`](https://bitbucket.org/refcodes/refcodes-logger/src/refcodes-logger-1.3.4/src/main/java/org/refcodes/logger/RuntimeLoggerFactoryImpl.java) (see Javadoc at [`RuntimeLoggerFactoryImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-logger/1.3.4/org/refcodes/logger/RuntimeLoggerFactoryImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`RuntimeLoggerImpl.java`](https://bitbucket.org/refcodes/refcodes-logger/src/refcodes-logger-1.3.4/src/main/java/org/refcodes/logger/RuntimeLoggerImpl.java) (see Javadoc at [`RuntimeLoggerImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-logger/1.3.4/org/refcodes/logger/RuntimeLoggerImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`RuntimeLogger.java`](https://bitbucket.org/refcodes/refcodes-logger/src/refcodes-logger-1.3.4/src/main/java/org/refcodes/logger/RuntimeLogger.java) (see Javadoc at [`RuntimeLogger.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-logger/1.3.4/org/refcodes/logger/RuntimeLogger.html))
* \[<span style="color:green">MODIFIED</span>\] [`StatisticalPartedTrimLoggerTest.java`](https://bitbucket.org/refcodes/refcodes-logger/src/refcodes-logger-1.3.4/src/test/java/org/refcodes/logger/StatisticalPartedTrimLoggerTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`runtimelogger.ini`](https://bitbucket.org/refcodes/refcodes-logger/src/refcodes-logger-1.3.4/src/test/resources/runtimelogger.ini) 

## Change list &lt;refcodes-logger-alt&gt; (version 1.3.4)

* \[<span style="color:blue">ADDED</span>\] [`IoLogger.java`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.3.4/refcodes-logger-alt-io/src/main/java/org/refcodes/logger/alt/io/IoLogger.java) (see Javadoc at [`IoLogger.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-logger-alt-io/1.3.4/org/refcodes/logger/alt/io/IoLogger.html))
* \[<span style="color:red">DELETED</span>\] `IoLoggerImpl.java`
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.3.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.3.4/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.3.4/refcodes-logger-alt-async/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.3.4/refcodes-logger-alt-async/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`AsyncLogger.java`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.3.4/refcodes-logger-alt-async/src/main/java/org/refcodes/logger/alt/async/AsyncLogger.java) (see Javadoc at [`AsyncLogger.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-logger-alt-async/1.3.4/org/refcodes/logger/alt/async/AsyncLogger.html))
* \[<span style="color:green">MODIFIED</span>\] [`AsyncRuntimeLoggerTest.java`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.3.4/refcodes-logger-alt-async/src/test/java/org/refcodes/logger/alt/async/AsyncRuntimeLoggerTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`RuntimeLoggerSingletonTest.java`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.3.4/refcodes-logger-alt-async/src/test/java/org/refcodes/logger/alt/async/RuntimeLoggerSingletonTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`runtimelogger.ini`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.3.4/refcodes-logger-alt-async/src/test/resources/runtimelogger.ini) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.3.4/refcodes-logger-alt-console/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.3.4/refcodes-logger-alt-console/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`ConsoleLoggerImpl.java`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.3.4/refcodes-logger-alt-console/src/main/java/org/refcodes/logger/alt/console/ConsoleLoggerImpl.java) (see Javadoc at [`ConsoleLoggerImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-logger-alt-console/1.3.4/org/refcodes/logger/alt/console/ConsoleLoggerImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`FormattedLoggerImpl.java`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.3.4/refcodes-logger-alt-console/src/main/java/org/refcodes/logger/alt/console/FormattedLoggerImpl.java) (see Javadoc at [`FormattedLoggerImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-logger-alt-console/1.3.4/org/refcodes/logger/alt/console/FormattedLoggerImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`RuntimeLoggerSingletonTest.java`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.3.4/refcodes-logger-alt-console/src/test/java/org/refcodes/logger/alt/console/RuntimeLoggerSingletonTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.3.4/refcodes-logger-alt-io/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.3.4/refcodes-logger-alt-io/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`runtimelogger.ini`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.3.4/refcodes-logger-alt-io/src/main/resources/runtimelogger.ini) 
* \[<span style="color:green">MODIFIED</span>\] [`IoRuntimeLoggerTest.java`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.3.4/refcodes-logger-alt-io/src/test/java/org/refcodes/logger/alt/io/IoRuntimeLoggerTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`RuntimeLoggerSingletonTest.java`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.3.4/refcodes-logger-alt-io/src/test/java/org/refcodes/logger/alt/io/RuntimeLoggerSingletonTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`runtimelogger.ini`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.3.4/refcodes-logger-alt-io/src/test/resources/runtimelogger.ini) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.3.4/refcodes-logger-alt-simpledb/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`AbstractSimpleDbLoggerFactory.java`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.3.4/refcodes-logger-alt-simpledb/src/main/java/org/refcodes/logger/alt/simpledb/AbstractSimpleDbLoggerFactory.java) (see Javadoc at [`AbstractSimpleDbLoggerFactory.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-logger-alt-simpledb/1.3.4/org/refcodes/logger/alt/simpledb/AbstractSimpleDbLoggerFactory.html))
* \[<span style="color:green">MODIFIED</span>\] [`package-info.java`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.3.4/refcodes-logger-alt-simpledb/src/main/java/org/refcodes/logger/alt/simpledb/package-info.java) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.3.4/refcodes-logger-alt-slf4j/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`package-info.java`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.3.4/refcodes-logger-alt-slf4j/src/main/java/org/refcodes/logger/alt/slf4j/package-info.java) 
* \[<span style="color:green">MODIFIED</span>\] [`Slf4jRuntimeLoggerFactorySingleton.java`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.3.4/refcodes-logger-alt-slf4j/src/main/java/org/refcodes/logger/alt/slf4j/Slf4jRuntimeLoggerFactorySingleton.java) (see Javadoc at [`Slf4jRuntimeLoggerFactorySingleton.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-logger-alt-slf4j/1.3.4/org/refcodes/logger/alt/slf4j/Slf4jRuntimeLoggerFactorySingleton.html))
* \[<span style="color:green">MODIFIED</span>\] [`runtimelogger.ini`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.3.4/refcodes-logger-alt-slf4j/src/main/resources/runtimelogger.ini) 
* \[<span style="color:green">MODIFIED</span>\] [`RuntimeLoggerSingletonTest.java`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.3.4/refcodes-logger-alt-slf4j/src/test/java/org/refcodes/logger/alt/slf4j/RuntimeLoggerSingletonTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`Slf4jRuntimeLoggerTest.java`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.3.4/refcodes-logger-alt-slf4j/src/test/java/org/refcodes/logger/alt/slf4j/Slf4jRuntimeLoggerTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`runtimelogger.ini`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.3.4/refcodes-logger-alt-slf4j/src/test/resources/runtimelogger.ini) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.3.4/refcodes-logger-alt-spring/pom.xml) 

## Change list &lt;refcodes-logger-ext&gt; (version 1.3.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-1.3.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-1.3.4/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-1.3.4/refcodes-logger-ext-slf4j/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-1.3.4/refcodes-logger-ext-slf4j/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`RuntimeLoggerAdapter.java`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-1.3.4/refcodes-logger-ext-slf4j/src/main/java/org/slf4j/impl/RuntimeLoggerAdapter.java) (see Javadoc at [`RuntimeLoggerAdapter.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-logger-ext-slf4j/1.3.4/org/slf4j/impl/RuntimeLoggerAdapter.html))
* \[<span style="color:green">MODIFIED</span>\] [`runtimelogger.ini`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-1.3.4/refcodes-logger-ext-slf4j/src/main/resources/runtimelogger.ini) 

## Change list &lt;refcodes-graphical-ext&gt; (version 1.3.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-1.3.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-1.3.4/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-1.3.4/refcodes-graphical-ext-javafx/pom.xml) 

## Change list &lt;refcodes-checkerboard&gt; (version 1.3.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-1.3.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-1.3.4/README.md) 

## Change list &lt;refcodes-checkerboard-alt&gt; (version 1.3.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-alt/src/refcodes-checkerboard-alt-1.3.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-alt/src/refcodes-checkerboard-alt-1.3.4/refcodes-checkerboard-alt-javafx/pom.xml) 

## Change list &lt;refcodes-checkerboard-ext&gt; (version 1.3.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-ext/src/refcodes-checkerboard-ext-1.3.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-checkerboard-ext/src/refcodes-checkerboard-ext-1.3.4/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-ext/src/refcodes-checkerboard-ext-1.3.4/refcodes-checkerboard-ext-javafx-boulderdash/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-ext/src/refcodes-checkerboard-ext-1.3.4/refcodes-checkerboard-ext-javafx-chess/pom.xml) 

## Change list &lt;refcodes-boulderdash&gt; (version 1.3.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-boulderdash/src/refcodes-boulderdash-1.3.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-boulderdash/src/refcodes-boulderdash-1.3.4/README.md) 

## Change list &lt;refcodes-net&gt; (version 1.3.4)

* \[<span style="color:blue">ADDED</span>\] [`MediaTypesAccessor.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.3.4/src/main/java/org/refcodes/net/MediaTypesAccessor.java) (see Javadoc at [`MediaTypesAccessor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/1.3.4/org/refcodes/net/MediaTypesAccessor.html))
* \[<span style="color:blue">ADDED</span>\] [`OauthRequestField.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.3.4/src/main/java/org/refcodes/net/OauthRequestField.java) (see Javadoc at [`OauthRequestField.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/1.3.4/org/refcodes/net/OauthRequestField.html))
* \[<span style="color:blue">ADDED</span>\] [`OAuthToken.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.3.4/src/main/java/org/refcodes/net/OAuthToken.java) (see Javadoc at [`OAuthToken.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/1.3.4/org/refcodes/net/OAuthToken.html))
* \[<span style="color:blue">ADDED</span>\] [`MediaTypeFactoriesTest.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.3.4/src/test/java/org/refcodes/net/MediaTypeFactoriesTest.java) 
* \[<span style="color:red">DELETED</span>\] `OauthToken.java`
* \[<span style="color:red">DELETED</span>\] `TextXmlFactory.java`
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.3.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`BasicAuthCredentials.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.3.4/src/main/java/org/refcodes/net/BasicAuthCredentials.java) (see Javadoc at [`BasicAuthCredentials.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/1.3.4/org/refcodes/net/BasicAuthCredentials.html))
* \[<span style="color:green">MODIFIED</span>\] [`BearerAuthCredentialsAccessor.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.3.4/src/main/java/org/refcodes/net/BearerAuthCredentialsAccessor.java) (see Javadoc at [`BearerAuthCredentialsAccessor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/1.3.4/org/refcodes/net/BearerAuthCredentialsAccessor.html))
* \[<span style="color:green">MODIFIED</span>\] [`BearerAuthCredentials.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.3.4/src/main/java/org/refcodes/net/BearerAuthCredentials.java) (see Javadoc at [`BearerAuthCredentials.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/1.3.4/org/refcodes/net/BearerAuthCredentials.html))
* \[<span style="color:green">MODIFIED</span>\] [`HeaderFields.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.3.4/src/main/java/org/refcodes/net/HeaderFields.java) (see Javadoc at [`HeaderFields.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/1.3.4/org/refcodes/net/HeaderFields.html))
* \[<span style="color:green">MODIFIED</span>\] [`HttpBodyMap.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.3.4/src/main/java/org/refcodes/net/HttpBodyMap.java) (see Javadoc at [`HttpBodyMap.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/1.3.4/org/refcodes/net/HttpBodyMap.html))
* \[<span style="color:green">MODIFIED</span>\] [`HttpClientRequestImpl.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.3.4/src/main/java/org/refcodes/net/HttpClientRequestImpl.java) (see Javadoc at [`HttpClientRequestImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/1.3.4/org/refcodes/net/HttpClientRequestImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`HttpServerRequestImpl.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.3.4/src/main/java/org/refcodes/net/HttpServerRequestImpl.java) (see Javadoc at [`HttpServerRequestImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/1.3.4/org/refcodes/net/HttpServerRequestImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`MediaTypeFactory.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.3.4/src/main/java/org/refcodes/net/MediaTypeFactory.java) (see Javadoc at [`MediaTypeFactory.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/1.3.4/org/refcodes/net/MediaTypeFactory.html))
* \[<span style="color:green">MODIFIED</span>\] [`MediaTypeFactoryLookup.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.3.4/src/main/java/org/refcodes/net/MediaTypeFactoryLookup.java) (see Javadoc at [`MediaTypeFactoryLookup.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/1.3.4/org/refcodes/net/MediaTypeFactoryLookup.html))
* \[<span style="color:green">MODIFIED</span>\] [`MediaType.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.3.4/src/main/java/org/refcodes/net/MediaType.java) (see Javadoc at [`MediaType.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/1.3.4/org/refcodes/net/MediaType.html))
* \[<span style="color:green">MODIFIED</span>\] [`OauthField.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.3.4/src/main/java/org/refcodes/net/OauthField.java) (see Javadoc at [`OauthField.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/1.3.4/org/refcodes/net/OauthField.html))
* \[<span style="color:green">MODIFIED</span>\] [`Url.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.3.4/src/main/java/org/refcodes/net/Url.java) (see Javadoc at [`Url.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/1.3.4/org/refcodes/net/Url.html))
* \[<span style="color:green">MODIFIED</span>\] [`OauthTokenTest.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.3.4/src/test/java/org/refcodes/net/OauthTokenTest.java) 

## Change list &lt;refcodes-rest&gt; (version 1.3.4)

* \[<span style="color:blue">ADDED</span>\] [`OauthTokenHandler.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.3.4/src/main/java/org/refcodes/rest/OauthTokenHandler.java) (see Javadoc at [`OauthTokenHandler.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/1.3.4/org/refcodes/rest/OauthTokenHandler.html))
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.3.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.3.4/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`AbstractRestClient.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.3.4/src/main/java/org/refcodes/rest/AbstractRestClient.java) (see Javadoc at [`AbstractRestClient.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/1.3.4/org/refcodes/rest/AbstractRestClient.html))
* \[<span style="color:green">MODIFIED</span>\] [`AbstractRestServer.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.3.4/src/main/java/org/refcodes/rest/AbstractRestServer.java) (see Javadoc at [`AbstractRestServer.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/1.3.4/org/refcodes/rest/AbstractRestServer.html))
* \[<span style="color:green">MODIFIED</span>\] [`HttpRestClientImpl.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.3.4/src/main/java/org/refcodes/rest/HttpRestClientImpl.java) (see Javadoc at [`HttpRestClientImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/1.3.4/org/refcodes/rest/HttpRestClientImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`HttpRestServerImpl.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.3.4/src/main/java/org/refcodes/rest/HttpRestServerImpl.java) (see Javadoc at [`HttpRestServerImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/1.3.4/org/refcodes/rest/HttpRestServerImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`RestClient.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.3.4/src/main/java/org/refcodes/rest/RestClient.java) (see Javadoc at [`RestClient.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/1.3.4/org/refcodes/rest/RestClient.html))
* \[<span style="color:green">MODIFIED</span>\] [`RestRequestClient.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.3.4/src/main/java/org/refcodes/rest/RestRequestClient.java) (see Javadoc at [`RestRequestClient.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/1.3.4/org/refcodes/rest/RestRequestClient.html))
* \[<span style="color:green">MODIFIED</span>\] [`HttpRestClientTest.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.3.4/src/test/java/org/refcodes/rest/HttpRestClientTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`LoopbackRestServerTest.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.3.4/src/test/java/org/refcodes/rest/LoopbackRestServerTest.java) 

## Change list &lt;refcodes-daemon&gt; (version 1.3.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-daemon/src/refcodes-daemon-1.3.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-daemon/src/refcodes-daemon-1.3.4/README.md) 

## Change list &lt;refcodes-eventbus&gt; (version 1.3.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-eventbus/src/refcodes-eventbus-1.3.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-eventbus/src/refcodes-eventbus-1.3.4/README.md) 

## Change list &lt;refcodes-eventbus-ext&gt; (version 1.3.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-1.3.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-1.3.4/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-1.3.4/refcodes-eventbus-ext-application/pom.xml) 

## Change list &lt;refcodes-filesystem&gt; (version 1.3.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-filesystem/src/refcodes-filesystem-1.3.4/pom.xml) 

## Change list &lt;refcodes-filesystem-alt&gt; (version 1.3.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-filesystem-alt/src/refcodes-filesystem-alt-1.3.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-filesystem-alt/src/refcodes-filesystem-alt-1.3.4/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-filesystem-alt/src/refcodes-filesystem-alt-1.3.4/refcodes-filesystem-alt-s3/pom.xml) 

## Change list &lt;refcodes-forwardsecrecy&gt; (version 1.3.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy/src/refcodes-forwardsecrecy-1.3.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy/src/refcodes-forwardsecrecy-1.3.4/README.md) 

## Change list &lt;refcodes-forwardsecrecy-alt&gt; (version 1.3.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-1.3.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-1.3.4/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-1.3.4/refcodes-forwardsecrecy-alt-filesystem/pom.xml) 

## Change list &lt;refcodes-io-ext&gt; (version 1.3.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-1.3.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-1.3.4/refcodes-io-ext-observable/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-1.3.4/refcodes-io-ext-observable/README.md) 

## Change list &lt;refcodes-interceptor&gt; (version 1.3.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-interceptor/src/refcodes-interceptor-1.3.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-interceptor/src/refcodes-interceptor-1.3.4/README.md) 

## Change list &lt;refcodes-jobbus&gt; (version 1.3.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-jobbus/src/refcodes-jobbus-1.3.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-jobbus/src/refcodes-jobbus-1.3.4/README.md) 

## Change list &lt;refcodes-rest-ext&gt; (version 1.3.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-1.3.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-1.3.4/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-1.3.4/refcodes-rest-ext-eureka/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-1.3.4/refcodes-rest-ext-eureka/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`EurekaServerDescriptorTest.java`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-1.3.4/refcodes-rest-ext-eureka/src/test/java/org/refcodes/rest/ext/eureka/EurekaServerDescriptorTest.java) 

## Change list &lt;refcodes-remoting&gt; (version 1.3.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-remoting/src/refcodes-remoting-1.3.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-remoting/src/refcodes-remoting-1.3.4/README.md) 

## Change list &lt;refcodes-remoting-ext&gt; (version 1.3.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-1.3.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-1.3.4/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-1.3.4/refcodes-remoting-ext-observer/pom.xml) 

## Change list &lt;refcodes-servicebus&gt; (version 1.3.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-servicebus/src/refcodes-servicebus-1.3.4/pom.xml) 

## Change list &lt;refcodes-servicebus-alt&gt; (version 1.3.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-servicebus-alt/src/refcodes-servicebus-alt-1.3.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-servicebus-alt/src/refcodes-servicebus-alt-1.3.4/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-servicebus-alt/src/refcodes-servicebus-alt-1.3.4/refcodes-servicebus-alt-spring/pom.xml) 

## Change list &lt;refcodes-tabular-alt&gt; (version 1.3.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-tabular-alt/src/refcodes-tabular-alt-1.3.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-tabular-alt/src/refcodes-tabular-alt-1.3.4/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-tabular-alt/src/refcodes-tabular-alt-1.3.4/refcodes-tabular-alt-forwardsecrecy/pom.xml) 
