# /////////////////////////////////////////////////////////////////////////////
# REFCODES.ORG
# =============================================================================
# This code is copyright (c) by Siegfried Steiner, Munich, Germany and licensed
# under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
# licenses:
# =============================================================================
# GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
# =============================================================================
# Apache License, v2.0 ("http://www.apache.org/licenses/LICENSE-2.0")
# =============================================================================
# Please contact the copyright holding author(s) of the software artifacts in
# question for licensing issues not being coveFG_RED by the above listed licenses,
# also regarding commercial licensing models or regarding the compatibility
# with other open source licenses.
# /////////////////////////////////////////////////////////////////////////////
#!/bin/bash

# ------------------------------------------------------------------------------
# INIT:
# ------------------------------------------------------------------------------

CURRENT_PATH="$(pwd)"
SCRIPT_PATH="$(dirname $0)"
cd "${SCRIPT_PATH}"
SCRIPT_PATH="$(pwd)"
cd "${CURRENT_PATH}"
SCRIPT_DIR="${SCRIPT_PATH##*/}"
SCRIPT_NAME="$(basename $0 .sh)"
PARENT_PATH="$(realpath $(dirname $0)/..)"
PARENT_DIR="${PARENT_PATH##*/}"
MODULE_NAME="$(echo -e "${SCRIPT_DIR}" | cut -d- -f3- )"
if [ -z "${MODULE_NAME}" ]; then
	MODULE_NAME="$(echo -e "${SCRIPT_DIR}" | cut -d- -f2- )"
	if [ -z "${MODULE_NAME}" ]; then
		MODULE_NAME="${SCRIPT_DIR}"
	fi
fi
if [ -z ${COLUMNS} ] ; then
	export COLUMNS=$(tput cols)
fi

DEPLOY_REACTOR="reactor.txt"
MAVEN_SEARCH="https://repo1.maven.org/maven2"
# SONATYPE_SEARCH="https://s01.oss.sonatype.org/service/local/lucene/search"
SONATYPE_SEARCH="https://s01.oss.sonatype.org/service/local/lucene/search"

# ------------------------------------------------------------------------------
# ANSI ESCAPE CODES:
# ------------------------------------------------------------------------------

ESC_BOLD="\E[1m"
ESC_FAINT="\E[2m"
ESC_ITALIC="\E[3m"
ESC_UNDERLINE="\E[4m"
ESC_FG_RED="\E[31m"
ESC_FG_GREEN="\E[32m"
ESC_FG_YELLOW="\E[33m"
ESC_FG_BLUE="\E[34m"
ESC_FG_MAGENTA="\E[35m"
ESC_FG_CYAN="\E[36m"
ESC_FG_WHITE="\E[37m"
ESC_RESET="\E[0m"

# ------------------------------------------------------------------------------
# PRINTLN:
# ------------------------------------------------------------------------------

function printLn {
	char="-"
	if [[ $# == 1 ]] ; then
		char="$1"
	fi
	echo -en "${ESC_FAINT}"
	for (( i=0; i< ${COLUMNS}; i++ )) ; do
		echo -en "${char}"
	done
	echo -e "${ESC_RESET}"
}

# ------------------------------------------------------------------------------
# QUIT:
# ------------------------------------------------------------------------------

function quit {
	input=""
	while ([ "$input" != "q" ] && [ "$input" != "y" ]); do
		echo -ne "> Continue? Enter [${ESC_BOLD}q${ESC_RESET}] to quit, [${ESC_BOLD}y${ESC_RESET}] to continue: ";
		read input;
	done
	# printf '%*s\n' "${COLUMNS:-$(tput cols)}" '' | tr ' ' -
	if [ "$input" == "q" ] ; then
		printLn
		echo -e "> ${ESC_BOLD}Aborting due to user input.${ESC_RESET}"
		exit
	fi
}

# ------------------------------------------------------------------------------
# BANNER:
# ------------------------------------------------------------------------------

function printBanner {
	banner=$( figlet -w 999 "/${MODULE_NAME}:>>>${SCRIPT_NAME}..." 2> /dev/null )
	if [ $? -eq 0 ]; then
		echo "${banner}" | cut -c -${COLUMNS}
	else
		banner "${SCRIPT_NAME}..." 2> /dev/null
		if [ $? -ne 0 ]; then
			echo -e "> ${SCRIPT_NAME}:" | tr a-z A-Z 
		fi
	fi
}

printBanner

# ------------------------------------------------------------------------------
# HELP:
# ------------------------------------------------------------------------------

function printHelp {
	printLn
	echo -e "Usage: ${ESC_BOLD}${SCRIPT_NAME}${ESC_RESET}.sh"
	printLn
	echo -e "Prints the latest artifacts's versions as of the reactor's \"${DEPLOY_REACTOR}\" artefact listing."
}

# ------------------------------------------------------------------------------
# ARGS:
# ------------------------------------------------------------------------------

POSITIONAL_ARGS=()
while [[ $# -gt 0 ]]; do
	case $1 in
		-h|--help)
			printHelp
			exit 0
			;;
		-*|--*)
			printHelp
			echo -e "> Unknown argument <${ESC_BOLD}${ESC_FG_RED}$1${ESC_RESET}>"
			exit 1
			;;
		*)
			POSITIONAL_ARGS+=("$1") # Save positional arg
			shift # Past argument
			;;
	esac
done
set -- "${POSITIONAL_ARGS[@]}"

# ------------------------------------------------------------------------------
# MAIN:
# ------------------------------------------------------------------------------


groupPath=$(cat ${SCRIPT_PATH}/pom.xml | xml2 | grep "/project/groupId=")
groupPath=$(echo "$groupPath" | sed 's/.*=//')
groupPath="${groupPath//\./\/}"
MAVEN_SEARCH="${MAVEN_SEARCH}/${groupPath}"

echo "----------------------------------------+------"
echo -e "${ESC_BOLD}ARTIFACT ID${ESC_RESET} ..................... ${ESC_BOLD}STAGE${ESC_RESET} | ${ESC_BOLD}MAVEN${ESC_RESET}"
echo "----------------------------------------+------"

while read artifact; do
	if [[ $artifact != \#* ]] ; then
		if [ ! -z "${artifact}" ]; then
			release=$(curl -s "${MAVEN_SEARCH}/$artifact/" | grep "<a href=" )
			release=$(echo "$release" | grep -v "maven" | tr -s '\n')
			release=$(echo "$release" | grep -v "\.\." | tr -s '\n')
			release=$(echo "$release" | sed 's/^[^"]*"//')
			release=$(echo "$release"| cut -f1 -d'/' )
			release=$(echo "$release"| sort -r | grep "" -m 1 )
			
			sonatype=$(curl -s "${SONATYPE_SEARCH}?q=$artifact&collapseresults=true" | grep latestRelease -m 1)
			sonatype=$(echo "$sonatype" | sed -n 's/.*<latestRelease>\(.*\)<\/latestRelease>.*/\1/p')


			if [ -z "${release}" ] ; then
				release='?.?.?'
			fi
			if [ -z "${sonatype}" ] ; then
				sonatype=$( echo "${release}" | sed 's/[0-9]/?/g' )
			fi
			
			
			sonatypeAnsi="${sonatype}"
			releaseAnsi="${release}"
			
			if [[ $sonatype = \?* ]] ; then
				sonatypeAnsi="${ESC_RESET}${sonatype}${ESC_RESET}"
			else			
				sonatypeAnsi="${ESC_FG_CYAN}${sonatype}${ESC_RESET}"
			fi
			
			if [[ $release = \?* ]] ; then
				releaseAnsi="${ESC_RESET}${release}${ESC_RESET}"
			elif [ "$sonatype" = "$release" ]; then
				releaseAnsi="${ESC_FG_GREEN}${release}${ESC_RESET}"
			else
				releaseAnsi="${ESC_FG_RED}${release}${ESC_RESET}"
			fi
			
			line=$(printf "%-32s %8b %8b\n" "${artifact}_" "_${sonatypeAnsi}_|" "|_${releaseAnsi}")
			line="${line// /.}"
			line="${line//_/\ }"
			line="${line//|\.|/|}"
			echo "$line"
		fi
	fi
done < "${SCRIPT_PATH}/${DEPLOY_REACTOR}"
