# Change list for REFCODES.ORG artifacts' version 1.1.3

> This change list has been auto-generated on `triton` by `steiner` with with `changelist-all.sh` on the 2017-04-26 at 21:49:25.

## Change list &lt;refcodes-licensing&gt; (version 1.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-licensing/src/refcodes-licensing-1.1.3/pom.xml) 

## Change list &lt;refcodes-parent&gt; (version 1.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-parent/src/refcodes-parent-1.1.3/pom.xml) 

## Change list &lt;refcodes-time&gt; (version 1.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-time/src/refcodes-time-1.1.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`DateFormat.java`](https://bitbucket.org/refcodes/refcodes-time/src/refcodes-time-1.1.3/src/main/java/org/refcodes/time/DateFormat.java) (see Javadoc at [`DateFormat.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-time/1.1.3/org/refcodes/time/DateFormat.html))

## Change list &lt;refcodes-exception&gt; (version 1.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-exception/src/refcodes-exception-1.1.3/pom.xml) 

## Change list &lt;refcodes-mixin&gt; (version 1.1.3)

* \[<span style="color:red">DELETED</span>\] `CorrelationIdAccessor.java`
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-mixin/src/refcodes-mixin-1.1.3/pom.xml) 

## Change list &lt;refcodes-data&gt; (version 1.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data/src/refcodes-data-1.1.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`AsciiColorPalette.java`](https://bitbucket.org/refcodes/refcodes-data/src/refcodes-data-1.1.3/src/main/java/org/refcodes/data/AsciiColorPalette.java) (see Javadoc at [`AsciiColorPalette.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-data/1.1.3/org/refcodes/data/AsciiColorPalette.html))
* \[<span style="color:green">MODIFIED</span>\] [`Delimiter.java`](https://bitbucket.org/refcodes/refcodes-data/src/refcodes-data-1.1.3/src/main/java/org/refcodes/data/Delimiter.java) (see Javadoc at [`Delimiter.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-data/1.1.3/org/refcodes/data/Delimiter.html))
* \[<span style="color:green">MODIFIED</span>\] [`Field.java`](https://bitbucket.org/refcodes/refcodes-data/src/refcodes-data-1.1.3/src/main/java/org/refcodes/data/Field.java) (see Javadoc at [`Field.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-data/1.1.3/org/refcodes/data/Field.html))
* \[<span style="color:green">MODIFIED</span>\] [`Literal.java`](https://bitbucket.org/refcodes/refcodes-data/src/refcodes-data-1.1.3/src/main/java/org/refcodes/data/Literal.java) (see Javadoc at [`Literal.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-data/1.1.3/org/refcodes/data/Literal.html))

## Change list &lt;refcodes-structure&gt; (version 1.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-1.1.3/pom.xml) 

## Change list &lt;refcodes-controlflow&gt; (version 1.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-controlflow/src/refcodes-controlflow-1.1.3/pom.xml) 

## Change list &lt;refcodes-numerical&gt; (version 1.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-numerical/src/refcodes-numerical-1.1.3/pom.xml) 

## Change list &lt;refcodes-generator&gt; (version 1.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-generator/src/refcodes-generator-1.1.3/pom.xml) 

## Change list &lt;refcodes-collection&gt; (version 1.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-collection/src/refcodes-collection-1.1.3/pom.xml) 

## Change list &lt;refcodes-runtime&gt; (version 1.1.3)

* \[<span style="color:blue">ADDED</span>\] [`Correlation.java`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-1.1.3/src/main/java/org/refcodes/runtime/Correlation.java) (see Javadoc at [`Correlation.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-runtime/1.1.3/org/refcodes/runtime/Correlation.html))
* \[<span style="color:red">DELETED</span>\] `ProcessCorrelationImpl.java`
* \[<span style="color:red">DELETED</span>\] `ProcessCorrelationSingleton.java`
* \[<span style="color:red">DELETED</span>\] `ProcessCorrelation.java`
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-1.1.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`CommandLineInterpreter.java`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-1.1.3/src/main/java/org/refcodes/runtime/CommandLineInterpreter.java) (see Javadoc at [`CommandLineInterpreter.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-runtime/1.1.3/org/refcodes/runtime/CommandLineInterpreter.html))
* \[<span style="color:green">MODIFIED</span>\] [`SystemUtility.java`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-1.1.3/src/main/java/org/refcodes/runtime/SystemUtility.java) (see Javadoc at [`SystemUtility.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-runtime/1.1.3/org/refcodes/runtime/SystemUtility.html))

## Change list &lt;refcodes-component&gt; (version 1.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-1.1.3/pom.xml) 

## Change list &lt;refcodes-interceptor&gt; (version 1.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-interceptor/src/refcodes-interceptor-1.1.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-interceptor/src/refcodes-interceptor-1.1.3/README.md) 

## Change list &lt;refcodes-factory&gt; (version 1.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-factory/src/refcodes-factory-1.1.3/pom.xml) 

## Change list &lt;refcodes-data-ext&gt; (version 1.1.3)

* \[<span style="color:blue">ADDED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.1.3/refcodes-data-ext-symbols/pom.xml) 
* \[<span style="color:blue">ADDED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.1.3/refcodes-data-ext-symbols/README.md) 
* \[<span style="color:blue">ADDED</span>\] [`SymbolPixmapInputStreamFactoryImpl.java`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.1.3/refcodes-data-ext-symbols/src/main/java/org/refcodes/data/ext/symbols/SymbolPixmapInputStreamFactoryImpl.java) (see Javadoc at [`SymbolPixmapInputStreamFactoryImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-data-ext-symbols/1.1.3/org/refcodes/data/ext/symbols/SymbolPixmapInputStreamFactoryImpl.html))
* \[<span style="color:blue">ADDED</span>\] [`SymbolPixmapUrlFactoryImpl.java`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.1.3/refcodes-data-ext-symbols/src/main/java/org/refcodes/data/ext/symbols/SymbolPixmapUrlFactoryImpl.java) (see Javadoc at [`SymbolPixmapUrlFactoryImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-data-ext-symbols/1.1.3/org/refcodes/data/ext/symbols/SymbolPixmapUrlFactoryImpl.html))
* \[<span style="color:blue">ADDED</span>\] [`SymbolPixmapInputStreamFactory.java`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.1.3/refcodes-data-ext-symbols/src/main/java/org/refcodes/data/ext/symbols/SymbolPixmapInputStreamFactory.java) (see Javadoc at [`SymbolPixmapInputStreamFactory.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-data-ext-symbols/1.1.3/org/refcodes/data/ext/symbols/SymbolPixmapInputStreamFactory.html))
* \[<span style="color:blue">ADDED</span>\] [`SymbolPixmap.java`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.1.3/refcodes-data-ext-symbols/src/main/java/org/refcodes/data/ext/symbols/SymbolPixmap.java) (see Javadoc at [`SymbolPixmap.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-data-ext-symbols/1.1.3/org/refcodes/data/ext/symbols/SymbolPixmap.html))
* \[<span style="color:blue">ADDED</span>\] [`SymbolPixmapUrlFactory.java`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.1.3/refcodes-data-ext-symbols/src/main/java/org/refcodes/data/ext/symbols/SymbolPixmapUrlFactory.java) (see Javadoc at [`SymbolPixmapUrlFactory.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-data-ext-symbols/1.1.3/org/refcodes/data/ext/symbols/SymbolPixmapUrlFactory.html))
* \[<span style="color:blue">ADDED</span>\] [`symbol-skull.png`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.1.3/refcodes-data-ext-symbols/src/main/resources/org/refcodes/data/ext/symbols/symbol-skull.png) 
* \[<span style="color:blue">ADDED</span>\] [`SymbolsPixmapDataLocatorTest.java`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.1.3/refcodes-data-ext-symbols/src/test/java/org/refcodes/data/ext/symbols/SymbolsPixmapDataLocatorTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.1.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.1.3/refcodes-data-ext-boulderdash/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.1.3/refcodes-data-ext-boulderdash/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`BoulderDashPixmap.java`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.1.3/refcodes-data-ext-boulderdash/src/main/java/org/refcodes/data/ext/boulderdash/BoulderDashPixmap.java) (see Javadoc at [`BoulderDashPixmap.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-data-ext-boulderdash/1.1.3/org/refcodes/data/ext/boulderdash/BoulderDashPixmap.html))
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.1.3/refcodes-data-ext-checkers/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.1.3/refcodes-data-ext-checkers/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`CheckerVectorGraphics.java`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.1.3/refcodes-data-ext-checkers/src/main/java/org/refcodes/data/ext/checkers/CheckerVectorGraphics.java) (see Javadoc at [`CheckerVectorGraphics.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-data-ext-checkers/1.1.3/org/refcodes/data/ext/checkers/CheckerVectorGraphics.html))
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.1.3/refcodes-data-ext-chess/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.1.3/refcodes-data-ext-chess/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`ChessVectorGraphics.java`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.1.3/refcodes-data-ext-chess/src/main/java/org/refcodes/data/ext/chess/ChessVectorGraphics.java) (see Javadoc at [`ChessVectorGraphics.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-data-ext-chess/1.1.3/org/refcodes/data/ext/chess/ChessVectorGraphics.html))
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.1.3/refcodes-data-ext-corporate/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`LogoPixmap.java`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.1.3/refcodes-data-ext-corporate/src/main/java/org/refcodes/data/ext/corporate/LogoPixmap.java) (see Javadoc at [`LogoPixmap.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-data-ext-corporate/1.1.3/org/refcodes/data/ext/corporate/LogoPixmap.html))

## Change list &lt;refcodes-graphical&gt; (version 1.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-graphical/src/refcodes-graphical-1.1.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`Dimension.java`](https://bitbucket.org/refcodes/refcodes-graphical/src/refcodes-graphical-1.1.3/src/main/java/org/refcodes/graphical/Dimension.java) (see Javadoc at [`Dimension.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-graphical/1.1.3/org/refcodes/graphical/Dimension.html))
* \[<span style="color:green">MODIFIED</span>\] [`GraphicalUtility.java`](https://bitbucket.org/refcodes/refcodes-graphical/src/refcodes-graphical-1.1.3/src/main/java/org/refcodes/graphical/GraphicalUtility.java) (see Javadoc at [`GraphicalUtility.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-graphical/1.1.3/org/refcodes/graphical/GraphicalUtility.html))
* \[<span style="color:green">MODIFIED</span>\] [`RgbPixmapImageBuilderImpl.java`](https://bitbucket.org/refcodes/refcodes-graphical/src/refcodes-graphical-1.1.3/src/main/java/org/refcodes/graphical/RgbPixmapImageBuilderImpl.java) (see Javadoc at [`RgbPixmapImageBuilderImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-graphical/1.1.3/org/refcodes/graphical/RgbPixmapImageBuilderImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`PixmapImageBuilder.java`](https://bitbucket.org/refcodes/refcodes-graphical/src/refcodes-graphical-1.1.3/src/main/java/org/refcodes/graphical/PixmapImageBuilder.java) (see Javadoc at [`PixmapImageBuilder.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-graphical/1.1.3/org/refcodes/graphical/PixmapImageBuilder.html))
* \[<span style="color:green">MODIFIED</span>\] [`RgbPixmapImageBuilder.java`](https://bitbucket.org/refcodes/refcodes-graphical/src/refcodes-graphical-1.1.3/src/main/java/org/refcodes/graphical/RgbPixmapImageBuilder.java) (see Javadoc at [`RgbPixmapImageBuilder.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-graphical/1.1.3/org/refcodes/graphical/RgbPixmapImageBuilder.html))

## Change list &lt;refcodes-textual&gt; (version 1.1.3)

* \[<span style="color:blue">ADDED</span>\] [`ColumnFormatMetrics.java`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-1.1.3/src/main/java/org/refcodes/textual/ColumnFormatMetrics.java) (see Javadoc at [`ColumnFormatMetrics.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-textual/1.1.3/org/refcodes/textual/ColumnFormatMetrics.html))
* \[<span style="color:blue">ADDED</span>\] [`ColumnFormatMetricsImpl.java`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-1.1.3/src/main/java/org/refcodes/textual/ColumnFormatMetricsImpl.java) (see Javadoc at [`ColumnFormatMetricsImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-textual/1.1.3/org/refcodes/textual/ColumnFormatMetricsImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-1.1.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`AsciiArtBuilder.java`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-1.1.3/src/main/java/org/refcodes/textual/AsciiArtBuilder.java) (see Javadoc at [`AsciiArtBuilder.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-textual/1.1.3/org/refcodes/textual/AsciiArtBuilder.html))
* \[<span style="color:green">MODIFIED</span>\] [`ColumnWidthMetrics.java`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-1.1.3/src/main/java/org/refcodes/textual/ColumnWidthMetrics.java) (see Javadoc at [`ColumnWidthMetrics.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-textual/1.1.3/org/refcodes/textual/ColumnWidthMetrics.html))
* \[<span style="color:green">MODIFIED</span>\] [`FontStyle.java`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-1.1.3/src/main/java/org/refcodes/textual/FontStyle.java) (see Javadoc at [`FontStyle.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-textual/1.1.3/org/refcodes/textual/FontStyle.html))
* \[<span style="color:green">MODIFIED</span>\] [`AsciiArtBuilderImpl.java`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-1.1.3/src/main/java/org/refcodes/textual/AsciiArtBuilderImpl.java) (see Javadoc at [`AsciiArtBuilderImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-textual/1.1.3/org/refcodes/textual/AsciiArtBuilderImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`ColumnWidthMetricsImpl.java`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-1.1.3/src/main/java/org/refcodes/textual/ColumnWidthMetricsImpl.java) (see Javadoc at [`ColumnWidthMetricsImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-textual/1.1.3/org/refcodes/textual/ColumnWidthMetricsImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`FontImpl.java`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-1.1.3/src/main/java/org/refcodes/textual/FontImpl.java) (see Javadoc at [`FontImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-textual/1.1.3/org/refcodes/textual/FontImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`TableBuilderImpl.java`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-1.1.3/src/main/java/org/refcodes/textual/TableBuilderImpl.java) (see Javadoc at [`TableBuilderImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-textual/1.1.3/org/refcodes/textual/TableBuilderImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`TableBuilder.java`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-1.1.3/src/main/java/org/refcodes/textual/TableBuilder.java) (see Javadoc at [`TableBuilder.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-textual/1.1.3/org/refcodes/textual/TableBuilder.html))

## Change list &lt;refcodes-criteria&gt; (version 1.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-1.1.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-1.1.3/README.md) 

## Change list &lt;refcodes-tabular&gt; (version 1.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-tabular/src/refcodes-tabular-1.1.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`RecordImpl.java`](https://bitbucket.org/refcodes/refcodes-tabular/src/refcodes-tabular-1.1.3/src/main/java/org/refcodes/tabular/RecordImpl.java) (see Javadoc at [`RecordImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-tabular/1.1.3/org/refcodes/tabular/RecordImpl.html))

## Change list &lt;refcodes-logger&gt; (version 1.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger/src/refcodes-logger-1.1.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger/src/refcodes-logger-1.1.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`RuntimeLoggerHeaderImpl.java`](https://bitbucket.org/refcodes/refcodes-logger/src/refcodes-logger-1.1.3/src/main/java/org/refcodes/logger/RuntimeLoggerHeaderImpl.java) (see Javadoc at [`RuntimeLoggerHeaderImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-logger/1.1.3/org/refcodes/logger/RuntimeLoggerHeaderImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`RuntimeLoggerImpl.java`](https://bitbucket.org/refcodes/refcodes-logger/src/refcodes-logger-1.1.3/src/main/java/org/refcodes/logger/RuntimeLoggerImpl.java) (see Javadoc at [`RuntimeLoggerImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-logger/1.1.3/org/refcodes/logger/RuntimeLoggerImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`LoggerField.java`](https://bitbucket.org/refcodes/refcodes-logger/src/refcodes-logger-1.1.3/src/main/java/org/refcodes/logger/LoggerField.java) (see Javadoc at [`LoggerField.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-logger/1.1.3/org/refcodes/logger/LoggerField.html))
* \[<span style="color:green">MODIFIED</span>\] [`RuntimeLoggerSingletonTest.java`](https://bitbucket.org/refcodes/refcodes-logger/src/refcodes-logger-1.1.3/src/test/java/org/refcodes/logger/RuntimeLoggerSingletonTest.java) 

## Change list &lt;refcodes-factory-alt&gt; (version 1.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-factory-alt/src/refcodes-factory-alt-1.1.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-factory-alt/src/refcodes-factory-alt-1.1.3/refcodes-factory-alt-spring/pom.xml) 

## Change list &lt;refcodes-logger-alt&gt; (version 1.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.1.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.1.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.1.3/refcodes-logger-alt-async/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.1.3/refcodes-logger-alt-async/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.1.3/refcodes-logger-alt-console/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.1.3/refcodes-logger-alt-console/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`ConsoleLoggerImpl.java`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.1.3/refcodes-logger-alt-console/src/main/java/org/refcodes/logger/alt/console/ConsoleLoggerImpl.java) (see Javadoc at [`ConsoleLoggerImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-logger-alt-console/1.1.3/org/refcodes/logger/alt/console/ConsoleLoggerImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.1.3/refcodes-logger-alt-io/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.1.3/refcodes-logger-alt-io/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.1.3/refcodes-logger-alt-simpledb/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.1.3/refcodes-logger-alt-slf4j/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`Slf4jLogger.java`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.1.3/refcodes-logger-alt-slf4j/src/main/java/org/refcodes/logger/alt/slf4j/Slf4jLogger.java) (see Javadoc at [`Slf4jLogger.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-logger-alt-slf4j/1.1.3/org/refcodes/logger/alt/slf4j/Slf4jLogger.html))
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.1.3/refcodes-logger-alt-spring/pom.xml) 

## Change list &lt;refcodes-matcher&gt; (version 1.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-matcher/src/refcodes-matcher-1.1.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`PathMatcherImpl.java`](https://bitbucket.org/refcodes/refcodes-matcher/src/refcodes-matcher-1.1.3/src/main/java/org/refcodes/matcher/PathMatcherImpl.java) (see Javadoc at [`PathMatcherImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-matcher/1.1.3/org/refcodes/matcher/PathMatcherImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`PathMatcher.java`](https://bitbucket.org/refcodes/refcodes-matcher/src/refcodes-matcher-1.1.3/src/main/java/org/refcodes/matcher/PathMatcher.java) (see Javadoc at [`PathMatcher.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-matcher/1.1.3/org/refcodes/matcher/PathMatcher.html))
* \[<span style="color:green">MODIFIED</span>\] [`PathMatcherTest.java`](https://bitbucket.org/refcodes/refcodes-matcher/src/refcodes-matcher-1.1.3/src/test/java/org/refcodes/matcher/PathMatcherTest.java) 

## Change list &lt;refcodes-observer&gt; (version 1.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-observer/src/refcodes-observer-1.1.3/pom.xml) 

## Change list &lt;refcodes-checkerboard&gt; (version 1.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-1.1.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-1.1.3/README.md) 

## Change list &lt;refcodes-graphical-ext&gt; (version 1.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-1.1.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-1.1.3/refcodes-graphical-ext-javafx/pom.xml) 

## Change list &lt;refcodes-checkerboard-alt&gt; (version 1.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-alt/src/refcodes-checkerboard-alt-1.1.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-alt/src/refcodes-checkerboard-alt-1.1.3/refcodes-checkerboard-alt-javafx/pom.xml) 

## Change list &lt;refcodes-io&gt; (version 1.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-io/src/refcodes-io-1.1.3/pom.xml) 

## Change list &lt;refcodes-codec&gt; (version 1.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-1.1.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-1.1.3/README.md) 

## Change list &lt;refcodes-command&gt; (version 1.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-command/src/refcodes-command-1.1.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-command/src/refcodes-command-1.1.3/README.md) 

## Change list &lt;refcodes-net&gt; (version 1.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.1.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`HeaderField.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.1.3/src/main/java/org/refcodes/net/HeaderField.java) (see Javadoc at [`HeaderField.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/1.1.3/org/refcodes/net/HeaderField.html))
* \[<span style="color:green">MODIFIED</span>\] [`HttpStatusCode.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.1.3/src/main/java/org/refcodes/net/HttpStatusCode.java) (see Javadoc at [`HttpStatusCode.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/1.1.3/org/refcodes/net/HttpStatusCode.html))
* \[<span style="color:green">MODIFIED</span>\] [`MediaType.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.1.3/src/main/java/org/refcodes/net/MediaType.java) (see Javadoc at [`MediaType.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/1.1.3/org/refcodes/net/MediaType.html))

## Change list &lt;refcodes-rest&gt; (version 1.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.1.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.1.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`HttpRestServerSugar.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.1.3/src/main/java/org/refcodes/rest/HttpRestServerSugar.java) (see Javadoc at [`HttpRestServerSugar.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/1.1.3/org/refcodes/rest/HttpRestServerSugar.html))
* \[<span style="color:green">MODIFIED</span>\] [`AbstractRestServer.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.1.3/src/main/java/org/refcodes/rest/AbstractRestServer.java) (see Javadoc at [`AbstractRestServer.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/1.1.3/org/refcodes/rest/AbstractRestServer.html))
* \[<span style="color:green">MODIFIED</span>\] [`package-info.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.1.3/src/main/java/org/refcodes/rest/package-info.java) (see Javadoc at [`package-info.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/1.1.3/org/refcodes/rest/package-info.html))
* \[<span style="color:green">MODIFIED</span>\] [`RestServer.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.1.3/src/main/java/org/refcodes/rest/RestServer.java) (see Javadoc at [`RestServer.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/1.1.3/org/refcodes/rest/RestServer.html))
* \[<span style="color:green">MODIFIED</span>\] [`HttpRestClientTest.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.1.3/src/test/java/org/refcodes/rest/HttpRestClientTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`HttpRestServerTest.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.1.3/src/test/java/org/refcodes/rest/HttpRestServerTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`HttpRestSugarTest.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.1.3/src/test/java/org/refcodes/rest/HttpRestSugarTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`HttpsRestServerTest.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.1.3/src/test/java/org/refcodes/rest/HttpsRestServerTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`LoopbackRestServerTest.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.1.3/src/test/java/org/refcodes/rest/LoopbackRestServerTest.java) 

## Change list &lt;refcodes-component-ext&gt; (version 1.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.1.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.1.3/refcodes-component-ext-observer/pom.xml) 

## Change list &lt;refcodes-cli&gt; (version 1.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-1.1.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-1.1.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`ConsoleUtility.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-1.1.3/src/main/java/org/refcodes/console/ConsoleUtility.java) (see Javadoc at [`ConsoleUtility.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/1.1.3/org/refcodes/console/ConsoleUtility.html))
* \[<span style="color:green">MODIFIED</span>\] [`AndConditionImpl.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-1.1.3/src/main/java/org/refcodes/console/AndConditionImpl.java) (see Javadoc at [`AndConditionImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/1.1.3/org/refcodes/console/AndConditionImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`OrConditionImpl.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-1.1.3/src/main/java/org/refcodes/console/OrConditionImpl.java) (see Javadoc at [`OrConditionImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/1.1.3/org/refcodes/console/OrConditionImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`XorConditionImpl.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-1.1.3/src/main/java/org/refcodes/console/XorConditionImpl.java) (see Javadoc at [`XorConditionImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/1.1.3/org/refcodes/console/XorConditionImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`Operand.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-1.1.3/src/main/java/org/refcodes/console/Operand.java) (see Javadoc at [`Operand.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/1.1.3/org/refcodes/console/Operand.html))

## Change list &lt;refcodes-security&gt; (version 1.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security/src/refcodes-security-1.1.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security/src/refcodes-security-1.1.3/README.md) 

## Change list &lt;refcodes-forwardsecrecy&gt; (version 1.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy/src/refcodes-forwardsecrecy-1.1.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy/src/refcodes-forwardsecrecy-1.1.3/README.md) 

## Change list &lt;refcodes-filesystem&gt; (version 1.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-filesystem/src/refcodes-filesystem-1.1.3/pom.xml) 

## Change list &lt;refcodes-forwardsecrecy-alt&gt; (version 1.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-1.1.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-1.1.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-1.1.3/refcodes-forwardsecrecy-alt-filesystem/pom.xml) 

## Change list &lt;refcodes-eventbus&gt; (version 1.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-eventbus/src/refcodes-eventbus-1.1.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-eventbus/src/refcodes-eventbus-1.1.3/README.md) 

## Change list &lt;refcodes-filesystem-alt&gt; (version 1.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-filesystem-alt/src/refcodes-filesystem-alt-1.1.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-filesystem-alt/src/refcodes-filesystem-alt-1.1.3/refcodes-filesystem-alt-s3/pom.xml) 

## Change list &lt;refcodes-io-ext&gt; (version 1.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-1.1.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-1.1.3/refcodes-io-ext-observable/pom.xml) 

## Change list &lt;refcodes-jobbus&gt; (version 1.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-jobbus/src/refcodes-jobbus-1.1.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-jobbus/src/refcodes-jobbus-1.1.3/README.md) 

## Change list &lt;refcodes-logger-ext&gt; (version 1.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-1.1.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-1.1.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-1.1.3/refcodes-logger-ext-slf4j/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-1.1.3/refcodes-logger-ext-slf4j/README.md) 

## Change list &lt;refcodes-remoting&gt; (version 1.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-remoting/src/refcodes-remoting-1.1.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-remoting/src/refcodes-remoting-1.1.3/README.md) 

## Change list &lt;refcodes-remoting-ext&gt; (version 1.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-1.1.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-1.1.3/refcodes-remoting-ext-observer/pom.xml) 

## Change list &lt;refcodes-security-alt&gt; (version 1.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-1.1.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-1.1.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-1.1.3/refcodes-security-alt-chaos/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-1.1.3/refcodes-security-alt-chaos/README.md) 

## Change list &lt;refcodes-security-ext&gt; (version 1.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-1.1.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-1.1.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-1.1.3/refcodes-security-ext-chaos/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-1.1.3/refcodes-security-ext-chaos/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-1.1.3/refcodes-security-ext-spring/pom.xml) 

## Change list &lt;refcodes-servicebus&gt; (version 1.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-servicebus/src/refcodes-servicebus-1.1.3/pom.xml) 

## Change list &lt;refcodes-servicebus-alt&gt; (version 1.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-servicebus-alt/src/refcodes-servicebus-alt-1.1.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-servicebus-alt/src/refcodes-servicebus-alt-1.1.3/refcodes-servicebus-alt-spring/pom.xml) 

## Change list &lt;refcodes-tabular-alt&gt; (version 1.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-tabular-alt/src/refcodes-tabular-alt-1.1.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-tabular-alt/src/refcodes-tabular-alt-1.1.3/refcodes-tabular-alt-forwardsecrecy/pom.xml) 
