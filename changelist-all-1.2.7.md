# Change list for REFCODES.ORG artifacts' version 1.2.7

> This change list has been auto-generated on `triton` by `steiner` with with `changelist-all.sh` on the 2018-05-19 at 16:53:28.

## Change list &lt;refcodes-licensing&gt; (version 1.2.7)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-licensing/src/refcodes-licensing-1.2.7/pom.xml) 

## Change list &lt;refcodes-parent&gt; (version 1.2.7)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-parent/src/refcodes-parent-1.2.7/pom.xml) 

## Change list &lt;refcodes-time&gt; (version 1.2.7)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-time/src/refcodes-time-1.2.7/pom.xml) 

## Change list &lt;refcodes-mixin&gt; (version 1.2.7)

* \[<span style="color:blue">ADDED</span>\] [`DecryptPrefixAccessor.java`](https://bitbucket.org/refcodes/refcodes-mixin/src/refcodes-mixin-1.2.7/src/main/java/org/refcodes/mixin/DecryptPrefixAccessor.java) (see Javadoc at [`DecryptPrefixAccessor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-mixin/1.2.7/org/refcodes/mixin/DecryptPrefixAccessor.html))
* \[<span style="color:blue">ADDED</span>\] [`EncryptPrefixAccessor.java`](https://bitbucket.org/refcodes/refcodes-mixin/src/refcodes-mixin-1.2.7/src/main/java/org/refcodes/mixin/EncryptPrefixAccessor.java) (see Javadoc at [`EncryptPrefixAccessor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-mixin/1.2.7/org/refcodes/mixin/EncryptPrefixAccessor.html))
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-mixin/src/refcodes-mixin-1.2.7/pom.xml) 

## Change list &lt;refcodes-data&gt; (version 1.2.7)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data/src/refcodes-data-1.2.7/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`Prefix.java`](https://bitbucket.org/refcodes/refcodes-data/src/refcodes-data-1.2.7/src/main/java/org/refcodes/data/Prefix.java) (see Javadoc at [`Prefix.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-data/1.2.7/org/refcodes/data/Prefix.html))
* \[<span style="color:green">MODIFIED</span>\] [`SystemProperty.java`](https://bitbucket.org/refcodes/refcodes-data/src/refcodes-data-1.2.7/src/main/java/org/refcodes/data/SystemProperty.java) (see Javadoc at [`SystemProperty.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-data/1.2.7/org/refcodes/data/SystemProperty.html))

## Change list &lt;refcodes-exception&gt; (version 1.2.7)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-exception/src/refcodes-exception-1.2.7/pom.xml) 

## Change list &lt;refcodes-factory&gt; (version 1.2.7)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-factory/src/refcodes-factory-1.2.7/pom.xml) 

## Change list &lt;refcodes-factory-alt&gt; (version 1.2.7)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-factory-alt/src/refcodes-factory-alt-1.2.7/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-factory-alt/src/refcodes-factory-alt-1.2.7/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-factory-alt/src/refcodes-factory-alt-1.2.7/refcodes-factory-alt-spring/pom.xml) 

## Change list &lt;refcodes-controlflow&gt; (version 1.2.7)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-controlflow/src/refcodes-controlflow-1.2.7/pom.xml) 

## Change list &lt;refcodes-numerical&gt; (version 1.2.7)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-numerical/src/refcodes-numerical-1.2.7/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`NumericalUtility.java`](https://bitbucket.org/refcodes/refcodes-numerical/src/refcodes-numerical-1.2.7/src/main/java/org/refcodes/numerical/NumericalUtility.java) (see Javadoc at [`NumericalUtility.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-numerical/1.2.7/org/refcodes/numerical/NumericalUtility.html))

## Change list &lt;refcodes-generator&gt; (version 1.2.7)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-generator/src/refcodes-generator-1.2.7/pom.xml) 

## Change list &lt;refcodes-structure&gt; (version 1.2.7)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-1.2.7/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-1.2.7/pom.xml) 

## Change list &lt;refcodes-runtime&gt; (version 1.2.7)

* \[<span style="color:blue">ADDED</span>\] [`SystemContext.java`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-1.2.7/src/main/java/org/refcodes/runtime/SystemContext.java) (see Javadoc at [`SystemContext.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-runtime/1.2.7/org/refcodes/runtime/SystemContext.html))
* \[<span style="color:blue">ADDED</span>\] [`SystemContextTest.java`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-1.2.7/src/test/java/org/refcodes/runtime/SystemContextTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-1.2.7/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`EnvironmentUtility.java`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-1.2.7/src/main/java/org/refcodes/runtime/EnvironmentUtility.java) (see Javadoc at [`EnvironmentUtility.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-runtime/1.2.7/org/refcodes/runtime/EnvironmentUtility.html))
* \[<span style="color:green">MODIFIED</span>\] [`SystemUtility.java`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-1.2.7/src/main/java/org/refcodes/runtime/SystemUtility.java) (see Javadoc at [`SystemUtility.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-runtime/1.2.7/org/refcodes/runtime/SystemUtility.html))
* \[<span style="color:green">MODIFIED</span>\] [`Terminal.java`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-1.2.7/src/main/java/org/refcodes/runtime/Terminal.java) (see Javadoc at [`Terminal.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-runtime/1.2.7/org/refcodes/runtime/Terminal.html))
* \[<span style="color:green">MODIFIED</span>\] [`SystemUtilityTest.java`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-1.2.7/src/test/java/org/refcodes/runtime/SystemUtilityTest.java) 

## Change list &lt;refcodes-component&gt; (version 1.2.7)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-1.2.7/pom.xml) 

## Change list &lt;refcodes-data-ext&gt; (version 1.2.7)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.2.7/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.2.7/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.2.7/refcodes-data-ext-boulderdash/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.2.7/refcodes-data-ext-checkers/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.2.7/refcodes-data-ext-checkers/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.2.7/refcodes-data-ext-chess/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.2.7/refcodes-data-ext-chess/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.2.7/refcodes-data-ext-corporate/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.2.7/refcodes-data-ext-symbols/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.2.7/refcodes-data-ext-symbols/pom.xml) 

## Change list &lt;refcodes-graphical&gt; (version 1.2.7)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-graphical/src/refcodes-graphical-1.2.7/pom.xml) 

## Change list &lt;refcodes-textual&gt; (version 1.2.7)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-1.2.7/pom.xml) 

## Change list &lt;refcodes-criteria&gt; (version 1.2.7)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-1.2.7/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-1.2.7/pom.xml) 

## Change list &lt;refcodes-tabular&gt; (version 1.2.7)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-tabular/src/refcodes-tabular-1.2.7/pom.xml) 

## Change list &lt;refcodes-logger&gt; (version 1.2.7)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger/src/refcodes-logger-1.2.7/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger/src/refcodes-logger-1.2.7/pom.xml) 

## Change list &lt;refcodes-logger-alt&gt; (version 1.2.7)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.2.7/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.2.7/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.2.7/refcodes-logger-alt-async/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.2.7/refcodes-logger-alt-async/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.2.7/refcodes-logger-alt-console/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.2.7/refcodes-logger-alt-console/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`ConsoleLoggerImpl.java`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.2.7/refcodes-logger-alt-console/src/main/java/org/refcodes/logger/alt/console/ConsoleLoggerImpl.java) (see Javadoc at [`ConsoleLoggerImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-logger-alt-console/1.2.7/org/refcodes/logger/alt/console/ConsoleLoggerImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.2.7/refcodes-logger-alt-io/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.2.7/refcodes-logger-alt-io/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.2.7/refcodes-logger-alt-simpledb/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.2.7/refcodes-logger-alt-slf4j/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.2.7/refcodes-logger-alt-spring/pom.xml) 

## Change list &lt;refcodes-graphical-ext&gt; (version 1.2.7)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-1.2.7/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-1.2.7/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-1.2.7/refcodes-graphical-ext-javafx/pom.xml) 

## Change list &lt;refcodes-matcher&gt; (version 1.2.7)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-matcher/src/refcodes-matcher-1.2.7/pom.xml) 

## Change list &lt;refcodes-observer&gt; (version 1.2.7)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-observer/src/refcodes-observer-1.2.7/pom.xml) 

## Change list &lt;refcodes-checkerboard&gt; (version 1.2.7)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-1.2.7/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-1.2.7/pom.xml) 

## Change list &lt;refcodes-checkerboard-alt&gt; (version 1.2.7)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-alt/src/refcodes-checkerboard-alt-1.2.7/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-alt/src/refcodes-checkerboard-alt-1.2.7/refcodes-checkerboard-alt-javafx/pom.xml) 

## Change list &lt;refcodes-checkerboard-ext&gt; (version 1.2.7)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-checkerboard-ext/src/refcodes-checkerboard-ext-1.2.7/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-ext/src/refcodes-checkerboard-ext-1.2.7/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-ext/src/refcodes-checkerboard-ext-1.2.7/refcodes-checkerboard-ext-javafx-boulderdash/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-ext/src/refcodes-checkerboard-ext-1.2.7/refcodes-checkerboard-ext-javafx-chess/pom.xml) 

## Change list &lt;refcodes-command&gt; (version 1.2.7)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-command/src/refcodes-command-1.2.7/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-command/src/refcodes-command-1.2.7/pom.xml) 

## Change list &lt;refcodes-cli&gt; (version 1.2.7)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-1.2.7/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-1.2.7/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`ArgsParserMixin.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-1.2.7/src/main/java/org/refcodes/console/ArgsParserMixin.java) (see Javadoc at [`ArgsParserMixin.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/1.2.7/org/refcodes/console/ArgsParserMixin.html))

## Change list &lt;refcodes-boulderdash&gt; (version 1.2.7)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-boulderdash/src/refcodes-boulderdash-1.2.7/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-boulderdash/src/refcodes-boulderdash-1.2.7/pom.xml) 

## Change list &lt;refcodes-io&gt; (version 1.2.7)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-io/src/refcodes-io-1.2.7/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-io/src/refcodes-io-1.2.7/pom.xml) 

## Change list &lt;refcodes-codec&gt; (version 1.2.7)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-1.2.7/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-1.2.7/pom.xml) 

## Change list &lt;refcodes-component-ext&gt; (version 1.2.7)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.2.7/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.2.7/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.2.7/refcodes-component-ext-observer/pom.xml) 

## Change list &lt;refcodes-properties&gt; (version 1.2.7)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.2.7/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.2.7/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`AbstractPropertiesBuilderDecorator.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.2.7/src/main/java/org/refcodes/configuration/AbstractPropertiesBuilderDecorator.java) (see Javadoc at [`AbstractPropertiesBuilderDecorator.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties/1.2.7/org/refcodes/configuration/AbstractPropertiesBuilderDecorator.html))
* \[<span style="color:green">MODIFIED</span>\] [`AbstractPropertiesDecorator.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.2.7/src/main/java/org/refcodes/configuration/AbstractPropertiesDecorator.java) (see Javadoc at [`AbstractPropertiesDecorator.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties/1.2.7/org/refcodes/configuration/AbstractPropertiesDecorator.html))
* \[<span style="color:green">MODIFIED</span>\] [`AbstractResourcePropertiesBuilderDecorator.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.2.7/src/main/java/org/refcodes/configuration/AbstractResourcePropertiesBuilderDecorator.java) (see Javadoc at [`AbstractResourcePropertiesBuilderDecorator.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties/1.2.7/org/refcodes/configuration/AbstractResourcePropertiesBuilderDecorator.html))
* \[<span style="color:green">MODIFIED</span>\] [`AbstractResourcePropertiesDecorator.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.2.7/src/main/java/org/refcodes/configuration/AbstractResourcePropertiesDecorator.java) (see Javadoc at [`AbstractResourcePropertiesDecorator.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties/1.2.7/org/refcodes/configuration/AbstractResourcePropertiesDecorator.html))
* \[<span style="color:green">MODIFIED</span>\] [`PropertiesPrecedenceComposite.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.2.7/src/main/java/org/refcodes/configuration/PropertiesPrecedenceComposite.java) (see Javadoc at [`PropertiesPrecedenceComposite.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties/1.2.7/org/refcodes/configuration/PropertiesPrecedenceComposite.html))
* \[<span style="color:green">MODIFIED</span>\] [`ResourceProperties.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.2.7/src/main/java/org/refcodes/configuration/ResourceProperties.java) (see Javadoc at [`ResourceProperties.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties/1.2.7/org/refcodes/configuration/ResourceProperties.html))
* \[<span style="color:green">MODIFIED</span>\] [`package-info.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.2.7/src/main/java/org/refcodes/configuration/package-info.java) 

## Change list &lt;refcodes-security&gt; (version 1.2.7)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security/src/refcodes-security-1.2.7/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security/src/refcodes-security-1.2.7/pom.xml) 

## Change list &lt;refcodes-net&gt; (version 1.2.7)

* \[<span style="color:blue">ADDED</span>\] [`BasicAuthCredentialsBuilderImpl.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.2.7/src/main/java/org/refcodes/net/BasicAuthCredentialsBuilderImpl.java) (see Javadoc at [`BasicAuthCredentialsBuilderImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/1.2.7/org/refcodes/net/BasicAuthCredentialsBuilderImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.2.7/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`BasicAuthCredentials.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.2.7/src/main/java/org/refcodes/net/BasicAuthCredentials.java) (see Javadoc at [`BasicAuthCredentials.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/1.2.7/org/refcodes/net/BasicAuthCredentials.html))
* \[<span style="color:green">MODIFIED</span>\] [`BasicAuthCredentialsImpl.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.2.7/src/main/java/org/refcodes/net/BasicAuthCredentialsImpl.java) (see Javadoc at [`BasicAuthCredentialsImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/1.2.7/org/refcodes/net/BasicAuthCredentialsImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`IpAddress.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.2.7/src/main/java/org/refcodes/net/IpAddress.java) (see Javadoc at [`IpAddress.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/1.2.7/org/refcodes/net/IpAddress.html))
* \[<span style="color:green">MODIFIED</span>\] [`Proxy.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.2.7/src/main/java/org/refcodes/net/Proxy.java) (see Javadoc at [`Proxy.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/1.2.7/org/refcodes/net/Proxy.html))
* \[<span style="color:green">MODIFIED</span>\] [`refcodes-net`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.2.7/src/main/java/org/refcodes/net/refcodes-net) 

## Change list &lt;refcodes-rest&gt; (version 1.2.7)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.2.7/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.2.7/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`RestDeleteClientSugar.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.2.7/src/main/java/org/refcodes/rest/RestDeleteClientSugar.java) (see Javadoc at [`RestDeleteClientSugar.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/1.2.7/org/refcodes/rest/RestDeleteClientSugar.html))
* \[<span style="color:green">MODIFIED</span>\] [`RestGetClientSugar.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.2.7/src/main/java/org/refcodes/rest/RestGetClientSugar.java) (see Javadoc at [`RestGetClientSugar.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/1.2.7/org/refcodes/rest/RestGetClientSugar.html))
* \[<span style="color:green">MODIFIED</span>\] [`RestPostClientSugar.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.2.7/src/main/java/org/refcodes/rest/RestPostClientSugar.java) (see Javadoc at [`RestPostClientSugar.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/1.2.7/org/refcodes/rest/RestPostClientSugar.html))
* \[<span style="color:green">MODIFIED</span>\] [`RestPutClientSugar.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.2.7/src/main/java/org/refcodes/rest/RestPutClientSugar.java) (see Javadoc at [`RestPutClientSugar.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/1.2.7/org/refcodes/rest/RestPutClientSugar.html))

## Change list &lt;refcodes-logger-ext&gt; (version 1.2.7)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-1.2.7/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-1.2.7/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-1.2.7/refcodes-logger-ext-slf4j/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-1.2.7/refcodes-logger-ext-slf4j/pom.xml) 

## Change list &lt;refcodes-daemon&gt; (version 1.2.7)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-daemon/src/refcodes-daemon-1.2.7/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-daemon/src/refcodes-daemon-1.2.7/pom.xml) 

## Change list &lt;refcodes-eventbus&gt; (version 1.2.7)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-eventbus/src/refcodes-eventbus-1.2.7/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-eventbus/src/refcodes-eventbus-1.2.7/pom.xml) 

## Change list &lt;refcodes-eventbus-ext&gt; (version 1.2.7)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-1.2.7/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-1.2.7/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-1.2.7/refcodes-eventbus-ext-application/pom.xml) 

## Change list &lt;refcodes-filesystem&gt; (version 1.2.7)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-filesystem/src/refcodes-filesystem-1.2.7/pom.xml) 

## Change list &lt;refcodes-filesystem-alt&gt; (version 1.2.7)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-filesystem-alt/src/refcodes-filesystem-alt-1.2.7/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-filesystem-alt/src/refcodes-filesystem-alt-1.2.7/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-filesystem-alt/src/refcodes-filesystem-alt-1.2.7/refcodes-filesystem-alt-s3/pom.xml) 

## Change list &lt;refcodes-forwardsecrecy&gt; (version 1.2.7)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy/src/refcodes-forwardsecrecy-1.2.7/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy/src/refcodes-forwardsecrecy-1.2.7/pom.xml) 

## Change list &lt;refcodes-forwardsecrecy-alt&gt; (version 1.2.7)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-1.2.7/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-1.2.7/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-1.2.7/refcodes-forwardsecrecy-alt-filesystem/pom.xml) 

## Change list &lt;refcodes-io-ext&gt; (version 1.2.7)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-1.2.7/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-1.2.7/refcodes-io-ext-observable/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-1.2.7/refcodes-io-ext-observable/pom.xml) 

## Change list &lt;refcodes-interceptor&gt; (version 1.2.7)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-interceptor/src/refcodes-interceptor-1.2.7/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-interceptor/src/refcodes-interceptor-1.2.7/pom.xml) 

## Change list &lt;refcodes-jobbus&gt; (version 1.2.7)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-jobbus/src/refcodes-jobbus-1.2.7/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-jobbus/src/refcodes-jobbus-1.2.7/pom.xml) 

## Change list &lt;refcodes-rest-ext&gt; (version 1.2.7)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-1.2.7/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-1.2.7/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-1.2.7/refcodes-rest-ext-eureka/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-1.2.7/refcodes-rest-ext-eureka/pom.xml) 

## Change list &lt;refcodes-remoting&gt; (version 1.2.7)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-remoting/src/refcodes-remoting-1.2.7/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-remoting/src/refcodes-remoting-1.2.7/pom.xml) 

## Change list &lt;refcodes-remoting-ext&gt; (version 1.2.7)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-1.2.7/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-1.2.7/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-1.2.7/refcodes-remoting-ext-observer/pom.xml) 

## Change list &lt;refcodes-security-alt&gt; (version 1.2.7)

* \[<span style="color:blue">ADDED</span>\] [`ChaosKeyImpl.java`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-1.2.7/refcodes-security-alt-chaos/src/main/java/org/refcodes/security/alt/chaos/ChaosKeyImpl.java) (see Javadoc at [`ChaosKeyImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-security-alt-chaos/1.2.7/org/refcodes/security/alt/chaos/ChaosKeyImpl.html))
* \[<span style="color:blue">ADDED</span>\] [`ChaosKeyTest.java`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-1.2.7/refcodes-security-alt-chaos/src/test/java/org/refcodes/security/alt/chaos/ChaosKeyTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-1.2.7/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-1.2.7/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-1.2.7/refcodes-security-alt-chaos/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-1.2.7/refcodes-security-alt-chaos/pom.xml) 

## Change list &lt;refcodes-security-ext&gt; (version 1.2.7)

* \[<span style="color:red">DELETED</span>\] `ChaosKeyImpl.java`
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-1.2.7/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-1.2.7/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-1.2.7/refcodes-security-ext-chaos/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-1.2.7/refcodes-security-ext-chaos/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`ChaosCipherImpl.java`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-1.2.7/refcodes-security-ext-chaos/src/main/java/org/refcodes/security/ext/chaos/ChaosCipherImpl.java) (see Javadoc at [`ChaosCipherImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-security-ext-chaos/1.2.7/org/refcodes/security/ext/chaos/ChaosCipherImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`ChaosKeyGeneratorImpl.java`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-1.2.7/refcodes-security-ext-chaos/src/main/java/org/refcodes/security/ext/chaos/ChaosKeyGeneratorImpl.java) (see Javadoc at [`ChaosKeyGeneratorImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-security-ext-chaos/1.2.7/org/refcodes/security/ext/chaos/ChaosKeyGeneratorImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`ChaosProviderImpl.java`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-1.2.7/refcodes-security-ext-chaos/src/main/java/org/refcodes/security/ext/chaos/ChaosProviderImpl.java) (see Javadoc at [`ChaosProviderImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-security-ext-chaos/1.2.7/org/refcodes/security/ext/chaos/ChaosProviderImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`ChaosProviderTest.java`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-1.2.7/refcodes-security-ext-chaos/src/test/java/org/refcodes/security/ext/chaos/ChaosProviderTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-1.2.7/refcodes-security-ext-spring/pom.xml) 

## Change list &lt;refcodes-properties-ext&gt; (version 1.2.7)

* \[<span style="color:blue">ADDED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.2.7/refcodes-properties-ext-obfuscation/README.md) 
* \[<span style="color:blue">ADDED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.2.7/refcodes-properties-ext-obfuscation/pom.xml) 
* \[<span style="color:blue">ADDED</span>\] [`AbstractObfuscationPropertiesBuilderDecorator.java`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.2.7/refcodes-properties-ext-obfuscation/src/main/java/org/refcodes/configuration/ext/obfuscation/AbstractObfuscationPropertiesBuilderDecorator.java) (see Javadoc at [`AbstractObfuscationPropertiesBuilderDecorator.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties-ext-obfuscation/1.2.7/org/refcodes/configuration/ext/obfuscation/AbstractObfuscationPropertiesBuilderDecorator.html))
* \[<span style="color:blue">ADDED</span>\] [`AbstractObfuscationPropertiesDecorator.java`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.2.7/refcodes-properties-ext-obfuscation/src/main/java/org/refcodes/configuration/ext/obfuscation/AbstractObfuscationPropertiesDecorator.java) (see Javadoc at [`AbstractObfuscationPropertiesDecorator.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties-ext-obfuscation/1.2.7/org/refcodes/configuration/ext/obfuscation/AbstractObfuscationPropertiesDecorator.html))
* \[<span style="color:blue">ADDED</span>\] [`AbstractObfuscationResourcePropertiesBuilderDecorator.java`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.2.7/refcodes-properties-ext-obfuscation/src/main/java/org/refcodes/configuration/ext/obfuscation/AbstractObfuscationResourcePropertiesBuilderDecorator.java) (see Javadoc at [`AbstractObfuscationResourcePropertiesBuilderDecorator.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties-ext-obfuscation/1.2.7/org/refcodes/configuration/ext/obfuscation/AbstractObfuscationResourcePropertiesBuilderDecorator.html))
* \[<span style="color:blue">ADDED</span>\] [`ObfuscationProperties.java`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.2.7/refcodes-properties-ext-obfuscation/src/main/java/org/refcodes/configuration/ext/obfuscation/ObfuscationProperties.java) (see Javadoc at [`ObfuscationProperties.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties-ext-obfuscation/1.2.7/org/refcodes/configuration/ext/obfuscation/ObfuscationProperties.html))
* \[<span style="color:blue">ADDED</span>\] [`ObfuscationPropertiesBuilderDecorator.java`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.2.7/refcodes-properties-ext-obfuscation/src/main/java/org/refcodes/configuration/ext/obfuscation/ObfuscationPropertiesBuilderDecorator.java) (see Javadoc at [`ObfuscationPropertiesBuilderDecorator.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties-ext-obfuscation/1.2.7/org/refcodes/configuration/ext/obfuscation/ObfuscationPropertiesBuilderDecorator.html))
* \[<span style="color:blue">ADDED</span>\] [`ObfuscationPropertiesDecorator.java`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.2.7/refcodes-properties-ext-obfuscation/src/main/java/org/refcodes/configuration/ext/obfuscation/ObfuscationPropertiesDecorator.java) (see Javadoc at [`ObfuscationPropertiesDecorator.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties-ext-obfuscation/1.2.7/org/refcodes/configuration/ext/obfuscation/ObfuscationPropertiesDecorator.html))
* \[<span style="color:blue">ADDED</span>\] [`ObfuscationPropertiesSugar.java`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.2.7/refcodes-properties-ext-obfuscation/src/main/java/org/refcodes/configuration/ext/obfuscation/ObfuscationPropertiesSugar.java) (see Javadoc at [`ObfuscationPropertiesSugar.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties-ext-obfuscation/1.2.7/org/refcodes/configuration/ext/obfuscation/ObfuscationPropertiesSugar.html))
* \[<span style="color:blue">ADDED</span>\] [`ObfuscationResourceProperties.java`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.2.7/refcodes-properties-ext-obfuscation/src/main/java/org/refcodes/configuration/ext/obfuscation/ObfuscationResourceProperties.java) (see Javadoc at [`ObfuscationResourceProperties.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties-ext-obfuscation/1.2.7/org/refcodes/configuration/ext/obfuscation/ObfuscationResourceProperties.html))
* \[<span style="color:blue">ADDED</span>\] [`ObfuscationResourcePropertiesBuilderDecorator.java`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.2.7/refcodes-properties-ext-obfuscation/src/main/java/org/refcodes/configuration/ext/obfuscation/ObfuscationResourcePropertiesBuilderDecorator.java) (see Javadoc at [`ObfuscationResourcePropertiesBuilderDecorator.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties-ext-obfuscation/1.2.7/org/refcodes/configuration/ext/obfuscation/ObfuscationResourcePropertiesBuilderDecorator.html))
* \[<span style="color:blue">ADDED</span>\] [`ObfuscationPropertiesBuilderTest.java`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.2.7/refcodes-properties-ext-obfuscation/src/test/java/org/refcodes/configuration/ext/obfuscation/ObfuscationPropertiesBuilderTest.java) 
* \[<span style="color:blue">ADDED</span>\] [`ObfuscationPropertiesSugarTest.java`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.2.7/refcodes-properties-ext-obfuscation/src/test/java/org/refcodes/configuration/ext/obfuscation/ObfuscationPropertiesSugarTest.java) 
* \[<span style="color:blue">ADDED</span>\] [`ObfuscationPropertiesTest.java`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.2.7/refcodes-properties-ext-obfuscation/src/test/java/org/refcodes/configuration/ext/obfuscation/ObfuscationPropertiesTest.java) 
* \[<span style="color:blue">ADDED</span>\] [`RuntimePropertiesSugar.java`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.2.7/refcodes-properties-ext-runtime/src/main/java/org/refcodes/configuration/ext/runtime/RuntimePropertiesSugar.java) (see Javadoc at [`RuntimePropertiesSugar.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties-ext-runtime/1.2.7/org/refcodes/configuration/ext/runtime/RuntimePropertiesSugar.html))
* \[<span style="color:blue">ADDED</span>\] [`RuntimePropertiesSugarTest.java`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.2.7/refcodes-properties-ext-runtime/src/test/java/org/refcodes/configuration/ext/runtime/RuntimePropertiesSugarTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.2.7/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.2.7/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.2.7/refcodes-properties-ext-cli/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.2.7/refcodes-properties-ext-cli/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.2.7/refcodes-properties-ext-observer/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.2.7/refcodes-properties-ext-observer/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`AbstractObservablePropertiesBuilderDecorator.java`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.2.7/refcodes-properties-ext-observer/src/main/java/org/refcodes/configuration/ext/observer/AbstractObservablePropertiesBuilderDecorator.java) (see Javadoc at [`AbstractObservablePropertiesBuilderDecorator.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties-ext-observer/1.2.7/org/refcodes/configuration/ext/observer/AbstractObservablePropertiesBuilderDecorator.html))
* \[<span style="color:green">MODIFIED</span>\] [`AbstractObservableResourcePropertiesBuilderDecorator.java`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.2.7/refcodes-properties-ext-observer/src/main/java/org/refcodes/configuration/ext/observer/AbstractObservableResourcePropertiesBuilderDecorator.java) (see Javadoc at [`AbstractObservableResourcePropertiesBuilderDecorator.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties-ext-observer/1.2.7/org/refcodes/configuration/ext/observer/AbstractObservableResourcePropertiesBuilderDecorator.html))
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.2.7/refcodes-properties-ext-runtime/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.2.7/refcodes-properties-ext-runtime/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`RuntimeProperties.java`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.2.7/refcodes-properties-ext-runtime/src/main/java/org/refcodes/configuration/ext/runtime/RuntimeProperties.java) (see Javadoc at [`RuntimeProperties.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties-ext-runtime/1.2.7/org/refcodes/configuration/ext/runtime/RuntimeProperties.html))
* \[<span style="color:green">MODIFIED</span>\] [`RuntimePropertiesImpl.java`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.2.7/refcodes-properties-ext-runtime/src/main/java/org/refcodes/configuration/ext/runtime/RuntimePropertiesImpl.java) (see Javadoc at [`RuntimePropertiesImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties-ext-runtime/1.2.7/org/refcodes/configuration/ext/runtime/RuntimePropertiesImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`RuntimePropertiesTest.java`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.2.7/refcodes-properties-ext-runtime/src/test/java/org/refcodes/configuration/ext/runtime/RuntimePropertiesTest.java) 

## Change list &lt;refcodes-servicebus&gt; (version 1.2.7)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-servicebus/src/refcodes-servicebus-1.2.7/pom.xml) 

## Change list &lt;refcodes-servicebus-alt&gt; (version 1.2.7)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-servicebus-alt/src/refcodes-servicebus-alt-1.2.7/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-servicebus-alt/src/refcodes-servicebus-alt-1.2.7/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-servicebus-alt/src/refcodes-servicebus-alt-1.2.7/refcodes-servicebus-alt-spring/pom.xml) 

## Change list &lt;refcodes-tabular-alt&gt; (version 1.2.7)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-tabular-alt/src/refcodes-tabular-alt-1.2.7/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-tabular-alt/src/refcodes-tabular-alt-1.2.7/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-tabular-alt/src/refcodes-tabular-alt-1.2.7/refcodes-tabular-alt-forwardsecrecy/pom.xml) 
