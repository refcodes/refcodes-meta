> This change list has been auto-generated on `triton` by `steiner` with `changelist-all.sh` on the 2021-02-11 at 20:18:53.

## Change list &lt;refcodes-licensing&gt; (version 2.1.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-licensing/src/refcodes-licensing-2.1.1/pom.xml) 

## Change list &lt;refcodes-parent&gt; (version 2.1.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-parent/src/refcodes-parent-2.1.1/pom.xml) 

## Change list &lt;refcodes-time&gt; (version 2.1.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-time/src/refcodes-time-2.1.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`DateFormat.java`](https://bitbucket.org/refcodes/refcodes-time/src/refcodes-time-2.1.1/src/main/java/org/refcodes/time/DateFormat.java) (see Javadoc at [`DateFormat.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-time/2.1.1/org.refcodes.time/org/refcodes/time/DateFormat.html))
* \[<span style="color:green">MODIFIED</span>\] [`DateFormats.java`](https://bitbucket.org/refcodes/refcodes-time/src/refcodes-time-2.1.1/src/main/java/org/refcodes/time/DateFormats.java) (see Javadoc at [`DateFormats.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-time/2.1.1/org.refcodes.time/org/refcodes/time/DateFormats.html))

## Change list &lt;refcodes-mixin&gt; (version 2.1.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-mixin/src/refcodes-mixin-2.1.1/pom.xml) 

## Change list &lt;refcodes-data&gt; (version 2.1.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data/src/refcodes-data-2.1.1/pom.xml) 

## Change list &lt;refcodes-exception&gt; (version 2.1.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-exception/src/refcodes-exception-2.1.1/pom.xml) 

## Change list &lt;refcodes-factory&gt; (version 2.1.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-factory/src/refcodes-factory-2.1.1/pom.xml) 

## Change list &lt;refcodes-factory-alt&gt; (version 2.1.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-factory-alt/src/refcodes-factory-alt-2.1.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-factory-alt/src/refcodes-factory-alt-2.1.1/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-factory-alt/src/refcodes-factory-alt-2.1.1/refcodes-factory-alt-spring/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`BeanTest.java`](https://bitbucket.org/refcodes/refcodes-factory-alt/src/refcodes-factory-alt-2.1.1/refcodes-factory-alt-spring/src/test/java/org/refcodes/factory/alt/spring/BeanTest.java) 

## Change list &lt;refcodes-controlflow&gt; (version 2.1.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-controlflow/src/refcodes-controlflow-2.1.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`ControlFlowUtility.java`](https://bitbucket.org/refcodes/refcodes-controlflow/src/refcodes-controlflow-2.1.1/src/main/java/org/refcodes/controlflow/ControlFlowUtility.java) (see Javadoc at [`ControlFlowUtility.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-controlflow/2.1.1/org.refcodes.controlflow/org/refcodes/controlflow/ControlFlowUtility.html))

## Change list &lt;refcodes-numerical&gt; (version 2.1.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-numerical/src/refcodes-numerical-2.1.1/pom.xml) 

## Change list &lt;refcodes-generator&gt; (version 2.1.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-generator/src/refcodes-generator-2.1.1/pom.xml) 

## Change list &lt;refcodes-matcher&gt; (version 2.1.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-matcher/src/refcodes-matcher-2.1.1/pom.xml) 

## Change list &lt;refcodes-struct&gt; (version 2.1.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-struct/src/refcodes-struct-2.1.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-struct/src/refcodes-struct-2.1.1/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`module-info.java`](https://bitbucket.org/refcodes/refcodes-struct/src/refcodes-struct-2.1.1/src/main/java/module-info.java) (see Javadoc at [`module-info.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-struct/2.1.1/./module-info.html))
* \[<span style="color:green">MODIFIED</span>\] [`CanonicalMapBuilderImpl.java`](https://bitbucket.org/refcodes/refcodes-struct/src/refcodes-struct-2.1.1/src/main/java/org/refcodes/struct/CanonicalMapBuilderImpl.java) (see Javadoc at [`CanonicalMapBuilderImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-struct/2.1.1/org.refcodes.struct/org/refcodes/struct/CanonicalMapBuilderImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`ClassStructMapBuilderImpl.java`](https://bitbucket.org/refcodes/refcodes-struct/src/refcodes-struct-2.1.1/src/main/java/org/refcodes/struct/ClassStructMapBuilderImpl.java) (see Javadoc at [`ClassStructMapBuilderImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-struct/2.1.1/org.refcodes.struct/org/refcodes/struct/ClassStructMapBuilderImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`PathMapBuilderImpl.java`](https://bitbucket.org/refcodes/refcodes-struct/src/refcodes-struct-2.1.1/src/main/java/org/refcodes/struct/PathMapBuilderImpl.java) (see Javadoc at [`PathMapBuilderImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-struct/2.1.1/org.refcodes.struct/org/refcodes/struct/PathMapBuilderImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`PathMap.java`](https://bitbucket.org/refcodes/refcodes-struct/src/refcodes-struct-2.1.1/src/main/java/org/refcodes/struct/PathMap.java) (see Javadoc at [`PathMap.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-struct/2.1.1/org.refcodes.struct/org/refcodes/struct/PathMap.html))
* \[<span style="color:green">MODIFIED</span>\] [`SimpleType.java`](https://bitbucket.org/refcodes/refcodes-struct/src/refcodes-struct-2.1.1/src/main/java/org/refcodes/struct/SimpleType.java) (see Javadoc at [`SimpleType.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-struct/2.1.1/org.refcodes.struct/org/refcodes/struct/SimpleType.html))
* \[<span style="color:green">MODIFIED</span>\] [`SimpleTypeMapBuilderImpl.java`](https://bitbucket.org/refcodes/refcodes-struct/src/refcodes-struct-2.1.1/src/main/java/org/refcodes/struct/SimpleTypeMapBuilderImpl.java) (see Javadoc at [`SimpleTypeMapBuilderImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-struct/2.1.1/org.refcodes.struct/org/refcodes/struct/SimpleTypeMapBuilderImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`TypeUtility.java`](https://bitbucket.org/refcodes/refcodes-struct/src/refcodes-struct-2.1.1/src/main/java/org/refcodes/struct/TypeUtility.java) (see Javadoc at [`TypeUtility.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-struct/2.1.1/org.refcodes.struct/org/refcodes/struct/TypeUtility.html))
* \[<span style="color:green">MODIFIED</span>\] [`PathMapTest.java`](https://bitbucket.org/refcodes/refcodes-struct/src/refcodes-struct-2.1.1/src/test/java/org/refcodes/struct/PathMapTest.java) 

## Change list &lt;refcodes-struct-ext&gt; (version 2.1.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-struct-ext/src/refcodes-struct-ext-2.1.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-struct-ext/src/refcodes-struct-ext-2.1.1/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-struct-ext/src/refcodes-struct-ext-2.1.1/refcodes-struct-ext-factory/pom.xml) 

## Change list &lt;refcodes-runtime&gt; (version 2.1.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-2.1.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-2.1.1/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`Terminal.java`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-2.1.1/src/main/java/org/refcodes/runtime/Terminal.java) (see Javadoc at [`Terminal.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-runtime/2.1.1/org.refcodes.runtime/org/refcodes/runtime/Terminal.html))

## Change list &lt;refcodes-component&gt; (version 2.1.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-2.1.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`ComponentUtility.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-2.1.1/src/main/java/org/refcodes/component/ComponentUtility.java) (see Javadoc at [`ComponentUtility.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/2.1.1/org.refcodes.component/org/refcodes/component/ComponentUtility.html))

## Change list &lt;refcodes-data-ext&gt; (version 2.1.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-2.1.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-2.1.1/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-2.1.1/refcodes-data-ext-boulderdash/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-2.1.1/refcodes-data-ext-checkers/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-2.1.1/refcodes-data-ext-checkers/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-2.1.1/refcodes-data-ext-chess/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-2.1.1/refcodes-data-ext-chess/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-2.1.1/refcodes-data-ext-corporate/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-2.1.1/refcodes-data-ext-corporate/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-2.1.1/refcodes-data-ext-symbols/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-2.1.1/refcodes-data-ext-symbols/README.md) 

## Change list &lt;refcodes-graphical&gt; (version 2.1.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-graphical/src/refcodes-graphical-2.1.1/pom.xml) 

## Change list &lt;refcodes-textual&gt; (version 2.1.1)

* \[<span style="color:red">DELETED</span>\] `EscapeTextBuilderImpl.java`
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-2.1.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`EscapeTextBuilder.java`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-2.1.1/src/main/java/org/refcodes/textual/EscapeTextBuilder.java) (see Javadoc at [`EscapeTextBuilder.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-textual/2.1.1/org.refcodes.textual/org/refcodes/textual/EscapeTextBuilder.html))
* \[<span style="color:green">MODIFIED</span>\] [`EscapeTextBuilderTest.java`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-2.1.1/src/test/java/org/refcodes/textual/EscapeTextBuilderTest.java) 

## Change list &lt;refcodes-criteria&gt; (version 2.1.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-2.1.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-2.1.1/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`ExpressionCriteriaFactoryImpl.java`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-2.1.1/src/main/java/org/refcodes/criteria/ExpressionCriteriaFactoryImpl.java) (see Javadoc at [`ExpressionCriteriaFactoryImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-criteria/2.1.1/org.refcodes.criteria/org/refcodes/criteria/ExpressionCriteriaFactoryImpl.html))

## Change list &lt;refcodes-io&gt; (version 2.1.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-io/src/refcodes-io-2.1.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-io/src/refcodes-io-2.1.1/README.md) 

## Change list &lt;refcodes-tabular&gt; (version 2.1.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-tabular/src/refcodes-tabular-2.1.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-tabular/src/refcodes-tabular-2.1.1/README.md) 

## Change list &lt;refcodes-observer&gt; (version 2.1.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-observer/src/refcodes-observer-2.1.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`EventMatcherSugar.java`](https://bitbucket.org/refcodes/refcodes-observer/src/refcodes-observer-2.1.1/src/main/java/org/refcodes/observer/EventMatcherSugar.java) (see Javadoc at [`EventMatcherSugar.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-observer/2.1.1/org.refcodes.observer/org/refcodes/observer/EventMatcherSugar.html))

## Change list &lt;refcodes-command&gt; (version 2.1.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-command/src/refcodes-command-2.1.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-command/src/refcodes-command-2.1.1/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`UndoableTest.java`](https://bitbucket.org/refcodes/refcodes-command/src/refcodes-command-2.1.1/src/test/java/org/refcodes/command/UndoableTest.java) 

## Change list &lt;refcodes-cli&gt; (version 2.1.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-2.1.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-2.1.1/README.md) 

## Change list &lt;refcodes-audio&gt; (version 2.1.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-audio/src/refcodes-audio-2.1.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-audio/src/refcodes-audio-2.1.1/README.md) 

## Change list &lt;refcodes-codec&gt; (version 2.1.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-2.1.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-2.1.1/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`BaseBuilder.java`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-2.1.1/src/main/java/org/refcodes/codec/BaseBuilder.java) (see Javadoc at [`BaseBuilder.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-codec/2.1.1/org.refcodes.codec/org/refcodes/codec/BaseBuilder.html))
* \[<span style="color:green">MODIFIED</span>\] [`BaseDecoder.java`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-2.1.1/src/main/java/org/refcodes/codec/BaseDecoder.java) (see Javadoc at [`BaseDecoder.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-codec/2.1.1/org.refcodes.codec/org/refcodes/codec/BaseDecoder.html))
* \[<span style="color:green">MODIFIED</span>\] [`BaseEncoder.java`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-2.1.1/src/main/java/org/refcodes/codec/BaseEncoder.java) (see Javadoc at [`BaseEncoder.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-codec/2.1.1/org.refcodes.codec/org/refcodes/codec/BaseEncoder.html))
* \[<span style="color:green">MODIFIED</span>\] [`BaseMetricsAccessor.java`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-2.1.1/src/main/java/org/refcodes/codec/BaseMetricsAccessor.java) (see Javadoc at [`BaseMetricsAccessor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-codec/2.1.1/org.refcodes.codec/org/refcodes/codec/BaseMetricsAccessor.html))

## Change list &lt;refcodes-component-ext&gt; (version 2.1.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-2.1.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-2.1.1/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-2.1.1/refcodes-component-ext-observer/pom.xml) 

## Change list &lt;refcodes-properties&gt; (version 2.1.1)

* \[<span style="color:blue">ADDED</span>\] [`ComplexType.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-2.1.1/src/test/java/org/refcodes/properties/ComplexType.java) 
* \[<span style="color:blue">ADDED</span>\] [`ComplexType.json`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-2.1.1/src/test/resources/ComplexType.json) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-2.1.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-2.1.1/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`package-info.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-2.1.1/src/main/java/org/refcodes/properties/package-info.java) 
* \[<span style="color:green">MODIFIED</span>\] [`ResourcePropertiesTest.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-2.1.1/src/test/java/org/refcodes/properties/ResourcePropertiesTest.java) 

## Change list &lt;refcodes-security&gt; (version 2.1.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security/src/refcodes-security-2.1.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security/src/refcodes-security-2.1.1/README.md) 

## Change list &lt;refcodes-security-alt&gt; (version 2.1.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-2.1.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-2.1.1/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-2.1.1/refcodes-security-alt-chaos/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-2.1.1/refcodes-security-alt-chaos/README.md) 

## Change list &lt;refcodes-security-ext&gt; (version 2.1.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-2.1.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-2.1.1/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-2.1.1/refcodes-security-ext-chaos/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-2.1.1/refcodes-security-ext-chaos/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-2.1.1/refcodes-security-ext-spring/pom.xml) 

## Change list &lt;refcodes-properties-ext&gt; (version 2.1.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-2.1.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-2.1.1/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-2.1.1/refcodes-properties-ext-cli/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-2.1.1/refcodes-properties-ext-cli/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-2.1.1/refcodes-properties-ext-obfuscation/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-2.1.1/refcodes-properties-ext-obfuscation/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-2.1.1/refcodes-properties-ext-observer/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-2.1.1/refcodes-properties-ext-observer/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-2.1.1/refcodes-properties-ext-runtime/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-2.1.1/refcodes-properties-ext-runtime/README.md) 

## Change list &lt;refcodes-logger&gt; (version 2.1.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger/src/refcodes-logger-2.1.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger/src/refcodes-logger-2.1.1/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`StatisticalCompositeTrimLoggerTest.java`](https://bitbucket.org/refcodes/refcodes-logger/src/refcodes-logger-2.1.1/src/test/java/org/refcodes/logger/StatisticalCompositeTrimLoggerTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`StatisticalPartedTrimLoggerTest.java`](https://bitbucket.org/refcodes/refcodes-logger/src/refcodes-logger-2.1.1/src/test/java/org/refcodes/logger/StatisticalPartedTrimLoggerTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`TestTrimLoggerUtility.java`](https://bitbucket.org/refcodes/refcodes-logger/src/refcodes-logger-2.1.1/src/test/java/org/refcodes/logger/TestTrimLoggerUtility.java) 

## Change list &lt;refcodes-logger-alt&gt; (version 2.1.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-2.1.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-2.1.1/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-2.1.1/refcodes-logger-alt-async/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-2.1.1/refcodes-logger-alt-async/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-2.1.1/refcodes-logger-alt-console/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-2.1.1/refcodes-logger-alt-console/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-2.1.1/refcodes-logger-alt-io/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-2.1.1/refcodes-logger-alt-io/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-2.1.1/refcodes-logger-alt-simpledb/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`SimpleDbQueryLogger.java`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-2.1.1/refcodes-logger-alt-simpledb/src/main/java/org/refcodes/logger/alt/simpledb/SimpleDbQueryLogger.java) (see Javadoc at [`SimpleDbQueryLogger.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-codec/2.1.1/org.refcodes.codec/org/refcodes/codec/BaseMetricsAccessor.html))
* \[<span style="color:green">MODIFIED</span>\] [`SimpleDbTrimLogger.java`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-2.1.1/refcodes-logger-alt-simpledb/src/main/java/org/refcodes/logger/alt/simpledb/SimpleDbTrimLogger.java) (see Javadoc at [`SimpleDbTrimLogger.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-codec/2.1.1/org.refcodes.codec/org/refcodes/codec/BaseMetricsAccessor.html))
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-2.1.1/refcodes-logger-alt-slf4j/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-2.1.1/refcodes-logger-alt-spring/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`SpringRuntimeLoggerSingleton.java`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-2.1.1/refcodes-logger-alt-spring/src/main/java/org/refcodes/logger/alt/spring/SpringRuntimeLoggerSingleton.java) (see Javadoc at [`SpringRuntimeLoggerSingleton.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-codec/2.1.1/org.refcodes.codec/org/refcodes/codec/BaseMetricsAccessor.html))

## Change list &lt;refcodes-logger-ext&gt; (version 2.1.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-2.1.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-2.1.1/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-2.1.1/refcodes-logger-ext-slf4j/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-2.1.1/refcodes-logger-ext-slf4j/README.md) 

## Change list &lt;refcodes-graphical-ext&gt; (version 2.1.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-2.1.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-2.1.1/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-2.1.1/refcodes-graphical-ext-javafx/pom.xml) 

## Change list &lt;refcodes-checkerboard&gt; (version 2.1.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-2.1.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-2.1.1/README.md) 

## Change list &lt;refcodes-checkerboard-alt&gt; (version 2.1.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-alt/src/refcodes-checkerboard-alt-2.1.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-alt/src/refcodes-checkerboard-alt-2.1.1/refcodes-checkerboard-alt-javafx/pom.xml) 

## Change list &lt;refcodes-checkerboard-ext&gt; (version 2.1.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-ext/src/refcodes-checkerboard-ext-2.1.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-checkerboard-ext/src/refcodes-checkerboard-ext-2.1.1/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-ext/src/refcodes-checkerboard-ext-2.1.1/refcodes-checkerboard-ext-javafx-boulderdash/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-ext/src/refcodes-checkerboard-ext-2.1.1/refcodes-checkerboard-ext-javafx-chess/pom.xml) 

## Change list &lt;refcodes-boulderdash&gt; (version 2.1.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-boulderdash/src/refcodes-boulderdash-2.1.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-boulderdash/src/refcodes-boulderdash-2.1.1/README.md) 

## Change list &lt;refcodes-web&gt; (version 2.1.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-web/src/refcodes-web-2.1.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`XmlMediaTypeFactory.java`](https://bitbucket.org/refcodes/refcodes-web/src/refcodes-web-2.1.1/src/main/java/org/refcodes/web/XmlMediaTypeFactory.java) (see Javadoc at [`XmlMediaTypeFactory.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-web/2.1.1/org.refcodes.web/org/refcodes/web/XmlMediaTypeFactory.html))

## Change list &lt;refcodes-rest&gt; (version 2.1.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-2.1.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-2.1.1/README.md) 

## Change list &lt;refcodes-hal&gt; (version 2.1.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-2.1.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-2.1.1/README.md) 

## Change list &lt;refcodes-eventbus&gt; (version 2.1.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-eventbus/src/refcodes-eventbus-2.1.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-eventbus/src/refcodes-eventbus-2.1.1/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`SimpleBusMatcherSugar.java`](https://bitbucket.org/refcodes/refcodes-eventbus/src/refcodes-eventbus-2.1.1/src/main/java/org/refcodes/eventbus/SimpleBusMatcherSugar.java) (see Javadoc at [`SimpleBusMatcherSugar.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-eventbus/2.1.1/org.refcodes.eventbus/org/refcodes/eventbus/SimpleBusMatcherSugar.html))
* \[<span style="color:green">MODIFIED</span>\] [`EventBusTest.java`](https://bitbucket.org/refcodes/refcodes-eventbus/src/refcodes-eventbus-2.1.1/src/test/java/org/refcodes/eventbus/EventBusTest.java) 

## Change list &lt;refcodes-eventbus-ext&gt; (version 2.1.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-2.1.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-2.1.1/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-2.1.1/refcodes-eventbus-ext-application/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`ApplicationMatcherSugar.java`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-2.1.1/refcodes-eventbus-ext-application/src/main/java/org/refcodes/eventbus/ext/application/ApplicationMatcherSugar.java) (see Javadoc at [`ApplicationMatcherSugar.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-eventbus/2.1.1/org.refcodes.eventbus/org/refcodes/eventbus/SimpleBusMatcherSugar.html))

## Change list &lt;refcodes-filesystem&gt; (version 2.1.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-filesystem/src/refcodes-filesystem-2.1.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-filesystem/src/refcodes-filesystem-2.1.1/README.md) 

## Change list &lt;refcodes-filesystem-alt&gt; (version 2.1.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-filesystem-alt/src/refcodes-filesystem-alt-2.1.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-filesystem-alt/src/refcodes-filesystem-alt-2.1.1/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-filesystem-alt/src/refcodes-filesystem-alt-2.1.1/refcodes-filesystem-alt-s3/pom.xml) 

## Change list &lt;refcodes-forwardsecrecy&gt; (version 2.1.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy/src/refcodes-forwardsecrecy-2.1.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy/src/refcodes-forwardsecrecy-2.1.1/README.md) 

## Change list &lt;refcodes-forwardsecrecy-alt&gt; (version 2.1.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-2.1.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-2.1.1/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-2.1.1/refcodes-forwardsecrecy-alt-filesystem/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`BasicForwardSecrecyFileSystemTest.java`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-2.1.1/refcodes-forwardsecrecy-alt-filesystem/src/test/java/org/refcodes/forwardsecrecy/alt/filesystem/BasicForwardSecrecyFileSystemTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`ForwardSecrecyFileSystemTest.java`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-2.1.1/refcodes-forwardsecrecy-alt-filesystem/src/test/java/org/refcodes/forwardsecrecy/alt/filesystem/ForwardSecrecyFileSystemTest.java) 

## Change list &lt;refcodes-io-ext&gt; (version 2.1.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-2.1.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-2.1.1/refcodes-io-ext-observer/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-2.1.1/refcodes-io-ext-observer/README.md) 

## Change list &lt;refcodes-interceptor&gt; (version 2.1.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-interceptor/src/refcodes-interceptor-2.1.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-interceptor/src/refcodes-interceptor-2.1.1/README.md) 

## Change list &lt;refcodes-jobbus&gt; (version 2.1.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-jobbus/src/refcodes-jobbus-2.1.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-jobbus/src/refcodes-jobbus-2.1.1/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`JobBusProxyImplTest.java`](https://bitbucket.org/refcodes/refcodes-jobbus/src/refcodes-jobbus-2.1.1/src/test/java/org/refcodes/jobbus/JobBusProxyImplTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`SimpleJobBusTest.java`](https://bitbucket.org/refcodes/refcodes-jobbus/src/refcodes-jobbus-2.1.1/src/test/java/org/refcodes/jobbus/SimpleJobBusTest.java) 

## Change list &lt;refcodes-rest-ext&gt; (version 2.1.1)

* \[<span style="color:red">DELETED</span>\] `Helper.class`
* \[<span style="color:red">DELETED</span>\] `Main.class`
* \[<span style="color:red">DELETED</span>\] `pom.xml`
* \[<span style="color:red">DELETED</span>\] `README.md`
* \[<span style="color:red">DELETED</span>\] `build.sh`
* \[<span style="color:red">DELETED</span>\] `pom.xml`
* \[<span style="color:red">DELETED</span>\] `README.md`
* \[<span style="color:red">DELETED</span>\] `shell-exec.inc`
* \[<span style="color:red">DELETED</span>\] `scriptify.sh`
* \[<span style="color:red">DELETED</span>\] `__artifactId__.ini`
* \[<span style="color:red">DELETED</span>\] `runtimelogger.ini`
* \[<span style="color:red">DELETED</span>\] `archetype-metadata.xml`
* \[<span style="color:red">DELETED</span>\] `pom.xml`
* \[<span style="color:red">DELETED</span>\] `README.md`
* \[<span style="color:red">DELETED</span>\] `build.sh`
* \[<span style="color:red">DELETED</span>\] `pom.xml`
* \[<span style="color:red">DELETED</span>\] `README.md`
* \[<span style="color:red">DELETED</span>\] `shell-exec.inc`
* \[<span style="color:red">DELETED</span>\] `scriptify.sh`
* \[<span style="color:red">DELETED</span>\] `Helper.java`
* \[<span style="color:red">DELETED</span>\] `Main.java`
* \[<span style="color:red">DELETED</span>\] `__artifactId__.ini`
* \[<span style="color:red">DELETED</span>\] `runtimelogger.ini`
* \[<span style="color:red">DELETED</span>\] `archetype-metadata.xml`
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-2.1.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-2.1.1/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-2.1.1/refcodes-rest-ext-eureka/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-2.1.1/refcodes-rest-ext-eureka/README.md) 

## Change list &lt;refcodes-remoting&gt; (version 2.1.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-remoting/src/refcodes-remoting-2.1.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-remoting/src/refcodes-remoting-2.1.1/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`CancelMethodReplyMessageImpl.java`](https://bitbucket.org/refcodes/refcodes-remoting/src/refcodes-remoting-2.1.1/src/main/java/org/refcodes/remoting/CancelMethodReplyMessageImpl.java) (see Javadoc at [`CancelMethodReplyMessageImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-remoting/2.1.1/org.refcodes.remoting/org/refcodes/remoting/CancelMethodReplyMessageImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`MethodReplyMessageImpl.java`](https://bitbucket.org/refcodes/refcodes-remoting/src/refcodes-remoting-2.1.1/src/main/java/org/refcodes/remoting/MethodReplyMessageImpl.java) (see Javadoc at [`MethodReplyMessageImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-remoting/2.1.1/org.refcodes.remoting/org/refcodes/remoting/MethodReplyMessageImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`PublishSubjectReplyMessageImpl.java`](https://bitbucket.org/refcodes/refcodes-remoting/src/refcodes-remoting-2.1.1/src/main/java/org/refcodes/remoting/PublishSubjectReplyMessageImpl.java) (see Javadoc at [`PublishSubjectReplyMessageImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-remoting/2.1.1/org.refcodes.remoting/org/refcodes/remoting/PublishSubjectReplyMessageImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`RemoteClientImpl.java`](https://bitbucket.org/refcodes/refcodes-remoting/src/refcodes-remoting-2.1.1/src/main/java/org/refcodes/remoting/RemoteClientImpl.java) (see Javadoc at [`RemoteClientImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-remoting/2.1.1/org.refcodes.remoting/org/refcodes/remoting/RemoteClientImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`RemoteServerImpl.java`](https://bitbucket.org/refcodes/refcodes-remoting/src/refcodes-remoting-2.1.1/src/main/java/org/refcodes/remoting/RemoteServerImpl.java) (see Javadoc at [`RemoteServerImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-remoting/2.1.1/org.refcodes.remoting/org/refcodes/remoting/RemoteServerImpl.html))

## Change list &lt;refcodes-remoting-ext&gt; (version 2.1.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-2.1.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-2.1.1/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-2.1.1/refcodes-remoting-ext-observer/pom.xml) 

## Change list &lt;refcodes-serial&gt; (version 2.1.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial/src/refcodes-serial-2.1.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-serial/src/refcodes-serial-2.1.1/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`AbstractLengthDecoratorSegment.java`](https://bitbucket.org/refcodes/refcodes-serial/src/refcodes-serial-2.1.1/src/main/java/org/refcodes/serial/AbstractLengthDecoratorSegment.java) (see Javadoc at [`AbstractLengthDecoratorSegment.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-serial/2.1.1/org.refcodes.serial/org/refcodes/serial/AbstractLengthDecoratorSegment.html))
* \[<span style="color:green">MODIFIED</span>\] [`AbstractPortDecorator.java`](https://bitbucket.org/refcodes/refcodes-serial/src/refcodes-serial-2.1.1/src/main/java/org/refcodes/serial/AbstractPortDecorator.java) (see Javadoc at [`AbstractPortDecorator.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-serial/2.1.1/org.refcodes.serial/org/refcodes/serial/AbstractPortDecorator.html))
* \[<span style="color:green">MODIFIED</span>\] [`FullDuplexPacketPortDecorator.java`](https://bitbucket.org/refcodes/refcodes-serial/src/refcodes-serial-2.1.1/src/main/java/org/refcodes/serial/FullDuplexPacketPortDecorator.java) (see Javadoc at [`FullDuplexPacketPortDecorator.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-serial/2.1.1/org.refcodes.serial/org/refcodes/serial/FullDuplexPacketPortDecorator.html))
* \[<span style="color:green">MODIFIED</span>\] [`FullDuplexTxPortDecorator.java`](https://bitbucket.org/refcodes/refcodes-serial/src/refcodes-serial-2.1.1/src/main/java/org/refcodes/serial/FullDuplexTxPortDecorator.java) (see Javadoc at [`FullDuplexTxPortDecorator.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-serial/2.1.1/org.refcodes.serial/org/refcodes/serial/FullDuplexTxPortDecorator.html))
* \[<span style="color:green">MODIFIED</span>\] [`SerialSugar.java`](https://bitbucket.org/refcodes/refcodes-serial/src/refcodes-serial-2.1.1/src/main/java/org/refcodes/serial/SerialSugar.java) (see Javadoc at [`SerialSugar.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-serial/2.1.1/org.refcodes.serial/org/refcodes/serial/SerialSugar.html))
* \[<span style="color:green">MODIFIED</span>\] [`AllocSegmentTest.java`](https://bitbucket.org/refcodes/refcodes-serial/src/refcodes-serial-2.1.1/src/test/java/org/refcodes/serial/AllocSegmentTest.java) 

## Change list &lt;refcodes-serial-ext&gt; (version 2.1.1)

* \[<span style="color:blue">ADDED</span>\] [`module-info.java`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-2.1.1/refcodes-serial-ext-observer/src/main/java/module-info.java) (see Javadoc at [`module-info.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-serial/2.1.1/org.refcodes.serial/org/refcodes/serial/LengthDecoratorSegment.html))
* \[<span style="color:blue">ADDED</span>\] [`ObservablePayloadSectionDecorator.java`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-2.1.1/refcodes-serial-ext-observer/src/main/java/org/refcodes/serial/ext/observer/ObservablePayloadSectionDecorator.java) (see Javadoc at [`ObservablePayloadSectionDecorator.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-serial/2.1.1/org.refcodes.serial/org/refcodes/serial/LengthDecoratorSegment.html))
* \[<span style="color:blue">ADDED</span>\] [`ObservablePayloadSection.java`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-2.1.1/refcodes-serial-ext-observer/src/main/java/org/refcodes/serial/ext/observer/ObservablePayloadSection.java) (see Javadoc at [`ObservablePayloadSection.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-serial/2.1.1/org.refcodes.serial/org/refcodes/serial/LengthDecoratorSegment.html))
* \[<span style="color:blue">ADDED</span>\] [`ObservablePayloadSegmentDecorator.java`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-2.1.1/refcodes-serial-ext-observer/src/main/java/org/refcodes/serial/ext/observer/ObservablePayloadSegmentDecorator.java) (see Javadoc at [`ObservablePayloadSegmentDecorator.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-serial/2.1.1/org.refcodes.serial/org/refcodes/serial/LengthDecoratorSegment.html))
* \[<span style="color:blue">ADDED</span>\] [`ObservablePayloadSegment.java`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-2.1.1/refcodes-serial-ext-observer/src/main/java/org/refcodes/serial/ext/observer/ObservablePayloadSegment.java) (see Javadoc at [`ObservablePayloadSegment.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-serial/2.1.1/org.refcodes.serial/org/refcodes/serial/LengthDecoratorSegment.html))
* \[<span style="color:blue">ADDED</span>\] [`ObservablePayloadTransmission.java`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-2.1.1/refcodes-serial-ext-observer/src/main/java/org/refcodes/serial/ext/observer/ObservablePayloadTransmission.java) (see Javadoc at [`ObservablePayloadTransmission.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-serial/2.1.1/org.refcodes.serial/org/refcodes/serial/LengthDecoratorSegment.html))
* \[<span style="color:blue">ADDED</span>\] [`ObservableSectionDecorator.java`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-2.1.1/refcodes-serial-ext-observer/src/main/java/org/refcodes/serial/ext/observer/ObservableSectionDecorator.java) (see Javadoc at [`ObservableSectionDecorator.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-serial/2.1.1/org.refcodes.serial/org/refcodes/serial/LengthDecoratorSegment.html))
* \[<span style="color:blue">ADDED</span>\] [`ObservableSection.java`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-2.1.1/refcodes-serial-ext-observer/src/main/java/org/refcodes/serial/ext/observer/ObservableSection.java) (see Javadoc at [`ObservableSection.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-serial/2.1.1/org.refcodes.serial/org/refcodes/serial/LengthDecoratorSegment.html))
* \[<span style="color:blue">ADDED</span>\] [`ObservableSegmentDecorator.java`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-2.1.1/refcodes-serial-ext-observer/src/main/java/org/refcodes/serial/ext/observer/ObservableSegmentDecorator.java) (see Javadoc at [`ObservableSegmentDecorator.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-serial/2.1.1/org.refcodes.serial/org/refcodes/serial/LengthDecoratorSegment.html))
* \[<span style="color:blue">ADDED</span>\] [`ObservableSegment.java`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-2.1.1/refcodes-serial-ext-observer/src/main/java/org/refcodes/serial/ext/observer/ObservableSegment.java) (see Javadoc at [`ObservableSegment.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-serial/2.1.1/org.refcodes.serial/org/refcodes/serial/LengthDecoratorSegment.html))
* \[<span style="color:blue">ADDED</span>\] [`ObservableSerialSugar.java`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-2.1.1/refcodes-serial-ext-observer/src/main/java/org/refcodes/serial/ext/observer/ObservableSerialSugar.java) (see Javadoc at [`ObservableSerialSugar.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-serial/2.1.1/org.refcodes.serial/org/refcodes/serial/LengthDecoratorSegment.html))
* \[<span style="color:blue">ADDED</span>\] [`ObservableTransmission.java`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-2.1.1/refcodes-serial-ext-observer/src/main/java/org/refcodes/serial/ext/observer/ObservableTransmission.java) (see Javadoc at [`ObservableTransmission.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-serial/2.1.1/org.refcodes.serial/org/refcodes/serial/LengthDecoratorSegment.html))
* \[<span style="color:blue">ADDED</span>\] [`PayloadEvent.java`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-2.1.1/refcodes-serial-ext-observer/src/main/java/org/refcodes/serial/ext/observer/PayloadEvent.java) (see Javadoc at [`PayloadEvent.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-serial/2.1.1/org.refcodes.serial/org/refcodes/serial/LengthDecoratorSegment.html))
* \[<span style="color:blue">ADDED</span>\] [`PayloadObserver.java`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-2.1.1/refcodes-serial-ext-observer/src/main/java/org/refcodes/serial/ext/observer/PayloadObserver.java) (see Javadoc at [`PayloadObserver.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-serial/2.1.1/org.refcodes.serial/org/refcodes/serial/LengthDecoratorSegment.html))
* \[<span style="color:blue">ADDED</span>\] [`TransmissionEvent.java`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-2.1.1/refcodes-serial-ext-observer/src/main/java/org/refcodes/serial/ext/observer/TransmissionEvent.java) (see Javadoc at [`TransmissionEvent.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-serial/2.1.1/org.refcodes.serial/org/refcodes/serial/LengthDecoratorSegment.html))
* \[<span style="color:blue">ADDED</span>\] [`TransmissionObserver.java`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-2.1.1/refcodes-serial-ext-observer/src/main/java/org/refcodes/serial/ext/observer/TransmissionObserver.java) (see Javadoc at [`TransmissionObserver.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-serial/2.1.1/org.refcodes.serial/org/refcodes/serial/LengthDecoratorSegment.html))
* \[<span style="color:blue">ADDED</span>\] [`AbstractPortTest.java`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-2.1.1/refcodes-serial-ext-observer/src/test/java/org/refcodes/serial/ext/observer/AbstractPortTest.java) 
* \[<span style="color:blue">ADDED</span>\] [`ObservablePayloadTest.java`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-2.1.1/refcodes-serial-ext-observer/src/test/java/org/refcodes/serial/ext/observer/ObservablePayloadTest.java) 
* \[<span style="color:blue">ADDED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-2.1.1/refcodes-serial-ext-security/pom.xml) 
* \[<span style="color:blue">ADDED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-2.1.1/refcodes-serial-ext-security/README.md) 
* \[<span style="color:blue">ADDED</span>\] [`module-info.java`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-2.1.1/refcodes-serial-ext-security/src/main/java/module-info.java) (see Javadoc at [`module-info.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-serial/2.1.1/org.refcodes.serial/org/refcodes/serial/LengthDecoratorSegment.html))
* \[<span style="color:blue">ADDED</span>\] [`CipherTransmission.java`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-2.1.1/refcodes-serial-ext-security/src/main/java/org/refcodes/serial/ext/security/CipherTransmission.java) (see Javadoc at [`CipherTransmission.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-serial/2.1.1/org.refcodes.serial/org/refcodes/serial/LengthDecoratorSegment.html))
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-2.1.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-2.1.1/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-2.1.1/refcodes-serial-ext-observer/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-2.1.1/refcodes-serial-ext-observer/README.md) 

## Change list &lt;refcodes-servicebus&gt; (version 2.1.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-servicebus/src/refcodes-servicebus-2.1.1/pom.xml) 

## Change list &lt;refcodes-servicebus-alt&gt; (version 2.1.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-servicebus-alt/src/refcodes-servicebus-alt-2.1.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-servicebus-alt/src/refcodes-servicebus-alt-2.1.1/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-servicebus-alt/src/refcodes-servicebus-alt-2.1.1/refcodes-servicebus-alt-spring/pom.xml) 

## Change list &lt;refcodes-tabular-alt&gt; (version 2.1.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-tabular-alt/src/refcodes-tabular-alt-2.1.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-tabular-alt/src/refcodes-tabular-alt-2.1.1/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-tabular-alt/src/refcodes-tabular-alt-2.1.1/refcodes-tabular-alt-forwardsecrecy/pom.xml) 

## Change list &lt;refcodes-archetype&gt; (version 2.1.1)

* \[<span style="color:blue">ADDED</span>\] [`AbstractDaemon.java`](https://bitbucket.org/refcodes/refcodes-archetype/src/refcodes-archetype-2.1.1/src/main/java/org/refcodes/archetype/AbstractDaemon.java) (see Javadoc at [`AbstractDaemon.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-archetype/2.1.1/org.refcodes.archetype/org/refcodes/archetype/AbstractDaemon.html))
* \[<span style="color:red">DELETED</span>\] `AbstractDaemon.java`
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype/src/refcodes-archetype-2.1.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype/src/refcodes-archetype-2.1.1/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`application.conf`](https://bitbucket.org/refcodes/refcodes-archetype/src/refcodes-archetype-2.1.1/src/test/resources/application.conf) 

## Change list &lt;refcodes-archetype-alt&gt; (version 2.1.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-2.1.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-2.1.1/refcodes-archetype-alt-cli/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-2.1.1/refcodes-archetype-alt-cli/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-2.1.1/refcodes-archetype-alt-cli/src/main/resources/archetype-resources/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`archetype-metadata.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-2.1.1/refcodes-archetype-alt-cli/src/main/resources/META-INF/maven/archetype-metadata.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-2.1.1/refcodes-archetype-alt-rest/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-2.1.1/refcodes-archetype-alt-rest/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-2.1.1/refcodes-archetype-alt-rest/src/main/resources/archetype-resources/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`archetype-metadata.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-2.1.1/refcodes-archetype-alt-rest/src/main/resources/META-INF/maven/archetype-metadata.xml) 
