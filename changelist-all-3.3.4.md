> This change list has been auto-generated on `triton` by `steiner` with `changelist-all.sh` on the 2023-11-22 at 16:19:36.

## Change list &lt;refcodes-licensing&gt; (version 3.3.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-licensing/src/refcodes-licensing-3.3.4/pom.xml) 

## Change list &lt;refcodes-parent&gt; (version 3.3.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-parent/src/refcodes-parent-3.3.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-parent/src/refcodes-parent-3.3.4/README.md) 

## Change list &lt;refcodes-time&gt; (version 3.3.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-time/src/refcodes-time-3.3.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-time/src/refcodes-time-3.3.4/README.md) 

## Change list &lt;refcodes-mixin&gt; (version 3.3.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-mixin/src/refcodes-mixin-3.3.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-mixin/src/refcodes-mixin-3.3.4/README.md) 

## Change list &lt;refcodes-data&gt; (version 3.3.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data/src/refcodes-data-3.3.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data/src/refcodes-data-3.3.4/README.md) 

## Change list &lt;refcodes-exception&gt; (version 3.3.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-exception/src/refcodes-exception-3.3.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-exception/src/refcodes-exception-3.3.4/README.md) 

## Change list &lt;refcodes-factory&gt; (version 3.3.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-factory/src/refcodes-factory-3.3.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-factory/src/refcodes-factory-3.3.4/README.md) 

## Change list &lt;refcodes-factory-alt&gt; (version 3.3.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-factory-alt/src/refcodes-factory-alt-3.3.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-factory-alt/src/refcodes-factory-alt-3.3.4/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-factory-alt/src/refcodes-factory-alt-3.3.4/refcodes-factory-alt-spring/pom.xml) 

## Change list &lt;refcodes-controlflow&gt; (version 3.3.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-controlflow/src/refcodes-controlflow-3.3.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-controlflow/src/refcodes-controlflow-3.3.4/README.md) 

## Change list &lt;refcodes-numerical&gt; (version 3.3.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-numerical/src/refcodes-numerical-3.3.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-numerical/src/refcodes-numerical-3.3.4/README.md) 

## Change list &lt;refcodes-generator&gt; (version 3.3.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-generator/src/refcodes-generator-3.3.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-generator/src/refcodes-generator-3.3.4/README.md) 

## Change list &lt;refcodes-schema&gt; (version 3.3.4)

* \[<span style="color:blue">ADDED</span>\] [`XmlVisitor.java`](https://bitbucket.org/refcodes/refcodes-schema/src/refcodes-schema-3.3.4/src/main/java/org/refcodes/schema/XmlVisitor.java) (see Javadoc at [`XmlVisitor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-schema/3.3.4/org.refcodes.schema/org/refcodes/schema/XmlVisitor.html))
* \[<span style="color:blue">ADDED</span>\] [`JsonSchemaTest.java`](https://bitbucket.org/refcodes/refcodes-schema/src/refcodes-schema-3.3.4/src/test/java/org/refcodes/schema/JsonSchemaTest.java) 
* \[<span style="color:blue">ADDED</span>\] [`XmlSchemaTest.java`](https://bitbucket.org/refcodes/refcodes-schema/src/refcodes-schema-3.3.4/src/test/java/org/refcodes/schema/XmlSchemaTest.java) 
* \[<span style="color:red">DELETED</span>\] `AbstractSchema.java`
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-schema/src/refcodes-schema-3.3.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-schema/src/refcodes-schema-3.3.4/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`JsonVisitor.java`](https://bitbucket.org/refcodes/refcodes-schema/src/refcodes-schema-3.3.4/src/main/java/org/refcodes/schema/JsonVisitor.java) (see Javadoc at [`JsonVisitor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-schema/3.3.4/org.refcodes.schema/org/refcodes/schema/JsonVisitor.html))
* \[<span style="color:green">MODIFIED</span>\] [`package-info.java`](https://bitbucket.org/refcodes/refcodes-schema/src/refcodes-schema-3.3.4/src/main/java/org/refcodes/schema/package-info.java) 
* \[<span style="color:green">MODIFIED</span>\] [`Schema.java`](https://bitbucket.org/refcodes/refcodes-schema/src/refcodes-schema-3.3.4/src/main/java/org/refcodes/schema/Schema.java) (see Javadoc at [`Schema.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-schema/3.3.4/org.refcodes.schema/org/refcodes/schema/Schema.html))
* \[<span style="color:green">MODIFIED</span>\] [`SchemaVisitor.java`](https://bitbucket.org/refcodes/refcodes-schema/src/refcodes-schema-3.3.4/src/main/java/org/refcodes/schema/SchemaVisitor.java) (see Javadoc at [`SchemaVisitor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-schema/3.3.4/org.refcodes.schema/org/refcodes/schema/SchemaVisitor.html))

## Change list &lt;refcodes-matcher&gt; (version 3.3.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-matcher/src/refcodes-matcher-3.3.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-matcher/src/refcodes-matcher-3.3.4/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`MatcherSchema.java`](https://bitbucket.org/refcodes/refcodes-matcher/src/refcodes-matcher-3.3.4/src/main/java/org/refcodes/matcher/MatcherSchema.java) (see Javadoc at [`MatcherSchema.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-matcher/3.3.4/org.refcodes.matcher/org/refcodes/matcher/MatcherSchema.html))

## Change list &lt;refcodes-struct&gt; (version 3.3.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-struct/src/refcodes-struct-3.3.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-struct/src/refcodes-struct-3.3.4/README.md) 

## Change list &lt;refcodes-runtime&gt; (version 3.3.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-3.3.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-3.3.4/README.md) 

## Change list &lt;refcodes-struct-ext&gt; (version 3.3.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-struct-ext/src/refcodes-struct-ext-3.3.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-struct-ext/src/refcodes-struct-ext-3.3.4/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-struct-ext/src/refcodes-struct-ext-3.3.4/refcodes-struct-ext-factory/pom.xml) 

## Change list &lt;refcodes-component&gt; (version 3.3.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.3.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.3.4/README.md) 

## Change list &lt;refcodes-data-ext&gt; (version 3.3.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-3.3.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-3.3.4/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-3.3.4/refcodes-data-ext-checkers/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-3.3.4/refcodes-data-ext-checkers/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-3.3.4/refcodes-data-ext-chess/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-3.3.4/refcodes-data-ext-chess/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-3.3.4/refcodes-data-ext-corporate/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-3.3.4/refcodes-data-ext-corporate/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-3.3.4/refcodes-data-ext-symbols/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-3.3.4/refcodes-data-ext-symbols/README.md) 

## Change list &lt;refcodes-graphical&gt; (version 3.3.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-graphical/src/refcodes-graphical-3.3.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-graphical/src/refcodes-graphical-3.3.4/README.md) 

## Change list &lt;refcodes-textual&gt; (version 3.3.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-3.3.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-3.3.4/README.md) 

## Change list &lt;refcodes-criteria&gt; (version 3.3.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-3.3.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-3.3.4/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`CriteriaSchema.java`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-3.3.4/src/main/java/org/refcodes/criteria/CriteriaSchema.java) (see Javadoc at [`CriteriaSchema.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-criteria/3.3.4/org.refcodes.criteria/org/refcodes/criteria/CriteriaSchema.html))

## Change list &lt;refcodes-io&gt; (version 3.3.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-io/src/refcodes-io-3.3.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-io/src/refcodes-io-3.3.4/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`AbstractPrefetchInputStreamByteReceiver.java`](https://bitbucket.org/refcodes/refcodes-io/src/refcodes-io-3.3.4/src/main/java/org/refcodes/io/AbstractPrefetchInputStreamByteReceiver.java) (see Javadoc at [`AbstractPrefetchInputStreamByteReceiver.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-io/3.3.4/org.refcodes.io/org/refcodes/io/AbstractPrefetchInputStreamByteReceiver.html))
* \[<span style="color:green">MODIFIED</span>\] [`AbstractPrefetchInputStreamReceiver.java`](https://bitbucket.org/refcodes/refcodes-io/src/refcodes-io-3.3.4/src/main/java/org/refcodes/io/AbstractPrefetchInputStreamReceiver.java) (see Javadoc at [`AbstractPrefetchInputStreamReceiver.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-io/3.3.4/org.refcodes.io/org/refcodes/io/AbstractPrefetchInputStreamReceiver.html))

## Change list &lt;refcodes-tabular&gt; (version 3.3.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-tabular/src/refcodes-tabular-3.3.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-tabular/src/refcodes-tabular-3.3.4/README.md) 

## Change list &lt;refcodes-observer&gt; (version 3.3.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-observer/src/refcodes-observer-3.3.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-observer/src/refcodes-observer-3.3.4/README.md) 

## Change list &lt;refcodes-command&gt; (version 3.3.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-command/src/refcodes-command-3.3.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-command/src/refcodes-command-3.3.4/README.md) 

## Change list &lt;refcodes-cli&gt; (version 3.3.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.3.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.3.4/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`AbstractCondition.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.3.4/src/main/java/org/refcodes/cli/AbstractCondition.java) (see Javadoc at [`AbstractCondition.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/3.3.4/org.refcodes.cli/org/refcodes/cli/AbstractCondition.html))
* \[<span style="color:green">MODIFIED</span>\] [`AbstractTerm.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.3.4/src/main/java/org/refcodes/cli/AbstractTerm.java) (see Javadoc at [`AbstractTerm.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/3.3.4/org.refcodes.cli/org/refcodes/cli/AbstractTerm.html))
* \[<span style="color:green">MODIFIED</span>\] [`AllCondition.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.3.4/src/main/java/org/refcodes/cli/AllCondition.java) (see Javadoc at [`AllCondition.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/3.3.4/org.refcodes.cli/org/refcodes/cli/AllCondition.html))
* \[<span style="color:green">MODIFIED</span>\] [`AndCondition.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.3.4/src/main/java/org/refcodes/cli/AndCondition.java) (see Javadoc at [`AndCondition.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/3.3.4/org.refcodes.cli/org/refcodes/cli/AndCondition.html))
* \[<span style="color:green">MODIFIED</span>\] [`AnyCondition.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.3.4/src/main/java/org/refcodes/cli/AnyCondition.java) (see Javadoc at [`AnyCondition.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/3.3.4/org.refcodes.cli/org/refcodes/cli/AnyCondition.html))
* \[<span style="color:green">MODIFIED</span>\] [`ArgsParser.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.3.4/src/main/java/org/refcodes/cli/ArgsParser.java) (see Javadoc at [`ArgsParser.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/3.3.4/org.refcodes.cli/org/refcodes/cli/ArgsParser.html))
* \[<span style="color:green">MODIFIED</span>\] [`ArrayOperand.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.3.4/src/main/java/org/refcodes/cli/ArrayOperand.java) (see Javadoc at [`ArrayOperand.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/3.3.4/org.refcodes.cli/org/refcodes/cli/ArrayOperand.html))
* \[<span style="color:green">MODIFIED</span>\] [`BooleanOption.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.3.4/src/main/java/org/refcodes/cli/BooleanOption.java) (see Javadoc at [`BooleanOption.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/3.3.4/org.refcodes.cli/org/refcodes/cli/BooleanOption.html))
* \[<span style="color:green">MODIFIED</span>\] [`BooleanProperty.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.3.4/src/main/java/org/refcodes/cli/BooleanProperty.java) (see Javadoc at [`BooleanProperty.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/3.3.4/org.refcodes.cli/org/refcodes/cli/BooleanProperty.html))
* \[<span style="color:green">MODIFIED</span>\] [`CharOption.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.3.4/src/main/java/org/refcodes/cli/CharOption.java) (see Javadoc at [`CharOption.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/3.3.4/org.refcodes.cli/org/refcodes/cli/CharOption.html))
* \[<span style="color:green">MODIFIED</span>\] [`CharProperty.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.3.4/src/main/java/org/refcodes/cli/CharProperty.java) (see Javadoc at [`CharProperty.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/3.3.4/org.refcodes.cli/org/refcodes/cli/CharProperty.html))
* \[<span style="color:green">MODIFIED</span>\] [`CliSchema.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.3.4/src/main/java/org/refcodes/cli/CliSchema.java) (see Javadoc at [`CliSchema.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/3.3.4/org.refcodes.cli/org/refcodes/cli/CliSchema.html))
* \[<span style="color:green">MODIFIED</span>\] [`DoubleOption.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.3.4/src/main/java/org/refcodes/cli/DoubleOption.java) (see Javadoc at [`DoubleOption.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/3.3.4/org.refcodes.cli/org/refcodes/cli/DoubleOption.html))
* \[<span style="color:green">MODIFIED</span>\] [`DoubleProperty.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.3.4/src/main/java/org/refcodes/cli/DoubleProperty.java) (see Javadoc at [`DoubleProperty.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/3.3.4/org.refcodes.cli/org/refcodes/cli/DoubleProperty.html))
* \[<span style="color:green">MODIFIED</span>\] [`EnumOption.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.3.4/src/main/java/org/refcodes/cli/EnumOption.java) (see Javadoc at [`EnumOption.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/3.3.4/org.refcodes.cli/org/refcodes/cli/EnumOption.html))
* \[<span style="color:green">MODIFIED</span>\] [`EnumProperty.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.3.4/src/main/java/org/refcodes/cli/EnumProperty.java) (see Javadoc at [`EnumProperty.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/3.3.4/org.refcodes.cli/org/refcodes/cli/EnumProperty.html))
* \[<span style="color:green">MODIFIED</span>\] [`FileOption.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.3.4/src/main/java/org/refcodes/cli/FileOption.java) (see Javadoc at [`FileOption.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/3.3.4/org.refcodes.cli/org/refcodes/cli/FileOption.html))
* \[<span style="color:green">MODIFIED</span>\] [`FileProperty.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.3.4/src/main/java/org/refcodes/cli/FileProperty.java) (see Javadoc at [`FileProperty.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/3.3.4/org.refcodes.cli/org/refcodes/cli/FileProperty.html))
* \[<span style="color:green">MODIFIED</span>\] [`Flag.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.3.4/src/main/java/org/refcodes/cli/Flag.java) (see Javadoc at [`Flag.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/3.3.4/org.refcodes.cli/org/refcodes/cli/Flag.html))
* \[<span style="color:green">MODIFIED</span>\] [`FloatOption.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.3.4/src/main/java/org/refcodes/cli/FloatOption.java) (see Javadoc at [`FloatOption.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/3.3.4/org.refcodes.cli/org/refcodes/cli/FloatOption.html))
* \[<span style="color:green">MODIFIED</span>\] [`FloatProperty.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.3.4/src/main/java/org/refcodes/cli/FloatProperty.java) (see Javadoc at [`FloatProperty.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/3.3.4/org.refcodes.cli/org/refcodes/cli/FloatProperty.html))
* \[<span style="color:green">MODIFIED</span>\] [`IntOption.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.3.4/src/main/java/org/refcodes/cli/IntOption.java) (see Javadoc at [`IntOption.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/3.3.4/org.refcodes.cli/org/refcodes/cli/IntOption.html))
* \[<span style="color:green">MODIFIED</span>\] [`IntProperty.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.3.4/src/main/java/org/refcodes/cli/IntProperty.java) (see Javadoc at [`IntProperty.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/3.3.4/org.refcodes.cli/org/refcodes/cli/IntProperty.html))
* \[<span style="color:green">MODIFIED</span>\] [`LongOption.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.3.4/src/main/java/org/refcodes/cli/LongOption.java) (see Javadoc at [`LongOption.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/3.3.4/org.refcodes.cli/org/refcodes/cli/LongOption.html))
* \[<span style="color:green">MODIFIED</span>\] [`LongProperty.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.3.4/src/main/java/org/refcodes/cli/LongProperty.java) (see Javadoc at [`LongProperty.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/3.3.4/org.refcodes.cli/org/refcodes/cli/LongProperty.html))
* \[<span style="color:green">MODIFIED</span>\] [`NoneOperand.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.3.4/src/main/java/org/refcodes/cli/NoneOperand.java) (see Javadoc at [`NoneOperand.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/3.3.4/org.refcodes.cli/org/refcodes/cli/NoneOperand.html))
* \[<span style="color:green">MODIFIED</span>\] [`Operation.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.3.4/src/main/java/org/refcodes/cli/Operation.java) (see Javadoc at [`Operation.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/3.3.4/org.refcodes.cli/org/refcodes/cli/Operation.html))
* \[<span style="color:green">MODIFIED</span>\] [`OrCondition.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.3.4/src/main/java/org/refcodes/cli/OrCondition.java) (see Javadoc at [`OrCondition.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/3.3.4/org.refcodes.cli/org/refcodes/cli/OrCondition.html))
* \[<span style="color:green">MODIFIED</span>\] [`StringOperand.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.3.4/src/main/java/org/refcodes/cli/StringOperand.java) (see Javadoc at [`StringOperand.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/3.3.4/org.refcodes.cli/org/refcodes/cli/StringOperand.html))
* \[<span style="color:green">MODIFIED</span>\] [`StringOption.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.3.4/src/main/java/org/refcodes/cli/StringOption.java) (see Javadoc at [`StringOption.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/3.3.4/org.refcodes.cli/org/refcodes/cli/StringOption.html))
* \[<span style="color:green">MODIFIED</span>\] [`StringProperty.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.3.4/src/main/java/org/refcodes/cli/StringProperty.java) (see Javadoc at [`StringProperty.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/3.3.4/org.refcodes.cli/org/refcodes/cli/StringProperty.html))
* \[<span style="color:green">MODIFIED</span>\] [`Term.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.3.4/src/main/java/org/refcodes/cli/Term.java) (see Javadoc at [`Term.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/3.3.4/org.refcodes.cli/org/refcodes/cli/Term.html))
* \[<span style="color:green">MODIFIED</span>\] [`XorCondition.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.3.4/src/main/java/org/refcodes/cli/XorCondition.java) (see Javadoc at [`XorCondition.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/3.3.4/org.refcodes.cli/org/refcodes/cli/XorCondition.html))
* \[<span style="color:green">MODIFIED</span>\] [`ArgsParserTest.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.3.4/src/test/java/org/refcodes/cli/ArgsParserTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`SyntaxNotationTest.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.3.4/src/test/java/org/refcodes/cli/SyntaxNotationTest.java) 

## Change list &lt;refcodes-audio&gt; (version 3.3.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-audio/src/refcodes-audio-3.3.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-audio/src/refcodes-audio-3.3.4/README.md) 

## Change list &lt;refcodes-codec&gt; (version 3.3.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-3.3.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-3.3.4/README.md) 

## Change list &lt;refcodes-component-ext&gt; (version 3.3.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-3.3.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-3.3.4/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-3.3.4/refcodes-component-ext-observer/pom.xml) 

## Change list &lt;refcodes-properties&gt; (version 3.3.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-3.3.4/pom.xml) 

## Change list &lt;refcodes-security&gt; (version 3.3.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security/src/refcodes-security-3.3.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security/src/refcodes-security-3.3.4/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`PasswordTextEncryptionTest.java`](https://bitbucket.org/refcodes/refcodes-security/src/refcodes-security-3.3.4/src/test/java/org/refcodes/security/PasswordTextEncryptionTest.java) 

## Change list &lt;refcodes-security-alt&gt; (version 3.3.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-3.3.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-3.3.4/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-3.3.4/refcodes-security-alt-chaos/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-3.3.4/refcodes-security-alt-chaos/README.md) 

## Change list &lt;refcodes-security-ext&gt; (version 3.3.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-3.3.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-3.3.4/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-3.3.4/refcodes-security-ext-chaos/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-3.3.4/refcodes-security-ext-chaos/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-3.3.4/refcodes-security-ext-spring/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-3.3.4/refcodes-security-ext-spring/README.md) 

## Change list &lt;refcodes-properties-ext&gt; (version 3.3.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.3.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.3.4/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.3.4/refcodes-properties-ext-application/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.3.4/refcodes-properties-ext-application/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.3.4/refcodes-properties-ext-cli/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.3.4/refcodes-properties-ext-cli/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.3.4/refcodes-properties-ext-obfuscation/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.3.4/refcodes-properties-ext-obfuscation/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.3.4/refcodes-properties-ext-observer/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.3.4/refcodes-properties-ext-observer/README.md) 

## Change list &lt;refcodes-logger&gt; (version 3.3.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger/src/refcodes-logger-3.3.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger/src/refcodes-logger-3.3.4/README.md) 

## Change list &lt;refcodes-logger-alt&gt; (version 3.3.4)

* \[<span style="color:blue">ADDED</span>\] [`module-info.java`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.3.4/refcodes-logger-alt-simpledb/src/main/java/module-info.java) (see Javadoc at [`module-info.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-logger-alt-simpledb/3.3.4//module-info.html))
* \[<span style="color:red">DELETED</span>\] `module-info.java.off`
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.3.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.3.4/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.3.4/refcodes-logger-alt-async/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.3.4/refcodes-logger-alt-async/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.3.4/refcodes-logger-alt-console/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.3.4/refcodes-logger-alt-console/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.3.4/refcodes-logger-alt-io/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.3.4/refcodes-logger-alt-io/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.3.4/refcodes-logger-alt-simpledb/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.3.4/refcodes-logger-alt-simpledb/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.3.4/refcodes-logger-alt-slf4j-legacy/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.3.4/refcodes-logger-alt-slf4j/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.3.4/refcodes-logger-alt-spring/pom.xml) 

## Change list &lt;refcodes-logger-ext&gt; (version 3.3.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-3.3.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-3.3.4/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-3.3.4/refcodes-logger-ext-slf4j-legacy/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-3.3.4/refcodes-logger-ext-slf4j-legacy/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-3.3.4/refcodes-logger-ext-slf4j/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-3.3.4/refcodes-logger-ext-slf4j/README.md) 

## Change list &lt;refcodes-graphical-ext&gt; (version 3.3.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-3.3.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-3.3.4/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-3.3.4/refcodes-graphical-ext-javafx/pom.xml) 

## Change list &lt;refcodes-checkerboard&gt; (version 3.3.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-3.3.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-3.3.4/README.md) 

## Change list &lt;refcodes-checkerboard-alt&gt; (version 3.3.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-alt/src/refcodes-checkerboard-alt-3.3.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-alt/src/refcodes-checkerboard-alt-3.3.4/refcodes-checkerboard-alt-javafx/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`FxCheckerboardViewer.java`](https://bitbucket.org/refcodes/refcodes-checkerboard-alt/src/refcodes-checkerboard-alt-3.3.4/refcodes-checkerboard-alt-javafx/src/main/java/org/refcodes/checkerboard/alt/javafx/FxCheckerboardViewer.java) (see Javadoc at [`FxCheckerboardViewer.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-checkerboard-alt-javafx/3.3.4/org.refcodes.checkerboard.alt.javafx/org/refcodes/checkerboard/alt/javafx/FxCheckerboardViewer.html))

## Change list &lt;refcodes-net&gt; (version 3.3.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-3.3.4/pom.xml) 

## Change list &lt;refcodes-web&gt; (version 3.3.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-web/src/refcodes-web-3.3.4/pom.xml) 

## Change list &lt;refcodes-rest&gt; (version 3.3.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-3.3.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-3.3.4/README.md) 

## Change list &lt;refcodes-hal&gt; (version 3.3.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-3.3.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-3.3.4/README.md) 

## Change list &lt;refcodes-eventbus&gt; (version 3.3.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-eventbus/src/refcodes-eventbus-3.3.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-eventbus/src/refcodes-eventbus-3.3.4/README.md) 

## Change list &lt;refcodes-eventbus-ext&gt; (version 3.3.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-3.3.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-3.3.4/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-3.3.4/refcodes-eventbus-ext-application/pom.xml) 

## Change list &lt;refcodes-decoupling&gt; (version 3.3.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-decoupling/src/refcodes-decoupling-3.3.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-decoupling/src/refcodes-decoupling-3.3.4/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`DependencySchema.java`](https://bitbucket.org/refcodes/refcodes-decoupling/src/refcodes-decoupling-3.3.4/src/main/java/org/refcodes/decoupling/DependencySchema.java) (see Javadoc at [`DependencySchema.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-decoupling/3.3.4/org.refcodes.decoupling/org/refcodes/decoupling/DependencySchema.html))

## Change list &lt;refcodes-decoupling-ext&gt; (version 3.3.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-decoupling-ext/src/refcodes-decoupling-ext-3.3.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-decoupling-ext/src/refcodes-decoupling-ext-3.3.4/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-decoupling-ext/src/refcodes-decoupling-ext-3.3.4/refcodes-decoupling-ext-application/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-decoupling-ext/src/refcodes-decoupling-ext-3.3.4/refcodes-decoupling-ext-application/README.md) 

## Change list &lt;refcodes-filesystem&gt; (version 3.3.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-filesystem/src/refcodes-filesystem-3.3.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-filesystem/src/refcodes-filesystem-3.3.4/README.md) 

## Change list &lt;refcodes-filesystem-alt&gt; (version 3.3.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-filesystem-alt/src/refcodes-filesystem-alt-3.3.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-filesystem-alt/src/refcodes-filesystem-alt-3.3.4/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-filesystem-alt/src/refcodes-filesystem-alt-3.3.4/refcodes-filesystem-alt-s3/pom.xml) 

## Change list &lt;refcodes-forwardsecrecy&gt; (version 3.3.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy/src/refcodes-forwardsecrecy-3.3.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy/src/refcodes-forwardsecrecy-3.3.4/README.md) 

## Change list &lt;refcodes-forwardsecrecy-alt&gt; (version 3.3.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-3.3.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-3.3.4/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-3.3.4/refcodes-forwardsecrecy-alt-filesystem/pom.xml) 

## Change list &lt;refcodes-io-ext&gt; (version 3.3.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-3.3.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-3.3.4/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-3.3.4/refcodes-io-ext-observer/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-3.3.4/refcodes-io-ext-observer/README.md) 

## Change list &lt;refcodes-jobbus&gt; (version 3.3.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-jobbus/src/refcodes-jobbus-3.3.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-jobbus/src/refcodes-jobbus-3.3.4/README.md) 

## Change list &lt;refcodes-rest-ext&gt; (version 3.3.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-3.3.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-3.3.4/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-3.3.4/refcodes-rest-ext-eureka/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-3.3.4/refcodes-rest-ext-eureka/README.md) 

## Change list &lt;refcodes-remoting&gt; (version 3.3.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-remoting/src/refcodes-remoting-3.3.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-remoting/src/refcodes-remoting-3.3.4/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`AbstractRemote.java`](https://bitbucket.org/refcodes/refcodes-remoting/src/refcodes-remoting-3.3.4/src/main/java/org/refcodes/remoting/AbstractRemote.java) (see Javadoc at [`AbstractRemote.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-remoting/3.3.4/org.refcodes.remoting/org/refcodes/remoting/AbstractRemote.html))
* \[<span style="color:green">MODIFIED</span>\] [`RemoteClient.java`](https://bitbucket.org/refcodes/refcodes-remoting/src/refcodes-remoting-3.3.4/src/main/java/org/refcodes/remoting/RemoteClient.java) (see Javadoc at [`RemoteClient.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-remoting/3.3.4/org.refcodes.remoting/org/refcodes/remoting/RemoteClient.html))

## Change list &lt;refcodes-remoting-ext&gt; (version 3.3.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-3.3.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-3.3.4/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-3.3.4/refcodes-remoting-ext-observer/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-3.3.4/refcodes-remoting-ext-observer/README.md) 

## Change list &lt;refcodes-serial&gt; (version 3.3.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial/src/refcodes-serial-3.3.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-serial/src/refcodes-serial-3.3.4/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`SerialSchema.java`](https://bitbucket.org/refcodes/refcodes-serial/src/refcodes-serial-3.3.4/src/main/java/org/refcodes/serial/SerialSchema.java) (see Javadoc at [`SerialSchema.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-serial/3.3.4/org.refcodes.serial/org/refcodes/serial/SerialSchema.html))
* \[<span style="color:green">MODIFIED</span>\] [`AllocSegmentTest.java`](https://bitbucket.org/refcodes/refcodes-serial/src/refcodes-serial-3.3.4/src/test/java/org/refcodes/serial/AllocSegmentTest.java) 

## Change list &lt;refcodes-serial-alt&gt; (version 3.3.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial-alt/src/refcodes-serial-alt-3.3.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-serial-alt/src/refcodes-serial-alt-3.3.4/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial-alt/src/refcodes-serial-alt-3.3.4/refcodes-serial-alt-tty/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-serial-alt/src/refcodes-serial-alt-3.3.4/refcodes-serial-alt-tty/README.md) 

## Change list &lt;refcodes-serial-ext&gt; (version 3.3.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-3.3.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-3.3.4/refcodes-serial-ext-handshake/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-3.3.4/refcodes-serial-ext-observer/pom.xml) 

## Change list &lt;refcodes-p2p&gt; (version 3.3.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-p2p/src/refcodes-p2p-3.3.4/pom.xml) 

## Change list &lt;refcodes-p2p-alt&gt; (version 3.3.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-p2p-alt/src/refcodes-p2p-alt-3.3.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-p2p-alt/src/refcodes-p2p-alt-3.3.4/refcodes-p2p-alt-serial/pom.xml) 

## Change list &lt;refcodes-p2p-ext&gt; (version 3.3.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-p2p-ext/src/refcodes-p2p-ext-3.3.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-p2p-ext/src/refcodes-p2p-ext-3.3.4/refcodes-p2p-ext-observer/pom.xml) 

## Change list &lt;refcodes-tabular-alt&gt; (version 3.3.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-tabular-alt/src/refcodes-tabular-alt-3.3.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-tabular-alt/src/refcodes-tabular-alt-3.3.4/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-tabular-alt/src/refcodes-tabular-alt-3.3.4/refcodes-tabular-alt-forwardsecrecy/pom.xml) 

## Change list &lt;refcodes-archetype&gt; (version 3.3.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype/src/refcodes-archetype-3.3.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype/src/refcodes-archetype-3.3.4/README.md) 

## Change list &lt;refcodes-archetype-alt&gt; (version 3.3.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.4/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.4/refcodes-archetype-alt-c2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.4/refcodes-archetype-alt-c2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.4/refcodes-archetype-alt-c2/src/main/resources/archetype-resources/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.4/refcodes-archetype-alt-cli/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.4/refcodes-archetype-alt-cli/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.4/refcodes-archetype-alt-cli/src/main/resources/archetype-resources/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.4/refcodes-archetype-alt-csv/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.4/refcodes-archetype-alt-csv/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.4/refcodes-archetype-alt-csv/src/main/resources/archetype-resources/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.4/refcodes-archetype-alt-decoupling/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.4/refcodes-archetype-alt-decoupling/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.4/refcodes-archetype-alt-decoupling/src/main/resources/archetype-resources/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.4/refcodes-archetype-alt-eventbus/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.4/refcodes-archetype-alt-eventbus/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.4/refcodes-archetype-alt-eventbus/src/main/resources/archetype-resources/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.4/refcodes-archetype-alt-filter/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.4/refcodes-archetype-alt-filter/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.4/refcodes-archetype-alt-filter/src/main/resources/archetype-resources/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.4/refcodes-archetype-alt-rest/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.4/refcodes-archetype-alt-rest/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.4/refcodes-archetype-alt-rest/src/main/resources/archetype-resources/pom.xml) 
