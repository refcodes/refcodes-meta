# Change list for REFCODES.ORG artifacts' version 1.0.4

> This change list has been auto-generated on `triton` by `steiner` with with `changelist-all.sh` on the 2017-04-26 at 21:49:06.

## Change list &lt;refcodes-licensing&gt; (version 1.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-licensing/src/refcodes-licensing-1.0.4/pom.xml) 

## Change list &lt;refcodes-parent&gt; (version 1.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-parent/src/refcodes-parent-1.0.4/pom.xml) 

## Change list &lt;refcodes-time&gt; (version 1.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-time/src/refcodes-time-1.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`log4j.xml`](https://bitbucket.org/refcodes/refcodes-time/src/refcodes-time-1.0.4/src/test/resources/log4j.xml) 

## Change list &lt;refcodes-exception&gt; (version 1.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-exception/src/refcodes-exception-1.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`log4j.xml`](https://bitbucket.org/refcodes/refcodes-exception/src/refcodes-exception-1.0.4/src/test/resources/log4j.xml) 

## Change list &lt;refcodes-mixin&gt; (version 1.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-mixin/src/refcodes-mixin-1.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`log4j.xml`](https://bitbucket.org/refcodes/refcodes-mixin/src/refcodes-mixin-1.0.4/src/test/resources/log4j.xml) 

## Change list &lt;refcodes-data&gt; (version 1.0.4)

* \[<span style="color:blue">ADDED</span>\] [`BooleanConsts.java`](https://bitbucket.org/refcodes/refcodes-data/src/refcodes-data-1.0.4/src/main/java/org/refcodes/data/BooleanConsts.java) (see Javadoc at [`BooleanConsts.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-data/1.0.4/org/refcodes/data/BooleanConsts.html))
* \[<span style="color:red">DELETED</span>\] `AnsiEscapeTest.java`
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data/src/refcodes-data-1.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`log4j.xml`](https://bitbucket.org/refcodes/refcodes-data/src/refcodes-data-1.0.4/src/test/resources/log4j.xml) 

## Change list &lt;refcodes-controlflow&gt; (version 1.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-controlflow/src/refcodes-controlflow-1.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`log4j.xml`](https://bitbucket.org/refcodes/refcodes-controlflow/src/refcodes-controlflow-1.0.4/src/test/resources/log4j.xml) 

## Change list &lt;refcodes-numerical&gt; (version 1.0.4)

* \[<span style="color:blue">ADDED</span>\] [`NumberBaseBuilderImpl.java`](https://bitbucket.org/refcodes/refcodes-numerical/src/refcodes-numerical-1.0.4/src/main/java/org/refcodes/numerical/NumberBaseBuilderImpl.java) (see Javadoc at [`NumberBaseBuilderImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-numerical/1.0.4/org/refcodes/numerical/NumberBaseBuilderImpl.html))
* \[<span style="color:blue">ADDED</span>\] [`NumberBaseAccessor.java`](https://bitbucket.org/refcodes/refcodes-numerical/src/refcodes-numerical-1.0.4/src/main/java/org/refcodes/numerical/NumberBaseAccessor.java) (see Javadoc at [`NumberBaseAccessor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-numerical/1.0.4/org/refcodes/numerical/NumberBaseAccessor.html))
* \[<span style="color:blue">ADDED</span>\] [`NumberBaseBuilder.java`](https://bitbucket.org/refcodes/refcodes-numerical/src/refcodes-numerical-1.0.4/src/main/java/org/refcodes/numerical/NumberBaseBuilder.java) (see Javadoc at [`NumberBaseBuilder.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-numerical/1.0.4/org/refcodes/numerical/NumberBaseBuilder.html))
* \[<span style="color:blue">ADDED</span>\] [`NumberBase.java`](https://bitbucket.org/refcodes/refcodes-numerical/src/refcodes-numerical-1.0.4/src/main/java/org/refcodes/numerical/NumberBase.java) (see Javadoc at [`NumberBase.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-numerical/1.0.4/org/refcodes/numerical/NumberBase.html))
* \[<span style="color:blue">ADDED</span>\] [`package-info.java`](https://bitbucket.org/refcodes/refcodes-numerical/src/refcodes-numerical-1.0.4/src/main/java/org/refcodes/numerical/package-info.java) (see Javadoc at [`package-info.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-numerical/1.0.4/org/refcodes/numerical/package-info.html))
* \[<span style="color:blue">ADDED</span>\] [`PaddingCharAccessor.java`](https://bitbucket.org/refcodes/refcodes-numerical/src/refcodes-numerical-1.0.4/src/main/java/org/refcodes/numerical/PaddingCharAccessor.java) (see Javadoc at [`PaddingCharAccessor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-numerical/1.0.4/org/refcodes/numerical/PaddingCharAccessor.html))
* \[<span style="color:blue">ADDED</span>\] [`NumberBaseBuilderTest.java`](https://bitbucket.org/refcodes/refcodes-numerical/src/refcodes-numerical-1.0.4/src/test/java/org/refcodes/numerical/NumberBaseBuilderTest.java) 
* \[<span style="color:blue">ADDED</span>\] [`log4j.xml`](https://bitbucket.org/refcodes/refcodes-numerical/src/refcodes-numerical-1.0.4/src/test/resources/log4j.xml) 
* \[<span style="color:red">DELETED</span>\] `package-info.java`
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-numerical/src/refcodes-numerical-1.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`NumericalUtility.java`](https://bitbucket.org/refcodes/refcodes-numerical/src/refcodes-numerical-1.0.4/src/main/java/org/refcodes/numerical/NumericalUtility.java) (see Javadoc at [`NumericalUtility.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-numerical/1.0.4/org/refcodes/numerical/NumericalUtility.html))
* \[<span style="color:green">MODIFIED</span>\] [`NumericalUtilityTest.java`](https://bitbucket.org/refcodes/refcodes-numerical/src/refcodes-numerical-1.0.4/src/test/java/org/refcodes/numerical/NumericalUtilityTest.java) 

## Change list &lt;refcodes-generator&gt; (version 1.0.4)

* \[<span style="color:blue">ADDED</span>\] [`package-info.java`](https://bitbucket.org/refcodes/refcodes-generator/src/refcodes-generator-1.0.4/src/main/java/org/refcodes/generator/package-info.java) (see Javadoc at [`package-info.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-generator/1.0.4/org/refcodes/generator/package-info.html))
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-generator/src/refcodes-generator-1.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`UniqueIdGeneratorImpl.java`](https://bitbucket.org/refcodes/refcodes-generator/src/refcodes-generator-1.0.4/src/main/java/org/refcodes/generator/UniqueIdGeneratorImpl.java) (see Javadoc at [`UniqueIdGeneratorImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-generator/1.0.4/org/refcodes/generator/UniqueIdGeneratorImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`UniqueIdGeneratorTest.java`](https://bitbucket.org/refcodes/refcodes-generator/src/refcodes-generator-1.0.4/src/test/java/org/refcodes/generator/UniqueIdGeneratorTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`log4j.xml`](https://bitbucket.org/refcodes/refcodes-generator/src/refcodes-generator-1.0.4/src/test/resources/log4j.xml) 

## Change list &lt;refcodes-collection&gt; (version 1.0.4)

* \[<span style="color:blue">ADDED</span>\] [`Attribute.java`](https://bitbucket.org/refcodes/refcodes-collection/src/refcodes-collection-1.0.4/src/main/java/org/refcodes/collection/Attribute.java) (see Javadoc at [`Attribute.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-collection/1.0.4/org/refcodes/collection/Attribute.html))
* \[<span style="color:blue">ADDED</span>\] [`AttributeImpl.java`](https://bitbucket.org/refcodes/refcodes-collection/src/refcodes-collection-1.0.4/src/main/java/org/refcodes/collection/AttributeImpl.java) (see Javadoc at [`AttributeImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-collection/1.0.4/org/refcodes/collection/AttributeImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-collection/src/refcodes-collection-1.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`log4j.xml`](https://bitbucket.org/refcodes/refcodes-collection/src/refcodes-collection-1.0.4/src/test/resources/log4j.xml) 

## Change list &lt;refcodes-runtime&gt; (version 1.0.4)

* \[<span style="color:red">DELETED</span>\] `Attribute.java`
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-1.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`ReflectionUtility.java`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-1.0.4/src/main/java/org/refcodes/runtime/ReflectionUtility.java) (see Javadoc at [`ReflectionUtility.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-runtime/1.0.4/org/refcodes/runtime/ReflectionUtility.html))
* \[<span style="color:green">MODIFIED</span>\] [`SystemUtilityTest.java`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-1.0.4/src/test/java/org/refcodes/runtime/SystemUtilityTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`log4j.xml`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-1.0.4/src/test/resources/log4j.xml) 

## Change list &lt;refcodes-component&gt; (version 1.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-1.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`log4j.xml`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-1.0.4/src/test/resources/log4j.xml) 

## Change list &lt;refcodes-interceptor&gt; (version 1.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-interceptor/src/refcodes-interceptor-1.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-interceptor/src/refcodes-interceptor-1.0.4/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`log4j.xml`](https://bitbucket.org/refcodes/refcodes-interceptor/src/refcodes-interceptor-1.0.4/src/test/resources/log4j.xml) 

## Change list &lt;refcodes-factory&gt; (version 1.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-factory/src/refcodes-factory-1.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`log4j.xml`](https://bitbucket.org/refcodes/refcodes-factory/src/refcodes-factory-1.0.4/src/test/resources/log4j.xml) 

## Change list &lt;refcodes-data-ext&gt; (version 1.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.0.4/refcodes-data-ext-boulderdash/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.0.4/refcodes-data-ext-boulderdash/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.0.4/refcodes-data-ext-checkers/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.0.4/refcodes-data-ext-checkers/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.0.4/refcodes-data-ext-chess/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.0.4/refcodes-data-ext-chess/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.0.4/refcodes-data-ext-corporate/pom.xml) 

## Change list &lt;refcodes-graphical&gt; (version 1.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-graphical/src/refcodes-graphical-1.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`BoxBorderModeTest.java`](https://bitbucket.org/refcodes/refcodes-graphical/src/refcodes-graphical-1.0.4/src/test/java/org/refcodes/graphical/BoxBorderModeTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`RgbPixmapImageBuilderTest.java`](https://bitbucket.org/refcodes/refcodes-graphical/src/refcodes-graphical-1.0.4/src/test/java/org/refcodes/graphical/RgbPixmapImageBuilderTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`log4j.xml`](https://bitbucket.org/refcodes/refcodes-graphical/src/refcodes-graphical-1.0.4/src/test/resources/log4j.xml) 

## Change list &lt;refcodes-textual&gt; (version 1.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-1.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`CsvBuilder.java`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-1.0.4/src/main/java/org/refcodes/textual/CsvBuilder.java) (see Javadoc at [`CsvBuilder.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-textual/1.0.4/org/refcodes/textual/CsvBuilder.html))
* \[<span style="color:green">MODIFIED</span>\] [`CsvBuilderImpl.java`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-1.0.4/src/main/java/org/refcodes/textual/CsvBuilderImpl.java) (see Javadoc at [`CsvBuilderImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-textual/1.0.4/org/refcodes/textual/CsvBuilderImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`VerboseTextBuilderImpl.java`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-1.0.4/src/main/java/org/refcodes/textual/VerboseTextBuilderImpl.java) (see Javadoc at [`VerboseTextBuilderImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-textual/1.0.4/org/refcodes/textual/VerboseTextBuilderImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`VerboseTextBuilder.java`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-1.0.4/src/main/java/org/refcodes/textual/VerboseTextBuilder.java) (see Javadoc at [`VerboseTextBuilder.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-textual/1.0.4/org/refcodes/textual/VerboseTextBuilder.html))
* \[<span style="color:green">MODIFIED</span>\] [`AsciiArtBuilderTest.java`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-1.0.4/src/test/java/org/refcodes/textual/AsciiArtBuilderTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`CsvBuilderTest.java`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-1.0.4/src/test/java/org/refcodes/textual/CsvBuilderTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`RandomTextGeneratorTest.java`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-1.0.4/src/test/java/org/refcodes/textual/RandomTextGeneratorTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`VerboseTextBuilderTest.java`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-1.0.4/src/test/java/org/refcodes/textual/VerboseTextBuilderTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`log4j.xml`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-1.0.4/src/test/resources/log4j.xml) 

## Change list &lt;refcodes-criteria&gt; (version 1.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-1.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-1.0.4/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`log4j.xml`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-1.0.4/src/test/resources/log4j.xml) 

## Change list &lt;refcodes-tabular&gt; (version 1.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-tabular/src/refcodes-tabular-1.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`ColumnImpl.java`](https://bitbucket.org/refcodes/refcodes-tabular/src/refcodes-tabular-1.0.4/src/main/java/org/refcodes/tabular/ColumnImpl.java) (see Javadoc at [`ColumnImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-tabular/1.0.4/org/refcodes/tabular/ColumnImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`log4j.xml`](https://bitbucket.org/refcodes/refcodes-tabular/src/refcodes-tabular-1.0.4/src/test/resources/log4j.xml) 

## Change list &lt;refcodes-logger&gt; (version 1.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger/src/refcodes-logger-1.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger/src/refcodes-logger-1.0.4/README.md) 

## Change list &lt;refcodes-factory-alt&gt; (version 1.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-factory-alt/src/refcodes-factory-alt-1.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-factory-alt/src/refcodes-factory-alt-1.0.4/refcodes-factory-alt-spring/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`log4j.xml`](https://bitbucket.org/refcodes/refcodes-factory-alt/src/refcodes-factory-alt-1.0.4/refcodes-factory-alt-spring/src/test/resources/log4j.xml) 

## Change list &lt;refcodes-logger-alt&gt; (version 1.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.0.4/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.0.4/refcodes-logger-alt-async/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.0.4/refcodes-logger-alt-async/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`log4j.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.0.4/refcodes-logger-alt-async/src/test/resources/log4j.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.0.4/refcodes-logger-alt-console/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.0.4/refcodes-logger-alt-console/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`log4j.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.0.4/refcodes-logger-alt-console/src/test/resources/log4j.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.0.4/refcodes-logger-alt-io/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.0.4/refcodes-logger-alt-io/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`log4j.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.0.4/refcodes-logger-alt-io/src/test/resources/log4j.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.0.4/refcodes-logger-alt-simpledb/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.0.4/refcodes-logger-alt-slf4j/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.0.4/refcodes-logger-alt-spring/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`log4j.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.0.4/refcodes-logger-alt-spring/src/test/resources/log4j.xml) 

## Change list &lt;refcodes-matcher&gt; (version 1.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-matcher/src/refcodes-matcher-1.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`log4j.xml`](https://bitbucket.org/refcodes/refcodes-matcher/src/refcodes-matcher-1.0.4/src/test/resources/log4j.xml) 

## Change list &lt;refcodes-observer&gt; (version 1.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-observer/src/refcodes-observer-1.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`log4j.xml`](https://bitbucket.org/refcodes/refcodes-observer/src/refcodes-observer-1.0.4/src/test/resources/log4j.xml) 

## Change list &lt;refcodes-checkerboard&gt; (version 1.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-1.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-1.0.4/README.md) 

## Change list &lt;refcodes-graphical-ext&gt; (version 1.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-1.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-1.0.4/refcodes-graphical-ext-javafx/pom.xml) 

## Change list &lt;refcodes-checkerboard-alt&gt; (version 1.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-alt/src/refcodes-checkerboard-alt-1.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-alt/src/refcodes-checkerboard-alt-1.0.4/refcodes-checkerboard-alt-javafx/pom.xml) 

## Change list &lt;refcodes-io&gt; (version 1.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-io/src/refcodes-io-1.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`log4j.xml`](https://bitbucket.org/refcodes/refcodes-io/src/refcodes-io-1.0.4/src/test/resources/log4j.xml) 

## Change list &lt;refcodes-codec&gt; (version 1.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-1.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-1.0.4/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`BaseCodecBuilder.java`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-1.0.4/src/main/java/org/refcodes/codec/BaseCodecBuilder.java) (see Javadoc at [`BaseCodecBuilder.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-codec/1.0.4/org/refcodes/codec/BaseCodecBuilder.html))
* \[<span style="color:green">MODIFIED</span>\] [`BaseCodecConfig.java`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-1.0.4/src/main/java/org/refcodes/codec/BaseCodecConfig.java) (see Javadoc at [`BaseCodecConfig.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-codec/1.0.4/org/refcodes/codec/BaseCodecConfig.html))
* \[<span style="color:green">MODIFIED</span>\] [`BaseCodecMetrics.java`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-1.0.4/src/main/java/org/refcodes/codec/BaseCodecMetrics.java) (see Javadoc at [`BaseCodecMetrics.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-codec/1.0.4/org/refcodes/codec/BaseCodecMetrics.html))
* \[<span style="color:green">MODIFIED</span>\] [`BaseCodecBuilderImpl.java`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-1.0.4/src/main/java/org/refcodes/codec/BaseCodecBuilderImpl.java) (see Javadoc at [`BaseCodecBuilderImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-codec/1.0.4/org/refcodes/codec/BaseCodecBuilderImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`BaseCodecMetricsImpl.java`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-1.0.4/src/main/java/org/refcodes/codec/BaseCodecMetricsImpl.java) (see Javadoc at [`BaseCodecMetricsImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-codec/1.0.4/org/refcodes/codec/BaseCodecMetricsImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`BaseDecodeReceiverImpl.java`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-1.0.4/src/main/java/org/refcodes/codec/BaseDecodeReceiverImpl.java) (see Javadoc at [`BaseDecodeReceiverImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-codec/1.0.4/org/refcodes/codec/BaseDecodeReceiverImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`BaseEncodeSenderImpl.java`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-1.0.4/src/main/java/org/refcodes/codec/BaseEncodeSenderImpl.java) (see Javadoc at [`BaseEncodeSenderImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-codec/1.0.4/org/refcodes/codec/BaseEncodeSenderImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`BaseCodecBuilderTest.java`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-1.0.4/src/test/java/org/refcodes/codec/BaseCodecBuilderTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`BaseDecodeInputStreamReceiverTest.java`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-1.0.4/src/test/java/org/refcodes/codec/BaseDecodeInputStreamReceiverTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`BaseEncodeOutputStreamSenderTest.java`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-1.0.4/src/test/java/org/refcodes/codec/BaseEncodeOutputStreamSenderTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`log4j.xml`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-1.0.4/src/test/resources/log4j.xml) 

## Change list &lt;refcodes-command&gt; (version 1.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-command/src/refcodes-command-1.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-command/src/refcodes-command-1.0.4/README.md) 

## Change list &lt;refcodes-component-ext&gt; (version 1.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.0.4/refcodes-component-ext-observer/pom.xml) 

## Change list &lt;refcodes-cli&gt; (version 1.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-1.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-1.0.4/README.md) 

## Change list &lt;refcodes-security&gt; (version 1.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security/src/refcodes-security-1.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security/src/refcodes-security-1.0.4/README.md) 

## Change list &lt;refcodes-forwardsecrecy&gt; (version 1.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy/src/refcodes-forwardsecrecy-1.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy/src/refcodes-forwardsecrecy-1.0.4/README.md) 

## Change list &lt;refcodes-filesystem&gt; (version 1.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-filesystem/src/refcodes-filesystem-1.0.4/pom.xml) 

## Change list &lt;refcodes-forwardsecrecy-alt&gt; (version 1.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-1.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-1.0.4/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-1.0.4/refcodes-forwardsecrecy-alt-filesystem/pom.xml) 

## Change list &lt;refcodes-eventbus&gt; (version 1.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-eventbus/src/refcodes-eventbus-1.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-eventbus/src/refcodes-eventbus-1.0.4/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`log4j.xml`](https://bitbucket.org/refcodes/refcodes-eventbus/src/refcodes-eventbus-1.0.4/src/test/resources/log4j.xml) 

## Change list &lt;refcodes-filesystem-alt&gt; (version 1.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-filesystem-alt/src/refcodes-filesystem-alt-1.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-filesystem-alt/src/refcodes-filesystem-alt-1.0.4/refcodes-filesystem-alt-s3/pom.xml) 

## Change list &lt;refcodes-io-ext&gt; (version 1.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-1.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-1.0.4/refcodes-io-ext-observable/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`log4j.xml`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-1.0.4/refcodes-io-ext-observable/src/test/resources/log4j.xml) 

## Change list &lt;refcodes-jobbus&gt; (version 1.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-jobbus/src/refcodes-jobbus-1.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-jobbus/src/refcodes-jobbus-1.0.4/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`log4j.xml`](https://bitbucket.org/refcodes/refcodes-jobbus/src/refcodes-jobbus-1.0.4/src/test/resources/log4j.xml) 

## Change list &lt;refcodes-logger-ext&gt; (version 1.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-1.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-1.0.4/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-1.0.4/refcodes-logger-ext-slf4j/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-1.0.4/refcodes-logger-ext-slf4j/README.md) 

## Change list &lt;refcodes-remoting&gt; (version 1.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-remoting/src/refcodes-remoting-1.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-remoting/src/refcodes-remoting-1.0.4/README.md) 

## Change list &lt;refcodes-remoting-ext&gt; (version 1.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-1.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-1.0.4/refcodes-remoting-ext-observer/pom.xml) 

## Change list &lt;refcodes-security-alt&gt; (version 1.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-1.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-1.0.4/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-1.0.4/refcodes-security-alt-chaos/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-1.0.4/refcodes-security-alt-chaos/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`ChaosTextDecrypterImpl.java`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-1.0.4/refcodes-security-alt-chaos/src/main/java/org/refcodes/security/alt/chaos/ChaosTextDecrypterImpl.java) (see Javadoc at [`ChaosTextDecrypterImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-security-alt-chaos/1.0.4/org/refcodes/security/alt/chaos/ChaosTextDecrypterImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`ChaosTextEncrypterImpl.java`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-1.0.4/refcodes-security-alt-chaos/src/main/java/org/refcodes/security/alt/chaos/ChaosTextEncrypterImpl.java) (see Javadoc at [`ChaosTextEncrypterImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-security-alt-chaos/1.0.4/org/refcodes/security/alt/chaos/ChaosTextEncrypterImpl.html))

## Change list &lt;refcodes-security-ext&gt; (version 1.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-1.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-1.0.4/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-1.0.4/refcodes-security-ext-chaos/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-1.0.4/refcodes-security-ext-chaos/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-1.0.4/refcodes-security-ext-spring/pom.xml) 

## Change list &lt;refcodes-servicebus&gt; (version 1.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-servicebus/src/refcodes-servicebus-1.0.4/pom.xml) 

## Change list &lt;refcodes-servicebus-alt&gt; (version 1.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-servicebus-alt/src/refcodes-servicebus-alt-1.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-servicebus-alt/src/refcodes-servicebus-alt-1.0.4/refcodes-servicebus-alt-spring/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`log4j.xml`](https://bitbucket.org/refcodes/refcodes-servicebus-alt/src/refcodes-servicebus-alt-1.0.4/refcodes-servicebus-alt-spring/src/test/resources/log4j.xml) 

## Change list &lt;refcodes-tabular-alt&gt; (version 1.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-tabular-alt/src/refcodes-tabular-alt-1.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-tabular-alt/src/refcodes-tabular-alt-1.0.4/refcodes-tabular-alt-forwardsecrecy/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`log4j.xml`](https://bitbucket.org/refcodes/refcodes-tabular-alt/src/refcodes-tabular-alt-1.0.4/refcodes-tabular-alt-forwardsecrecy/src/test/resources/log4j.xml) 
