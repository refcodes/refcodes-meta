# Change list for REFCODES.ORG artifacts' version 1.3.1

> This change list has been auto-generated on `triton` by `steiner` with with `changelist-all.sh` on the 2018-09-18 at 06:45:52.

## Change list &lt;refcodes-licensing&gt; (version 1.3.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-licensing/src/refcodes-licensing-1.3.1/pom.xml) 

## Change list &lt;refcodes-parent&gt; (version 1.3.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-parent/src/refcodes-parent-1.3.1/pom.xml) 

## Change list &lt;refcodes-time&gt; (version 1.3.1)

* \[<span style="color:red">DELETED</span>\] `#Untitled-1#`
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-time/src/refcodes-time-1.3.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`module-info.java.off`](https://bitbucket.org/refcodes/refcodes-time/src/refcodes-time-1.3.1/src/main/java/module-info.java.off) 

## Change list &lt;refcodes-mixin&gt; (version 1.3.1)

* \[<span style="color:blue">ADDED</span>\] [`ResultAccessor.java`](https://bitbucket.org/refcodes/refcodes-mixin/src/refcodes-mixin-1.3.1/src/main/java/org/refcodes/mixin/ResultAccessor.java) (see Javadoc at [`ResultAccessor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-mixin/1.3.1/org/refcodes/mixin/ResultAccessor.html))
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-mixin/src/refcodes-mixin-1.3.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`module-info.java.off`](https://bitbucket.org/refcodes/refcodes-mixin/src/refcodes-mixin-1.3.1/src/main/java/module-info.java.off) 

## Change list &lt;refcodes-data&gt; (version 1.3.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data/src/refcodes-data-1.3.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`module-info.java.off`](https://bitbucket.org/refcodes/refcodes-data/src/refcodes-data-1.3.1/src/main/java/module-info.java.off) 

## Change list &lt;refcodes-exception&gt; (version 1.3.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-exception/src/refcodes-exception-1.3.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`module-info.java.off`](https://bitbucket.org/refcodes/refcodes-exception/src/refcodes-exception-1.3.1/src/main/java/module-info.java.off) 

## Change list &lt;refcodes-factory&gt; (version 1.3.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-factory/src/refcodes-factory-1.3.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`module-info.java.off`](https://bitbucket.org/refcodes/refcodes-factory/src/refcodes-factory-1.3.1/src/main/java/module-info.java.off) 

## Change list &lt;refcodes-factory-alt&gt; (version 1.3.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-factory-alt/src/refcodes-factory-alt-1.3.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-factory-alt/src/refcodes-factory-alt-1.3.1/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-factory-alt/src/refcodes-factory-alt-1.3.1/refcodes-factory-alt-spring/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`module-info.java.off`](https://bitbucket.org/refcodes/refcodes-factory-alt/src/refcodes-factory-alt-1.3.1/refcodes-factory-alt-spring/src/main/java/module-info.java.off) 

## Change list &lt;refcodes-controlflow&gt; (version 1.3.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-controlflow/src/refcodes-controlflow-1.3.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`module-info.java.off`](https://bitbucket.org/refcodes/refcodes-controlflow/src/refcodes-controlflow-1.3.1/src/main/java/module-info.java.off) 

## Change list &lt;refcodes-numerical&gt; (version 1.3.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-numerical/src/refcodes-numerical-1.3.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`module-info.java.off`](https://bitbucket.org/refcodes/refcodes-numerical/src/refcodes-numerical-1.3.1/src/main/java/module-info.java.off) 
* \[<span style="color:green">MODIFIED</span>\] [`NumericalUtility.java`](https://bitbucket.org/refcodes/refcodes-numerical/src/refcodes-numerical-1.3.1/src/main/java/org/refcodes/numerical/NumericalUtility.java) (see Javadoc at [`NumericalUtility.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-numerical/1.3.1/org/refcodes/numerical/NumericalUtility.html))

## Change list &lt;refcodes-generator&gt; (version 1.3.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-generator/src/refcodes-generator-1.3.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`module-info.java.off`](https://bitbucket.org/refcodes/refcodes-generator/src/refcodes-generator-1.3.1/src/main/java/module-info.java.off) 

## Change list &lt;refcodes-structure&gt; (version 1.3.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-1.3.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-1.3.1/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`module-info.java.off`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-1.3.1/src/main/java/module-info.java.off) 

## Change list &lt;refcodes-runtime&gt; (version 1.3.1)

* \[<span style="color:blue">ADDED</span>\] [`ProcessResult.java`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-1.3.1/src/main/java/org/refcodes/runtime/ProcessResult.java) (see Javadoc at [`ProcessResult.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-runtime/1.3.1/org/refcodes/runtime/ProcessResult.html))
* \[<span style="color:blue">ADDED</span>\] [`TerminalTest.java`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-1.3.1/src/test/java/org/refcodes/runtime/TerminalTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-1.3.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-1.3.1/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`module-info.java.off`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-1.3.1/src/main/java/module-info.java.off) 
* \[<span style="color:green">MODIFIED</span>\] [`ConfigLocator.java`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-1.3.1/src/main/java/org/refcodes/runtime/ConfigLocator.java) (see Javadoc at [`ConfigLocator.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-runtime/1.3.1/org/refcodes/runtime/ConfigLocator.html))
* \[<span style="color:green">MODIFIED</span>\] [`RuntimeUtility.java`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-1.3.1/src/main/java/org/refcodes/runtime/RuntimeUtility.java) (see Javadoc at [`RuntimeUtility.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-runtime/1.3.1/org/refcodes/runtime/RuntimeUtility.html))
* \[<span style="color:green">MODIFIED</span>\] [`Shell.java`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-1.3.1/src/main/java/org/refcodes/runtime/Shell.java) (see Javadoc at [`Shell.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-runtime/1.3.1/org/refcodes/runtime/Shell.html))
* \[<span style="color:green">MODIFIED</span>\] [`SystemContext.java`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-1.3.1/src/main/java/org/refcodes/runtime/SystemContext.java) (see Javadoc at [`SystemContext.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-runtime/1.3.1/org/refcodes/runtime/SystemContext.html))
* \[<span style="color:green">MODIFIED</span>\] [`SystemUtility.java`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-1.3.1/src/main/java/org/refcodes/runtime/SystemUtility.java) (see Javadoc at [`SystemUtility.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-runtime/1.3.1/org/refcodes/runtime/SystemUtility.html))
* \[<span style="color:green">MODIFIED</span>\] [`Terminal.java`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-1.3.1/src/main/java/org/refcodes/runtime/Terminal.java) (see Javadoc at [`Terminal.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-runtime/1.3.1/org/refcodes/runtime/Terminal.html))
* \[<span style="color:green">MODIFIED</span>\] [`RuntimeUtilityTest.java`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-1.3.1/src/test/java/org/refcodes/runtime/RuntimeUtilityTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`SystemUtilityTest.java`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-1.3.1/src/test/java/org/refcodes/runtime/SystemUtilityTest.java) 

## Change list &lt;refcodes-component&gt; (version 1.3.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-1.3.1/pom.xml) 

## Change list &lt;refcodes-data-ext&gt; (version 1.3.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.3.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.3.1/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.3.1/refcodes-data-ext-boulderdash/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.3.1/refcodes-data-ext-checkers/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.3.1/refcodes-data-ext-checkers/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.3.1/refcodes-data-ext-chess/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.3.1/refcodes-data-ext-chess/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.3.1/refcodes-data-ext-corporate/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.3.1/refcodes-data-ext-symbols/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.3.1/refcodes-data-ext-symbols/README.md) 

## Change list &lt;refcodes-graphical&gt; (version 1.3.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-graphical/src/refcodes-graphical-1.3.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`DimensionImpl.java`](https://bitbucket.org/refcodes/refcodes-graphical/src/refcodes-graphical-1.3.1/src/main/java/org/refcodes/graphical/DimensionImpl.java) (see Javadoc at [`DimensionImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-graphical/1.3.1/org/refcodes/graphical/DimensionImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`FieldDimensionImpl.java`](https://bitbucket.org/refcodes/refcodes-graphical/src/refcodes-graphical-1.3.1/src/main/java/org/refcodes/graphical/FieldDimensionImpl.java) (see Javadoc at [`FieldDimensionImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-graphical/1.3.1/org/refcodes/graphical/FieldDimensionImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`GridDimensionImpl.java`](https://bitbucket.org/refcodes/refcodes-graphical/src/refcodes-graphical-1.3.1/src/main/java/org/refcodes/graphical/GridDimensionImpl.java) (see Javadoc at [`GridDimensionImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-graphical/1.3.1/org/refcodes/graphical/GridDimensionImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`PositionImpl.java`](https://bitbucket.org/refcodes/refcodes-graphical/src/refcodes-graphical-1.3.1/src/main/java/org/refcodes/graphical/PositionImpl.java) (see Javadoc at [`PositionImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-graphical/1.3.1/org/refcodes/graphical/PositionImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`ScaleMode.java`](https://bitbucket.org/refcodes/refcodes-graphical/src/refcodes-graphical-1.3.1/src/main/java/org/refcodes/graphical/ScaleMode.java) (see Javadoc at [`ScaleMode.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-graphical/1.3.1/org/refcodes/graphical/ScaleMode.html))
* \[<span style="color:green">MODIFIED</span>\] [`ViewportDimensionImpl.java`](https://bitbucket.org/refcodes/refcodes-graphical/src/refcodes-graphical-1.3.1/src/main/java/org/refcodes/graphical/ViewportDimensionImpl.java) (see Javadoc at [`ViewportDimensionImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-graphical/1.3.1/org/refcodes/graphical/ViewportDimensionImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`ViewportOffsetImpl.java`](https://bitbucket.org/refcodes/refcodes-graphical/src/refcodes-graphical-1.3.1/src/main/java/org/refcodes/graphical/ViewportOffsetImpl.java) (see Javadoc at [`ViewportOffsetImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-graphical/1.3.1/org/refcodes/graphical/ViewportOffsetImpl.html))

## Change list &lt;refcodes-textual&gt; (version 1.3.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-1.3.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`AbstractText.java`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-1.3.1/src/main/java/org/refcodes/textual/AbstractText.java) (see Javadoc at [`AbstractText.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-textual/1.3.1/org/refcodes/textual/AbstractText.html))
* \[<span style="color:green">MODIFIED</span>\] [`AsciiArtBuilderImpl.java`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-1.3.1/src/main/java/org/refcodes/textual/AsciiArtBuilderImpl.java) (see Javadoc at [`AsciiArtBuilderImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-textual/1.3.1/org/refcodes/textual/AsciiArtBuilderImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`HorizAlignTextBuilderImpl.java`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-1.3.1/src/main/java/org/refcodes/textual/HorizAlignTextBuilderImpl.java) (see Javadoc at [`HorizAlignTextBuilderImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-textual/1.3.1/org/refcodes/textual/HorizAlignTextBuilderImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`MoreTextBuilderImpl.java`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-1.3.1/src/main/java/org/refcodes/textual/MoreTextBuilderImpl.java) (see Javadoc at [`MoreTextBuilderImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-textual/1.3.1/org/refcodes/textual/MoreTextBuilderImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`TableBuilderImpl.java`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-1.3.1/src/main/java/org/refcodes/textual/TableBuilderImpl.java) (see Javadoc at [`TableBuilderImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-textual/1.3.1/org/refcodes/textual/TableBuilderImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`TableBuilder.java`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-1.3.1/src/main/java/org/refcodes/textual/TableBuilder.java) (see Javadoc at [`TableBuilder.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-textual/1.3.1/org/refcodes/textual/TableBuilder.html))
* \[<span style="color:green">MODIFIED</span>\] [`TextBlockBuilderImpl.java`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-1.3.1/src/main/java/org/refcodes/textual/TextBlockBuilderImpl.java) (see Javadoc at [`TextBlockBuilderImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-textual/1.3.1/org/refcodes/textual/TextBlockBuilderImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`TextLineBuilderImpl.java`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-1.3.1/src/main/java/org/refcodes/textual/TextLineBuilderImpl.java) (see Javadoc at [`TextLineBuilderImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-textual/1.3.1/org/refcodes/textual/TextLineBuilderImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`TextLineBuilder.java`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-1.3.1/src/main/java/org/refcodes/textual/TextLineBuilder.java) (see Javadoc at [`TextLineBuilder.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-textual/1.3.1/org/refcodes/textual/TextLineBuilder.html))
* \[<span style="color:green">MODIFIED</span>\] [`AsciiArtBuilderTest.java`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-1.3.1/src/test/java/org/refcodes/textual/AsciiArtBuilderTest.java) 

## Change list &lt;refcodes-criteria&gt; (version 1.3.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-1.3.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-1.3.1/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`module-info.java.off`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-1.3.1/src/main/java/module-info.java.off) 

## Change list &lt;refcodes-tabular&gt; (version 1.3.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-tabular/src/refcodes-tabular-1.3.1/pom.xml) 

## Change list &lt;refcodes-matcher&gt; (version 1.3.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-matcher/src/refcodes-matcher-1.3.1/pom.xml) 

## Change list &lt;refcodes-observer&gt; (version 1.3.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-observer/src/refcodes-observer-1.3.1/pom.xml) 

## Change list &lt;refcodes-command&gt; (version 1.3.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-command/src/refcodes-command-1.3.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-command/src/refcodes-command-1.3.1/README.md) 

## Change list &lt;refcodes-cli&gt; (version 1.3.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-1.3.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-1.3.1/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`ArgsParserImpl.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-1.3.1/src/main/java/org/refcodes/console/ArgsParserImpl.java) (see Javadoc at [`ArgsParserImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/1.3.1/org/refcodes/console/ArgsParserImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`ArgsParser.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-1.3.1/src/main/java/org/refcodes/console/ArgsParser.java) (see Javadoc at [`ArgsParser.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/1.3.1/org/refcodes/console/ArgsParser.html))

## Change list &lt;refcodes-io&gt; (version 1.3.1)

* \[<span style="color:red">DELETED</span>\] `org.refcodes.io.receiver`
* \[<span style="color:red">DELETED</span>\] `org.refcodes.io.receiver`
* \[<span style="color:red">DELETED</span>\] `org.refcodes.io.sender`
* \[<span style="color:red">DELETED</span>\] `org.refcodes.io.sender`
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-io/src/refcodes-io-1.3.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-io/src/refcodes-io-1.3.1/README.md) 

## Change list &lt;refcodes-codec&gt; (version 1.3.1)

* \[<span style="color:red">DELETED</span>\] `log4j.xml`
* \[<span style="color:red">DELETED</span>\] `runtimelogger-config.xml`
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-1.3.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-1.3.1/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`BaseInputStreamDecoderTest.java`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-1.3.1/src/test/java/org/refcodes/codec/BaseInputStreamDecoderTest.java) 

## Change list &lt;refcodes-component-ext&gt; (version 1.3.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.3.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.3.1/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.3.1/refcodes-component-ext-observer/pom.xml) 

## Change list &lt;refcodes-properties&gt; (version 1.3.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.3.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.3.1/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`AbstractResourcePropertiesBuilder.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.3.1/src/main/java/org/refcodes/configuration/AbstractResourcePropertiesBuilder.java) (see Javadoc at [`AbstractResourcePropertiesBuilder.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties/1.3.1/org/refcodes/configuration/AbstractResourcePropertiesBuilder.html))
* \[<span style="color:green">MODIFIED</span>\] [`package-info.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.3.1/src/main/java/org/refcodes/configuration/package-info.java) 
* \[<span style="color:green">MODIFIED</span>\] [`PolyglotPropertiesBuilder.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.3.1/src/main/java/org/refcodes/configuration/PolyglotPropertiesBuilder.java) (see Javadoc at [`PolyglotPropertiesBuilder.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties/1.3.1/org/refcodes/configuration/PolyglotPropertiesBuilder.html))
* \[<span style="color:green">MODIFIED</span>\] [`PolyglotProperties.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.3.1/src/main/java/org/refcodes/configuration/PolyglotProperties.java) (see Javadoc at [`PolyglotProperties.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties/1.3.1/org/refcodes/configuration/PolyglotProperties.html))

## Change list &lt;refcodes-security&gt; (version 1.3.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security/src/refcodes-security-1.3.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security/src/refcodes-security-1.3.1/README.md) 

## Change list &lt;refcodes-security-alt&gt; (version 1.3.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-1.3.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-1.3.1/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-1.3.1/refcodes-security-alt-chaos/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-1.3.1/refcodes-security-alt-chaos/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`ChaosKeyImpl.java`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-1.3.1/refcodes-security-alt-chaos/src/main/java/org/refcodes/security/alt/chaos/ChaosKeyImpl.java) (see Javadoc at [`ChaosKeyImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-security-alt-chaos/1.3.1/org/refcodes/security/alt/chaos/ChaosKeyImpl.html))

## Change list &lt;refcodes-security-ext&gt; (version 1.3.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-1.3.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-1.3.1/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-1.3.1/refcodes-security-ext-chaos/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-1.3.1/refcodes-security-ext-chaos/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-1.3.1/refcodes-security-ext-spring/pom.xml) 

## Change list &lt;refcodes-properties-ext&gt; (version 1.3.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.3.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.3.1/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.3.1/refcodes-properties-ext-cli/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.3.1/refcodes-properties-ext-cli/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.3.1/refcodes-properties-ext-obfuscation/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.3.1/refcodes-properties-ext-obfuscation/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`ObfuscationPropertiesBuilderDecorator.java`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.3.1/refcodes-properties-ext-obfuscation/src/main/java/org/refcodes/configuration/ext/obfuscation/ObfuscationPropertiesBuilderDecorator.java) (see Javadoc at [`ObfuscationPropertiesBuilderDecorator.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties-ext-obfuscation/1.3.1/org/refcodes/configuration/ext/obfuscation/ObfuscationPropertiesBuilderDecorator.html))
* \[<span style="color:green">MODIFIED</span>\] [`ObfuscationPropertiesDecorator.java`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.3.1/refcodes-properties-ext-obfuscation/src/main/java/org/refcodes/configuration/ext/obfuscation/ObfuscationPropertiesDecorator.java) (see Javadoc at [`ObfuscationPropertiesDecorator.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties-ext-obfuscation/1.3.1/org/refcodes/configuration/ext/obfuscation/ObfuscationPropertiesDecorator.html))
* \[<span style="color:green">MODIFIED</span>\] [`ObfuscationResourcePropertiesBuilderDecorator.java`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.3.1/refcodes-properties-ext-obfuscation/src/main/java/org/refcodes/configuration/ext/obfuscation/ObfuscationResourcePropertiesBuilderDecorator.java) (see Javadoc at [`ObfuscationResourcePropertiesBuilderDecorator.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties-ext-obfuscation/1.3.1/org/refcodes/configuration/ext/obfuscation/ObfuscationResourcePropertiesBuilderDecorator.html))
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.3.1/refcodes-properties-ext-observer/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.3.1/refcodes-properties-ext-observer/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.3.1/refcodes-properties-ext-runtime/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.3.1/refcodes-properties-ext-runtime/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`RuntimePropertiesSugarTest.java`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.3.1/refcodes-properties-ext-runtime/src/test/java/org/refcodes/configuration/ext/runtime/RuntimePropertiesSugarTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`RuntimePropertiesTest.java`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.3.1/refcodes-properties-ext-runtime/src/test/java/org/refcodes/configuration/ext/runtime/RuntimePropertiesTest.java) 

## Change list &lt;refcodes-logger&gt; (version 1.3.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger/src/refcodes-logger-1.3.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger/src/refcodes-logger-1.3.1/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`package-info.java`](https://bitbucket.org/refcodes/refcodes-logger/src/refcodes-logger-1.3.1/src/main/java/org/refcodes/logger/package-info.java) 
* \[<span style="color:green">MODIFIED</span>\] [`RuntimeLogger.java`](https://bitbucket.org/refcodes/refcodes-logger/src/refcodes-logger-1.3.1/src/main/java/org/refcodes/logger/RuntimeLogger.java) (see Javadoc at [`RuntimeLogger.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-logger/1.3.1/org/refcodes/logger/RuntimeLogger.html))
* \[<span style="color:green">MODIFIED</span>\] [`StatisticalCompositeTrimLoggerTest.java`](https://bitbucket.org/refcodes/refcodes-logger/src/refcodes-logger-1.3.1/src/test/java/org/refcodes/logger/StatisticalCompositeTrimLoggerTest.java) 

## Change list &lt;refcodes-logger-alt&gt; (version 1.3.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.3.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.3.1/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.3.1/refcodes-logger-alt-async/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.3.1/refcodes-logger-alt-async/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`AsyncRuntimeLoggerTest.java`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.3.1/refcodes-logger-alt-async/src/test/java/org/refcodes/logger/alt/async/AsyncRuntimeLoggerTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`runtimelogger.ini`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.3.1/refcodes-logger-alt-async/src/test/resources/runtimelogger.ini) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.3.1/refcodes-logger-alt-console/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.3.1/refcodes-logger-alt-console/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`module-info.java.off`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.3.1/refcodes-logger-alt-console/src/main/java/module-info.java.off) 
* \[<span style="color:green">MODIFIED</span>\] [`ConsoleLoggerImpl.java`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.3.1/refcodes-logger-alt-console/src/main/java/org/refcodes/logger/alt/console/ConsoleLoggerImpl.java) (see Javadoc at [`ConsoleLoggerImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-logger-alt-console/1.3.1/org/refcodes/logger/alt/console/ConsoleLoggerImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`ConsoleLoggerSingleton.java`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.3.1/refcodes-logger-alt-console/src/main/java/org/refcodes/logger/alt/console/ConsoleLoggerSingleton.java) (see Javadoc at [`ConsoleLoggerSingleton.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-logger-alt-console/1.3.1/org/refcodes/logger/alt/console/ConsoleLoggerSingleton.html))
* \[<span style="color:green">MODIFIED</span>\] [`FormattedLoggerImpl.java`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.3.1/refcodes-logger-alt-console/src/main/java/org/refcodes/logger/alt/console/FormattedLoggerImpl.java) (see Javadoc at [`FormattedLoggerImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-logger-alt-console/1.3.1/org/refcodes/logger/alt/console/FormattedLoggerImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`package-info.java`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.3.1/refcodes-logger-alt-console/src/main/java/org/refcodes/logger/alt/console/package-info.java) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.3.1/refcodes-logger-alt-io/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.3.1/refcodes-logger-alt-io/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.3.1/refcodes-logger-alt-simpledb/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.3.1/refcodes-logger-alt-slf4j/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.3.1/refcodes-logger-alt-spring/pom.xml) 

## Change list &lt;refcodes-logger-ext&gt; (version 1.3.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-1.3.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-1.3.1/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-1.3.1/refcodes-logger-ext-slf4j/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-1.3.1/refcodes-logger-ext-slf4j/README.md) 

## Change list &lt;refcodes-graphical-ext&gt; (version 1.3.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-1.3.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-1.3.1/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-1.3.1/refcodes-graphical-ext-javafx/pom.xml) 

## Change list &lt;refcodes-checkerboard&gt; (version 1.3.1)

* \[<span style="color:blue">ADDED</span>\] [`ChangePositionObserver.java`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-1.3.1/src/main/java/org/refcodes/checkerboard/ChangePositionObserver.java) (see Javadoc at [`ChangePositionObserver.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-checkerboard/1.3.1/org/refcodes/checkerboard/ChangePositionObserver.html))
* \[<span style="color:blue">ADDED</span>\] [`ClickedEventImpl.java`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-1.3.1/src/main/java/org/refcodes/checkerboard/ClickedEventImpl.java) (see Javadoc at [`ClickedEventImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-checkerboard/1.3.1/org/refcodes/checkerboard/ClickedEventImpl.html))
* \[<span style="color:blue">ADDED</span>\] [`ClickedEvent.java`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-1.3.1/src/main/java/org/refcodes/checkerboard/ClickedEvent.java) (see Javadoc at [`ClickedEvent.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-checkerboard/1.3.1/org/refcodes/checkerboard/ClickedEvent.html))
* \[<span style="color:blue">ADDED</span>\] [`ClickedObserver.java`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-1.3.1/src/main/java/org/refcodes/checkerboard/ClickedObserver.java) (see Javadoc at [`ClickedObserver.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-checkerboard/1.3.1/org/refcodes/checkerboard/ClickedObserver.html))
* \[<span style="color:blue">ADDED</span>\] [`DraggabilityChangedObserver.java`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-1.3.1/src/main/java/org/refcodes/checkerboard/DraggabilityChangedObserver.java) (see Javadoc at [`DraggabilityChangedObserver.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-checkerboard/1.3.1/org/refcodes/checkerboard/DraggabilityChangedObserver.html))
* \[<span style="color:blue">ADDED</span>\] [`PositionChangedObserver.java`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-1.3.1/src/main/java/org/refcodes/checkerboard/PositionChangedObserver.java) (see Javadoc at [`PositionChangedObserver.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-checkerboard/1.3.1/org/refcodes/checkerboard/PositionChangedObserver.html))
* \[<span style="color:blue">ADDED</span>\] [`StateChangedObserver.java`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-1.3.1/src/main/java/org/refcodes/checkerboard/StateChangedObserver.java) (see Javadoc at [`StateChangedObserver.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-checkerboard/1.3.1/org/refcodes/checkerboard/StateChangedObserver.html))
* \[<span style="color:blue">ADDED</span>\] [`VisibilityChangedObserver.java`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-1.3.1/src/main/java/org/refcodes/checkerboard/VisibilityChangedObserver.java) (see Javadoc at [`VisibilityChangedObserver.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-checkerboard/1.3.1/org/refcodes/checkerboard/VisibilityChangedObserver.html))
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-1.3.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-1.3.1/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`AbstractGraphicalCheckerboardViewer.java`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-1.3.1/src/main/java/org/refcodes/checkerboard/AbstractGraphicalCheckerboardViewer.java) (see Javadoc at [`AbstractGraphicalCheckerboardViewer.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-checkerboard/1.3.1/org/refcodes/checkerboard/AbstractGraphicalCheckerboardViewer.html))
* \[<span style="color:green">MODIFIED</span>\] [`AbstractPlayer.java`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-1.3.1/src/main/java/org/refcodes/checkerboard/AbstractPlayer.java) (see Javadoc at [`AbstractPlayer.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-checkerboard/1.3.1/org/refcodes/checkerboard/AbstractPlayer.html))
* \[<span style="color:green">MODIFIED</span>\] [`CheckerboardImpl.java`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-1.3.1/src/main/java/org/refcodes/checkerboard/CheckerboardImpl.java) (see Javadoc at [`CheckerboardImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-checkerboard/1.3.1/org/refcodes/checkerboard/CheckerboardImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`GridDimensionChangedEventImpl.java`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-1.3.1/src/main/java/org/refcodes/checkerboard/GridDimensionChangedEventImpl.java) (see Javadoc at [`GridDimensionChangedEventImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-checkerboard/1.3.1/org/refcodes/checkerboard/GridDimensionChangedEventImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`PlayerAction.java`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-1.3.1/src/main/java/org/refcodes/checkerboard/PlayerAction.java) (see Javadoc at [`PlayerAction.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-checkerboard/1.3.1/org/refcodes/checkerboard/PlayerAction.html))
* \[<span style="color:green">MODIFIED</span>\] [`Player.java`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-1.3.1/src/main/java/org/refcodes/checkerboard/Player.java) (see Javadoc at [`Player.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-checkerboard/1.3.1/org/refcodes/checkerboard/Player.html))
* \[<span style="color:green">MODIFIED</span>\] [`PlayerObserver.java`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-1.3.1/src/main/java/org/refcodes/checkerboard/PlayerObserver.java) (see Javadoc at [`PlayerObserver.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-checkerboard/1.3.1/org/refcodes/checkerboard/PlayerObserver.html))
* \[<span style="color:green">MODIFIED</span>\] [`ViewportDimensionChangedEventImpl.java`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-1.3.1/src/main/java/org/refcodes/checkerboard/ViewportDimensionChangedEventImpl.java) (see Javadoc at [`ViewportDimensionChangedEventImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-checkerboard/1.3.1/org/refcodes/checkerboard/ViewportDimensionChangedEventImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`ViewportOffsetChangedEventImpl.java`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-1.3.1/src/main/java/org/refcodes/checkerboard/ViewportOffsetChangedEventImpl.java) (see Javadoc at [`ViewportOffsetChangedEventImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-checkerboard/1.3.1/org/refcodes/checkerboard/ViewportOffsetChangedEventImpl.html))

## Change list &lt;refcodes-checkerboard-alt&gt; (version 1.3.1)

* \[<span style="color:blue">ADDED</span>\] [`runtimelogger.ini`](https://bitbucket.org/refcodes/refcodes-checkerboard-alt/src/refcodes-checkerboard-alt-1.3.1/refcodes-checkerboard-alt-javafx/src/test/resources/runtimelogger.ini) 
* \[<span style="color:red">DELETED</span>\] `runtimelogger-config.xml`
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-alt/src/refcodes-checkerboard-alt-1.3.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-alt/src/refcodes-checkerboard-alt-1.3.1/refcodes-checkerboard-alt-javafx/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`FxCheckerboardViewerImpl.java`](https://bitbucket.org/refcodes/refcodes-checkerboard-alt/src/refcodes-checkerboard-alt-1.3.1/refcodes-checkerboard-alt-javafx/src/main/java/org/refcodes/checkerboard/alt/javafx/FxCheckerboardViewerImpl.java) (see Javadoc at [`FxCheckerboardViewerImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-checkerboard-alt-javafx/1.3.1/org/refcodes/checkerboard/alt/javafx/FxCheckerboardViewerImpl.html))

## Change list &lt;refcodes-checkerboard-ext&gt; (version 1.3.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-ext/src/refcodes-checkerboard-ext-1.3.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-checkerboard-ext/src/refcodes-checkerboard-ext-1.3.1/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-ext/src/refcodes-checkerboard-ext-1.3.1/refcodes-checkerboard-ext-javafx-boulderdash/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`BoulderDashDemo.java`](https://bitbucket.org/refcodes/refcodes-checkerboard-ext/src/refcodes-checkerboard-ext-1.3.1/refcodes-checkerboard-ext-javafx-boulderdash/src/test/java/org/refcodes/checkerboard/ext/javafx/boulderdash/BoulderDashDemo.java) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-ext/src/refcodes-checkerboard-ext-1.3.1/refcodes-checkerboard-ext-javafx-chess/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`ChessDemo.java`](https://bitbucket.org/refcodes/refcodes-checkerboard-ext/src/refcodes-checkerboard-ext-1.3.1/refcodes-checkerboard-ext-javafx-chess/src/test/java/org/refcodes/checkerboard/ext/javafx/chess/ChessDemo.java) 

## Change list &lt;refcodes-boulderdash&gt; (version 1.3.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-boulderdash/src/refcodes-boulderdash-1.3.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-boulderdash/src/refcodes-boulderdash-1.3.1/README.md) 

## Change list &lt;refcodes-net&gt; (version 1.3.1)

* \[<span style="color:red">DELETED</span>\] `refcodes-net`
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.3.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`IpAddress.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.3.1/src/main/java/org/refcodes/net/IpAddress.java) (see Javadoc at [`IpAddress.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/1.3.1/org/refcodes/net/IpAddress.html))
* \[<span style="color:green">MODIFIED</span>\] [`IpAddressTest.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.3.1/src/test/java/org/refcodes/net/IpAddressTest.java) 

## Change list &lt;refcodes-rest&gt; (version 1.3.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.3.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.3.1/README.md) 

## Change list &lt;refcodes-daemon&gt; (version 1.3.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-daemon/src/refcodes-daemon-1.3.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-daemon/src/refcodes-daemon-1.3.1/README.md) 

## Change list &lt;refcodes-eventbus&gt; (version 1.3.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-eventbus/src/refcodes-eventbus-1.3.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-eventbus/src/refcodes-eventbus-1.3.1/README.md) 

## Change list &lt;refcodes-eventbus-ext&gt; (version 1.3.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-1.3.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-1.3.1/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-1.3.1/refcodes-eventbus-ext-application/pom.xml) 

## Change list &lt;refcodes-filesystem&gt; (version 1.3.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-filesystem/src/refcodes-filesystem-1.3.1/pom.xml) 

## Change list &lt;refcodes-filesystem-alt&gt; (version 1.3.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-filesystem-alt/src/refcodes-filesystem-alt-1.3.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-filesystem-alt/src/refcodes-filesystem-alt-1.3.1/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-filesystem-alt/src/refcodes-filesystem-alt-1.3.1/refcodes-filesystem-alt-s3/pom.xml) 

## Change list &lt;refcodes-forwardsecrecy&gt; (version 1.3.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy/src/refcodes-forwardsecrecy-1.3.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy/src/refcodes-forwardsecrecy-1.3.1/README.md) 

## Change list &lt;refcodes-forwardsecrecy-alt&gt; (version 1.3.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-1.3.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-1.3.1/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-1.3.1/refcodes-forwardsecrecy-alt-filesystem/pom.xml) 

## Change list &lt;refcodes-io-ext&gt; (version 1.3.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-1.3.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-1.3.1/refcodes-io-ext-observable/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-1.3.1/refcodes-io-ext-observable/README.md) 

## Change list &lt;refcodes-interceptor&gt; (version 1.3.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-interceptor/src/refcodes-interceptor-1.3.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-interceptor/src/refcodes-interceptor-1.3.1/README.md) 

## Change list &lt;refcodes-jobbus&gt; (version 1.3.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-jobbus/src/refcodes-jobbus-1.3.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-jobbus/src/refcodes-jobbus-1.3.1/README.md) 

## Change list &lt;refcodes-rest-ext&gt; (version 1.3.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-1.3.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-1.3.1/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-1.3.1/refcodes-rest-ext-eureka/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-1.3.1/refcodes-rest-ext-eureka/README.md) 

## Change list &lt;refcodes-remoting&gt; (version 1.3.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-remoting/src/refcodes-remoting-1.3.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-remoting/src/refcodes-remoting-1.3.1/README.md) 

## Change list &lt;refcodes-remoting-ext&gt; (version 1.3.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-1.3.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-1.3.1/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-1.3.1/refcodes-remoting-ext-observer/pom.xml) 

## Change list &lt;refcodes-servicebus&gt; (version 1.3.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-servicebus/src/refcodes-servicebus-1.3.1/pom.xml) 

## Change list &lt;refcodes-servicebus-alt&gt; (version 1.3.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-servicebus-alt/src/refcodes-servicebus-alt-1.3.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-servicebus-alt/src/refcodes-servicebus-alt-1.3.1/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-servicebus-alt/src/refcodes-servicebus-alt-1.3.1/refcodes-servicebus-alt-spring/pom.xml) 

## Change list &lt;refcodes-tabular-alt&gt; (version 1.3.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-tabular-alt/src/refcodes-tabular-alt-1.3.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-tabular-alt/src/refcodes-tabular-alt-1.3.1/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-tabular-alt/src/refcodes-tabular-alt-1.3.1/refcodes-tabular-alt-forwardsecrecy/pom.xml) 
