> This change list has been auto-generated on `triton` by `steiner` with `changelist-all.sh` on the 2022-09-11 at 14:55:58.

## Change list &lt;refcodes-licensing&gt; (version 3.0.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-licensing/src/refcodes-licensing-3.0.5/pom.xml) 

## Change list &lt;refcodes-parent&gt; (version 3.0.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-parent/src/refcodes-parent-3.0.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-parent/src/refcodes-parent-3.0.5/README.md) 

## Change list &lt;refcodes-time&gt; (version 3.0.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-time/src/refcodes-time-3.0.5/pom.xml) 

## Change list &lt;refcodes-mixin&gt; (version 3.0.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-mixin/src/refcodes-mixin-3.0.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-mixin/src/refcodes-mixin-3.0.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`ResultAccessor.java`](https://bitbucket.org/refcodes/refcodes-mixin/src/refcodes-mixin-3.0.5/src/main/java/org/refcodes/mixin/ResultAccessor.java) (see Javadoc at [`ResultAccessor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-mixin/3.0.5/org.refcodes.mixin/org/refcodes/mixin/ResultAccessor.html))

## Change list &lt;refcodes-data&gt; (version 3.0.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data/src/refcodes-data-3.0.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data/src/refcodes-data-3.0.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`Encoding.java`](https://bitbucket.org/refcodes/refcodes-data/src/refcodes-data-3.0.5/src/main/java/org/refcodes/data/Encoding.java) (see Javadoc at [`Encoding.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-data/3.0.5/org.refcodes.data/org/refcodes/data/Encoding.html))
* \[<span style="color:green">MODIFIED</span>\] [`EnvironmentVariable.java`](https://bitbucket.org/refcodes/refcodes-data/src/refcodes-data-3.0.5/src/main/java/org/refcodes/data/EnvironmentVariable.java) (see Javadoc at [`EnvironmentVariable.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-data/3.0.5/org.refcodes.data/org/refcodes/data/EnvironmentVariable.html))

## Change list &lt;refcodes-exception&gt; (version 3.0.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-exception/src/refcodes-exception-3.0.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-exception/src/refcodes-exception-3.0.5/README.md) 

## Change list &lt;refcodes-factory&gt; (version 3.0.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-factory/src/refcodes-factory-3.0.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-factory/src/refcodes-factory-3.0.5/README.md) 

## Change list &lt;refcodes-factory-alt&gt; (version 3.0.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-factory-alt/src/refcodes-factory-alt-3.0.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-factory-alt/src/refcodes-factory-alt-3.0.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-factory-alt/src/refcodes-factory-alt-3.0.5/refcodes-factory-alt-spring/pom.xml) 

## Change list &lt;refcodes-controlflow&gt; (version 3.0.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-controlflow/src/refcodes-controlflow-3.0.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-controlflow/src/refcodes-controlflow-3.0.5/README.md) 

## Change list &lt;refcodes-numerical&gt; (version 3.0.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-numerical/src/refcodes-numerical-3.0.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-numerical/src/refcodes-numerical-3.0.5/README.md) 

## Change list &lt;refcodes-generator&gt; (version 3.0.5)

* \[<span style="color:blue">ADDED</span>\] [`specialwords.txt`](https://bitbucket.org/refcodes/refcodes-generator/src/refcodes-generator-3.0.5/src/test/resources/specialwords.txt) 
* \[<span style="color:blue">ADDED</span>\] [`wordlist.txt`](https://bitbucket.org/refcodes/refcodes-generator/src/refcodes-generator-3.0.5/src/test/resources/wordlist.txt) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-generator/src/refcodes-generator-3.0.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-generator/src/refcodes-generator-3.0.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`AlphabetCounter.java`](https://bitbucket.org/refcodes/refcodes-generator/src/refcodes-generator-3.0.5/src/main/java/org/refcodes/generator/AlphabetCounter.java) (see Javadoc at [`AlphabetCounter.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-generator/3.0.5/org.refcodes.generator/org/refcodes/generator/AlphabetCounter.html))
* \[<span style="color:green">MODIFIED</span>\] [`AlphabetCounterMetrics.java`](https://bitbucket.org/refcodes/refcodes-generator/src/refcodes-generator-3.0.5/src/main/java/org/refcodes/generator/AlphabetCounterMetrics.java) (see Javadoc at [`AlphabetCounterMetrics.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-generator/3.0.5/org.refcodes.generator/org/refcodes/generator/AlphabetCounterMetrics.html))
* \[<span style="color:green">MODIFIED</span>\] [`BufferedGenerator.java`](https://bitbucket.org/refcodes/refcodes-generator/src/refcodes-generator-3.0.5/src/main/java/org/refcodes/generator/BufferedGenerator.java) (see Javadoc at [`BufferedGenerator.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-generator/3.0.5/org.refcodes.generator/org/refcodes/generator/BufferedGenerator.html))
* \[<span style="color:green">MODIFIED</span>\] [`Generator.java`](https://bitbucket.org/refcodes/refcodes-generator/src/refcodes-generator-3.0.5/src/main/java/org/refcodes/generator/Generator.java) (see Javadoc at [`Generator.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-generator/3.0.5/org.refcodes.generator/org/refcodes/generator/Generator.html))
* \[<span style="color:green">MODIFIED</span>\] [`ThreadLocalBufferedGeneratorDecorator.java`](https://bitbucket.org/refcodes/refcodes-generator/src/refcodes-generator-3.0.5/src/main/java/org/refcodes/generator/ThreadLocalBufferedGeneratorDecorator.java) (see Javadoc at [`ThreadLocalBufferedGeneratorDecorator.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-generator/3.0.5/org.refcodes.generator/org/refcodes/generator/ThreadLocalBufferedGeneratorDecorator.html))
* \[<span style="color:green">MODIFIED</span>\] [`AlphabetCounterCompositeTest.java`](https://bitbucket.org/refcodes/refcodes-generator/src/refcodes-generator-3.0.5/src/test/java/org/refcodes/generator/AlphabetCounterCompositeTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`AlphabetCounterMetricsTest.java`](https://bitbucket.org/refcodes/refcodes-generator/src/refcodes-generator-3.0.5/src/test/java/org/refcodes/generator/AlphabetCounterMetricsTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`AlphabetCounterTest.java`](https://bitbucket.org/refcodes/refcodes-generator/src/refcodes-generator-3.0.5/src/test/java/org/refcodes/generator/AlphabetCounterTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`BufferedGeneratorTest.java`](https://bitbucket.org/refcodes/refcodes-generator/src/refcodes-generator-3.0.5/src/test/java/org/refcodes/generator/BufferedGeneratorTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`GeneratorExamples.java`](https://bitbucket.org/refcodes/refcodes-generator/src/refcodes-generator-3.0.5/src/test/java/org/refcodes/generator/GeneratorExamples.java) 

## Change list &lt;refcodes-matcher&gt; (version 3.0.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-matcher/src/refcodes-matcher-3.0.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-matcher/src/refcodes-matcher-3.0.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`MatcherExamples.java`](https://bitbucket.org/refcodes/refcodes-matcher/src/refcodes-matcher-3.0.5/src/test/java/org/refcodes/matcher/MatcherExamples.java) 

## Change list &lt;refcodes-struct&gt; (version 3.0.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-struct/src/refcodes-struct-3.0.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-struct/src/refcodes-struct-3.0.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`CanonicalMapImpl.java`](https://bitbucket.org/refcodes/refcodes-struct/src/refcodes-struct-3.0.5/src/main/java/org/refcodes/struct/CanonicalMapImpl.java) (see Javadoc at [`CanonicalMapImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-struct/3.0.5/org.refcodes.struct/org/refcodes/struct/CanonicalMapImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`Container.java`](https://bitbucket.org/refcodes/refcodes-struct/src/refcodes-struct-3.0.5/src/main/java/org/refcodes/struct/Container.java) (see Javadoc at [`Container.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-struct/3.0.5/org.refcodes.struct/org/refcodes/struct/Container.html))
* \[<span style="color:green">MODIFIED</span>\] [`Dictionary.java`](https://bitbucket.org/refcodes/refcodes-struct/src/refcodes-struct-3.0.5/src/main/java/org/refcodes/struct/Dictionary.java) (see Javadoc at [`Dictionary.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-struct/3.0.5/org.refcodes.struct/org/refcodes/struct/Dictionary.html))
* \[<span style="color:green">MODIFIED</span>\] [`Elements.java`](https://bitbucket.org/refcodes/refcodes-struct/src/refcodes-struct-3.0.5/src/main/java/org/refcodes/struct/Elements.java) (see Javadoc at [`Elements.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-struct/3.0.5/org.refcodes.struct/org/refcodes/struct/Elements.html))
* \[<span style="color:green">MODIFIED</span>\] [`InterOperableMap.java`](https://bitbucket.org/refcodes/refcodes-struct/src/refcodes-struct-3.0.5/src/main/java/org/refcodes/struct/InterOperableMap.java) (see Javadoc at [`InterOperableMap.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-struct/3.0.5/org.refcodes.struct/org/refcodes/struct/InterOperableMap.html))
* \[<span style="color:green">MODIFIED</span>\] [`PathMapBuilderImpl.java`](https://bitbucket.org/refcodes/refcodes-struct/src/refcodes-struct-3.0.5/src/main/java/org/refcodes/struct/PathMapBuilderImpl.java) (see Javadoc at [`PathMapBuilderImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-struct/3.0.5/org.refcodes.struct/org/refcodes/struct/PathMapBuilderImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`PathMap.java`](https://bitbucket.org/refcodes/refcodes-struct/src/refcodes-struct-3.0.5/src/main/java/org/refcodes/struct/PathMap.java) (see Javadoc at [`PathMap.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-struct/3.0.5/org.refcodes.struct/org/refcodes/struct/PathMap.html))
* \[<span style="color:green">MODIFIED</span>\] [`SimpleTypeMapImpl.java`](https://bitbucket.org/refcodes/refcodes-struct/src/refcodes-struct-3.0.5/src/main/java/org/refcodes/struct/SimpleTypeMapImpl.java) (see Javadoc at [`SimpleTypeMapImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-struct/3.0.5/org.refcodes.struct/org/refcodes/struct/SimpleTypeMapImpl.html))

## Change list &lt;refcodes-struct-ext&gt; (version 3.0.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-struct-ext/src/refcodes-struct-ext-3.0.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-struct-ext/src/refcodes-struct-ext-3.0.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-struct-ext/src/refcodes-struct-ext-3.0.5/refcodes-struct-ext-factory/pom.xml) 

## Change list &lt;refcodes-runtime&gt; (version 3.0.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-3.0.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-3.0.5/README.md) 

## Change list &lt;refcodes-component&gt; (version 3.0.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.0.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.0.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`ComponentUtility.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.0.5/src/main/java/org/refcodes/component/ComponentUtility.java) (see Javadoc at [`ComponentUtility.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/3.0.5/org.refcodes.component/org/refcodes/component/ComponentUtility.html))

## Change list &lt;refcodes-data-ext&gt; (version 3.0.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-3.0.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-3.0.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-3.0.5/refcodes-data-ext-checkers/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-3.0.5/refcodes-data-ext-checkers/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-3.0.5/refcodes-data-ext-chess/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-3.0.5/refcodes-data-ext-chess/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-3.0.5/refcodes-data-ext-corporate/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-3.0.5/refcodes-data-ext-corporate/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-3.0.5/refcodes-data-ext-symbols/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-3.0.5/refcodes-data-ext-symbols/README.md) 

## Change list &lt;refcodes-graphical&gt; (version 3.0.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-graphical/src/refcodes-graphical-3.0.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-graphical/src/refcodes-graphical-3.0.5/README.md) 

## Change list &lt;refcodes-textual&gt; (version 3.0.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-3.0.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-3.0.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`ColumnWidthType.java`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-3.0.5/src/main/java/org/refcodes/textual/ColumnWidthType.java) (see Javadoc at [`ColumnWidthType.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-textual/3.0.5/org.refcodes.textual/org/refcodes/textual/ColumnWidthType.html))
* \[<span style="color:green">MODIFIED</span>\] [`RandomTextGenerartor.java`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-3.0.5/src/main/java/org/refcodes/textual/RandomTextGenerartor.java) (see Javadoc at [`RandomTextGenerartor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-textual/3.0.5/org.refcodes.textual/org/refcodes/textual/RandomTextGenerartor.html))
* \[<span style="color:green">MODIFIED</span>\] [`TablePrinter.java`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-3.0.5/src/main/java/org/refcodes/textual/TablePrinter.java) (see Javadoc at [`TablePrinter.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-textual/3.0.5/org.refcodes.textual/org/refcodes/textual/TablePrinter.html))
* \[<span style="color:green">MODIFIED</span>\] [`TextFormatMode.java`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-3.0.5/src/main/java/org/refcodes/textual/TextFormatMode.java) (see Javadoc at [`TextFormatMode.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-textual/3.0.5/org.refcodes.textual/org/refcodes/textual/TextFormatMode.html))

## Change list &lt;refcodes-criteria&gt; (version 3.0.5)

* \[<span style="color:red">DELETED</span>\] `AbstractSingleCriteriaNode.java`
* \[<span style="color:red">DELETED</span>\] `SingleCriteriaNode.java`
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-3.0.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-3.0.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`MathUtils.java`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-3.0.5/src/main/java/org/matheclipse/parser/client/math/MathUtils.java) (see Javadoc at [`MathUtils.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-criteria/3.0.5/org.matheclipse.parser.client.math/org/matheclipse/parser/client/math/MathUtils.html))
* \[<span style="color:green">MODIFIED</span>\] [`Parser.java`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-3.0.5/src/main/java/org/matheclipse/parser/client/Parser.java) (see Javadoc at [`Parser.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-criteria/3.0.5/org.matheclipse.parser.client/org/matheclipse/parser/client/Parser.html))
* \[<span style="color:green">MODIFIED</span>\] [`AbstractCriteriaLeaf.java`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-3.0.5/src/main/java/org/refcodes/criteria/AbstractCriteriaLeaf.java) (see Javadoc at [`AbstractCriteriaLeaf.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-criteria/3.0.5/org.refcodes.criteria/org/refcodes/criteria/AbstractCriteriaLeaf.html))
* \[<span style="color:green">MODIFIED</span>\] [`AbstractCriteriaNode.java`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-3.0.5/src/main/java/org/refcodes/criteria/AbstractCriteriaNode.java) (see Javadoc at [`AbstractCriteriaNode.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-criteria/3.0.5/org.refcodes.criteria/org/refcodes/criteria/AbstractCriteriaNode.html))
* \[<span style="color:green">MODIFIED</span>\] [`AndCriteria.java`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-3.0.5/src/main/java/org/refcodes/criteria/AndCriteria.java) (see Javadoc at [`AndCriteria.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-criteria/3.0.5/org.refcodes.criteria/org/refcodes/criteria/AndCriteria.html))
* \[<span style="color:green">MODIFIED</span>\] [`ComplexCriteriaException.java`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-3.0.5/src/main/java/org/refcodes/criteria/ComplexCriteriaException.java) (see Javadoc at [`ComplexCriteriaException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-criteria/3.0.5/org.refcodes.criteria/org/refcodes/criteria/ComplexCriteriaException.html))
* \[<span style="color:green">MODIFIED</span>\] [`CriteriaAccessor.java`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-3.0.5/src/main/java/org/refcodes/criteria/CriteriaAccessor.java) (see Javadoc at [`CriteriaAccessor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-criteria/3.0.5/org.refcodes.criteria/org/refcodes/criteria/CriteriaAccessor.html))
* \[<span style="color:green">MODIFIED</span>\] [`Criteria.java`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-3.0.5/src/main/java/org/refcodes/criteria/Criteria.java) (see Javadoc at [`Criteria.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-criteria/3.0.5/org.refcodes.criteria/org/refcodes/criteria/Criteria.html))
* \[<span style="color:green">MODIFIED</span>\] [`CriteriaNode.java`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-3.0.5/src/main/java/org/refcodes/criteria/CriteriaNode.java) (see Javadoc at [`CriteriaNode.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-criteria/3.0.5/org.refcodes.criteria/org/refcodes/criteria/CriteriaNode.html))
* \[<span style="color:green">MODIFIED</span>\] [`CriteriaRuntimeException.java`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-3.0.5/src/main/java/org/refcodes/criteria/CriteriaRuntimeException.java) (see Javadoc at [`CriteriaRuntimeException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-criteria/3.0.5/org.refcodes.criteria/org/refcodes/criteria/CriteriaRuntimeException.html))
* \[<span style="color:green">MODIFIED</span>\] [`CriteriaUtility.java`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-3.0.5/src/main/java/org/refcodes/criteria/CriteriaUtility.java) (see Javadoc at [`CriteriaUtility.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-criteria/3.0.5/org.refcodes.criteria/org/refcodes/criteria/CriteriaUtility.html))
* \[<span style="color:green">MODIFIED</span>\] [`EqualWithCriteria.java`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-3.0.5/src/main/java/org/refcodes/criteria/EqualWithCriteria.java) (see Javadoc at [`EqualWithCriteria.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-criteria/3.0.5/org.refcodes.criteria/org/refcodes/criteria/EqualWithCriteria.html))
* \[<span style="color:green">MODIFIED</span>\] [`ExpressionCriteriaFactory.java`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-3.0.5/src/main/java/org/refcodes/criteria/ExpressionCriteriaFactory.java) (see Javadoc at [`ExpressionCriteriaFactory.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-criteria/3.0.5/org.refcodes.criteria/org/refcodes/criteria/ExpressionCriteriaFactory.html))
* \[<span style="color:green">MODIFIED</span>\] [`NotCriteria.java`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-3.0.5/src/main/java/org/refcodes/criteria/NotCriteria.java) (see Javadoc at [`NotCriteria.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-criteria/3.0.5/org.refcodes.criteria/org/refcodes/criteria/NotCriteria.html))
* \[<span style="color:green">MODIFIED</span>\] [`OrCriteria.java`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-3.0.5/src/main/java/org/refcodes/criteria/OrCriteria.java) (see Javadoc at [`OrCriteria.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-criteria/3.0.5/org.refcodes.criteria/org/refcodes/criteria/OrCriteria.html))
* \[<span style="color:green">MODIFIED</span>\] [`package-info.java`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-3.0.5/src/main/java/org/refcodes/criteria/package-info.java) 
* \[<span style="color:green">MODIFIED</span>\] [`PartitionQueryFactory.java`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-3.0.5/src/main/java/org/refcodes/criteria/PartitionQueryFactory.java) (see Javadoc at [`PartitionQueryFactory.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-criteria/3.0.5/org.refcodes.criteria/org/refcodes/criteria/PartitionQueryFactory.html))
* \[<span style="color:green">MODIFIED</span>\] [`UnknownCriteriaRuntimeException.java`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-3.0.5/src/main/java/org/refcodes/criteria/UnknownCriteriaRuntimeException.java) (see Javadoc at [`UnknownCriteriaRuntimeException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-criteria/3.0.5/org.refcodes.criteria/org/refcodes/criteria/UnknownCriteriaRuntimeException.html))

## Change list &lt;refcodes-io&gt; (version 3.0.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-io/src/refcodes-io-3.0.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-io/src/refcodes-io-3.0.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`SkipAvailableInputStream.java`](https://bitbucket.org/refcodes/refcodes-io/src/refcodes-io-3.0.5/src/main/java/org/refcodes/io/SkipAvailableInputStream.java) (see Javadoc at [`SkipAvailableInputStream.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-io/3.0.5/org.refcodes.io/org/refcodes/io/SkipAvailableInputStream.html))

## Change list &lt;refcodes-tabular&gt; (version 3.0.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-tabular/src/refcodes-tabular-3.0.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-tabular/src/refcodes-tabular-3.0.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`Column.java`](https://bitbucket.org/refcodes/refcodes-tabular/src/refcodes-tabular-3.0.5/src/main/java/org/refcodes/tabular/Column.java) (see Javadoc at [`Column.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-tabular/3.0.5/org.refcodes.tabular/org/refcodes/tabular/Column.html))
* \[<span style="color:green">MODIFIED</span>\] [`HeaderRow.java`](https://bitbucket.org/refcodes/refcodes-tabular/src/refcodes-tabular-3.0.5/src/main/java/org/refcodes/tabular/HeaderRow.java) (see Javadoc at [`HeaderRow.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-tabular/3.0.5/org.refcodes.tabular/org/refcodes/tabular/HeaderRow.html))
* \[<span style="color:green">MODIFIED</span>\] [`TabularUtility.java`](https://bitbucket.org/refcodes/refcodes-tabular/src/refcodes-tabular-3.0.5/src/main/java/org/refcodes/tabular/TabularUtility.java) (see Javadoc at [`TabularUtility.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-tabular/3.0.5/org.refcodes.tabular/org/refcodes/tabular/TabularUtility.html))

## Change list &lt;refcodes-observer&gt; (version 3.0.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-observer/src/refcodes-observer-3.0.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-observer/src/refcodes-observer-3.0.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`AbstractEventMatcher.java`](https://bitbucket.org/refcodes/refcodes-observer/src/refcodes-observer-3.0.5/src/main/java/org/refcodes/observer/AbstractEventMatcher.java) (see Javadoc at [`AbstractEventMatcher.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-observer/3.0.5/org.refcodes.observer/org/refcodes/observer/AbstractEventMatcher.html))
* \[<span style="color:green">MODIFIED</span>\] [`ActionEqualWithEventMatcher.java`](https://bitbucket.org/refcodes/refcodes-observer/src/refcodes-observer-3.0.5/src/main/java/org/refcodes/observer/ActionEqualWithEventMatcher.java) (see Javadoc at [`ActionEqualWithEventMatcher.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-observer/3.0.5/org.refcodes.observer/org/refcodes/observer/ActionEqualWithEventMatcher.html))
* \[<span style="color:green">MODIFIED</span>\] [`AliasEqualWithEventMatcher.java`](https://bitbucket.org/refcodes/refcodes-observer/src/refcodes-observer-3.0.5/src/main/java/org/refcodes/observer/AliasEqualWithEventMatcher.java) (see Javadoc at [`AliasEqualWithEventMatcher.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-observer/3.0.5/org.refcodes.observer/org/refcodes/observer/AliasEqualWithEventMatcher.html))
* \[<span style="color:green">MODIFIED</span>\] [`ChannelEqualWithEventMatcher.java`](https://bitbucket.org/refcodes/refcodes-observer/src/refcodes-observer-3.0.5/src/main/java/org/refcodes/observer/ChannelEqualWithEventMatcher.java) (see Javadoc at [`ChannelEqualWithEventMatcher.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-observer/3.0.5/org.refcodes.observer/org/refcodes/observer/ChannelEqualWithEventMatcher.html))
* \[<span style="color:green">MODIFIED</span>\] [`GroupEqualWithEventMatcher.java`](https://bitbucket.org/refcodes/refcodes-observer/src/refcodes-observer-3.0.5/src/main/java/org/refcodes/observer/GroupEqualWithEventMatcher.java) (see Javadoc at [`GroupEqualWithEventMatcher.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-observer/3.0.5/org.refcodes.observer/org/refcodes/observer/GroupEqualWithEventMatcher.html))
* \[<span style="color:green">MODIFIED</span>\] [`PublisherIsAssignableFromMatcher.java`](https://bitbucket.org/refcodes/refcodes-observer/src/refcodes-observer-3.0.5/src/main/java/org/refcodes/observer/PublisherIsAssignableFromMatcher.java) (see Javadoc at [`PublisherIsAssignableFromMatcher.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-observer/3.0.5/org.refcodes.observer/org/refcodes/observer/PublisherIsAssignableFromMatcher.html))
* \[<span style="color:green">MODIFIED</span>\] [`UniversalIdEqualWithEventMatcher.java`](https://bitbucket.org/refcodes/refcodes-observer/src/refcodes-observer-3.0.5/src/main/java/org/refcodes/observer/UniversalIdEqualWithEventMatcher.java) (see Javadoc at [`UniversalIdEqualWithEventMatcher.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-observer/3.0.5/org.refcodes.observer/org/refcodes/observer/UniversalIdEqualWithEventMatcher.html))

## Change list &lt;refcodes-command&gt; (version 3.0.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-command/src/refcodes-command-3.0.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-command/src/refcodes-command-3.0.5/README.md) 

## Change list &lt;refcodes-cli&gt; (version 3.0.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.0.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.0.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`CliSugar.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.0.5/src/main/java/org/refcodes/cli/CliSugar.java) (see Javadoc at [`CliSugar.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/3.0.5/org.refcodes.cli/org/refcodes/cli/CliSugar.html))
* \[<span style="color:green">MODIFIED</span>\] [`Operand.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.0.5/src/main/java/org/refcodes/cli/Operand.java) (see Javadoc at [`Operand.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/3.0.5/org.refcodes.cli/org/refcodes/cli/Operand.html))

## Change list &lt;refcodes-audio&gt; (version 3.0.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-audio/src/refcodes-audio-3.0.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-audio/src/refcodes-audio-3.0.5/README.md) 
3.0.5/org.refcodes.audio/org/refcodes/audio/SoundSample.html))

## Change list &lt;refcodes-codec&gt; (version 3.0.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-3.0.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-3.0.5/README.md) 

## Change list &lt;refcodes-component-ext&gt; (version 3.0.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-3.0.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-3.0.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-3.0.5/refcodes-component-ext-observer/pom.xml) 

## Change list &lt;refcodes-properties&gt; (version 3.0.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-3.0.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`Properties.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-3.0.5/src/main/java/org/refcodes/properties/Properties.java) (see Javadoc at [`Properties.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties/3.0.5/org.refcodes.properties/org/refcodes/properties/Properties.html))
* \[<span style="color:green">MODIFIED</span>\] [`ResourceProperties.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-3.0.5/src/main/java/org/refcodes/properties/ResourceProperties.java) (see Javadoc at [`ResourceProperties.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties/3.0.5/org.refcodes.properties/org/refcodes/properties/ResourceProperties.html))

## Change list &lt;refcodes-security&gt; (version 3.0.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security/src/refcodes-security-3.0.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security/src/refcodes-security-3.0.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`Decrypter.java`](https://bitbucket.org/refcodes/refcodes-security/src/refcodes-security-3.0.5/src/main/java/org/refcodes/security/Decrypter.java) (see Javadoc at [`Decrypter.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-security/3.0.5/org.refcodes.security/org/refcodes/security/Decrypter.html))
* \[<span style="color:green">MODIFIED</span>\] [`Encrypter.java`](https://bitbucket.org/refcodes/refcodes-security/src/refcodes-security-3.0.5/src/main/java/org/refcodes/security/Encrypter.java) (see Javadoc at [`Encrypter.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-security/3.0.5/org.refcodes.security/org/refcodes/security/Encrypter.html))

## Change list &lt;refcodes-security-alt&gt; (version 3.0.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-3.0.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-3.0.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-3.0.5/refcodes-security-alt-chaos/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-3.0.5/refcodes-security-alt-chaos/README.md) 

## Change list &lt;refcodes-security-ext&gt; (version 3.0.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-3.0.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-3.0.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-3.0.5/refcodes-security-ext-chaos/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-3.0.5/refcodes-security-ext-chaos/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-3.0.5/refcodes-security-ext-spring/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-3.0.5/refcodes-security-ext-spring/README.md) 

## Change list &lt;refcodes-properties-ext&gt; (version 3.0.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.0.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.0.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.0.5/refcodes-properties-ext-cli/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.0.5/refcodes-properties-ext-cli/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.0.5/refcodes-properties-ext-obfuscation/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.0.5/refcodes-properties-ext-obfuscation/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.0.5/refcodes-properties-ext-observer/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.0.5/refcodes-properties-ext-observer/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.0.5/refcodes-properties-ext-runtime/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.0.5/refcodes-properties-ext-runtime/README.md) 

## Change list &lt;refcodes-logger&gt; (version 3.0.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger/src/refcodes-logger-3.0.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger/src/refcodes-logger-3.0.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`AbstractPartedLogger.java`](https://bitbucket.org/refcodes/refcodes-logger/src/refcodes-logger-3.0.5/src/main/java/org/refcodes/logger/AbstractPartedLogger.java) (see Javadoc at [`AbstractPartedLogger.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-logger/3.0.5/org.refcodes.logger/org/refcodes/logger/AbstractPartedLogger.html))

## Change list &lt;refcodes-logger-alt&gt; (version 3.0.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.0.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.0.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.0.5/refcodes-logger-alt-async/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.0.5/refcodes-logger-alt-async/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.0.5/refcodes-logger-alt-console/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.0.5/refcodes-logger-alt-console/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.0.5/refcodes-logger-alt-io/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.0.5/refcodes-logger-alt-io/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.0.5/refcodes-logger-alt-simpledb/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.0.5/refcodes-logger-alt-simpledb/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.0.5/refcodes-logger-alt-slf4j/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.0.5/refcodes-logger-alt-spring/pom.xml) 

## Change list &lt;refcodes-logger-ext&gt; (version 3.0.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-3.0.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-3.0.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-3.0.5/refcodes-logger-ext-slf4j/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-3.0.5/refcodes-logger-ext-slf4j/README.md) 

## Change list &lt;refcodes-graphical-ext&gt; (version 3.0.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-3.0.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-3.0.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-3.0.5/refcodes-graphical-ext-javafx/pom.xml) 

## Change list &lt;refcodes-checkerboard&gt; (version 3.0.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-3.0.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-3.0.5/README.md) 

## Change list &lt;refcodes-checkerboard-alt&gt; (version 3.0.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-alt/src/refcodes-checkerboard-alt-3.0.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-alt/src/refcodes-checkerboard-alt-3.0.5/refcodes-checkerboard-alt-javafx/pom.xml) 

## Change list &lt;refcodes-net&gt; (version 3.0.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-3.0.5/pom.xml) 

## Change list &lt;refcodes-web&gt; (version 3.0.5)

* \[<span style="color:green">MODIFIED</span>\] [`.gitignore`](https://bitbucket.org/refcodes/refcodes-web/src/refcodes-web-3.0.5/.gitignore) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-web/src/refcodes-web-3.0.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-web/src/refcodes-web-3.0.5/README.md) 

## Change list &lt;refcodes-rest&gt; (version 3.0.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-3.0.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-3.0.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`OauthTokenHandlerTest.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-3.0.5/src/test/java/org/refcodes/rest/OauthTokenHandlerTest.java) 

## Change list &lt;refcodes-hal&gt; (version 3.0.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-3.0.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-3.0.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`HalData.java`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-3.0.5/src/main/java/org/refcodes/hal/HalData.java) (see Javadoc at [`HalData.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-hal/3.0.5/org.refcodes.hal/org/refcodes/hal/HalData.html))
* \[<span style="color:green">MODIFIED</span>\] [`HalStruct.java`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-3.0.5/src/main/java/org/refcodes/hal/HalStruct.java) (see Javadoc at [`HalStruct.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-hal/3.0.5/org.refcodes.hal/org/refcodes/hal/HalStruct.html))
* \[<span style="color:green">MODIFIED</span>\] [`TraversalMode.java`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-3.0.5/src/main/java/org/refcodes/hal/TraversalMode.java) (see Javadoc at [`TraversalMode.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-hal/3.0.5/org.refcodes.hal/org/refcodes/hal/TraversalMode.html))

## Change list &lt;refcodes-eventbus&gt; (version 3.0.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-eventbus/src/refcodes-eventbus-3.0.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-eventbus/src/refcodes-eventbus-3.0.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`EventBus.java`](https://bitbucket.org/refcodes/refcodes-eventbus/src/refcodes-eventbus-3.0.5/src/main/java/org/refcodes/eventbus/EventBus.java) (see Javadoc at [`EventBus.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-eventbus/3.0.5/org.refcodes.eventbus/org/refcodes/eventbus/EventBus.html))

## Change list &lt;refcodes-eventbus-ext&gt; (version 3.0.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-3.0.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-3.0.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-3.0.5/refcodes-eventbus-ext-application/pom.xml) 

## Change list &lt;refcodes-filesystem&gt; (version 3.0.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-filesystem/src/refcodes-filesystem-3.0.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-filesystem/src/refcodes-filesystem-3.0.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`FileHandleImpl.java`](https://bitbucket.org/refcodes/refcodes-filesystem/src/refcodes-filesystem-3.0.5/src/main/java/org/refcodes/filesystem/FileHandleImpl.java) (see Javadoc at [`FileHandleImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-filesystem/3.0.5/org.refcodes.filesystem/org/refcodes/filesystem/FileHandleImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`FileSystem.java`](https://bitbucket.org/refcodes/refcodes-filesystem/src/refcodes-filesystem-3.0.5/src/main/java/org/refcodes/filesystem/FileSystem.java) (see Javadoc at [`FileSystem.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-filesystem/3.0.5/org.refcodes.filesystem/org/refcodes/filesystem/FileSystem.html))

## Change list &lt;refcodes-filesystem-alt&gt; (version 3.0.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-filesystem-alt/src/refcodes-filesystem-alt-3.0.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-filesystem-alt/src/refcodes-filesystem-alt-3.0.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-filesystem-alt/src/refcodes-filesystem-alt-3.0.5/refcodes-filesystem-alt-s3/pom.xml) 

## Change list &lt;refcodes-forwardsecrecy&gt; (version 3.0.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy/src/refcodes-forwardsecrecy-3.0.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy/src/refcodes-forwardsecrecy-3.0.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`AbstractCipherVersionGenerator.java`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy/src/refcodes-forwardsecrecy-3.0.5/src/main/java/org/refcodes/forwardsecrecy/AbstractCipherVersionGenerator.java) (see Javadoc at [`AbstractCipherVersionGenerator.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-forwardsecrecy/3.0.5/org.refcodes.forwardsecrecy/org/refcodes/forwardsecrecy/AbstractCipherVersionGenerator.html))
* \[<span style="color:green">MODIFIED</span>\] [`CipherVersion.java`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy/src/refcodes-forwardsecrecy-3.0.5/src/main/java/org/refcodes/forwardsecrecy/CipherVersion.java) (see Javadoc at [`CipherVersion.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-forwardsecrecy/3.0.5/org.refcodes.forwardsecrecy/org/refcodes/forwardsecrecy/CipherVersion.html))

## Change list &lt;refcodes-forwardsecrecy-alt&gt; (version 3.0.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-3.0.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-3.0.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-3.0.5/refcodes-forwardsecrecy-alt-filesystem/pom.xml) 

## Change list &lt;refcodes-io-ext&gt; (version 3.0.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-3.0.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-3.0.5/refcodes-io-ext-observer/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-3.0.5/refcodes-io-ext-observer/README.md) 

## Change list &lt;refcodes-interceptor&gt; (version 3.0.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-interceptor/src/refcodes-interceptor-3.0.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-interceptor/src/refcodes-interceptor-3.0.5/README.md) 

## Change list &lt;refcodes-jobbus&gt; (version 3.0.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-jobbus/src/refcodes-jobbus-3.0.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-jobbus/src/refcodes-jobbus-3.0.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`JobBus.java`](https://bitbucket.org/refcodes/refcodes-jobbus/src/refcodes-jobbus-3.0.5/src/main/java/org/refcodes/jobbus/JobBus.java) (see Javadoc at [`JobBus.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-jobbus/3.0.5/org.refcodes.jobbus/org/refcodes/jobbus/JobBus.html))

## Change list &lt;refcodes-rest-ext&gt; (version 3.0.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-3.0.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-3.0.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-3.0.5/refcodes-rest-ext-eureka/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-3.0.5/refcodes-rest-ext-eureka/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`EurekaRegistryContext.java`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-3.0.5/refcodes-rest-ext-eureka/src/main/java/org/refcodes/rest/ext/eureka/EurekaRegistryContext.java) (see Javadoc at [`EurekaRegistryContext.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest-ext-eureka/3.0.5/org.refcodes.rest.ext.eureka/org/refcodes/rest/ext/eureka/EurekaRegistryContext.html))

## Change list &lt;refcodes-remoting&gt; (version 3.0.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-remoting/src/refcodes-remoting-3.0.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-remoting/src/refcodes-remoting-3.0.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`SerializeUtility.java`](https://bitbucket.org/refcodes/refcodes-remoting/src/refcodes-remoting-3.0.5/src/main/java/org/refcodes/remoting/SerializeUtility.java) (see Javadoc at [`SerializeUtility.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-remoting/3.0.5/org.refcodes.remoting/org/refcodes/remoting/SerializeUtility.html))

## Change list &lt;refcodes-remoting-ext&gt; (version 3.0.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-3.0.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-3.0.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-3.0.5/refcodes-remoting-ext-observer/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-3.0.5/refcodes-remoting-ext-observer/README.md) 

## Change list &lt;refcodes-serial&gt; (version 3.0.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial/src/refcodes-serial-3.0.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-serial/src/refcodes-serial-3.0.5/README.md) 

## Change list &lt;refcodes-serial-alt&gt; (version 3.0.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial-alt/src/refcodes-serial-alt-3.0.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-serial-alt/src/refcodes-serial-alt-3.0.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial-alt/src/refcodes-serial-alt-3.0.5/refcodes-serial-alt-net/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-serial-alt/src/refcodes-serial-alt-3.0.5/refcodes-serial-alt-net/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial-alt/src/refcodes-serial-alt-3.0.5/refcodes-serial-alt-tty/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-serial-alt/src/refcodes-serial-alt-3.0.5/refcodes-serial-alt-tty/README.md) 

## Change list &lt;refcodes-serial-ext&gt; (version 3.0.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-3.0.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-3.0.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-3.0.5/refcodes-serial-ext-handshake/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-3.0.5/refcodes-serial-ext-handshake/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`HandshakePortController.java`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-3.0.5/refcodes-serial-ext-handshake/src/main/java/org/refcodes/serial/ext/handshake/HandshakePortController.java) (see Javadoc at [`HandshakePortController.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-serial-ext-handshake/3.0.5/org.refcodes.serial.ext.handshake/org/refcodes/serial/ext/handshake/HandshakePortController.html))
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-3.0.5/refcodes-serial-ext-observer/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-3.0.5/refcodes-serial-ext-observer/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-3.0.5/refcodes-serial-ext-security/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-3.0.5/refcodes-serial-ext-security/README.md) 

## Change list &lt;refcodes-p2p&gt; (version 3.0.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-p2p/src/refcodes-p2p-3.0.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-p2p/src/refcodes-p2p-3.0.5/README.md) 

## Change list &lt;refcodes-p2p-alt&gt; (version 3.0.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-p2p-alt/src/refcodes-p2p-alt-3.0.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-p2p-alt/src/refcodes-p2p-alt-3.0.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-p2p-alt/src/refcodes-p2p-alt-3.0.5/refcodes-p2p-alt-rest/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-p2p-alt/src/refcodes-p2p-alt-3.0.5/refcodes-p2p-alt-serial/pom.xml) 

## Change list &lt;refcodes-p2p-ext&gt; (version 3.0.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-p2p-ext/src/refcodes-p2p-ext-3.0.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-p2p-ext/src/refcodes-p2p-ext-3.0.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-p2p-ext/src/refcodes-p2p-ext-3.0.5/refcodes-p2p-ext-observer/pom.xml) 

## Change list &lt;refcodes-servicebus&gt; (version 3.0.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-servicebus/src/refcodes-servicebus-3.0.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-servicebus/src/refcodes-servicebus-3.0.5/README.md) 

## Change list &lt;refcodes-servicebus-alt&gt; (version 3.0.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-servicebus-alt/src/refcodes-servicebus-alt-3.0.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-servicebus-alt/src/refcodes-servicebus-alt-3.0.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-servicebus-alt/src/refcodes-servicebus-alt-3.0.5/refcodes-servicebus-alt-spring/pom.xml) 

## Change list &lt;refcodes-tabular-alt&gt; (version 3.0.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-tabular-alt/src/refcodes-tabular-alt-3.0.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-tabular-alt/src/refcodes-tabular-alt-3.0.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-tabular-alt/src/refcodes-tabular-alt-3.0.5/refcodes-tabular-alt-forwardsecrecy/pom.xml) 

## Change list &lt;refcodes-archetype&gt; (version 3.0.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype/src/refcodes-archetype-3.0.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype/src/refcodes-archetype-3.0.5/README.md) 

## Change list &lt;refcodes-archetype-alt&gt; (version 3.0.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.0.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.0.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.0.5/refcodes-archetype-alt-c2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.0.5/refcodes-archetype-alt-c2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.0.5/refcodes-archetype-alt-cli/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.0.5/refcodes-archetype-alt-cli/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.0.5/refcodes-archetype-alt-csv/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.0.5/refcodes-archetype-alt-csv/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`Main.java`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.0.5/refcodes-archetype-alt-csv/src/main/resources/archetype-resources/src/main/java/Main.java) (see Javadoc at [`Main.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-archetype-alt-csv/src/main/resources/archetype-resources/3.0.5/src.main.resources.archetype-resources/Main.html))
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.0.5/refcodes-archetype-alt-eventbus/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.0.5/refcodes-archetype-alt-eventbus/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.0.5/refcodes-archetype-alt-rest/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.0.5/refcodes-archetype-alt-rest/README.md) 
