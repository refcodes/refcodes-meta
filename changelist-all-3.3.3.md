> This change list has been auto-generated on `triton` by `steiner` with `changelist-all.sh` on the 2023-10-23 at 15:19:40.

## Change list &lt;refcodes-licensing&gt; (version 3.3.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-licensing/src/refcodes-licensing-3.3.3/pom.xml) 

## Change list &lt;refcodes-parent&gt; (version 3.3.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-parent/src/refcodes-parent-3.3.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-parent/src/refcodes-parent-3.3.3/README.md) 

## Change list &lt;refcodes-time&gt; (version 3.3.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-time/src/refcodes-time-3.3.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-time/src/refcodes-time-3.3.3/README.md) 

## Change list &lt;refcodes-mixin&gt; (version 3.3.3)

* \[<span style="color:blue">ADDED</span>\] [`Bytes.java`](https://bitbucket.org/refcodes/refcodes-mixin/src/refcodes-mixin-3.3.3/src/main/java/org/refcodes/mixin/Bytes.java) (see Javadoc at [`Bytes.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-mixin/3.3.3/org.refcodes.mixin/org/refcodes/mixin/Bytes.html))
* \[<span style="color:red">DELETED</span>\] `AbstractSchema.java`
* \[<span style="color:red">DELETED</span>\] `Schemable.java`
* \[<span style="color:red">DELETED</span>\] `Schema.java`
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-mixin/src/refcodes-mixin-3.3.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-mixin/src/refcodes-mixin-3.3.3/README.md) 

## Change list &lt;refcodes-data&gt; (version 3.3.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data/src/refcodes-data-3.3.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data/src/refcodes-data-3.3.3/README.md) 

## Change list &lt;refcodes-exception&gt; (version 3.3.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-exception/src/refcodes-exception-3.3.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-exception/src/refcodes-exception-3.3.3/README.md) 

## Change list &lt;refcodes-factory&gt; (version 3.3.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-factory/src/refcodes-factory-3.3.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-factory/src/refcodes-factory-3.3.3/README.md) 

## Change list &lt;refcodes-factory-alt&gt; (version 3.3.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-factory-alt/src/refcodes-factory-alt-3.3.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-factory-alt/src/refcodes-factory-alt-3.3.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-factory-alt/src/refcodes-factory-alt-3.3.3/refcodes-factory-alt-spring/pom.xml) 

## Change list &lt;refcodes-controlflow&gt; (version 3.3.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-controlflow/src/refcodes-controlflow-3.3.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-controlflow/src/refcodes-controlflow-3.3.3/README.md) 

## Change list &lt;refcodes-numerical&gt; (version 3.3.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-numerical/src/refcodes-numerical-3.3.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-numerical/src/refcodes-numerical-3.3.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`NumericalUtility.java`](https://bitbucket.org/refcodes/refcodes-numerical/src/refcodes-numerical-3.3.3/src/main/java/org/refcodes/numerical/NumericalUtility.java) (see Javadoc at [`NumericalUtility.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-numerical/3.3.3/org.refcodes.numerical/org/refcodes/numerical/NumericalUtility.html))

## Change list &lt;refcodes-generator&gt; (version 3.3.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-generator/src/refcodes-generator-3.3.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-generator/src/refcodes-generator-3.3.3/README.md) 

## Change list &lt;refcodes-schema&gt; (version 3.3.3)

* \[<span style="color:blue">ADDED</span>\] [`package-info.java`](https://bitbucket.org/refcodes/refcodes-schema/src/refcodes-schema-3.3.3/src/main/java/org/refcodes/schema/package-info.java) 
* \[<span style="color:blue">ADDED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-schema/src/refcodes-schema-3.3.3/pom.xml) 
* \[<span style="color:blue">ADDED</span>\] [`module-info.java`](https://bitbucket.org/refcodes/refcodes-schema/src/refcodes-schema-3.3.3/src/main/java/module-info.java) (see Javadoc at [`module-info.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-schema/3.3.3/./module-info.html))
* \[<span style="color:blue">ADDED</span>\] [`AbstractSchema.java`](https://bitbucket.org/refcodes/refcodes-schema/src/refcodes-schema-3.3.3/src/main/java/org/refcodes/schema/AbstractSchema.java) (see Javadoc at [`AbstractSchema.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-schema/3.3.3/org.refcodes.schema/org/refcodes/schema/AbstractSchema.html))
* \[<span style="color:blue">ADDED</span>\] [`JsonVisitor.java`](https://bitbucket.org/refcodes/refcodes-schema/src/refcodes-schema-3.3.3/src/main/java/org/refcodes/schema/JsonVisitor.java) (see Javadoc at [`JsonVisitor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-schema/3.3.3/org.refcodes.schema/org/refcodes/schema/JsonVisitor.html))
* \[<span style="color:blue">ADDED</span>\] [`Schemable.java`](https://bitbucket.org/refcodes/refcodes-schema/src/refcodes-schema-3.3.3/src/main/java/org/refcodes/schema/Schemable.java) (see Javadoc at [`Schemable.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-schema/3.3.3/org.refcodes.schema/org/refcodes/schema/Schemable.html))
* \[<span style="color:blue">ADDED</span>\] [`Schema.java`](https://bitbucket.org/refcodes/refcodes-schema/src/refcodes-schema-3.3.3/src/main/java/org/refcodes/schema/Schema.java) (see Javadoc at [`Schema.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-schema/3.3.3/org.refcodes.schema/org/refcodes/schema/Schema.html))
* \[<span style="color:blue">ADDED</span>\] [`SchemaVisitor.java`](https://bitbucket.org/refcodes/refcodes-schema/src/refcodes-schema-3.3.3/src/main/java/org/refcodes/schema/SchemaVisitor.java) (see Javadoc at [`SchemaVisitor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-schema/3.3.3/org.refcodes.schema/org/refcodes/schema/SchemaVisitor.html))

## Change list &lt;refcodes-matcher&gt; (version 3.3.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-matcher/src/refcodes-matcher-3.3.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-matcher/src/refcodes-matcher-3.3.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`module-info.java`](https://bitbucket.org/refcodes/refcodes-matcher/src/refcodes-matcher-3.3.3/src/main/java/module-info.java) (see Javadoc at [`module-info.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-matcher/3.3.3/./module-info.html))
* \[<span style="color:green">MODIFIED</span>\] [`AbstractMatcheeMatcher.java`](https://bitbucket.org/refcodes/refcodes-matcher/src/refcodes-matcher-3.3.3/src/main/java/org/refcodes/matcher/AbstractMatcheeMatcher.java) (see Javadoc at [`AbstractMatcheeMatcher.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-matcher/3.3.3/org.refcodes.matcher/org/refcodes/matcher/AbstractMatcheeMatcher.html))
* \[<span style="color:green">MODIFIED</span>\] [`IsAssignableFromMatcher.java`](https://bitbucket.org/refcodes/refcodes-matcher/src/refcodes-matcher-3.3.3/src/main/java/org/refcodes/matcher/IsAssignableFromMatcher.java) (see Javadoc at [`IsAssignableFromMatcher.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-matcher/3.3.3/org.refcodes.matcher/org/refcodes/matcher/IsAssignableFromMatcher.html))
* \[<span style="color:green">MODIFIED</span>\] [`Matcher.java`](https://bitbucket.org/refcodes/refcodes-matcher/src/refcodes-matcher-3.3.3/src/main/java/org/refcodes/matcher/Matcher.java) (see Javadoc at [`Matcher.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-matcher/3.3.3/org.refcodes.matcher/org/refcodes/matcher/Matcher.html))
* \[<span style="color:green">MODIFIED</span>\] [`MatcherSchema.java`](https://bitbucket.org/refcodes/refcodes-matcher/src/refcodes-matcher-3.3.3/src/main/java/org/refcodes/matcher/MatcherSchema.java) (see Javadoc at [`MatcherSchema.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-matcher/3.3.3/org.refcodes.matcher/org/refcodes/matcher/MatcherSchema.html))

## Change list &lt;refcodes-struct&gt; (version 3.3.3)

* \[<span style="color:blue">ADDED</span>\] [`TypeUtilityTest.java`](https://bitbucket.org/refcodes/refcodes-struct/src/refcodes-struct-3.3.3/src/test/java/org/refcodes/struct/TypeUtilityTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-struct/src/refcodes-struct-3.3.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-struct/src/refcodes-struct-3.3.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`ClassStructMapBuilderImpl.java`](https://bitbucket.org/refcodes/refcodes-struct/src/refcodes-struct-3.3.3/src/main/java/org/refcodes/struct/ClassStructMapBuilderImpl.java) (see Javadoc at [`ClassStructMapBuilderImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-struct/3.3.3/org.refcodes.struct/org/refcodes/struct/ClassStructMapBuilderImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`PathMapBuilderImpl.java`](https://bitbucket.org/refcodes/refcodes-struct/src/refcodes-struct-3.3.3/src/main/java/org/refcodes/struct/PathMapBuilderImpl.java) (see Javadoc at [`PathMapBuilderImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-struct/3.3.3/org.refcodes.struct/org/refcodes/struct/PathMapBuilderImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`TypeUtility.java`](https://bitbucket.org/refcodes/refcodes-struct/src/refcodes-struct-3.3.3/src/main/java/org/refcodes/struct/TypeUtility.java) (see Javadoc at [`TypeUtility.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-struct/3.3.3/org.refcodes.struct/org/refcodes/struct/TypeUtility.html))

## Change list &lt;refcodes-runtime&gt; (version 3.3.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-3.3.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-3.3.3/README.md) 

## Change list &lt;refcodes-struct-ext&gt; (version 3.3.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-struct-ext/src/refcodes-struct-ext-3.3.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-struct-ext/src/refcodes-struct-ext-3.3.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-struct-ext/src/refcodes-struct-ext-3.3.3/refcodes-struct-ext-factory/pom.xml) 

## Change list &lt;refcodes-component&gt; (version 3.3.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.3.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.3.3/README.md) 

## Change list &lt;refcodes-data-ext&gt; (version 3.3.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-3.3.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-3.3.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-3.3.3/refcodes-data-ext-checkers/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-3.3.3/refcodes-data-ext-checkers/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-3.3.3/refcodes-data-ext-chess/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-3.3.3/refcodes-data-ext-chess/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-3.3.3/refcodes-data-ext-corporate/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-3.3.3/refcodes-data-ext-corporate/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-3.3.3/refcodes-data-ext-symbols/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-3.3.3/refcodes-data-ext-symbols/README.md) 

## Change list &lt;refcodes-graphical&gt; (version 3.3.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-graphical/src/refcodes-graphical-3.3.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-graphical/src/refcodes-graphical-3.3.3/README.md) 

## Change list &lt;refcodes-textual&gt; (version 3.3.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-3.3.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-3.3.3/README.md) 

## Change list &lt;refcodes-criteria&gt; (version 3.3.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-3.3.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-3.3.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`module-info.java`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-3.3.3/src/main/java/module-info.java) (see Javadoc at [`module-info.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-criteria/3.3.3/./module-info.html))
* \[<span style="color:green">MODIFIED</span>\] [`AbstractCriteriaLeaf.java`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-3.3.3/src/main/java/org/refcodes/criteria/AbstractCriteriaLeaf.java) (see Javadoc at [`AbstractCriteriaLeaf.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-criteria/3.3.3/org.refcodes.criteria/org/refcodes/criteria/AbstractCriteriaLeaf.html))
* \[<span style="color:green">MODIFIED</span>\] [`AbstractCriteriaNode.java`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-3.3.3/src/main/java/org/refcodes/criteria/AbstractCriteriaNode.java) (see Javadoc at [`AbstractCriteriaNode.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-criteria/3.3.3/org.refcodes.criteria/org/refcodes/criteria/AbstractCriteriaNode.html))
* \[<span style="color:green">MODIFIED</span>\] [`Criteria.java`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-3.3.3/src/main/java/org/refcodes/criteria/Criteria.java) (see Javadoc at [`Criteria.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-criteria/3.3.3/org.refcodes.criteria/org/refcodes/criteria/Criteria.html))
* \[<span style="color:green">MODIFIED</span>\] [`CriteriaSchema.java`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-3.3.3/src/main/java/org/refcodes/criteria/CriteriaSchema.java) (see Javadoc at [`CriteriaSchema.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-criteria/3.3.3/org.refcodes.criteria/org/refcodes/criteria/CriteriaSchema.html))

## Change list &lt;refcodes-io&gt; (version 3.3.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-io/src/refcodes-io-3.3.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-io/src/refcodes-io-3.3.3/README.md) 

## Change list &lt;refcodes-tabular&gt; (version 3.3.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-tabular/src/refcodes-tabular-3.3.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-tabular/src/refcodes-tabular-3.3.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`CsvRecordReader.java`](https://bitbucket.org/refcodes/refcodes-tabular/src/refcodes-tabular-3.3.3/src/main/java/org/refcodes/tabular/CsvRecordReader.java) (see Javadoc at [`CsvRecordReader.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-tabular/3.3.3/org.refcodes.tabular/org/refcodes/tabular/CsvRecordReader.html))

## Change list &lt;refcodes-observer&gt; (version 3.3.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-observer/src/refcodes-observer-3.3.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-observer/src/refcodes-observer-3.3.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`ActionEqualWithEventMatcher.java`](https://bitbucket.org/refcodes/refcodes-observer/src/refcodes-observer-3.3.3/src/main/java/org/refcodes/observer/ActionEqualWithEventMatcher.java) (see Javadoc at [`ActionEqualWithEventMatcher.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-observer/3.3.3/org.refcodes.observer/org/refcodes/observer/ActionEqualWithEventMatcher.html))
* \[<span style="color:green">MODIFIED</span>\] [`AliasEqualWithEventMatcher.java`](https://bitbucket.org/refcodes/refcodes-observer/src/refcodes-observer-3.3.3/src/main/java/org/refcodes/observer/AliasEqualWithEventMatcher.java) (see Javadoc at [`AliasEqualWithEventMatcher.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-observer/3.3.3/org.refcodes.observer/org/refcodes/observer/AliasEqualWithEventMatcher.html))
* \[<span style="color:green">MODIFIED</span>\] [`ChannelEqualWithEventMatcher.java`](https://bitbucket.org/refcodes/refcodes-observer/src/refcodes-observer-3.3.3/src/main/java/org/refcodes/observer/ChannelEqualWithEventMatcher.java) (see Javadoc at [`ChannelEqualWithEventMatcher.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-observer/3.3.3/org.refcodes.observer/org/refcodes/observer/ChannelEqualWithEventMatcher.html))
* \[<span style="color:green">MODIFIED</span>\] [`GroupEqualWithEventMatcher.java`](https://bitbucket.org/refcodes/refcodes-observer/src/refcodes-observer-3.3.3/src/main/java/org/refcodes/observer/GroupEqualWithEventMatcher.java) (see Javadoc at [`GroupEqualWithEventMatcher.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-observer/3.3.3/org.refcodes.observer/org/refcodes/observer/GroupEqualWithEventMatcher.html))
* \[<span style="color:green">MODIFIED</span>\] [`PublisherTypeOfEventMatcher.java`](https://bitbucket.org/refcodes/refcodes-observer/src/refcodes-observer-3.3.3/src/main/java/org/refcodes/observer/PublisherTypeOfEventMatcher.java) (see Javadoc at [`PublisherTypeOfEventMatcher.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-observer/3.3.3/org.refcodes.observer/org/refcodes/observer/PublisherTypeOfEventMatcher.html))
* \[<span style="color:green">MODIFIED</span>\] [`UniversalIdEqualWithEventMatcher.java`](https://bitbucket.org/refcodes/refcodes-observer/src/refcodes-observer-3.3.3/src/main/java/org/refcodes/observer/UniversalIdEqualWithEventMatcher.java) (see Javadoc at [`UniversalIdEqualWithEventMatcher.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-observer/3.3.3/org.refcodes.observer/org/refcodes/observer/UniversalIdEqualWithEventMatcher.html))

## Change list &lt;refcodes-command&gt; (version 3.3.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-command/src/refcodes-command-3.3.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-command/src/refcodes-command-3.3.3/README.md) 

## Change list &lt;refcodes-cli&gt; (version 3.3.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.3.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.3.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`module-info.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.3.3/src/main/java/module-info.java) (see Javadoc at [`module-info.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/3.3.3/./module-info.html))
* \[<span style="color:green">MODIFIED</span>\] [`AbstractOperand.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.3.3/src/main/java/org/refcodes/cli/AbstractOperand.java) (see Javadoc at [`AbstractOperand.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/3.3.3/org.refcodes.cli/org/refcodes/cli/AbstractOperand.html))
* \[<span style="color:green">MODIFIED</span>\] [`AllCondition.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.3.3/src/main/java/org/refcodes/cli/AllCondition.java) (see Javadoc at [`AllCondition.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/3.3.3/org.refcodes.cli/org/refcodes/cli/AllCondition.html))
* \[<span style="color:green">MODIFIED</span>\] [`CliSchema.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.3.3/src/main/java/org/refcodes/cli/CliSchema.java) (see Javadoc at [`CliSchema.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/3.3.3/org.refcodes.cli/org/refcodes/cli/CliSchema.html))
* \[<span style="color:green">MODIFIED</span>\] [`ParseArgs.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.3.3/src/main/java/org/refcodes/cli/ParseArgs.java) (see Javadoc at [`ParseArgs.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/3.3.3/org.refcodes.cli/org/refcodes/cli/ParseArgs.html))
* \[<span style="color:green">MODIFIED</span>\] [`Term.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.3.3/src/main/java/org/refcodes/cli/Term.java) (see Javadoc at [`Term.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/3.3.3/org.refcodes.cli/org/refcodes/cli/Term.html))
* \[<span style="color:green">MODIFIED</span>\] [`MessageTest.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.3.3/src/test/java/org/refcodes/cli/MessageTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`SchemaTest.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.3.3/src/test/java/org/refcodes/cli/SchemaTest.java) 

## Change list &lt;refcodes-audio&gt; (version 3.3.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-audio/src/refcodes-audio-3.3.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-audio/src/refcodes-audio-3.3.3/README.md) 

## Change list &lt;refcodes-codec&gt; (version 3.3.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-3.3.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-3.3.3/README.md) 

## Change list &lt;refcodes-component-ext&gt; (version 3.3.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-3.3.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-3.3.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-3.3.3/refcodes-component-ext-observer/pom.xml) 

## Change list &lt;refcodes-properties&gt; (version 3.3.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-3.3.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`JavaProperties.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-3.3.3/src/main/java/org/refcodes/properties/JavaProperties.java) (see Javadoc at [`JavaProperties.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties/3.3.3/org.refcodes.properties/org/refcodes/properties/JavaProperties.html))

## Change list &lt;refcodes-security&gt; (version 3.3.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security/src/refcodes-security-3.3.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security/src/refcodes-security-3.3.3/README.md) 

## Change list &lt;refcodes-security-alt&gt; (version 3.3.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-3.3.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-3.3.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-3.3.3/refcodes-security-alt-chaos/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-3.3.3/refcodes-security-alt-chaos/README.md) 

## Change list &lt;refcodes-security-ext&gt; (version 3.3.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-3.3.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-3.3.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-3.3.3/refcodes-security-ext-chaos/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-3.3.3/refcodes-security-ext-chaos/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-3.3.3/refcodes-security-ext-spring/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-3.3.3/refcodes-security-ext-spring/README.md) 

## Change list &lt;refcodes-properties-ext&gt; (version 3.3.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.3.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.3.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.3.3/refcodes-properties-ext-application/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.3.3/refcodes-properties-ext-application/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.3.3/refcodes-properties-ext-cli/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.3.3/refcodes-properties-ext-cli/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`module-info.java`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.3.3/refcodes-properties-ext-cli/src/main/java/module-info.java) (see Javadoc at [`module-info.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties-ext-cli/3.3.3//module-info.html))
* \[<span style="color:green">MODIFIED</span>\] [`ArgsParserPropertiesTest.java`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.3.3/refcodes-properties-ext-cli/src/test/java/org/refcodes/properties/ext/cli/ArgsParserPropertiesTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.3.3/refcodes-properties-ext-obfuscation/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.3.3/refcodes-properties-ext-obfuscation/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.3.3/refcodes-properties-ext-observer/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.3.3/refcodes-properties-ext-observer/README.md) 

## Change list &lt;refcodes-logger&gt; (version 3.3.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger/src/refcodes-logger-3.3.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger/src/refcodes-logger-3.3.3/README.md) 

## Change list &lt;refcodes-logger-alt&gt; (version 3.3.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.3.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.3.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.3.3/refcodes-logger-alt-async/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.3.3/refcodes-logger-alt-async/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.3.3/refcodes-logger-alt-console/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.3.3/refcodes-logger-alt-console/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.3.3/refcodes-logger-alt-io/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.3.3/refcodes-logger-alt-io/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.3.3/refcodes-logger-alt-simpledb/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.3.3/refcodes-logger-alt-simpledb/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.3.3/refcodes-logger-alt-slf4j-legacy/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.3.3/refcodes-logger-alt-slf4j/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.3.3/refcodes-logger-alt-spring/pom.xml) 

## Change list &lt;refcodes-logger-ext&gt; (version 3.3.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-3.3.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-3.3.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-3.3.3/refcodes-logger-ext-slf4j-legacy/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-3.3.3/refcodes-logger-ext-slf4j-legacy/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-3.3.3/refcodes-logger-ext-slf4j/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-3.3.3/refcodes-logger-ext-slf4j/README.md) 

## Change list &lt;refcodes-graphical-ext&gt; (version 3.3.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-3.3.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-3.3.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-3.3.3/refcodes-graphical-ext-javafx/pom.xml) 

## Change list &lt;refcodes-checkerboard&gt; (version 3.3.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-3.3.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-3.3.3/README.md) 

## Change list &lt;refcodes-checkerboard-alt&gt; (version 3.3.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-alt/src/refcodes-checkerboard-alt-3.3.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-alt/src/refcodes-checkerboard-alt-3.3.3/refcodes-checkerboard-alt-javafx/pom.xml) 

## Change list &lt;refcodes-net&gt; (version 3.3.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-3.3.3/pom.xml) 

## Change list &lt;refcodes-web&gt; (version 3.3.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-web/src/refcodes-web-3.3.3/pom.xml) 

## Change list &lt;refcodes-rest&gt; (version 3.3.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-3.3.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-3.3.3/README.md) 

## Change list &lt;refcodes-hal&gt; (version 3.3.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-3.3.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-3.3.3/README.md) 

## Change list &lt;refcodes-eventbus&gt; (version 3.3.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-eventbus/src/refcodes-eventbus-3.3.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-eventbus/src/refcodes-eventbus-3.3.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`module-info.java`](https://bitbucket.org/refcodes/refcodes-eventbus/src/refcodes-eventbus-3.3.3/src/main/java/module-info.java) (see Javadoc at [`module-info.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-eventbus/3.3.3/./module-info.html))

## Change list &lt;refcodes-eventbus-ext&gt; (version 3.3.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-3.3.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-3.3.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-3.3.3/refcodes-eventbus-ext-application/pom.xml) 

## Change list &lt;refcodes-decoupling&gt; (version 3.3.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-decoupling/src/refcodes-decoupling-3.3.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-decoupling/src/refcodes-decoupling-3.3.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`module-info.java`](https://bitbucket.org/refcodes/refcodes-decoupling/src/refcodes-decoupling-3.3.3/src/main/java/module-info.java) (see Javadoc at [`module-info.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-decoupling/3.3.3/./module-info.html))
* \[<span style="color:green">MODIFIED</span>\] [`Claim.java`](https://bitbucket.org/refcodes/refcodes-decoupling/src/refcodes-decoupling-3.3.3/src/main/java/org/refcodes/decoupling/Claim.java) (see Javadoc at [`Claim.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-decoupling/3.3.3/org.refcodes.decoupling/org/refcodes/decoupling/Claim.html))
* \[<span style="color:green">MODIFIED</span>\] [`Context.java`](https://bitbucket.org/refcodes/refcodes-decoupling/src/refcodes-decoupling-3.3.3/src/main/java/org/refcodes/decoupling/Context.java) (see Javadoc at [`Context.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-decoupling/3.3.3/org.refcodes.decoupling/org/refcodes/decoupling/Context.html))
* \[<span style="color:green">MODIFIED</span>\] [`Dependency.java`](https://bitbucket.org/refcodes/refcodes-decoupling/src/refcodes-decoupling-3.3.3/src/main/java/org/refcodes/decoupling/Dependency.java) (see Javadoc at [`Dependency.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-decoupling/3.3.3/org.refcodes.decoupling/org/refcodes/decoupling/Dependency.html))
* \[<span style="color:green">MODIFIED</span>\] [`DependencySchema.java`](https://bitbucket.org/refcodes/refcodes-decoupling/src/refcodes-decoupling-3.3.3/src/main/java/org/refcodes/decoupling/DependencySchema.java) (see Javadoc at [`DependencySchema.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-decoupling/3.3.3/org.refcodes.decoupling/org/refcodes/decoupling/DependencySchema.html))

## Change list &lt;refcodes-decoupling-ext&gt; (version 3.3.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-decoupling-ext/src/refcodes-decoupling-ext-3.3.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-decoupling-ext/src/refcodes-decoupling-ext-3.3.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-decoupling-ext/src/refcodes-decoupling-ext-3.3.3/refcodes-decoupling-ext-application/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-decoupling-ext/src/refcodes-decoupling-ext-3.3.3/refcodes-decoupling-ext-application/README.md) 

## Change list &lt;refcodes-filesystem&gt; (version 3.3.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-filesystem/src/refcodes-filesystem-3.3.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-filesystem/src/refcodes-filesystem-3.3.3/README.md) 

## Change list &lt;refcodes-filesystem-alt&gt; (version 3.3.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-filesystem-alt/src/refcodes-filesystem-alt-3.3.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-filesystem-alt/src/refcodes-filesystem-alt-3.3.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-filesystem-alt/src/refcodes-filesystem-alt-3.3.3/refcodes-filesystem-alt-s3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`module-info.java`](https://bitbucket.org/refcodes/refcodes-filesystem-alt/src/refcodes-filesystem-alt-3.3.3/refcodes-filesystem-alt-s3/src/main/java/module-info.java) (see Javadoc at [`module-info.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-filesystem-alt-s3/3.3.3//module-info.html))

## Change list &lt;refcodes-forwardsecrecy&gt; (version 3.3.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy/src/refcodes-forwardsecrecy-3.3.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy/src/refcodes-forwardsecrecy-3.3.3/README.md) 

## Change list &lt;refcodes-forwardsecrecy-alt&gt; (version 3.3.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-3.3.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-3.3.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-3.3.3/refcodes-forwardsecrecy-alt-filesystem/pom.xml) 

## Change list &lt;refcodes-io-ext&gt; (version 3.3.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-3.3.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-3.3.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-3.3.3/refcodes-io-ext-observer/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-3.3.3/refcodes-io-ext-observer/README.md) 

## Change list &lt;refcodes-jobbus&gt; (version 3.3.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-jobbus/src/refcodes-jobbus-3.3.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-jobbus/src/refcodes-jobbus-3.3.3/README.md) 

## Change list &lt;refcodes-rest-ext&gt; (version 3.3.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-3.3.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-3.3.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-3.3.3/refcodes-rest-ext-eureka/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-3.3.3/refcodes-rest-ext-eureka/README.md) 

## Change list &lt;refcodes-remoting&gt; (version 3.3.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-remoting/src/refcodes-remoting-3.3.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-remoting/src/refcodes-remoting-3.3.3/README.md) 

## Change list &lt;refcodes-remoting-ext&gt; (version 3.3.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-3.3.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-3.3.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-3.3.3/refcodes-remoting-ext-observer/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-3.3.3/refcodes-remoting-ext-observer/README.md) 

## Change list &lt;refcodes-serial&gt; (version 3.3.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial/src/refcodes-serial-3.3.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-serial/src/refcodes-serial-3.3.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`module-info.java`](https://bitbucket.org/refcodes/refcodes-serial/src/refcodes-serial-3.3.3/src/main/java/module-info.java) (see Javadoc at [`module-info.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-serial/3.3.3/./module-info.html))
* \[<span style="color:green">MODIFIED</span>\] [`Sequence.java`](https://bitbucket.org/refcodes/refcodes-serial/src/refcodes-serial-3.3.3/src/main/java/org/refcodes/serial/Sequence.java) (see Javadoc at [`Sequence.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-serial/3.3.3/org.refcodes.serial/org/refcodes/serial/Sequence.html))
* \[<span style="color:green">MODIFIED</span>\] [`SerialSchema.java`](https://bitbucket.org/refcodes/refcodes-serial/src/refcodes-serial-3.3.3/src/main/java/org/refcodes/serial/SerialSchema.java) (see Javadoc at [`SerialSchema.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-serial/3.3.3/org.refcodes.serial/org/refcodes/serial/SerialSchema.html))
* \[<span style="color:green">MODIFIED</span>\] [`Transmission.java`](https://bitbucket.org/refcodes/refcodes-serial/src/refcodes-serial-3.3.3/src/main/java/org/refcodes/serial/Transmission.java) (see Javadoc at [`Transmission.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-serial/3.3.3/org.refcodes.serial/org/refcodes/serial/Transmission.html))

## Change list &lt;refcodes-serial-alt&gt; (version 3.3.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial-alt/src/refcodes-serial-alt-3.3.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-serial-alt/src/refcodes-serial-alt-3.3.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial-alt/src/refcodes-serial-alt-3.3.3/refcodes-serial-alt-tty/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-serial-alt/src/refcodes-serial-alt-3.3.3/refcodes-serial-alt-tty/README.md) 

## Change list &lt;refcodes-serial-ext&gt; (version 3.3.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-3.3.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-3.3.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-3.3.3/refcodes-serial-ext-handshake/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-3.3.3/refcodes-serial-ext-handshake/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-3.3.3/refcodes-serial-ext-observer/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-3.3.3/refcodes-serial-ext-observer/README.md) 

## Change list &lt;refcodes-p2p&gt; (version 3.3.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-p2p/src/refcodes-p2p-3.3.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-p2p/src/refcodes-p2p-3.3.3/README.md) 

## Change list &lt;refcodes-p2p-alt&gt; (version 3.3.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-p2p-alt/src/refcodes-p2p-alt-3.3.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-p2p-alt/src/refcodes-p2p-alt-3.3.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-p2p-alt/src/refcodes-p2p-alt-3.3.3/refcodes-p2p-alt-serial/pom.xml) 

## Change list &lt;refcodes-p2p-ext&gt; (version 3.3.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-p2p-ext/src/refcodes-p2p-ext-3.3.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-p2p-ext/src/refcodes-p2p-ext-3.3.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-p2p-ext/src/refcodes-p2p-ext-3.3.3/refcodes-p2p-ext-observer/pom.xml) 

## Change list &lt;refcodes-tabular-alt&gt; (version 3.3.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-tabular-alt/src/refcodes-tabular-alt-3.3.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-tabular-alt/src/refcodes-tabular-alt-3.3.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-tabular-alt/src/refcodes-tabular-alt-3.3.3/refcodes-tabular-alt-forwardsecrecy/pom.xml) 

## Change list &lt;refcodes-archetype&gt; (version 3.3.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype/src/refcodes-archetype-3.3.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype/src/refcodes-archetype-3.3.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`CliHelper.java`](https://bitbucket.org/refcodes/refcodes-archetype/src/refcodes-archetype-3.3.3/src/main/java/org/refcodes/archetype/CliHelper.java) (see Javadoc at [`CliHelper.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-archetype/3.3.3/org.refcodes.archetype/org/refcodes/archetype/CliHelper.html))

## Change list &lt;refcodes-archetype-alt&gt; (version 3.3.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.3/refcodes-archetype-alt-c2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.3/refcodes-archetype-alt-c2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`build.sh`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.3/refcodes-archetype-alt-c2/src/main/resources/archetype-resources/build.sh) 
* \[<span style="color:green">MODIFIED</span>\] [`bundle.sh`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.3/refcodes-archetype-alt-c2/src/main/resources/archetype-resources/bundle.sh) 
* \[<span style="color:green">MODIFIED</span>\] [`installer.sh`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.3/refcodes-archetype-alt-c2/src/main/resources/archetype-resources/installer.sh) 
* \[<span style="color:green">MODIFIED</span>\] [`jexefy.sh`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.3/refcodes-archetype-alt-c2/src/main/resources/archetype-resources/jexefy.sh) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.3/refcodes-archetype-alt-c2/src/main/resources/archetype-resources/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`scriptify.sh`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.3/refcodes-archetype-alt-c2/src/main/resources/archetype-resources/scriptify.sh) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.3/refcodes-archetype-alt-cli/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.3/refcodes-archetype-alt-cli/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`build.sh`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.3/refcodes-archetype-alt-cli/src/main/resources/archetype-resources/build.sh) 
* \[<span style="color:green">MODIFIED</span>\] [`bundle.sh`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.3/refcodes-archetype-alt-cli/src/main/resources/archetype-resources/bundle.sh) 
* \[<span style="color:green">MODIFIED</span>\] [`installer.sh`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.3/refcodes-archetype-alt-cli/src/main/resources/archetype-resources/installer.sh) 
* \[<span style="color:green">MODIFIED</span>\] [`jexefy.sh`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.3/refcodes-archetype-alt-cli/src/main/resources/archetype-resources/jexefy.sh) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.3/refcodes-archetype-alt-cli/src/main/resources/archetype-resources/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`scriptify.sh`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.3/refcodes-archetype-alt-cli/src/main/resources/archetype-resources/scriptify.sh) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.3/refcodes-archetype-alt-csv/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.3/refcodes-archetype-alt-csv/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`build.sh`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.3/refcodes-archetype-alt-csv/src/main/resources/archetype-resources/build.sh) 
* \[<span style="color:green">MODIFIED</span>\] [`bundle.sh`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.3/refcodes-archetype-alt-csv/src/main/resources/archetype-resources/bundle.sh) 
* \[<span style="color:green">MODIFIED</span>\] [`installer.sh`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.3/refcodes-archetype-alt-csv/src/main/resources/archetype-resources/installer.sh) 
* \[<span style="color:green">MODIFIED</span>\] [`jexefy.sh`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.3/refcodes-archetype-alt-csv/src/main/resources/archetype-resources/jexefy.sh) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.3/refcodes-archetype-alt-csv/src/main/resources/archetype-resources/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`scriptify.sh`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.3/refcodes-archetype-alt-csv/src/main/resources/archetype-resources/scriptify.sh) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.3/refcodes-archetype-alt-decoupling/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.3/refcodes-archetype-alt-decoupling/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`build.sh`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.3/refcodes-archetype-alt-decoupling/src/main/resources/archetype-resources/build.sh) 
* \[<span style="color:green">MODIFIED</span>\] [`bundle.sh`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.3/refcodes-archetype-alt-decoupling/src/main/resources/archetype-resources/bundle.sh) 
* \[<span style="color:green">MODIFIED</span>\] [`installer.sh`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.3/refcodes-archetype-alt-decoupling/src/main/resources/archetype-resources/installer.sh) 
* \[<span style="color:green">MODIFIED</span>\] [`jexefy.sh`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.3/refcodes-archetype-alt-decoupling/src/main/resources/archetype-resources/jexefy.sh) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.3/refcodes-archetype-alt-decoupling/src/main/resources/archetype-resources/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`scriptify.sh`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.3/refcodes-archetype-alt-decoupling/src/main/resources/archetype-resources/scriptify.sh) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.3/refcodes-archetype-alt-eventbus/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.3/refcodes-archetype-alt-eventbus/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`build.sh`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.3/refcodes-archetype-alt-eventbus/src/main/resources/archetype-resources/build.sh) 
* \[<span style="color:green">MODIFIED</span>\] [`bundle.sh`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.3/refcodes-archetype-alt-eventbus/src/main/resources/archetype-resources/bundle.sh) 
* \[<span style="color:green">MODIFIED</span>\] [`installer.sh`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.3/refcodes-archetype-alt-eventbus/src/main/resources/archetype-resources/installer.sh) 
* \[<span style="color:green">MODIFIED</span>\] [`jexefy.sh`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.3/refcodes-archetype-alt-eventbus/src/main/resources/archetype-resources/jexefy.sh) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.3/refcodes-archetype-alt-eventbus/src/main/resources/archetype-resources/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`scriptify.sh`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.3/refcodes-archetype-alt-eventbus/src/main/resources/archetype-resources/scriptify.sh) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.3/refcodes-archetype-alt-filter/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.3/refcodes-archetype-alt-filter/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`build.sh`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.3/refcodes-archetype-alt-filter/src/main/resources/archetype-resources/build.sh) 
* \[<span style="color:green">MODIFIED</span>\] [`bundle.sh`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.3/refcodes-archetype-alt-filter/src/main/resources/archetype-resources/bundle.sh) 
* \[<span style="color:green">MODIFIED</span>\] [`installer.sh`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.3/refcodes-archetype-alt-filter/src/main/resources/archetype-resources/installer.sh) 
* \[<span style="color:green">MODIFIED</span>\] [`jexefy.sh`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.3/refcodes-archetype-alt-filter/src/main/resources/archetype-resources/jexefy.sh) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.3/refcodes-archetype-alt-filter/src/main/resources/archetype-resources/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`scriptify.sh`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.3/refcodes-archetype-alt-filter/src/main/resources/archetype-resources/scriptify.sh) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.3/refcodes-archetype-alt-rest/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.3/refcodes-archetype-alt-rest/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`build.sh`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.3/refcodes-archetype-alt-rest/src/main/resources/archetype-resources/build.sh) 
* \[<span style="color:green">MODIFIED</span>\] [`bundle.sh`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.3/refcodes-archetype-alt-rest/src/main/resources/archetype-resources/bundle.sh) 
* \[<span style="color:green">MODIFIED</span>\] [`installer.sh`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.3/refcodes-archetype-alt-rest/src/main/resources/archetype-resources/installer.sh) 
* \[<span style="color:green">MODIFIED</span>\] [`jexefy.sh`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.3/refcodes-archetype-alt-rest/src/main/resources/archetype-resources/jexefy.sh) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.3/refcodes-archetype-alt-rest/src/main/resources/archetype-resources/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`scriptify.sh`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.3/refcodes-archetype-alt-rest/src/main/resources/archetype-resources/scriptify.sh) 
