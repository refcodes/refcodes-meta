> This change list has been auto-generated on `triton` by `steiner` with `changelist-all.sh` on the 2021-08-29 at 21:57:06.

## Change list &lt;refcodes-licensing&gt; (version 2.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-licensing/src/refcodes-licensing-2.1.3/pom.xml) 

## Change list &lt;refcodes-parent&gt; (version 2.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-parent/src/refcodes-parent-2.1.3/pom.xml) 

## Change list &lt;refcodes-time&gt; (version 2.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-time/src/refcodes-time-2.1.3/pom.xml) 

## Change list &lt;refcodes-mixin&gt; (version 2.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-mixin/src/refcodes-mixin-2.1.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`EncodedAccessor.java`](https://bitbucket.org/refcodes/refcodes-mixin/src/refcodes-mixin-2.1.3/src/main/java/org/refcodes/mixin/EncodedAccessor.java) (see Javadoc at [`EncodedAccessor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-mixin/2.1.3/org.refcodes.mixin/org/refcodes/mixin/EncodedAccessor.html))

## Change list &lt;refcodes-data&gt; (version 2.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data/src/refcodes-data-2.1.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`AnsiEscapeCode.java`](https://bitbucket.org/refcodes/refcodes-data/src/refcodes-data-2.1.3/src/main/java/org/refcodes/data/AnsiEscapeCode.java) (see Javadoc at [`AnsiEscapeCode.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-data/2.1.3/org.refcodes.data/org/refcodes/data/AnsiEscapeCode.html))
* \[<span style="color:green">MODIFIED</span>\] [`SystemProperty.java`](https://bitbucket.org/refcodes/refcodes-data/src/refcodes-data-2.1.3/src/main/java/org/refcodes/data/SystemProperty.java) (see Javadoc at [`SystemProperty.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-data/2.1.3/org.refcodes.data/org/refcodes/data/SystemProperty.html))

## Change list &lt;refcodes-exception&gt; (version 2.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-exception/src/refcodes-exception-2.1.3/pom.xml) 

## Change list &lt;refcodes-factory&gt; (version 2.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-factory/src/refcodes-factory-2.1.3/pom.xml) 

## Change list &lt;refcodes-factory-alt&gt; (version 2.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-factory-alt/src/refcodes-factory-alt-2.1.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-factory-alt/src/refcodes-factory-alt-2.1.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-factory-alt/src/refcodes-factory-alt-2.1.3/refcodes-factory-alt-spring/pom.xml) 

## Change list &lt;refcodes-controlflow&gt; (version 2.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-controlflow/src/refcodes-controlflow-2.1.3/pom.xml) 

## Change list &lt;refcodes-numerical&gt; (version 2.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-numerical/src/refcodes-numerical-2.1.3/pom.xml) 

## Change list &lt;refcodes-generator&gt; (version 2.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-generator/src/refcodes-generator-2.1.3/pom.xml) 

## Change list &lt;refcodes-matcher&gt; (version 2.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-matcher/src/refcodes-matcher-2.1.3/pom.xml) 

## Change list &lt;refcodes-struct&gt; (version 2.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-struct/src/refcodes-struct-2.1.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-struct/src/refcodes-struct-2.1.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`TypeModeTest.java`](https://bitbucket.org/refcodes/refcodes-struct/src/refcodes-struct-2.1.3/src/test/java/org/refcodes/struct/TypeModeTest.java) 

## Change list &lt;refcodes-struct-ext&gt; (version 2.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-struct-ext/src/refcodes-struct-ext-2.1.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-struct-ext/src/refcodes-struct-ext-2.1.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-struct-ext/src/refcodes-struct-ext-2.1.3/refcodes-struct-ext-factory/pom.xml) 

## Change list &lt;refcodes-runtime&gt; (version 2.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-2.1.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-2.1.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`module-info.java`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-2.1.3/src/main/java/module-info.java) (see Javadoc at [`module-info.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-runtime/2.1.3/./module-info.html))
* \[<span style="color:green">MODIFIED</span>\] [`ConfigLocator.java`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-2.1.3/src/main/java/org/refcodes/runtime/ConfigLocator.java) (see Javadoc at [`ConfigLocator.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-runtime/2.1.3/org.refcodes.runtime/org/refcodes/runtime/ConfigLocator.html))
* \[<span style="color:green">MODIFIED</span>\] [`RuntimeUtility.java`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-2.1.3/src/main/java/org/refcodes/runtime/RuntimeUtility.java) (see Javadoc at [`RuntimeUtility.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-runtime/2.1.3/org.refcodes.runtime/org/refcodes/runtime/RuntimeUtility.html))
* \[<span style="color:green">MODIFIED</span>\] [`Terminal.java`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-2.1.3/src/main/java/org/refcodes/runtime/Terminal.java) (see Javadoc at [`Terminal.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-runtime/2.1.3/org.refcodes.runtime/org/refcodes/runtime/Terminal.html))

## Change list &lt;refcodes-component&gt; (version 2.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-2.1.3/pom.xml) 

## Change list &lt;refcodes-data-ext&gt; (version 2.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-2.1.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-2.1.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-2.1.3/refcodes-data-ext-boulderdash/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-2.1.3/refcodes-data-ext-checkers/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-2.1.3/refcodes-data-ext-checkers/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-2.1.3/refcodes-data-ext-chess/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-2.1.3/refcodes-data-ext-chess/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-2.1.3/refcodes-data-ext-corporate/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-2.1.3/refcodes-data-ext-corporate/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-2.1.3/refcodes-data-ext-symbols/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-2.1.3/refcodes-data-ext-symbols/README.md) 

## Change list &lt;refcodes-graphical&gt; (version 2.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-graphical/src/refcodes-graphical-2.1.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`RgbPixmapImpl.java`](https://bitbucket.org/refcodes/refcodes-graphical/src/refcodes-graphical-2.1.3/src/main/java/org/refcodes/graphical/RgbPixmapImpl.java) (see Javadoc at [`RgbPixmapImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-graphical/2.1.3/org.refcodes.graphical/org/refcodes/graphical/RgbPixmapImpl.html))

## Change list &lt;refcodes-textual&gt; (version 2.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-2.1.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`AsciiArtBuilder.java`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-2.1.3/src/main/java/org/refcodes/textual/AsciiArtBuilder.java) (see Javadoc at [`AsciiArtBuilder.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-textual/2.1.3/org.refcodes.textual/org/refcodes/textual/AsciiArtBuilder.html))
* \[<span style="color:green">MODIFIED</span>\] [`TableBuilder.java`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-2.1.3/src/main/java/org/refcodes/textual/TableBuilder.java) (see Javadoc at [`TableBuilder.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-textual/2.1.3/org.refcodes.textual/org/refcodes/textual/TableBuilder.html))
* \[<span style="color:green">MODIFIED</span>\] [`TextBlockBuilder.java`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-2.1.3/src/main/java/org/refcodes/textual/TextBlockBuilder.java) (see Javadoc at [`TextBlockBuilder.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-textual/2.1.3/org.refcodes.textual/org/refcodes/textual/TextBlockBuilder.html))
* \[<span style="color:green">MODIFIED</span>\] [`TextBorderBuilder.java`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-2.1.3/src/main/java/org/refcodes/textual/TextBorderBuilder.java) (see Javadoc at [`TextBorderBuilder.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-textual/2.1.3/org.refcodes.textual/org/refcodes/textual/TextBorderBuilder.html))
* \[<span style="color:green">MODIFIED</span>\] [`VertAlignTextBuilder.java`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-2.1.3/src/main/java/org/refcodes/textual/VertAlignTextBuilder.java) (see Javadoc at [`VertAlignTextBuilder.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-textual/2.1.3/org.refcodes.textual/org/refcodes/textual/VertAlignTextBuilder.html))

## Change list &lt;refcodes-criteria&gt; (version 2.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-2.1.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-2.1.3/README.md) 

## Change list &lt;refcodes-io&gt; (version 2.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-io/src/refcodes-io-2.1.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-io/src/refcodes-io-2.1.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`FileUtility.java`](https://bitbucket.org/refcodes/refcodes-io/src/refcodes-io-2.1.3/src/main/java/org/refcodes/io/FileUtility.java) (see Javadoc at [`FileUtility.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-io/2.1.3/org.refcodes.io/org/refcodes/io/FileUtility.html))
* \[<span style="color:green">MODIFIED</span>\] [`ZipFileStreamTest.java`](https://bitbucket.org/refcodes/refcodes-io/src/refcodes-io-2.1.3/src/test/java/org/refcodes/io/ZipFileStreamTest.java) 

## Change list &lt;refcodes-tabular&gt; (version 2.1.3)

* \[<span style="color:blue">ADDED</span>\] [`BooleanColumnFactory.java`](https://bitbucket.org/refcodes/refcodes-tabular/src/refcodes-tabular-2.1.3/src/main/java/org/refcodes/tabular/BooleanColumnFactory.java) (see Javadoc at [`BooleanColumnFactory.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-tabular/2.1.3/org.refcodes.tabular/org/refcodes/tabular/BooleanColumnFactory.html))
* \[<span style="color:blue">ADDED</span>\] [`BooleanColumn.java`](https://bitbucket.org/refcodes/refcodes-tabular/src/refcodes-tabular-2.1.3/src/main/java/org/refcodes/tabular/BooleanColumn.java) (see Javadoc at [`BooleanColumn.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-tabular/2.1.3/org.refcodes.tabular/org/refcodes/tabular/BooleanColumn.html))
* \[<span style="color:red">DELETED</span>\] `FormattedHeaderImpl.java`
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-tabular/src/refcodes-tabular-2.1.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-tabular/src/refcodes-tabular-2.1.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`EnumColumn.java`](https://bitbucket.org/refcodes/refcodes-tabular/src/refcodes-tabular-2.1.3/src/main/java/org/refcodes/tabular/EnumColumn.java) (see Javadoc at [`EnumColumn.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-tabular/2.1.3/org.refcodes.tabular/org/refcodes/tabular/EnumColumn.html))
* \[<span style="color:green">MODIFIED</span>\] [`FormattedHeader.java`](https://bitbucket.org/refcodes/refcodes-tabular/src/refcodes-tabular-2.1.3/src/main/java/org/refcodes/tabular/FormattedHeader.java) (see Javadoc at [`FormattedHeader.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-tabular/2.1.3/org.refcodes.tabular/org/refcodes/tabular/FormattedHeader.html))
* \[<span style="color:green">MODIFIED</span>\] [`TabularSugar.java`](https://bitbucket.org/refcodes/refcodes-tabular/src/refcodes-tabular-2.1.3/src/main/java/org/refcodes/tabular/TabularSugar.java) (see Javadoc at [`TabularSugar.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-tabular/2.1.3/org.refcodes.tabular/org/refcodes/tabular/TabularSugar.html))

## Change list &lt;refcodes-observer&gt; (version 2.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-observer/src/refcodes-observer-2.1.3/pom.xml) 

## Change list &lt;refcodes-command&gt; (version 2.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-command/src/refcodes-command-2.1.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-command/src/refcodes-command-2.1.3/README.md) 

## Change list &lt;refcodes-cli&gt; (version 2.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-2.1.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-2.1.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`AbstractCondition.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-2.1.3/src/main/java/org/refcodes/cli/AbstractCondition.java) (see Javadoc at [`AbstractCondition.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/2.1.3/org.refcodes.cli/org/refcodes/cli/AbstractCondition.html))
* \[<span style="color:green">MODIFIED</span>\] [`AbstractOperand.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-2.1.3/src/main/java/org/refcodes/cli/AbstractOperand.java) (see Javadoc at [`AbstractOperand.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/2.1.3/org.refcodes.cli/org/refcodes/cli/AbstractOperand.html))
* \[<span style="color:green">MODIFIED</span>\] [`AbstractOption.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-2.1.3/src/main/java/org/refcodes/cli/AbstractOption.java) (see Javadoc at [`AbstractOption.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/2.1.3/org.refcodes.cli/org/refcodes/cli/AbstractOption.html))
* \[<span style="color:green">MODIFIED</span>\] [`AbstractSyntaxable.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-2.1.3/src/main/java/org/refcodes/cli/AbstractSyntaxable.java) (see Javadoc at [`AbstractSyntaxable.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/2.1.3/org.refcodes.cli/org/refcodes/cli/AbstractSyntaxable.html))
* \[<span style="color:green">MODIFIED</span>\] [`AllCondition.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-2.1.3/src/main/java/org/refcodes/cli/AllCondition.java) (see Javadoc at [`AllCondition.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/2.1.3/org.refcodes.cli/org/refcodes/cli/AllCondition.html))
* \[<span style="color:green">MODIFIED</span>\] [`AndCondition.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-2.1.3/src/main/java/org/refcodes/cli/AndCondition.java) (see Javadoc at [`AndCondition.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/2.1.3/org.refcodes.cli/org/refcodes/cli/AndCondition.html))
* \[<span style="color:green">MODIFIED</span>\] [`AnyCondition.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-2.1.3/src/main/java/org/refcodes/cli/AnyCondition.java) (see Javadoc at [`AnyCondition.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/2.1.3/org.refcodes.cli/org/refcodes/cli/AnyCondition.html))
* \[<span style="color:green">MODIFIED</span>\] [`ArgsParserImpl.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-2.1.3/src/main/java/org/refcodes/cli/ArgsParserImpl.java) (see Javadoc at [`ArgsParserImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/2.1.3/org.refcodes.cli/org/refcodes/cli/ArgsParserImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`ArgsParser.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-2.1.3/src/main/java/org/refcodes/cli/ArgsParser.java) (see Javadoc at [`ArgsParser.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/2.1.3/org.refcodes.cli/org/refcodes/cli/ArgsParser.html))
* \[<span style="color:green">MODIFIED</span>\] [`ArgsSyntax.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-2.1.3/src/main/java/org/refcodes/cli/ArgsSyntax.java) (see Javadoc at [`ArgsSyntax.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/2.1.3/org.refcodes.cli/org/refcodes/cli/ArgsSyntax.html))
* \[<span style="color:green">MODIFIED</span>\] [`ArrayOperand.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-2.1.3/src/main/java/org/refcodes/cli/ArrayOperand.java) (see Javadoc at [`ArrayOperand.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/2.1.3/org.refcodes.cli/org/refcodes/cli/ArrayOperand.html))
* \[<span style="color:green">MODIFIED</span>\] [`ArrayOption.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-2.1.3/src/main/java/org/refcodes/cli/ArrayOption.java) (see Javadoc at [`ArrayOption.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/2.1.3/org.refcodes.cli/org/refcodes/cli/ArrayOption.html))
* \[<span style="color:green">MODIFIED</span>\] [`CliException.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-2.1.3/src/main/java/org/refcodes/cli/CliException.java) (see Javadoc at [`CliException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/2.1.3/org.refcodes.cli/org/refcodes/cli/CliException.html))
* \[<span style="color:green">MODIFIED</span>\] [`CliSugar.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-2.1.3/src/main/java/org/refcodes/cli/CliSugar.java) (see Javadoc at [`CliSugar.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/2.1.3/org.refcodes.cli/org/refcodes/cli/CliSugar.html))
* \[<span style="color:green">MODIFIED</span>\] [`CliUtility.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-2.1.3/src/main/java/org/refcodes/cli/CliUtility.java) (see Javadoc at [`CliUtility.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/2.1.3/org.refcodes.cli/org/refcodes/cli/CliUtility.html))
* \[<span style="color:green">MODIFIED</span>\] [`Condition.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-2.1.3/src/main/java/org/refcodes/cli/Condition.java) (see Javadoc at [`Condition.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/2.1.3/org.refcodes.cli/org/refcodes/cli/Condition.html))
* \[<span style="color:green">MODIFIED</span>\] [`ConfigOption.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-2.1.3/src/main/java/org/refcodes/cli/ConfigOption.java) (see Javadoc at [`ConfigOption.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/2.1.3/org.refcodes.cli/org/refcodes/cli/ConfigOption.html))
* \[<span style="color:green">MODIFIED</span>\] [`DaemonFlag.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-2.1.3/src/main/java/org/refcodes/cli/DaemonFlag.java) (see Javadoc at [`DaemonFlag.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/2.1.3/org.refcodes.cli/org/refcodes/cli/DaemonFlag.html))
* \[<span style="color:green">MODIFIED</span>\] [`DebugFlag.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-2.1.3/src/main/java/org/refcodes/cli/DebugFlag.java) (see Javadoc at [`DebugFlag.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/2.1.3/org.refcodes.cli/org/refcodes/cli/DebugFlag.html))
* \[<span style="color:green">MODIFIED</span>\] [`Flag.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-2.1.3/src/main/java/org/refcodes/cli/Flag.java) (see Javadoc at [`Flag.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/2.1.3/org.refcodes.cli/org/refcodes/cli/Flag.html))
* \[<span style="color:green">MODIFIED</span>\] [`ForceFlag.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-2.1.3/src/main/java/org/refcodes/cli/ForceFlag.java) (see Javadoc at [`ForceFlag.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/2.1.3/org.refcodes.cli/org/refcodes/cli/ForceFlag.html))
* \[<span style="color:green">MODIFIED</span>\] [`HelpFlag.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-2.1.3/src/main/java/org/refcodes/cli/HelpFlag.java) (see Javadoc at [`HelpFlag.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/2.1.3/org.refcodes.cli/org/refcodes/cli/HelpFlag.html))
* \[<span style="color:green">MODIFIED</span>\] [`InitFlag.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-2.1.3/src/main/java/org/refcodes/cli/InitFlag.java) (see Javadoc at [`InitFlag.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/2.1.3/org.refcodes.cli/org/refcodes/cli/InitFlag.html))
* \[<span style="color:green">MODIFIED</span>\] [`NoneOperand.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-2.1.3/src/main/java/org/refcodes/cli/NoneOperand.java) (see Javadoc at [`NoneOperand.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/2.1.3/org.refcodes.cli/org/refcodes/cli/NoneOperand.html))
* \[<span style="color:green">MODIFIED</span>\] [`Operand.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-2.1.3/src/main/java/org/refcodes/cli/Operand.java) (see Javadoc at [`Operand.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/2.1.3/org.refcodes.cli/org/refcodes/cli/Operand.html))
* \[<span style="color:green">MODIFIED</span>\] [`Operation.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-2.1.3/src/main/java/org/refcodes/cli/Operation.java) (see Javadoc at [`Operation.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/2.1.3/org.refcodes.cli/org/refcodes/cli/Operation.html))
* \[<span style="color:green">MODIFIED</span>\] [`OptionCondition.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-2.1.3/src/main/java/org/refcodes/cli/OptionCondition.java) (see Javadoc at [`OptionCondition.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/2.1.3/org.refcodes.cli/org/refcodes/cli/OptionCondition.html))
* \[<span style="color:green">MODIFIED</span>\] [`OrCondition.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-2.1.3/src/main/java/org/refcodes/cli/OrCondition.java) (see Javadoc at [`OrCondition.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/2.1.3/org.refcodes.cli/org/refcodes/cli/OrCondition.html))
* \[<span style="color:green">MODIFIED</span>\] [`package-info.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-2.1.3/src/main/java/org/refcodes/cli/package-info.java) 
* \[<span style="color:green">MODIFIED</span>\] [`QuietFlag.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-2.1.3/src/main/java/org/refcodes/cli/QuietFlag.java) (see Javadoc at [`QuietFlag.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/2.1.3/org.refcodes.cli/org/refcodes/cli/QuietFlag.html))
* \[<span style="color:green">MODIFIED</span>\] [`RootConditionAccessor.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-2.1.3/src/main/java/org/refcodes/cli/RootConditionAccessor.java) (see Javadoc at [`RootConditionAccessor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/2.1.3/org.refcodes.cli/org/refcodes/cli/RootConditionAccessor.html))
* \[<span style="color:green">MODIFIED</span>\] [`StringOperand.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-2.1.3/src/main/java/org/refcodes/cli/StringOperand.java) (see Javadoc at [`StringOperand.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/2.1.3/org.refcodes.cli/org/refcodes/cli/StringOperand.html))
* \[<span style="color:green">MODIFIED</span>\] [`Syntaxable.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-2.1.3/src/main/java/org/refcodes/cli/Syntaxable.java) (see Javadoc at [`Syntaxable.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/2.1.3/org.refcodes.cli/org/refcodes/cli/Syntaxable.html))
* \[<span style="color:green">MODIFIED</span>\] [`SyntaxNotation.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-2.1.3/src/main/java/org/refcodes/cli/SyntaxNotation.java) (see Javadoc at [`SyntaxNotation.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/2.1.3/org.refcodes.cli/org/refcodes/cli/SyntaxNotation.html))
* \[<span style="color:green">MODIFIED</span>\] [`SysInfoFlag.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-2.1.3/src/main/java/org/refcodes/cli/SysInfoFlag.java) (see Javadoc at [`SysInfoFlag.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/2.1.3/org.refcodes.cli/org/refcodes/cli/SysInfoFlag.html))
* \[<span style="color:green">MODIFIED</span>\] [`VerboseFlag.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-2.1.3/src/main/java/org/refcodes/cli/VerboseFlag.java) (see Javadoc at [`VerboseFlag.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/2.1.3/org.refcodes.cli/org/refcodes/cli/VerboseFlag.html))
* \[<span style="color:green">MODIFIED</span>\] [`XorCondition.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-2.1.3/src/main/java/org/refcodes/cli/XorCondition.java) (see Javadoc at [`XorCondition.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/2.1.3/org.refcodes.cli/org/refcodes/cli/XorCondition.html))
* \[<span style="color:green">MODIFIED</span>\] [`ArgsParserTest.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-2.1.3/src/test/java/org/refcodes/cli/ArgsParserTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`StackOverflowTest.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-2.1.3/src/test/java/org/refcodes/cli/StackOverflowTest.java) 

## Change list &lt;refcodes-audio&gt; (version 2.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-audio/src/refcodes-audio-2.1.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-audio/src/refcodes-audio-2.1.3/README.md) 

## Change list &lt;refcodes-codec&gt; (version 2.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-2.1.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-2.1.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`BaseEncoderDecodeStreamTest.java`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-2.1.3/src/test/java/org/refcodes/codec/BaseEncoderDecodeStreamTest.java) 

## Change list &lt;refcodes-component-ext&gt; (version 2.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-2.1.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-2.1.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-2.1.3/refcodes-component-ext-observer/pom.xml) 

## Change list &lt;refcodes-properties&gt; (version 2.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-2.1.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-2.1.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`package-info.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-2.1.3/src/main/java/org/refcodes/properties/package-info.java) 

## Change list &lt;refcodes-security&gt; (version 2.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security/src/refcodes-security-2.1.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security/src/refcodes-security-2.1.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`EncryptionOutputStream.java`](https://bitbucket.org/refcodes/refcodes-security/src/refcodes-security-2.1.3/src/main/java/org/refcodes/security/EncryptionOutputStream.java) (see Javadoc at [`EncryptionOutputStream.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-security/2.1.3/org.refcodes.security/org/refcodes/security/EncryptionOutputStream.html))
* \[<span style="color:green">MODIFIED</span>\] [`KeyStoreDescriptorAccessor.java`](https://bitbucket.org/refcodes/refcodes-security/src/refcodes-security-2.1.3/src/main/java/org/refcodes/security/KeyStoreDescriptorAccessor.java) (see Javadoc at [`KeyStoreDescriptorAccessor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-security/2.1.3/org.refcodes.security/org/refcodes/security/KeyStoreDescriptorAccessor.html))
* \[<span style="color:green">MODIFIED</span>\] [`TrustStoreDescriptorAccessor.java`](https://bitbucket.org/refcodes/refcodes-security/src/refcodes-security-2.1.3/src/main/java/org/refcodes/security/TrustStoreDescriptorAccessor.java) (see Javadoc at [`TrustStoreDescriptorAccessor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-security/2.1.3/org.refcodes.security/org/refcodes/security/TrustStoreDescriptorAccessor.html))

## Change list &lt;refcodes-security-alt&gt; (version 2.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-2.1.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-2.1.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`analysis.csv`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-2.1.3/refcodes-security-alt-chaos/analysis.csv) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-2.1.3/refcodes-security-alt-chaos/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-2.1.3/refcodes-security-alt-chaos/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`S-Ranges.txt`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-2.1.3/refcodes-security-alt-chaos/S-Ranges.txt) 
* \[<span style="color:green">MODIFIED</span>\] [`ChaosDecrypter.java`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-2.1.3/refcodes-security-alt-chaos/src/main/java/org/refcodes/security/alt/chaos/ChaosDecrypter.java) (see Javadoc at [`ChaosDecrypter.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-security-alt-chaos/2.1.3/org.refcodes.security.alt.chaos/org/refcodes/security/alt/chaos/ChaosDecrypter.html))
* \[<span style="color:green">MODIFIED</span>\] [`ChaosDecryptionInputStream.java`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-2.1.3/refcodes-security-alt-chaos/src/main/java/org/refcodes/security/alt/chaos/ChaosDecryptionInputStream.java) (see Javadoc at [`ChaosDecryptionInputStream.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-security-alt-chaos/2.1.3/org.refcodes.security.alt.chaos/org/refcodes/security/alt/chaos/ChaosDecryptionInputStream.html))
* \[<span style="color:green">MODIFIED</span>\] [`ChaosEncrypter.java`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-2.1.3/refcodes-security-alt-chaos/src/main/java/org/refcodes/security/alt/chaos/ChaosEncrypter.java) (see Javadoc at [`ChaosEncrypter.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-security-alt-chaos/2.1.3/org.refcodes.security.alt.chaos/org/refcodes/security/alt/chaos/ChaosEncrypter.html))
* \[<span style="color:green">MODIFIED</span>\] [`ChaosEncryptionOutputStream.java`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-2.1.3/refcodes-security-alt-chaos/src/main/java/org/refcodes/security/alt/chaos/ChaosEncryptionOutputStream.java) (see Javadoc at [`ChaosEncryptionOutputStream.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-security-alt-chaos/2.1.3/org.refcodes.security.alt.chaos/org/refcodes/security/alt/chaos/ChaosEncryptionOutputStream.html))
* \[<span style="color:green">MODIFIED</span>\] [`ChaosKey.java`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-2.1.3/refcodes-security-alt-chaos/src/main/java/org/refcodes/security/alt/chaos/ChaosKey.java) (see Javadoc at [`ChaosKey.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-security-alt-chaos/2.1.3/org.refcodes.security.alt.chaos/org/refcodes/security/alt/chaos/ChaosKey.html))
* \[<span style="color:green">MODIFIED</span>\] [`ChaosMode.java`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-2.1.3/refcodes-security-alt-chaos/src/main/java/org/refcodes/security/alt/chaos/ChaosMode.java) (see Javadoc at [`ChaosMode.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-security-alt-chaos/2.1.3/org.refcodes.security.alt.chaos/org/refcodes/security/alt/chaos/ChaosMode.html))
* \[<span style="color:green">MODIFIED</span>\] [`ChaosOptionsBuilder.java`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-2.1.3/refcodes-security-alt-chaos/src/main/java/org/refcodes/security/alt/chaos/ChaosOptionsBuilder.java) (see Javadoc at [`ChaosOptionsBuilder.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-security-alt-chaos/2.1.3/org.refcodes.security.alt.chaos/org/refcodes/security/alt/chaos/ChaosOptionsBuilder.html))
* \[<span style="color:green">MODIFIED</span>\] [`ChaosOptions.java`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-2.1.3/refcodes-security-alt-chaos/src/main/java/org/refcodes/security/alt/chaos/ChaosOptions.java) (see Javadoc at [`ChaosOptions.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-security-alt-chaos/2.1.3/org.refcodes.security.alt.chaos/org/refcodes/security/alt/chaos/ChaosOptions.html))
* \[<span style="color:green">MODIFIED</span>\] [`ChaosTextDecrypter.java`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-2.1.3/refcodes-security-alt-chaos/src/main/java/org/refcodes/security/alt/chaos/ChaosTextDecrypter.java) (see Javadoc at [`ChaosTextDecrypter.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-security-alt-chaos/2.1.3/org.refcodes.security.alt.chaos/org/refcodes/security/alt/chaos/ChaosTextDecrypter.html))
* \[<span style="color:green">MODIFIED</span>\] [`ChaosTextEncrypter.java`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-2.1.3/refcodes-security-alt-chaos/src/main/java/org/refcodes/security/alt/chaos/ChaosTextEncrypter.java) (see Javadoc at [`ChaosTextEncrypter.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-security-alt-chaos/2.1.3/org.refcodes.security.alt.chaos/org/refcodes/security/alt/chaos/ChaosTextEncrypter.html))
* \[<span style="color:green">MODIFIED</span>\] [`AbstractStreamTest.java`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-2.1.3/refcodes-security-alt-chaos/src/test/java/org/refcodes/security/alt/chaos/AbstractStreamTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`ChaosEncryptionDecryptionStreamTest.java`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-2.1.3/refcodes-security-alt-chaos/src/test/java/org/refcodes/security/alt/chaos/ChaosEncryptionDecryptionStreamTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`ChaosKeyTest.java`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-2.1.3/refcodes-security-alt-chaos/src/test/java/org/refcodes/security/alt/chaos/ChaosKeyTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`ChaosTest.java`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-2.1.3/refcodes-security-alt-chaos/src/test/java/org/refcodes/security/alt/chaos/ChaosTest.java) 

## Change list &lt;refcodes-security-ext&gt; (version 2.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-2.1.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-2.1.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-2.1.3/refcodes-security-ext-chaos/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-2.1.3/refcodes-security-ext-chaos/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-2.1.3/refcodes-security-ext-spring/pom.xml) 

## Change list &lt;refcodes-properties-ext&gt; (version 2.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-2.1.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-2.1.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-2.1.3/refcodes-properties-ext-cli/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-2.1.3/refcodes-properties-ext-cli/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`ArgsParserPropertiesTest.java`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-2.1.3/refcodes-properties-ext-cli/src/test/java/org/refcodes/properties/ext/cli/ArgsParserPropertiesTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-2.1.3/refcodes-properties-ext-obfuscation/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-2.1.3/refcodes-properties-ext-obfuscation/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-2.1.3/refcodes-properties-ext-observer/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-2.1.3/refcodes-properties-ext-observer/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`AbstractObservableResourcePropertiesTest.java`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-2.1.3/refcodes-properties-ext-observer/src/test/java/org/refcodes/properties/ext/observer/AbstractObservableResourcePropertiesTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-2.1.3/refcodes-properties-ext-runtime/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-2.1.3/refcodes-properties-ext-runtime/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`RuntimePropertiesImpl.java`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-2.1.3/refcodes-properties-ext-runtime/src/main/java/org/refcodes/properties/ext/runtime/RuntimePropertiesImpl.java) (see Javadoc at [`RuntimePropertiesImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties-ext-runtime/2.1.3/org.refcodes.properties.ext.runtime/org/refcodes/properties/ext/runtime/RuntimePropertiesImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`RuntimeProperties.java`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-2.1.3/refcodes-properties-ext-runtime/src/main/java/org/refcodes/properties/ext/runtime/RuntimeProperties.java) (see Javadoc at [`RuntimeProperties.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties-ext-runtime/2.1.3/org.refcodes.properties.ext.runtime/org/refcodes/properties/ext/runtime/RuntimeProperties.html))
* \[<span style="color:green">MODIFIED</span>\] [`RuntimePropertiesSugarTest.java`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-2.1.3/refcodes-properties-ext-runtime/src/test/java/org/refcodes/properties/ext/runtime/RuntimePropertiesSugarTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`RuntimePropertiesTest.java`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-2.1.3/refcodes-properties-ext-runtime/src/test/java/org/refcodes/properties/ext/runtime/RuntimePropertiesTest.java) 

## Change list &lt;refcodes-logger&gt; (version 2.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger/src/refcodes-logger-2.1.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger/src/refcodes-logger-2.1.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`ColumnLayout.java`](https://bitbucket.org/refcodes/refcodes-logger/src/refcodes-logger-2.1.3/src/main/java/org/refcodes/logger/ColumnLayout.java) (see Javadoc at [`ColumnLayout.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-logger/2.1.3/org.refcodes.logger/org/refcodes/logger/ColumnLayout.html))
* \[<span style="color:green">MODIFIED</span>\] [`package-info.java`](https://bitbucket.org/refcodes/refcodes-logger/src/refcodes-logger-2.1.3/src/main/java/org/refcodes/logger/package-info.java) 
* \[<span style="color:green">MODIFIED</span>\] [`RuntimeLogger.java`](https://bitbucket.org/refcodes/refcodes-logger/src/refcodes-logger-2.1.3/src/main/java/org/refcodes/logger/RuntimeLogger.java) (see Javadoc at [`RuntimeLogger.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-logger/2.1.3/org.refcodes.logger/org/refcodes/logger/RuntimeLogger.html))

## Change list &lt;refcodes-logger-alt&gt; (version 2.1.3)

* \[<span style="color:red">DELETED</span>\] `ConsoleLoggerImpl.java`
* \[<span style="color:red">DELETED</span>\] `FormattedLogger.java`
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-2.1.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-2.1.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-2.1.3/refcodes-logger-alt-async/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-2.1.3/refcodes-logger-alt-async/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-2.1.3/refcodes-logger-alt-console/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-2.1.3/refcodes-logger-alt-console/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`ConsoleLogger.java`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-2.1.3/refcodes-logger-alt-console/src/main/java/org/refcodes/logger/alt/console/ConsoleLogger.java) (see Javadoc at [`ConsoleLogger.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-logger-alt-console/2.1.3/org.refcodes.logger.alt.console/org/refcodes/logger/alt/console/ConsoleLogger.html))
* \[<span style="color:green">MODIFIED</span>\] [`ConsoleLoggerSingleton.java`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-2.1.3/refcodes-logger-alt-console/src/main/java/org/refcodes/logger/alt/console/ConsoleLoggerSingleton.java) (see Javadoc at [`ConsoleLoggerSingleton.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-logger-alt-console/2.1.3/org.refcodes.logger.alt.console/org/refcodes/logger/alt/console/ConsoleLoggerSingleton.html))
* \[<span style="color:green">MODIFIED</span>\] [`package-info.java`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-2.1.3/refcodes-logger-alt-console/src/main/java/org/refcodes/logger/alt/console/package-info.java) 
* \[<span style="color:green">MODIFIED</span>\] [`ConsoleRuntimeLoggerTest.java`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-2.1.3/refcodes-logger-alt-console/src/test/java/org/refcodes/logger/alt/console/ConsoleRuntimeLoggerTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-2.1.3/refcodes-logger-alt-io/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-2.1.3/refcodes-logger-alt-io/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-2.1.3/refcodes-logger-alt-simpledb/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-2.1.3/refcodes-logger-alt-slf4j/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-2.1.3/refcodes-logger-alt-spring/pom.xml) 

## Change list &lt;refcodes-logger-ext&gt; (version 2.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-2.1.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-2.1.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-2.1.3/refcodes-logger-ext-slf4j/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-2.1.3/refcodes-logger-ext-slf4j/README.md) 

## Change list &lt;refcodes-graphical-ext&gt; (version 2.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-2.1.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-2.1.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-2.1.3/refcodes-graphical-ext-javafx/pom.xml) 

## Change list &lt;refcodes-checkerboard&gt; (version 2.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-2.1.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-2.1.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`ConsoleCheckerboardViewerImpl.java`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-2.1.3/src/main/java/org/refcodes/checkerboard/ConsoleCheckerboardViewerImpl.java) (see Javadoc at [`ConsoleCheckerboardViewerImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-checkerboard/2.1.3/org.refcodes.checkerboard/org/refcodes/checkerboard/ConsoleCheckerboardViewerImpl.html))

## Change list &lt;refcodes-checkerboard-alt&gt; (version 2.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-alt/src/refcodes-checkerboard-alt-2.1.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-alt/src/refcodes-checkerboard-alt-2.1.3/refcodes-checkerboard-alt-javafx/pom.xml) 

## Change list &lt;refcodes-checkerboard-ext&gt; (version 2.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-ext/src/refcodes-checkerboard-ext-2.1.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-checkerboard-ext/src/refcodes-checkerboard-ext-2.1.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-ext/src/refcodes-checkerboard-ext-2.1.3/refcodes-checkerboard-ext-javafx-boulderdash/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`BoulderDashDemo.java`](https://bitbucket.org/refcodes/refcodes-checkerboard-ext/src/refcodes-checkerboard-ext-2.1.3/refcodes-checkerboard-ext-javafx-boulderdash/src/test/java/org/refcodes/checkerboard/ext/javafx/boulderdash/BoulderDashDemo.java) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-ext/src/refcodes-checkerboard-ext-2.1.3/refcodes-checkerboard-ext-javafx-chess/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`ChessDemo.java`](https://bitbucket.org/refcodes/refcodes-checkerboard-ext/src/refcodes-checkerboard-ext-2.1.3/refcodes-checkerboard-ext-javafx-chess/src/test/java/org/refcodes/checkerboard/ext/javafx/chess/ChessDemo.java) 

## Change list &lt;refcodes-boulderdash&gt; (version 2.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-boulderdash/src/refcodes-boulderdash-2.1.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-boulderdash/src/refcodes-boulderdash-2.1.3/README.md) 

## Change list &lt;refcodes-web&gt; (version 2.1.3)

* \[<span style="color:blue">ADDED</span>\] [`HttpClientContextBuilder.java`](https://bitbucket.org/refcodes/refcodes-web/src/refcodes-web-2.1.3/src/main/java/org/refcodes/web/HttpClientContextBuilder.java) (see Javadoc at [`HttpClientContextBuilder.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-web/2.1.3/org.refcodes.web/org/refcodes/web/HttpClientContextBuilder.html))
* \[<span style="color:red">DELETED</span>\] `HttpClientContextBuilderImpl.java`
* \[<span style="color:red">DELETED</span>\] `PortManagerImpl.java`
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-web/src/refcodes-web-2.1.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`HttpClientContext.java`](https://bitbucket.org/refcodes/refcodes-web/src/refcodes-web-2.1.3/src/main/java/org/refcodes/web/HttpClientContext.java) (see Javadoc at [`HttpClientContext.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-web/2.1.3/org.refcodes.web/org/refcodes/web/HttpClientContext.html))
* \[<span style="color:green">MODIFIED</span>\] [`HttpClientRequestImpl.java`](https://bitbucket.org/refcodes/refcodes-web/src/refcodes-web-2.1.3/src/main/java/org/refcodes/web/HttpClientRequestImpl.java) (see Javadoc at [`HttpClientRequestImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-web/2.1.3/org.refcodes.web/org/refcodes/web/HttpClientRequestImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`HttpClientRequest.java`](https://bitbucket.org/refcodes/refcodes-web/src/refcodes-web-2.1.3/src/main/java/org/refcodes/web/HttpClientRequest.java) (see Javadoc at [`HttpClientRequest.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-web/2.1.3/org.refcodes.web/org/refcodes/web/HttpClientRequest.html))
* \[<span style="color:green">MODIFIED</span>\] [`HttpClientResponseImpl.java`](https://bitbucket.org/refcodes/refcodes-web/src/refcodes-web-2.1.3/src/main/java/org/refcodes/web/HttpClientResponseImpl.java) (see Javadoc at [`HttpClientResponseImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-web/2.1.3/org.refcodes.web/org/refcodes/web/HttpClientResponseImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`HttpRequest.java`](https://bitbucket.org/refcodes/refcodes-web/src/refcodes-web-2.1.3/src/main/java/org/refcodes/web/HttpRequest.java) (see Javadoc at [`HttpRequest.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-web/2.1.3/org.refcodes.web/org/refcodes/web/HttpRequest.html))
* \[<span style="color:green">MODIFIED</span>\] [`HttpServerContext.java`](https://bitbucket.org/refcodes/refcodes-web/src/refcodes-web-2.1.3/src/main/java/org/refcodes/web/HttpServerContext.java) (see Javadoc at [`HttpServerContext.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-web/2.1.3/org.refcodes.web/org/refcodes/web/HttpServerContext.html))
* \[<span style="color:green">MODIFIED</span>\] [`HttpServerRequestImpl.java`](https://bitbucket.org/refcodes/refcodes-web/src/refcodes-web-2.1.3/src/main/java/org/refcodes/web/HttpServerRequestImpl.java) (see Javadoc at [`HttpServerRequestImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-web/2.1.3/org.refcodes.web/org/refcodes/web/HttpServerRequestImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`HttpServerResponseImpl.java`](https://bitbucket.org/refcodes/refcodes-web/src/refcodes-web-2.1.3/src/main/java/org/refcodes/web/HttpServerResponseImpl.java) (see Javadoc at [`HttpServerResponseImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-web/2.1.3/org.refcodes.web/org/refcodes/web/HttpServerResponseImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`PortManager.java`](https://bitbucket.org/refcodes/refcodes-web/src/refcodes-web-2.1.3/src/main/java/org/refcodes/web/PortManager.java) (see Javadoc at [`PortManager.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-web/2.1.3/org.refcodes.web/org/refcodes/web/PortManager.html))
* \[<span style="color:green">MODIFIED</span>\] [`PortManagerSingleton.java`](https://bitbucket.org/refcodes/refcodes-web/src/refcodes-web-2.1.3/src/main/java/org/refcodes/web/PortManagerSingleton.java) (see Javadoc at [`PortManagerSingleton.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-web/2.1.3/org.refcodes.web/org/refcodes/web/PortManagerSingleton.html))

## Change list &lt;refcodes-rest&gt; (version 2.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-2.1.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-2.1.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`AbstractHttpDiscoverySidecar.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-2.1.3/src/main/java/org/refcodes/rest/AbstractHttpDiscoverySidecar.java) (see Javadoc at [`AbstractHttpDiscoverySidecar.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/2.1.3/org.refcodes.rest/org/refcodes/rest/AbstractHttpDiscoverySidecar.html))
* \[<span style="color:green">MODIFIED</span>\] [`AbstractHttpRegistryContextBuilder.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-2.1.3/src/main/java/org/refcodes/rest/AbstractHttpRegistryContextBuilder.java) (see Javadoc at [`AbstractHttpRegistryContextBuilder.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/2.1.3/org.refcodes.rest/org/refcodes/rest/AbstractHttpRegistryContextBuilder.html))
* \[<span style="color:green">MODIFIED</span>\] [`AbstractHttpRegistryRestServerDecorator.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-2.1.3/src/main/java/org/refcodes/rest/AbstractHttpRegistryRestServerDecorator.java) (see Javadoc at [`AbstractHttpRegistryRestServerDecorator.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/2.1.3/org.refcodes.rest/org/refcodes/rest/AbstractHttpRegistryRestServerDecorator.html))
* \[<span style="color:green">MODIFIED</span>\] [`AbstractHttpRegistrySidecar.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-2.1.3/src/main/java/org/refcodes/rest/AbstractHttpRegistrySidecar.java) (see Javadoc at [`AbstractHttpRegistrySidecar.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/2.1.3/org.refcodes.rest/org/refcodes/rest/AbstractHttpRegistrySidecar.html))
* \[<span style="color:green">MODIFIED</span>\] [`AbstractHttpRestClientDecorator.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-2.1.3/src/main/java/org/refcodes/rest/AbstractHttpRestClientDecorator.java) (see Javadoc at [`AbstractHttpRestClientDecorator.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/2.1.3/org.refcodes.rest/org/refcodes/rest/AbstractHttpRestClientDecorator.html))
* \[<span style="color:green">MODIFIED</span>\] [`AbstractHttpRestServerDecorator.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-2.1.3/src/main/java/org/refcodes/rest/AbstractHttpRestServerDecorator.java) (see Javadoc at [`AbstractHttpRestServerDecorator.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/2.1.3/org.refcodes.rest/org/refcodes/rest/AbstractHttpRestServerDecorator.html))
* \[<span style="color:green">MODIFIED</span>\] [`HttpDiscoveryContextBuilderImpl.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-2.1.3/src/main/java/org/refcodes/rest/HttpDiscoveryContextBuilderImpl.java) (see Javadoc at [`HttpDiscoveryContextBuilderImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/2.1.3/org.refcodes.rest/org/refcodes/rest/HttpDiscoveryContextBuilderImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`HttpRestClientImpl.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-2.1.3/src/main/java/org/refcodes/rest/HttpRestClientImpl.java) (see Javadoc at [`HttpRestClientImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/2.1.3/org.refcodes.rest/org/refcodes/rest/HttpRestClientImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`HttpRestServerImpl.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-2.1.3/src/main/java/org/refcodes/rest/HttpRestServerImpl.java) (see Javadoc at [`HttpRestServerImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/2.1.3/org.refcodes.rest/org/refcodes/rest/HttpRestServerImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`HttpRestServer.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-2.1.3/src/main/java/org/refcodes/rest/HttpRestServer.java) (see Javadoc at [`HttpRestServer.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/2.1.3/org.refcodes.rest/org/refcodes/rest/HttpRestServer.html))
* \[<span style="color:green">MODIFIED</span>\] [`RestRequestClient.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-2.1.3/src/main/java/org/refcodes/rest/RestRequestClient.java) (see Javadoc at [`RestRequestClient.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/2.1.3/org.refcodes.rest/org/refcodes/rest/RestRequestClient.html))

## Change list &lt;refcodes-hal&gt; (version 2.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-2.1.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-2.1.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`HalClientTest.java`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-2.1.3/src/test/java/org/refcodes/hal/HalClientTest.java) 

## Change list &lt;refcodes-eventbus&gt; (version 2.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-eventbus/src/refcodes-eventbus-2.1.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-eventbus/src/refcodes-eventbus-2.1.3/README.md) 

## Change list &lt;refcodes-eventbus-ext&gt; (version 2.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-2.1.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-2.1.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-2.1.3/refcodes-eventbus-ext-application/pom.xml) 

## Change list &lt;refcodes-filesystem&gt; (version 2.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-filesystem/src/refcodes-filesystem-2.1.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-filesystem/src/refcodes-filesystem-2.1.3/README.md) 

## Change list &lt;refcodes-filesystem-alt&gt; (version 2.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-filesystem-alt/src/refcodes-filesystem-alt-2.1.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-filesystem-alt/src/refcodes-filesystem-alt-2.1.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-filesystem-alt/src/refcodes-filesystem-alt-2.1.3/refcodes-filesystem-alt-s3/pom.xml) 

## Change list &lt;refcodes-forwardsecrecy&gt; (version 2.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy/src/refcodes-forwardsecrecy-2.1.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy/src/refcodes-forwardsecrecy-2.1.3/README.md) 

## Change list &lt;refcodes-forwardsecrecy-alt&gt; (version 2.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-2.1.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-2.1.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-2.1.3/refcodes-forwardsecrecy-alt-filesystem/pom.xml) 

## Change list &lt;refcodes-io-ext&gt; (version 2.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-2.1.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-2.1.3/refcodes-io-ext-observer/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-2.1.3/refcodes-io-ext-observer/README.md) 

## Change list &lt;refcodes-interceptor&gt; (version 2.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-interceptor/src/refcodes-interceptor-2.1.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-interceptor/src/refcodes-interceptor-2.1.3/README.md) 

## Change list &lt;refcodes-jobbus&gt; (version 2.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-jobbus/src/refcodes-jobbus-2.1.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-jobbus/src/refcodes-jobbus-2.1.3/README.md) 

## Change list &lt;refcodes-rest-ext&gt; (version 2.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-2.1.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-2.1.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-2.1.3/refcodes-rest-ext-eureka/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-2.1.3/refcodes-rest-ext-eureka/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`EurekaDiscoverySidecar.java`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-2.1.3/refcodes-rest-ext-eureka/src/main/java/org/refcodes/rest/ext/eureka/EurekaDiscoverySidecar.java) (see Javadoc at [`EurekaDiscoverySidecar.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest-ext-eureka/2.1.3/org.refcodes.rest.ext.eureka/org/refcodes/rest/ext/eureka/EurekaDiscoverySidecar.html))
* \[<span style="color:green">MODIFIED</span>\] [`EurekaRegistryContext.java`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-2.1.3/refcodes-rest-ext-eureka/src/main/java/org/refcodes/rest/ext/eureka/EurekaRegistryContext.java) (see Javadoc at [`EurekaRegistryContext.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest-ext-eureka/2.1.3/org.refcodes.rest.ext.eureka/org/refcodes/rest/ext/eureka/EurekaRegistryContext.html))
* \[<span style="color:green">MODIFIED</span>\] [`EurekaRegistrySidecar.java`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-2.1.3/refcodes-rest-ext-eureka/src/main/java/org/refcodes/rest/ext/eureka/EurekaRegistrySidecar.java) (see Javadoc at [`EurekaRegistrySidecar.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest-ext-eureka/2.1.3/org.refcodes.rest.ext.eureka/org/refcodes/rest/ext/eureka/EurekaRegistrySidecar.html))

## Change list &lt;refcodes-remoting&gt; (version 2.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-remoting/src/refcodes-remoting-2.1.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-remoting/src/refcodes-remoting-2.1.3/README.md) 

## Change list &lt;refcodes-remoting-ext&gt; (version 2.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-2.1.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-2.1.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-2.1.3/refcodes-remoting-ext-observer/pom.xml) 

## Change list &lt;refcodes-serial&gt; (version 2.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial/src/refcodes-serial-2.1.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-serial/src/refcodes-serial-2.1.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`AbstractPortDecorator.java`](https://bitbucket.org/refcodes/refcodes-serial/src/refcodes-serial-2.1.3/src/main/java/org/refcodes/serial/AbstractPortDecorator.java) (see Javadoc at [`AbstractPortDecorator.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-serial/2.1.3/org.refcodes.serial/org/refcodes/serial/AbstractPortDecorator.html))
* \[<span style="color:green">MODIFIED</span>\] [`FullDuplexPacketPortDecorator.java`](https://bitbucket.org/refcodes/refcodes-serial/src/refcodes-serial-2.1.3/src/main/java/org/refcodes/serial/FullDuplexPacketPortDecorator.java) (see Javadoc at [`FullDuplexPacketPortDecorator.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-serial/2.1.3/org.refcodes.serial/org/refcodes/serial/FullDuplexPacketPortDecorator.html))
* \[<span style="color:green">MODIFIED</span>\] [`FullDuplexTransmissionPortDecorator.java`](https://bitbucket.org/refcodes/refcodes-serial/src/refcodes-serial-2.1.3/src/main/java/org/refcodes/serial/FullDuplexTransmissionPortDecorator.java) (see Javadoc at [`FullDuplexTransmissionPortDecorator.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-serial/2.1.3/org.refcodes.serial/org/refcodes/serial/FullDuplexTransmissionPortDecorator.html))
* \[<span style="color:green">MODIFIED</span>\] [`LoopbackPort.java`](https://bitbucket.org/refcodes/refcodes-serial/src/refcodes-serial-2.1.3/src/main/java/org/refcodes/serial/LoopbackPort.java) (see Javadoc at [`LoopbackPort.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-serial/2.1.3/org.refcodes.serial/org/refcodes/serial/LoopbackPort.html))
* \[<span style="color:green">MODIFIED</span>\] [`Port.java`](https://bitbucket.org/refcodes/refcodes-serial/src/refcodes-serial-2.1.3/src/main/java/org/refcodes/serial/Port.java) (see Javadoc at [`Port.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-serial/2.1.3/org.refcodes.serial/org/refcodes/serial/Port.html))
* \[<span style="color:green">MODIFIED</span>\] [`ReadyToSendSegmentDecorator.java`](https://bitbucket.org/refcodes/refcodes-serial/src/refcodes-serial-2.1.3/src/main/java/org/refcodes/serial/ReadyToSendSegmentDecorator.java) (see Javadoc at [`ReadyToSendSegmentDecorator.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-serial/2.1.3/org.refcodes.serial/org/refcodes/serial/ReadyToSendSegmentDecorator.html))
* \[<span style="color:green">MODIFIED</span>\] [`FileTest.java`](https://bitbucket.org/refcodes/refcodes-serial/src/refcodes-serial-2.1.3/src/test/java/org/refcodes/serial/FileTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`FullDuplexPacketPortTest.java`](https://bitbucket.org/refcodes/refcodes-serial/src/refcodes-serial-2.1.3/src/test/java/org/refcodes/serial/FullDuplexPacketPortTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`FullDuplexTransmissionPortTest.java`](https://bitbucket.org/refcodes/refcodes-serial/src/refcodes-serial-2.1.3/src/test/java/org/refcodes/serial/FullDuplexTransmissionPortTest.java) 

## Change list &lt;refcodes-serial-alt&gt; (version 2.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial-alt/src/refcodes-serial-alt-2.1.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-serial-alt/src/refcodes-serial-alt-2.1.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial-alt/src/refcodes-serial-alt-2.1.3/refcodes-serial-alt-tty/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-serial-alt/src/refcodes-serial-alt-2.1.3/refcodes-serial-alt-tty/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`TtyPort.java`](https://bitbucket.org/refcodes/refcodes-serial-alt/src/refcodes-serial-alt-2.1.3/refcodes-serial-alt-tty/src/main/java/org/refcodes/serial/alt/tty/TtyPort.java) (see Javadoc at [`TtyPort.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-serial-alt-tty/2.1.3/org.refcodes.serial.alt.tty/org/refcodes/serial/alt/tty/TtyPort.html))
* \[<span style="color:green">MODIFIED</span>\] [`AbstractTtyPortTest.java`](https://bitbucket.org/refcodes/refcodes-serial-alt/src/refcodes-serial-alt-2.1.3/refcodes-serial-alt-tty/src/test/java/org/refcodes/serial/alt/tty/AbstractTtyPortTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`TtyFileTest.java`](https://bitbucket.org/refcodes/refcodes-serial-alt/src/refcodes-serial-alt-2.1.3/refcodes-serial-alt-tty/src/test/java/org/refcodes/serial/alt/tty/TtyFileTest.java) 

## Change list &lt;refcodes-serial-ext&gt; (version 2.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-2.1.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-2.1.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-2.1.3/refcodes-serial-ext-observer/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-2.1.3/refcodes-serial-ext-observer/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-2.1.3/refcodes-serial-ext-security/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-2.1.3/refcodes-serial-ext-security/README.md) 

## Change list &lt;refcodes-servicebus&gt; (version 2.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-servicebus/src/refcodes-servicebus-2.1.3/pom.xml) 

## Change list &lt;refcodes-servicebus-alt&gt; (version 2.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-servicebus-alt/src/refcodes-servicebus-alt-2.1.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-servicebus-alt/src/refcodes-servicebus-alt-2.1.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-servicebus-alt/src/refcodes-servicebus-alt-2.1.3/refcodes-servicebus-alt-spring/pom.xml) 

## Change list &lt;refcodes-tabular-alt&gt; (version 2.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-tabular-alt/src/refcodes-tabular-alt-2.1.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-tabular-alt/src/refcodes-tabular-alt-2.1.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-tabular-alt/src/refcodes-tabular-alt-2.1.3/refcodes-tabular-alt-forwardsecrecy/pom.xml) 

## Change list &lt;refcodes-archetype&gt; (version 2.1.3)

* \[<span style="color:blue">ADDED</span>\] [`C2Helper.java`](https://bitbucket.org/refcodes/refcodes-archetype/src/refcodes-archetype-2.1.3/src/main/java/org/refcodes/archetype/C2Helper.java) (see Javadoc at [`C2Helper.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-archetype/2.1.3/org.refcodes.archetype/org/refcodes/archetype/C2Helper.html))
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype/src/refcodes-archetype-2.1.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype/src/refcodes-archetype-2.1.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`module-info.java`](https://bitbucket.org/refcodes/refcodes-archetype/src/refcodes-archetype-2.1.3/src/main/java/module-info.java) (see Javadoc at [`module-info.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-archetype/2.1.3/./module-info.html))
* \[<span style="color:green">MODIFIED</span>\] [`CliHelper.java`](https://bitbucket.org/refcodes/refcodes-archetype/src/refcodes-archetype-2.1.3/src/main/java/org/refcodes/archetype/CliHelper.java) (see Javadoc at [`CliHelper.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-archetype/2.1.3/org.refcodes.archetype/org/refcodes/archetype/CliHelper.html))

## Change list &lt;refcodes-archetype-alt&gt; (version 2.1.3)

* \[<span style="color:blue">ADDED</span>\] [`gitignore`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-2.1.3/refcodes-archetype-alt-c2/gitignore) 
* \[<span style="color:blue">ADDED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-2.1.3/refcodes-archetype-alt-c2/pom.xml) 
* \[<span style="color:blue">ADDED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-2.1.3/refcodes-archetype-alt-c2/README.md) 
* \[<span style="color:blue">ADDED</span>\] [`build.sh`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-2.1.3/refcodes-archetype-alt-c2/src/main/resources/archetype-resources/build.sh) 
* \[<span style="color:blue">ADDED</span>\] [`bundle.conf`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-2.1.3/refcodes-archetype-alt-c2/src/main/resources/archetype-resources/bundle.conf) 
* \[<span style="color:blue">ADDED</span>\] [`bundle-launcher.cmd`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-2.1.3/refcodes-archetype-alt-c2/src/main/resources/archetype-resources/bundle-launcher.cmd) 
* \[<span style="color:blue">ADDED</span>\] [`bundle-launcher.sh`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-2.1.3/refcodes-archetype-alt-c2/src/main/resources/archetype-resources/bundle-launcher.sh) 
* \[<span style="color:blue">ADDED</span>\] [`bundle.sh`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-2.1.3/refcodes-archetype-alt-c2/src/main/resources/archetype-resources/bundle.sh) 
* \[<span style="color:blue">ADDED</span>\] [`gitignore`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-2.1.3/refcodes-archetype-alt-c2/src/main/resources/archetype-resources/gitignore) 
* \[<span style="color:blue">ADDED</span>\] [`installer.sh`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-2.1.3/refcodes-archetype-alt-c2/src/main/resources/archetype-resources/installer.sh) 
* \[<span style="color:blue">ADDED</span>\] [`jexefy-launcher-x86_64.elf`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-2.1.3/refcodes-archetype-alt-c2/src/main/resources/archetype-resources/jexefy-launcher-x86_64.elf) 
* \[<span style="color:blue">ADDED</span>\] [`jexefy-launcher-x86_64.exe`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-2.1.3/refcodes-archetype-alt-c2/src/main/resources/archetype-resources/jexefy-launcher-x86_64.exe) 
* \[<span style="color:blue">ADDED</span>\] [`jexefy.sh`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-2.1.3/refcodes-archetype-alt-c2/src/main/resources/archetype-resources/jexefy.sh) 
* \[<span style="color:blue">ADDED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-2.1.3/refcodes-archetype-alt-c2/src/main/resources/archetype-resources/pom.xml) 
* \[<span style="color:blue">ADDED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-2.1.3/refcodes-archetype-alt-c2/src/main/resources/archetype-resources/README.md) 
* \[<span style="color:blue">ADDED</span>\] [`scriptify-launcher.sh`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-2.1.3/refcodes-archetype-alt-c2/src/main/resources/archetype-resources/scriptify-launcher.sh) 
* \[<span style="color:blue">ADDED</span>\] [`scriptify.sh`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-2.1.3/refcodes-archetype-alt-c2/src/main/resources/archetype-resources/scriptify.sh) 
* \[<span style="color:blue">ADDED</span>\] [`Main.java`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-2.1.3/refcodes-archetype-alt-c2/src/main/resources/archetype-resources/src/main/java/Main.java) (see Javadoc at [`Main.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-archetype-alt-c2/src/main/resources/archetype-resources/2.1.3/src.main.resources.archetype-resources/Main.html))
* \[<span style="color:blue">ADDED</span>\] [`application.ico`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-2.1.3/refcodes-archetype-alt-c2/src/main/resources/archetype-resources/src/main/resources/application.ico) 
* \[<span style="color:blue">ADDED</span>\] [`__artifactId__.ini`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-2.1.3/refcodes-archetype-alt-c2/src/main/resources/archetype-resources/src/main/resources/__artifactId__.ini) 
* \[<span style="color:blue">ADDED</span>\] [`log4j2.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-2.1.3/refcodes-archetype-alt-c2/src/main/resources/archetype-resources/src/main/resources/log4j2.xml) 
* \[<span style="color:blue">ADDED</span>\] [`jni-config.json`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-2.1.3/refcodes-archetype-alt-c2/src/main/resources/archetype-resources/src/main/resources/META-INF/__groupId__/__artifactId__/jni-config.json) 
* \[<span style="color:blue">ADDED</span>\] [`native-image.properties`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-2.1.3/refcodes-archetype-alt-c2/src/main/resources/archetype-resources/src/main/resources/META-INF/__groupId__/__artifactId__/native-image.properties) 
* \[<span style="color:blue">ADDED</span>\] [`proxy-config.json`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-2.1.3/refcodes-archetype-alt-c2/src/main/resources/archetype-resources/src/main/resources/META-INF/__groupId__/__artifactId__/proxy-config.json) 
* \[<span style="color:blue">ADDED</span>\] [`reflect-config.json`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-2.1.3/refcodes-archetype-alt-c2/src/main/resources/archetype-resources/src/main/resources/META-INF/__groupId__/__artifactId__/reflect-config.json) 
* \[<span style="color:blue">ADDED</span>\] [`resource-config.json`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-2.1.3/refcodes-archetype-alt-c2/src/main/resources/archetype-resources/src/main/resources/META-INF/__groupId__/__artifactId__/resource-config.json) 
* \[<span style="color:blue">ADDED</span>\] [`serialization-config.json`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-2.1.3/refcodes-archetype-alt-c2/src/main/resources/archetype-resources/src/main/resources/META-INF/__groupId__/__artifactId__/serialization-config.json) 
* \[<span style="color:blue">ADDED</span>\] [`runtimelogger.ini`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-2.1.3/refcodes-archetype-alt-c2/src/main/resources/archetype-resources/src/main/resources/runtimelogger.ini) 
* \[<span style="color:blue">ADDED</span>\] [`archetype-post-generate.groovy`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-2.1.3/refcodes-archetype-alt-c2/src/main/resources/META-INF/archetype-post-generate.groovy) 
* \[<span style="color:blue">ADDED</span>\] [`archetype-metadata.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-2.1.3/refcodes-archetype-alt-c2/src/main/resources/META-INF/maven/archetype-metadata.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-2.1.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-2.1.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-2.1.3/refcodes-archetype-alt-cli/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-2.1.3/refcodes-archetype-alt-cli/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-2.1.3/refcodes-archetype-alt-cli/src/main/resources/archetype-resources/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`Main.java`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-2.1.3/refcodes-archetype-alt-cli/src/main/resources/archetype-resources/src/main/java/Main.java) (see Javadoc at [`Main.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-archetype-alt-cli/src/main/resources/archetype-resources/2.1.3/src.main.resources.archetype-resources/Main.html))
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-2.1.3/refcodes-archetype-alt-csv/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-2.1.3/refcodes-archetype-alt-csv/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-2.1.3/refcodes-archetype-alt-csv/src/main/resources/archetype-resources/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`Main.java`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-2.1.3/refcodes-archetype-alt-csv/src/main/resources/archetype-resources/src/main/java/Main.java) (see Javadoc at [`Main.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-archetype-alt-csv/src/main/resources/archetype-resources/2.1.3/src.main.resources.archetype-resources/Main.html))
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-2.1.3/refcodes-archetype-alt-eventbus/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-2.1.3/refcodes-archetype-alt-eventbus/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-2.1.3/refcodes-archetype-alt-eventbus/src/main/resources/archetype-resources/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`Main.java`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-2.1.3/refcodes-archetype-alt-eventbus/src/main/resources/archetype-resources/src/main/java/Main.java) (see Javadoc at [`Main.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-archetype-alt-eventbus/src/main/resources/archetype-resources/2.1.3/src.main.resources.archetype-resources/Main.html))
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-2.1.3/refcodes-archetype-alt-rest/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-2.1.3/refcodes-archetype-alt-rest/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-2.1.3/refcodes-archetype-alt-rest/src/main/resources/archetype-resources/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-2.1.3/refcodes-archetype-alt-rest/src/main/resources/archetype-resources/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`Main.java`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-2.1.3/refcodes-archetype-alt-rest/src/main/resources/archetype-resources/src/main/java/Main.java) (see Javadoc at [`Main.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-archetype-alt-rest/src/main/resources/archetype-resources/2.1.3/src.main.resources.archetype-resources/Main.html))
