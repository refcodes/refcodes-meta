> This change list has been auto-generated on `triton` by `steiner` with `changelist-all.sh` on the 2022-12-01 at 13:08:12.

## Change list &lt;refcodes-factory-alt&gt; (version 3.0.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-factory-alt/src/refcodes-factory-alt-3.0.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-factory-alt/src/refcodes-factory-alt-3.0.8/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-factory-alt/src/refcodes-factory-alt-3.0.8/refcodes-factory-alt-spring/pom.xml) 

## Change list &lt;refcodes-controlflow&gt; (version 3.0.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-controlflow/src/refcodes-controlflow-3.0.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-controlflow/src/refcodes-controlflow-3.0.8/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`TweakableThreadFactory.java`](https://bitbucket.org/refcodes/refcodes-controlflow/src/refcodes-controlflow-3.0.8/src/main/java/org/refcodes/controlflow/TweakableThreadFactory.java) (see Javadoc at [`TweakableThreadFactory.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-controlflow/3.0.8/org.refcodes.controlflow/org/refcodes/controlflow/TweakableThreadFactory.html))

## Change list &lt;refcodes-numerical&gt; (version 3.0.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-numerical/src/refcodes-numerical-3.0.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-numerical/src/refcodes-numerical-3.0.8/README.md) 

## Change list &lt;refcodes-generator&gt; (version 3.0.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-generator/src/refcodes-generator-3.0.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-generator/src/refcodes-generator-3.0.8/README.md) 

## Change list &lt;refcodes-matcher&gt; (version 3.0.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-matcher/src/refcodes-matcher-3.0.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-matcher/src/refcodes-matcher-3.0.8/README.md) 

## Change list &lt;refcodes-struct&gt; (version 3.0.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-struct/src/refcodes-struct-3.0.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-struct/src/refcodes-struct-3.0.8/README.md) 

## Change list &lt;refcodes-struct-ext&gt; (version 3.0.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-struct-ext/src/refcodes-struct-ext-3.0.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-struct-ext/src/refcodes-struct-ext-3.0.8/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-struct-ext/src/refcodes-struct-ext-3.0.8/refcodes-struct-ext-factory/pom.xml) 

## Change list &lt;refcodes-runtime&gt; (version 3.0.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-3.0.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-3.0.8/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`RuntimeUtility.java`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-3.0.8/src/main/java/org/refcodes/runtime/RuntimeUtility.java) (see Javadoc at [`RuntimeUtility.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-runtime/3.0.8/org.refcodes.runtime/org/refcodes/runtime/RuntimeUtility.html))

## Change list &lt;refcodes-component&gt; (version 3.0.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.0.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.0.8/README.md) 

## Change list &lt;refcodes-data-ext&gt; (version 3.0.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-3.0.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-3.0.8/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-3.0.8/refcodes-data-ext-checkers/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-3.0.8/refcodes-data-ext-checkers/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-3.0.8/refcodes-data-ext-chess/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-3.0.8/refcodes-data-ext-chess/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-3.0.8/refcodes-data-ext-corporate/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-3.0.8/refcodes-data-ext-corporate/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-3.0.8/refcodes-data-ext-symbols/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-3.0.8/refcodes-data-ext-symbols/README.md) 

## Change list &lt;refcodes-graphical&gt; (version 3.0.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-graphical/src/refcodes-graphical-3.0.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-graphical/src/refcodes-graphical-3.0.8/README.md) 

## Change list &lt;refcodes-textual&gt; (version 3.0.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-3.0.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-3.0.8/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`TableBuilder.java`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-3.0.8/src/main/java/org/refcodes/textual/TableBuilder.java) (see Javadoc at [`TableBuilder.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-textual/3.0.8/org.refcodes.textual/org/refcodes/textual/TableBuilder.html))
* \[<span style="color:green">MODIFIED</span>\] [`TablePrinter.java`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-3.0.8/src/main/java/org/refcodes/textual/TablePrinter.java) (see Javadoc at [`TablePrinter.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-textual/3.0.8/org.refcodes.textual/org/refcodes/textual/TablePrinter.html))
* \[<span style="color:green">MODIFIED</span>\] [`TableStyle.java`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-3.0.8/src/main/java/org/refcodes/textual/TableStyle.java) (see Javadoc at [`TableStyle.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-textual/3.0.8/org.refcodes.textual/org/refcodes/textual/TableStyle.html))
* \[<span style="color:green">MODIFIED</span>\] [`TextBoxStyle.java`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-3.0.8/src/main/java/org/refcodes/textual/TextBoxStyle.java) (see Javadoc at [`TextBoxStyle.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-textual/3.0.8/org.refcodes.textual/org/refcodes/textual/TextBoxStyle.html))

## Change list &lt;refcodes-criteria&gt; (version 3.0.8)

* \[<span style="color:green">MODIFIED</span>\] [`.gitignore`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-3.0.8/.gitignore) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-3.0.8/pom.xml) 

## Change list &lt;refcodes-io&gt; (version 3.0.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-io/src/refcodes-io-3.0.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-io/src/refcodes-io-3.0.8/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`TimeoutInputStream.java`](https://bitbucket.org/refcodes/refcodes-io/src/refcodes-io-3.0.8/src/main/java/org/refcodes/io/TimeoutInputStream.java) (see Javadoc at [`TimeoutInputStream.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-io/3.0.8/org.refcodes.io/org/refcodes/io/TimeoutInputStream.html))

## Change list &lt;refcodes-tabular&gt; (version 3.0.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-tabular/src/refcodes-tabular-3.0.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-tabular/src/refcodes-tabular-3.0.8/README.md) 

## Change list &lt;refcodes-observer&gt; (version 3.0.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-observer/src/refcodes-observer-3.0.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-observer/src/refcodes-observer-3.0.8/README.md) 

## Change list &lt;refcodes-command&gt; (version 3.0.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-command/src/refcodes-command-3.0.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-command/src/refcodes-command-3.0.8/README.md) 

## Change list &lt;refcodes-cli&gt; (version 3.0.8)

* \[<span style="color:blue">ADDED</span>\] [`ArgumentEscapeCodeAccessor.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.0.8/src/main/java/org/refcodes/cli/ArgumentEscapeCodeAccessor.java) (see Javadoc at [`ArgumentEscapeCodeAccessor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/3.0.8/org.refcodes.cli/org/refcodes/cli/ArgumentEscapeCodeAccessor.html))
* \[<span style="color:blue">ADDED</span>\] [`ArgumentPrefixAccessor.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.0.8/src/main/java/org/refcodes/cli/ArgumentPrefixAccessor.java) (see Javadoc at [`ArgumentPrefixAccessor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/3.0.8/org.refcodes.cli/org/refcodes/cli/ArgumentPrefixAccessor.html))
* \[<span style="color:blue">ADDED</span>\] [`ArgumentSuffixAccessor.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.0.8/src/main/java/org/refcodes/cli/ArgumentSuffixAccessor.java) (see Javadoc at [`ArgumentSuffixAccessor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/3.0.8/org.refcodes.cli/org/refcodes/cli/ArgumentSuffixAccessor.html))
* \[<span style="color:blue">ADDED</span>\] [`CliContextAccessor.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.0.8/src/main/java/org/refcodes/cli/CliContextAccessor.java) (see Javadoc at [`CliContextAccessor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/3.0.8/org.refcodes.cli/org/refcodes/cli/CliContextAccessor.html))
* \[<span style="color:blue">ADDED</span>\] [`CliContext.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.0.8/src/main/java/org/refcodes/cli/CliContext.java) (see Javadoc at [`CliContext.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/3.0.8/org.refcodes.cli/org/refcodes/cli/CliContext.html))
* \[<span style="color:blue">ADDED</span>\] [`CliMetrics.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.0.8/src/main/java/org/refcodes/cli/CliMetrics.java) (see Javadoc at [`CliMetrics.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/3.0.8/org.refcodes.cli/org/refcodes/cli/CliMetrics.html))
* \[<span style="color:blue">ADDED</span>\] [`Term.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.0.8/src/main/java/org/refcodes/cli/Term.java) (see Javadoc at [`Term.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/3.0.8/org.refcodes.cli/org/refcodes/cli/Term.html))
* \[<span style="color:blue">ADDED</span>\] [`LongOptionPrefixAccessor.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.0.8/src/main/java/org/refcodes/cli/LongOptionPrefixAccessor.java) (see Javadoc at [`LongOptionPrefixAccessor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/3.0.8/org.refcodes.cli/org/refcodes/cli/LongOptionPrefixAccessor.html))
* \[<span style="color:blue">ADDED</span>\] [`Optionable.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.0.8/src/main/java/org/refcodes/cli/Optionable.java) (see Javadoc at [`Optionable.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/3.0.8/org.refcodes.cli/org/refcodes/cli/Optionable.html))
* \[<span style="color:blue">ADDED</span>\] [`OptionEscapeCodeAccessor.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.0.8/src/main/java/org/refcodes/cli/OptionEscapeCodeAccessor.java) (see Javadoc at [`OptionEscapeCodeAccessor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/3.0.8/org.refcodes.cli/org/refcodes/cli/OptionEscapeCodeAccessor.html))
* \[<span style="color:blue">ADDED</span>\] [`refcodes-cli.di`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.0.8/src/main/java/org/refcodes/cli/refcodes-cli.di) 
* \[<span style="color:blue">ADDED</span>\] [`refcodes-cli.notation`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.0.8/src/main/java/org/refcodes/cli/refcodes-cli.notation) 
* \[<span style="color:blue">ADDED</span>\] [`refcodes-cli.uml`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.0.8/src/main/java/org/refcodes/cli/refcodes-cli.uml) 
* \[<span style="color:blue">ADDED</span>\] [`ShortOptionPrefixAccessor.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.0.8/src/main/java/org/refcodes/cli/ShortOptionPrefixAccessor.java) (see Javadoc at [`ShortOptionPrefixAccessor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/3.0.8/org.refcodes.cli/org/refcodes/cli/ShortOptionPrefixAccessor.html))
* \[<span style="color:blue">ADDED</span>\] [`SyntaxMetricsAccessor.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.0.8/src/main/java/org/refcodes/cli/SyntaxMetricsAccessor.java) (see Javadoc at [`SyntaxMetricsAccessor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/3.0.8/org.refcodes.cli/org/refcodes/cli/SyntaxMetricsAccessor.html))
* \[<span style="color:blue">ADDED</span>\] [`SyntaxMetricsImpl.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.0.8/src/main/java/org/refcodes/cli/SyntaxMetricsImpl.java) (see Javadoc at [`SyntaxMetricsImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/3.0.8/org.refcodes.cli/org/refcodes/cli/SyntaxMetricsImpl.html))
* \[<span style="color:blue">ADDED</span>\] [`SyntaxMetrics.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.0.8/src/main/java/org/refcodes/cli/SyntaxMetrics.java) (see Javadoc at [`SyntaxMetrics.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/3.0.8/org.refcodes.cli/org/refcodes/cli/SyntaxMetrics.html))
* \[<span style="color:blue">ADDED</span>\] [`SyntaxNotationTest.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.0.8/src/test/java/org/refcodes/cli/SyntaxNotationTest.java) 
* \[<span style="color:red">DELETED</span>\] `ArgsSyntax.java`
* \[<span style="color:red">DELETED</span>\] `OptionCondition.java`
* \[<span style="color:red">DELETED</span>\] `Syntaxable.java`
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.0.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.0.8/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`AbstractCondition.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.0.8/src/main/java/org/refcodes/cli/AbstractCondition.java) (see Javadoc at [`AbstractCondition.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/3.0.8/org.refcodes.cli/org/refcodes/cli/AbstractCondition.html))
* \[<span style="color:green">MODIFIED</span>\] [`AbstractOperand.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.0.8/src/main/java/org/refcodes/cli/AbstractOperand.java) (see Javadoc at [`AbstractOperand.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/3.0.8/org.refcodes.cli/org/refcodes/cli/AbstractOperand.html))
* \[<span style="color:green">MODIFIED</span>\] [`AbstractOption.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.0.8/src/main/java/org/refcodes/cli/AbstractOption.java) (see Javadoc at [`AbstractOption.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/3.0.8/org.refcodes.cli/org/refcodes/cli/AbstractOption.html))
* \[<span style="color:green">MODIFIED</span>\] [`AllCondition.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.0.8/src/main/java/org/refcodes/cli/AllCondition.java) (see Javadoc at [`AllCondition.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/3.0.8/org.refcodes.cli/org/refcodes/cli/AllCondition.html))
* \[<span style="color:green">MODIFIED</span>\] [`AndCondition.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.0.8/src/main/java/org/refcodes/cli/AndCondition.java) (see Javadoc at [`AndCondition.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/3.0.8/org.refcodes.cli/org/refcodes/cli/AndCondition.html))
* \[<span style="color:green">MODIFIED</span>\] [`AnyCondition.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.0.8/src/main/java/org/refcodes/cli/AnyCondition.java) (see Javadoc at [`AnyCondition.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/3.0.8/org.refcodes.cli/org/refcodes/cli/AnyCondition.html))
* \[<span style="color:green">MODIFIED</span>\] [`ArgsAccessor.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.0.8/src/main/java/org/refcodes/cli/ArgsAccessor.java) (see Javadoc at [`ArgsAccessor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/3.0.8/org.refcodes.cli/org/refcodes/cli/ArgsAccessor.html))
* \[<span style="color:green">MODIFIED</span>\] [`ArgsFilter.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.0.8/src/main/java/org/refcodes/cli/ArgsFilter.java) (see Javadoc at [`ArgsFilter.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/3.0.8/org.refcodes.cli/org/refcodes/cli/ArgsFilter.html))
* \[<span style="color:green">MODIFIED</span>\] [`ArgsParserAccessor.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.0.8/src/main/java/org/refcodes/cli/ArgsParserAccessor.java) (see Javadoc at [`ArgsParserAccessor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/3.0.8/org.refcodes.cli/org/refcodes/cli/ArgsParserAccessor.html))
* \[<span style="color:green">MODIFIED</span>\] [`ArgsParserImpl.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.0.8/src/main/java/org/refcodes/cli/ArgsParserImpl.java) (see Javadoc at [`ArgsParserImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/3.0.8/org.refcodes.cli/org/refcodes/cli/ArgsParserImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`ArgsParser.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.0.8/src/main/java/org/refcodes/cli/ArgsParser.java) (see Javadoc at [`ArgsParser.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/3.0.8/org.refcodes.cli/org/refcodes/cli/ArgsParser.html))
* \[<span style="color:green">MODIFIED</span>\] [`ArgsParserMixin.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.0.8/src/main/java/org/refcodes/cli/ArgsParserMixin.java) (see Javadoc at [`ArgsParserMixin.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/3.0.8/org.refcodes.cli/org/refcodes/cli/ArgsParserMixin.html))
* \[<span style="color:green">MODIFIED</span>\] [`ArrayOperand.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.0.8/src/main/java/org/refcodes/cli/ArrayOperand.java) (see Javadoc at [`ArrayOperand.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/3.0.8/org.refcodes.cli/org/refcodes/cli/ArrayOperand.html))
* \[<span style="color:green">MODIFIED</span>\] [`ArrayOption.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.0.8/src/main/java/org/refcodes/cli/ArrayOption.java) (see Javadoc at [`ArrayOption.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/3.0.8/org.refcodes.cli/org/refcodes/cli/ArrayOption.html))
* \[<span style="color:green">MODIFIED</span>\] [`CasesCondition.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.0.8/src/main/java/org/refcodes/cli/CasesCondition.java) (see Javadoc at [`CasesCondition.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/3.0.8/org.refcodes.cli/org/refcodes/cli/CasesCondition.html))
* \[<span style="color:green">MODIFIED</span>\] [`CharOption.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.0.8/src/main/java/org/refcodes/cli/CharOption.java) (see Javadoc at [`CharOption.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/3.0.8/org.refcodes.cli/org/refcodes/cli/CharOption.html))
* \[<span style="color:green">MODIFIED</span>\] [`CleanFlag.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.0.8/src/main/java/org/refcodes/cli/CleanFlag.java) (see Javadoc at [`CleanFlag.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/3.0.8/org.refcodes.cli/org/refcodes/cli/CleanFlag.html))
* \[<span style="color:green">MODIFIED</span>\] [`CliSugar.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.0.8/src/main/java/org/refcodes/cli/CliSugar.java) (see Javadoc at [`CliSugar.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/3.0.8/org.refcodes.cli/org/refcodes/cli/CliSugar.html))
* \[<span style="color:green">MODIFIED</span>\] [`CliUtility.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.0.8/src/main/java/org/refcodes/cli/CliUtility.java) (see Javadoc at [`CliUtility.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/3.0.8/org.refcodes.cli/org/refcodes/cli/CliUtility.html))
* \[<span style="color:green">MODIFIED</span>\] [`Condition.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.0.8/src/main/java/org/refcodes/cli/Condition.java) (see Javadoc at [`Condition.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/3.0.8/org.refcodes.cli/org/refcodes/cli/Condition.html))
* \[<span style="color:green">MODIFIED</span>\] [`ConfigOption.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.0.8/src/main/java/org/refcodes/cli/ConfigOption.java) (see Javadoc at [`ConfigOption.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/3.0.8/org.refcodes.cli/org/refcodes/cli/ConfigOption.html))
* \[<span style="color:green">MODIFIED</span>\] [`DaemonFlag.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.0.8/src/main/java/org/refcodes/cli/DaemonFlag.java) (see Javadoc at [`DaemonFlag.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/3.0.8/org.refcodes.cli/org/refcodes/cli/DaemonFlag.html))
* \[<span style="color:green">MODIFIED</span>\] [`DebugFlag.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.0.8/src/main/java/org/refcodes/cli/DebugFlag.java) (see Javadoc at [`DebugFlag.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/3.0.8/org.refcodes.cli/org/refcodes/cli/DebugFlag.html))
* \[<span style="color:green">MODIFIED</span>\] [`DoubleOption.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.0.8/src/main/java/org/refcodes/cli/DoubleOption.java) (see Javadoc at [`DoubleOption.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/3.0.8/org.refcodes.cli/org/refcodes/cli/DoubleOption.html))
* \[<span style="color:green">MODIFIED</span>\] [`EnumOption.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.0.8/src/main/java/org/refcodes/cli/EnumOption.java) (see Javadoc at [`EnumOption.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/3.0.8/org.refcodes.cli/org/refcodes/cli/EnumOption.html))
* \[<span style="color:green">MODIFIED</span>\] [`FileOption.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.0.8/src/main/java/org/refcodes/cli/FileOption.java) (see Javadoc at [`FileOption.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/3.0.8/org.refcodes.cli/org/refcodes/cli/FileOption.html))
* \[<span style="color:green">MODIFIED</span>\] [`Flag.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.0.8/src/main/java/org/refcodes/cli/Flag.java) (see Javadoc at [`Flag.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/3.0.8/org.refcodes.cli/org/refcodes/cli/Flag.html))
* \[<span style="color:green">MODIFIED</span>\] [`FloatOption.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.0.8/src/main/java/org/refcodes/cli/FloatOption.java) (see Javadoc at [`FloatOption.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/3.0.8/org.refcodes.cli/org/refcodes/cli/FloatOption.html))
* \[<span style="color:green">MODIFIED</span>\] [`ForceFlag.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.0.8/src/main/java/org/refcodes/cli/ForceFlag.java) (see Javadoc at [`ForceFlag.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/3.0.8/org.refcodes.cli/org/refcodes/cli/ForceFlag.html))
* \[<span style="color:green">MODIFIED</span>\] [`HelpFlag.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.0.8/src/main/java/org/refcodes/cli/HelpFlag.java) (see Javadoc at [`HelpFlag.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/3.0.8/org.refcodes.cli/org/refcodes/cli/HelpFlag.html))
* \[<span style="color:green">MODIFIED</span>\] [`InitFlag.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.0.8/src/main/java/org/refcodes/cli/InitFlag.java) (see Javadoc at [`InitFlag.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/3.0.8/org.refcodes.cli/org/refcodes/cli/InitFlag.html))
* \[<span style="color:green">MODIFIED</span>\] [`IntOption.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.0.8/src/main/java/org/refcodes/cli/IntOption.java) (see Javadoc at [`IntOption.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/3.0.8/org.refcodes.cli/org/refcodes/cli/IntOption.html))
* \[<span style="color:green">MODIFIED</span>\] [`LongOption.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.0.8/src/main/java/org/refcodes/cli/LongOption.java) (see Javadoc at [`LongOption.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/3.0.8/org.refcodes.cli/org/refcodes/cli/LongOption.html))
* \[<span style="color:green">MODIFIED</span>\] [`NoneOperand.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.0.8/src/main/java/org/refcodes/cli/NoneOperand.java) (see Javadoc at [`NoneOperand.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/3.0.8/org.refcodes.cli/org/refcodes/cli/NoneOperand.html))
* \[<span style="color:green">MODIFIED</span>\] [`Operand.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.0.8/src/main/java/org/refcodes/cli/Operand.java) (see Javadoc at [`Operand.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/3.0.8/org.refcodes.cli/org/refcodes/cli/Operand.html))
* \[<span style="color:green">MODIFIED</span>\] [`Operation.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.0.8/src/main/java/org/refcodes/cli/Operation.java) (see Javadoc at [`Operation.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/3.0.8/org.refcodes.cli/org/refcodes/cli/Operation.html))
* \[<span style="color:green">MODIFIED</span>\] [`Option.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.0.8/src/main/java/org/refcodes/cli/Option.java) (see Javadoc at [`Option.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/3.0.8/org.refcodes.cli/org/refcodes/cli/Option.html))
* \[<span style="color:green">MODIFIED</span>\] [`OrCondition.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.0.8/src/main/java/org/refcodes/cli/OrCondition.java) (see Javadoc at [`OrCondition.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/3.0.8/org.refcodes.cli/org/refcodes/cli/OrCondition.html))
* \[<span style="color:green">MODIFIED</span>\] [`QuietFlag.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.0.8/src/main/java/org/refcodes/cli/QuietFlag.java) (see Javadoc at [`QuietFlag.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/3.0.8/org.refcodes.cli/org/refcodes/cli/QuietFlag.html))
* \[<span style="color:green">MODIFIED</span>\] [`StringOperand.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.0.8/src/main/java/org/refcodes/cli/StringOperand.java) (see Javadoc at [`StringOperand.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/3.0.8/org.refcodes.cli/org/refcodes/cli/StringOperand.html))
* \[<span style="color:green">MODIFIED</span>\] [`StringOption.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.0.8/src/main/java/org/refcodes/cli/StringOption.java) (see Javadoc at [`StringOption.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/3.0.8/org.refcodes.cli/org/refcodes/cli/StringOption.html))
* \[<span style="color:green">MODIFIED</span>\] [`Synopsisable.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.0.8/src/main/java/org/refcodes/cli/Synopsisable.java) (see Javadoc at [`Synopsisable.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/3.0.8/org.refcodes.cli/org/refcodes/cli/Synopsisable.html))
* \[<span style="color:green">MODIFIED</span>\] [`SyntaxNotation.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.0.8/src/main/java/org/refcodes/cli/SyntaxNotation.java) (see Javadoc at [`SyntaxNotation.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/3.0.8/org.refcodes.cli/org/refcodes/cli/SyntaxNotation.html))
* \[<span style="color:green">MODIFIED</span>\] [`SysInfoFlag.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.0.8/src/main/java/org/refcodes/cli/SysInfoFlag.java) (see Javadoc at [`SysInfoFlag.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/3.0.8/org.refcodes.cli/org/refcodes/cli/SysInfoFlag.html))
* \[<span style="color:green">MODIFIED</span>\] [`VerboseFlag.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.0.8/src/main/java/org/refcodes/cli/VerboseFlag.java) (see Javadoc at [`VerboseFlag.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/3.0.8/org.refcodes.cli/org/refcodes/cli/VerboseFlag.html))
* \[<span style="color:green">MODIFIED</span>\] [`XorCondition.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.0.8/src/main/java/org/refcodes/cli/XorCondition.java) (see Javadoc at [`XorCondition.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/3.0.8/org.refcodes.cli/org/refcodes/cli/XorCondition.html))
* \[<span style="color:green">MODIFIED</span>\] [`ArgsFilterTest.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.0.8/src/test/java/org/refcodes/cli/ArgsFilterTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`ArgsParserTest.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.0.8/src/test/java/org/refcodes/cli/ArgsParserTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`OptionalConditionTest.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.0.8/src/test/java/org/refcodes/cli/OptionalConditionTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`StackOverflowTest.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.0.8/src/test/java/org/refcodes/cli/StackOverflowTest.java) 

## Change list &lt;refcodes-audio&gt; (version 3.0.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-audio/src/refcodes-audio-3.0.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-audio/src/refcodes-audio-3.0.8/README.md) 

## Change list &lt;refcodes-codec&gt; (version 3.0.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-3.0.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-3.0.8/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`BaseMetricsConfig.java`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-3.0.8/src/main/java/org/refcodes/codec/BaseMetricsConfig.java) (see Javadoc at [`BaseMetricsConfig.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-codec/3.0.8/org.refcodes.codec/org/refcodes/codec/BaseMetricsConfig.html))

## Change list &lt;refcodes-component-ext&gt; (version 3.0.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-3.0.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-3.0.8/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-3.0.8/refcodes-component-ext-observer/pom.xml) 

## Change list &lt;refcodes-properties&gt; (version 3.0.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-3.0.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`ArgsProperties.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-3.0.8/src/main/java/org/refcodes/properties/ArgsProperties.java) (see Javadoc at [`ArgsProperties.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties/3.0.8/org.refcodes.properties/org/refcodes/properties/ArgsProperties.html))

## Change list &lt;refcodes-security&gt; (version 3.0.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security/src/refcodes-security-3.0.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security/src/refcodes-security-3.0.8/README.md) 

## Change list &lt;refcodes-security-alt&gt; (version 3.0.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-3.0.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-3.0.8/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-3.0.8/refcodes-security-alt-chaos/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-3.0.8/refcodes-security-alt-chaos/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`module-info.java`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-3.0.8/refcodes-security-alt-chaos/src/main/java/module-info.java) (see Javadoc at [`module-info.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-security-alt-chaos/3.0.8//module-info.html))
* \[<span style="color:green">MODIFIED</span>\] [`ChaosKey.java`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-3.0.8/refcodes-security-alt-chaos/src/main/java/org/refcodes/security/alt/chaos/ChaosKey.java) (see Javadoc at [`ChaosKey.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-security-alt-chaos/3.0.8/org.refcodes.security.alt.chaos/org/refcodes/security/alt/chaos/ChaosKey.html))
* \[<span style="color:green">MODIFIED</span>\] [`ChaosMode.java`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-3.0.8/refcodes-security-alt-chaos/src/main/java/org/refcodes/security/alt/chaos/ChaosMode.java) (see Javadoc at [`ChaosMode.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-security-alt-chaos/3.0.8/org.refcodes.security.alt.chaos/org/refcodes/security/alt/chaos/ChaosMode.html))
* \[<span style="color:green">MODIFIED</span>\] [`ChaosEncryptionDecryptionStreamTest.java`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-3.0.8/refcodes-security-alt-chaos/src/test/java/org/refcodes/security/alt/chaos/ChaosEncryptionDecryptionStreamTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`ChaosKeyTest.java`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-3.0.8/refcodes-security-alt-chaos/src/test/java/org/refcodes/security/alt/chaos/ChaosKeyTest.java) 

## Change list &lt;refcodes-security-ext&gt; (version 3.0.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-3.0.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-3.0.8/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-3.0.8/refcodes-security-ext-chaos/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-3.0.8/refcodes-security-ext-chaos/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-3.0.8/refcodes-security-ext-spring/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-3.0.8/refcodes-security-ext-spring/README.md) 

## Change list &lt;refcodes-properties-ext&gt; (version 3.0.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.0.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.0.8/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.0.8/refcodes-properties-ext-cli/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.0.8/refcodes-properties-ext-cli/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`ArgsParserPropertiesAccessor.java`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.0.8/refcodes-properties-ext-cli/src/main/java/org/refcodes/properties/ext/cli/ArgsParserPropertiesAccessor.java) (see Javadoc at [`ArgsParserPropertiesAccessor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties-ext-cli/3.0.8/org.refcodes.properties.ext.cli/org/refcodes/properties/ext/cli/ArgsParserPropertiesAccessor.html))
* \[<span style="color:green">MODIFIED</span>\] [`ArgsParserPropertiesImpl.java`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.0.8/refcodes-properties-ext-cli/src/main/java/org/refcodes/properties/ext/cli/ArgsParserPropertiesImpl.java) (see Javadoc at [`ArgsParserPropertiesImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties-ext-cli/3.0.8/org.refcodes.properties.ext.cli/org/refcodes/properties/ext/cli/ArgsParserPropertiesImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`ArgsParserProperties.java`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.0.8/refcodes-properties-ext-cli/src/main/java/org/refcodes/properties/ext/cli/ArgsParserProperties.java) (see Javadoc at [`ArgsParserProperties.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties-ext-cli/3.0.8/org.refcodes.properties.ext.cli/org/refcodes/properties/ext/cli/ArgsParserProperties.html))
* \[<span style="color:green">MODIFIED</span>\] [`package-info.java`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.0.8/refcodes-properties-ext-cli/src/main/java/org/refcodes/properties/ext/cli/package-info.java) 
* \[<span style="color:green">MODIFIED</span>\] [`ArgsParserPropertiesTest.java`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.0.8/refcodes-properties-ext-cli/src/test/java/org/refcodes/properties/ext/cli/ArgsParserPropertiesTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.0.8/refcodes-properties-ext-obfuscation/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.0.8/refcodes-properties-ext-obfuscation/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.0.8/refcodes-properties-ext-observer/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.0.8/refcodes-properties-ext-observer/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.0.8/refcodes-properties-ext-runtime/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.0.8/refcodes-properties-ext-runtime/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`package-info.java`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.0.8/refcodes-properties-ext-runtime/src/main/java/org/refcodes/properties/ext/runtime/package-info.java) 
* \[<span style="color:green">MODIFIED</span>\] [`RuntimePropertiesImpl.java`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.0.8/refcodes-properties-ext-runtime/src/main/java/org/refcodes/properties/ext/runtime/RuntimePropertiesImpl.java) (see Javadoc at [`RuntimePropertiesImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties-ext-runtime/3.0.8/org.refcodes.properties.ext.runtime/org/refcodes/properties/ext/runtime/RuntimePropertiesImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`RuntimeProperties.java`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.0.8/refcodes-properties-ext-runtime/src/main/java/org/refcodes/properties/ext/runtime/RuntimeProperties.java) (see Javadoc at [`RuntimeProperties.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties-ext-runtime/3.0.8/org.refcodes.properties.ext.runtime/org/refcodes/properties/ext/runtime/RuntimeProperties.html))
* \[<span style="color:green">MODIFIED</span>\] [`RuntimePropertiesSugar.java`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.0.8/refcodes-properties-ext-runtime/src/main/java/org/refcodes/properties/ext/runtime/RuntimePropertiesSugar.java) (see Javadoc at [`RuntimePropertiesSugar.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties-ext-runtime/3.0.8/org.refcodes.properties.ext.runtime/org/refcodes/properties/ext/runtime/RuntimePropertiesSugar.html))
* \[<span style="color:green">MODIFIED</span>\] [`RuntimePropertiesSugarTest.java`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.0.8/refcodes-properties-ext-runtime/src/test/java/org/refcodes/properties/ext/runtime/RuntimePropertiesSugarTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`RuntimePropertiesTest.java`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.0.8/refcodes-properties-ext-runtime/src/test/java/org/refcodes/properties/ext/runtime/RuntimePropertiesTest.java) 

## Change list &lt;refcodes-logger&gt; (version 3.0.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger/src/refcodes-logger-3.0.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger/src/refcodes-logger-3.0.8/README.md) 


## Change list &lt;refcodes-logger-alt&gt; (version 3.0.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.0.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.0.8/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.0.8/refcodes-logger-alt-async/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.0.8/refcodes-logger-alt-async/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.0.8/refcodes-logger-alt-console/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.0.8/refcodes-logger-alt-console/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.0.8/refcodes-logger-alt-io/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.0.8/refcodes-logger-alt-io/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.0.8/refcodes-logger-alt-simpledb/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.0.8/refcodes-logger-alt-simpledb/README.md) 

## Change list &lt;refcodes-logger-ext&gt; (version 3.0.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-3.0.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-3.0.8/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-3.0.8/refcodes-logger-ext-slf4j-legacy/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-3.0.8/refcodes-logger-ext-slf4j-legacy/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-3.0.8/refcodes-logger-ext-slf4j/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-3.0.8/refcodes-logger-ext-slf4j/README.md) 

## Change list &lt;refcodes-graphical-ext&gt; (version 3.0.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-3.0.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-3.0.8/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-3.0.8/refcodes-graphical-ext-javafx/pom.xml) 

## Change list &lt;refcodes-checkerboard&gt; (version 3.0.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-3.0.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-3.0.8/README.md) 

## Change list &lt;refcodes-checkerboard-alt&gt; (version 3.0.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-alt/src/refcodes-checkerboard-alt-3.0.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-alt/src/refcodes-checkerboard-alt-3.0.8/refcodes-checkerboard-alt-javafx/pom.xml) 

## Change list &lt;refcodes-net&gt; (version 3.0.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-3.0.8/pom.xml) 

## Change list &lt;refcodes-web&gt; (version 3.0.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-web/src/refcodes-web-3.0.8/pom.xml) 

## Change list &lt;refcodes-rest&gt; (version 3.0.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-3.0.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-3.0.8/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`AbstractRestClient.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-3.0.8/src/main/java/org/refcodes/rest/AbstractRestClient.java) (see Javadoc at [`AbstractRestClient.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/3.0.8/org.refcodes.rest/org/refcodes/rest/AbstractRestClient.html))
* \[<span style="color:green">MODIFIED</span>\] [`AbstractRestServer.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-3.0.8/src/main/java/org/refcodes/rest/AbstractRestServer.java) (see Javadoc at [`AbstractRestServer.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/3.0.8/org.refcodes.rest/org/refcodes/rest/AbstractRestServer.html))
* \[<span style="color:green">MODIFIED</span>\] [`HttpRestClientImpl.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-3.0.8/src/main/java/org/refcodes/rest/HttpRestClientImpl.java) (see Javadoc at [`HttpRestClientImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/3.0.8/org.refcodes.rest/org/refcodes/rest/HttpRestClientImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`HttpRestServerImpl.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-3.0.8/src/main/java/org/refcodes/rest/HttpRestServerImpl.java) (see Javadoc at [`HttpRestServerImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/3.0.8/org.refcodes.rest/org/refcodes/rest/HttpRestServerImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`HttpRestServerSugar.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-3.0.8/src/main/java/org/refcodes/rest/HttpRestServerSugar.java) (see Javadoc at [`HttpRestServerSugar.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/3.0.8/org.refcodes.rest/org/refcodes/rest/HttpRestServerSugar.html))
* \[<span style="color:green">MODIFIED</span>\] [`RestServer.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-3.0.8/src/main/java/org/refcodes/rest/RestServer.java) (see Javadoc at [`RestServer.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/3.0.8/org.refcodes.rest/org/refcodes/rest/RestServer.html))

## Change list &lt;refcodes-hal&gt; (version 3.0.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-3.0.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-3.0.8/README.md) 

## Change list &lt;refcodes-eventbus&gt; (version 3.0.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-eventbus/src/refcodes-eventbus-3.0.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-eventbus/src/refcodes-eventbus-3.0.8/README.md) 

## Change list &lt;refcodes-eventbus-ext&gt; (version 3.0.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-3.0.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-3.0.8/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-3.0.8/refcodes-eventbus-ext-application/pom.xml) 

## Change list &lt;refcodes-filesystem&gt; (version 3.0.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-filesystem/src/refcodes-filesystem-3.0.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-filesystem/src/refcodes-filesystem-3.0.8/README.md) 

## Change list &lt;refcodes-filesystem-alt&gt; (version 3.0.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-filesystem-alt/src/refcodes-filesystem-alt-3.0.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-filesystem-alt/src/refcodes-filesystem-alt-3.0.8/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-filesystem-alt/src/refcodes-filesystem-alt-3.0.8/refcodes-filesystem-alt-s3/pom.xml) 


## Change list &lt;refcodes-forwardsecrecy&gt; (version 3.0.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy/src/refcodes-forwardsecrecy-3.0.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy/src/refcodes-forwardsecrecy-3.0.8/README.md) 

## Change list &lt;refcodes-forwardsecrecy-alt&gt; (version 3.0.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-3.0.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-3.0.8/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-3.0.8/refcodes-forwardsecrecy-alt-filesystem/pom.xml) 

## Change list &lt;refcodes-io-ext&gt; (version 3.0.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-3.0.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-3.0.8/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-3.0.8/refcodes-io-ext-observer/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-3.0.8/refcodes-io-ext-observer/README.md) 

## Change list &lt;refcodes-interceptor&gt; (version 3.0.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-interceptor/src/refcodes-interceptor-3.0.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-interceptor/src/refcodes-interceptor-3.0.8/README.md) 

## Change list &lt;refcodes-jobbus&gt; (version 3.0.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-jobbus/src/refcodes-jobbus-3.0.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-jobbus/src/refcodes-jobbus-3.0.8/README.md) 

## Change list &lt;refcodes-rest-ext&gt; (version 3.0.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-3.0.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-3.0.8/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-3.0.8/refcodes-rest-ext-eureka/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-3.0.8/refcodes-rest-ext-eureka/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`EurekaRestServerDecorator.java`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-3.0.8/refcodes-rest-ext-eureka/src/main/java/org/refcodes/rest/ext/eureka/EurekaRestServerDecorator.java) (see Javadoc at [`EurekaRestServerDecorator.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest-ext-eureka/3.0.8/org.refcodes.rest.ext.eureka/org/refcodes/rest/ext/eureka/EurekaRestServerDecorator.html))
* \[<span style="color:green">MODIFIED</span>\] [`EurekaRestClientTest.java`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-3.0.8/refcodes-rest-ext-eureka/src/test/java/org/refcodes/rest/ext/eureka/EurekaRestClientTest.java) 

## Change list &lt;refcodes-remoting&gt; (version 3.0.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-remoting/src/refcodes-remoting-3.0.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-remoting/src/refcodes-remoting-3.0.8/README.md) 

## Change list &lt;refcodes-remoting-ext&gt; (version 3.0.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-3.0.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-3.0.8/README.md) 

## Change list &lt;refcodes-serial&gt; (version 3.0.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial/src/refcodes-serial-3.0.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-serial/src/refcodes-serial-3.0.8/README.md) 

## Change list &lt;refcodes-serial-alt&gt; (version 3.0.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial-alt/src/refcodes-serial-alt-3.0.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-serial-alt/src/refcodes-serial-alt-3.0.8/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial-alt/src/refcodes-serial-alt-3.0.8/refcodes-serial-alt-net/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-serial-alt/src/refcodes-serial-alt-3.0.8/refcodes-serial-alt-net/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial-alt/src/refcodes-serial-alt-3.0.8/refcodes-serial-alt-tty/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-serial-alt/src/refcodes-serial-alt-3.0.8/refcodes-serial-alt-tty/README.md) 

## Change list &lt;refcodes-serial-ext&gt; (version 3.0.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-3.0.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-3.0.8/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-3.0.8/refcodes-serial-ext-handshake/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-3.0.8/refcodes-serial-ext-handshake/README.md) 

## Change list &lt;refcodes-p2p&gt; (version 3.0.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-p2p/src/refcodes-p2p-3.0.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-p2p/src/refcodes-p2p-3.0.8/README.md) 

## Change list &lt;refcodes-p2p-alt&gt; (version 3.0.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-p2p-alt/src/refcodes-p2p-alt-3.0.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-p2p-alt/src/refcodes-p2p-alt-3.0.8/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-p2p-alt/src/refcodes-p2p-alt-3.0.8/refcodes-p2p-alt-rest/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-p2p-alt/src/refcodes-p2p-alt-3.0.8/refcodes-p2p-alt-serial/pom.xml) 

## Change list &lt;refcodes-p2p-ext&gt; (version 3.0.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-p2p-ext/src/refcodes-p2p-ext-3.0.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-p2p-ext/src/refcodes-p2p-ext-3.0.8/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-p2p-ext/src/refcodes-p2p-ext-3.0.8/refcodes-p2p-ext-observer/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`ObservablePeerFacade.java`](https://bitbucket.org/refcodes/refcodes-p2p-ext/src/refcodes-p2p-ext-3.0.8/refcodes-p2p-ext-observer/src/main/java/org/refcodes/p2p/ext/observer/ObservablePeerFacade.java) (see Javadoc at [`ObservablePeerFacade.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-p2p-ext-observer/3.0.8/org.refcodes.p2p.ext.observer/org/refcodes/p2p/ext/observer/ObservablePeerFacade.html))

## Change list &lt;refcodes-servicebus&gt; (version 3.0.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-servicebus/src/refcodes-servicebus-3.0.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-servicebus/src/refcodes-servicebus-3.0.8/README.md) 

## Change list &lt;refcodes-servicebus-alt&gt; (version 3.0.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-servicebus-alt/src/refcodes-servicebus-alt-3.0.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-servicebus-alt/src/refcodes-servicebus-alt-3.0.8/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-servicebus-alt/src/refcodes-servicebus-alt-3.0.8/refcodes-servicebus-alt-spring/pom.xml) 

## Change list &lt;refcodes-tabular-alt&gt; (version 3.0.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-tabular-alt/src/refcodes-tabular-alt-3.0.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-tabular-alt/src/refcodes-tabular-alt-3.0.8/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-tabular-alt/src/refcodes-tabular-alt-3.0.8/refcodes-tabular-alt-forwardsecrecy/pom.xml) 

## Change list &lt;refcodes-archetype&gt; (version 3.0.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype/src/refcodes-archetype-3.0.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype/src/refcodes-archetype-3.0.8/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`AppHelper.java`](https://bitbucket.org/refcodes/refcodes-archetype/src/refcodes-archetype-3.0.8/src/main/java/org/refcodes/archetype/AppHelper.java) (see Javadoc at [`AppHelper.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-archetype/3.0.8/org.refcodes.archetype/org/refcodes/archetype/AppHelper.html))
* \[<span style="color:green">MODIFIED</span>\] [`CliHelper.java`](https://bitbucket.org/refcodes/refcodes-archetype/src/refcodes-archetype-3.0.8/src/main/java/org/refcodes/archetype/CliHelper.java) (see Javadoc at [`CliHelper.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-archetype/3.0.8/org.refcodes.archetype/org/refcodes/archetype/CliHelper.html))

## Change list &lt;refcodes-archetype-alt&gt; (version 3.0.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.0.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.0.8/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.0.8/refcodes-archetype-alt-c2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.0.8/refcodes-archetype-alt-c2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`bundle-launcher.sh`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.0.8/refcodes-archetype-alt-c2/src/main/resources/archetype-resources/bundle-launcher.sh) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.0.8/refcodes-archetype-alt-c2/src/main/resources/archetype-resources/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`scriptify-launcher.sh`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.0.8/refcodes-archetype-alt-c2/src/main/resources/archetype-resources/scriptify-launcher.sh) 
* \[<span style="color:green">MODIFIED</span>\] [`Main.java`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.0.8/refcodes-archetype-alt-c2/src/main/resources/archetype-resources/src/main/java/Main.java) (see Javadoc at [`Main.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-archetype-alt-c2/src/main/resources/archetype-resources/3.0.8/src.main.resources.archetype-resources/Main.html))
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.0.8/refcodes-archetype-alt-cli/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.0.8/refcodes-archetype-alt-cli/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`bundle-launcher.sh`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.0.8/refcodes-archetype-alt-cli/src/main/resources/archetype-resources/bundle-launcher.sh) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.0.8/refcodes-archetype-alt-cli/src/main/resources/archetype-resources/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`scriptify-launcher.sh`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.0.8/refcodes-archetype-alt-cli/src/main/resources/archetype-resources/scriptify-launcher.sh) 
* \[<span style="color:green">MODIFIED</span>\] [`Main.java`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.0.8/refcodes-archetype-alt-cli/src/main/resources/archetype-resources/src/main/java/Main.java) (see Javadoc at [`Main.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-archetype-alt-cli/src/main/resources/archetype-resources/3.0.8/src.main.resources.archetype-resources/Main.html))
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.0.8/refcodes-archetype-alt-csv/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.0.8/refcodes-archetype-alt-csv/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`bundle-launcher.sh`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.0.8/refcodes-archetype-alt-csv/src/main/resources/archetype-resources/bundle-launcher.sh) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.0.8/refcodes-archetype-alt-csv/src/main/resources/archetype-resources/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`scriptify-launcher.sh`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.0.8/refcodes-archetype-alt-csv/src/main/resources/archetype-resources/scriptify-launcher.sh) 
* \[<span style="color:green">MODIFIED</span>\] [`Main.java`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.0.8/refcodes-archetype-alt-csv/src/main/resources/archetype-resources/src/main/java/Main.java) (see Javadoc at [`Main.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-archetype-alt-csv/src/main/resources/archetype-resources/3.0.8/src.main.resources.archetype-resources/Main.html))
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.0.8/refcodes-archetype-alt-eventbus/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.0.8/refcodes-archetype-alt-eventbus/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`bundle-launcher.sh`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.0.8/refcodes-archetype-alt-eventbus/src/main/resources/archetype-resources/bundle-launcher.sh) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.0.8/refcodes-archetype-alt-eventbus/src/main/resources/archetype-resources/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`scriptify-launcher.sh`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.0.8/refcodes-archetype-alt-eventbus/src/main/resources/archetype-resources/scriptify-launcher.sh) 
* \[<span style="color:green">MODIFIED</span>\] [`Main.java`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.0.8/refcodes-archetype-alt-eventbus/src/main/resources/archetype-resources/src/main/java/Main.java) (see Javadoc at [`Main.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-archetype-alt-eventbus/src/main/resources/archetype-resources/3.0.8/src.main.resources.archetype-resources/Main.html))
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.0.8/refcodes-archetype-alt-filter/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.0.8/refcodes-archetype-alt-filter/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`bundle-launcher.sh`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.0.8/refcodes-archetype-alt-filter/src/main/resources/archetype-resources/bundle-launcher.sh) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.0.8/refcodes-archetype-alt-filter/src/main/resources/archetype-resources/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`scriptify-launcher.sh`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.0.8/refcodes-archetype-alt-filter/src/main/resources/archetype-resources/scriptify-launcher.sh) 
* \[<span style="color:green">MODIFIED</span>\] [`Main.java`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.0.8/refcodes-archetype-alt-filter/src/main/resources/archetype-resources/src/main/java/Main.java) (see Javadoc at [`Main.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-archetype-alt-filter/src/main/resources/archetype-resources/3.0.8/src.main.resources.archetype-resources/Main.html))
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.0.8/refcodes-archetype-alt-rest/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.0.8/refcodes-archetype-alt-rest/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`bundle-launcher.sh`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.0.8/refcodes-archetype-alt-rest/src/main/resources/archetype-resources/bundle-launcher.sh) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.0.8/refcodes-archetype-alt-rest/src/main/resources/archetype-resources/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`scriptify-launcher.sh`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.0.8/refcodes-archetype-alt-rest/src/main/resources/archetype-resources/scriptify-launcher.sh) 
* \[<span style="color:green">MODIFIED</span>\] [`Main.java`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.0.8/refcodes-archetype-alt-rest/src/main/resources/archetype-resources/src/main/java/Main.java) (see Javadoc at [`Main.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-archetype-alt-rest/src/main/resources/archetype-resources/3.0.8/src.main.resources.archetype-resources/Main.html))