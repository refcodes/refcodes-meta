# Change list for REFCODES.ORG artifacts' version 1.0.5

> This change list has been auto-generated on `triton` by `steiner` with with `changelist-all.sh` on the 2017-04-26 at 21:49:09.

## Change list &lt;refcodes-licensing&gt; (version 1.0.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-licensing/src/refcodes-licensing-1.0.5/pom.xml) 

## Change list &lt;refcodes-parent&gt; (version 1.0.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-parent/src/refcodes-parent-1.0.5/pom.xml) 

## Change list &lt;refcodes-time&gt; (version 1.0.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-time/src/refcodes-time-1.0.5/pom.xml) 

## Change list &lt;refcodes-exception&gt; (version 1.0.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-exception/src/refcodes-exception-1.0.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`ExceptionUtility.java`](https://bitbucket.org/refcodes/refcodes-exception/src/refcodes-exception-1.0.5/src/main/java/org/refcodes/exception/ExceptionUtility.java) (see Javadoc at [`ExceptionUtility.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-exception/1.0.5/org/refcodes/exception/ExceptionUtility.html))

## Change list &lt;refcodes-mixin&gt; (version 1.0.5)

* \[<span style="color:blue">ADDED</span>\] [`LocatorAccessor.java`](https://bitbucket.org/refcodes/refcodes-mixin/src/refcodes-mixin-1.0.5/src/main/java/org/refcodes/mixin/LocatorAccessor.java) (see Javadoc at [`LocatorAccessor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-mixin/1.0.5/org/refcodes/mixin/LocatorAccessor.html))
* \[<span style="color:blue">ADDED</span>\] [`RequestAccessor.java`](https://bitbucket.org/refcodes/refcodes-mixin/src/refcodes-mixin-1.0.5/src/main/java/org/refcodes/mixin/RequestAccessor.java) (see Javadoc at [`RequestAccessor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-mixin/1.0.5/org/refcodes/mixin/RequestAccessor.html))
* \[<span style="color:blue">ADDED</span>\] [`ResponseAccessor.java`](https://bitbucket.org/refcodes/refcodes-mixin/src/refcodes-mixin-1.0.5/src/main/java/org/refcodes/mixin/ResponseAccessor.java) (see Javadoc at [`ResponseAccessor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-mixin/1.0.5/org/refcodes/mixin/ResponseAccessor.html))
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-mixin/src/refcodes-mixin-1.0.5/pom.xml) 

## Change list &lt;refcodes-data&gt; (version 1.0.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data/src/refcodes-data-1.0.5/pom.xml) 

## Change list &lt;refcodes-controlflow&gt; (version 1.0.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-controlflow/src/refcodes-controlflow-1.0.5/pom.xml) 

## Change list &lt;refcodes-numerical&gt; (version 1.0.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-numerical/src/refcodes-numerical-1.0.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`NumberBaseBuilderImpl.java`](https://bitbucket.org/refcodes/refcodes-numerical/src/refcodes-numerical-1.0.5/src/main/java/org/refcodes/numerical/NumberBaseBuilderImpl.java) (see Javadoc at [`NumberBaseBuilderImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-numerical/1.0.5/org/refcodes/numerical/NumberBaseBuilderImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`NumberBaseBuilderTest.java`](https://bitbucket.org/refcodes/refcodes-numerical/src/refcodes-numerical-1.0.5/src/test/java/org/refcodes/numerical/NumberBaseBuilderTest.java) 

## Change list &lt;refcodes-generator&gt; (version 1.0.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-generator/src/refcodes-generator-1.0.5/pom.xml) 

## Change list &lt;refcodes-collection&gt; (version 1.0.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-collection/src/refcodes-collection-1.0.5/pom.xml) 

## Change list &lt;refcodes-runtime&gt; (version 1.0.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-1.0.5/pom.xml) 

## Change list &lt;refcodes-component&gt; (version 1.0.5)

* \[<span style="color:blue">ADDED</span>\] [`LinkComponentHandle.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-1.0.5/src/main/java/org/refcodes/component/LinkComponentHandle.java) (see Javadoc at [`LinkComponentHandle.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/1.0.5/org/refcodes/component/LinkComponentHandle.html))
* \[<span style="color:blue">ADDED</span>\] [`LinkComponent.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-1.0.5/src/main/java/org/refcodes/component/LinkComponent.java) (see Javadoc at [`LinkComponent.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/1.0.5/org/refcodes/component/LinkComponent.html))
* \[<span style="color:red">DELETED</span>\] `DeviceComponentHandle.java`
* \[<span style="color:red">DELETED</span>\] `DeviceComponent.java`
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-1.0.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`BidirectionalConnectionComponent.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-1.0.5/src/main/java/org/refcodes/component/BidirectionalConnectionComponent.java) (see Javadoc at [`BidirectionalConnectionComponent.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/1.0.5/org/refcodes/component/BidirectionalConnectionComponent.html))
* \[<span style="color:green">MODIFIED</span>\] [`Ceasable.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-1.0.5/src/main/java/org/refcodes/component/Ceasable.java) (see Javadoc at [`Ceasable.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/1.0.5/org/refcodes/component/Ceasable.html))
* \[<span style="color:green">MODIFIED</span>\] [`CloseException.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-1.0.5/src/main/java/org/refcodes/component/CloseException.java) (see Javadoc at [`CloseException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/1.0.5/org/refcodes/component/CloseException.html))
* \[<span style="color:green">MODIFIED</span>\] [`CompositeComponent.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-1.0.5/src/main/java/org/refcodes/component/CompositeComponent.java) (see Javadoc at [`CompositeComponent.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/1.0.5/org/refcodes/component/CompositeComponent.html))
* \[<span style="color:green">MODIFIED</span>\] [`ConnectionComponent.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-1.0.5/src/main/java/org/refcodes/component/ConnectionComponent.java) (see Javadoc at [`ConnectionComponent.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/1.0.5/org/refcodes/component/ConnectionComponent.html))
* \[<span style="color:green">MODIFIED</span>\] [`AbstractDeviceAutomaton.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-1.0.5/src/main/java/org/refcodes/component/AbstractDeviceAutomaton.java) (see Javadoc at [`AbstractDeviceAutomaton.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/1.0.5/org/refcodes/component/AbstractDeviceAutomaton.html))
* \[<span style="color:green">MODIFIED</span>\] [`DeviceAutomatonImpl.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-1.0.5/src/main/java/org/refcodes/component/DeviceAutomatonImpl.java) (see Javadoc at [`DeviceAutomatonImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/1.0.5/org/refcodes/component/DeviceAutomatonImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`OpenException.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-1.0.5/src/main/java/org/refcodes/component/OpenException.java) (see Javadoc at [`OpenException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/1.0.5/org/refcodes/component/OpenException.html))
* \[<span style="color:green">MODIFIED</span>\] [`OpenTimeoutException.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-1.0.5/src/main/java/org/refcodes/component/OpenTimeoutException.java) (see Javadoc at [`OpenTimeoutException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/1.0.5/org/refcodes/component/OpenTimeoutException.html))
* \[<span style="color:green">MODIFIED</span>\] [`DeviceAutomatonTest.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-1.0.5/src/test/java/org/refcodes/component/DeviceAutomatonTest.java) 

## Change list &lt;refcodes-interceptor&gt; (version 1.0.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-interceptor/src/refcodes-interceptor-1.0.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-interceptor/src/refcodes-interceptor-1.0.5/README.md) 

## Change list &lt;refcodes-factory&gt; (version 1.0.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-factory/src/refcodes-factory-1.0.5/pom.xml) 

## Change list &lt;refcodes-data-ext&gt; (version 1.0.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.0.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.0.5/refcodes-data-ext-boulderdash/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.0.5/refcodes-data-ext-boulderdash/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.0.5/refcodes-data-ext-checkers/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.0.5/refcodes-data-ext-checkers/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.0.5/refcodes-data-ext-chess/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.0.5/refcodes-data-ext-chess/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.0.5/refcodes-data-ext-corporate/pom.xml) 

## Change list &lt;refcodes-graphical&gt; (version 1.0.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-graphical/src/refcodes-graphical-1.0.5/pom.xml) 

## Change list &lt;refcodes-textual&gt; (version 1.0.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-1.0.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`TableBuilderImpl.java`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-1.0.5/src/main/java/org/refcodes/textual/TableBuilderImpl.java) (see Javadoc at [`TableBuilderImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-textual/1.0.5/org/refcodes/textual/TableBuilderImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`VerboseTextBuilder.java`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-1.0.5/src/main/java/org/refcodes/textual/VerboseTextBuilder.java) (see Javadoc at [`VerboseTextBuilder.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-textual/1.0.5/org/refcodes/textual/VerboseTextBuilder.html))
* \[<span style="color:green">MODIFIED</span>\] [`CsvBuilderTest.java`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-1.0.5/src/test/java/org/refcodes/textual/CsvBuilderTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`SecretHintBuilderTest.java`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-1.0.5/src/test/java/org/refcodes/textual/SecretHintBuilderTest.java) 

## Change list &lt;refcodes-criteria&gt; (version 1.0.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-1.0.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-1.0.5/README.md) 

## Change list &lt;refcodes-tabular&gt; (version 1.0.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-tabular/src/refcodes-tabular-1.0.5/pom.xml) 

## Change list &lt;refcodes-logger&gt; (version 1.0.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger/src/refcodes-logger-1.0.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger/src/refcodes-logger-1.0.5/README.md) 

## Change list &lt;refcodes-factory-alt&gt; (version 1.0.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-factory-alt/src/refcodes-factory-alt-1.0.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-factory-alt/src/refcodes-factory-alt-1.0.5/refcodes-factory-alt-spring/pom.xml) 

## Change list &lt;refcodes-logger-alt&gt; (version 1.0.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.0.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.0.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.0.5/refcodes-logger-alt-async/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.0.5/refcodes-logger-alt-async/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.0.5/refcodes-logger-alt-console/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.0.5/refcodes-logger-alt-console/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.0.5/refcodes-logger-alt-io/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.0.5/refcodes-logger-alt-io/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.0.5/refcodes-logger-alt-simpledb/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.0.5/refcodes-logger-alt-slf4j/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.0.5/refcodes-logger-alt-spring/pom.xml) 

## Change list &lt;refcodes-matcher&gt; (version 1.0.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-matcher/src/refcodes-matcher-1.0.5/pom.xml) 

## Change list &lt;refcodes-observer&gt; (version 1.0.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-observer/src/refcodes-observer-1.0.5/pom.xml) 

## Change list &lt;refcodes-checkerboard&gt; (version 1.0.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-1.0.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-1.0.5/README.md) 

## Change list &lt;refcodes-graphical-ext&gt; (version 1.0.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-1.0.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-1.0.5/refcodes-graphical-ext-javafx/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`FxDragSpriteEventHandler.java`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-1.0.5/refcodes-graphical-ext-javafx/src/main/java/org/refcodes/graphical/ext/javafx/FxDragSpriteEventHandler.java) (see Javadoc at [`FxDragSpriteEventHandler.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-graphical-ext-javafx/1.0.5/org/refcodes/graphical/ext/javafx/FxDragSpriteEventHandler.html))
* \[<span style="color:green">MODIFIED</span>\] [`FxDragSpriteEventHandlerImpl.java`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-1.0.5/refcodes-graphical-ext-javafx/src/main/java/org/refcodes/graphical/ext/javafx/FxDragSpriteEventHandlerImpl.java) (see Javadoc at [`FxDragSpriteEventHandlerImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-graphical-ext-javafx/1.0.5/org/refcodes/graphical/ext/javafx/FxDragSpriteEventHandlerImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`FxViewportPaneImpl.java`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-1.0.5/refcodes-graphical-ext-javafx/src/main/java/org/refcodes/graphical/ext/javafx/FxViewportPaneImpl.java) (see Javadoc at [`FxViewportPaneImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-graphical-ext-javafx/1.0.5/org/refcodes/graphical/ext/javafx/FxViewportPaneImpl.html))

## Change list &lt;refcodes-checkerboard-alt&gt; (version 1.0.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-alt/src/refcodes-checkerboard-alt-1.0.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-alt/src/refcodes-checkerboard-alt-1.0.5/refcodes-checkerboard-alt-javafx/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`FxCheckerboardViewerImpl.java`](https://bitbucket.org/refcodes/refcodes-checkerboard-alt/src/refcodes-checkerboard-alt-1.0.5/refcodes-checkerboard-alt-javafx/src/main/java/org/refcodes/checkerboard/alt/javafx/FxCheckerboardViewerImpl.java) (see Javadoc at [`FxCheckerboardViewerImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-checkerboard-alt-javafx/1.0.5/org/refcodes/checkerboard/alt/javafx/FxCheckerboardViewerImpl.html))

## Change list &lt;refcodes-io&gt; (version 1.0.5)

* \[<span style="color:blue">ADDED</span>\] [`MaxConnectionsAccessor.java`](https://bitbucket.org/refcodes/refcodes-io/src/refcodes-io-1.0.5/src/main/java/org/refcodes/io/MaxConnectionsAccessor.java) (see Javadoc at [`MaxConnectionsAccessor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-io/1.0.5/org/refcodes/io/MaxConnectionsAccessor.html))
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-io/src/refcodes-io-1.0.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`BidirectionalStreamConnectionTransceiver.java`](https://bitbucket.org/refcodes/refcodes-io/src/refcodes-io-1.0.5/src/main/java/org/refcodes/io/BidirectionalStreamConnectionTransceiver.java) (see Javadoc at [`BidirectionalStreamConnectionTransceiver.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-io/1.0.5/org/refcodes/io/BidirectionalStreamConnectionTransceiver.html))

## Change list &lt;refcodes-codec&gt; (version 1.0.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-1.0.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-1.0.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`BaseCodecBuilder.java`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-1.0.5/src/main/java/org/refcodes/codec/BaseCodecBuilder.java) (see Javadoc at [`BaseCodecBuilder.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-codec/1.0.5/org/refcodes/codec/BaseCodecBuilder.html))
* \[<span style="color:green">MODIFIED</span>\] [`BaseCodecConfig.java`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-1.0.5/src/main/java/org/refcodes/codec/BaseCodecConfig.java) (see Javadoc at [`BaseCodecConfig.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-codec/1.0.5/org/refcodes/codec/BaseCodecConfig.html))
* \[<span style="color:green">MODIFIED</span>\] [`BaseCodecMetrics.java`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-1.0.5/src/main/java/org/refcodes/codec/BaseCodecMetrics.java) (see Javadoc at [`BaseCodecMetrics.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-codec/1.0.5/org/refcodes/codec/BaseCodecMetrics.html))
* \[<span style="color:green">MODIFIED</span>\] [`BaseDecodeReceiver.java`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-1.0.5/src/main/java/org/refcodes/codec/BaseDecodeReceiver.java) (see Javadoc at [`BaseDecodeReceiver.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-codec/1.0.5/org/refcodes/codec/BaseDecodeReceiver.html))
* \[<span style="color:green">MODIFIED</span>\] [`BaseEncodeSender.java`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-1.0.5/src/main/java/org/refcodes/codec/BaseEncodeSender.java) (see Javadoc at [`BaseEncodeSender.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-codec/1.0.5/org/refcodes/codec/BaseEncodeSender.html))
* \[<span style="color:green">MODIFIED</span>\] [`DecodeReceiver.java`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-1.0.5/src/main/java/org/refcodes/codec/DecodeReceiver.java) (see Javadoc at [`DecodeReceiver.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-codec/1.0.5/org/refcodes/codec/DecodeReceiver.html))
* \[<span style="color:green">MODIFIED</span>\] [`EncodeSender.java`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-1.0.5/src/main/java/org/refcodes/codec/EncodeSender.java) (see Javadoc at [`EncodeSender.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-codec/1.0.5/org/refcodes/codec/EncodeSender.html))
* \[<span style="color:green">MODIFIED</span>\] [`BaseCodecBuilderImpl.java`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-1.0.5/src/main/java/org/refcodes/codec/BaseCodecBuilderImpl.java) (see Javadoc at [`BaseCodecBuilderImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-codec/1.0.5/org/refcodes/codec/BaseCodecBuilderImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`BaseCodecMetricsImpl.java`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-1.0.5/src/main/java/org/refcodes/codec/BaseCodecMetricsImpl.java) (see Javadoc at [`BaseCodecMetricsImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-codec/1.0.5/org/refcodes/codec/BaseCodecMetricsImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`BaseDecodeInputStreamReceiverImpl.java`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-1.0.5/src/main/java/org/refcodes/codec/BaseDecodeInputStreamReceiverImpl.java) (see Javadoc at [`BaseDecodeInputStreamReceiverImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-codec/1.0.5/org/refcodes/codec/BaseDecodeInputStreamReceiverImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`BaseDecodeReceiverImpl.java`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-1.0.5/src/main/java/org/refcodes/codec/BaseDecodeReceiverImpl.java) (see Javadoc at [`BaseDecodeReceiverImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-codec/1.0.5/org/refcodes/codec/BaseDecodeReceiverImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`BaseEncodeOutputStreamSenderImpl.java`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-1.0.5/src/main/java/org/refcodes/codec/BaseEncodeOutputStreamSenderImpl.java) (see Javadoc at [`BaseEncodeOutputStreamSenderImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-codec/1.0.5/org/refcodes/codec/BaseEncodeOutputStreamSenderImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`BaseEncodeSenderImpl.java`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-1.0.5/src/main/java/org/refcodes/codec/BaseEncodeSenderImpl.java) (see Javadoc at [`BaseEncodeSenderImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-codec/1.0.5/org/refcodes/codec/BaseEncodeSenderImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`BaseCodecBuilderTest.java`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-1.0.5/src/test/java/org/refcodes/codec/BaseCodecBuilderTest.java) 

## Change list &lt;refcodes-command&gt; (version 1.0.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-command/src/refcodes-command-1.0.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-command/src/refcodes-command-1.0.5/README.md) 

## Change list &lt;refcodes-component-ext&gt; (version 1.0.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.0.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.0.5/refcodes-component-ext-observer/pom.xml) 

## Change list &lt;refcodes-cli&gt; (version 1.0.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-1.0.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-1.0.5/README.md) 

## Change list &lt;refcodes-security&gt; (version 1.0.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security/src/refcodes-security-1.0.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security/src/refcodes-security-1.0.5/README.md) 

## Change list &lt;refcodes-forwardsecrecy&gt; (version 1.0.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy/src/refcodes-forwardsecrecy-1.0.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy/src/refcodes-forwardsecrecy-1.0.5/README.md) 

## Change list &lt;refcodes-filesystem&gt; (version 1.0.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-filesystem/src/refcodes-filesystem-1.0.5/pom.xml) 

## Change list &lt;refcodes-forwardsecrecy-alt&gt; (version 1.0.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-1.0.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-1.0.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-1.0.5/refcodes-forwardsecrecy-alt-filesystem/pom.xml) 

## Change list &lt;refcodes-eventbus&gt; (version 1.0.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-eventbus/src/refcodes-eventbus-1.0.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-eventbus/src/refcodes-eventbus-1.0.5/README.md) 

## Change list &lt;refcodes-filesystem-alt&gt; (version 1.0.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-filesystem-alt/src/refcodes-filesystem-alt-1.0.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-filesystem-alt/src/refcodes-filesystem-alt-1.0.5/refcodes-filesystem-alt-s3/pom.xml) 

## Change list &lt;refcodes-io-ext&gt; (version 1.0.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-1.0.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-1.0.5/refcodes-io-ext-observable/pom.xml) 

## Change list &lt;refcodes-jobbus&gt; (version 1.0.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-jobbus/src/refcodes-jobbus-1.0.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-jobbus/src/refcodes-jobbus-1.0.5/README.md) 

## Change list &lt;refcodes-logger-ext&gt; (version 1.0.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-1.0.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-1.0.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-1.0.5/refcodes-logger-ext-slf4j/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-1.0.5/refcodes-logger-ext-slf4j/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`RuntimeLoggerFactory.java`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-1.0.5/refcodes-logger-ext-slf4j/src/main/java/org/slf4j/impl/RuntimeLoggerFactory.java) (see Javadoc at [`RuntimeLoggerFactory.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-logger-ext-slf4j/1.0.5/org/slf4j/impl/RuntimeLoggerFactory.html))

## Change list &lt;refcodes-remoting&gt; (version 1.0.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-remoting/src/refcodes-remoting-1.0.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-remoting/src/refcodes-remoting-1.0.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`RemoteClientImpl.java`](https://bitbucket.org/refcodes/refcodes-remoting/src/refcodes-remoting-1.0.5/src/main/java/org/refcodes/remoting/RemoteClientImpl.java) (see Javadoc at [`RemoteClientImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-remoting/1.0.5/org/refcodes/remoting/RemoteClientImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`RemoteServerImpl.java`](https://bitbucket.org/refcodes/refcodes-remoting/src/refcodes-remoting-1.0.5/src/main/java/org/refcodes/remoting/RemoteServerImpl.java) (see Javadoc at [`RemoteServerImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-remoting/1.0.5/org/refcodes/remoting/RemoteServerImpl.html))

## Change list &lt;refcodes-remoting-ext&gt; (version 1.0.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-1.0.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-1.0.5/refcodes-remoting-ext-observer/pom.xml) 

## Change list &lt;refcodes-security-alt&gt; (version 1.0.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-1.0.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-1.0.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-1.0.5/refcodes-security-alt-chaos/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-1.0.5/refcodes-security-alt-chaos/README.md) 

## Change list &lt;refcodes-security-ext&gt; (version 1.0.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-1.0.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-1.0.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-1.0.5/refcodes-security-ext-chaos/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-1.0.5/refcodes-security-ext-chaos/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-1.0.5/refcodes-security-ext-spring/pom.xml) 

## Change list &lt;refcodes-servicebus&gt; (version 1.0.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-servicebus/src/refcodes-servicebus-1.0.5/pom.xml) 

## Change list &lt;refcodes-servicebus-alt&gt; (version 1.0.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-servicebus-alt/src/refcodes-servicebus-alt-1.0.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-servicebus-alt/src/refcodes-servicebus-alt-1.0.5/refcodes-servicebus-alt-spring/pom.xml) 

## Change list &lt;refcodes-tabular-alt&gt; (version 1.0.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-tabular-alt/src/refcodes-tabular-alt-1.0.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-tabular-alt/src/refcodes-tabular-alt-1.0.5/refcodes-tabular-alt-forwardsecrecy/pom.xml) 
