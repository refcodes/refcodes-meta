# Change list for REFCODES.ORG artifacts' version 2.0.3

> This change list has been auto-generated on `triton.local` by `steiner` with `changelist-all.sh` on the 2019-07-07 at 19:12:52.

## Change list &lt;refcodes-licensing&gt; (version 2.0.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-licensing/src/refcodes-licensing-2.0.3/pom.xml) 

## Change list &lt;refcodes-parent&gt; (version 2.0.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-parent/src/refcodes-parent-2.0.3/pom.xml) 

## Change list &lt;refcodes-time&gt; (version 2.0.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-time/src/refcodes-time-2.0.3/pom.xml) 

## Change list &lt;refcodes-mixin&gt; (version 2.0.3)

* \[<span style="color:blue">ADDED</span>\] [`AnnotatorAccessor.java`](https://bitbucket.org/refcodes/refcodes-mixin/src/refcodes-mixin-2.0.3/src/main/java/org/refcodes/mixin/AnnotatorAccessor.java) (see Javadoc at [`AnnotatorAccessor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-mixin/2.0.3/org/refcodes/mixin/AnnotatorAccessor.html))
* \[<span style="color:blue">ADDED</span>\] [`EscapeCodesStatusAccessor.java`](https://bitbucket.org/refcodes/refcodes-mixin/src/refcodes-mixin-2.0.3/src/main/java/org/refcodes/mixin/EscapeCodesStatusAccessor.java) (see Javadoc at [`EscapeCodesStatusAccessor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-mixin/2.0.3/org/refcodes/mixin/EscapeCodesStatusAccessor.html))
* \[<span style="color:blue">ADDED</span>\] [`ResetEscapeCodeAccessor.java`](https://bitbucket.org/refcodes/refcodes-mixin/src/refcodes-mixin-2.0.3/src/main/java/org/refcodes/mixin/ResetEscapeCodeAccessor.java) (see Javadoc at [`ResetEscapeCodeAccessor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-mixin/2.0.3/org/refcodes/mixin/ResetEscapeCodeAccessor.html))
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-mixin/src/refcodes-mixin-2.0.3/pom.xml) 

## Change list &lt;refcodes-data&gt; (version 2.0.3)

* \[<span style="color:blue">ADDED</span>\] [`BooleanLiterals.java`](https://bitbucket.org/refcodes/refcodes-data/src/refcodes-data-2.0.3/src/main/java/org/refcodes/data/BooleanLiterals.java) (see Javadoc at [`BooleanLiterals.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-data/2.0.3/org/refcodes/data/BooleanLiterals.html))
* \[<span style="color:blue">ADDED</span>\] [`CommandArgPrefixes.java`](https://bitbucket.org/refcodes/refcodes-data/src/refcodes-data-2.0.3/src/main/java/org/refcodes/data/CommandArgPrefixes.java) (see Javadoc at [`CommandArgPrefixes.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-data/2.0.3/org/refcodes/data/CommandArgPrefixes.html))
* \[<span style="color:blue">ADDED</span>\] [`AnsiEscapeCodeTest.java`](https://bitbucket.org/refcodes/refcodes-data/src/refcodes-data-2.0.3/src/test/java/org/refcodes/data/AnsiEscapeCodeTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data/src/refcodes-data-2.0.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`AnsiEscapeCode.java`](https://bitbucket.org/refcodes/refcodes-data/src/refcodes-data-2.0.3/src/main/java/org/refcodes/data/AnsiEscapeCode.java) (see Javadoc at [`AnsiEscapeCode.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-data/2.0.3/org/refcodes/data/AnsiEscapeCode.html))
* \[<span style="color:green">MODIFIED</span>\] [`CommandArgPrefix.java`](https://bitbucket.org/refcodes/refcodes-data/src/refcodes-data-2.0.3/src/main/java/org/refcodes/data/CommandArgPrefix.java) (see Javadoc at [`CommandArgPrefix.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-data/2.0.3/org/refcodes/data/CommandArgPrefix.html))
* \[<span style="color:green">MODIFIED</span>\] [`Delimiter.java`](https://bitbucket.org/refcodes/refcodes-data/src/refcodes-data-2.0.3/src/main/java/org/refcodes/data/Delimiter.java) (see Javadoc at [`Delimiter.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-data/2.0.3/org/refcodes/data/Delimiter.html))
* \[<span style="color:green">MODIFIED</span>\] [`EnvironmentVariable.java`](https://bitbucket.org/refcodes/refcodes-data/src/refcodes-data-2.0.3/src/main/java/org/refcodes/data/EnvironmentVariable.java) (see Javadoc at [`EnvironmentVariable.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-data/2.0.3/org/refcodes/data/EnvironmentVariable.html))
* \[<span style="color:green">MODIFIED</span>\] [`Prefix.java`](https://bitbucket.org/refcodes/refcodes-data/src/refcodes-data-2.0.3/src/main/java/org/refcodes/data/Prefix.java) (see Javadoc at [`Prefix.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-data/2.0.3/org/refcodes/data/Prefix.html))

## Change list &lt;refcodes-exception&gt; (version 2.0.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-exception/src/refcodes-exception-2.0.3/pom.xml) 

## Change list &lt;refcodes-factory&gt; (version 2.0.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-factory/src/refcodes-factory-2.0.3/pom.xml) 

## Change list &lt;refcodes-factory-alt&gt; (version 2.0.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-factory-alt/src/refcodes-factory-alt-2.0.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-factory-alt/src/refcodes-factory-alt-2.0.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-factory-alt/src/refcodes-factory-alt-2.0.3/refcodes-factory-alt-spring/pom.xml) 

## Change list &lt;refcodes-controlflow&gt; (version 2.0.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-controlflow/src/refcodes-controlflow-2.0.3/pom.xml) 

## Change list &lt;refcodes-numerical&gt; (version 2.0.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-numerical/src/refcodes-numerical-2.0.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`NumericalUtility.java`](https://bitbucket.org/refcodes/refcodes-numerical/src/refcodes-numerical-2.0.3/src/main/java/org/refcodes/numerical/NumericalUtility.java) (see Javadoc at [`NumericalUtility.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-numerical/2.0.3/org/refcodes/numerical/NumericalUtility.html))

## Change list &lt;refcodes-generator&gt; (version 2.0.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-generator/src/refcodes-generator-2.0.3/pom.xml) 

## Change list &lt;refcodes-matcher&gt; (version 2.0.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-matcher/src/refcodes-matcher-2.0.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`PathMatcherTest.java`](https://bitbucket.org/refcodes/refcodes-matcher/src/refcodes-matcher-2.0.3/src/test/java/org/refcodes/matcher/PathMatcherTest.java) 

## Change list &lt;refcodes-structure&gt; (version 2.0.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-2.0.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-2.0.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`CanonicalMapImpl.java`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-2.0.3/src/main/java/org/refcodes/structure/CanonicalMapImpl.java) (see Javadoc at [`CanonicalMapImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-structure/2.0.3/org/refcodes/structure/CanonicalMapImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`CanonicalMap.java`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-2.0.3/src/main/java/org/refcodes/structure/CanonicalMap.java) (see Javadoc at [`CanonicalMap.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-structure/2.0.3/org/refcodes/structure/CanonicalMap.html))
* \[<span style="color:green">MODIFIED</span>\] [`PathMapBuilderImpl.java`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-2.0.3/src/main/java/org/refcodes/structure/PathMapBuilderImpl.java) (see Javadoc at [`PathMapBuilderImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-structure/2.0.3/org/refcodes/structure/PathMapBuilderImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`PathMapImpl.java`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-2.0.3/src/main/java/org/refcodes/structure/PathMapImpl.java) (see Javadoc at [`PathMapImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-structure/2.0.3/org/refcodes/structure/PathMapImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`PathMap.java`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-2.0.3/src/main/java/org/refcodes/structure/PathMap.java) (see Javadoc at [`PathMap.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-structure/2.0.3/org/refcodes/structure/PathMap.html))
* \[<span style="color:green">MODIFIED</span>\] [`PropertiesAccessorMixin.java`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-2.0.3/src/main/java/org/refcodes/structure/PropertiesAccessorMixin.java) (see Javadoc at [`PropertiesAccessorMixin.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-structure/2.0.3/org/refcodes/structure/PropertiesAccessorMixin.html))
* \[<span style="color:green">MODIFIED</span>\] [`StructureUtility.java`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-2.0.3/src/main/java/org/refcodes/structure/StructureUtility.java) (see Javadoc at [`StructureUtility.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-structure/2.0.3/org/refcodes/structure/StructureUtility.html))
* \[<span style="color:green">MODIFIED</span>\] [`TypeUtility.java`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-2.0.3/src/main/java/org/refcodes/structure/TypeUtility.java) (see Javadoc at [`TypeUtility.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-structure/2.0.3/org/refcodes/structure/TypeUtility.html))
* \[<span style="color:green">MODIFIED</span>\] [`PathMapArrayTest.java`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-2.0.3/src/test/java/org/refcodes/structure/PathMapArrayTest.java) 

## Change list &lt;refcodes-structure-ext&gt; (version 2.0.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-structure-ext/src/refcodes-structure-ext-2.0.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-structure-ext/src/refcodes-structure-ext-2.0.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-structure-ext/src/refcodes-structure-ext-2.0.3/refcodes-structure-ext-factory/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`AbstractCanonicalMapFactory.java`](https://bitbucket.org/refcodes/refcodes-structure-ext/src/refcodes-structure-ext-2.0.3/refcodes-structure-ext-factory/src/main/java/org/refcodes/structure/ext/factory/AbstractCanonicalMapFactory.java) (see Javadoc at [`AbstractCanonicalMapFactory.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-structure-ext-factory/2.0.3/org/refcodes/structure/ext/factory/AbstractCanonicalMapFactory.html))
* \[<span style="color:green">MODIFIED</span>\] [`CanonicalMapFactory.java`](https://bitbucket.org/refcodes/refcodes-structure-ext/src/refcodes-structure-ext-2.0.3/refcodes-structure-ext-factory/src/main/java/org/refcodes/structure/ext/factory/CanonicalMapFactory.java) (see Javadoc at [`CanonicalMapFactory.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-structure-ext-factory/2.0.3/org/refcodes/structure/ext/factory/CanonicalMapFactory.html))
* \[<span style="color:green">MODIFIED</span>\] [`HtmlCanonicalMapFactory.java`](https://bitbucket.org/refcodes/refcodes-structure-ext/src/refcodes-structure-ext-2.0.3/refcodes-structure-ext-factory/src/main/java/org/refcodes/structure/ext/factory/HtmlCanonicalMapFactory.java) (see Javadoc at [`HtmlCanonicalMapFactory.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-structure-ext-factory/2.0.3/org/refcodes/structure/ext/factory/HtmlCanonicalMapFactory.html))
* \[<span style="color:green">MODIFIED</span>\] [`JavaCanonicalMapFactory.java`](https://bitbucket.org/refcodes/refcodes-structure-ext/src/refcodes-structure-ext-2.0.3/refcodes-structure-ext-factory/src/main/java/org/refcodes/structure/ext/factory/JavaCanonicalMapFactory.java) (see Javadoc at [`JavaCanonicalMapFactory.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-structure-ext-factory/2.0.3/org/refcodes/structure/ext/factory/JavaCanonicalMapFactory.html))
* \[<span style="color:green">MODIFIED</span>\] [`JsonCanonicalMapFactory.java`](https://bitbucket.org/refcodes/refcodes-structure-ext/src/refcodes-structure-ext-2.0.3/refcodes-structure-ext-factory/src/main/java/org/refcodes/structure/ext/factory/JsonCanonicalMapFactory.java) (see Javadoc at [`JsonCanonicalMapFactory.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-structure-ext-factory/2.0.3/org/refcodes/structure/ext/factory/JsonCanonicalMapFactory.html))
* \[<span style="color:green">MODIFIED</span>\] [`TomlCanonicalMapFactory.java`](https://bitbucket.org/refcodes/refcodes-structure-ext/src/refcodes-structure-ext-2.0.3/refcodes-structure-ext-factory/src/main/java/org/refcodes/structure/ext/factory/TomlCanonicalMapFactory.java) (see Javadoc at [`TomlCanonicalMapFactory.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-structure-ext-factory/2.0.3/org/refcodes/structure/ext/factory/TomlCanonicalMapFactory.html))
* \[<span style="color:green">MODIFIED</span>\] [`XmlCanonicalMapFactory.java`](https://bitbucket.org/refcodes/refcodes-structure-ext/src/refcodes-structure-ext-2.0.3/refcodes-structure-ext-factory/src/main/java/org/refcodes/structure/ext/factory/XmlCanonicalMapFactory.java) (see Javadoc at [`XmlCanonicalMapFactory.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-structure-ext-factory/2.0.3/org/refcodes/structure/ext/factory/XmlCanonicalMapFactory.html))
* \[<span style="color:green">MODIFIED</span>\] [`YamlCanonicalMapFactory.java`](https://bitbucket.org/refcodes/refcodes-structure-ext/src/refcodes-structure-ext-2.0.3/refcodes-structure-ext-factory/src/main/java/org/refcodes/structure/ext/factory/YamlCanonicalMapFactory.java) (see Javadoc at [`YamlCanonicalMapFactory.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-structure-ext-factory/2.0.3/org/refcodes/structure/ext/factory/YamlCanonicalMapFactory.html))
* \[<span style="color:green">MODIFIED</span>\] [`AbstractCanonicalMapFactoryTest.java`](https://bitbucket.org/refcodes/refcodes-structure-ext/src/refcodes-structure-ext-2.0.3/refcodes-structure-ext-factory/src/test/java/org/refcodes/structure/ext/factory/AbstractCanonicalMapFactoryTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`HtmlCanonicalMapFactoryTest.java`](https://bitbucket.org/refcodes/refcodes-structure-ext/src/refcodes-structure-ext-2.0.3/refcodes-structure-ext-factory/src/test/java/org/refcodes/structure/ext/factory/HtmlCanonicalMapFactoryTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`JsonCanonicalMapFactoryTest.java`](https://bitbucket.org/refcodes/refcodes-structure-ext/src/refcodes-structure-ext-2.0.3/refcodes-structure-ext-factory/src/test/java/org/refcodes/structure/ext/factory/JsonCanonicalMapFactoryTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`XmlCanonicalMapFactoryTest.java`](https://bitbucket.org/refcodes/refcodes-structure-ext/src/refcodes-structure-ext-2.0.3/refcodes-structure-ext-factory/src/test/java/org/refcodes/structure/ext/factory/XmlCanonicalMapFactoryTest.java) 

## Change list &lt;refcodes-runtime&gt; (version 2.0.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-2.0.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-2.0.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`Terminal.java`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-2.0.3/src/main/java/org/refcodes/runtime/Terminal.java) (see Javadoc at [`Terminal.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-runtime/2.0.3/org/refcodes/runtime/Terminal.html))

## Change list &lt;refcodes-component&gt; (version 2.0.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-2.0.3/pom.xml) 

## Change list &lt;refcodes-data-ext&gt; (version 2.0.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-2.0.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-2.0.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-2.0.3/refcodes-data-ext-boulderdash/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-2.0.3/refcodes-data-ext-checkers/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-2.0.3/refcodes-data-ext-checkers/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-2.0.3/refcodes-data-ext-chess/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-2.0.3/refcodes-data-ext-chess/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-2.0.3/refcodes-data-ext-corporate/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-2.0.3/refcodes-data-ext-corporate/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-2.0.3/refcodes-data-ext-symbols/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-2.0.3/refcodes-data-ext-symbols/README.md) 

## Change list &lt;refcodes-graphical&gt; (version 2.0.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-graphical/src/refcodes-graphical-2.0.3/pom.xml) 

## Change list &lt;refcodes-textual&gt; (version 2.0.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-2.0.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`TableBuilderImpl.java`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-2.0.3/src/main/java/org/refcodes/textual/TableBuilderImpl.java) (see Javadoc at [`TableBuilderImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-textual/2.0.3/org/refcodes/textual/TableBuilderImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`TableBuilder.java`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-2.0.3/src/main/java/org/refcodes/textual/TableBuilder.java) (see Javadoc at [`TableBuilder.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-textual/2.0.3/org/refcodes/textual/TableBuilder.html))
* \[<span style="color:green">MODIFIED</span>\] [`TableStyle.java`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-2.0.3/src/main/java/org/refcodes/textual/TableStyle.java) (see Javadoc at [`TableStyle.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-textual/2.0.3/org/refcodes/textual/TableStyle.html))
* \[<span style="color:green">MODIFIED</span>\] [`TableBuilderTest.java`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-2.0.3/src/test/java/org/refcodes/textual/TableBuilderTest.java) 

## Change list &lt;refcodes-criteria&gt; (version 2.0.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-2.0.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`AndCriteria.java`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-2.0.3/src/main/java/org/refcodes/criteria/AndCriteria.java) (see Javadoc at [`AndCriteria.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-criteria/2.0.3/org/refcodes/criteria/AndCriteria.html))
* \[<span style="color:green">MODIFIED</span>\] [`Criteria.java`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-2.0.3/src/main/java/org/refcodes/criteria/Criteria.java) (see Javadoc at [`Criteria.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-criteria/2.0.3/org/refcodes/criteria/Criteria.html))
* \[<span style="color:green">MODIFIED</span>\] [`CriteriaLeaf.java`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-2.0.3/src/main/java/org/refcodes/criteria/CriteriaLeaf.java) (see Javadoc at [`CriteriaLeaf.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-criteria/2.0.3/org/refcodes/criteria/CriteriaLeaf.html))
* \[<span style="color:green">MODIFIED</span>\] [`CriteriaNode.java`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-2.0.3/src/main/java/org/refcodes/criteria/CriteriaNode.java) (see Javadoc at [`CriteriaNode.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-criteria/2.0.3/org/refcodes/criteria/CriteriaNode.html))
* \[<span style="color:green">MODIFIED</span>\] [`EqualWithCriteria.java`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-2.0.3/src/main/java/org/refcodes/criteria/EqualWithCriteria.java) (see Javadoc at [`EqualWithCriteria.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-criteria/2.0.3/org/refcodes/criteria/EqualWithCriteria.html))
* \[<span style="color:green">MODIFIED</span>\] [`GreaterOrEqualThanCriteria.java`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-2.0.3/src/main/java/org/refcodes/criteria/GreaterOrEqualThanCriteria.java) (see Javadoc at [`GreaterOrEqualThanCriteria.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-criteria/2.0.3/org/refcodes/criteria/GreaterOrEqualThanCriteria.html))
* \[<span style="color:green">MODIFIED</span>\] [`GreaterThanCriteria.java`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-2.0.3/src/main/java/org/refcodes/criteria/GreaterThanCriteria.java) (see Javadoc at [`GreaterThanCriteria.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-criteria/2.0.3/org/refcodes/criteria/GreaterThanCriteria.html))
* \[<span style="color:green">MODIFIED</span>\] [`IntersectWithCriteria.java`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-2.0.3/src/main/java/org/refcodes/criteria/IntersectWithCriteria.java) (see Javadoc at [`IntersectWithCriteria.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-criteria/2.0.3/org/refcodes/criteria/IntersectWithCriteria.html))
* \[<span style="color:green">MODIFIED</span>\] [`LessOrEqualThanCriteria.java`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-2.0.3/src/main/java/org/refcodes/criteria/LessOrEqualThanCriteria.java) (see Javadoc at [`LessOrEqualThanCriteria.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-criteria/2.0.3/org/refcodes/criteria/LessOrEqualThanCriteria.html))
* \[<span style="color:green">MODIFIED</span>\] [`LessThanCriteria.java`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-2.0.3/src/main/java/org/refcodes/criteria/LessThanCriteria.java) (see Javadoc at [`LessThanCriteria.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-criteria/2.0.3/org/refcodes/criteria/LessThanCriteria.html))
* \[<span style="color:green">MODIFIED</span>\] [`NotCriteria.java`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-2.0.3/src/main/java/org/refcodes/criteria/NotCriteria.java) (see Javadoc at [`NotCriteria.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-criteria/2.0.3/org/refcodes/criteria/NotCriteria.html))
* \[<span style="color:green">MODIFIED</span>\] [`NotEqualWithCriteria.java`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-2.0.3/src/main/java/org/refcodes/criteria/NotEqualWithCriteria.java) (see Javadoc at [`NotEqualWithCriteria.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-criteria/2.0.3/org/refcodes/criteria/NotEqualWithCriteria.html))
* \[<span style="color:green">MODIFIED</span>\] [`OrCriteria.java`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-2.0.3/src/main/java/org/refcodes/criteria/OrCriteria.java) (see Javadoc at [`OrCriteria.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-criteria/2.0.3/org/refcodes/criteria/OrCriteria.html))
* \[<span style="color:green">MODIFIED</span>\] [`package-info.java`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-2.0.3/src/main/java/org/refcodes/criteria/package-info.java) 
* \[<span style="color:green">MODIFIED</span>\] [`SingleCriteriaNode.java`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-2.0.3/src/main/java/org/refcodes/criteria/SingleCriteriaNode.java) (see Javadoc at [`SingleCriteriaNode.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-criteria/2.0.3/org/refcodes/criteria/SingleCriteriaNode.html))
* \[<span style="color:green">MODIFIED</span>\] [`log4j.xml`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-2.0.3/src/test/resources/log4j.xml) 

## Change list &lt;refcodes-tabular&gt; (version 2.0.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-tabular/src/refcodes-tabular-2.0.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-tabular/src/refcodes-tabular-2.0.3/README.md) 

## Change list &lt;refcodes-observer&gt; (version 2.0.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-observer/src/refcodes-observer-2.0.3/pom.xml) 

## Change list &lt;refcodes-command&gt; (version 2.0.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-command/src/refcodes-command-2.0.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-command/src/refcodes-command-2.0.3/README.md) 

## Change list &lt;refcodes-cli&gt; (version 2.0.3)

* \[<span style="color:blue">ADDED</span>\] [`FileOption.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-2.0.3/src/main/java/org/refcodes/console/FileOption.java) (see Javadoc at [`FileOption.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/2.0.3/org/refcodes/console/FileOption.html))
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-2.0.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-2.0.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`AbstractCondition.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-2.0.3/src/main/java/org/refcodes/console/AbstractCondition.java) (see Javadoc at [`AbstractCondition.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/2.0.3/org/refcodes/console/AbstractCondition.html))
* \[<span style="color:green">MODIFIED</span>\] [`AbstractOperand.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-2.0.3/src/main/java/org/refcodes/console/AbstractOperand.java) (see Javadoc at [`AbstractOperand.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/2.0.3/org/refcodes/console/AbstractOperand.html))
* \[<span style="color:green">MODIFIED</span>\] [`AbstractOption.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-2.0.3/src/main/java/org/refcodes/console/AbstractOption.java) (see Javadoc at [`AbstractOption.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/2.0.3/org/refcodes/console/AbstractOption.html))
* \[<span style="color:green">MODIFIED</span>\] [`AbstractSyntaxable.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-2.0.3/src/main/java/org/refcodes/console/AbstractSyntaxable.java) (see Javadoc at [`AbstractSyntaxable.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/2.0.3/org/refcodes/console/AbstractSyntaxable.html))
* \[<span style="color:green">MODIFIED</span>\] [`AndCondition.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-2.0.3/src/main/java/org/refcodes/console/AndCondition.java) (see Javadoc at [`AndCondition.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/2.0.3/org/refcodes/console/AndCondition.html))
* \[<span style="color:green">MODIFIED</span>\] [`ArgsParserImpl.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-2.0.3/src/main/java/org/refcodes/console/ArgsParserImpl.java) (see Javadoc at [`ArgsParserImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/2.0.3/org/refcodes/console/ArgsParserImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`ArgsParser.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-2.0.3/src/main/java/org/refcodes/console/ArgsParser.java) (see Javadoc at [`ArgsParser.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/2.0.3/org/refcodes/console/ArgsParser.html))
* \[<span style="color:green">MODIFIED</span>\] [`CliSugar.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-2.0.3/src/main/java/org/refcodes/console/CliSugar.java) (see Javadoc at [`CliSugar.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/2.0.3/org/refcodes/console/CliSugar.html))
* \[<span style="color:green">MODIFIED</span>\] [`ConsoleUtility.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-2.0.3/src/main/java/org/refcodes/console/ConsoleUtility.java) (see Javadoc at [`ConsoleUtility.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/2.0.3/org/refcodes/console/ConsoleUtility.html))
* \[<span style="color:green">MODIFIED</span>\] [`EnumOption.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-2.0.3/src/main/java/org/refcodes/console/EnumOption.java) (see Javadoc at [`EnumOption.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/2.0.3/org/refcodes/console/EnumOption.html))
* \[<span style="color:green">MODIFIED</span>\] [`Flag.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-2.0.3/src/main/java/org/refcodes/console/Flag.java) (see Javadoc at [`Flag.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/2.0.3/org/refcodes/console/Flag.html))
* \[<span style="color:green">MODIFIED</span>\] [`IntOption.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-2.0.3/src/main/java/org/refcodes/console/IntOption.java) (see Javadoc at [`IntOption.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/2.0.3/org/refcodes/console/IntOption.html))
* \[<span style="color:green">MODIFIED</span>\] [`LongOption.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-2.0.3/src/main/java/org/refcodes/console/LongOption.java) (see Javadoc at [`LongOption.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/2.0.3/org/refcodes/console/LongOption.html))
* \[<span style="color:green">MODIFIED</span>\] [`Operand.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-2.0.3/src/main/java/org/refcodes/console/Operand.java) (see Javadoc at [`Operand.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/2.0.3/org/refcodes/console/Operand.html))
* \[<span style="color:green">MODIFIED</span>\] [`AnyCondition.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-2.0.3/src/main/java/org/refcodes/console/AnyCondition.java) (see Javadoc at [`AnyCondition.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/2.0.3/org/refcodes/console/AnyCondition.html))
* \[<span style="color:green">MODIFIED</span>\] [`Option.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-2.0.3/src/main/java/org/refcodes/console/Option.java) (see Javadoc at [`Option.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/2.0.3/org/refcodes/console/Option.html))
* \[<span style="color:green">MODIFIED</span>\] [`OrCondition.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-2.0.3/src/main/java/org/refcodes/console/OrCondition.java) (see Javadoc at [`OrCondition.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/2.0.3/org/refcodes/console/OrCondition.html))
* \[<span style="color:green">MODIFIED</span>\] [`StringOperand.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-2.0.3/src/main/java/org/refcodes/console/StringOperand.java) (see Javadoc at [`StringOperand.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/2.0.3/org/refcodes/console/StringOperand.html))
* \[<span style="color:green">MODIFIED</span>\] [`StringOption.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-2.0.3/src/main/java/org/refcodes/console/StringOption.java) (see Javadoc at [`StringOption.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/2.0.3/org/refcodes/console/StringOption.html))
* \[<span style="color:green">MODIFIED</span>\] [`Syntaxable.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-2.0.3/src/main/java/org/refcodes/console/Syntaxable.java) (see Javadoc at [`Syntaxable.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/2.0.3/org/refcodes/console/Syntaxable.html))
* \[<span style="color:green">MODIFIED</span>\] [`SyntaxNotation.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-2.0.3/src/main/java/org/refcodes/console/SyntaxNotation.java) (see Javadoc at [`SyntaxNotation.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/2.0.3/org/refcodes/console/SyntaxNotation.html))
* \[<span style="color:green">MODIFIED</span>\] [`SyntaxUsage.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-2.0.3/src/main/java/org/refcodes/console/SyntaxUsage.java) (see Javadoc at [`SyntaxUsage.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/2.0.3/org/refcodes/console/SyntaxUsage.html))
* \[<span style="color:green">MODIFIED</span>\] [`XorCondition.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-2.0.3/src/main/java/org/refcodes/console/XorCondition.java) (see Javadoc at [`XorCondition.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/2.0.3/org/refcodes/console/XorCondition.html))
* \[<span style="color:green">MODIFIED</span>\] [`ArgsParserTest.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-2.0.3/src/test/java/org/refcodes/console/ArgsParserTest.java) 

## Change list &lt;refcodes-cli-ext&gt; (version 2.0.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-cli-ext/src/refcodes-cli-ext-2.0.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-cli-ext/src/refcodes-cli-ext-2.0.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-cli-ext/src/refcodes-cli-ext-2.0.3/refcodes-cli-ext-archetype/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-cli-ext/src/refcodes-cli-ext-2.0.3/refcodes-cli-ext-archetype/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`shell-exec.inc`](https://bitbucket.org/refcodes/refcodes-cli-ext/src/refcodes-cli-ext-2.0.3/refcodes-cli-ext-archetype/src/main/resources/archetype-resources/shell-exec.inc) 

## Change list &lt;refcodes-io&gt; (version 2.0.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-io/src/refcodes-io-2.0.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-io/src/refcodes-io-2.0.3/README.md) 

## Change list &lt;refcodes-codec&gt; (version 2.0.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-2.0.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-2.0.3/README.md) 

## Change list &lt;refcodes-component-ext&gt; (version 2.0.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-2.0.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-2.0.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-2.0.3/refcodes-component-ext-observer/pom.xml) 

## Change list &lt;refcodes-properties&gt; (version 2.0.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-2.0.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-2.0.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`AbstractPropertiesDecorator.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-2.0.3/src/main/java/org/refcodes/configuration/AbstractPropertiesDecorator.java) (see Javadoc at [`AbstractPropertiesDecorator.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties/2.0.3/org/refcodes/configuration/AbstractPropertiesDecorator.html))
* \[<span style="color:green">MODIFIED</span>\] [`package-info.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-2.0.3/src/main/java/org/refcodes/configuration/package-info.java) 
* \[<span style="color:green">MODIFIED</span>\] [`Properties.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-2.0.3/src/main/java/org/refcodes/configuration/Properties.java) (see Javadoc at [`Properties.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties/2.0.3/org/refcodes/configuration/Properties.html))
* \[<span style="color:green">MODIFIED</span>\] [`ResourceProperties.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-2.0.3/src/main/java/org/refcodes/configuration/ResourceProperties.java) (see Javadoc at [`ResourceProperties.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties/2.0.3/org/refcodes/configuration/ResourceProperties.html))
* \[<span style="color:green">MODIFIED</span>\] [`XmlPropertiesBuilder.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-2.0.3/src/main/java/org/refcodes/configuration/XmlPropertiesBuilder.java) (see Javadoc at [`XmlPropertiesBuilder.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties/2.0.3/org/refcodes/configuration/XmlPropertiesBuilder.html))
* \[<span style="color:green">MODIFIED</span>\] [`ConfigurationPropertiesTest.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-2.0.3/src/test/java/org/refcodes/configuration/ConfigurationPropertiesTest.java) 

## Change list &lt;refcodes-security&gt; (version 2.0.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security/src/refcodes-security-2.0.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security/src/refcodes-security-2.0.3/README.md) 

## Change list &lt;refcodes-security-alt&gt; (version 2.0.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-2.0.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-2.0.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-2.0.3/refcodes-security-alt-chaos/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-2.0.3/refcodes-security-alt-chaos/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`ChaosKeyImpl.java`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-2.0.3/refcodes-security-alt-chaos/src/main/java/org/refcodes/security/alt/chaos/ChaosKeyImpl.java) (see Javadoc at [`ChaosKeyImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-security-alt-chaos/2.0.3/org/refcodes/security/alt/chaos/ChaosKeyImpl.html))

## Change list &lt;refcodes-security-ext&gt; (version 2.0.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-2.0.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-2.0.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-2.0.3/refcodes-security-ext-chaos/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-2.0.3/refcodes-security-ext-chaos/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`ChaosProviderImpl.java`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-2.0.3/refcodes-security-ext-chaos/src/main/java/org/refcodes/security/ext/chaos/ChaosProviderImpl.java) (see Javadoc at [`ChaosProviderImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-security-ext-chaos/2.0.3/org/refcodes/security/ext/chaos/ChaosProviderImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`ChaosProviderTest.java`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-2.0.3/refcodes-security-ext-chaos/src/test/java/org/refcodes/security/ext/chaos/ChaosProviderTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-2.0.3/refcodes-security-ext-spring/pom.xml) 

## Change list &lt;refcodes-properties-ext&gt; (version 2.0.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-2.0.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-2.0.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-2.0.3/refcodes-properties-ext-cli/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-2.0.3/refcodes-properties-ext-cli/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`ArgsParserPropertiesImpl.java`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-2.0.3/refcodes-properties-ext-cli/src/main/java/org/refcodes/configuration/ext/console/ArgsParserPropertiesImpl.java) (see Javadoc at [`ArgsParserPropertiesImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties-ext-cli/2.0.3/org/refcodes/configuration/ext/console/ArgsParserPropertiesImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-2.0.3/refcodes-properties-ext-obfuscation/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-2.0.3/refcodes-properties-ext-obfuscation/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-2.0.3/refcodes-properties-ext-observer/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-2.0.3/refcodes-properties-ext-observer/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-2.0.3/refcodes-properties-ext-runtime/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-2.0.3/refcodes-properties-ext-runtime/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`RuntimePropertiesImpl.java`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-2.0.3/refcodes-properties-ext-runtime/src/main/java/org/refcodes/configuration/ext/runtime/RuntimePropertiesImpl.java) (see Javadoc at [`RuntimePropertiesImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties-ext-runtime/2.0.3/org/refcodes/configuration/ext/runtime/RuntimePropertiesImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`RuntimeProperties.java`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-2.0.3/refcodes-properties-ext-runtime/src/main/java/org/refcodes/configuration/ext/runtime/RuntimeProperties.java) (see Javadoc at [`RuntimeProperties.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties-ext-runtime/2.0.3/org/refcodes/configuration/ext/runtime/RuntimeProperties.html))

## Change list &lt;refcodes-logger&gt; (version 2.0.3)

* \[<span style="color:blue">ADDED</span>\] [`LogDecorator.java`](https://bitbucket.org/refcodes/refcodes-logger/src/refcodes-logger-2.0.3/src/main/java/org/refcodes/logger/LogDecorator.java) (see Javadoc at [`LogDecorator.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-logger/2.0.3/org/refcodes/logger/LogDecorator.html))
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger/src/refcodes-logger-2.0.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger/src/refcodes-logger-2.0.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`Logger.java`](https://bitbucket.org/refcodes/refcodes-logger/src/refcodes-logger-2.0.3/src/main/java/org/refcodes/logger/Logger.java) (see Javadoc at [`Logger.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-logger/2.0.3/org/refcodes/logger/Logger.html))
* \[<span style="color:green">MODIFIED</span>\] [`RuntimeLoggerImpl.java`](https://bitbucket.org/refcodes/refcodes-logger/src/refcodes-logger-2.0.3/src/main/java/org/refcodes/logger/RuntimeLoggerImpl.java) (see Javadoc at [`RuntimeLoggerImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-logger/2.0.3/org/refcodes/logger/RuntimeLoggerImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`RuntimeLogger.java`](https://bitbucket.org/refcodes/refcodes-logger/src/refcodes-logger-2.0.3/src/main/java/org/refcodes/logger/RuntimeLogger.java) (see Javadoc at [`RuntimeLogger.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-logger/2.0.3/org/refcodes/logger/RuntimeLogger.html))

## Change list &lt;refcodes-logger-alt&gt; (version 2.0.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-2.0.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-2.0.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-2.0.3/refcodes-logger-alt-async/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-2.0.3/refcodes-logger-alt-async/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-2.0.3/refcodes-logger-alt-console/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-2.0.3/refcodes-logger-alt-console/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`ConsoleLoggerHeaderImpl.java`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-2.0.3/refcodes-logger-alt-console/src/main/java/org/refcodes/logger/alt/console/ConsoleLoggerHeaderImpl.java) (see Javadoc at [`ConsoleLoggerHeaderImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-logger-alt-console/2.0.3/org/refcodes/logger/alt/console/ConsoleLoggerHeaderImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`ConsoleLoggerImpl.java`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-2.0.3/refcodes-logger-alt-console/src/main/java/org/refcodes/logger/alt/console/ConsoleLoggerImpl.java) (see Javadoc at [`ConsoleLoggerImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-logger-alt-console/2.0.3/org/refcodes/logger/alt/console/ConsoleLoggerImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`ConsoleLoggerSingleton.java`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-2.0.3/refcodes-logger-alt-console/src/main/java/org/refcodes/logger/alt/console/ConsoleLoggerSingleton.java) (see Javadoc at [`ConsoleLoggerSingleton.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-logger-alt-console/2.0.3/org/refcodes/logger/alt/console/ConsoleLoggerSingleton.html))
* \[<span style="color:green">MODIFIED</span>\] [`FormattedLoggerImpl.java`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-2.0.3/refcodes-logger-alt-console/src/main/java/org/refcodes/logger/alt/console/FormattedLoggerImpl.java) (see Javadoc at [`FormattedLoggerImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-logger-alt-console/2.0.3/org/refcodes/logger/alt/console/FormattedLoggerImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`ConsoleRuntimeLoggerTest.java`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-2.0.3/refcodes-logger-alt-console/src/test/java/org/refcodes/logger/alt/console/ConsoleRuntimeLoggerTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-2.0.3/refcodes-logger-alt-io/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-2.0.3/refcodes-logger-alt-io/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-2.0.3/refcodes-logger-alt-simpledb/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-2.0.3/refcodes-logger-alt-slf4j/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-2.0.3/refcodes-logger-alt-spring/pom.xml) 

## Change list &lt;refcodes-logger-ext&gt; (version 2.0.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-2.0.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-2.0.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-2.0.3/refcodes-logger-ext-slf4j/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-2.0.3/refcodes-logger-ext-slf4j/README.md) 

## Change list &lt;refcodes-graphical-ext&gt; (version 2.0.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-2.0.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-2.0.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-2.0.3/refcodes-graphical-ext-javafx/pom.xml) 

## Change list &lt;refcodes-checkerboard&gt; (version 2.0.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-2.0.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-2.0.3/README.md) 

## Change list &lt;refcodes-checkerboard-alt&gt; (version 2.0.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-alt/src/refcodes-checkerboard-alt-2.0.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-alt/src/refcodes-checkerboard-alt-2.0.3/refcodes-checkerboard-alt-javafx/pom.xml) 

## Change list &lt;refcodes-checkerboard-ext&gt; (version 2.0.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-ext/src/refcodes-checkerboard-ext-2.0.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-checkerboard-ext/src/refcodes-checkerboard-ext-2.0.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-ext/src/refcodes-checkerboard-ext-2.0.3/refcodes-checkerboard-ext-javafx-boulderdash/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-ext/src/refcodes-checkerboard-ext-2.0.3/refcodes-checkerboard-ext-javafx-chess/pom.xml) 

## Change list &lt;refcodes-boulderdash&gt; (version 2.0.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-boulderdash/src/refcodes-boulderdash-2.0.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-boulderdash/src/refcodes-boulderdash-2.0.3/README.md) 

## Change list &lt;refcodes-net&gt; (version 2.0.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-2.0.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`FormMediaTypeFactory.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-2.0.3/src/main/java/org/refcodes/net/FormMediaTypeFactory.java) (see Javadoc at [`FormMediaTypeFactory.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/2.0.3/org/refcodes/net/FormMediaTypeFactory.html))
* \[<span style="color:green">MODIFIED</span>\] [`HtmlMediaTypeFactory.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-2.0.3/src/main/java/org/refcodes/net/HtmlMediaTypeFactory.java) (see Javadoc at [`HtmlMediaTypeFactory.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/2.0.3/org/refcodes/net/HtmlMediaTypeFactory.html))
* \[<span style="color:green">MODIFIED</span>\] [`HttpClientResponseImpl.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-2.0.3/src/main/java/org/refcodes/net/HttpClientResponseImpl.java) (see Javadoc at [`HttpClientResponseImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/2.0.3/org/refcodes/net/HttpClientResponseImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`HttpClientResponse.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-2.0.3/src/main/java/org/refcodes/net/HttpClientResponse.java) (see Javadoc at [`HttpClientResponse.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/2.0.3/org/refcodes/net/HttpClientResponse.html))
* \[<span style="color:green">MODIFIED</span>\] [`JsonMediaTypeFactory.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-2.0.3/src/main/java/org/refcodes/net/JsonMediaTypeFactory.java) (see Javadoc at [`JsonMediaTypeFactory.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/2.0.3/org/refcodes/net/JsonMediaTypeFactory.html))
* \[<span style="color:green">MODIFIED</span>\] [`MediaTypeFactory.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-2.0.3/src/main/java/org/refcodes/net/MediaTypeFactory.java) (see Javadoc at [`MediaTypeFactory.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/2.0.3/org/refcodes/net/MediaTypeFactory.html))
* \[<span style="color:green">MODIFIED</span>\] [`MediaTypeParameter.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-2.0.3/src/main/java/org/refcodes/net/MediaTypeParameter.java) (see Javadoc at [`MediaTypeParameter.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/2.0.3/org/refcodes/net/MediaTypeParameter.html))
* \[<span style="color:green">MODIFIED</span>\] [`XmlMediaTypeFactory.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-2.0.3/src/main/java/org/refcodes/net/XmlMediaTypeFactory.java) (see Javadoc at [`XmlMediaTypeFactory.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/2.0.3/org/refcodes/net/XmlMediaTypeFactory.html))
* \[<span style="color:green">MODIFIED</span>\] [`YamlMediaTypeFactory.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-2.0.3/src/main/java/org/refcodes/net/YamlMediaTypeFactory.java) (see Javadoc at [`YamlMediaTypeFactory.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/2.0.3/org/refcodes/net/YamlMediaTypeFactory.html))
* \[<span style="color:green">MODIFIED</span>\] [`AbstractMediaFactoryTest.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-2.0.3/src/test/java/org/refcodes/net/AbstractMediaFactoryTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`HtmlMediaTypeFactoryTest.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-2.0.3/src/test/java/org/refcodes/net/HtmlMediaTypeFactoryTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`JsonMediaTypeFactoryTest.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-2.0.3/src/test/java/org/refcodes/net/JsonMediaTypeFactoryTest.java) 

## Change list &lt;refcodes-rest&gt; (version 2.0.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-2.0.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-2.0.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`HttpRestClientImpl.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-2.0.3/src/main/java/org/refcodes/rest/HttpRestClientImpl.java) (see Javadoc at [`HttpRestClientImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/2.0.3/org/refcodes/rest/HttpRestClientImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`RestRequestClient.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-2.0.3/src/main/java/org/refcodes/rest/RestRequestClient.java) (see Javadoc at [`RestRequestClient.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/2.0.3/org/refcodes/rest/RestRequestClient.html))
* \[<span style="color:green">MODIFIED</span>\] [`RestResponseEventImpl.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-2.0.3/src/main/java/org/refcodes/rest/RestResponseEventImpl.java) (see Javadoc at [`RestResponseEventImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/2.0.3/org/refcodes/rest/RestResponseEventImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`RestResponseImpl.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-2.0.3/src/main/java/org/refcodes/rest/RestResponseImpl.java) (see Javadoc at [`RestResponseImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/2.0.3/org/refcodes/rest/RestResponseImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`HttpRestClientTest.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-2.0.3/src/test/java/org/refcodes/rest/HttpRestClientTest.java) 

## Change list &lt;refcodes-hal&gt; (version 2.0.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-2.0.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-2.0.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`HalClientImpl.java`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-2.0.3/src/main/java/org/refcodes/hal/HalClientImpl.java) (see Javadoc at [`HalClientImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-hal/2.0.3/org/refcodes/hal/HalClientImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`HalData.java`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-2.0.3/src/main/java/org/refcodes/hal/HalData.java) (see Javadoc at [`HalData.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-hal/2.0.3/org/refcodes/hal/HalData.html))
* \[<span style="color:green">MODIFIED</span>\] [`HalMap.java`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-2.0.3/src/main/java/org/refcodes/hal/HalMap.java) (see Javadoc at [`HalMap.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-hal/2.0.3/org/refcodes/hal/HalMap.html))
* \[<span style="color:green">MODIFIED</span>\] [`AbstractHalClientTest.java`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-2.0.3/src/test/java/org/refcodes/hal/AbstractHalClientTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`HalClientTest.java`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-2.0.3/src/test/java/org/refcodes/hal/HalClientTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`TraversalModeTest.java`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-2.0.3/src/test/java/org/refcodes/hal/TraversalModeTest.java) 

## Change list &lt;refcodes-daemon&gt; (version 2.0.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-daemon/src/refcodes-daemon-2.0.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-daemon/src/refcodes-daemon-2.0.3/README.md) 

## Change list &lt;refcodes-eventbus&gt; (version 2.0.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-eventbus/src/refcodes-eventbus-2.0.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-eventbus/src/refcodes-eventbus-2.0.3/README.md) 

## Change list &lt;refcodes-eventbus-ext&gt; (version 2.0.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-2.0.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-2.0.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-2.0.3/refcodes-eventbus-ext-application/pom.xml) 

## Change list &lt;refcodes-filesystem&gt; (version 2.0.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-filesystem/src/refcodes-filesystem-2.0.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-filesystem/src/refcodes-filesystem-2.0.3/README.md) 

## Change list &lt;refcodes-filesystem-alt&gt; (version 2.0.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-filesystem-alt/src/refcodes-filesystem-alt-2.0.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-filesystem-alt/src/refcodes-filesystem-alt-2.0.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-filesystem-alt/src/refcodes-filesystem-alt-2.0.3/refcodes-filesystem-alt-s3/pom.xml) 

## Change list &lt;refcodes-forwardsecrecy&gt; (version 2.0.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy/src/refcodes-forwardsecrecy-2.0.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy/src/refcodes-forwardsecrecy-2.0.3/README.md) 

## Change list &lt;refcodes-forwardsecrecy-alt&gt; (version 2.0.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-2.0.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-2.0.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-2.0.3/refcodes-forwardsecrecy-alt-filesystem/pom.xml) 

## Change list &lt;refcodes-io-ext&gt; (version 2.0.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-2.0.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-2.0.3/refcodes-io-ext-observable/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-2.0.3/refcodes-io-ext-observable/README.md) 

## Change list &lt;refcodes-interceptor&gt; (version 2.0.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-interceptor/src/refcodes-interceptor-2.0.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-interceptor/src/refcodes-interceptor-2.0.3/README.md) 

## Change list &lt;refcodes-jobbus&gt; (version 2.0.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-jobbus/src/refcodes-jobbus-2.0.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-jobbus/src/refcodes-jobbus-2.0.3/README.md) 

## Change list &lt;refcodes-rest-ext&gt; (version 2.0.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-2.0.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-2.0.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-2.0.3/refcodes-rest-ext-archetype/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-2.0.3/refcodes-rest-ext-archetype/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`shell-exec.inc`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-2.0.3/refcodes-rest-ext-archetype/src/main/resources/archetype-resources/shell-exec.inc) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-2.0.3/refcodes-rest-ext-eureka/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-2.0.3/refcodes-rest-ext-eureka/README.md) 

## Change list &lt;refcodes-remoting&gt; (version 2.0.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-remoting/src/refcodes-remoting-2.0.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-remoting/src/refcodes-remoting-2.0.3/README.md) 

## Change list &lt;refcodes-remoting-ext&gt; (version 2.0.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-2.0.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-2.0.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-2.0.3/refcodes-remoting-ext-observer/pom.xml) 

## Change list &lt;refcodes-servicebus&gt; (version 2.0.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-servicebus/src/refcodes-servicebus-2.0.3/pom.xml) 

## Change list &lt;refcodes-servicebus-alt&gt; (version 2.0.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-servicebus-alt/src/refcodes-servicebus-alt-2.0.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-servicebus-alt/src/refcodes-servicebus-alt-2.0.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-servicebus-alt/src/refcodes-servicebus-alt-2.0.3/refcodes-servicebus-alt-spring/pom.xml) 

## Change list &lt;refcodes-tabular-alt&gt; (version 2.0.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-tabular-alt/src/refcodes-tabular-alt-2.0.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-tabular-alt/src/refcodes-tabular-alt-2.0.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-tabular-alt/src/refcodes-tabular-alt-2.0.3/refcodes-tabular-alt-forwardsecrecy/pom.xml) 
