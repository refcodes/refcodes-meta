# Change list for REFCODES.ORG artifacts' version 1.9.9

> This change list has been auto-generated on `triton` by `steiner` with with `changelist-all.sh` on the 2018-11-18 at 10:55:49.

## Change list &lt;refcodes-licensing&gt; (version 1.9.9)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-licensing/src/refcodes-licensing-1.9.9/pom.xml) 

## Change list &lt;refcodes-parent&gt; (version 1.9.9)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-parent/src/refcodes-parent-1.9.9/pom.xml) 

## Change list &lt;refcodes-time&gt; (version 1.9.9)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-time/src/refcodes-time-1.9.9/pom.xml) 

## Change list &lt;refcodes-mixin&gt; (version 1.9.9)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-mixin/src/refcodes-mixin-1.9.9/pom.xml) 

## Change list &lt;refcodes-data&gt; (version 1.9.9)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data/src/refcodes-data-1.9.9/pom.xml) 

## Change list &lt;refcodes-exception&gt; (version 1.9.9)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-exception/src/refcodes-exception-1.9.9/pom.xml) 

## Change list &lt;refcodes-factory&gt; (version 1.9.9)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-factory/src/refcodes-factory-1.9.9/pom.xml) 

## Change list &lt;refcodes-factory-alt&gt; (version 1.9.9)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-factory-alt/src/refcodes-factory-alt-1.9.9/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-factory-alt/src/refcodes-factory-alt-1.9.9/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-factory-alt/src/refcodes-factory-alt-1.9.9/refcodes-factory-alt-spring/pom.xml) 

## Change list &lt;refcodes-controlflow&gt; (version 1.9.9)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-controlflow/src/refcodes-controlflow-1.9.9/pom.xml) 

## Change list &lt;refcodes-numerical&gt; (version 1.9.9)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-numerical/src/refcodes-numerical-1.9.9/pom.xml) 

## Change list &lt;refcodes-generator&gt; (version 1.9.9)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-generator/src/refcodes-generator-1.9.9/pom.xml) 

## Change list &lt;refcodes-matcher&gt; (version 1.9.9)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-matcher/src/refcodes-matcher-1.9.9/pom.xml) 

## Change list &lt;refcodes-structure&gt; (version 1.9.9)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-1.9.9/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-1.9.9/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`CanonicalMapBuilderImpl.java`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-1.9.9/src/main/java/org/refcodes/structure/CanonicalMapBuilderImpl.java) (see Javadoc at [`CanonicalMapBuilderImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-structure/1.9.9/org/refcodes/structure/CanonicalMapBuilderImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`CanonicalMapImpl.java`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-1.9.9/src/main/java/org/refcodes/structure/CanonicalMapImpl.java) (see Javadoc at [`CanonicalMapImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-structure/1.9.9/org/refcodes/structure/CanonicalMapImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`CanonicalMap.java`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-1.9.9/src/main/java/org/refcodes/structure/CanonicalMap.java) (see Javadoc at [`CanonicalMap.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-structure/1.9.9/org/refcodes/structure/CanonicalMap.html))
* \[<span style="color:green">MODIFIED</span>\] [`PathMapBuilderImpl.java`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-1.9.9/src/main/java/org/refcodes/structure/PathMapBuilderImpl.java) (see Javadoc at [`PathMapBuilderImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-structure/1.9.9/org/refcodes/structure/PathMapBuilderImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`PathMapImpl.java`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-1.9.9/src/main/java/org/refcodes/structure/PathMapImpl.java) (see Javadoc at [`PathMapImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-structure/1.9.9/org/refcodes/structure/PathMapImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`PathMap.java`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-1.9.9/src/main/java/org/refcodes/structure/PathMap.java) (see Javadoc at [`PathMap.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-structure/1.9.9/org/refcodes/structure/PathMap.html))
* \[<span style="color:green">MODIFIED</span>\] [`PropertiesAccessorMixin.java`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-1.9.9/src/main/java/org/refcodes/structure/PropertiesAccessorMixin.java) (see Javadoc at [`PropertiesAccessorMixin.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-structure/1.9.9/org/refcodes/structure/PropertiesAccessorMixin.html))
* \[<span style="color:green">MODIFIED</span>\] [`StructureUtility.java`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-1.9.9/src/main/java/org/refcodes/structure/StructureUtility.java) (see Javadoc at [`StructureUtility.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-structure/1.9.9/org/refcodes/structure/StructureUtility.html))
* \[<span style="color:green">MODIFIED</span>\] [`PathMapTest.java`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-1.9.9/src/test/java/org/refcodes/structure/PathMapTest.java) 

## Change list &lt;refcodes-runtime&gt; (version 1.9.9)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-1.9.9/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-1.9.9/README.md) 

## Change list &lt;refcodes-component&gt; (version 1.9.9)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-1.9.9/pom.xml) 

## Change list &lt;refcodes-data-ext&gt; (version 1.9.9)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.9.9/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.9.9/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.9.9/refcodes-data-ext-boulderdash/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.9.9/refcodes-data-ext-checkers/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.9.9/refcodes-data-ext-checkers/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.9.9/refcodes-data-ext-chess/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.9.9/refcodes-data-ext-chess/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.9.9/refcodes-data-ext-corporate/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.9.9/refcodes-data-ext-symbols/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.9.9/refcodes-data-ext-symbols/README.md) 

## Change list &lt;refcodes-graphical&gt; (version 1.9.9)

* \[<span style="color:blue">ADDED</span>\] [`LayoutModeAccessor.java`](https://bitbucket.org/refcodes/refcodes-graphical/src/refcodes-graphical-1.9.9/src/main/java/org/refcodes/graphical/LayoutModeAccessor.java) (see Javadoc at [`LayoutModeAccessor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-graphical/1.9.9/org/refcodes/graphical/LayoutModeAccessor.html))
* \[<span style="color:blue">ADDED</span>\] [`LayoutMode.java`](https://bitbucket.org/refcodes/refcodes-graphical/src/refcodes-graphical-1.9.9/src/main/java/org/refcodes/graphical/LayoutMode.java) (see Javadoc at [`LayoutMode.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-graphical/1.9.9/org/refcodes/graphical/LayoutMode.html))
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-graphical/src/refcodes-graphical-1.9.9/pom.xml) 

## Change list &lt;refcodes-textual&gt; (version 1.9.9)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-1.9.9/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`CsvBuilderImpl.java`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-1.9.9/src/main/java/org/refcodes/textual/CsvBuilderImpl.java) (see Javadoc at [`CsvBuilderImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-textual/1.9.9/org/refcodes/textual/CsvBuilderImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`CsvMixin.java`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-1.9.9/src/main/java/org/refcodes/textual/CsvMixin.java) (see Javadoc at [`CsvMixin.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-textual/1.9.9/org/refcodes/textual/CsvMixin.html))
* \[<span style="color:green">MODIFIED</span>\] [`TableBuilderImpl.java`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-1.9.9/src/main/java/org/refcodes/textual/TableBuilderImpl.java) (see Javadoc at [`TableBuilderImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-textual/1.9.9/org/refcodes/textual/TableBuilderImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`VertAlignTextBuilderImpl.java`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-1.9.9/src/main/java/org/refcodes/textual/VertAlignTextBuilderImpl.java) (see Javadoc at [`VertAlignTextBuilderImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-textual/1.9.9/org/refcodes/textual/VertAlignTextBuilderImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`VertAlignTextMode.java`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-1.9.9/src/main/java/org/refcodes/textual/VertAlignTextMode.java) (see Javadoc at [`VertAlignTextMode.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-textual/1.9.9/org/refcodes/textual/VertAlignTextMode.html))
* \[<span style="color:green">MODIFIED</span>\] [`CsvBuilderTest.java`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-1.9.9/src/test/java/org/refcodes/textual/CsvBuilderTest.java) 

## Change list &lt;refcodes-criteria&gt; (version 1.9.9)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-1.9.9/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-1.9.9/README.md) 

## Change list &lt;refcodes-tabular&gt; (version 1.9.9)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-tabular/src/refcodes-tabular-1.9.9/pom.xml) 

## Change list &lt;refcodes-observer&gt; (version 1.9.9)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-observer/src/refcodes-observer-1.9.9/pom.xml) 

## Change list &lt;refcodes-command&gt; (version 1.9.9)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-command/src/refcodes-command-1.9.9/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-command/src/refcodes-command-1.9.9/README.md) 

## Change list &lt;refcodes-cli&gt; (version 1.9.9)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-1.9.9/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-1.9.9/README.md) 

## Change list &lt;refcodes-io&gt; (version 1.9.9)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-io/src/refcodes-io-1.9.9/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-io/src/refcodes-io-1.9.9/README.md) 

## Change list &lt;refcodes-codec&gt; (version 1.9.9)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-1.9.9/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-1.9.9/README.md) 

## Change list &lt;refcodes-component-ext&gt; (version 1.9.9)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.9.9/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.9.9/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.9.9/refcodes-component-ext-observer/pom.xml) 

## Change list &lt;refcodes-properties&gt; (version 1.9.9)

* \[<span style="color:blue">ADDED</span>\] [`edge_case.ini`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.9.9/src/test/resources/edge_case.ini) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.9.9/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.9.9/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`AbstractPropertiesBuilderDecorator.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.9.9/src/main/java/org/refcodes/configuration/AbstractPropertiesBuilderDecorator.java) (see Javadoc at [`AbstractPropertiesBuilderDecorator.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties/1.9.9/org/refcodes/configuration/AbstractPropertiesBuilderDecorator.html))
* \[<span style="color:green">MODIFIED</span>\] [`AbstractPropertiesDecorator.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.9.9/src/main/java/org/refcodes/configuration/AbstractPropertiesDecorator.java) (see Javadoc at [`AbstractPropertiesDecorator.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties/1.9.9/org/refcodes/configuration/AbstractPropertiesDecorator.html))
* \[<span style="color:green">MODIFIED</span>\] [`AbstractResourcePropertiesBuilderDecorator.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.9.9/src/main/java/org/refcodes/configuration/AbstractResourcePropertiesBuilderDecorator.java) (see Javadoc at [`AbstractResourcePropertiesBuilderDecorator.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties/1.9.9/org/refcodes/configuration/AbstractResourcePropertiesBuilderDecorator.html))
* \[<span style="color:green">MODIFIED</span>\] [`EnvironmentProperties.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.9.9/src/main/java/org/refcodes/configuration/EnvironmentProperties.java) (see Javadoc at [`EnvironmentProperties.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties/1.9.9/org/refcodes/configuration/EnvironmentProperties.html))
* \[<span style="color:green">MODIFIED</span>\] [`JavaProperties.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.9.9/src/main/java/org/refcodes/configuration/JavaProperties.java) (see Javadoc at [`JavaProperties.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties/1.9.9/org/refcodes/configuration/JavaProperties.html))
* \[<span style="color:green">MODIFIED</span>\] [`JsonProperties.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.9.9/src/main/java/org/refcodes/configuration/JsonProperties.java) (see Javadoc at [`JsonProperties.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties/1.9.9/org/refcodes/configuration/JsonProperties.html))
* \[<span style="color:green">MODIFIED</span>\] [`NormalizedPropertiesDecorator.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.9.9/src/main/java/org/refcodes/configuration/NormalizedPropertiesDecorator.java) (see Javadoc at [`NormalizedPropertiesDecorator.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties/1.9.9/org/refcodes/configuration/NormalizedPropertiesDecorator.html))
* \[<span style="color:green">MODIFIED</span>\] [`package-info.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.9.9/src/main/java/org/refcodes/configuration/package-info.java) 
* \[<span style="color:green">MODIFIED</span>\] [`ProfilePropertiesProjection.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.9.9/src/main/java/org/refcodes/configuration/ProfilePropertiesProjection.java) (see Javadoc at [`ProfilePropertiesProjection.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties/1.9.9/org/refcodes/configuration/ProfilePropertiesProjection.html))
* \[<span style="color:green">MODIFIED</span>\] [`PropertiesBuilderImpl.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.9.9/src/main/java/org/refcodes/configuration/PropertiesBuilderImpl.java) (see Javadoc at [`PropertiesBuilderImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties/1.9.9/org/refcodes/configuration/PropertiesBuilderImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`PropertiesImpl.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.9.9/src/main/java/org/refcodes/configuration/PropertiesImpl.java) (see Javadoc at [`PropertiesImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties/1.9.9/org/refcodes/configuration/PropertiesImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`Properties.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.9.9/src/main/java/org/refcodes/configuration/Properties.java) (see Javadoc at [`Properties.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties/1.9.9/org/refcodes/configuration/Properties.html))
* \[<span style="color:green">MODIFIED</span>\] [`PropertiesPrecedenceComposite.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.9.9/src/main/java/org/refcodes/configuration/PropertiesPrecedenceComposite.java) (see Javadoc at [`PropertiesPrecedenceComposite.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties/1.9.9/org/refcodes/configuration/PropertiesPrecedenceComposite.html))
* \[<span style="color:green">MODIFIED</span>\] [`SystemProperties.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.9.9/src/main/java/org/refcodes/configuration/SystemProperties.java) (see Javadoc at [`SystemProperties.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties/1.9.9/org/refcodes/configuration/SystemProperties.html))
* \[<span style="color:green">MODIFIED</span>\] [`TomlPropertiesBuilder.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.9.9/src/main/java/org/refcodes/configuration/TomlPropertiesBuilder.java) (see Javadoc at [`TomlPropertiesBuilder.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties/1.9.9/org/refcodes/configuration/TomlPropertiesBuilder.html))
* \[<span style="color:green">MODIFIED</span>\] [`TomlProperties.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.9.9/src/main/java/org/refcodes/configuration/TomlProperties.java) (see Javadoc at [`TomlProperties.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties/1.9.9/org/refcodes/configuration/TomlProperties.html))
* \[<span style="color:green">MODIFIED</span>\] [`XmlPropertiesBuilder.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.9.9/src/main/java/org/refcodes/configuration/XmlPropertiesBuilder.java) (see Javadoc at [`XmlPropertiesBuilder.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties/1.9.9/org/refcodes/configuration/XmlPropertiesBuilder.html))
* \[<span style="color:green">MODIFIED</span>\] [`XmlProperties.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.9.9/src/main/java/org/refcodes/configuration/XmlProperties.java) (see Javadoc at [`XmlProperties.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties/1.9.9/org/refcodes/configuration/XmlProperties.html))
* \[<span style="color:green">MODIFIED</span>\] [`YamlProperties.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.9.9/src/main/java/org/refcodes/configuration/YamlProperties.java) (see Javadoc at [`YamlProperties.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties/1.9.9/org/refcodes/configuration/YamlProperties.html))
* \[<span style="color:green">MODIFIED</span>\] [`AbstractResourcePropertiesTest.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.9.9/src/test/java/org/refcodes/configuration/AbstractResourcePropertiesTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`ConfigurationPropertiesTest.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.9.9/src/test/java/org/refcodes/configuration/ConfigurationPropertiesTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`IniPropertiesTest.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.9.9/src/test/java/org/refcodes/configuration/IniPropertiesTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`PropertiesTest.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.9.9/src/test/java/org/refcodes/configuration/PropertiesTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`configuration.ini`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.9.9/src/test/resources/configuration.ini) 

## Change list &lt;refcodes-security&gt; (version 1.9.9)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security/src/refcodes-security-1.9.9/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security/src/refcodes-security-1.9.9/README.md) 

## Change list &lt;refcodes-security-alt&gt; (version 1.9.9)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-1.9.9/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-1.9.9/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-1.9.9/refcodes-security-alt-chaos/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-1.9.9/refcodes-security-alt-chaos/README.md) 

## Change list &lt;refcodes-security-ext&gt; (version 1.9.9)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-1.9.9/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-1.9.9/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-1.9.9/refcodes-security-ext-chaos/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-1.9.9/refcodes-security-ext-chaos/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-1.9.9/refcodes-security-ext-spring/pom.xml) 

## Change list &lt;refcodes-properties-ext&gt; (version 1.9.9)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.9.9/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.9.9/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.9.9/refcodes-properties-ext-cli/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.9.9/refcodes-properties-ext-cli/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`ArgsParserPropertiesImpl.java`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.9.9/refcodes-properties-ext-cli/src/main/java/org/refcodes/configuration/ext/console/ArgsParserPropertiesImpl.java) (see Javadoc at [`ArgsParserPropertiesImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties-ext-cli/1.9.9/org/refcodes/configuration/ext/console/ArgsParserPropertiesImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.9.9/refcodes-properties-ext-obfuscation/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.9.9/refcodes-properties-ext-obfuscation/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`AbstractObfuscationPropertiesBuilderDecorator.java`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.9.9/refcodes-properties-ext-obfuscation/src/main/java/org/refcodes/configuration/ext/obfuscation/AbstractObfuscationPropertiesBuilderDecorator.java) (see Javadoc at [`AbstractObfuscationPropertiesBuilderDecorator.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties-ext-obfuscation/1.9.9/org/refcodes/configuration/ext/obfuscation/AbstractObfuscationPropertiesBuilderDecorator.html))
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.9.9/refcodes-properties-ext-observer/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.9.9/refcodes-properties-ext-observer/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`AbstractObservablePropertiesBuilderDecorator.java`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.9.9/refcodes-properties-ext-observer/src/main/java/org/refcodes/configuration/ext/observer/AbstractObservablePropertiesBuilderDecorator.java) (see Javadoc at [`AbstractObservablePropertiesBuilderDecorator.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties-ext-observer/1.9.9/org/refcodes/configuration/ext/observer/AbstractObservablePropertiesBuilderDecorator.html))
* \[<span style="color:green">MODIFIED</span>\] [`AbstractObservableResourcePropertiesBuilderDecorator.java`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.9.9/refcodes-properties-ext-observer/src/main/java/org/refcodes/configuration/ext/observer/AbstractObservableResourcePropertiesBuilderDecorator.java) (see Javadoc at [`AbstractObservableResourcePropertiesBuilderDecorator.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties-ext-observer/1.9.9/org/refcodes/configuration/ext/observer/AbstractObservableResourcePropertiesBuilderDecorator.html))
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.9.9/refcodes-properties-ext-runtime/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.9.9/refcodes-properties-ext-runtime/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`RuntimePropertiesImpl.java`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.9.9/refcodes-properties-ext-runtime/src/main/java/org/refcodes/configuration/ext/runtime/RuntimePropertiesImpl.java) (see Javadoc at [`RuntimePropertiesImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties-ext-runtime/1.9.9/org/refcodes/configuration/ext/runtime/RuntimePropertiesImpl.html))

## Change list &lt;refcodes-logger&gt; (version 1.9.9)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger/src/refcodes-logger-1.9.9/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger/src/refcodes-logger-1.9.9/README.md) 

## Change list &lt;refcodes-logger-alt&gt; (version 1.9.9)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.9.9/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.9.9/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.9.9/refcodes-logger-alt-async/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.9.9/refcodes-logger-alt-async/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.9.9/refcodes-logger-alt-console/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.9.9/refcodes-logger-alt-console/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.9.9/refcodes-logger-alt-io/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.9.9/refcodes-logger-alt-io/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.9.9/refcodes-logger-alt-simpledb/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.9.9/refcodes-logger-alt-slf4j/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`Slf4jLogger.java`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.9.9/refcodes-logger-alt-slf4j/src/main/java/org/refcodes/logger/alt/slf4j/Slf4jLogger.java) (see Javadoc at [`Slf4jLogger.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-logger-alt-slf4j/1.9.9/org/refcodes/logger/alt/slf4j/Slf4jLogger.html))
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.9.9/refcodes-logger-alt-spring/pom.xml) 

## Change list &lt;refcodes-logger-ext&gt; (version 1.9.9)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-1.9.9/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-1.9.9/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-1.9.9/refcodes-logger-ext-slf4j/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-1.9.9/refcodes-logger-ext-slf4j/README.md) 

## Change list &lt;refcodes-graphical-ext&gt; (version 1.9.9)

* \[<span style="color:blue">ADDED</span>\] [`FxCellBuilderImpl.java`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-1.9.9/refcodes-graphical-ext-javafx/src/main/java/org/refcodes/graphical/ext/javafx/FxCellBuilderImpl.java) (see Javadoc at [`FxCellBuilderImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-graphical-ext-javafx/1.9.9/org/refcodes/graphical/ext/javafx/FxCellBuilderImpl.html))
* \[<span style="color:blue">ADDED</span>\] [`FxCellBuilder.java`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-1.9.9/refcodes-graphical-ext-javafx/src/main/java/org/refcodes/graphical/ext/javafx/FxCellBuilder.java) (see Javadoc at [`FxCellBuilder.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-graphical-ext-javafx/1.9.9/org/refcodes/graphical/ext/javafx/FxCellBuilder.html))
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-1.9.9/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-1.9.9/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-1.9.9/refcodes-graphical-ext-javafx/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`FxGraphicalUtility.java`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-1.9.9/refcodes-graphical-ext-javafx/src/main/java/org/refcodes/graphical/ext/javafx/FxGraphicalUtility.java) (see Javadoc at [`FxGraphicalUtility.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-graphical-ext-javafx/1.9.9/org/refcodes/graphical/ext/javafx/FxGraphicalUtility.html))

## Change list &lt;refcodes-checkerboard&gt; (version 1.9.9)

* \[<span style="color:blue">ADDED</span>\] [`AbstractCheckerboard.java`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-1.9.9/src/main/java/org/refcodes/checkerboard/AbstractCheckerboard.java) (see Javadoc at [`AbstractCheckerboard.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-checkerboard/1.9.9/org/refcodes/checkerboard/AbstractCheckerboard.html))
* \[<span style="color:blue">ADDED</span>\] [`ConsoleCheckerboardViewerImpl.java`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-1.9.9/src/main/java/org/refcodes/checkerboard/ConsoleCheckerboardViewerImpl.java) (see Javadoc at [`ConsoleCheckerboardViewerImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-checkerboard/1.9.9/org/refcodes/checkerboard/ConsoleCheckerboardViewerImpl.html))
* \[<span style="color:blue">ADDED</span>\] [`ConsoleCheckerboardViewer.java`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-1.9.9/src/main/java/org/refcodes/checkerboard/ConsoleCheckerboardViewer.java) (see Javadoc at [`ConsoleCheckerboardViewer.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-checkerboard/1.9.9/org/refcodes/checkerboard/ConsoleCheckerboardViewer.html))
* \[<span style="color:blue">ADDED</span>\] [`ConsoleSpriteFactory.java`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-1.9.9/src/main/java/org/refcodes/checkerboard/ConsoleSpriteFactory.java) (see Javadoc at [`ConsoleSpriteFactory.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-checkerboard/1.9.9/org/refcodes/checkerboard/ConsoleSpriteFactory.html))
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-1.9.9/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-1.9.9/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`AbstractCheckerboardViewer.java`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-1.9.9/src/main/java/org/refcodes/checkerboard/AbstractCheckerboardViewer.java) (see Javadoc at [`AbstractCheckerboardViewer.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-checkerboard/1.9.9/org/refcodes/checkerboard/AbstractCheckerboardViewer.html))
* \[<span style="color:green">MODIFIED</span>\] [`AbstractGraphicalCheckerboardViewer.java`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-1.9.9/src/main/java/org/refcodes/checkerboard/AbstractGraphicalCheckerboardViewer.java) (see Javadoc at [`AbstractGraphicalCheckerboardViewer.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-checkerboard/1.9.9/org/refcodes/checkerboard/AbstractGraphicalCheckerboardViewer.html))
* \[<span style="color:green">MODIFIED</span>\] [`AbstractPlayer.java`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-1.9.9/src/main/java/org/refcodes/checkerboard/AbstractPlayer.java) (see Javadoc at [`AbstractPlayer.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-checkerboard/1.9.9/org/refcodes/checkerboard/AbstractPlayer.html))
* \[<span style="color:green">MODIFIED</span>\] [`CheckerboardImpl.java`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-1.9.9/src/main/java/org/refcodes/checkerboard/CheckerboardImpl.java) (see Javadoc at [`CheckerboardImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-checkerboard/1.9.9/org/refcodes/checkerboard/CheckerboardImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`MooreNeighbourhood.java`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-1.9.9/src/main/java/org/refcodes/checkerboard/MooreNeighbourhood.java) (see Javadoc at [`MooreNeighbourhood.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-checkerboard/1.9.9/org/refcodes/checkerboard/MooreNeighbourhood.html))
* \[<span style="color:green">MODIFIED</span>\] [`Player.java`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-1.9.9/src/main/java/org/refcodes/checkerboard/Player.java) (see Javadoc at [`Player.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-checkerboard/1.9.9/org/refcodes/checkerboard/Player.html))
* \[<span style="color:green">MODIFIED</span>\] [`SpriteFactory.java`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-1.9.9/src/main/java/org/refcodes/checkerboard/SpriteFactory.java) (see Javadoc at [`SpriteFactory.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-checkerboard/1.9.9/org/refcodes/checkerboard/SpriteFactory.html))
* \[<span style="color:green">MODIFIED</span>\] [`StateChangedEventImpl.java`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-1.9.9/src/main/java/org/refcodes/checkerboard/StateChangedEventImpl.java) (see Javadoc at [`StateChangedEventImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-checkerboard/1.9.9/org/refcodes/checkerboard/StateChangedEventImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`StateChangedEvent.java`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-1.9.9/src/main/java/org/refcodes/checkerboard/StateChangedEvent.java) (see Javadoc at [`StateChangedEvent.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-checkerboard/1.9.9/org/refcodes/checkerboard/StateChangedEvent.html))

## Change list &lt;refcodes-checkerboard-alt&gt; (version 1.9.9)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-alt/src/refcodes-checkerboard-alt-1.9.9/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-alt/src/refcodes-checkerboard-alt-1.9.9/refcodes-checkerboard-alt-javafx/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`AbstractFxSpriteFactory.java`](https://bitbucket.org/refcodes/refcodes-checkerboard-alt/src/refcodes-checkerboard-alt-1.9.9/refcodes-checkerboard-alt-javafx/src/main/java/org/refcodes/checkerboard/alt/javafx/AbstractFxSpriteFactory.java) (see Javadoc at [`AbstractFxSpriteFactory.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-checkerboard-alt-javafx/1.9.9/org/refcodes/checkerboard/alt/javafx/AbstractFxSpriteFactory.html))
* \[<span style="color:green">MODIFIED</span>\] [`FxCheckerboardViewerImpl.java`](https://bitbucket.org/refcodes/refcodes-checkerboard-alt/src/refcodes-checkerboard-alt-1.9.9/refcodes-checkerboard-alt-javafx/src/main/java/org/refcodes/checkerboard/alt/javafx/FxCheckerboardViewerImpl.java) (see Javadoc at [`FxCheckerboardViewerImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-checkerboard-alt-javafx/1.9.9/org/refcodes/checkerboard/alt/javafx/FxCheckerboardViewerImpl.html))

## Change list &lt;refcodes-checkerboard-ext&gt; (version 1.9.9)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-ext/src/refcodes-checkerboard-ext-1.9.9/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-checkerboard-ext/src/refcodes-checkerboard-ext-1.9.9/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-ext/src/refcodes-checkerboard-ext-1.9.9/refcodes-checkerboard-ext-javafx-boulderdash/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`FxBoulderDashSpriteFactoryImpl.java`](https://bitbucket.org/refcodes/refcodes-checkerboard-ext/src/refcodes-checkerboard-ext-1.9.9/refcodes-checkerboard-ext-javafx-boulderdash/src/main/java/org/refcodes/checkerboard/ext/javafx/boulderdash/FxBoulderDashSpriteFactoryImpl.java) (see Javadoc at [`FxBoulderDashSpriteFactoryImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-checkerboard-ext-javafx-boulderdash/1.9.9/org/refcodes/checkerboard/ext/javafx/boulderdash/FxBoulderDashSpriteFactoryImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`BoulderDashDemo.java`](https://bitbucket.org/refcodes/refcodes-checkerboard-ext/src/refcodes-checkerboard-ext-1.9.9/refcodes-checkerboard-ext-javafx-boulderdash/src/test/java/org/refcodes/checkerboard/ext/javafx/boulderdash/BoulderDashDemo.java) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-ext/src/refcodes-checkerboard-ext-1.9.9/refcodes-checkerboard-ext-javafx-chess/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`FxChessmenFactoryImpl.java`](https://bitbucket.org/refcodes/refcodes-checkerboard-ext/src/refcodes-checkerboard-ext-1.9.9/refcodes-checkerboard-ext-javafx-chess/src/main/java/org/refcodes/checkerboard/ext/javafx/chess/FxChessmenFactoryImpl.java) (see Javadoc at [`FxChessmenFactoryImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-checkerboard-ext-javafx-chess/1.9.9/org/refcodes/checkerboard/ext/javafx/chess/FxChessmenFactoryImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`ChessDemo.java`](https://bitbucket.org/refcodes/refcodes-checkerboard-ext/src/refcodes-checkerboard-ext-1.9.9/refcodes-checkerboard-ext-javafx-chess/src/test/java/org/refcodes/checkerboard/ext/javafx/chess/ChessDemo.java) 

## Change list &lt;refcodes-boulderdash&gt; (version 1.9.9)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-boulderdash/src/refcodes-boulderdash-1.9.9/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-boulderdash/src/refcodes-boulderdash-1.9.9/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`Amoeba.java`](https://bitbucket.org/refcodes/refcodes-boulderdash/src/refcodes-boulderdash-1.9.9/src/main/java/org/refcodes/boulderdash/Amoeba.java) (see Javadoc at [`Amoeba.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-boulderdash/1.9.9/org/refcodes/boulderdash/Amoeba.html))
* \[<span style="color:green">MODIFIED</span>\] [`BoulderDashAutomaton.java`](https://bitbucket.org/refcodes/refcodes-boulderdash/src/refcodes-boulderdash-1.9.9/src/main/java/org/refcodes/boulderdash/BoulderDashAutomaton.java) (see Javadoc at [`BoulderDashAutomaton.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-boulderdash/1.9.9/org/refcodes/boulderdash/BoulderDashAutomaton.html))
* \[<span style="color:green">MODIFIED</span>\] [`Boulder.java`](https://bitbucket.org/refcodes/refcodes-boulderdash/src/refcodes-boulderdash-1.9.9/src/main/java/org/refcodes/boulderdash/Boulder.java) (see Javadoc at [`Boulder.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-boulderdash/1.9.9/org/refcodes/boulderdash/Boulder.html))
* \[<span style="color:green">MODIFIED</span>\] [`BrickWall.java`](https://bitbucket.org/refcodes/refcodes-boulderdash/src/refcodes-boulderdash-1.9.9/src/main/java/org/refcodes/boulderdash/BrickWall.java) (see Javadoc at [`BrickWall.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-boulderdash/1.9.9/org/refcodes/boulderdash/BrickWall.html))
* \[<span style="color:green">MODIFIED</span>\] [`Butterfly.java`](https://bitbucket.org/refcodes/refcodes-boulderdash/src/refcodes-boulderdash-1.9.9/src/main/java/org/refcodes/boulderdash/Butterfly.java) (see Javadoc at [`Butterfly.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-boulderdash/1.9.9/org/refcodes/boulderdash/Butterfly.html))
* \[<span style="color:green">MODIFIED</span>\] [`Diamond.java`](https://bitbucket.org/refcodes/refcodes-boulderdash/src/refcodes-boulderdash-1.9.9/src/main/java/org/refcodes/boulderdash/Diamond.java) (see Javadoc at [`Diamond.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-boulderdash/1.9.9/org/refcodes/boulderdash/Diamond.html))
* \[<span style="color:green">MODIFIED</span>\] [`Dirt.java`](https://bitbucket.org/refcodes/refcodes-boulderdash/src/refcodes-boulderdash-1.9.9/src/main/java/org/refcodes/boulderdash/Dirt.java) (see Javadoc at [`Dirt.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-boulderdash/1.9.9/org/refcodes/boulderdash/Dirt.html))
* \[<span style="color:green">MODIFIED</span>\] [`ExpandingWall.java`](https://bitbucket.org/refcodes/refcodes-boulderdash/src/refcodes-boulderdash-1.9.9/src/main/java/org/refcodes/boulderdash/ExpandingWall.java) (see Javadoc at [`ExpandingWall.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-boulderdash/1.9.9/org/refcodes/boulderdash/ExpandingWall.html))
* \[<span style="color:green">MODIFIED</span>\] [`Firefly.java`](https://bitbucket.org/refcodes/refcodes-boulderdash/src/refcodes-boulderdash-1.9.9/src/main/java/org/refcodes/boulderdash/Firefly.java) (see Javadoc at [`Firefly.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-boulderdash/1.9.9/org/refcodes/boulderdash/Firefly.html))
* \[<span style="color:green">MODIFIED</span>\] [`MagicWall.java`](https://bitbucket.org/refcodes/refcodes-boulderdash/src/refcodes-boulderdash-1.9.9/src/main/java/org/refcodes/boulderdash/MagicWall.java) (see Javadoc at [`MagicWall.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-boulderdash/1.9.9/org/refcodes/boulderdash/MagicWall.html))
* \[<span style="color:green">MODIFIED</span>\] [`Rockford.java`](https://bitbucket.org/refcodes/refcodes-boulderdash/src/refcodes-boulderdash-1.9.9/src/main/java/org/refcodes/boulderdash/Rockford.java) (see Javadoc at [`Rockford.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-boulderdash/1.9.9/org/refcodes/boulderdash/Rockford.html))
* \[<span style="color:green">MODIFIED</span>\] [`SteelWall.java`](https://bitbucket.org/refcodes/refcodes-boulderdash/src/refcodes-boulderdash-1.9.9/src/main/java/org/refcodes/boulderdash/SteelWall.java) (see Javadoc at [`SteelWall.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-boulderdash/1.9.9/org/refcodes/boulderdash/SteelWall.html))

## Change list &lt;refcodes-net&gt; (version 1.9.9)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.9.9/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`HttpBodyMapImpl.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.9.9/src/main/java/org/refcodes/net/HttpBodyMapImpl.java) (see Javadoc at [`HttpBodyMapImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/1.9.9/org/refcodes/net/HttpBodyMapImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`HttpBodyMap.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.9.9/src/main/java/org/refcodes/net/HttpBodyMap.java) (see Javadoc at [`HttpBodyMap.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/1.9.9/org/refcodes/net/HttpBodyMap.html))
* \[<span style="color:green">MODIFIED</span>\] [`JsonMediaTypeFactory.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.9.9/src/main/java/org/refcodes/net/JsonMediaTypeFactory.java) (see Javadoc at [`JsonMediaTypeFactory.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/1.9.9/org/refcodes/net/JsonMediaTypeFactory.html))
* \[<span style="color:green">MODIFIED</span>\] [`XmlMediaTypeFactory.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.9.9/src/main/java/org/refcodes/net/XmlMediaTypeFactory.java) (see Javadoc at [`XmlMediaTypeFactory.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/1.9.9/org/refcodes/net/XmlMediaTypeFactory.html))
* \[<span style="color:green">MODIFIED</span>\] [`YamlMediaTypeFactory.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.9.9/src/main/java/org/refcodes/net/YamlMediaTypeFactory.java) (see Javadoc at [`YamlMediaTypeFactory.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/1.9.9/org/refcodes/net/YamlMediaTypeFactory.html))

## Change list &lt;refcodes-rest&gt; (version 1.9.9)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.9.9/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.9.9/README.md) 

## Change list &lt;refcodes-daemon&gt; (version 1.9.9)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-daemon/src/refcodes-daemon-1.9.9/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-daemon/src/refcodes-daemon-1.9.9/README.md) 

## Change list &lt;refcodes-eventbus&gt; (version 1.9.9)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-eventbus/src/refcodes-eventbus-1.9.9/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-eventbus/src/refcodes-eventbus-1.9.9/README.md) 

## Change list &lt;refcodes-eventbus-ext&gt; (version 1.9.9)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-1.9.9/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-1.9.9/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-1.9.9/refcodes-eventbus-ext-application/pom.xml) 

## Change list &lt;refcodes-filesystem&gt; (version 1.9.9)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-filesystem/src/refcodes-filesystem-1.9.9/pom.xml) 

## Change list &lt;refcodes-filesystem-alt&gt; (version 1.9.9)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-filesystem-alt/src/refcodes-filesystem-alt-1.9.9/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-filesystem-alt/src/refcodes-filesystem-alt-1.9.9/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-filesystem-alt/src/refcodes-filesystem-alt-1.9.9/refcodes-filesystem-alt-s3/pom.xml) 

## Change list &lt;refcodes-forwardsecrecy&gt; (version 1.9.9)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy/src/refcodes-forwardsecrecy-1.9.9/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy/src/refcodes-forwardsecrecy-1.9.9/README.md) 

## Change list &lt;refcodes-forwardsecrecy-alt&gt; (version 1.9.9)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-1.9.9/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-1.9.9/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-1.9.9/refcodes-forwardsecrecy-alt-filesystem/pom.xml) 

## Change list &lt;refcodes-io-ext&gt; (version 1.9.9)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-1.9.9/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-1.9.9/refcodes-io-ext-observable/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-1.9.9/refcodes-io-ext-observable/README.md) 

## Change list &lt;refcodes-interceptor&gt; (version 1.9.9)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-interceptor/src/refcodes-interceptor-1.9.9/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-interceptor/src/refcodes-interceptor-1.9.9/README.md) 

## Change list &lt;refcodes-jobbus&gt; (version 1.9.9)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-jobbus/src/refcodes-jobbus-1.9.9/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-jobbus/src/refcodes-jobbus-1.9.9/README.md) 

## Change list &lt;refcodes-rest-ext&gt; (version 1.9.9)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-1.9.9/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-1.9.9/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-1.9.9/refcodes-rest-ext-eureka/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-1.9.9/refcodes-rest-ext-eureka/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`EurekaDiscoverySidecarImpl.java`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-1.9.9/refcodes-rest-ext-eureka/src/main/java/org/refcodes/rest/ext/eureka/EurekaDiscoverySidecarImpl.java) (see Javadoc at [`EurekaDiscoverySidecarImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest-ext-eureka/1.9.9/org/refcodes/rest/ext/eureka/EurekaDiscoverySidecarImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`EurekaInstanceDescriptor.java`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-1.9.9/refcodes-rest-ext-eureka/src/main/java/org/refcodes/rest/ext/eureka/EurekaInstanceDescriptor.java) (see Javadoc at [`EurekaInstanceDescriptor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest-ext-eureka/1.9.9/org/refcodes/rest/ext/eureka/EurekaInstanceDescriptor.html))
* \[<span style="color:green">MODIFIED</span>\] [`EurekaServerDescriptor.java`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-1.9.9/refcodes-rest-ext-eureka/src/main/java/org/refcodes/rest/ext/eureka/EurekaServerDescriptor.java) (see Javadoc at [`EurekaServerDescriptor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest-ext-eureka/1.9.9/org/refcodes/rest/ext/eureka/EurekaServerDescriptor.html))

## Change list &lt;refcodes-remoting&gt; (version 1.9.9)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-remoting/src/refcodes-remoting-1.9.9/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-remoting/src/refcodes-remoting-1.9.9/README.md) 

## Change list &lt;refcodes-remoting-ext&gt; (version 1.9.9)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-1.9.9/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-1.9.9/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-1.9.9/refcodes-remoting-ext-observer/pom.xml) 

## Change list &lt;refcodes-servicebus&gt; (version 1.9.9)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-servicebus/src/refcodes-servicebus-1.9.9/pom.xml) 

## Change list &lt;refcodes-servicebus-alt&gt; (version 1.9.9)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-servicebus-alt/src/refcodes-servicebus-alt-1.9.9/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-servicebus-alt/src/refcodes-servicebus-alt-1.9.9/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-servicebus-alt/src/refcodes-servicebus-alt-1.9.9/refcodes-servicebus-alt-spring/pom.xml) 

## Change list &lt;refcodes-tabular-alt&gt; (version 1.9.9)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-tabular-alt/src/refcodes-tabular-alt-1.9.9/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-tabular-alt/src/refcodes-tabular-alt-1.9.9/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-tabular-alt/src/refcodes-tabular-alt-1.9.9/refcodes-tabular-alt-forwardsecrecy/pom.xml) 
