# Change list for REFCODES.ORG artifacts' version 2.0.4

> This change list has been auto-generated on `triton.local` by `steiner` with `changelist-all.sh` on the 2019-07-21 at 14:09:42.

## Change list &lt;refcodes-licensing&gt; (version 2.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-licensing/src/refcodes-licensing-2.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-licensing/src/refcodes-licensing-2.0.4/README.md) 

## Change list &lt;refcodes-parent&gt; (version 2.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-parent/src/refcodes-parent-2.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-parent/src/refcodes-parent-2.0.4/README.md) 

## Change list &lt;refcodes-time&gt; (version 2.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-time/src/refcodes-time-2.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-time/src/refcodes-time-2.0.4/README.md) 

## Change list &lt;refcodes-mixin&gt; (version 2.0.4)

* \[<span style="color:blue">ADDED</span>\] [`BoxGrid.java`](https://bitbucket.org/refcodes/refcodes-mixin/src/refcodes-mixin-2.0.4/src/main/java/org/refcodes/mixin/BoxGrid.java) (see Javadoc at [`BoxGrid.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-mixin/2.0.4/org/refcodes/mixin/BoxGrid.html))
* \[<span style="color:blue">ADDED</span>\] [`PatternAccessor.java`](https://bitbucket.org/refcodes/refcodes-mixin/src/refcodes-mixin-2.0.4/src/main/java/org/refcodes/mixin/PatternAccessor.java) (see Javadoc at [`PatternAccessor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-mixin/2.0.4/org/refcodes/mixin/PatternAccessor.html))
* \[<span style="color:red">DELETED</span>\] `BottomDividerEdgeAccessor.java`
* \[<span style="color:red">DELETED</span>\] `BottomLeftEdgeAccessor.java`
* \[<span style="color:red">DELETED</span>\] `BottomRightEdgeAccessor.java`
* \[<span style="color:red">DELETED</span>\] `TableBody.java`
* \[<span style="color:red">DELETED</span>\] `TableHeaderImpl.java`
* \[<span style="color:red">DELETED</span>\] `TableHeader.java`
* \[<span style="color:red">DELETED</span>\] `TableTailImpl.java`
* \[<span style="color:red">DELETED</span>\] `TableTail.java`
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-mixin/src/refcodes-mixin-2.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-mixin/src/refcodes-mixin-2.0.4/README.md) 

## Change list &lt;refcodes-data&gt; (version 2.0.4)

* \[<span style="color:red">DELETED</span>\] `.~lock.ANSI`
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data/src/refcodes-data-2.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data/src/refcodes-data-2.0.4/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`AnsiEscapeCode.java`](https://bitbucket.org/refcodes/refcodes-data/src/refcodes-data-2.0.4/src/main/java/org/refcodes/data/AnsiEscapeCode.java) (see Javadoc at [`AnsiEscapeCode.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-data/2.0.4/org/refcodes/data/AnsiEscapeCode.html))
* \[<span style="color:green">MODIFIED</span>\] [`EnvironmentVariable.java`](https://bitbucket.org/refcodes/refcodes-data/src/refcodes-data-2.0.4/src/main/java/org/refcodes/data/EnvironmentVariable.java) (see Javadoc at [`EnvironmentVariable.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-data/2.0.4/org/refcodes/data/EnvironmentVariable.html))

## Change list &lt;refcodes-exception&gt; (version 2.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-exception/src/refcodes-exception-2.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-exception/src/refcodes-exception-2.0.4/README.md) 

## Change list &lt;refcodes-factory&gt; (version 2.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-factory/src/refcodes-factory-2.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-factory/src/refcodes-factory-2.0.4/README.md) 

## Change list &lt;refcodes-factory-alt&gt; (version 2.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-factory-alt/src/refcodes-factory-alt-2.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-factory-alt/src/refcodes-factory-alt-2.0.4/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-factory-alt/src/refcodes-factory-alt-2.0.4/refcodes-factory-alt-spring/pom.xml) 

## Change list &lt;refcodes-controlflow&gt; (version 2.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-controlflow/src/refcodes-controlflow-2.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-controlflow/src/refcodes-controlflow-2.0.4/README.md) 

## Change list &lt;refcodes-numerical&gt; (version 2.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-numerical/src/refcodes-numerical-2.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-numerical/src/refcodes-numerical-2.0.4/README.md) 

## Change list &lt;refcodes-generator&gt; (version 2.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-generator/src/refcodes-generator-2.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-generator/src/refcodes-generator-2.0.4/README.md) 

## Change list &lt;refcodes-matcher&gt; (version 2.0.4)

* \[<span style="color:blue">ADDED</span>\] [`RegExpMatcherImpl.java`](https://bitbucket.org/refcodes/refcodes-matcher/src/refcodes-matcher-2.0.4/src/main/java/org/refcodes/matcher/RegExpMatcherImpl.java) (see Javadoc at [`RegExpMatcherImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-matcher/2.0.4/org/refcodes/matcher/RegExpMatcherImpl.html))
* \[<span style="color:blue">ADDED</span>\] [`RegExpMatcher.java`](https://bitbucket.org/refcodes/refcodes-matcher/src/refcodes-matcher-2.0.4/src/main/java/org/refcodes/matcher/RegExpMatcher.java) (see Javadoc at [`RegExpMatcher.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-matcher/2.0.4/org/refcodes/matcher/RegExpMatcher.html))
* \[<span style="color:blue">ADDED</span>\] [`WildcardMatcher.java`](https://bitbucket.org/refcodes/refcodes-matcher/src/refcodes-matcher-2.0.4/src/main/java/org/refcodes/matcher/WildcardMatcher.java) (see Javadoc at [`WildcardMatcher.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-matcher/2.0.4/org/refcodes/matcher/WildcardMatcher.html))
* \[<span style="color:blue">ADDED</span>\] [`RegExpMatcherTest.java`](https://bitbucket.org/refcodes/refcodes-matcher/src/refcodes-matcher-2.0.4/src/test/java/org/refcodes/matcher/RegExpMatcherTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-matcher/src/refcodes-matcher-2.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-matcher/src/refcodes-matcher-2.0.4/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`PathMatcher.java`](https://bitbucket.org/refcodes/refcodes-matcher/src/refcodes-matcher-2.0.4/src/main/java/org/refcodes/matcher/PathMatcher.java) (see Javadoc at [`PathMatcher.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-matcher/2.0.4/org/refcodes/matcher/PathMatcher.html))

## Change list &lt;refcodes-structure&gt; (version 2.0.4)

* \[<span style="color:blue">ADDED</span>\] [`PathMapRegExpTest.java`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-2.0.4/src/test/java/org/refcodes/structure/PathMapRegExpTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-2.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-2.0.4/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`CanonicalMap.java`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-2.0.4/src/main/java/org/refcodes/structure/CanonicalMap.java) (see Javadoc at [`CanonicalMap.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-structure/2.0.4/org/refcodes/structure/CanonicalMap.html))
* \[<span style="color:green">MODIFIED</span>\] [`PathMap.java`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-2.0.4/src/main/java/org/refcodes/structure/PathMap.java) (see Javadoc at [`PathMap.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-structure/2.0.4/org/refcodes/structure/PathMap.html))
* \[<span style="color:green">MODIFIED</span>\] [`PathMapQueryTest.java`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-2.0.4/src/test/java/org/refcodes/structure/PathMapQueryTest.java) 

## Change list &lt;refcodes-structure-ext&gt; (version 2.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-structure-ext/src/refcodes-structure-ext-2.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-structure-ext/src/refcodes-structure-ext-2.0.4/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-structure-ext/src/refcodes-structure-ext-2.0.4/refcodes-structure-ext-factory/pom.xml) 

## Change list &lt;refcodes-runtime&gt; (version 2.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-2.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-2.0.4/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`module-info.java.off`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-2.0.4/src/main/java/module-info.java.off) 
* \[<span style="color:green">MODIFIED</span>\] [`ConfigLocator.java`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-2.0.4/src/main/java/org/refcodes/runtime/ConfigLocator.java) (see Javadoc at [`ConfigLocator.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-runtime/2.0.4/org/refcodes/runtime/ConfigLocator.html))
* \[<span style="color:green">MODIFIED</span>\] [`SystemContext.java`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-2.0.4/src/main/java/org/refcodes/runtime/SystemContext.java) (see Javadoc at [`SystemContext.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-runtime/2.0.4/org/refcodes/runtime/SystemContext.html))
* \[<span style="color:green">MODIFIED</span>\] [`Terminal.java`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-2.0.4/src/main/java/org/refcodes/runtime/Terminal.java) (see Javadoc at [`Terminal.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-runtime/2.0.4/org/refcodes/runtime/Terminal.html))

## Change list &lt;refcodes-component&gt; (version 2.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-2.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-2.0.4/README.md) 

## Change list &lt;refcodes-data-ext&gt; (version 2.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-2.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-2.0.4/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-2.0.4/refcodes-data-ext-boulderdash/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-2.0.4/refcodes-data-ext-boulderdash/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-2.0.4/refcodes-data-ext-checkers/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-2.0.4/refcodes-data-ext-checkers/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-2.0.4/refcodes-data-ext-chess/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-2.0.4/refcodes-data-ext-chess/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-2.0.4/refcodes-data-ext-corporate/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-2.0.4/refcodes-data-ext-corporate/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-2.0.4/refcodes-data-ext-symbols/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-2.0.4/refcodes-data-ext-symbols/README.md) 

## Change list &lt;refcodes-graphical&gt; (version 2.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-graphical/src/refcodes-graphical-2.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-graphical/src/refcodes-graphical-2.0.4/README.md) 

## Change list &lt;refcodes-textual&gt; (version 2.0.4)

* \[<span style="color:blue">ADDED</span>\] [`TextBoxGridAccessor.java`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-2.0.4/src/main/java/org/refcodes/textual/TextBoxGridAccessor.java) (see Javadoc at [`TextBoxGridAccessor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-textual/2.0.4/org/refcodes/textual/TextBoxGridAccessor.html))
* \[<span style="color:blue">ADDED</span>\] [`TextBoxGridImpl.java`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-2.0.4/src/main/java/org/refcodes/textual/TextBoxGridImpl.java) (see Javadoc at [`TextBoxGridImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-textual/2.0.4/org/refcodes/textual/TextBoxGridImpl.html))
* \[<span style="color:blue">ADDED</span>\] [`TextBoxGrid.java`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-2.0.4/src/main/java/org/refcodes/textual/TextBoxGrid.java) (see Javadoc at [`TextBoxGrid.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-textual/2.0.4/org/refcodes/textual/TextBoxGrid.html))
* \[<span style="color:blue">ADDED</span>\] [`TextBoxStyle.java`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-2.0.4/src/main/java/org/refcodes/textual/TextBoxStyle.java) (see Javadoc at [`TextBoxStyle.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-textual/2.0.4/org/refcodes/textual/TextBoxStyle.html))
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-2.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-2.0.4/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`AsciiArtBuilderImpl.java`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-2.0.4/src/main/java/org/refcodes/textual/AsciiArtBuilderImpl.java) (see Javadoc at [`AsciiArtBuilderImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-textual/2.0.4/org/refcodes/textual/AsciiArtBuilderImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`AsciiArtBuilder.java`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-2.0.4/src/main/java/org/refcodes/textual/AsciiArtBuilder.java) (see Javadoc at [`AsciiArtBuilder.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-textual/2.0.4/org/refcodes/textual/AsciiArtBuilder.html))
* \[<span style="color:green">MODIFIED</span>\] [`PixmapRatioMode.java`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-2.0.4/src/main/java/org/refcodes/textual/PixmapRatioMode.java) (see Javadoc at [`PixmapRatioMode.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-textual/2.0.4/org/refcodes/textual/PixmapRatioMode.html))
* \[<span style="color:green">MODIFIED</span>\] [`SecretHintBuilderImpl.java`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-2.0.4/src/main/java/org/refcodes/textual/SecretHintBuilderImpl.java) (see Javadoc at [`SecretHintBuilderImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-textual/2.0.4/org/refcodes/textual/SecretHintBuilderImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`SecretHintBuilder.java`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-2.0.4/src/main/java/org/refcodes/textual/SecretHintBuilder.java) (see Javadoc at [`SecretHintBuilder.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-textual/2.0.4/org/refcodes/textual/SecretHintBuilder.html))
* \[<span style="color:green">MODIFIED</span>\] [`TableBuilderImpl.java`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-2.0.4/src/main/java/org/refcodes/textual/TableBuilderImpl.java) (see Javadoc at [`TableBuilderImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-textual/2.0.4/org/refcodes/textual/TableBuilderImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`TableBuilder.java`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-2.0.4/src/main/java/org/refcodes/textual/TableBuilder.java) (see Javadoc at [`TableBuilder.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-textual/2.0.4/org/refcodes/textual/TableBuilder.html))
* \[<span style="color:green">MODIFIED</span>\] [`TableStyle.java`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-2.0.4/src/main/java/org/refcodes/textual/TableStyle.java) (see Javadoc at [`TableStyle.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-textual/2.0.4/org/refcodes/textual/TableStyle.html))
* \[<span style="color:green">MODIFIED</span>\] [`TextBorderBuilderImpl.java`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-2.0.4/src/main/java/org/refcodes/textual/TextBorderBuilderImpl.java) (see Javadoc at [`TextBorderBuilderImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-textual/2.0.4/org/refcodes/textual/TextBorderBuilderImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`TextBorderBuilder.java`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-2.0.4/src/main/java/org/refcodes/textual/TextBorderBuilder.java) (see Javadoc at [`TextBorderBuilder.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-textual/2.0.4/org/refcodes/textual/TextBorderBuilder.html))
* \[<span style="color:green">MODIFIED</span>\] [`VerboseTextBuilder.java`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-2.0.4/src/main/java/org/refcodes/textual/VerboseTextBuilder.java) (see Javadoc at [`VerboseTextBuilder.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-textual/2.0.4/org/refcodes/textual/VerboseTextBuilder.html))
* \[<span style="color:green">MODIFIED</span>\] [`TableBuilderTest.java`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-2.0.4/src/test/java/org/refcodes/textual/TableBuilderTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`TextBorderBuilderTest.java`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-2.0.4/src/test/java/org/refcodes/textual/TextBorderBuilderTest.java) 

## Change list &lt;refcodes-criteria&gt; (version 2.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-2.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-2.0.4/README.md) 

## Change list &lt;refcodes-tabular&gt; (version 2.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-tabular/src/refcodes-tabular-2.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-tabular/src/refcodes-tabular-2.0.4/README.md) 

## Change list &lt;refcodes-observer&gt; (version 2.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-observer/src/refcodes-observer-2.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-observer/src/refcodes-observer-2.0.4/README.md) 

## Change list &lt;refcodes-command&gt; (version 2.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-command/src/refcodes-command-2.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-command/src/refcodes-command-2.0.4/README.md) 

## Change list &lt;refcodes-cli&gt; (version 2.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-2.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-2.0.4/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`AndCondition.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-2.0.4/src/main/java/org/refcodes/console/AndCondition.java) (see Javadoc at [`AndCondition.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/2.0.4/org/refcodes/console/AndCondition.html))
* \[<span style="color:green">MODIFIED</span>\] [`ArgsParserImpl.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-2.0.4/src/main/java/org/refcodes/console/ArgsParserImpl.java) (see Javadoc at [`ArgsParserImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/2.0.4/org/refcodes/console/ArgsParserImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`ArgsParser.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-2.0.4/src/main/java/org/refcodes/console/ArgsParser.java) (see Javadoc at [`ArgsParser.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/2.0.4/org/refcodes/console/ArgsParser.html))
* \[<span style="color:green">MODIFIED</span>\] [`AnyCondition.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-2.0.4/src/main/java/org/refcodes/console/AnyCondition.java) (see Javadoc at [`AnyCondition.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/2.0.4/org/refcodes/console/AnyCondition.html))
* \[<span style="color:green">MODIFIED</span>\] [`OrCondition.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-2.0.4/src/main/java/org/refcodes/console/OrCondition.java) (see Javadoc at [`OrCondition.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/2.0.4/org/refcodes/console/OrCondition.html))
* \[<span style="color:green">MODIFIED</span>\] [`XorCondition.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-2.0.4/src/main/java/org/refcodes/console/XorCondition.java) (see Javadoc at [`XorCondition.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/2.0.4/org/refcodes/console/XorCondition.html))

## Change list &lt;refcodes-cli-ext&gt; (version 2.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-cli-ext/src/refcodes-cli-ext-2.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-cli-ext/src/refcodes-cli-ext-2.0.4/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-cli-ext/src/refcodes-cli-ext-2.0.4/refcodes-cli-ext-archetype/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-cli-ext/src/refcodes-cli-ext-2.0.4/refcodes-cli-ext-archetype/README.md) 

## Change list &lt;refcodes-io&gt; (version 2.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-io/src/refcodes-io-2.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-io/src/refcodes-io-2.0.4/README.md) 

## Change list &lt;refcodes-codec&gt; (version 2.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-2.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-2.0.4/README.md) 

## Change list &lt;refcodes-component-ext&gt; (version 2.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-2.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-2.0.4/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-2.0.4/refcodes-component-ext-observer/pom.xml) 

## Change list &lt;refcodes-properties&gt; (version 2.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-2.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-2.0.4/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`AbstractResourcePropertiesBuilder.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-2.0.4/src/main/java/org/refcodes/configuration/AbstractResourcePropertiesBuilder.java) (see Javadoc at [`AbstractResourcePropertiesBuilder.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties/2.0.4/org/refcodes/configuration/AbstractResourcePropertiesBuilder.html))
* \[<span style="color:green">MODIFIED</span>\] [`package-info.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-2.0.4/src/main/java/org/refcodes/configuration/package-info.java) 
* \[<span style="color:green">MODIFIED</span>\] [`PolyglotPropertiesBuilder.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-2.0.4/src/main/java/org/refcodes/configuration/PolyglotPropertiesBuilder.java) (see Javadoc at [`PolyglotPropertiesBuilder.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties/2.0.4/org/refcodes/configuration/PolyglotPropertiesBuilder.html))
* \[<span style="color:green">MODIFIED</span>\] [`PolyglotProperties.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-2.0.4/src/main/java/org/refcodes/configuration/PolyglotProperties.java) (see Javadoc at [`PolyglotProperties.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties/2.0.4/org/refcodes/configuration/PolyglotProperties.html))
* \[<span style="color:green">MODIFIED</span>\] [`Properties.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-2.0.4/src/main/java/org/refcodes/configuration/Properties.java) (see Javadoc at [`Properties.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties/2.0.4/org/refcodes/configuration/Properties.html))

## Change list &lt;refcodes-security&gt; (version 2.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security/src/refcodes-security-2.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security/src/refcodes-security-2.0.4/README.md) 

## Change list &lt;refcodes-security-alt&gt; (version 2.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-2.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-2.0.4/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-2.0.4/refcodes-security-alt-chaos/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-2.0.4/refcodes-security-alt-chaos/README.md) 

## Change list &lt;refcodes-security-ext&gt; (version 2.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-2.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-2.0.4/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-2.0.4/refcodes-security-ext-chaos/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-2.0.4/refcodes-security-ext-chaos/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-2.0.4/refcodes-security-ext-spring/pom.xml) 

## Change list &lt;refcodes-properties-ext&gt; (version 2.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-2.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-2.0.4/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-2.0.4/refcodes-properties-ext-cli/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-2.0.4/refcodes-properties-ext-cli/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-2.0.4/refcodes-properties-ext-obfuscation/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-2.0.4/refcodes-properties-ext-obfuscation/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-2.0.4/refcodes-properties-ext-observer/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-2.0.4/refcodes-properties-ext-observer/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-2.0.4/refcodes-properties-ext-runtime/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-2.0.4/refcodes-properties-ext-runtime/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`RuntimePropertiesImpl.java`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-2.0.4/refcodes-properties-ext-runtime/src/main/java/org/refcodes/configuration/ext/runtime/RuntimePropertiesImpl.java) (see Javadoc at [`RuntimePropertiesImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties-ext-runtime/2.0.4/org/refcodes/configuration/ext/runtime/RuntimePropertiesImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`RuntimeProperties.java`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-2.0.4/refcodes-properties-ext-runtime/src/main/java/org/refcodes/configuration/ext/runtime/RuntimeProperties.java) (see Javadoc at [`RuntimeProperties.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties-ext-runtime/2.0.4/org/refcodes/configuration/ext/runtime/RuntimeProperties.html))

## Change list &lt;refcodes-logger&gt; (version 2.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger/src/refcodes-logger-2.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger/src/refcodes-logger-2.0.4/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`LogDecorator.java`](https://bitbucket.org/refcodes/refcodes-logger/src/refcodes-logger-2.0.4/src/main/java/org/refcodes/logger/LogDecorator.java) (see Javadoc at [`LogDecorator.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-logger/2.0.4/org/refcodes/logger/LogDecorator.html))

## Change list &lt;refcodes-logger-alt&gt; (version 2.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-2.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-2.0.4/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-2.0.4/refcodes-logger-alt-async/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-2.0.4/refcodes-logger-alt-async/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-2.0.4/refcodes-logger-alt-console/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-2.0.4/refcodes-logger-alt-console/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`ConsoleLoggerHeaderImpl.java`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-2.0.4/refcodes-logger-alt-console/src/main/java/org/refcodes/logger/alt/console/ConsoleLoggerHeaderImpl.java) (see Javadoc at [`ConsoleLoggerHeaderImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-logger-alt-console/2.0.4/org/refcodes/logger/alt/console/ConsoleLoggerHeaderImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`ConsoleLoggerImpl.java`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-2.0.4/refcodes-logger-alt-console/src/main/java/org/refcodes/logger/alt/console/ConsoleLoggerImpl.java) (see Javadoc at [`ConsoleLoggerImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-logger-alt-console/2.0.4/org/refcodes/logger/alt/console/ConsoleLoggerImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`FormattedLoggerImpl.java`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-2.0.4/refcodes-logger-alt-console/src/main/java/org/refcodes/logger/alt/console/FormattedLoggerImpl.java) (see Javadoc at [`FormattedLoggerImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-logger-alt-console/2.0.4/org/refcodes/logger/alt/console/FormattedLoggerImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`FormattedLogger.java`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-2.0.4/refcodes-logger-alt-console/src/main/java/org/refcodes/logger/alt/console/FormattedLogger.java) (see Javadoc at [`FormattedLogger.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-logger-alt-console/2.0.4/org/refcodes/logger/alt/console/FormattedLogger.html))
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-2.0.4/refcodes-logger-alt-io/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-2.0.4/refcodes-logger-alt-io/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-2.0.4/refcodes-logger-alt-simpledb/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-2.0.4/refcodes-logger-alt-slf4j/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-2.0.4/refcodes-logger-alt-slf4j/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-2.0.4/refcodes-logger-alt-spring/pom.xml) 

## Change list &lt;refcodes-logger-ext&gt; (version 2.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-2.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-2.0.4/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-2.0.4/refcodes-logger-ext-slf4j/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-2.0.4/refcodes-logger-ext-slf4j/README.md) 

## Change list &lt;refcodes-graphical-ext&gt; (version 2.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-2.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-2.0.4/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-2.0.4/refcodes-graphical-ext-javafx/pom.xml) 

## Change list &lt;refcodes-checkerboard&gt; (version 2.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-2.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-2.0.4/README.md) 

## Change list &lt;refcodes-checkerboard-alt&gt; (version 2.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-alt/src/refcodes-checkerboard-alt-2.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-checkerboard-alt/src/refcodes-checkerboard-alt-2.0.4/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-alt/src/refcodes-checkerboard-alt-2.0.4/refcodes-checkerboard-alt-javafx/pom.xml) 

## Change list &lt;refcodes-checkerboard-ext&gt; (version 2.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-ext/src/refcodes-checkerboard-ext-2.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-checkerboard-ext/src/refcodes-checkerboard-ext-2.0.4/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-ext/src/refcodes-checkerboard-ext-2.0.4/refcodes-checkerboard-ext-javafx-boulderdash/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-ext/src/refcodes-checkerboard-ext-2.0.4/refcodes-checkerboard-ext-javafx-chess/pom.xml) 

## Change list &lt;refcodes-boulderdash&gt; (version 2.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-boulderdash/src/refcodes-boulderdash-2.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-boulderdash/src/refcodes-boulderdash-2.0.4/README.md) 

## Change list &lt;refcodes-net&gt; (version 2.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-2.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-2.0.4/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`HeaderFields.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-2.0.4/src/main/java/org/refcodes/net/HeaderFields.java) (see Javadoc at [`HeaderFields.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/2.0.4/org/refcodes/net/HeaderFields.html))
* \[<span style="color:green">MODIFIED</span>\] [`HttpBodyMap.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-2.0.4/src/main/java/org/refcodes/net/HttpBodyMap.java) (see Javadoc at [`HttpBodyMap.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/2.0.4/org/refcodes/net/HttpBodyMap.html))
* \[<span style="color:green">MODIFIED</span>\] [`HttpClientResponseImpl.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-2.0.4/src/main/java/org/refcodes/net/HttpClientResponseImpl.java) (see Javadoc at [`HttpClientResponseImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/2.0.4/org/refcodes/net/HttpClientResponseImpl.html))

## Change list &lt;refcodes-rest&gt; (version 2.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-2.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-2.0.4/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`AbstractHttpRestServerDecorator.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-2.0.4/src/main/java/org/refcodes/rest/AbstractHttpRestServerDecorator.java) (see Javadoc at [`AbstractHttpRestServerDecorator.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/2.0.4/org/refcodes/rest/AbstractHttpRestServerDecorator.html))
* \[<span style="color:green">MODIFIED</span>\] [`AbstractRestServer.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-2.0.4/src/main/java/org/refcodes/rest/AbstractRestServer.java) (see Javadoc at [`AbstractRestServer.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/2.0.4/org/refcodes/rest/AbstractRestServer.html))
* \[<span style="color:green">MODIFIED</span>\] [`BasicAuthEndpointBuilderImpl.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-2.0.4/src/main/java/org/refcodes/rest/BasicAuthEndpointBuilderImpl.java) (see Javadoc at [`BasicAuthEndpointBuilderImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/2.0.4/org/refcodes/rest/BasicAuthEndpointBuilderImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`BasicAuthEndpointBuilder.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-2.0.4/src/main/java/org/refcodes/rest/BasicAuthEndpointBuilder.java) (see Javadoc at [`BasicAuthEndpointBuilder.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/2.0.4/org/refcodes/rest/BasicAuthEndpointBuilder.html))
* \[<span style="color:green">MODIFIED</span>\] [`BasicAuthEndpoint.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-2.0.4/src/main/java/org/refcodes/rest/BasicAuthEndpoint.java) (see Javadoc at [`BasicAuthEndpoint.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/2.0.4/org/refcodes/rest/BasicAuthEndpoint.html))
* \[<span style="color:green">MODIFIED</span>\] [`HttpRestClientImpl.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-2.0.4/src/main/java/org/refcodes/rest/HttpRestClientImpl.java) (see Javadoc at [`HttpRestClientImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/2.0.4/org/refcodes/rest/HttpRestClientImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`HttpRestServerSugar.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-2.0.4/src/main/java/org/refcodes/rest/HttpRestServerSugar.java) (see Javadoc at [`HttpRestServerSugar.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/2.0.4/org/refcodes/rest/HttpRestServerSugar.html))
* \[<span style="color:green">MODIFIED</span>\] [`RestEndpointBuilderImpl.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-2.0.4/src/main/java/org/refcodes/rest/RestEndpointBuilderImpl.java) (see Javadoc at [`RestEndpointBuilderImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/2.0.4/org/refcodes/rest/RestEndpointBuilderImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`RestEndpointBuilder.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-2.0.4/src/main/java/org/refcodes/rest/RestEndpointBuilder.java) (see Javadoc at [`RestEndpointBuilder.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/2.0.4/org/refcodes/rest/RestEndpointBuilder.html))
* \[<span style="color:green">MODIFIED</span>\] [`RestEndpoint.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-2.0.4/src/main/java/org/refcodes/rest/RestEndpoint.java) (see Javadoc at [`RestEndpoint.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/2.0.4/org/refcodes/rest/RestEndpoint.html))
* \[<span style="color:green">MODIFIED</span>\] [`RestServer.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-2.0.4/src/main/java/org/refcodes/rest/RestServer.java) (see Javadoc at [`RestServer.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/2.0.4/org/refcodes/rest/RestServer.html))
* \[<span style="color:green">MODIFIED</span>\] [`HttpRestServerTest.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-2.0.4/src/test/java/org/refcodes/rest/HttpRestServerTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`HttpsRestServerTest.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-2.0.4/src/test/java/org/refcodes/rest/HttpsRestServerTest.java) 

## Change list &lt;refcodes-hal&gt; (version 2.0.4)

* \[<span style="color:blue">ADDED</span>\] [`HalDataPageImpl.java`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-2.0.4/src/main/java/org/refcodes/hal/HalDataPageImpl.java) (see Javadoc at [`HalDataPageImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-hal/2.0.4/org/refcodes/hal/HalDataPageImpl.html))
* \[<span style="color:blue">ADDED</span>\] [`HalDataPage.java`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-2.0.4/src/main/java/org/refcodes/hal/HalDataPage.java) (see Javadoc at [`HalDataPage.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-hal/2.0.4/org/refcodes/hal/HalDataPage.html))
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-2.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-2.0.4/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`HalClientImpl.java`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-2.0.4/src/main/java/org/refcodes/hal/HalClientImpl.java) (see Javadoc at [`HalClientImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-hal/2.0.4/org/refcodes/hal/HalClientImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`HalClient.java`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-2.0.4/src/main/java/org/refcodes/hal/HalClient.java) (see Javadoc at [`HalClient.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-hal/2.0.4/org/refcodes/hal/HalClient.html))
* \[<span style="color:green">MODIFIED</span>\] [`HalData.java`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-2.0.4/src/main/java/org/refcodes/hal/HalData.java) (see Javadoc at [`HalData.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-hal/2.0.4/org/refcodes/hal/HalData.html))
* \[<span style="color:green">MODIFIED</span>\] [`HalMap.java`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-2.0.4/src/main/java/org/refcodes/hal/HalMap.java) (see Javadoc at [`HalMap.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-hal/2.0.4/org/refcodes/hal/HalMap.html))
* \[<span style="color:green">MODIFIED</span>\] [`HalStruct.java`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-2.0.4/src/main/java/org/refcodes/hal/HalStruct.java) (see Javadoc at [`HalStruct.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-hal/2.0.4/org/refcodes/hal/HalStruct.html))
* \[<span style="color:green">MODIFIED</span>\] [`AddressRepository.java`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-2.0.4/src/test/java/org/refcodes/hal/AddressRepository.java) 
* \[<span style="color:green">MODIFIED</span>\] [`AuthorRepository.java`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-2.0.4/src/test/java/org/refcodes/hal/AuthorRepository.java) 
* \[<span style="color:green">MODIFIED</span>\] [`BookRepository.java`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-2.0.4/src/test/java/org/refcodes/hal/BookRepository.java) 
* \[<span style="color:green">MODIFIED</span>\] [`HalClientTest.java`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-2.0.4/src/test/java/org/refcodes/hal/HalClientTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`LibraryRepository.java`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-2.0.4/src/test/java/org/refcodes/hal/LibraryRepository.java) 
* \[<span style="color:green">MODIFIED</span>\] [`PersonRepository.java`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-2.0.4/src/test/java/org/refcodes/hal/PersonRepository.java) 

## Change list &lt;refcodes-daemon&gt; (version 2.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-daemon/src/refcodes-daemon-2.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-daemon/src/refcodes-daemon-2.0.4/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`AbstractDaemon.java`](https://bitbucket.org/refcodes/refcodes-daemon/src/refcodes-daemon-2.0.4/src/main/java/org/refcodes/daemon/AbstractDaemon.java) (see Javadoc at [`AbstractDaemon.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-daemon/2.0.4/org/refcodes/daemon/AbstractDaemon.html))

## Change list &lt;refcodes-eventbus&gt; (version 2.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-eventbus/src/refcodes-eventbus-2.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-eventbus/src/refcodes-eventbus-2.0.4/README.md) 

## Change list &lt;refcodes-eventbus-ext&gt; (version 2.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-2.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-2.0.4/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-2.0.4/refcodes-eventbus-ext-application/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-2.0.4/refcodes-eventbus-ext-application/README.md) 

## Change list &lt;refcodes-filesystem&gt; (version 2.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-filesystem/src/refcodes-filesystem-2.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-filesystem/src/refcodes-filesystem-2.0.4/README.md) 

## Change list &lt;refcodes-filesystem-alt&gt; (version 2.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-filesystem-alt/src/refcodes-filesystem-alt-2.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-filesystem-alt/src/refcodes-filesystem-alt-2.0.4/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-filesystem-alt/src/refcodes-filesystem-alt-2.0.4/refcodes-filesystem-alt-s3/pom.xml) 

## Change list &lt;refcodes-forwardsecrecy&gt; (version 2.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy/src/refcodes-forwardsecrecy-2.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy/src/refcodes-forwardsecrecy-2.0.4/README.md) 

## Change list &lt;refcodes-forwardsecrecy-alt&gt; (version 2.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-2.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-2.0.4/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-2.0.4/refcodes-forwardsecrecy-alt-filesystem/pom.xml) 

## Change list &lt;refcodes-io-ext&gt; (version 2.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-2.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-2.0.4/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-2.0.4/refcodes-io-ext-observable/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-2.0.4/refcodes-io-ext-observable/README.md) 

## Change list &lt;refcodes-interceptor&gt; (version 2.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-interceptor/src/refcodes-interceptor-2.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-interceptor/src/refcodes-interceptor-2.0.4/README.md) 

## Change list &lt;refcodes-jobbus&gt; (version 2.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-jobbus/src/refcodes-jobbus-2.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-jobbus/src/refcodes-jobbus-2.0.4/README.md) 

## Change list &lt;refcodes-rest-ext&gt; (version 2.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-2.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-2.0.4/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-2.0.4/refcodes-rest-ext-archetype/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-2.0.4/refcodes-rest-ext-archetype/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-2.0.4/refcodes-rest-ext-eureka/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-2.0.4/refcodes-rest-ext-eureka/README.md) 

## Change list &lt;refcodes-remoting&gt; (version 2.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-remoting/src/refcodes-remoting-2.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-remoting/src/refcodes-remoting-2.0.4/README.md) 

## Change list &lt;refcodes-remoting-ext&gt; (version 2.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-2.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-2.0.4/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-2.0.4/refcodes-remoting-ext-observer/pom.xml) 

## Change list &lt;refcodes-servicebus&gt; (version 2.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-servicebus/src/refcodes-servicebus-2.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-servicebus/src/refcodes-servicebus-2.0.4/README.md) 

## Change list &lt;refcodes-servicebus-alt&gt; (version 2.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-servicebus-alt/src/refcodes-servicebus-alt-2.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-servicebus-alt/src/refcodes-servicebus-alt-2.0.4/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-servicebus-alt/src/refcodes-servicebus-alt-2.0.4/refcodes-servicebus-alt-spring/pom.xml) 

## Change list &lt;refcodes-tabular-alt&gt; (version 2.0.4)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-tabular-alt/src/refcodes-tabular-alt-2.0.4/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-tabular-alt/src/refcodes-tabular-alt-2.0.4/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-tabular-alt/src/refcodes-tabular-alt-2.0.4/refcodes-tabular-alt-forwardsecrecy/pom.xml) 
