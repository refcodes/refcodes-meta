> This change list has been auto-generated on `triton` by `steiner` with `changelist-all.sh` on the 2022-09-25 at 14:25:29.

## Change list &lt;refcodes-licensing&gt; (version 3.0.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-licensing/src/refcodes-licensing-3.0.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-licensing/src/refcodes-licensing-3.0.6/README.md) 

## Change list &lt;refcodes-parent&gt; (version 3.0.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-parent/src/refcodes-parent-3.0.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-parent/src/refcodes-parent-3.0.6/README.md) 

## Change list &lt;refcodes-time&gt; (version 3.0.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-time/src/refcodes-time-3.0.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-time/src/refcodes-time-3.0.6/README.md) 

## Change list &lt;refcodes-mixin&gt; (version 3.0.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-mixin/src/refcodes-mixin-3.0.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-mixin/src/refcodes-mixin-3.0.6/README.md) 

## Change list &lt;refcodes-data&gt; (version 3.0.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data/src/refcodes-data-3.0.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data/src/refcodes-data-3.0.6/README.md) 

## Change list &lt;refcodes-exception&gt; (version 3.0.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-exception/src/refcodes-exception-3.0.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-exception/src/refcodes-exception-3.0.6/README.md) 

## Change list &lt;refcodes-factory&gt; (version 3.0.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-factory/src/refcodes-factory-3.0.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-factory/src/refcodes-factory-3.0.6/README.md) 

## Change list &lt;refcodes-factory-alt&gt; (version 3.0.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-factory-alt/src/refcodes-factory-alt-3.0.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-factory-alt/src/refcodes-factory-alt-3.0.6/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-factory-alt/src/refcodes-factory-alt-3.0.6/refcodes-factory-alt-spring/pom.xml) 

## Change list &lt;refcodes-controlflow&gt; (version 3.0.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-controlflow/src/refcodes-controlflow-3.0.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-controlflow/src/refcodes-controlflow-3.0.6/README.md) 

## Change list &lt;refcodes-numerical&gt; (version 3.0.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-numerical/src/refcodes-numerical-3.0.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-numerical/src/refcodes-numerical-3.0.6/README.md) 

## Change list &lt;refcodes-generator&gt; (version 3.0.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-generator/src/refcodes-generator-3.0.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-generator/src/refcodes-generator-3.0.6/README.md) 

## Change list &lt;refcodes-matcher&gt; (version 3.0.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-matcher/src/refcodes-matcher-3.0.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-matcher/src/refcodes-matcher-3.0.6/README.md) 

## Change list &lt;refcodes-struct&gt; (version 3.0.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-struct/src/refcodes-struct-3.0.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-struct/src/refcodes-struct-3.0.6/README.md) 

## Change list &lt;refcodes-struct-ext&gt; (version 3.0.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-struct-ext/src/refcodes-struct-ext-3.0.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-struct-ext/src/refcodes-struct-ext-3.0.6/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-struct-ext/src/refcodes-struct-ext-3.0.6/refcodes-struct-ext-factory/pom.xml) 

## Change list &lt;refcodes-runtime&gt; (version 3.0.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-3.0.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-3.0.6/README.md) 

## Change list &lt;refcodes-component&gt; (version 3.0.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.0.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.0.6/README.md) 

## Change list &lt;refcodes-data-ext&gt; (version 3.0.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-3.0.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-3.0.6/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-3.0.6/refcodes-data-ext-checkers/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-3.0.6/refcodes-data-ext-checkers/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-3.0.6/refcodes-data-ext-chess/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-3.0.6/refcodes-data-ext-chess/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-3.0.6/refcodes-data-ext-corporate/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-3.0.6/refcodes-data-ext-corporate/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-3.0.6/refcodes-data-ext-symbols/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-3.0.6/refcodes-data-ext-symbols/README.md) 

## Change list &lt;refcodes-graphical&gt; (version 3.0.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-graphical/src/refcodes-graphical-3.0.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-graphical/src/refcodes-graphical-3.0.6/README.md) 

## Change list &lt;refcodes-textual&gt; (version 3.0.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-3.0.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-3.0.6/README.md) 

## Change list &lt;refcodes-criteria&gt; (version 3.0.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-3.0.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-3.0.6/README.md) 

## Change list &lt;refcodes-io&gt; (version 3.0.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-io/src/refcodes-io-3.0.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-io/src/refcodes-io-3.0.6/README.md) 

## Change list &lt;refcodes-tabular&gt; (version 3.0.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-tabular/src/refcodes-tabular-3.0.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-tabular/src/refcodes-tabular-3.0.6/README.md) 

## Change list &lt;refcodes-observer&gt; (version 3.0.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-observer/src/refcodes-observer-3.0.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-observer/src/refcodes-observer-3.0.6/README.md) 

## Change list &lt;refcodes-command&gt; (version 3.0.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-command/src/refcodes-command-3.0.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-command/src/refcodes-command-3.0.6/README.md) 

## Change list &lt;refcodes-cli&gt; (version 3.0.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.0.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.0.6/README.md) 

## Change list &lt;refcodes-audio&gt; (version 3.0.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-audio/src/refcodes-audio-3.0.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-audio/src/refcodes-audio-3.0.6/README.md) 

## Change list &lt;refcodes-codec&gt; (version 3.0.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-3.0.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-3.0.6/README.md) 

## Change list &lt;refcodes-component-ext&gt; (version 3.0.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-3.0.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-3.0.6/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-3.0.6/refcodes-component-ext-observer/pom.xml) 

## Change list &lt;refcodes-properties&gt; (version 3.0.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-3.0.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-3.0.6/README.md) 

## Change list &lt;refcodes-security&gt; (version 3.0.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security/src/refcodes-security-3.0.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security/src/refcodes-security-3.0.6/README.md) 

## Change list &lt;refcodes-security-alt&gt; (version 3.0.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-3.0.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-3.0.6/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-3.0.6/refcodes-security-alt-chaos/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-3.0.6/refcodes-security-alt-chaos/README.md) 

## Change list &lt;refcodes-security-ext&gt; (version 3.0.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-3.0.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-3.0.6/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-3.0.6/refcodes-security-ext-chaos/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-3.0.6/refcodes-security-ext-chaos/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-3.0.6/refcodes-security-ext-spring/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-3.0.6/refcodes-security-ext-spring/README.md) 

## Change list &lt;refcodes-properties-ext&gt; (version 3.0.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.0.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.0.6/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.0.6/refcodes-properties-ext-cli/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.0.6/refcodes-properties-ext-cli/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.0.6/refcodes-properties-ext-obfuscation/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.0.6/refcodes-properties-ext-obfuscation/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.0.6/refcodes-properties-ext-observer/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.0.6/refcodes-properties-ext-observer/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.0.6/refcodes-properties-ext-runtime/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.0.6/refcodes-properties-ext-runtime/README.md) 

## Change list &lt;refcodes-logger&gt; (version 3.0.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger/src/refcodes-logger-3.0.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger/src/refcodes-logger-3.0.6/README.md) 

## Change list &lt;refcodes-logger-alt&gt; (version 3.0.6)

* \[<span style="color:blue">ADDED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.0.6/refcodes-logger-alt-slf4j-legacy/pom.xml) 
* \[<span style="color:blue">ADDED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.0.6/refcodes-logger-alt-slf4j-legacy/README.md) 
* \[<span style="color:blue">ADDED</span>\] [`module-info.java`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.0.6/refcodes-logger-alt-slf4j-legacy/src/main/java/module-info.java) (see Javadoc at [`module-info.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-logger-alt-slf4j-legacy/3.0.6//module-info.html))
* \[<span style="color:blue">ADDED</span>\] [`package-info.java`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.0.6/refcodes-logger-alt-slf4j-legacy/src/main/java/org/refcodes/logger/alt/slf4j/legacy/package-info.java) 
* \[<span style="color:blue">ADDED</span>\] [`Slf4jLogger.java`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.0.6/refcodes-logger-alt-slf4j-legacy/src/main/java/org/refcodes/logger/alt/slf4j/legacy/Slf4jLogger.java) (see Javadoc at [`Slf4jLogger.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-logger-alt-slf4j-legacy/3.0.6/org.refcodes.logger.alt.slf4j.legacy/org/refcodes/logger/alt/slf4j/legacy/Slf4jLogger.html))
* \[<span style="color:blue">ADDED</span>\] [`Slf4jRuntimeLoggerFactory.java`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.0.6/refcodes-logger-alt-slf4j-legacy/src/main/java/org/refcodes/logger/alt/slf4j/legacy/Slf4jRuntimeLoggerFactory.java) (see Javadoc at [`Slf4jRuntimeLoggerFactory.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-logger-alt-slf4j-legacy/3.0.6/org.refcodes.logger.alt.slf4j.legacy/org/refcodes/logger/alt/slf4j/legacy/Slf4jRuntimeLoggerFactory.html))
* \[<span style="color:blue">ADDED</span>\] [`Slf4jRuntimeLoggerFactorySingleton.java`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.0.6/refcodes-logger-alt-slf4j-legacy/src/main/java/org/refcodes/logger/alt/slf4j/legacy/Slf4jRuntimeLoggerFactorySingleton.java) (see Javadoc at [`Slf4jRuntimeLoggerFactorySingleton.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-logger-alt-slf4j-legacy/3.0.6/org.refcodes.logger.alt.slf4j.legacy/org/refcodes/logger/alt/slf4j/legacy/Slf4jRuntimeLoggerFactorySingleton.html))
* \[<span style="color:blue">ADDED</span>\] [`Slf4jRuntimeLogger.java`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.0.6/refcodes-logger-alt-slf4j-legacy/src/main/java/org/refcodes/logger/alt/slf4j/legacy/Slf4jRuntimeLogger.java) (see Javadoc at [`Slf4jRuntimeLogger.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-logger-alt-slf4j-legacy/3.0.6/org.refcodes.logger.alt.slf4j.legacy/org/refcodes/logger/alt/slf4j/legacy/Slf4jRuntimeLogger.html))
* \[<span style="color:blue">ADDED</span>\] [`runtimelogger.ini`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.0.6/refcodes-logger-alt-slf4j-legacy/src/main/resources/runtimelogger.ini) 
* \[<span style="color:blue">ADDED</span>\] [`RuntimeLoggerSingletonTest.java`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.0.6/refcodes-logger-alt-slf4j-legacy/src/test/java/org/refcodes/logger/alt/slf4j/legacy/RuntimeLoggerSingletonTest.java) 
* \[<span style="color:blue">ADDED</span>\] [`Slf4jRuntimeLoggerTest.java`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.0.6/refcodes-logger-alt-slf4j-legacy/src/test/java/org/refcodes/logger/alt/slf4j/legacy/Slf4jRuntimeLoggerTest.java) 
* \[<span style="color:red">DELETED</span>\] `runtimelogger.ini`
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.0.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.0.6/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.0.6/refcodes-logger-alt-async/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.0.6/refcodes-logger-alt-async/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.0.6/refcodes-logger-alt-console/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.0.6/refcodes-logger-alt-console/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.0.6/refcodes-logger-alt-io/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.0.6/refcodes-logger-alt-io/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.0.6/refcodes-logger-alt-simpledb/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.0.6/refcodes-logger-alt-simpledb/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.0.6/refcodes-logger-alt-slf4j/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.0.6/refcodes-logger-alt-slf4j/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`package-info.java`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.0.6/refcodes-logger-alt-slf4j/src/main/java/org/refcodes/logger/alt/slf4j/package-info.java) 
* \[<span style="color:green">MODIFIED</span>\] [`Slf4jRuntimeLoggerFactory.java`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.0.6/refcodes-logger-alt-slf4j/src/main/java/org/refcodes/logger/alt/slf4j/Slf4jRuntimeLoggerFactory.java) (see Javadoc at [`Slf4jRuntimeLoggerFactory.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-logger-alt-slf4j/3.0.6/org.refcodes.logger.alt.slf4j/org/refcodes/logger/alt/slf4j/Slf4jRuntimeLoggerFactory.html))
* \[<span style="color:green">MODIFIED</span>\] [`RuntimeLoggerSingletonTest.java`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.0.6/refcodes-logger-alt-slf4j/src/test/java/org/refcodes/logger/alt/slf4j/RuntimeLoggerSingletonTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`Slf4jRuntimeLoggerTest.java`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.0.6/refcodes-logger-alt-slf4j/src/test/java/org/refcodes/logger/alt/slf4j/Slf4jRuntimeLoggerTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.0.6/refcodes-logger-alt-spring/pom.xml) 

## Change list &lt;refcodes-logger-ext&gt; (version 3.0.6)

* \[<span style="color:blue">ADDED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-3.0.6/refcodes-logger-ext-slf4j-legacy/pom.xml) 
* \[<span style="color:blue">ADDED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-3.0.6/refcodes-logger-ext-slf4j-legacy/README.md) 
* \[<span style="color:blue">ADDED</span>\] [`runtimelogger.ini`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-3.0.6/refcodes-logger-ext-slf4j-legacy/src/main/resources/runtimelogger.ini) 
* \[<span style="color:blue">ADDED</span>\] [`Slf4j1RuntimeLoggerTest.java`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-3.0.6/refcodes-logger-ext-slf4j-legacy/src/test/java/org/refcodes/logger/ext/slf4j/legacy/Slf4j1RuntimeLoggerTest.java) 
* \[<span style="color:blue">ADDED</span>\] [`runtimelogger.ini`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-3.0.6/refcodes-logger-ext-slf4j-legacy/src/test/resources/runtimelogger.ini) 
* \[<span style="color:blue">ADDED</span>\] [`module-info.java`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-3.0.6/refcodes-logger-ext-slf4j/src/main/java/module-info.java) (see Javadoc at [`module-info.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-logger-ext-slf4j/3.0.6//module-info.html))
* \[<span style="color:blue">ADDED</span>\] [`package-info.java`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-3.0.6/refcodes-logger-ext-slf4j/src/main/java/org/refcodes/logger/ext/slf4j/package-info.java) 
* \[<span style="color:blue">ADDED</span>\] [`RuntimeLoggerAdapter.java`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-3.0.6/refcodes-logger-ext-slf4j/src/main/java/org/refcodes/logger/ext/slf4j/RuntimeLoggerAdapter.java) (see Javadoc at [`RuntimeLoggerAdapter.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-logger-ext-slf4j/3.0.6/org.refcodes.logger.ext.slf4j/org/refcodes/logger/ext/slf4j/RuntimeLoggerAdapter.html))
* \[<span style="color:blue">ADDED</span>\] [`RuntimeLoggerFactory.java`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-3.0.6/refcodes-logger-ext-slf4j/src/main/java/org/refcodes/logger/ext/slf4j/RuntimeLoggerFactory.java) (see Javadoc at [`RuntimeLoggerFactory.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-logger-ext-slf4j/3.0.6/org.refcodes.logger.ext.slf4j/org/refcodes/logger/ext/slf4j/RuntimeLoggerFactory.html))
* \[<span style="color:blue">ADDED</span>\] [`RuntimeLoggerServiceProvider.java`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-3.0.6/refcodes-logger-ext-slf4j/src/main/java/org/refcodes/logger/ext/slf4j/RuntimeLoggerServiceProvider.java) (see Javadoc at [`RuntimeLoggerServiceProvider.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-logger-ext-slf4j/3.0.6/org.refcodes.logger.ext.slf4j/org/refcodes/logger/ext/slf4j/RuntimeLoggerServiceProvider.html))
* \[<span style="color:blue">ADDED</span>\] [`org.slf4j.spi.SLF4JServiceProvider`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-3.0.6/refcodes-logger-ext-slf4j/src/main/resources/META-INF/services/org.slf4j.spi.SLF4JServiceProvider) 
* \[<span style="color:blue">ADDED</span>\] [`Slf4j2RuntimeLoggerTest.java`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-3.0.6/refcodes-logger-ext-slf4j/src/test/java/org/refcodes/logger/ext/slf4j/Slf4j2RuntimeLoggerTest.java) 
* \[<span style="color:blue">ADDED</span>\] [`runtimelogger.ini`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-3.0.6/refcodes-logger-ext-slf4j/src/test/resources/runtimelogger.ini) 
* \[<span style="color:red">DELETED</span>\] `org.refcodes`
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-3.0.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-3.0.6/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-3.0.6/refcodes-logger-ext-slf4j/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-3.0.6/refcodes-logger-ext-slf4j/README.md) 

## Change list &lt;refcodes-graphical-ext&gt; (version 3.0.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-3.0.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-3.0.6/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-3.0.6/refcodes-graphical-ext-javafx/pom.xml) 

## Change list &lt;refcodes-checkerboard&gt; (version 3.0.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-3.0.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-3.0.6/README.md) 

## Change list &lt;refcodes-checkerboard-alt&gt; (version 3.0.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-alt/src/refcodes-checkerboard-alt-3.0.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-checkerboard-alt/src/refcodes-checkerboard-alt-3.0.6/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-alt/src/refcodes-checkerboard-alt-3.0.6/refcodes-checkerboard-alt-javafx/pom.xml) 

## Change list &lt;refcodes-net&gt; (version 3.0.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-3.0.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-3.0.6/README.md) 

## Change list &lt;refcodes-web&gt; (version 3.0.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-web/src/refcodes-web-3.0.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-web/src/refcodes-web-3.0.6/README.md) 

## Change list &lt;refcodes-rest&gt; (version 3.0.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-3.0.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-3.0.6/README.md) 

## Change list &lt;refcodes-hal&gt; (version 3.0.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-3.0.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-3.0.6/README.md) 

## Change list &lt;refcodes-eventbus&gt; (version 3.0.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-eventbus/src/refcodes-eventbus-3.0.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-eventbus/src/refcodes-eventbus-3.0.6/README.md) 

## Change list &lt;refcodes-eventbus-ext&gt; (version 3.0.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-3.0.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-3.0.6/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-3.0.6/refcodes-eventbus-ext-application/pom.xml) 

## Change list &lt;refcodes-filesystem&gt; (version 3.0.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-filesystem/src/refcodes-filesystem-3.0.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-filesystem/src/refcodes-filesystem-3.0.6/README.md) 

## Change list &lt;refcodes-filesystem-alt&gt; (version 3.0.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-filesystem-alt/src/refcodes-filesystem-alt-3.0.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-filesystem-alt/src/refcodes-filesystem-alt-3.0.6/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-filesystem-alt/src/refcodes-filesystem-alt-3.0.6/refcodes-filesystem-alt-s3/pom.xml) 

## Change list &lt;refcodes-forwardsecrecy&gt; (version 3.0.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy/src/refcodes-forwardsecrecy-3.0.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy/src/refcodes-forwardsecrecy-3.0.6/README.md) 

## Change list &lt;refcodes-forwardsecrecy-alt&gt; (version 3.0.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-3.0.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-3.0.6/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-3.0.6/refcodes-forwardsecrecy-alt-filesystem/pom.xml) 

## Change list &lt;refcodes-io-ext&gt; (version 3.0.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-3.0.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-3.0.6/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-3.0.6/refcodes-io-ext-observer/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-3.0.6/refcodes-io-ext-observer/README.md) 

## Change list &lt;refcodes-interceptor&gt; (version 3.0.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-interceptor/src/refcodes-interceptor-3.0.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-interceptor/src/refcodes-interceptor-3.0.6/README.md) 

## Change list &lt;refcodes-jobbus&gt; (version 3.0.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-jobbus/src/refcodes-jobbus-3.0.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-jobbus/src/refcodes-jobbus-3.0.6/README.md) 

## Change list &lt;refcodes-rest-ext&gt; (version 3.0.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-3.0.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-3.0.6/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-3.0.6/refcodes-rest-ext-eureka/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-3.0.6/refcodes-rest-ext-eureka/README.md) 

## Change list &lt;refcodes-remoting&gt; (version 3.0.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-remoting/src/refcodes-remoting-3.0.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-remoting/src/refcodes-remoting-3.0.6/README.md) 

## Change list &lt;refcodes-remoting-ext&gt; (version 3.0.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-3.0.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-3.0.6/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-3.0.6/refcodes-remoting-ext-observer/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-3.0.6/refcodes-remoting-ext-observer/README.md) 

## Change list &lt;refcodes-serial&gt; (version 3.0.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial/src/refcodes-serial-3.0.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-serial/src/refcodes-serial-3.0.6/README.md) 

## Change list &lt;refcodes-serial-alt&gt; (version 3.0.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial-alt/src/refcodes-serial-alt-3.0.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-serial-alt/src/refcodes-serial-alt-3.0.6/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial-alt/src/refcodes-serial-alt-3.0.6/refcodes-serial-alt-net/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-serial-alt/src/refcodes-serial-alt-3.0.6/refcodes-serial-alt-net/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial-alt/src/refcodes-serial-alt-3.0.6/refcodes-serial-alt-tty/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-serial-alt/src/refcodes-serial-alt-3.0.6/refcodes-serial-alt-tty/README.md) 

## Change list &lt;refcodes-serial-ext&gt; (version 3.0.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-3.0.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-3.0.6/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-3.0.6/refcodes-serial-ext-handshake/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-3.0.6/refcodes-serial-ext-handshake/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-3.0.6/refcodes-serial-ext-observer/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-3.0.6/refcodes-serial-ext-observer/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-3.0.6/refcodes-serial-ext-security/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-3.0.6/refcodes-serial-ext-security/README.md) 

## Change list &lt;refcodes-p2p&gt; (version 3.0.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-p2p/src/refcodes-p2p-3.0.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-p2p/src/refcodes-p2p-3.0.6/README.md) 

## Change list &lt;refcodes-p2p-alt&gt; (version 3.0.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-p2p-alt/src/refcodes-p2p-alt-3.0.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-p2p-alt/src/refcodes-p2p-alt-3.0.6/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-p2p-alt/src/refcodes-p2p-alt-3.0.6/refcodes-p2p-alt-rest/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-p2p-alt/src/refcodes-p2p-alt-3.0.6/refcodes-p2p-alt-serial/pom.xml) 

## Change list &lt;refcodes-p2p-ext&gt; (version 3.0.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-p2p-ext/src/refcodes-p2p-ext-3.0.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-p2p-ext/src/refcodes-p2p-ext-3.0.6/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-p2p-ext/src/refcodes-p2p-ext-3.0.6/refcodes-p2p-ext-observer/pom.xml) 

## Change list &lt;refcodes-servicebus&gt; (version 3.0.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-servicebus/src/refcodes-servicebus-3.0.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-servicebus/src/refcodes-servicebus-3.0.6/README.md) 

## Change list &lt;refcodes-servicebus-alt&gt; (version 3.0.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-servicebus-alt/src/refcodes-servicebus-alt-3.0.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-servicebus-alt/src/refcodes-servicebus-alt-3.0.6/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-servicebus-alt/src/refcodes-servicebus-alt-3.0.6/refcodes-servicebus-alt-spring/pom.xml) 

## Change list &lt;refcodes-tabular-alt&gt; (version 3.0.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-tabular-alt/src/refcodes-tabular-alt-3.0.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-tabular-alt/src/refcodes-tabular-alt-3.0.6/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-tabular-alt/src/refcodes-tabular-alt-3.0.6/refcodes-tabular-alt-forwardsecrecy/pom.xml) 

## Change list &lt;refcodes-archetype&gt; (version 3.0.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype/src/refcodes-archetype-3.0.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype/src/refcodes-archetype-3.0.6/README.md) 

## Change list &lt;refcodes-archetype-alt&gt; (version 3.0.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.0.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.0.6/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.0.6/refcodes-archetype-alt-c2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.0.6/refcodes-archetype-alt-c2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`build.sh`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.0.6/refcodes-archetype-alt-c2/src/main/resources/archetype-resources/build.sh) 
* \[<span style="color:green">MODIFIED</span>\] [`bundle.sh`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.0.6/refcodes-archetype-alt-c2/src/main/resources/archetype-resources/bundle.sh) 
* \[<span style="color:green">MODIFIED</span>\] [`installer.sh`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.0.6/refcodes-archetype-alt-c2/src/main/resources/archetype-resources/installer.sh) 
* \[<span style="color:green">MODIFIED</span>\] [`jexefy.sh`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.0.6/refcodes-archetype-alt-c2/src/main/resources/archetype-resources/jexefy.sh) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.0.6/refcodes-archetype-alt-c2/src/main/resources/archetype-resources/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`scriptify.sh`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.0.6/refcodes-archetype-alt-c2/src/main/resources/archetype-resources/scriptify.sh) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.0.6/refcodes-archetype-alt-cli/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.0.6/refcodes-archetype-alt-cli/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`build.sh`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.0.6/refcodes-archetype-alt-cli/src/main/resources/archetype-resources/build.sh) 
* \[<span style="color:green">MODIFIED</span>\] [`bundle.sh`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.0.6/refcodes-archetype-alt-cli/src/main/resources/archetype-resources/bundle.sh) 
* \[<span style="color:green">MODIFIED</span>\] [`installer.sh`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.0.6/refcodes-archetype-alt-cli/src/main/resources/archetype-resources/installer.sh) 
* \[<span style="color:green">MODIFIED</span>\] [`jexefy.sh`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.0.6/refcodes-archetype-alt-cli/src/main/resources/archetype-resources/jexefy.sh) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.0.6/refcodes-archetype-alt-cli/src/main/resources/archetype-resources/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`scriptify.sh`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.0.6/refcodes-archetype-alt-cli/src/main/resources/archetype-resources/scriptify.sh) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.0.6/refcodes-archetype-alt-csv/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.0.6/refcodes-archetype-alt-csv/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`build.sh`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.0.6/refcodes-archetype-alt-csv/src/main/resources/archetype-resources/build.sh) 
* \[<span style="color:green">MODIFIED</span>\] [`bundle.sh`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.0.6/refcodes-archetype-alt-csv/src/main/resources/archetype-resources/bundle.sh) 
* \[<span style="color:green">MODIFIED</span>\] [`installer.sh`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.0.6/refcodes-archetype-alt-csv/src/main/resources/archetype-resources/installer.sh) 
* \[<span style="color:green">MODIFIED</span>\] [`jexefy.sh`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.0.6/refcodes-archetype-alt-csv/src/main/resources/archetype-resources/jexefy.sh) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.0.6/refcodes-archetype-alt-csv/src/main/resources/archetype-resources/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`scriptify.sh`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.0.6/refcodes-archetype-alt-csv/src/main/resources/archetype-resources/scriptify.sh) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.0.6/refcodes-archetype-alt-eventbus/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.0.6/refcodes-archetype-alt-eventbus/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`build.sh`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.0.6/refcodes-archetype-alt-eventbus/src/main/resources/archetype-resources/build.sh) 
* \[<span style="color:green">MODIFIED</span>\] [`bundle.sh`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.0.6/refcodes-archetype-alt-eventbus/src/main/resources/archetype-resources/bundle.sh) 
* \[<span style="color:green">MODIFIED</span>\] [`installer.sh`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.0.6/refcodes-archetype-alt-eventbus/src/main/resources/archetype-resources/installer.sh) 
* \[<span style="color:green">MODIFIED</span>\] [`jexefy.sh`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.0.6/refcodes-archetype-alt-eventbus/src/main/resources/archetype-resources/jexefy.sh) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.0.6/refcodes-archetype-alt-eventbus/src/main/resources/archetype-resources/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`scriptify.sh`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.0.6/refcodes-archetype-alt-eventbus/src/main/resources/archetype-resources/scriptify.sh) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.0.6/refcodes-archetype-alt-rest/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.0.6/refcodes-archetype-alt-rest/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`build.sh`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.0.6/refcodes-archetype-alt-rest/src/main/resources/archetype-resources/build.sh) 
* \[<span style="color:green">MODIFIED</span>\] [`bundle.sh`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.0.6/refcodes-archetype-alt-rest/src/main/resources/archetype-resources/bundle.sh) 
* \[<span style="color:green">MODIFIED</span>\] [`installer.sh`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.0.6/refcodes-archetype-alt-rest/src/main/resources/archetype-resources/installer.sh) 
* \[<span style="color:green">MODIFIED</span>\] [`jexefy.sh`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.0.6/refcodes-archetype-alt-rest/src/main/resources/archetype-resources/jexefy.sh) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.0.6/refcodes-archetype-alt-rest/src/main/resources/archetype-resources/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`scriptify.sh`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.0.6/refcodes-archetype-alt-rest/src/main/resources/archetype-resources/scriptify.sh) 
