> This change list has been auto-generated on `triton` by `steiner` with `changelist-all.sh` on the 2023-04-30 at 14:39:59.

## Change list &lt;refcodes-properties-ext&gt; (version 3.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.1.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.1.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.1.3/refcodes-properties-ext-application/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.1.3/refcodes-properties-ext-application/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.1.3/refcodes-properties-ext-cli/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.1.3/refcodes-properties-ext-cli/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.1.3/refcodes-properties-ext-obfuscation/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.1.3/refcodes-properties-ext-obfuscation/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.1.3/refcodes-properties-ext-observer/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.1.3/refcodes-properties-ext-observer/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`AbstractObservablePropertiesBuilderDecorator.java`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.1.3/refcodes-properties-ext-observer/src/main/java/org/refcodes/properties/ext/observer/AbstractObservablePropertiesBuilderDecorator.java) (see Javadoc at [`AbstractObservablePropertiesBuilderDecorator.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties-ext-observer/3.1.3/org.refcodes.properties.ext.observer/org/refcodes/properties/ext/observer/AbstractObservablePropertiesBuilderDecorator.html))

## Change list &lt;refcodes-logger&gt; (version 3.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger/src/refcodes-logger-3.1.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger/src/refcodes-logger-3.1.3/README.md) 

## Change list &lt;refcodes-logger-alt&gt; (version 3.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.1.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.1.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.1.3/refcodes-logger-alt-async/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.1.3/refcodes-logger-alt-async/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.1.3/refcodes-logger-alt-console/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.1.3/refcodes-logger-alt-console/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.1.3/refcodes-logger-alt-io/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.1.3/refcodes-logger-alt-io/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.1.3/refcodes-logger-alt-simpledb/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.1.3/refcodes-logger-alt-simpledb/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.1.3/refcodes-logger-alt-slf4j-legacy/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.1.3/refcodes-logger-alt-slf4j/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.1.3/refcodes-logger-alt-spring/pom.xml) 

## Change list &lt;refcodes-logger-ext&gt; (version 3.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-3.1.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-3.1.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-3.1.3/refcodes-logger-ext-slf4j-legacy/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-3.1.3/refcodes-logger-ext-slf4j-legacy/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-3.1.3/refcodes-logger-ext-slf4j/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-3.1.3/refcodes-logger-ext-slf4j/README.md) 

## Change list &lt;refcodes-graphical-ext&gt; (version 3.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-3.1.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-3.1.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-3.1.3/refcodes-graphical-ext-javafx/pom.xml) 

## Change list &lt;refcodes-checkerboard&gt; (version 3.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-3.1.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-3.1.3/README.md) 

## Change list &lt;refcodes-checkerboard-alt&gt; (version 3.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-alt/src/refcodes-checkerboard-alt-3.1.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-alt/src/refcodes-checkerboard-alt-3.1.3/refcodes-checkerboard-alt-javafx/pom.xml) 

## Change list &lt;refcodes-net&gt; (version 3.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-3.1.3/pom.xml) 

## Change list &lt;refcodes-web&gt; (version 3.1.3)

* \[<span style="color:blue">ADDED</span>\] [`HttpClientInterceptable.java`](https://bitbucket.org/refcodes/refcodes-web/src/refcodes-web-3.1.3/src/main/java/org/refcodes/web/HttpClientInterceptable.java) (see Javadoc at [`HttpClientInterceptable.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-web/3.1.3/org.refcodes.web/org/refcodes/web/HttpClientInterceptable.html))
* \[<span style="color:blue">ADDED</span>\] [`HttpClientInterceptor.java`](https://bitbucket.org/refcodes/refcodes-web/src/refcodes-web-3.1.3/src/main/java/org/refcodes/web/HttpClientInterceptor.java) (see Javadoc at [`HttpClientInterceptor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-web/3.1.3/org.refcodes.web/org/refcodes/web/HttpClientInterceptor.html))
* \[<span style="color:blue">ADDED</span>\] [`HttpInterceptable.java`](https://bitbucket.org/refcodes/refcodes-web/src/refcodes-web-3.1.3/src/main/java/org/refcodes/web/HttpInterceptable.java) (see Javadoc at [`HttpInterceptable.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-web/3.1.3/org.refcodes.web/org/refcodes/web/HttpInterceptable.html))
* \[<span style="color:blue">ADDED</span>\] [`HttpInterceptor.java`](https://bitbucket.org/refcodes/refcodes-web/src/refcodes-web-3.1.3/src/main/java/org/refcodes/web/HttpInterceptor.java) (see Javadoc at [`HttpInterceptor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-web/3.1.3/org.refcodes.web/org/refcodes/web/HttpInterceptor.html))
* \[<span style="color:blue">ADDED</span>\] [`HttpServerInterceptable.java`](https://bitbucket.org/refcodes/refcodes-web/src/refcodes-web-3.1.3/src/main/java/org/refcodes/web/HttpServerInterceptable.java) (see Javadoc at [`HttpServerInterceptable.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-web/3.1.3/org.refcodes.web/org/refcodes/web/HttpServerInterceptable.html))
* \[<span style="color:blue">ADDED</span>\] [`HttpServerInterceptor.java`](https://bitbucket.org/refcodes/refcodes-web/src/refcodes-web-3.1.3/src/main/java/org/refcodes/web/HttpServerInterceptor.java) (see Javadoc at [`HttpServerInterceptor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-web/3.1.3/org.refcodes.web/org/refcodes/web/HttpServerInterceptor.html))
* \[<span style="color:blue">ADDED</span>\] [`PostHttpClientInterceptable.java`](https://bitbucket.org/refcodes/refcodes-web/src/refcodes-web-3.1.3/src/main/java/org/refcodes/web/PostHttpClientInterceptable.java) (see Javadoc at [`PostHttpClientInterceptable.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-web/3.1.3/org.refcodes.web/org/refcodes/web/PostHttpClientInterceptable.html))
* \[<span style="color:blue">ADDED</span>\] [`PostHttpClientInterceptor.java`](https://bitbucket.org/refcodes/refcodes-web/src/refcodes-web-3.1.3/src/main/java/org/refcodes/web/PostHttpClientInterceptor.java) (see Javadoc at [`PostHttpClientInterceptor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-web/3.1.3/org.refcodes.web/org/refcodes/web/PostHttpClientInterceptor.html))
* \[<span style="color:blue">ADDED</span>\] [`PostHttpInterceptable.java`](https://bitbucket.org/refcodes/refcodes-web/src/refcodes-web-3.1.3/src/main/java/org/refcodes/web/PostHttpInterceptable.java) (see Javadoc at [`PostHttpInterceptable.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-web/3.1.3/org.refcodes.web/org/refcodes/web/PostHttpInterceptable.html))
* \[<span style="color:blue">ADDED</span>\] [`PostHttpInterceptor.java`](https://bitbucket.org/refcodes/refcodes-web/src/refcodes-web-3.1.3/src/main/java/org/refcodes/web/PostHttpInterceptor.java) (see Javadoc at [`PostHttpInterceptor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-web/3.1.3/org.refcodes.web/org/refcodes/web/PostHttpInterceptor.html))
* \[<span style="color:blue">ADDED</span>\] [`PostHttpServerInterceptable.java`](https://bitbucket.org/refcodes/refcodes-web/src/refcodes-web-3.1.3/src/main/java/org/refcodes/web/PostHttpServerInterceptable.java) (see Javadoc at [`PostHttpServerInterceptable.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-web/3.1.3/org.refcodes.web/org/refcodes/web/PostHttpServerInterceptable.html))
* \[<span style="color:blue">ADDED</span>\] [`PostHttpServerInterceptor.java`](https://bitbucket.org/refcodes/refcodes-web/src/refcodes-web-3.1.3/src/main/java/org/refcodes/web/PostHttpServerInterceptor.java) (see Javadoc at [`PostHttpServerInterceptor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-web/3.1.3/org.refcodes.web/org/refcodes/web/PostHttpServerInterceptor.html))
* \[<span style="color:blue">ADDED</span>\] [`PreHttpClientInterceptable.java`](https://bitbucket.org/refcodes/refcodes-web/src/refcodes-web-3.1.3/src/main/java/org/refcodes/web/PreHttpClientInterceptable.java) (see Javadoc at [`PreHttpClientInterceptable.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-web/3.1.3/org.refcodes.web/org/refcodes/web/PreHttpClientInterceptable.html))
* \[<span style="color:blue">ADDED</span>\] [`PreHttpClientInterceptor.java`](https://bitbucket.org/refcodes/refcodes-web/src/refcodes-web-3.1.3/src/main/java/org/refcodes/web/PreHttpClientInterceptor.java) (see Javadoc at [`PreHttpClientInterceptor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-web/3.1.3/org.refcodes.web/org/refcodes/web/PreHttpClientInterceptor.html))
* \[<span style="color:blue">ADDED</span>\] [`PreHttpInterceptable.java`](https://bitbucket.org/refcodes/refcodes-web/src/refcodes-web-3.1.3/src/main/java/org/refcodes/web/PreHttpInterceptable.java) (see Javadoc at [`PreHttpInterceptable.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-web/3.1.3/org.refcodes.web/org/refcodes/web/PreHttpInterceptable.html))
* \[<span style="color:blue">ADDED</span>\] [`PreHttpInterceptor.java`](https://bitbucket.org/refcodes/refcodes-web/src/refcodes-web-3.1.3/src/main/java/org/refcodes/web/PreHttpInterceptor.java) (see Javadoc at [`PreHttpInterceptor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-web/3.1.3/org.refcodes.web/org/refcodes/web/PreHttpInterceptor.html))
* \[<span style="color:blue">ADDED</span>\] [`PreHttpServerInterceptable.java`](https://bitbucket.org/refcodes/refcodes-web/src/refcodes-web-3.1.3/src/main/java/org/refcodes/web/PreHttpServerInterceptable.java) (see Javadoc at [`PreHttpServerInterceptable.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-web/3.1.3/org.refcodes.web/org/refcodes/web/PreHttpServerInterceptable.html))
* \[<span style="color:blue">ADDED</span>\] [`PreHttpServerInterceptor.java`](https://bitbucket.org/refcodes/refcodes-web/src/refcodes-web-3.1.3/src/main/java/org/refcodes/web/PreHttpServerInterceptor.java) (see Javadoc at [`PreHttpServerInterceptor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-web/3.1.3/org.refcodes.web/org/refcodes/web/PreHttpServerInterceptor.html))
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-web/src/refcodes-web-3.1.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`AbstractHttpResponse.java`](https://bitbucket.org/refcodes/refcodes-web/src/refcodes-web-3.1.3/src/main/java/org/refcodes/web/AbstractHttpResponse.java) (see Javadoc at [`AbstractHttpResponse.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-web/3.1.3/org.refcodes.web/org/refcodes/web/AbstractHttpResponse.html))
* \[<span style="color:green">MODIFIED</span>\] [`HttpClientResponse.java`](https://bitbucket.org/refcodes/refcodes-web/src/refcodes-web-3.1.3/src/main/java/org/refcodes/web/HttpClientResponse.java) (see Javadoc at [`HttpClientResponse.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-web/3.1.3/org.refcodes.web/org/refcodes/web/HttpClientResponse.html))
* \[<span style="color:green">MODIFIED</span>\] [`HttpServerResponse.java`](https://bitbucket.org/refcodes/refcodes-web/src/refcodes-web-3.1.3/src/main/java/org/refcodes/web/HttpServerResponse.java) (see Javadoc at [`HttpServerResponse.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-web/3.1.3/org.refcodes.web/org/refcodes/web/HttpServerResponse.html))
* \[<span style="color:green">MODIFIED</span>\] [`HttpStatusCode.java`](https://bitbucket.org/refcodes/refcodes-web/src/refcodes-web-3.1.3/src/main/java/org/refcodes/web/HttpStatusCode.java) (see Javadoc at [`HttpStatusCode.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-web/3.1.3/org.refcodes.web/org/refcodes/web/HttpStatusCode.html))
* \[<span style="color:green">MODIFIED</span>\] [`HttpStatusException.java`](https://bitbucket.org/refcodes/refcodes-web/src/refcodes-web-3.1.3/src/main/java/org/refcodes/web/HttpStatusException.java) (see Javadoc at [`HttpStatusException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-web/3.1.3/org.refcodes.web/org/refcodes/web/HttpStatusException.html))

## Change list &lt;refcodes-rest&gt; (version 3.1.3)

* \[<span style="color:blue">ADDED</span>\] [`CorrelationClientInterceptor.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-3.1.3/src/main/java/org/refcodes/rest/CorrelationClientInterceptor.java) (see Javadoc at [`CorrelationClientInterceptor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/3.1.3/org.refcodes.rest/org/refcodes/rest/CorrelationClientInterceptor.html))
* \[<span style="color:blue">ADDED</span>\] [`CorrelationServerInterceptor.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-3.1.3/src/main/java/org/refcodes/rest/CorrelationServerInterceptor.java) (see Javadoc at [`CorrelationServerInterceptor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/3.1.3/org.refcodes.rest/org/refcodes/rest/CorrelationServerInterceptor.html))
* \[<span style="color:blue">ADDED</span>\] [`RestfulHttpClient.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-3.1.3/src/main/java/org/refcodes/rest/RestfulHttpClient.java) (see Javadoc at [`RestfulHttpClient.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/3.1.3/org.refcodes.rest/org/refcodes/rest/RestfulHttpClient.html))
* \[<span style="color:blue">ADDED</span>\] [`RestfulHttpServer.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-3.1.3/src/main/java/org/refcodes/rest/RestfulHttpServer.java) (see Javadoc at [`RestfulHttpServer.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/3.1.3/org.refcodes.rest/org/refcodes/rest/RestfulHttpServer.html))
* \[<span style="color:blue">ADDED</span>\] [`CorrelationRestClientTest.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-3.1.3/src/test/java/org/refcodes/rest/CorrelationRestClientTest.java) 
* \[<span style="color:blue">ADDED</span>\] [`CorrelationRestServerTest.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-3.1.3/src/test/java/org/refcodes/rest/CorrelationRestServerTest.java) 
* \[<span style="color:red">DELETED</span>\] `HttpRestClientImpl.java`
* \[<span style="color:red">DELETED</span>\] `HttpRestServerImpl.java`
* \[<span style="color:red">DELETED</span>\] `LoopbackRestClientImpl.java`
* \[<span style="color:red">DELETED</span>\] `LoopbackRestServerImpl.java`
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-3.1.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-3.1.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`AbstractHttpDiscoverySidecar.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-3.1.3/src/main/java/org/refcodes/rest/AbstractHttpDiscoverySidecar.java) (see Javadoc at [`AbstractHttpDiscoverySidecar.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/3.1.3/org.refcodes.rest/org/refcodes/rest/AbstractHttpDiscoverySidecar.html))
* \[<span style="color:green">MODIFIED</span>\] [`AbstractHttpRegistryContextBuilder.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-3.1.3/src/main/java/org/refcodes/rest/AbstractHttpRegistryContextBuilder.java) (see Javadoc at [`AbstractHttpRegistryContextBuilder.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/3.1.3/org.refcodes.rest/org/refcodes/rest/AbstractHttpRegistryContextBuilder.html))
* \[<span style="color:green">MODIFIED</span>\] [`AbstractHttpRegistrySidecar.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-3.1.3/src/main/java/org/refcodes/rest/AbstractHttpRegistrySidecar.java) (see Javadoc at [`AbstractHttpRegistrySidecar.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/3.1.3/org.refcodes.rest/org/refcodes/rest/AbstractHttpRegistrySidecar.html))
* \[<span style="color:green">MODIFIED</span>\] [`BasicAuthEndpoint.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-3.1.3/src/main/java/org/refcodes/rest/BasicAuthEndpoint.java) (see Javadoc at [`BasicAuthEndpoint.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/3.1.3/org.refcodes.rest/org/refcodes/rest/BasicAuthEndpoint.html))
* \[<span style="color:green">MODIFIED</span>\] [`BasicAuthEvent.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-3.1.3/src/main/java/org/refcodes/rest/BasicAuthEvent.java) (see Javadoc at [`BasicAuthEvent.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/3.1.3/org.refcodes.rest/org/refcodes/rest/BasicAuthEvent.html))
* \[<span style="color:green">MODIFIED</span>\] [`HttpDiscoveryContextBuilder.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-3.1.3/src/main/java/org/refcodes/rest/HttpDiscoveryContextBuilder.java) (see Javadoc at [`HttpDiscoveryContextBuilder.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/3.1.3/org.refcodes.rest/org/refcodes/rest/HttpDiscoveryContextBuilder.html))
* \[<span style="color:green">MODIFIED</span>\] [`HttpDiscovery.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-3.1.3/src/main/java/org/refcodes/rest/HttpDiscovery.java) (see Javadoc at [`HttpDiscovery.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/3.1.3/org.refcodes.rest/org/refcodes/rest/HttpDiscovery.html))
* \[<span style="color:green">MODIFIED</span>\] [`HttpRestClient.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-3.1.3/src/main/java/org/refcodes/rest/HttpRestClient.java) (see Javadoc at [`HttpRestClient.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/3.1.3/org.refcodes.rest/org/refcodes/rest/HttpRestClient.html))
* \[<span style="color:green">MODIFIED</span>\] [`HttpRestClientSingleton.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-3.1.3/src/main/java/org/refcodes/rest/HttpRestClientSingleton.java) (see Javadoc at [`HttpRestClientSingleton.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/3.1.3/org.refcodes.rest/org/refcodes/rest/HttpRestClientSingleton.html))
* \[<span style="color:green">MODIFIED</span>\] [`HttpRestClientSugar.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-3.1.3/src/main/java/org/refcodes/rest/HttpRestClientSugar.java) (see Javadoc at [`HttpRestClientSugar.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/3.1.3/org.refcodes.rest/org/refcodes/rest/HttpRestClientSugar.html))
* \[<span style="color:green">MODIFIED</span>\] [`HttpRestServer.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-3.1.3/src/main/java/org/refcodes/rest/HttpRestServer.java) (see Javadoc at [`HttpRestServer.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/3.1.3/org.refcodes.rest/org/refcodes/rest/HttpRestServer.html))
* \[<span style="color:green">MODIFIED</span>\] [`HttpRestServerSingleton.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-3.1.3/src/main/java/org/refcodes/rest/HttpRestServerSingleton.java) (see Javadoc at [`HttpRestServerSingleton.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/3.1.3/org.refcodes.rest/org/refcodes/rest/HttpRestServerSingleton.html))
* \[<span style="color:green">MODIFIED</span>\] [`HttpRestServerSugar.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-3.1.3/src/main/java/org/refcodes/rest/HttpRestServerSugar.java) (see Javadoc at [`HttpRestServerSugar.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/3.1.3/org.refcodes.rest/org/refcodes/rest/HttpRestServerSugar.html))
* \[<span style="color:green">MODIFIED</span>\] [`HttpServerDescriptor.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-3.1.3/src/main/java/org/refcodes/rest/HttpServerDescriptor.java) (see Javadoc at [`HttpServerDescriptor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/3.1.3/org.refcodes.rest/org/refcodes/rest/HttpServerDescriptor.html))
* \[<span style="color:green">MODIFIED</span>\] [`LoopbackRestClient.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-3.1.3/src/main/java/org/refcodes/rest/LoopbackRestClient.java) (see Javadoc at [`LoopbackRestClient.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/3.1.3/org.refcodes.rest/org/refcodes/rest/LoopbackRestClient.html))
* \[<span style="color:green">MODIFIED</span>\] [`LoopbackRestClientSingleton.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-3.1.3/src/main/java/org/refcodes/rest/LoopbackRestClientSingleton.java) (see Javadoc at [`LoopbackRestClientSingleton.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/3.1.3/org.refcodes.rest/org/refcodes/rest/LoopbackRestClientSingleton.html))
* \[<span style="color:green">MODIFIED</span>\] [`LoopbackRestServer.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-3.1.3/src/main/java/org/refcodes/rest/LoopbackRestServer.java) (see Javadoc at [`LoopbackRestServer.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/3.1.3/org.refcodes.rest/org/refcodes/rest/LoopbackRestServer.html))
* \[<span style="color:green">MODIFIED</span>\] [`LoopbackRestServerSingleton.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-3.1.3/src/main/java/org/refcodes/rest/LoopbackRestServerSingleton.java) (see Javadoc at [`LoopbackRestServerSingleton.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/3.1.3/org.refcodes.rest/org/refcodes/rest/LoopbackRestServerSingleton.html))
* \[<span style="color:green">MODIFIED</span>\] [`OauthTokenHandler.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-3.1.3/src/main/java/org/refcodes/rest/OauthTokenHandler.java) (see Javadoc at [`OauthTokenHandler.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/3.1.3/org.refcodes.rest/org/refcodes/rest/OauthTokenHandler.html))
* \[<span style="color:green">MODIFIED</span>\] [`RestDeleteClientSugar.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-3.1.3/src/main/java/org/refcodes/rest/RestDeleteClientSugar.java) (see Javadoc at [`RestDeleteClientSugar.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/3.1.3/org.refcodes.rest/org/refcodes/rest/RestDeleteClientSugar.html))
* \[<span style="color:green">MODIFIED</span>\] [`RestEndpoint.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-3.1.3/src/main/java/org/refcodes/rest/RestEndpoint.java) (see Javadoc at [`RestEndpoint.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/3.1.3/org.refcodes.rest/org/refcodes/rest/RestEndpoint.html))
* \[<span style="color:green">MODIFIED</span>\] [`RestGetClientSugar.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-3.1.3/src/main/java/org/refcodes/rest/RestGetClientSugar.java) (see Javadoc at [`RestGetClientSugar.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/3.1.3/org.refcodes.rest/org/refcodes/rest/RestGetClientSugar.html))
* \[<span style="color:green">MODIFIED</span>\] [`RestPostClientSugar.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-3.1.3/src/main/java/org/refcodes/rest/RestPostClientSugar.java) (see Javadoc at [`RestPostClientSugar.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/3.1.3/org.refcodes.rest/org/refcodes/rest/RestPostClientSugar.html))
* \[<span style="color:green">MODIFIED</span>\] [`RestPutClientSugar.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-3.1.3/src/main/java/org/refcodes/rest/RestPutClientSugar.java) (see Javadoc at [`RestPutClientSugar.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/3.1.3/org.refcodes.rest/org/refcodes/rest/RestPutClientSugar.html))
* \[<span style="color:green">MODIFIED</span>\] [`RestRequestBuilder.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-3.1.3/src/main/java/org/refcodes/rest/RestRequestBuilder.java) (see Javadoc at [`RestRequestBuilder.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/3.1.3/org.refcodes.rest/org/refcodes/rest/RestRequestBuilder.html))
* \[<span style="color:green">MODIFIED</span>\] [`RestRequestClientSugar.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-3.1.3/src/main/java/org/refcodes/rest/RestRequestClientSugar.java) (see Javadoc at [`RestRequestClientSugar.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/3.1.3/org.refcodes.rest/org/refcodes/rest/RestRequestClientSugar.html))
* \[<span style="color:green">MODIFIED</span>\] [`RestRequestEvent.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-3.1.3/src/main/java/org/refcodes/rest/RestRequestEvent.java) (see Javadoc at [`RestRequestEvent.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/3.1.3/org.refcodes.rest/org/refcodes/rest/RestRequestEvent.html))
* \[<span style="color:green">MODIFIED</span>\] [`RestRequestHandler.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-3.1.3/src/main/java/org/refcodes/rest/RestRequestHandler.java) (see Javadoc at [`RestRequestHandler.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/3.1.3/org.refcodes.rest/org/refcodes/rest/RestRequestHandler.html))
* \[<span style="color:green">MODIFIED</span>\] [`RestRequest.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-3.1.3/src/main/java/org/refcodes/rest/RestRequest.java) (see Javadoc at [`RestRequest.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/3.1.3/org.refcodes.rest/org/refcodes/rest/RestRequest.html))
* \[<span style="color:green">MODIFIED</span>\] [`RestResponseEvent.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-3.1.3/src/main/java/org/refcodes/rest/RestResponseEvent.java) (see Javadoc at [`RestResponseEvent.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/3.1.3/org.refcodes.rest/org/refcodes/rest/RestResponseEvent.html))
* \[<span style="color:green">MODIFIED</span>\] [`RestResponseHandler.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-3.1.3/src/main/java/org/refcodes/rest/RestResponseHandler.java) (see Javadoc at [`RestResponseHandler.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/3.1.3/org.refcodes.rest/org/refcodes/rest/RestResponseHandler.html))
* \[<span style="color:green">MODIFIED</span>\] [`RestResponse.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-3.1.3/src/main/java/org/refcodes/rest/RestResponse.java) (see Javadoc at [`RestResponse.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/3.1.3/org.refcodes.rest/org/refcodes/rest/RestResponse.html))
* \[<span style="color:green">MODIFIED</span>\] [`RestResponseResult.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-3.1.3/src/main/java/org/refcodes/rest/RestResponseResult.java) (see Javadoc at [`RestResponseResult.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/3.1.3/org.refcodes.rest/org/refcodes/rest/RestResponseResult.html))
* \[<span style="color:green">MODIFIED</span>\] [`HttpRestClientTest.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-3.1.3/src/test/java/org/refcodes/rest/HttpRestClientTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`HttpRestServerTest.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-3.1.3/src/test/java/org/refcodes/rest/HttpRestServerTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`HttpRestSugarTest.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-3.1.3/src/test/java/org/refcodes/rest/HttpRestSugarTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`HttpsRestServerTest.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-3.1.3/src/test/java/org/refcodes/rest/HttpsRestServerTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`LoopbackRestServerTest.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-3.1.3/src/test/java/org/refcodes/rest/LoopbackRestServerTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`OauthTokenHandlerTest.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-3.1.3/src/test/java/org/refcodes/rest/OauthTokenHandlerTest.java) 

## Change list &lt;refcodes-hal&gt; (version 3.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-3.1.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-3.1.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`HalClient.java`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-3.1.3/src/main/java/org/refcodes/hal/HalClient.java) (see Javadoc at [`HalClient.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-hal/3.1.3/org.refcodes.hal/org/refcodes/hal/HalClient.html))
* \[<span style="color:green">MODIFIED</span>\] [`HalClientTest.java`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-3.1.3/src/test/java/org/refcodes/hal/HalClientTest.java) 

## Change list &lt;refcodes-eventbus&gt; (version 3.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-eventbus/src/refcodes-eventbus-3.1.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-eventbus/src/refcodes-eventbus-3.1.3/README.md) 

## Change list &lt;refcodes-eventbus-ext&gt; (version 3.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-3.1.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-3.1.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-3.1.3/refcodes-eventbus-ext-application/pom.xml) 

## Change list &lt;refcodes-decoupling&gt; (version 3.1.3)

* \[<span style="color:blue">ADDED</span>\] [`DependencyInterceptor.java`](https://bitbucket.org/refcodes/refcodes-decoupling/src/refcodes-decoupling-3.1.3/src/main/java/org/refcodes/decoupling/DependencyInterceptor.java) (see Javadoc at [`DependencyInterceptor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-decoupling/3.1.3/org.refcodes.decoupling/org/refcodes/decoupling/DependencyInterceptor.html))
* \[<span style="color:blue">ADDED</span>\] [`ComponentExtendsA2.java`](https://bitbucket.org/refcodes/refcodes-decoupling/src/refcodes-decoupling-3.1.3/src/test/java/org/refcodes/decoupling/ComponentExtendsA2.java) 
* \[<span style="color:blue">ADDED</span>\] [`InterceptorTest.java`](https://bitbucket.org/refcodes/refcodes-decoupling/src/refcodes-decoupling-3.1.3/src/test/java/org/refcodes/decoupling/InterceptorTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-decoupling/src/refcodes-decoupling-3.1.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-decoupling/src/refcodes-decoupling-3.1.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`CircularDependencyException.java`](https://bitbucket.org/refcodes/refcodes-decoupling/src/refcodes-decoupling-3.1.3/src/main/java/org/refcodes/decoupling/CircularDependencyException.java) (see Javadoc at [`CircularDependencyException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-decoupling/3.1.3/org.refcodes.decoupling/org/refcodes/decoupling/CircularDependencyException.html))
* \[<span style="color:green">MODIFIED</span>\] [`Claim.java`](https://bitbucket.org/refcodes/refcodes-decoupling/src/refcodes-decoupling-3.1.3/src/main/java/org/refcodes/decoupling/Claim.java) (see Javadoc at [`Claim.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-decoupling/3.1.3/org.refcodes.decoupling/org/refcodes/decoupling/Claim.html))
* \[<span style="color:green">MODIFIED</span>\] [`Context.java`](https://bitbucket.org/refcodes/refcodes-decoupling/src/refcodes-decoupling-3.1.3/src/main/java/org/refcodes/decoupling/Context.java) (see Javadoc at [`Context.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-decoupling/3.1.3/org.refcodes.decoupling/org/refcodes/decoupling/Context.html))
* \[<span style="color:green">MODIFIED</span>\] [`DependencyException.java`](https://bitbucket.org/refcodes/refcodes-decoupling/src/refcodes-decoupling-3.1.3/src/main/java/org/refcodes/decoupling/DependencyException.java) (see Javadoc at [`DependencyException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-decoupling/3.1.3/org.refcodes.decoupling/org/refcodes/decoupling/DependencyException.html))
* \[<span style="color:green">MODIFIED</span>\] [`Dependency.java`](https://bitbucket.org/refcodes/refcodes-decoupling/src/refcodes-decoupling-3.1.3/src/main/java/org/refcodes/decoupling/Dependency.java) (see Javadoc at [`Dependency.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-decoupling/3.1.3/org.refcodes.decoupling/org/refcodes/decoupling/Dependency.html))
* \[<span style="color:green">MODIFIED</span>\] [`FactoryClaim.java`](https://bitbucket.org/refcodes/refcodes-decoupling/src/refcodes-decoupling-3.1.3/src/main/java/org/refcodes/decoupling/FactoryClaim.java) (see Javadoc at [`FactoryClaim.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-decoupling/3.1.3/org.refcodes.decoupling/org/refcodes/decoupling/FactoryClaim.html))
* \[<span style="color:green">MODIFIED</span>\] [`InitializerClaim.java`](https://bitbucket.org/refcodes/refcodes-decoupling/src/refcodes-decoupling-3.1.3/src/main/java/org/refcodes/decoupling/InitializerClaim.java) (see Javadoc at [`InitializerClaim.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-decoupling/3.1.3/org.refcodes.decoupling/org/refcodes/decoupling/InitializerClaim.html))
* \[<span style="color:green">MODIFIED</span>\] [`Reactor.java`](https://bitbucket.org/refcodes/refcodes-decoupling/src/refcodes-decoupling-3.1.3/src/main/java/org/refcodes/decoupling/Reactor.java) (see Javadoc at [`Reactor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-decoupling/3.1.3/org.refcodes.decoupling/org/refcodes/decoupling/Reactor.html))
* \[<span style="color:green">MODIFIED</span>\] [`UnsatisfiedClaimException.java`](https://bitbucket.org/refcodes/refcodes-decoupling/src/refcodes-decoupling-3.1.3/src/main/java/org/refcodes/decoupling/UnsatisfiedClaimException.java) (see Javadoc at [`UnsatisfiedClaimException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-decoupling/3.1.3/org.refcodes.decoupling/org/refcodes/decoupling/UnsatisfiedClaimException.html))
* \[<span style="color:green">MODIFIED</span>\] [`UnsatisfiedFactoryException.java`](https://bitbucket.org/refcodes/refcodes-decoupling/src/refcodes-decoupling-3.1.3/src/main/java/org/refcodes/decoupling/UnsatisfiedFactoryException.java) (see Javadoc at [`UnsatisfiedFactoryException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-decoupling/3.1.3/org.refcodes.decoupling/org/refcodes/decoupling/UnsatisfiedFactoryException.html))
* \[<span style="color:green">MODIFIED</span>\] [`UnsatisfiedInitializerException.java`](https://bitbucket.org/refcodes/refcodes-decoupling/src/refcodes-decoupling-3.1.3/src/main/java/org/refcodes/decoupling/UnsatisfiedInitializerException.java) (see Javadoc at [`UnsatisfiedInitializerException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-decoupling/3.1.3/org.refcodes.decoupling/org/refcodes/decoupling/UnsatisfiedInitializerException.html))
* \[<span style="color:green">MODIFIED</span>\] [`createdFiles.lst`](https://bitbucket.org/refcodes/refcodes-decoupling/src/refcodes-decoupling-3.1.3/target/maven-status/maven-compiler-plugin/compile/default-compile/createdFiles.lst) 
* \[<span style="color:green">MODIFIED</span>\] [`inputFiles.lst`](https://bitbucket.org/refcodes/refcodes-decoupling/src/refcodes-decoupling-3.1.3/target/maven-status/maven-compiler-plugin/compile/default-compile/inputFiles.lst) 

## Change list &lt;refcodes-decoupling-ext&gt; (version 3.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-decoupling-ext/src/refcodes-decoupling-ext-3.1.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-decoupling-ext/src/refcodes-decoupling-ext-3.1.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-decoupling-ext/src/refcodes-decoupling-ext-3.1.3/refcodes-decoupling-ext-application/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-decoupling-ext/src/refcodes-decoupling-ext-3.1.3/refcodes-decoupling-ext-application/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`ApplicationReactor.java`](https://bitbucket.org/refcodes/refcodes-decoupling-ext/src/refcodes-decoupling-ext-3.1.3/refcodes-decoupling-ext-application/src/main/java/org/refcodes/decoupling/ext/application/ApplicationReactor.java) (see Javadoc at [`ApplicationReactor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-decoupling-ext-application/3.1.3/org.refcodes.decoupling.ext.application/org/refcodes/decoupling/ext/application/ApplicationReactor.html))

## Change list &lt;refcodes-filesystem&gt; (version 3.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-filesystem/src/refcodes-filesystem-3.1.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-filesystem/src/refcodes-filesystem-3.1.3/README.md) 

## Change list &lt;refcodes-filesystem-alt&gt; (version 3.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-filesystem-alt/src/refcodes-filesystem-alt-3.1.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-filesystem-alt/src/refcodes-filesystem-alt-3.1.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-filesystem-alt/src/refcodes-filesystem-alt-3.1.3/refcodes-filesystem-alt-s3/pom.xml) 

## Change list &lt;refcodes-forwardsecrecy&gt; (version 3.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy/src/refcodes-forwardsecrecy-3.1.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy/src/refcodes-forwardsecrecy-3.1.3/README.md) 

## Change list &lt;refcodes-forwardsecrecy-alt&gt; (version 3.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-3.1.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-3.1.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-3.1.3/refcodes-forwardsecrecy-alt-filesystem/pom.xml) 

## Change list &lt;refcodes-io-ext&gt; (version 3.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-3.1.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-3.1.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-3.1.3/refcodes-io-ext-observer/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-3.1.3/refcodes-io-ext-observer/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`ObservableConnectionReceiver.java`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-3.1.3/refcodes-io-ext-observer/src/main/java/org/refcodes/io/ext/observer/ObservableConnectionReceiver.java) (see Javadoc at [`ObservableConnectionReceiver.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-io-ext-observer/3.1.3/org.refcodes.io.ext.observer/org/refcodes/io/ext/observer/ObservableConnectionReceiver.html))
* \[<span style="color:green">MODIFIED</span>\] [`ObservableConnectionRequestReceiver.java`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-3.1.3/refcodes-io-ext-observer/src/main/java/org/refcodes/io/ext/observer/ObservableConnectionRequestReceiver.java) (see Javadoc at [`ObservableConnectionRequestReceiver.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-io-ext-observer/3.1.3/org.refcodes.io.ext.observer/org/refcodes/io/ext/observer/ObservableConnectionRequestReceiver.html))
* \[<span style="color:green">MODIFIED</span>\] [`ObservableConnectionRequestTransmitter.java`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-3.1.3/refcodes-io-ext-observer/src/main/java/org/refcodes/io/ext/observer/ObservableConnectionRequestTransmitter.java) (see Javadoc at [`ObservableConnectionRequestTransmitter.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-io-ext-observer/3.1.3/org.refcodes.io.ext.observer/org/refcodes/io/ext/observer/ObservableConnectionRequestTransmitter.html))
* \[<span style="color:green">MODIFIED</span>\] [`ObservableConnectionTransmitter.java`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-3.1.3/refcodes-io-ext-observer/src/main/java/org/refcodes/io/ext/observer/ObservableConnectionTransmitter.java) (see Javadoc at [`ObservableConnectionTransmitter.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-io-ext-observer/3.1.3/org.refcodes.io.ext.observer/org/refcodes/io/ext/observer/ObservableConnectionTransmitter.html))

## Change list &lt;refcodes-jobbus&gt; (version 3.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-jobbus/src/refcodes-jobbus-3.1.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-jobbus/src/refcodes-jobbus-3.1.3/README.md) 

## Change list &lt;refcodes-rest-ext&gt; (version 3.1.3)

* \[<span style="color:blue">ADDED</span>\] [`RestfulEurekaClient.java`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-3.1.3/refcodes-rest-ext-eureka/src/main/java/org/refcodes/rest/ext/eureka/RestfulEurekaClient.java) (see Javadoc at [`RestfulEurekaClient.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest-ext-eureka/3.1.3/org.refcodes.rest.ext.eureka/org/refcodes/rest/ext/eureka/RestfulEurekaClient.html))
* \[<span style="color:blue">ADDED</span>\] [`RestfulEurekaServer.java`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-3.1.3/refcodes-rest-ext-eureka/src/main/java/org/refcodes/rest/ext/eureka/RestfulEurekaServer.java) (see Javadoc at [`RestfulEurekaServer.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest-ext-eureka/3.1.3/org.refcodes.rest.ext.eureka/org/refcodes/rest/ext/eureka/RestfulEurekaServer.html))
* \[<span style="color:red">DELETED</span>\] `EurekaRestClientImpl.java`
* \[<span style="color:red">DELETED</span>\] `EurekaRestServerImpl.java`
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-3.1.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-3.1.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-3.1.3/refcodes-rest-ext-eureka/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-3.1.3/refcodes-rest-ext-eureka/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`EurekaDiscoverySidecar.java`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-3.1.3/refcodes-rest-ext-eureka/src/main/java/org/refcodes/rest/ext/eureka/EurekaDiscoverySidecar.java) (see Javadoc at [`EurekaDiscoverySidecar.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest-ext-eureka/3.1.3/org.refcodes.rest.ext.eureka/org/refcodes/rest/ext/eureka/EurekaDiscoverySidecar.html))
* \[<span style="color:green">MODIFIED</span>\] [`EurekaRegistry.java`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-3.1.3/refcodes-rest-ext-eureka/src/main/java/org/refcodes/rest/ext/eureka/EurekaRegistry.java) (see Javadoc at [`EurekaRegistry.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest-ext-eureka/3.1.3/org.refcodes.rest.ext.eureka/org/refcodes/rest/ext/eureka/EurekaRegistry.html))
* \[<span style="color:green">MODIFIED</span>\] [`EurekaRegistrySidecar.java`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-3.1.3/refcodes-rest-ext-eureka/src/main/java/org/refcodes/rest/ext/eureka/EurekaRegistrySidecar.java) (see Javadoc at [`EurekaRegistrySidecar.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest-ext-eureka/3.1.3/org.refcodes.rest.ext.eureka/org/refcodes/rest/ext/eureka/EurekaRegistrySidecar.html))
* \[<span style="color:green">MODIFIED</span>\] [`EurekaRestClientDecorator.java`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-3.1.3/refcodes-rest-ext-eureka/src/main/java/org/refcodes/rest/ext/eureka/EurekaRestClientDecorator.java) (see Javadoc at [`EurekaRestClientDecorator.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest-ext-eureka/3.1.3/org.refcodes.rest.ext.eureka/org/refcodes/rest/ext/eureka/EurekaRestClientDecorator.html))
* \[<span style="color:green">MODIFIED</span>\] [`EurekaRestClient.java`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-3.1.3/refcodes-rest-ext-eureka/src/main/java/org/refcodes/rest/ext/eureka/EurekaRestClient.java) (see Javadoc at [`EurekaRestClient.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest-ext-eureka/3.1.3/org.refcodes.rest.ext.eureka/org/refcodes/rest/ext/eureka/EurekaRestClient.html))
* \[<span style="color:green">MODIFIED</span>\] [`EurekaRestServerDecorator.java`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-3.1.3/refcodes-rest-ext-eureka/src/main/java/org/refcodes/rest/ext/eureka/EurekaRestServerDecorator.java) (see Javadoc at [`EurekaRestServerDecorator.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest-ext-eureka/3.1.3/org.refcodes.rest.ext.eureka/org/refcodes/rest/ext/eureka/EurekaRestServerDecorator.html))
* \[<span style="color:green">MODIFIED</span>\] [`EurekaRestServer.java`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-3.1.3/refcodes-rest-ext-eureka/src/main/java/org/refcodes/rest/ext/eureka/EurekaRestServer.java) (see Javadoc at [`EurekaRestServer.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest-ext-eureka/3.1.3/org.refcodes.rest.ext.eureka/org/refcodes/rest/ext/eureka/EurekaRestServer.html))
* \[<span style="color:green">MODIFIED</span>\] [`EurekaDiscoverySidecarTest.java`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-3.1.3/refcodes-rest-ext-eureka/src/test/java/org/refcodes/rest/ext/eureka/EurekaDiscoverySidecarTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`EurekaRestClientTest.java`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-3.1.3/refcodes-rest-ext-eureka/src/test/java/org/refcodes/rest/ext/eureka/EurekaRestClientTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`EurekaRestServerTest.java`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-3.1.3/refcodes-rest-ext-eureka/src/test/java/org/refcodes/rest/ext/eureka/EurekaRestServerTest.java) 

## Change list &lt;refcodes-remoting&gt; (version 3.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-remoting/src/refcodes-remoting-3.1.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-remoting/src/refcodes-remoting-3.1.3/README.md) 

## Change list &lt;refcodes-remoting-ext&gt; (version 3.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-3.1.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-3.1.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-3.1.3/refcodes-remoting-ext-observer/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-3.1.3/refcodes-remoting-ext-observer/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`ObservableRemoteClient.java`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-3.1.3/refcodes-remoting-ext-observer/src/main/java/org/refcodes/remoting/ext/observer/ObservableRemoteClient.java) (see Javadoc at [`ObservableRemoteClient.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-remoting-ext-observer/3.1.3/org.refcodes.remoting.ext.observer/org/refcodes/remoting/ext/observer/ObservableRemoteClient.html))
* \[<span style="color:green">MODIFIED</span>\] [`ObservableRemoteServer.java`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-3.1.3/refcodes-remoting-ext-observer/src/main/java/org/refcodes/remoting/ext/observer/ObservableRemoteServer.java) (see Javadoc at [`ObservableRemoteServer.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-remoting-ext-observer/3.1.3/org.refcodes.remoting.ext.observer/org/refcodes/remoting/ext/observer/ObservableRemoteServer.html))

## Change list &lt;refcodes-serial&gt; (version 3.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial/src/refcodes-serial-3.1.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-serial/src/refcodes-serial-3.1.3/README.md) 

## Change list &lt;refcodes-serial-alt&gt; (version 3.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial-alt/src/refcodes-serial-alt-3.1.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-serial-alt/src/refcodes-serial-alt-3.1.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial-alt/src/refcodes-serial-alt-3.1.3/refcodes-serial-alt-net/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-serial-alt/src/refcodes-serial-alt-3.1.3/refcodes-serial-alt-net/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial-alt/src/refcodes-serial-alt-3.1.3/refcodes-serial-alt-tty/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-serial-alt/src/refcodes-serial-alt-3.1.3/refcodes-serial-alt-tty/README.md) 

## Change list &lt;refcodes-serial-ext&gt; (version 3.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-3.1.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-3.1.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-3.1.3/refcodes-serial-ext-handshake/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-3.1.3/refcodes-serial-ext-handshake/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-3.1.3/refcodes-serial-ext-observer/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-3.1.3/refcodes-serial-ext-observer/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`ObservablePayloadSectionDecorator.java`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-3.1.3/refcodes-serial-ext-observer/src/main/java/org/refcodes/serial/ext/observer/ObservablePayloadSectionDecorator.java) (see Javadoc at [`ObservablePayloadSectionDecorator.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-serial-ext-observer/3.1.3/org.refcodes.serial.ext.observer/org/refcodes/serial/ext/observer/ObservablePayloadSectionDecorator.html))
* \[<span style="color:green">MODIFIED</span>\] [`ObservablePayloadSegmentDecorator.java`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-3.1.3/refcodes-serial-ext-observer/src/main/java/org/refcodes/serial/ext/observer/ObservablePayloadSegmentDecorator.java) (see Javadoc at [`ObservablePayloadSegmentDecorator.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-serial-ext-observer/3.1.3/org.refcodes.serial.ext.observer/org/refcodes/serial/ext/observer/ObservablePayloadSegmentDecorator.html))
* \[<span style="color:green">MODIFIED</span>\] [`ObservableSectionDecorator.java`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-3.1.3/refcodes-serial-ext-observer/src/main/java/org/refcodes/serial/ext/observer/ObservableSectionDecorator.java) (see Javadoc at [`ObservableSectionDecorator.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-serial-ext-observer/3.1.3/org.refcodes.serial.ext.observer/org/refcodes/serial/ext/observer/ObservableSectionDecorator.html))
* \[<span style="color:green">MODIFIED</span>\] [`ObservableSegmentDecorator.java`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-3.1.3/refcodes-serial-ext-observer/src/main/java/org/refcodes/serial/ext/observer/ObservableSegmentDecorator.java) (see Javadoc at [`ObservableSegmentDecorator.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-serial-ext-observer/3.1.3/org.refcodes.serial.ext.observer/org/refcodes/serial/ext/observer/ObservableSegmentDecorator.html))
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-3.1.3/refcodes-serial-ext-security/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-3.1.3/refcodes-serial-ext-security/README.md) 

## Change list &lt;refcodes-p2p&gt; (version 3.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-p2p/src/refcodes-p2p-3.1.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-p2p/src/refcodes-p2p-3.1.3/README.md) 

## Change list &lt;refcodes-p2p-alt&gt; (version 3.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-p2p-alt/src/refcodes-p2p-alt-3.1.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-p2p-alt/src/refcodes-p2p-alt-3.1.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-p2p-alt/src/refcodes-p2p-alt-3.1.3/refcodes-p2p-alt-rest/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-p2p-alt/src/refcodes-p2p-alt-3.1.3/refcodes-p2p-alt-serial/pom.xml) 

## Change list &lt;refcodes-p2p-ext&gt; (version 3.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-p2p-ext/src/refcodes-p2p-ext-3.1.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-p2p-ext/src/refcodes-p2p-ext-3.1.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-p2p-ext/src/refcodes-p2p-ext-3.1.3/refcodes-p2p-ext-observer/pom.xml) 

## Change list &lt;refcodes-servicebus&gt; (version 3.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-servicebus/src/refcodes-servicebus-3.1.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-servicebus/src/refcodes-servicebus-3.1.3/README.md) 

## Change list &lt;refcodes-servicebus-alt&gt; (version 3.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-servicebus-alt/src/refcodes-servicebus-alt-3.1.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-servicebus-alt/src/refcodes-servicebus-alt-3.1.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-servicebus-alt/src/refcodes-servicebus-alt-3.1.3/refcodes-servicebus-alt-spring/pom.xml) 

## Change list &lt;refcodes-tabular-alt&gt; (version 3.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-tabular-alt/src/refcodes-tabular-alt-3.1.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-tabular-alt/src/refcodes-tabular-alt-3.1.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-tabular-alt/src/refcodes-tabular-alt-3.1.3/refcodes-tabular-alt-forwardsecrecy/pom.xml) 

## Change list &lt;refcodes-archetype&gt; (version 3.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype/src/refcodes-archetype-3.1.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype/src/refcodes-archetype-3.1.3/README.md) 

## Change list &lt;refcodes-archetype-alt&gt; (version 3.1.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.1.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.1.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.1.3/refcodes-archetype-alt-c2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.1.3/refcodes-archetype-alt-c2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.1.3/refcodes-archetype-alt-c2/src/main/resources/archetype-resources/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`Main.java`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.1.3/refcodes-archetype-alt-c2/src/main/resources/archetype-resources/src/main/java/Main.java) (see Javadoc at [`Main.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-archetype-alt-c2/src/main/resources/archetype-resources/3.1.3/src.main.resources.archetype-resources/Main.html))
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.1.3/refcodes-archetype-alt-cli/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.1.3/refcodes-archetype-alt-cli/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.1.3/refcodes-archetype-alt-cli/src/main/resources/archetype-resources/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.1.3/refcodes-archetype-alt-csv/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.1.3/refcodes-archetype-alt-csv/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.1.3/refcodes-archetype-alt-csv/src/main/resources/archetype-resources/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.1.3/refcodes-archetype-alt-decoupling/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.1.3/refcodes-archetype-alt-decoupling/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.1.3/refcodes-archetype-alt-decoupling/src/main/resources/archetype-resources/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.1.3/refcodes-archetype-alt-eventbus/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.1.3/refcodes-archetype-alt-eventbus/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.1.3/refcodes-archetype-alt-eventbus/src/main/resources/archetype-resources/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.1.3/refcodes-archetype-alt-filter/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.1.3/refcodes-archetype-alt-filter/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.1.3/refcodes-archetype-alt-filter/src/main/resources/archetype-resources/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.1.3/refcodes-archetype-alt-rest/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.1.3/refcodes-archetype-alt-rest/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.1.3/refcodes-archetype-alt-rest/src/main/resources/archetype-resources/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`Main.java`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.1.3/refcodes-archetype-alt-rest/src/main/resources/archetype-resources/src/main/java/Main.java) (see Javadoc at [`Main.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-archetype-alt-rest/src/main/resources/archetype-resources/3.1.3/src.main.resources.archetype-resources/Main.html))
