> This change list has been auto-generated on `triton` by `steiner` with `changelist-all.sh` on the 2022-06-20 at 19:45:22.

## Change list &lt;refcodes-licensing&gt; (version 3.0.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-licensing/src/refcodes-licensing-3.0.3/pom.xml) 

## Change list &lt;refcodes-parent&gt; (version 3.0.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-parent/src/refcodes-parent-3.0.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-parent/src/refcodes-parent-3.0.3/README.md) 

## Change list &lt;refcodes-time&gt; (version 3.0.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-time/src/refcodes-time-3.0.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-time/src/refcodes-time-3.0.3/README.md) 

## Change list &lt;refcodes-mixin&gt; (version 3.0.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-mixin/src/refcodes-mixin-3.0.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-mixin/src/refcodes-mixin-3.0.3/README.md) 

## Change list &lt;refcodes-data&gt; (version 3.0.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data/src/refcodes-data-3.0.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data/src/refcodes-data-3.0.3/README.md) 

## Change list &lt;refcodes-exception&gt; (version 3.0.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-exception/src/refcodes-exception-3.0.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-exception/src/refcodes-exception-3.0.3/README.md) 

## Change list &lt;refcodes-factory&gt; (version 3.0.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-factory/src/refcodes-factory-3.0.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-factory/src/refcodes-factory-3.0.3/README.md) 

## Change list &lt;refcodes-factory-alt&gt; (version 3.0.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-factory-alt/src/refcodes-factory-alt-3.0.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-factory-alt/src/refcodes-factory-alt-3.0.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-factory-alt/src/refcodes-factory-alt-3.0.3/refcodes-factory-alt-spring/pom.xml) 

## Change list &lt;refcodes-controlflow&gt; (version 3.0.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-controlflow/src/refcodes-controlflow-3.0.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-controlflow/src/refcodes-controlflow-3.0.3/README.md) 

## Change list &lt;refcodes-numerical&gt; (version 3.0.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-numerical/src/refcodes-numerical-3.0.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-numerical/src/refcodes-numerical-3.0.3/README.md) 

## Change list &lt;refcodes-generator&gt; (version 3.0.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-generator/src/refcodes-generator-3.0.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-generator/src/refcodes-generator-3.0.3/README.md) 

## Change list &lt;refcodes-matcher&gt; (version 3.0.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-matcher/src/refcodes-matcher-3.0.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-matcher/src/refcodes-matcher-3.0.3/README.md) 

## Change list &lt;refcodes-struct&gt; (version 3.0.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-struct/src/refcodes-struct-3.0.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-struct/src/refcodes-struct-3.0.3/README.md) 

## Change list &lt;refcodes-struct-ext&gt; (version 3.0.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-struct-ext/src/refcodes-struct-ext-3.0.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-struct-ext/src/refcodes-struct-ext-3.0.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-struct-ext/src/refcodes-struct-ext-3.0.3/refcodes-struct-ext-factory/pom.xml) 

## Change list &lt;refcodes-runtime&gt; (version 3.0.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-3.0.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-3.0.3/README.md) 

## Change list &lt;refcodes-component&gt; (version 3.0.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.0.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.0.3/README.md) 

## Change list &lt;refcodes-data-ext&gt; (version 3.0.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-3.0.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-3.0.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-3.0.3/refcodes-data-ext-checkers/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-3.0.3/refcodes-data-ext-checkers/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-3.0.3/refcodes-data-ext-chess/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-3.0.3/refcodes-data-ext-chess/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-3.0.3/refcodes-data-ext-corporate/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-3.0.3/refcodes-data-ext-corporate/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-3.0.3/refcodes-data-ext-symbols/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-3.0.3/refcodes-data-ext-symbols/README.md) 

## Change list &lt;refcodes-graphical&gt; (version 3.0.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-graphical/src/refcodes-graphical-3.0.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-graphical/src/refcodes-graphical-3.0.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`PositionImpl.java`](https://bitbucket.org/refcodes/refcodes-graphical/src/refcodes-graphical-3.0.3/src/main/java/org/refcodes/graphical/PositionImpl.java) (see Javadoc at [`PositionImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-graphical/3.0.3/org.refcodes.graphical/org/refcodes/graphical/PositionImpl.html))

## Change list &lt;refcodes-textual&gt; (version 3.0.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-3.0.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-3.0.3/README.md) 

## Change list &lt;refcodes-criteria&gt; (version 3.0.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-3.0.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-3.0.3/README.md) 

## Change list &lt;refcodes-io&gt; (version 3.0.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-io/src/refcodes-io-3.0.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-io/src/refcodes-io-3.0.3/README.md) 

## Change list &lt;refcodes-tabular&gt; (version 3.0.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-tabular/src/refcodes-tabular-3.0.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-tabular/src/refcodes-tabular-3.0.3/README.md) 

## Change list &lt;refcodes-observer&gt; (version 3.0.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-observer/src/refcodes-observer-3.0.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-observer/src/refcodes-observer-3.0.3/README.md) 

## Change list &lt;refcodes-command&gt; (version 3.0.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-command/src/refcodes-command-3.0.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-command/src/refcodes-command-3.0.3/README.md) 

## Change list &lt;refcodes-cli&gt; (version 3.0.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.0.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.0.3/README.md) 

## Change list &lt;refcodes-audio&gt; (version 3.0.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-audio/src/refcodes-audio-3.0.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-audio/src/refcodes-audio-3.0.3/README.md) 

## Change list &lt;refcodes-codec&gt; (version 3.0.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-3.0.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-3.0.3/README.md) 

## Change list &lt;refcodes-component-ext&gt; (version 3.0.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-3.0.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-3.0.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-3.0.3/refcodes-component-ext-observer/pom.xml) 

## Change list &lt;refcodes-properties&gt; (version 3.0.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-3.0.3/pom.xml) 

## Change list &lt;refcodes-security&gt; (version 3.0.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security/src/refcodes-security-3.0.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security/src/refcodes-security-3.0.3/README.md) 

## Change list &lt;refcodes-security-alt&gt; (version 3.0.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-3.0.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-3.0.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-3.0.3/refcodes-security-alt-chaos/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-3.0.3/refcodes-security-alt-chaos/README.md) 

## Change list &lt;refcodes-security-ext&gt; (version 3.0.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-3.0.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-3.0.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-3.0.3/refcodes-security-ext-chaos/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-3.0.3/refcodes-security-ext-chaos/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-3.0.3/refcodes-security-ext-spring/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-3.0.3/refcodes-security-ext-spring/README.md) 

## Change list &lt;refcodes-properties-ext&gt; (version 3.0.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.0.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.0.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.0.3/refcodes-properties-ext-cli/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.0.3/refcodes-properties-ext-cli/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.0.3/refcodes-properties-ext-obfuscation/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.0.3/refcodes-properties-ext-obfuscation/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.0.3/refcodes-properties-ext-observer/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.0.3/refcodes-properties-ext-observer/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.0.3/refcodes-properties-ext-runtime/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.0.3/refcodes-properties-ext-runtime/README.md) 

## Change list &lt;refcodes-logger&gt; (version 3.0.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger/src/refcodes-logger-3.0.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger/src/refcodes-logger-3.0.3/README.md) 

## Change list &lt;refcodes-logger-alt&gt; (version 3.0.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.0.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.0.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.0.3/refcodes-logger-alt-async/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.0.3/refcodes-logger-alt-async/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.0.3/refcodes-logger-alt-console/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.0.3/refcodes-logger-alt-console/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.0.3/refcodes-logger-alt-io/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.0.3/refcodes-logger-alt-io/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.0.3/refcodes-logger-alt-simpledb/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.0.3/refcodes-logger-alt-simpledb/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.0.3/refcodes-logger-alt-slf4j/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.0.3/refcodes-logger-alt-spring/pom.xml) 

## Change list &lt;refcodes-logger-ext&gt; (version 3.0.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-3.0.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-3.0.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-3.0.3/refcodes-logger-ext-slf4j/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-3.0.3/refcodes-logger-ext-slf4j/README.md) 

## Change list &lt;refcodes-graphical-ext&gt; (version 3.0.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-3.0.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-3.0.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-3.0.3/refcodes-graphical-ext-javafx/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`AbstractFxGridViewportPane.java`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-3.0.3/refcodes-graphical-ext-javafx/src/main/java/org/refcodes/graphical/ext/javafx/AbstractFxGridViewportPane.java) (see Javadoc at [`AbstractFxGridViewportPane.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-graphical-ext-javafx/3.0.3/org.refcodes.graphical.ext.javafx/org/refcodes/graphical/ext/javafx/AbstractFxGridViewportPane.html))
* \[<span style="color:green">MODIFIED</span>\] [`FxGridDragSpriteEventHandler.java`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-3.0.3/refcodes-graphical-ext-javafx/src/main/java/org/refcodes/graphical/ext/javafx/FxGridDragSpriteEventHandler.java) (see Javadoc at [`FxGridDragSpriteEventHandler.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-graphical-ext-javafx/3.0.3/org.refcodes.graphical.ext.javafx/org/refcodes/graphical/ext/javafx/FxGridDragSpriteEventHandler.html))

## Change list &lt;refcodes-checkerboard&gt; (version 3.0.3)

* \[<span style="color:blue">ADDED</span>\] [`GridPositionClickedEvent.java`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-3.0.3/src/main/java/org/refcodes/checkerboard/GridPositionClickedEvent.java) (see Javadoc at [`GridPositionClickedEvent.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-checkerboard/3.0.3/org.refcodes.checkerboard/org/refcodes/checkerboard/GridPositionClickedEvent.html))
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-3.0.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-3.0.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`AbstractCheckerboard.java`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-3.0.3/src/main/java/org/refcodes/checkerboard/AbstractCheckerboard.java) (see Javadoc at [`AbstractCheckerboard.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-checkerboard/3.0.3/org.refcodes.checkerboard/org/refcodes/checkerboard/AbstractCheckerboard.html))
* \[<span style="color:green">MODIFIED</span>\] [`AbstractPlayer.java`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-3.0.3/src/main/java/org/refcodes/checkerboard/AbstractPlayer.java) (see Javadoc at [`AbstractPlayer.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-checkerboard/3.0.3/org.refcodes.checkerboard/org/refcodes/checkerboard/AbstractPlayer.html))
* \[<span style="color:green">MODIFIED</span>\] [`CheckerboardAction.java`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-3.0.3/src/main/java/org/refcodes/checkerboard/CheckerboardAction.java) (see Javadoc at [`CheckerboardAction.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-checkerboard/3.0.3/org.refcodes.checkerboard/org/refcodes/checkerboard/CheckerboardAction.html))
* \[<span style="color:green">MODIFIED</span>\] [`CheckerboardObserver.java`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-3.0.3/src/main/java/org/refcodes/checkerboard/CheckerboardObserver.java) (see Javadoc at [`CheckerboardObserver.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-checkerboard/3.0.3/org.refcodes.checkerboard/org/refcodes/checkerboard/CheckerboardObserver.html))
* \[<span style="color:green">MODIFIED</span>\] [`ConsoleCheckerboardViewer.java`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-3.0.3/src/main/java/org/refcodes/checkerboard/ConsoleCheckerboardViewer.java) (see Javadoc at [`ConsoleCheckerboardViewer.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-checkerboard/3.0.3/org.refcodes.checkerboard/org/refcodes/checkerboard/ConsoleCheckerboardViewer.html))
* \[<span style="color:green">MODIFIED</span>\] [`GridDimensionChangedEvent.java`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-3.0.3/src/main/java/org/refcodes/checkerboard/GridDimensionChangedEvent.java) (see Javadoc at [`GridDimensionChangedEvent.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-checkerboard/3.0.3/org.refcodes.checkerboard/org/refcodes/checkerboard/GridDimensionChangedEvent.html))
* \[<span style="color:green">MODIFIED</span>\] [`Player.java`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-3.0.3/src/main/java/org/refcodes/checkerboard/Player.java) (see Javadoc at [`Player.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-checkerboard/3.0.3/org.refcodes.checkerboard/org/refcodes/checkerboard/Player.html))
* \[<span style="color:green">MODIFIED</span>\] [`PlayerObserver.java`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-3.0.3/src/main/java/org/refcodes/checkerboard/PlayerObserver.java) (see Javadoc at [`PlayerObserver.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-checkerboard/3.0.3/org.refcodes.checkerboard/org/refcodes/checkerboard/PlayerObserver.html))

## Change list &lt;refcodes-checkerboard-alt&gt; (version 3.0.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-alt/src/refcodes-checkerboard-alt-3.0.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-alt/src/refcodes-checkerboard-alt-3.0.3/refcodes-checkerboard-alt-javafx/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`FxCheckerboardViewer.java`](https://bitbucket.org/refcodes/refcodes-checkerboard-alt/src/refcodes-checkerboard-alt-3.0.3/refcodes-checkerboard-alt-javafx/src/main/java/org/refcodes/checkerboard/alt/javafx/FxCheckerboardViewer.java) (see Javadoc at [`FxCheckerboardViewer.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-checkerboard-alt-javafx/3.0.3/org.refcodes.checkerboard.alt.javafx/org/refcodes/checkerboard/alt/javafx/FxCheckerboardViewer.html))

## Change list &lt;refcodes-net&gt; (version 3.0.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-3.0.3/pom.xml) 

## Change list &lt;refcodes-web&gt; (version 3.0.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-web/src/refcodes-web-3.0.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`GrantType.java`](https://bitbucket.org/refcodes/refcodes-web/src/refcodes-web-3.0.3/src/main/java/org/refcodes/web/GrantType.java) (see Javadoc at [`GrantType.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-web/3.0.3/org.refcodes.web/org/refcodes/web/GrantType.html))
* \[<span style="color:green">MODIFIED</span>\] [`HttpClientResponse.java`](https://bitbucket.org/refcodes/refcodes-web/src/refcodes-web-3.0.3/src/main/java/org/refcodes/web/HttpClientResponse.java) (see Javadoc at [`HttpClientResponse.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-web/3.0.3/org.refcodes.web/org/refcodes/web/HttpClientResponse.html))
* \[<span style="color:green">MODIFIED</span>\] [`MediaType.java`](https://bitbucket.org/refcodes/refcodes-web/src/refcodes-web-3.0.3/src/main/java/org/refcodes/web/MediaType.java) (see Javadoc at [`MediaType.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-web/3.0.3/org.refcodes.web/org/refcodes/web/MediaType.html))
* \[<span style="color:green">MODIFIED</span>\] [`OauthField.java`](https://bitbucket.org/refcodes/refcodes-web/src/refcodes-web-3.0.3/src/main/java/org/refcodes/web/OauthField.java) (see Javadoc at [`OauthField.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-web/3.0.3/org.refcodes.web/org/refcodes/web/OauthField.html))
* \[<span style="color:green">MODIFIED</span>\] [`OauthToken.java`](https://bitbucket.org/refcodes/refcodes-web/src/refcodes-web-3.0.3/src/main/java/org/refcodes/web/OauthToken.java) (see Javadoc at [`OauthToken.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-web/3.0.3/org.refcodes.web/org/refcodes/web/OauthToken.html))
* \[<span style="color:green">MODIFIED</span>\] [`TokenType.java`](https://bitbucket.org/refcodes/refcodes-web/src/refcodes-web-3.0.3/src/main/java/org/refcodes/web/TokenType.java) (see Javadoc at [`TokenType.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-web/3.0.3/org.refcodes.web/org/refcodes/web/TokenType.html))

## Change list &lt;refcodes-rest&gt; (version 3.0.3)

* \[<span style="color:blue">ADDED</span>\] [`OauthTokenHandlerTest.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-3.0.3/src/test/java/org/refcodes/rest/OauthTokenHandlerTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-3.0.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-3.0.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`HttpRestClient.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-3.0.3/src/main/java/org/refcodes/rest/HttpRestClient.java) (see Javadoc at [`HttpRestClient.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/3.0.3/org.refcodes.rest/org/refcodes/rest/HttpRestClient.html))
* \[<span style="color:green">MODIFIED</span>\] [`HttpRestServerImpl.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-3.0.3/src/main/java/org/refcodes/rest/HttpRestServerImpl.java) (see Javadoc at [`HttpRestServerImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/3.0.3/org.refcodes.rest/org/refcodes/rest/HttpRestServerImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`OauthTokenHandler.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-3.0.3/src/main/java/org/refcodes/rest/OauthTokenHandler.java) (see Javadoc at [`OauthTokenHandler.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/3.0.3/org.refcodes.rest/org/refcodes/rest/OauthTokenHandler.html))

## Change list &lt;refcodes-hal&gt; (version 3.0.3)

* \[<span style="color:red">DELETED</span>\] `HalClientImpl.java`
* \[<span style="color:red">DELETED</span>\] `HalDataImpl.java`
* \[<span style="color:red">DELETED</span>\] `HalDataPageImpl.java`
* \[<span style="color:red">DELETED</span>\] `HalMapImpl.java`
* \[<span style="color:red">DELETED</span>\] `HalStructImpl.java`
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-3.0.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-3.0.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`HalClient.java`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-3.0.3/src/main/java/org/refcodes/hal/HalClient.java) (see Javadoc at [`HalClient.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-hal/3.0.3/org.refcodes.hal/org/refcodes/hal/HalClient.html))
* \[<span style="color:green">MODIFIED</span>\] [`HalData.java`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-3.0.3/src/main/java/org/refcodes/hal/HalData.java) (see Javadoc at [`HalData.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-hal/3.0.3/org.refcodes.hal/org/refcodes/hal/HalData.html))
* \[<span style="color:green">MODIFIED</span>\] [`HalDataPage.java`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-3.0.3/src/main/java/org/refcodes/hal/HalDataPage.java) (see Javadoc at [`HalDataPage.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-hal/3.0.3/org.refcodes.hal/org/refcodes/hal/HalDataPage.html))
* \[<span style="color:green">MODIFIED</span>\] [`HalMap.java`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-3.0.3/src/main/java/org/refcodes/hal/HalMap.java) (see Javadoc at [`HalMap.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-hal/3.0.3/org.refcodes.hal/org/refcodes/hal/HalMap.html))
* \[<span style="color:green">MODIFIED</span>\] [`HalStruct.java`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-3.0.3/src/main/java/org/refcodes/hal/HalStruct.java) (see Javadoc at [`HalStruct.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-hal/3.0.3/org.refcodes.hal/org/refcodes/hal/HalStruct.html))
* \[<span style="color:green">MODIFIED</span>\] [`HalClientTest.java`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-3.0.3/src/test/java/org/refcodes/hal/HalClientTest.java) 

## Change list &lt;refcodes-eventbus&gt; (version 3.0.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-eventbus/src/refcodes-eventbus-3.0.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-eventbus/src/refcodes-eventbus-3.0.3/README.md) 

## Change list &lt;refcodes-eventbus-ext&gt; (version 3.0.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-3.0.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-3.0.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-3.0.3/refcodes-eventbus-ext-application/pom.xml) 

## Change list &lt;refcodes-filesystem&gt; (version 3.0.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-filesystem/src/refcodes-filesystem-3.0.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-filesystem/src/refcodes-filesystem-3.0.3/README.md) 

## Change list &lt;refcodes-filesystem-alt&gt; (version 3.0.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-filesystem-alt/src/refcodes-filesystem-alt-3.0.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-filesystem-alt/src/refcodes-filesystem-alt-3.0.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-filesystem-alt/src/refcodes-filesystem-alt-3.0.3/refcodes-filesystem-alt-s3/pom.xml) 

## Change list &lt;refcodes-forwardsecrecy&gt; (version 3.0.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy/src/refcodes-forwardsecrecy-3.0.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy/src/refcodes-forwardsecrecy-3.0.3/README.md) 

## Change list &lt;refcodes-forwardsecrecy-alt&gt; (version 3.0.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-3.0.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-3.0.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-3.0.3/refcodes-forwardsecrecy-alt-filesystem/pom.xml) 

## Change list &lt;refcodes-io-ext&gt; (version 3.0.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-3.0.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-3.0.3/refcodes-io-ext-observer/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-3.0.3/refcodes-io-ext-observer/README.md) 

## Change list &lt;refcodes-interceptor&gt; (version 3.0.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-interceptor/src/refcodes-interceptor-3.0.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-interceptor/src/refcodes-interceptor-3.0.3/README.md) 

## Change list &lt;refcodes-jobbus&gt; (version 3.0.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-jobbus/src/refcodes-jobbus-3.0.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-jobbus/src/refcodes-jobbus-3.0.3/README.md) 

## Change list &lt;refcodes-rest-ext&gt; (version 3.0.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-3.0.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-3.0.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-3.0.3/refcodes-rest-ext-eureka/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-3.0.3/refcodes-rest-ext-eureka/README.md) 

## Change list &lt;refcodes-remoting&gt; (version 3.0.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-remoting/src/refcodes-remoting-3.0.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-remoting/src/refcodes-remoting-3.0.3/README.md) 

## Change list &lt;refcodes-remoting-ext&gt; (version 3.0.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-3.0.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-3.0.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-3.0.3/refcodes-remoting-ext-observer/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-3.0.3/refcodes-remoting-ext-observer/README.md) 

## Change list &lt;refcodes-serial&gt; (version 3.0.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial/src/refcodes-serial-3.0.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-serial/src/refcodes-serial-3.0.3/README.md) 

## Change list &lt;refcodes-serial-alt&gt; (version 3.0.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial-alt/src/refcodes-serial-alt-3.0.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-serial-alt/src/refcodes-serial-alt-3.0.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial-alt/src/refcodes-serial-alt-3.0.3/refcodes-serial-alt-net/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-serial-alt/src/refcodes-serial-alt-3.0.3/refcodes-serial-alt-net/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial-alt/src/refcodes-serial-alt-3.0.3/refcodes-serial-alt-tty/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-serial-alt/src/refcodes-serial-alt-3.0.3/refcodes-serial-alt-tty/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`TtyPort.java`](https://bitbucket.org/refcodes/refcodes-serial-alt/src/refcodes-serial-alt-3.0.3/refcodes-serial-alt-tty/src/main/java/org/refcodes/serial/alt/tty/TtyPort.java) (see Javadoc at [`TtyPort.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-serial-alt-tty/3.0.3/org.refcodes.serial.alt.tty/org/refcodes/serial/alt/tty/TtyPort.html))

## Change list &lt;refcodes-serial-ext&gt; (version 3.0.3)

* \[<span style="color:red">DELETED</span>\] `Example.java`
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-3.0.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-3.0.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-3.0.3/refcodes-serial-ext-handshake/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-3.0.3/refcodes-serial-ext-handshake/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`module-info.java`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-3.0.3/refcodes-serial-ext-handshake/src/main/java/module-info.java) (see Javadoc at [`module-info.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-serial-ext-handshake/3.0.3//module-info.html))
* \[<span style="color:green">MODIFIED</span>\] [`HandshakePortController.java`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-3.0.3/refcodes-serial-ext-handshake/src/main/java/org/refcodes/serial/ext/handshake/HandshakePortController.java) (see Javadoc at [`HandshakePortController.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-serial-ext-handshake/3.0.3/org.refcodes.serial.ext.handshake/org/refcodes/serial/ext/handshake/HandshakePortController.html))
* \[<span style="color:green">MODIFIED</span>\] [`HandshakePortControllerTest.java`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-3.0.3/refcodes-serial-ext-handshake/src/test/java/org/refcodes/serial/ext/handshake/HandshakePortControllerTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`LoopbackHandshakePortControllerTest.java`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-3.0.3/refcodes-serial-ext-handshake/src/test/java/org/refcodes/serial/ext/handshake/LoopbackHandshakePortControllerTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`TtyHandshakePortControllerTest.java`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-3.0.3/refcodes-serial-ext-handshake/src/test/java/org/refcodes/serial/ext/handshake/TtyHandshakePortControllerTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-3.0.3/refcodes-serial-ext-observer/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-3.0.3/refcodes-serial-ext-observer/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-3.0.3/refcodes-serial-ext-security/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-3.0.3/refcodes-serial-ext-security/README.md) 

## Change list &lt;refcodes-p2p&gt; (version 3.0.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-p2p/src/refcodes-p2p-3.0.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-p2p/src/refcodes-p2p-3.0.3/README.md) 

## Change list &lt;refcodes-p2p-alt&gt; (version 3.0.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-p2p-alt/src/refcodes-p2p-alt-3.0.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-p2p-alt/src/refcodes-p2p-alt-3.0.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-p2p-alt/src/refcodes-p2p-alt-3.0.3/refcodes-p2p-alt-rest/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-p2p-alt/src/refcodes-p2p-alt-3.0.3/refcodes-p2p-alt-serial/pom.xml) 

## Change list &lt;refcodes-p2p-ext&gt; (version 3.0.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-p2p-ext/src/refcodes-p2p-ext-3.0.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-p2p-ext/src/refcodes-p2p-ext-3.0.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-p2p-ext/src/refcodes-p2p-ext-3.0.3/refcodes-p2p-ext-observer/pom.xml) 

## Change list &lt;refcodes-servicebus&gt; (version 3.0.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-servicebus/src/refcodes-servicebus-3.0.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-servicebus/src/refcodes-servicebus-3.0.3/README.md) 

## Change list &lt;refcodes-servicebus-alt&gt; (version 3.0.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-servicebus-alt/src/refcodes-servicebus-alt-3.0.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-servicebus-alt/src/refcodes-servicebus-alt-3.0.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-servicebus-alt/src/refcodes-servicebus-alt-3.0.3/refcodes-servicebus-alt-spring/pom.xml) 

## Change list &lt;refcodes-tabular-alt&gt; (version 3.0.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-tabular-alt/src/refcodes-tabular-alt-3.0.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-tabular-alt/src/refcodes-tabular-alt-3.0.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-tabular-alt/src/refcodes-tabular-alt-3.0.3/refcodes-tabular-alt-forwardsecrecy/pom.xml) 

## Change list &lt;refcodes-archetype&gt; (version 3.0.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype/src/refcodes-archetype-3.0.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype/src/refcodes-archetype-3.0.3/README.md) 

## Change list &lt;refcodes-archetype-alt&gt; (version 3.0.3)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.0.3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.0.3/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.0.3/refcodes-archetype-alt-c2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.0.3/refcodes-archetype-alt-c2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.0.3/refcodes-archetype-alt-cli/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.0.3/refcodes-archetype-alt-cli/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.0.3/refcodes-archetype-alt-csv/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.0.3/refcodes-archetype-alt-csv/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.0.3/refcodes-archetype-alt-eventbus/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.0.3/refcodes-archetype-alt-eventbus/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.0.3/refcodes-archetype-alt-rest/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.0.3/refcodes-archetype-alt-rest/README.md) 
