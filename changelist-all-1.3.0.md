# Change list for REFCODES.ORG artifacts' version 1.3.0

> This change list has been auto-generated on `triton` by `steiner` with with `changelist-all.sh` on the 2018-06-24 at 11:49:24.

## Change list &lt;refcodes-licensing&gt; (version 1.3.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-licensing/src/refcodes-licensing-1.3.0/pom.xml) 

## Change list &lt;refcodes-parent&gt; (version 1.3.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-parent/src/refcodes-parent-1.3.0/pom.xml) 

## Change list &lt;refcodes-time&gt; (version 1.3.0)

* \[<span style="color:blue">ADDED</span>\] [`module-info.java.off`](https://bitbucket.org/refcodes/refcodes-time/src/refcodes-time-1.3.0/src/main/java/module-info.java.off) 
* \[<span style="color:blue">ADDED</span>\] [`#Untitled-1#`](https://bitbucket.org/refcodes/refcodes-time/src/refcodes-time-1.3.0/src/main/java/#Untitled-1#) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-time/src/refcodes-time-1.3.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`DateFormat.java`](https://bitbucket.org/refcodes/refcodes-time/src/refcodes-time-1.3.0/src/main/java/org/refcodes/time/DateFormat.java) (see Javadoc at [`DateFormat.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-time/1.3.0/org/refcodes/time/DateFormat.html))

## Change list &lt;refcodes-mixin&gt; (version 1.3.0)

* \[<span style="color:blue">ADDED</span>\] [`module-info.java.off`](https://bitbucket.org/refcodes/refcodes-mixin/src/refcodes-mixin-1.3.0/src/main/java/module-info.java.off) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-mixin/src/refcodes-mixin-1.3.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`Dumpable.java`](https://bitbucket.org/refcodes/refcodes-mixin/src/refcodes-mixin-1.3.0/src/main/java/org/refcodes/mixin/Dumpable.java) (see Javadoc at [`Dumpable.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-mixin/1.3.0/org/refcodes/mixin/Dumpable.html))

## Change list &lt;refcodes-data&gt; (version 1.3.0)

* \[<span style="color:blue">ADDED</span>\] [`module-info.java.off`](https://bitbucket.org/refcodes/refcodes-data/src/refcodes-data-1.3.0/src/main/java/module-info.java.off) 
* \[<span style="color:red">DELETED</span>\] `comcodes-logo.png`
* \[<span style="color:red">DELETED</span>\] `funcodes-logo.png`
* \[<span style="color:red">DELETED</span>\] `refcodes-logo.png`
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data/src/refcodes-data-1.3.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`SchemeTest.java`](https://bitbucket.org/refcodes/refcodes-data/src/refcodes-data-1.3.0/src/test/java/org/refcodes/data/SchemeTest.java) 

## Change list &lt;refcodes-exception&gt; (version 1.3.0)

* \[<span style="color:blue">ADDED</span>\] [`module-info.java.off`](https://bitbucket.org/refcodes/refcodes-exception/src/refcodes-exception-1.3.0/src/main/java/module-info.java.off) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-exception/src/refcodes-exception-1.3.0/pom.xml) 

## Change list &lt;refcodes-factory&gt; (version 1.3.0)

* \[<span style="color:blue">ADDED</span>\] [`module-info.java.off`](https://bitbucket.org/refcodes/refcodes-factory/src/refcodes-factory-1.3.0/src/main/java/module-info.java.off) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-factory/src/refcodes-factory-1.3.0/pom.xml) 

## Change list &lt;refcodes-factory-alt&gt; (version 1.3.0)

* \[<span style="color:blue">ADDED</span>\] [`module-info.java.off`](https://bitbucket.org/refcodes/refcodes-factory-alt/src/refcodes-factory-alt-1.3.0/refcodes-factory-alt-spring/src/main/java/module-info.java.off) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-factory-alt/src/refcodes-factory-alt-1.3.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-factory-alt/src/refcodes-factory-alt-1.3.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-factory-alt/src/refcodes-factory-alt-1.3.0/refcodes-factory-alt-spring/pom.xml) 

## Change list &lt;refcodes-controlflow&gt; (version 1.3.0)

* \[<span style="color:blue">ADDED</span>\] [`module-info.java.off`](https://bitbucket.org/refcodes/refcodes-controlflow/src/refcodes-controlflow-1.3.0/src/main/java/module-info.java.off) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-controlflow/src/refcodes-controlflow-1.3.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`ExceptionWatchdogTest.java`](https://bitbucket.org/refcodes/refcodes-controlflow/src/refcodes-controlflow-1.3.0/src/test/java/org/refcodes/controlflow/ExceptionWatchdogTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`RetryCounterTest.java`](https://bitbucket.org/refcodes/refcodes-controlflow/src/refcodes-controlflow-1.3.0/src/test/java/org/refcodes/controlflow/RetryCounterTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`RetryTimeoutTest.java`](https://bitbucket.org/refcodes/refcodes-controlflow/src/refcodes-controlflow-1.3.0/src/test/java/org/refcodes/controlflow/RetryTimeoutTest.java) 

## Change list &lt;refcodes-numerical&gt; (version 1.3.0)

* \[<span style="color:blue">ADDED</span>\] [`module-info.java.off`](https://bitbucket.org/refcodes/refcodes-numerical/src/refcodes-numerical-1.3.0/src/main/java/module-info.java.off) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-numerical/src/refcodes-numerical-1.3.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`NumberBaseBuilderTest.java`](https://bitbucket.org/refcodes/refcodes-numerical/src/refcodes-numerical-1.3.0/src/test/java/org/refcodes/numerical/NumberBaseBuilderTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`NumericalUtilityTest.java`](https://bitbucket.org/refcodes/refcodes-numerical/src/refcodes-numerical-1.3.0/src/test/java/org/refcodes/numerical/NumericalUtilityTest.java) 

## Change list &lt;refcodes-generator&gt; (version 1.3.0)

* \[<span style="color:blue">ADDED</span>\] [`module-info.java.off`](https://bitbucket.org/refcodes/refcodes-generator/src/refcodes-generator-1.3.0/src/main/java/module-info.java.off) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-generator/src/refcodes-generator-1.3.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`UniqueIdGeneratorTest.java`](https://bitbucket.org/refcodes/refcodes-generator/src/refcodes-generator-1.3.0/src/test/java/org/refcodes/generator/UniqueIdGeneratorTest.java) 

## Change list &lt;refcodes-structure&gt; (version 1.3.0)

* \[<span style="color:blue">ADDED</span>\] [`module-info.java.off`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-1.3.0/src/main/java/module-info.java.off) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-1.3.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-1.3.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`PathMapBuilderImpl.java`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-1.3.0/src/main/java/org/refcodes/structure/PathMapBuilderImpl.java) (see Javadoc at [`PathMapBuilderImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-structure/1.3.0/org/refcodes/structure/PathMapBuilderImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`PathMapImpl.java`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-1.3.0/src/main/java/org/refcodes/structure/PathMapImpl.java) (see Javadoc at [`PathMapImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-structure/1.3.0/org/refcodes/structure/PathMapImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`TypeUtility.java`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-1.3.0/src/main/java/org/refcodes/structure/TypeUtility.java) (see Javadoc at [`TypeUtility.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-structure/1.3.0/org/refcodes/structure/TypeUtility.html))
* \[<span style="color:green">MODIFIED</span>\] [`CanonicalMapTest.java`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-1.3.0/src/test/java/org/refcodes/structure/CanonicalMapTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`PathMapTest.java`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-1.3.0/src/test/java/org/refcodes/structure/PathMapTest.java) 

## Change list &lt;refcodes-runtime&gt; (version 1.3.0)

* \[<span style="color:blue">ADDED</span>\] [`module-info.java.off`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-1.3.0/src/main/java/module-info.java.off) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-1.3.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`DumpBuilderImpl.java`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-1.3.0/src/main/java/org/refcodes/runtime/DumpBuilderImpl.java) (see Javadoc at [`DumpBuilderImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-runtime/1.3.0/org/refcodes/runtime/DumpBuilderImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`ConfigLocatorTest.java`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-1.3.0/src/test/java/org/refcodes/runtime/ConfigLocatorTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`PropertyBuilderTest.java`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-1.3.0/src/test/java/org/refcodes/runtime/PropertyBuilderTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`RuntimeUtilityTest.java`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-1.3.0/src/test/java/org/refcodes/runtime/RuntimeUtilityTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`SystemContextTest.java`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-1.3.0/src/test/java/org/refcodes/runtime/SystemContextTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`SystemUtilityTest.java`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-1.3.0/src/test/java/org/refcodes/runtime/SystemUtilityTest.java) 

## Change list &lt;refcodes-component&gt; (version 1.3.0)

* \[<span style="color:blue">ADDED</span>\] [`module-info.java.off`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-1.3.0/src/main/java/module-info.java.off) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-1.3.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`Initializable.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-1.3.0/src/main/java/org/refcodes/component/Initializable.java) (see Javadoc at [`Initializable.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/1.3.0/org/refcodes/component/Initializable.html))
* \[<span style="color:green">MODIFIED</span>\] [`ConfigurableLifeCycleAutomatonTest.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-1.3.0/src/test/java/org/refcodes/component/ConfigurableLifeCycleAutomatonTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`ConnectionAutomatonTest.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-1.3.0/src/test/java/org/refcodes/component/ConnectionAutomatonTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`DeviceAutomatonTest.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-1.3.0/src/test/java/org/refcodes/component/DeviceAutomatonTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`LifeCycleAutomatonTest.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-1.3.0/src/test/java/org/refcodes/component/LifeCycleAutomatonTest.java) 

## Change list &lt;refcodes-data-ext&gt; (version 1.3.0)

* \[<span style="color:blue">ADDED</span>\] [`module-info.java.off`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.3.0/refcodes-data-ext-boulderdash/src/main/java/module-info.java.off) 
* \[<span style="color:blue">ADDED</span>\] [`module-info.java.off`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.3.0/refcodes-data-ext-checkers/src/main/java/module-info.java.off) 
* \[<span style="color:blue">ADDED</span>\] [`module-info.java.off`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.3.0/refcodes-data-ext-chess/src/main/java/module-info.java.off) 
* \[<span style="color:blue">ADDED</span>\] [`module-info.java.off`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.3.0/refcodes-data-ext-corporate/src/main/java/module-info.java.off) 
* \[<span style="color:blue">ADDED</span>\] [`comcodes-logo.png`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.3.0/refcodes-data-ext-corporate/src/main/resources/org/refcodes/data/ext/corporate/comcodes-logo.png) 
* \[<span style="color:blue">ADDED</span>\] [`funcodes-logo.png`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.3.0/refcodes-data-ext-corporate/src/main/resources/org/refcodes/data/ext/corporate/funcodes-logo.png) 
* \[<span style="color:blue">ADDED</span>\] [`refcodes-logo.png`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.3.0/refcodes-data-ext-corporate/src/main/resources/org/refcodes/data/ext/corporate/refcodes-logo.png) 
* \[<span style="color:blue">ADDED</span>\] [`module-info.java.off`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.3.0/refcodes-data-ext-symbols/src/main/java/module-info.java.off) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.3.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.3.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.3.0/refcodes-data-ext-boulderdash/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`BoulderDashCaveMapTest.java`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.3.0/refcodes-data-ext-boulderdash/src/test/java/org/refcodes/data/ext/boulderdash/BoulderDashCaveMapTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.3.0/refcodes-data-ext-checkers/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.3.0/refcodes-data-ext-checkers/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.3.0/refcodes-data-ext-chess/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.3.0/refcodes-data-ext-chess/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.3.0/refcodes-data-ext-corporate/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`LogoPixmap.java`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.3.0/refcodes-data-ext-corporate/src/main/java/org/refcodes/data/ext/corporate/LogoPixmap.java) (see Javadoc at [`LogoPixmap.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-data-ext-corporate/1.3.0/org/refcodes/data/ext/corporate/LogoPixmap.html))
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.3.0/refcodes-data-ext-symbols/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.3.0/refcodes-data-ext-symbols/README.md) 

## Change list &lt;refcodes-graphical&gt; (version 1.3.0)

* \[<span style="color:blue">ADDED</span>\] [`module-info.java.off`](https://bitbucket.org/refcodes/refcodes-graphical/src/refcodes-graphical-1.3.0/src/main/java/module-info.java.off) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-graphical/src/refcodes-graphical-1.3.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`BoxBorderModeTest.java`](https://bitbucket.org/refcodes/refcodes-graphical/src/refcodes-graphical-1.3.0/src/test/java/org/refcodes/graphical/BoxBorderModeTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`GraphicalUtilityTest.java`](https://bitbucket.org/refcodes/refcodes-graphical/src/refcodes-graphical-1.3.0/src/test/java/org/refcodes/graphical/GraphicalUtilityTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`RgbPixmapImageBuilderTest.java`](https://bitbucket.org/refcodes/refcodes-graphical/src/refcodes-graphical-1.3.0/src/test/java/org/refcodes/graphical/RgbPixmapImageBuilderTest.java) 

## Change list &lt;refcodes-textual&gt; (version 1.3.0)

* \[<span style="color:blue">ADDED</span>\] [`module-info.java.off`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-1.3.0/src/main/java/module-info.java.off) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-1.3.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`AlignTextBuilderTest.java`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-1.3.0/src/test/java/org/refcodes/textual/AlignTextBuilderTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`AsciiArtBuilderTest.java`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-1.3.0/src/test/java/org/refcodes/textual/AsciiArtBuilderTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`CaseStyleBuilderTest.java`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-1.3.0/src/test/java/org/refcodes/textual/CaseStyleBuilderTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`CaseStyleTest.java`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-1.3.0/src/test/java/org/refcodes/textual/CaseStyleTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`CsvBuilderTest.java`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-1.3.0/src/test/java/org/refcodes/textual/CsvBuilderTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`EscapeTextBuilderTest.java`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-1.3.0/src/test/java/org/refcodes/textual/EscapeTextBuilderTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`MoreTextBuilderTest.java`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-1.3.0/src/test/java/org/refcodes/textual/MoreTextBuilderTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`OverwriteTextBuilderTest.java`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-1.3.0/src/test/java/org/refcodes/textual/OverwriteTextBuilderTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`RandomTextGeneratorTest.java`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-1.3.0/src/test/java/org/refcodes/textual/RandomTextGeneratorTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`ReplaceTextBuilderTest.java`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-1.3.0/src/test/java/org/refcodes/textual/ReplaceTextBuilderTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`SecretHintBuilderTest.java`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-1.3.0/src/test/java/org/refcodes/textual/SecretHintBuilderTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`TableBuilderTest.java`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-1.3.0/src/test/java/org/refcodes/textual/TableBuilderTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`TextBlockBuilderTest.java`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-1.3.0/src/test/java/org/refcodes/textual/TextBlockBuilderTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`TextBorderBuilderTest.java`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-1.3.0/src/test/java/org/refcodes/textual/TextBorderBuilderTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`TruncateTextBuilderTest.java`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-1.3.0/src/test/java/org/refcodes/textual/TruncateTextBuilderTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`VerboseTextBuilderTest.java`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-1.3.0/src/test/java/org/refcodes/textual/VerboseTextBuilderTest.java) 

## Change list &lt;refcodes-criteria&gt; (version 1.3.0)

* \[<span style="color:blue">ADDED</span>\] [`module-info.java.off`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-1.3.0/src/main/java/module-info.java.off) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-1.3.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-1.3.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`ExpressionCriteriaFactoryImplTest.java`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-1.3.0/src/test/java/org/refcodes/criteria/ExpressionCriteriaFactoryImplTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`ExpressionQueryFactoryTest.java`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-1.3.0/src/test/java/org/refcodes/criteria/ExpressionQueryFactoryTest.java) 

## Change list &lt;refcodes-tabular&gt; (version 1.3.0)

* \[<span style="color:blue">ADDED</span>\] [`module-info.java.off`](https://bitbucket.org/refcodes/refcodes-tabular/src/refcodes-tabular-1.3.0/src/main/java/module-info.java.off) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-tabular/src/refcodes-tabular-1.3.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`ColumnTest.java`](https://bitbucket.org/refcodes/refcodes-tabular/src/refcodes-tabular-1.3.0/src/test/java/org/refcodes/tabular/ColumnTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`CsvStreamRecordsTest.java`](https://bitbucket.org/refcodes/refcodes-tabular/src/refcodes-tabular-1.3.0/src/test/java/org/refcodes/tabular/CsvStreamRecordsTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`HeaderTest.java`](https://bitbucket.org/refcodes/refcodes-tabular/src/refcodes-tabular-1.3.0/src/test/java/org/refcodes/tabular/HeaderTest.java) 

## Change list &lt;refcodes-matcher&gt; (version 1.3.0)

* \[<span style="color:blue">ADDED</span>\] [`module-info.java.off`](https://bitbucket.org/refcodes/refcodes-matcher/src/refcodes-matcher-1.3.0/src/main/java/module-info.java.off) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-matcher/src/refcodes-matcher-1.3.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`PathMatcherImpl.java`](https://bitbucket.org/refcodes/refcodes-matcher/src/refcodes-matcher-1.3.0/src/main/java/org/refcodes/matcher/PathMatcherImpl.java) (see Javadoc at [`PathMatcherImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-matcher/1.3.0/org/refcodes/matcher/PathMatcherImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`PathMatcherTest.java`](https://bitbucket.org/refcodes/refcodes-matcher/src/refcodes-matcher-1.3.0/src/test/java/org/refcodes/matcher/PathMatcherTest.java) 

## Change list &lt;refcodes-observer&gt; (version 1.3.0)

* \[<span style="color:blue">ADDED</span>\] [`module-info.java.off`](https://bitbucket.org/refcodes/refcodes-observer/src/refcodes-observer-1.3.0/src/main/java/module-info.java.off) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-observer/src/refcodes-observer-1.3.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`AbstractObservable.java`](https://bitbucket.org/refcodes/refcodes-observer/src/refcodes-observer-1.3.0/src/main/java/org/refcodes/observer/AbstractObservable.java) (see Javadoc at [`AbstractObservable.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-observer/1.3.0/org/refcodes/observer/AbstractObservable.html))
* \[<span style="color:green">MODIFIED</span>\] [`ObservableTest.java`](https://bitbucket.org/refcodes/refcodes-observer/src/refcodes-observer-1.3.0/src/test/java/org/refcodes/observer/ObservableTest.java) 

## Change list &lt;refcodes-command&gt; (version 1.3.0)

* \[<span style="color:blue">ADDED</span>\] [`module-info.java.off`](https://bitbucket.org/refcodes/refcodes-command/src/refcodes-command-1.3.0/src/main/java/module-info.java.off) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-command/src/refcodes-command-1.3.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-command/src/refcodes-command-1.3.0/README.md) 

## Change list &lt;refcodes-cli&gt; (version 1.3.0)

* \[<span style="color:blue">ADDED</span>\] [`module-info.java.off`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-1.3.0/src/main/java/module-info.java.off) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-1.3.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-1.3.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`ArgsParserTest.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-1.3.0/src/test/java/org/refcodes/console/ArgsParserTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`OptionalConditionTest.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-1.3.0/src/test/java/org/refcodes/console/OptionalConditionTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`StackOverflowTest.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-1.3.0/src/test/java/org/refcodes/console/StackOverflowTest.java) 

## Change list &lt;refcodes-io&gt; (version 1.3.0)

* \[<span style="color:blue">ADDED</span>\] [`module-info.java.off`](https://bitbucket.org/refcodes/refcodes-io/src/refcodes-io-1.3.0/src/main/java/module-info.java.off) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-io/src/refcodes-io-1.3.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-io/src/refcodes-io-1.3.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`FileUtilityTest.java`](https://bitbucket.org/refcodes/refcodes-io/src/refcodes-io-1.3.0/src/test/java/org/refcodes/io/FileUtilityTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`IoStreamConnectionTest.java`](https://bitbucket.org/refcodes/refcodes-io/src/refcodes-io-1.3.0/src/test/java/org/refcodes/io/IoStreamConnectionTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`LoopbackConnectionTest.java`](https://bitbucket.org/refcodes/refcodes-io/src/refcodes-io-1.3.0/src/test/java/org/refcodes/io/LoopbackConnectionTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`ZipFileStreamTest.java`](https://bitbucket.org/refcodes/refcodes-io/src/refcodes-io-1.3.0/src/test/java/org/refcodes/io/ZipFileStreamTest.java) 

## Change list &lt;refcodes-codec&gt; (version 1.3.0)

* \[<span style="color:blue">ADDED</span>\] [`module-info.java.off`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-1.3.0/src/main/java/module-info.java.off) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-1.3.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-1.3.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`BaseBuilderTest.java`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-1.3.0/src/test/java/org/refcodes/codec/BaseBuilderTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`BaseInputStreamDecoderTest.java`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-1.3.0/src/test/java/org/refcodes/codec/BaseInputStreamDecoderTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`BaseOutputStreamEncoderTest.java`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-1.3.0/src/test/java/org/refcodes/codec/BaseOutputStreamEncoderTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`ModemTest.java`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-1.3.0/src/test/java/org/refcodes/codec/ModemTest.java) 

## Change list &lt;refcodes-component-ext&gt; (version 1.3.0)

* \[<span style="color:blue">ADDED</span>\] [`module-info.java.off`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.3.0/refcodes-component-ext-observer/src/main/java/module-info.java.off) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.3.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.3.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.3.0/refcodes-component-ext-observer/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`ObservableLifeCycleAutomatonTest.java`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.3.0/refcodes-component-ext-observer/src/test/java/org/refcodes/component/ext/observer/ObservableLifeCycleAutomatonTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`ObservableLifeCycleRequestAutomatonTest.java`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.3.0/refcodes-component-ext-observer/src/test/java/org/refcodes/component/ext/observer/ObservableLifeCycleRequestAutomatonTest.java) 

## Change list &lt;refcodes-properties&gt; (version 1.3.0)

* \[<span style="color:blue">ADDED</span>\] [`module-info.java.off`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.3.0/src/main/java/module-info.java.off) 
* \[<span style="color:blue">ADDED</span>\] [`configuration.json`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.3.0/src/test/resources/configuration.json) 
* \[<span style="color:blue">ADDED</span>\] [`configuration.properties`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.3.0/src/test/resources/configuration.properties) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.3.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.3.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`JavaPropertiesBuilder.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.3.0/src/main/java/org/refcodes/configuration/JavaPropertiesBuilder.java) (see Javadoc at [`JavaPropertiesBuilder.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties/1.3.0/org/refcodes/configuration/JavaPropertiesBuilder.html))
* \[<span style="color:green">MODIFIED</span>\] [`JsonPropertiesBuilder.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.3.0/src/main/java/org/refcodes/configuration/JsonPropertiesBuilder.java) (see Javadoc at [`JsonPropertiesBuilder.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties/1.3.0/org/refcodes/configuration/JsonPropertiesBuilder.html))
* \[<span style="color:green">MODIFIED</span>\] [`package-info.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.3.0/src/main/java/org/refcodes/configuration/package-info.java) 
* \[<span style="color:green">MODIFIED</span>\] [`PolyglotPropertiesBuilder.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.3.0/src/main/java/org/refcodes/configuration/PolyglotPropertiesBuilder.java) (see Javadoc at [`PolyglotPropertiesBuilder.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties/1.3.0/org/refcodes/configuration/PolyglotPropertiesBuilder.html))
* \[<span style="color:green">MODIFIED</span>\] [`PolyglotProperties.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.3.0/src/main/java/org/refcodes/configuration/PolyglotProperties.java) (see Javadoc at [`PolyglotProperties.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties/1.3.0/org/refcodes/configuration/PolyglotProperties.html))
* \[<span style="color:green">MODIFIED</span>\] [`XmlProperties.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.3.0/src/main/java/org/refcodes/configuration/XmlProperties.java) (see Javadoc at [`XmlProperties.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties/1.3.0/org/refcodes/configuration/XmlProperties.html))
* \[<span style="color:green">MODIFIED</span>\] [`AbstractResourcePropertiesTest.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.3.0/src/test/java/org/refcodes/configuration/AbstractResourcePropertiesTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`ArgsPropertiesTest.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.3.0/src/test/java/org/refcodes/configuration/ArgsPropertiesTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`ConfigurationPropertiesTest.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.3.0/src/test/java/org/refcodes/configuration/ConfigurationPropertiesTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`EnvironmentPropertiesTest.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.3.0/src/test/java/org/refcodes/configuration/EnvironmentPropertiesTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`IniPropertiesTest.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.3.0/src/test/java/org/refcodes/configuration/IniPropertiesTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`NormalizedPathPropertiesTest.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.3.0/src/test/java/org/refcodes/configuration/NormalizedPathPropertiesTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`PolyglotPropertiesBuilderTest.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.3.0/src/test/java/org/refcodes/configuration/PolyglotPropertiesBuilderTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`PolyglotPropertiesTest.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.3.0/src/test/java/org/refcodes/configuration/PolyglotPropertiesTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`ProfilePropertiesTest.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.3.0/src/test/java/org/refcodes/configuration/ProfilePropertiesTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`PropertiesSugarTest.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.3.0/src/test/java/org/refcodes/configuration/PropertiesSugarTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`PropertiesTest.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.3.0/src/test/java/org/refcodes/configuration/PropertiesTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`ResourcePropertiesTest.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.3.0/src/test/java/org/refcodes/configuration/ResourcePropertiesTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`SystemPropertiesTest.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.3.0/src/test/java/org/refcodes/configuration/SystemPropertiesTest.java) 

## Change list &lt;refcodes-security&gt; (version 1.3.0)

* \[<span style="color:blue">ADDED</span>\] [`module-info.java.off`](https://bitbucket.org/refcodes/refcodes-security/src/refcodes-security-1.3.0/src/main/java/module-info.java.off) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security/src/refcodes-security-1.3.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security/src/refcodes-security-1.3.0/README.md) 

## Change list &lt;refcodes-security-alt&gt; (version 1.3.0)

* \[<span style="color:blue">ADDED</span>\] [`module-info.java.off`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-1.3.0/refcodes-security-alt-chaos/src/main/java/module-info.java.off) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-1.3.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-1.3.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-1.3.0/refcodes-security-alt-chaos/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-1.3.0/refcodes-security-alt-chaos/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`ChaosKeyTest.java`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-1.3.0/refcodes-security-alt-chaos/src/test/java/org/refcodes/security/alt/chaos/ChaosKeyTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`ChaosTest.java`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-1.3.0/refcodes-security-alt-chaos/src/test/java/org/refcodes/security/alt/chaos/ChaosTest.java) 

## Change list &lt;refcodes-security-ext&gt; (version 1.3.0)

* \[<span style="color:blue">ADDED</span>\] [`module-info.java.off`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-1.3.0/refcodes-security-ext-chaos/src/main/java/module-info.java.off) 
* \[<span style="color:blue">ADDED</span>\] [`module-info.java.off`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-1.3.0/refcodes-security-ext-spring/src/main/java/module-info.java.off) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-1.3.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-1.3.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-1.3.0/refcodes-security-ext-chaos/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-1.3.0/refcodes-security-ext-chaos/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`ChaosProviderTest.java`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-1.3.0/refcodes-security-ext-chaos/src/test/java/org/refcodes/security/ext/chaos/ChaosProviderTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-1.3.0/refcodes-security-ext-spring/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`TextDecrypterBeanTest.java`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-1.3.0/refcodes-security-ext-spring/src/test/java/org/refcodes/security/ext/spring/TextDecrypterBeanTest.java) 

## Change list &lt;refcodes-properties-ext&gt; (version 1.3.0)

* \[<span style="color:blue">ADDED</span>\] [`module-info.java.off`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.3.0/refcodes-properties-ext-cli/src/main/java/module-info.java.off) 
* \[<span style="color:blue">ADDED</span>\] [`module-info.java.off`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.3.0/refcodes-properties-ext-obfuscation/src/main/java/module-info.java.off) 
* \[<span style="color:blue">ADDED</span>\] [`module-info.java.off`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.3.0/refcodes-properties-ext-observer/src/main/java/module-info.java.off) 
* \[<span style="color:blue">ADDED</span>\] [`module-info.java.off`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.3.0/refcodes-properties-ext-runtime/src/main/java/module-info.java.off) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.3.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.3.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.3.0/refcodes-properties-ext-cli/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.3.0/refcodes-properties-ext-cli/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`ArgsParserPropertiesTest.java`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.3.0/refcodes-properties-ext-cli/src/test/java/org/refcodes/configuration/ext/console/ArgsParserPropertiesTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.3.0/refcodes-properties-ext-obfuscation/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.3.0/refcodes-properties-ext-obfuscation/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`ObfuscationPropertiesBuilderTest.java`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.3.0/refcodes-properties-ext-obfuscation/src/test/java/org/refcodes/configuration/ext/obfuscation/ObfuscationPropertiesBuilderTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`ObfuscationPropertiesTest.java`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.3.0/refcodes-properties-ext-obfuscation/src/test/java/org/refcodes/configuration/ext/obfuscation/ObfuscationPropertiesTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.3.0/refcodes-properties-ext-observer/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.3.0/refcodes-properties-ext-observer/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`ObservableProperties.java`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.3.0/refcodes-properties-ext-observer/src/main/java/org/refcodes/configuration/ext/observer/ObservableProperties.java) (see Javadoc at [`ObservableProperties.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties-ext-observer/1.3.0/org/refcodes/configuration/ext/observer/ObservableProperties.html))
* \[<span style="color:green">MODIFIED</span>\] [`ObservableResourcePropertiesBuilderDecorator.java`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.3.0/refcodes-properties-ext-observer/src/main/java/org/refcodes/configuration/ext/observer/ObservableResourcePropertiesBuilderDecorator.java) (see Javadoc at [`ObservableResourcePropertiesBuilderDecorator.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties-ext-observer/1.3.0/org/refcodes/configuration/ext/observer/ObservableResourcePropertiesBuilderDecorator.html))
* \[<span style="color:green">MODIFIED</span>\] [`AbstractObservableResourcePropertiesTest.java`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.3.0/refcodes-properties-ext-observer/src/test/java/org/refcodes/configuration/ext/observer/AbstractObservableResourcePropertiesTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`ObservablePropertiesSugarTest.java`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.3.0/refcodes-properties-ext-observer/src/test/java/org/refcodes/configuration/ext/observer/ObservablePropertiesSugarTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.3.0/refcodes-properties-ext-runtime/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.3.0/refcodes-properties-ext-runtime/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`RuntimePropertiesSugarTest.java`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.3.0/refcodes-properties-ext-runtime/src/test/java/org/refcodes/configuration/ext/runtime/RuntimePropertiesSugarTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`RuntimePropertiesTest.java`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.3.0/refcodes-properties-ext-runtime/src/test/java/org/refcodes/configuration/ext/runtime/RuntimePropertiesTest.java) 

## Change list &lt;refcodes-logger&gt; (version 1.3.0)

* \[<span style="color:blue">ADDED</span>\] [`module-info.java.off`](https://bitbucket.org/refcodes/refcodes-logger/src/refcodes-logger-1.3.0/src/main/java/module-info.java.off) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger/src/refcodes-logger-1.3.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger/src/refcodes-logger-1.3.0/README.md) 

## Change list &lt;refcodes-logger-alt&gt; (version 1.3.0)

* \[<span style="color:blue">ADDED</span>\] [`module-info.java.off`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.3.0/refcodes-logger-alt-async/src/main/java/module-info.java.off) 
* \[<span style="color:blue">ADDED</span>\] [`module-info.java.off`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.3.0/refcodes-logger-alt-console/src/main/java/module-info.java.off) 
* \[<span style="color:blue">ADDED</span>\] [`module-info.java.off`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.3.0/refcodes-logger-alt-io/src/main/java/module-info.java.off) 
* \[<span style="color:blue">ADDED</span>\] [`module-info.java.off`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.3.0/refcodes-logger-alt-slf4j/src/main/java/module-info.java.off) 
* \[<span style="color:blue">ADDED</span>\] [`module-info.java.off`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.3.0/refcodes-logger-alt-spring/src/main/java/module-info.java.off) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.3.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.3.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.3.0/refcodes-logger-alt-async/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.3.0/refcodes-logger-alt-async/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.3.0/refcodes-logger-alt-console/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.3.0/refcodes-logger-alt-console/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.3.0/refcodes-logger-alt-io/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.3.0/refcodes-logger-alt-io/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.3.0/refcodes-logger-alt-simpledb/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.3.0/refcodes-logger-alt-slf4j/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`Slf4jRuntimeLoggerTest.java`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.3.0/refcodes-logger-alt-slf4j/src/test/java/org/refcodes/logger/alt/slf4j/Slf4jRuntimeLoggerTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.3.0/refcodes-logger-alt-spring/pom.xml) 

## Change list &lt;refcodes-logger-ext&gt; (version 1.3.0)

* \[<span style="color:blue">ADDED</span>\] [`org.refcodes`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-1.3.0/"refcodes-logger-ext-slf4j/org.refcodes) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-1.3.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-1.3.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-1.3.0/refcodes-logger-ext-slf4j/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-1.3.0/refcodes-logger-ext-slf4j/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`RuntimeLoggerAdapter.java`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-1.3.0/refcodes-logger-ext-slf4j/src/main/java/org/slf4j/impl/RuntimeLoggerAdapter.java) (see Javadoc at [`RuntimeLoggerAdapter.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-logger-ext-slf4j/1.3.0/org/slf4j/impl/RuntimeLoggerAdapter.html))

## Change list &lt;refcodes-graphical-ext&gt; (version 1.3.0)

* \[<span style="color:blue">ADDED</span>\] [`module-info.java.off`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-1.3.0/refcodes-graphical-ext-javafx/src/main/java/module-info.java.off) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-1.3.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-1.3.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-1.3.0/refcodes-graphical-ext-javafx/pom.xml) 

## Change list &lt;refcodes-checkerboard&gt; (version 1.3.0)

* \[<span style="color:blue">ADDED</span>\] [`module-info.java.off`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-1.3.0/src/main/java/module-info.java.off) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-1.3.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-1.3.0/README.md) 

## Change list &lt;refcodes-checkerboard-alt&gt; (version 1.3.0)

* \[<span style="color:blue">ADDED</span>\] [`module-info.java.off`](https://bitbucket.org/refcodes/refcodes-checkerboard-alt/src/refcodes-checkerboard-alt-1.3.0/refcodes-checkerboard-alt-javafx/src/main/java/module-info.java.off) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-alt/src/refcodes-checkerboard-alt-1.3.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-alt/src/refcodes-checkerboard-alt-1.3.0/refcodes-checkerboard-alt-javafx/pom.xml) 

## Change list &lt;refcodes-checkerboard-ext&gt; (version 1.3.0)

* \[<span style="color:blue">ADDED</span>\] [`module-info.java.off`](https://bitbucket.org/refcodes/refcodes-checkerboard-ext/src/refcodes-checkerboard-ext-1.3.0/refcodes-checkerboard-ext-javafx-boulderdash/src/main/java/module-info.java.off) 
* \[<span style="color:blue">ADDED</span>\] [`module-info.java.off`](https://bitbucket.org/refcodes/refcodes-checkerboard-ext/src/refcodes-checkerboard-ext-1.3.0/refcodes-checkerboard-ext-javafx-chess/src/main/java/module-info.java.off) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-ext/src/refcodes-checkerboard-ext-1.3.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-checkerboard-ext/src/refcodes-checkerboard-ext-1.3.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-ext/src/refcodes-checkerboard-ext-1.3.0/refcodes-checkerboard-ext-javafx-boulderdash/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-ext/src/refcodes-checkerboard-ext-1.3.0/refcodes-checkerboard-ext-javafx-chess/pom.xml) 

## Change list &lt;refcodes-boulderdash&gt; (version 1.3.0)

* \[<span style="color:blue">ADDED</span>\] [`module-info.java.off`](https://bitbucket.org/refcodes/refcodes-boulderdash/src/refcodes-boulderdash-1.3.0/src/main/java/module-info.java.off) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-boulderdash/src/refcodes-boulderdash-1.3.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-boulderdash/src/refcodes-boulderdash-1.3.0/README.md) 

## Change list &lt;refcodes-net&gt; (version 1.3.0)

* \[<span style="color:blue">ADDED</span>\] [`module-info.java.off`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.3.0/src/main/java/module-info.java.off) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.3.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`AbstractApplicationFactoryTest.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.3.0/src/test/java/org/refcodes/net/AbstractApplicationFactoryTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`ApplicationFormFactoryTest.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.3.0/src/test/java/org/refcodes/net/ApplicationFormFactoryTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`ApplicationJsonFactoryTest.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.3.0/src/test/java/org/refcodes/net/ApplicationJsonFactoryTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`ContentTypeTest.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.3.0/src/test/java/org/refcodes/net/ContentTypeTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`FormFieldsTest.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.3.0/src/test/java/org/refcodes/net/FormFieldsTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`HeaderFieldsTest.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.3.0/src/test/java/org/refcodes/net/HeaderFieldsTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`HttpStatusCodeTest.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.3.0/src/test/java/org/refcodes/net/HttpStatusCodeTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`IpAddressTest.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.3.0/src/test/java/org/refcodes/net/IpAddressTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`MediaTypeTest.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.3.0/src/test/java/org/refcodes/net/MediaTypeTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`OauthTokenTest.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.3.0/src/test/java/org/refcodes/net/OauthTokenTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`PortManagerTest.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.3.0/src/test/java/org/refcodes/net/PortManagerTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`RequestHeaderFieldsTest.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.3.0/src/test/java/org/refcodes/net/RequestHeaderFieldsTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`ResponseHeaderFieldsTest.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.3.0/src/test/java/org/refcodes/net/ResponseHeaderFieldsTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`UrlTest.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.3.0/src/test/java/org/refcodes/net/UrlTest.java) 

## Change list &lt;refcodes-rest&gt; (version 1.3.0)

* \[<span style="color:blue">ADDED</span>\] [`module-info.java.off`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.3.0/src/main/java/module-info.java.off) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.3.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.3.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`HttpRestClientTest.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.3.0/src/test/java/org/refcodes/rest/HttpRestClientTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`HttpRestServerTest.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.3.0/src/test/java/org/refcodes/rest/HttpRestServerTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`HttpRestSugarTest.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.3.0/src/test/java/org/refcodes/rest/HttpRestSugarTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`HttpsRestServerTest.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.3.0/src/test/java/org/refcodes/rest/HttpsRestServerTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`LoopbackRestServerTest.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.3.0/src/test/java/org/refcodes/rest/LoopbackRestServerTest.java) 

## Change list &lt;refcodes-daemon&gt; (version 1.3.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-daemon/src/refcodes-daemon-1.3.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-daemon/src/refcodes-daemon-1.3.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`AbstractDaemon.java`](https://bitbucket.org/refcodes/refcodes-daemon/src/refcodes-daemon-1.3.0/src/main/java/org/refcodes/daemon/AbstractDaemon.java) (see Javadoc at [`AbstractDaemon.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-daemon/1.3.0/org/refcodes/daemon/AbstractDaemon.html))

## Change list &lt;refcodes-eventbus&gt; (version 1.3.0)

* \[<span style="color:blue">ADDED</span>\] [`module-info.java.off`](https://bitbucket.org/refcodes/refcodes-eventbus/src/refcodes-eventbus-1.3.0/src/main/java/module-info.java.off) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-eventbus/src/refcodes-eventbus-1.3.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-eventbus/src/refcodes-eventbus-1.3.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`GenericBusObservable.java`](https://bitbucket.org/refcodes/refcodes-eventbus/src/refcodes-eventbus-1.3.0/src/main/java/org/refcodes/eventbus/GenericBusObservable.java) (see Javadoc at [`GenericBusObservable.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-eventbus/1.3.0/org/refcodes/eventbus/GenericBusObservable.html))
* \[<span style="color:green">MODIFIED</span>\] [`GenericBusPublisher.java`](https://bitbucket.org/refcodes/refcodes-eventbus/src/refcodes-eventbus-1.3.0/src/main/java/org/refcodes/eventbus/GenericBusPublisher.java) (see Javadoc at [`GenericBusPublisher.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-eventbus/1.3.0/org/refcodes/eventbus/GenericBusPublisher.html))
* \[<span style="color:green">MODIFIED</span>\] [`DisptachStrategyTest.java`](https://bitbucket.org/refcodes/refcodes-eventbus/src/refcodes-eventbus-1.3.0/src/test/java/org/refcodes/eventbus/DisptachStrategyTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`EventBusTest.java`](https://bitbucket.org/refcodes/refcodes-eventbus/src/refcodes-eventbus-1.3.0/src/test/java/org/refcodes/eventbus/EventBusTest.java) 

## Change list &lt;refcodes-eventbus-ext&gt; (version 1.3.0)

* \[<span style="color:blue">ADDED</span>\] [`module-info.java.off`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-1.3.0/refcodes-eventbus-ext-application/src/main/java/module-info.java.off) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-1.3.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-1.3.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-1.3.0/refcodes-eventbus-ext-application/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`ApplicationBusTest.java`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-1.3.0/refcodes-eventbus-ext-application/src/test/java/org/refcodes/eventbus/ext/application/ApplicationBusTest.java) 

## Change list &lt;refcodes-filesystem&gt; (version 1.3.0)

* \[<span style="color:blue">ADDED</span>\] [`module-info.java.off`](https://bitbucket.org/refcodes/refcodes-filesystem/src/refcodes-filesystem-1.3.0/src/main/java/module-info.java.off) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-filesystem/src/refcodes-filesystem-1.3.0/pom.xml) 

## Change list &lt;refcodes-filesystem-alt&gt; (version 1.3.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-filesystem-alt/src/refcodes-filesystem-alt-1.3.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-filesystem-alt/src/refcodes-filesystem-alt-1.3.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-filesystem-alt/src/refcodes-filesystem-alt-1.3.0/refcodes-filesystem-alt-s3/pom.xml) 

## Change list &lt;refcodes-forwardsecrecy&gt; (version 1.3.0)

* \[<span style="color:blue">ADDED</span>\] [`module-info.java.off`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy/src/refcodes-forwardsecrecy-1.3.0/src/main/java/module-info.java.off) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy/src/refcodes-forwardsecrecy-1.3.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy/src/refcodes-forwardsecrecy-1.3.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`ForwardSecrecyTest.java`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy/src/refcodes-forwardsecrecy-1.3.0/src/test/java/org/refcodes/forwardsecrecy/ForwardSecrecyTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`ForwardSecrecyUtilityTest.java`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy/src/refcodes-forwardsecrecy-1.3.0/src/test/java/org/refcodes/forwardsecrecy/ForwardSecrecyUtilityTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`ForwardSecrecyWorkshopTest.java`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy/src/refcodes-forwardsecrecy-1.3.0/src/test/java/org/refcodes/forwardsecrecy/ForwardSecrecyWorkshopTest.java) 

## Change list &lt;refcodes-forwardsecrecy-alt&gt; (version 1.3.0)

* \[<span style="color:blue">ADDED</span>\] [`module-info.java.off`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-1.3.0/refcodes-forwardsecrecy-alt-filesystem/src/main/java/module-info.java.off) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-1.3.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-1.3.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-1.3.0/refcodes-forwardsecrecy-alt-filesystem/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`BasicForwardSecrecyFileSystemTest.java`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-1.3.0/refcodes-forwardsecrecy-alt-filesystem/src/test/java/org/refcodes/forwardsecrecy/alt/filesystem/BasicForwardSecrecyFileSystemTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`ForwardSecrecyFileSystemTest.java`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-1.3.0/refcodes-forwardsecrecy-alt-filesystem/src/test/java/org/refcodes/forwardsecrecy/alt/filesystem/ForwardSecrecyFileSystemTest.java) 

## Change list &lt;refcodes-io-ext&gt; (version 1.3.0)

* \[<span style="color:blue">ADDED</span>\] [`module-info.java.off`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-1.3.0/refcodes-io-ext-observable/src/main/java/module-info.java.off) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-1.3.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-1.3.0/refcodes-io-ext-observable/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-1.3.0/refcodes-io-ext-observable/README.md) 

## Change list &lt;refcodes-interceptor&gt; (version 1.3.0)

* \[<span style="color:blue">ADDED</span>\] [`module-info.java.off`](https://bitbucket.org/refcodes/refcodes-interceptor/src/refcodes-interceptor-1.3.0/src/main/java/module-info.java.off) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-interceptor/src/refcodes-interceptor-1.3.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-interceptor/src/refcodes-interceptor-1.3.0/README.md) 

## Change list &lt;refcodes-jobbus&gt; (version 1.3.0)

* \[<span style="color:blue">ADDED</span>\] [`module-info.java.off`](https://bitbucket.org/refcodes/refcodes-jobbus/src/refcodes-jobbus-1.3.0/src/main/java/module-info.java.off) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-jobbus/src/refcodes-jobbus-1.3.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-jobbus/src/refcodes-jobbus-1.3.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`JobBusImplTest.java`](https://bitbucket.org/refcodes/refcodes-jobbus/src/refcodes-jobbus-1.3.0/src/test/java/org/refcodes/jobbus/JobBusImplTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`JobBusProxyImplTest.java`](https://bitbucket.org/refcodes/refcodes-jobbus/src/refcodes-jobbus-1.3.0/src/test/java/org/refcodes/jobbus/JobBusProxyImplTest.java) 

## Change list &lt;refcodes-rest-ext&gt; (version 1.3.0)

* \[<span style="color:blue">ADDED</span>\] [`module-info.java.off`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-1.3.0/refcodes-rest-ext-eureka/src/main/java/module-info.java.off) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-1.3.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-1.3.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-1.3.0/refcodes-rest-ext-eureka/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-1.3.0/refcodes-rest-ext-eureka/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`EurekaRegistry.java`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-1.3.0/refcodes-rest-ext-eureka/src/main/java/org/refcodes/rest/ext/eureka/EurekaRegistry.java) (see Javadoc at [`EurekaRegistry.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest-ext-eureka/1.3.0/org/refcodes/rest/ext/eureka/EurekaRegistry.html))
* \[<span style="color:green">MODIFIED</span>\] [`EurekaDiscoverySidecarTest.java`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-1.3.0/refcodes-rest-ext-eureka/src/test/java/org/refcodes/rest/ext/eureka/EurekaDiscoverySidecarTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`EurekaRestClientTest.java`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-1.3.0/refcodes-rest-ext-eureka/src/test/java/org/refcodes/rest/ext/eureka/EurekaRestClientTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`EurekaRestServerTest.java`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-1.3.0/refcodes-rest-ext-eureka/src/test/java/org/refcodes/rest/ext/eureka/EurekaRestServerTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`EurekaServerDescriptorTest.java`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-1.3.0/refcodes-rest-ext-eureka/src/test/java/org/refcodes/rest/ext/eureka/EurekaServerDescriptorTest.java) 

## Change list &lt;refcodes-remoting&gt; (version 1.3.0)

* \[<span style="color:blue">ADDED</span>\] [`module-info.java.off`](https://bitbucket.org/refcodes/refcodes-remoting/src/refcodes-remoting-1.3.0/src/main/java/module-info.java.off) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-remoting/src/refcodes-remoting-1.3.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-remoting/src/refcodes-remoting-1.3.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`AbstractRemote.java`](https://bitbucket.org/refcodes/refcodes-remoting/src/refcodes-remoting-1.3.0/src/main/java/org/refcodes/remoting/AbstractRemote.java) (see Javadoc at [`AbstractRemote.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-remoting/1.3.0/org/refcodes/remoting/AbstractRemote.html))
* \[<span style="color:green">MODIFIED</span>\] [`IoStreamRemoteTest.java`](https://bitbucket.org/refcodes/refcodes-remoting/src/refcodes-remoting-1.3.0/src/test/java/org/refcodes/remoting/IoStreamRemoteTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`LoopbackRemoteTest.java`](https://bitbucket.org/refcodes/refcodes-remoting/src/refcodes-remoting-1.3.0/src/test/java/org/refcodes/remoting/LoopbackRemoteTest.java) 

## Change list &lt;refcodes-remoting-ext&gt; (version 1.3.0)

* \[<span style="color:blue">ADDED</span>\] [`module-info.java.off`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-1.3.0/refcodes-remoting-ext-observer/src/main/java/module-info.java.off) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-1.3.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-1.3.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-1.3.0/refcodes-remoting-ext-observer/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`ObservableLoopbackRemoteTest.java`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-1.3.0/refcodes-remoting-ext-observer/src/test/java/org/refcodes/remoting/ext/observer/ObservableLoopbackRemoteTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`ObservableSocketRemoteTest.java`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-1.3.0/refcodes-remoting-ext-observer/src/test/java/org/refcodes/remoting/ext/observer/ObservableSocketRemoteTest.java) 

## Change list &lt;refcodes-servicebus&gt; (version 1.3.0)

* \[<span style="color:blue">ADDED</span>\] [`module-info.java.off`](https://bitbucket.org/refcodes/refcodes-servicebus/src/refcodes-servicebus-1.3.0/src/main/java/module-info.java.off) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-servicebus/src/refcodes-servicebus-1.3.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`TestServiceAImpl.java`](https://bitbucket.org/refcodes/refcodes-servicebus/src/refcodes-servicebus-1.3.0/src/test/java/org/refcodes/servicebus/TestServiceAImpl.java) 
* \[<span style="color:green">MODIFIED</span>\] [`TestServiceBImpl.java`](https://bitbucket.org/refcodes/refcodes-servicebus/src/refcodes-servicebus-1.3.0/src/test/java/org/refcodes/servicebus/TestServiceBImpl.java) 

## Change list &lt;refcodes-servicebus-alt&gt; (version 1.3.0)

* \[<span style="color:blue">ADDED</span>\] [`module-info.java.off`](https://bitbucket.org/refcodes/refcodes-servicebus-alt/src/refcodes-servicebus-alt-1.3.0/refcodes-servicebus-alt-spring/src/main/java/module-info.java.off) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-servicebus-alt/src/refcodes-servicebus-alt-1.3.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-servicebus-alt/src/refcodes-servicebus-alt-1.3.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-servicebus-alt/src/refcodes-servicebus-alt-1.3.0/refcodes-servicebus-alt-spring/pom.xml) 

## Change list &lt;refcodes-tabular-alt&gt; (version 1.3.0)

* \[<span style="color:blue">ADDED</span>\] [`module-info.java.off`](https://bitbucket.org/refcodes/refcodes-tabular-alt/src/refcodes-tabular-alt-1.3.0/refcodes-tabular-alt-forwardsecrecy/src/main/java/module-info.java.off) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-tabular-alt/src/refcodes-tabular-alt-1.3.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-tabular-alt/src/refcodes-tabular-alt-1.3.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-tabular-alt/src/refcodes-tabular-alt-1.3.0/refcodes-tabular-alt-forwardsecrecy/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`ForwardsSecrecyColumnTest.java`](https://bitbucket.org/refcodes/refcodes-tabular-alt/src/refcodes-tabular-alt-1.3.0/refcodes-tabular-alt-forwardsecrecy/src/test/java/org/refcodes/tabular/alt/forwardsecrecy/ForwardsSecrecyColumnTest.java) 
