> This change list has been auto-generated on `triton` by `steiner` with `changelist-all.sh` on the 2021-12-22 at 11:51:53.

## Change list &lt;refcodes-forwardsecrecy-alt&gt; (version 2.2.1)

* \[<span style="color:red">DELETED</span>\] `BasicForwardSecrecyFileSystemTest.java`
* \[<span style="color:red">DELETED</span>\] `ForwardSecrecyFileSystemTest.java`
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-2.2.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-2.2.1/refcodes-forwardsecrecy-alt-filesystem/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`FileSystemDecryptionServer.java`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-2.2.1/refcodes-forwardsecrecy-alt-filesystem/src/main/java/org/refcodes/forwardsecrecy/alt/filesystem/FileSystemDecryptionServer.java) (see Javadoc at [`FileSystemDecryptionServer.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-forwardsecrecy-alt-filesystem/2.2.1/org.refcodes.forwardsecrecy.alt.filesystem/org/refcodes/forwardsecrecy/alt/filesystem/FileSystemDecryptionServer.html))
* \[<span style="color:green">MODIFIED</span>\] [`FileSystemEncryptionServer.java`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-2.2.1/refcodes-forwardsecrecy-alt-filesystem/src/main/java/org/refcodes/forwardsecrecy/alt/filesystem/FileSystemEncryptionServer.java) (see Javadoc at [`FileSystemEncryptionServer.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-forwardsecrecy-alt-filesystem/2.2.1/org.refcodes.forwardsecrecy.alt.filesystem/org/refcodes/forwardsecrecy/alt/filesystem/FileSystemEncryptionServer.html))

## Change list &lt;refcodes-io-ext&gt; (version 2.2.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-2.2.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-2.2.1/refcodes-io-ext-observer/pom.xml) 

## Change list &lt;refcodes-interceptor&gt; (version 2.2.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-interceptor/src/refcodes-interceptor-2.2.1/pom.xml) 

## Change list &lt;refcodes-jobbus&gt; (version 2.2.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-jobbus/src/refcodes-jobbus-2.2.1/pom.xml) 

## Change list &lt;refcodes-rest-ext&gt; (version 2.2.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-2.2.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-2.2.1/refcodes-rest-ext-eureka/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`EurekaRestServerTest.java`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-2.2.1/refcodes-rest-ext-eureka/src/test/java/org/refcodes/rest/ext/eureka/EurekaRestServerTest.java) 

## Change list &lt;refcodes-remoting&gt; (version 2.2.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-remoting/src/refcodes-remoting-2.2.1/pom.xml) 

## Change list &lt;refcodes-remoting-ext&gt; (version 2.2.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-2.2.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-2.2.1/refcodes-remoting-ext-observer/pom.xml) 

## Change list &lt;refcodes-serial&gt; (version 2.2.1)

* \[<span style="color:blue">ADDED</span>\] [`EnumSegment.java`](https://bitbucket.org/refcodes/refcodes-serial/src/refcodes-serial-2.2.1/src/main/java/org/refcodes/serial/EnumSegment.java) (see Javadoc at [`EnumSegment.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-serial/2.2.1/org.refcodes.serial/org/refcodes/serial/EnumSegment.html))
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial/src/refcodes-serial-2.2.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`ComplexTypeSegment.java`](https://bitbucket.org/refcodes/refcodes-serial/src/refcodes-serial-2.2.1/src/main/java/org/refcodes/serial/ComplexTypeSegment.java) (see Javadoc at [`ComplexTypeSegment.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-serial/2.2.1/org.refcodes.serial/org/refcodes/serial/ComplexTypeSegment.html))
* \[<span style="color:green">MODIFIED</span>\] [`IntSegment.java`](https://bitbucket.org/refcodes/refcodes-serial/src/refcodes-serial-2.2.1/src/main/java/org/refcodes/serial/IntSegment.java) (see Javadoc at [`IntSegment.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-serial/2.2.1/org.refcodes.serial/org/refcodes/serial/IntSegment.html))
* \[<span style="color:green">MODIFIED</span>\] [`SerialSugar.java`](https://bitbucket.org/refcodes/refcodes-serial/src/refcodes-serial-2.2.1/src/main/java/org/refcodes/serial/SerialSugar.java) (see Javadoc at [`SerialSugar.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-serial/2.2.1/org.refcodes.serial/org/refcodes/serial/SerialSugar.html))
* \[<span style="color:green">MODIFIED</span>\] [`ComplexTypeSegmentTest.java`](https://bitbucket.org/refcodes/refcodes-serial/src/refcodes-serial-2.2.1/src/test/java/org/refcodes/serial/ComplexTypeSegmentTest.java) 

## Change list &lt;refcodes-serial-alt&gt; (version 2.2.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial-alt/src/refcodes-serial-alt-2.2.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial-alt/src/refcodes-serial-alt-2.2.1/refcodes-serial-alt-net/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial-alt/src/refcodes-serial-alt-2.2.1/refcodes-serial-alt-tty/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`TtyPort.java`](https://bitbucket.org/refcodes/refcodes-serial-alt/src/refcodes-serial-alt-2.2.1/refcodes-serial-alt-tty/src/main/java/org/refcodes/serial/alt/tty/TtyPort.java) (see Javadoc at [`TtyPort.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-serial-alt-tty/2.2.1/org.refcodes.serial.alt.tty/org/refcodes/serial/alt/tty/TtyPort.html))
* \[<span style="color:green">MODIFIED</span>\] [`TtyPortMetrics.java`](https://bitbucket.org/refcodes/refcodes-serial-alt/src/refcodes-serial-alt-2.2.1/refcodes-serial-alt-tty/src/main/java/org/refcodes/serial/alt/tty/TtyPortMetrics.java) (see Javadoc at [`TtyPortMetrics.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-serial-alt-tty/2.2.1/org.refcodes.serial.alt.tty/org/refcodes/serial/alt/tty/TtyPortMetrics.html))

## Change list &lt;refcodes-serial-ext&gt; (version 2.2.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-2.2.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-2.2.1/refcodes-serial-ext-handshake/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-2.2.1/refcodes-serial-ext-observer/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-2.2.1/refcodes-serial-ext-security/pom.xml) 

## Change list &lt;refcodes-p2p&gt; (version 2.2.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-p2p/src/refcodes-p2p-2.2.1/pom.xml) 

## Change list &lt;refcodes-p2p-alt&gt; (version 2.2.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-p2p-alt/src/refcodes-p2p-alt-2.2.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-p2p-alt/src/refcodes-p2p-alt-2.2.1/refcodes-p2p-alt-rest/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-p2p-alt/src/refcodes-p2p-alt-2.2.1/refcodes-p2p-alt-serial/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`SerialP2PHeader.java`](https://bitbucket.org/refcodes/refcodes-p2p-alt/src/refcodes-p2p-alt-2.2.1/refcodes-p2p-alt-serial/src/main/java/org/refcodes/p2p/alt/serial/SerialP2PHeader.java) (see Javadoc at [`SerialP2PHeader.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-p2p-alt-serial/2.2.1/org.refcodes.p2p.alt.serial/org/refcodes/p2p/alt/serial/SerialP2PHeader.html))
* \[<span style="color:green">MODIFIED</span>\] [`SerialP2PMessage.java`](https://bitbucket.org/refcodes/refcodes-p2p-alt/src/refcodes-p2p-alt-2.2.1/refcodes-p2p-alt-serial/src/main/java/org/refcodes/p2p/alt/serial/SerialP2PMessage.java) (see Javadoc at [`SerialP2PMessage.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-p2p-alt-serial/2.2.1/org.refcodes.p2p.alt.serial/org/refcodes/p2p/alt/serial/SerialP2PMessage.html))

## Change list &lt;refcodes-p2p-ext&gt; (version 2.2.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-p2p-ext/src/refcodes-p2p-ext-2.2.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-p2p-ext/src/refcodes-p2p-ext-2.2.1/refcodes-p2p-ext-observer/pom.xml) 

## Change list &lt;refcodes-servicebus&gt; (version 2.2.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-servicebus/src/refcodes-servicebus-2.2.1/pom.xml) 

## Change list &lt;refcodes-servicebus-alt&gt; (version 2.2.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-servicebus-alt/src/refcodes-servicebus-alt-2.2.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-servicebus-alt/src/refcodes-servicebus-alt-2.2.1/refcodes-servicebus-alt-spring/pom.xml) 

## Change list &lt;refcodes-tabular-alt&gt; (version 2.2.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-tabular-alt/src/refcodes-tabular-alt-2.2.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-tabular-alt/src/refcodes-tabular-alt-2.2.1/refcodes-tabular-alt-forwardsecrecy/pom.xml) 

## Change list &lt;refcodes-archetype&gt; (version 2.2.1)

* \[<span style="color:blue">ADDED</span>\] [`module-info.java`](https://bitbucket.org/refcodes/refcodes-archetype/src/refcodes-archetype-2.2.1/src/main/java/module-info.java) (see Javadoc at [`module-info.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-archetype/2.2.1/./module-info.html))
* \[<span style="color:blue">ADDED</span>\] [`C2Helper.java`](https://bitbucket.org/refcodes/refcodes-archetype/src/refcodes-archetype-2.2.1/src/main/java/org/refcodes/archetype/C2Helper.java) (see Javadoc at [`C2Helper.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-archetype/2.2.1/org.refcodes.archetype/org/refcodes/archetype/C2Helper.html))
* \[<span style="color:blue">ADDED</span>\] [`CliHelper.java`](https://bitbucket.org/refcodes/refcodes-archetype/src/refcodes-archetype-2.2.1/src/main/java/org/refcodes/archetype/CliHelper.java) (see Javadoc at [`CliHelper.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-archetype/2.2.1/org.refcodes.archetype/org/refcodes/archetype/CliHelper.html))
* \[<span style="color:red">DELETED</span>\] `AbstractDaemon.java`
* \[<span style="color:red">DELETED</span>\] `TestDaemon.java`
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype/src/refcodes-archetype-2.2.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype/src/refcodes-archetype-2.2.1/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`application.conf`](https://bitbucket.org/refcodes/refcodes-archetype/src/refcodes-archetype-2.2.1/src/test/resources/application.conf) 

## Change list &lt;refcodes-archetype-alt&gt; (version 2.2.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-2.2.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-2.2.1/refcodes-archetype-alt-c2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-2.2.1/refcodes-archetype-alt-c2/src/main/resources/archetype-resources/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`Main.java`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-2.2.1/refcodes-archetype-alt-c2/src/main/resources/archetype-resources/src/main/java/Main.java) (see Javadoc at [`Main.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-archetype-alt-c2/src/main/resources/archetype-resources/2.2.1/src.main.resources.archetype-resources/Main.html))
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-2.2.1/refcodes-archetype-alt-cli/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-2.2.1/refcodes-archetype-alt-cli/src/main/resources/archetype-resources/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`Main.java`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-2.2.1/refcodes-archetype-alt-cli/src/main/resources/archetype-resources/src/main/java/Main.java) (see Javadoc at [`Main.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-archetype-alt-cli/src/main/resources/archetype-resources/2.2.1/src.main.resources.archetype-resources/Main.html))
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-2.2.1/refcodes-archetype-alt-csv/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-2.2.1/refcodes-archetype-alt-csv/src/main/resources/archetype-resources/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`Main.java`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-2.2.1/refcodes-archetype-alt-csv/src/main/resources/archetype-resources/src/main/java/Main.java) (see Javadoc at [`Main.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-archetype-alt-csv/src/main/resources/archetype-resources/2.2.1/src.main.resources.archetype-resources/Main.html))
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-2.2.1/refcodes-archetype-alt-eventbus/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-2.2.1/refcodes-archetype-alt-eventbus/src/main/resources/archetype-resources/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`Main.java`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-2.2.1/refcodes-archetype-alt-eventbus/src/main/resources/archetype-resources/src/main/java/Main.java) (see Javadoc at [`Main.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-archetype-alt-eventbus/src/main/resources/archetype-resources/2.2.1/src.main.resources.archetype-resources/Main.html))
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-2.2.1/refcodes-archetype-alt-rest/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-2.2.1/refcodes-archetype-alt-rest/src/main/resources/archetype-resources/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`Main.java`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-2.2.1/refcodes-archetype-alt-rest/src/main/resources/archetype-resources/src/main/java/Main.java) (see Javadoc at [`Main.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-archetype-alt-rest/src/main/resources/archetype-resources/2.2.1/src.main.resources.archetype-resources/Main.html))
