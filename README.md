# README #

> The [`REFCODES.ORG`](http://www.refcodes.org/refcodes) codes represent a group of artifacts consolidating parts of my work in the past years. Several topics are covered which I consider useful for you, programmers, developers and software engineers.

## What is this repository for? ##

***The `refcodes-meta` artifact is a meta artifact used for building the [`REFCODES.ORG`](http://www.refcodes.org/refcodes) artifacts ([`org.refcodes`](https://bitbucket.org/refcodes) group) from scratch, usually used by developers. Note that the tools (a bunch of shell scripts) build around this meta artifact are not bundled with this meta artifact.***

## How do I get set up? ##

Clone all the repos by invoking `./clone-all.sh` for basic `HTTP` authentication or `./clone-all-ssh.sh` when using `SSH` authentication with public and provate keys.

## Contribution guidelines ##

* [Report issues](https://bitbucket.org/refcodes/refcodes-meta/issues)
* Finding bugs
* Helping fixing bugs
* Making code and documentation better
* Enhance the code

## Who do I talk to? ##

* Siegfried Steiner (steiner@refcodes.org)

## Terms and conditions ##

The [`REFCODES.ORG`](http://www.refcodes.org/refcodes) group of artifacts is published under some open source licenses; covered by the  [`refcodes-licensing`](https://bitbucket.org/refcodes/refcodes-licensing) ([`org.refcodes`](https://bitbucket.org/refcodes) group) artifact - evident in each artifact in question as of the `pom.xml` dependency included in such artifact.
