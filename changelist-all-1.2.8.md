# Change list for REFCODES.ORG artifacts' version 1.2.8

> This change list has been auto-generated on `triton` by `steiner` with with `changelist-all.sh` on the 2018-05-26 at 21:00:49.

## Change list &lt;refcodes-licensing&gt; (version 1.2.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-licensing/src/refcodes-licensing-1.2.8/pom.xml) 

## Change list &lt;refcodes-parent&gt; (version 1.2.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-parent/src/refcodes-parent-1.2.8/pom.xml) 

## Change list &lt;refcodes-time&gt; (version 1.2.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-time/src/refcodes-time-1.2.8/pom.xml) 

## Change list &lt;refcodes-mixin&gt; (version 1.2.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-mixin/src/refcodes-mixin-1.2.8/pom.xml) 

## Change list &lt;refcodes-data&gt; (version 1.2.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data/src/refcodes-data-1.2.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`EnvironmentVariable.java`](https://bitbucket.org/refcodes/refcodes-data/src/refcodes-data-1.2.8/src/main/java/org/refcodes/data/EnvironmentVariable.java) (see Javadoc at [`EnvironmentVariable.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-data/1.2.8/org/refcodes/data/EnvironmentVariable.html))

## Change list &lt;refcodes-exception&gt; (version 1.2.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-exception/src/refcodes-exception-1.2.8/pom.xml) 

## Change list &lt;refcodes-factory&gt; (version 1.2.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-factory/src/refcodes-factory-1.2.8/pom.xml) 

## Change list &lt;refcodes-factory-alt&gt; (version 1.2.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-factory-alt/src/refcodes-factory-alt-1.2.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-factory-alt/src/refcodes-factory-alt-1.2.8/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-factory-alt/src/refcodes-factory-alt-1.2.8/refcodes-factory-alt-spring/pom.xml) 

## Change list &lt;refcodes-controlflow&gt; (version 1.2.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-controlflow/src/refcodes-controlflow-1.2.8/pom.xml) 

## Change list &lt;refcodes-numerical&gt; (version 1.2.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-numerical/src/refcodes-numerical-1.2.8/pom.xml) 

## Change list &lt;refcodes-generator&gt; (version 1.2.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-generator/src/refcodes-generator-1.2.8/pom.xml) 

## Change list &lt;refcodes-structure&gt; (version 1.2.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-1.2.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-1.2.8/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`PathMapBuilderImpl.java`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-1.2.8/src/main/java/org/refcodes/structure/PathMapBuilderImpl.java) (see Javadoc at [`PathMapBuilderImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-structure/1.2.8/org/refcodes/structure/PathMapBuilderImpl.html))

## Change list &lt;refcodes-runtime&gt; (version 1.2.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-1.2.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`SystemContext.java`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-1.2.8/src/main/java/org/refcodes/runtime/SystemContext.java) (see Javadoc at [`SystemContext.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-runtime/1.2.8/org/refcodes/runtime/SystemContext.html))
* \[<span style="color:green">MODIFIED</span>\] [`SystemUtility.java`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-1.2.8/src/main/java/org/refcodes/runtime/SystemUtility.java) (see Javadoc at [`SystemUtility.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-runtime/1.2.8/org/refcodes/runtime/SystemUtility.html))
* \[<span style="color:green">MODIFIED</span>\] [`SystemContextTest.java`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-1.2.8/src/test/java/org/refcodes/runtime/SystemContextTest.java) 

## Change list &lt;refcodes-component&gt; (version 1.2.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-1.2.8/pom.xml) 

## Change list &lt;refcodes-data-ext&gt; (version 1.2.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.2.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.2.8/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.2.8/refcodes-data-ext-boulderdash/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.2.8/refcodes-data-ext-checkers/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.2.8/refcodes-data-ext-checkers/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.2.8/refcodes-data-ext-chess/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.2.8/refcodes-data-ext-chess/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.2.8/refcodes-data-ext-corporate/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.2.8/refcodes-data-ext-symbols/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.2.8/refcodes-data-ext-symbols/README.md) 

## Change list &lt;refcodes-graphical&gt; (version 1.2.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-graphical/src/refcodes-graphical-1.2.8/pom.xml) 

## Change list &lt;refcodes-textual&gt; (version 1.2.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-1.2.8/pom.xml) 

## Change list &lt;refcodes-criteria&gt; (version 1.2.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-1.2.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-1.2.8/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`ASTNodeFactory.java`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-1.2.8/src/main/java/org/matheclipse/parser/client/operator/ASTNodeFactory.java) (see Javadoc at [`ASTNodeFactory.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-criteria/1.2.8/org/matheclipse/parser/client/operator/ASTNodeFactory.html))
* \[<span style="color:green">MODIFIED</span>\] [`Scanner.java`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-1.2.8/src/main/java/org/matheclipse/parser/client/Scanner.java) (see Javadoc at [`Scanner.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-criteria/1.2.8/org/matheclipse/parser/client/Scanner.html))

## Change list &lt;refcodes-tabular&gt; (version 1.2.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-tabular/src/refcodes-tabular-1.2.8/pom.xml) 

## Change list &lt;refcodes-logger&gt; (version 1.2.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger/src/refcodes-logger-1.2.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger/src/refcodes-logger-1.2.8/README.md) 

## Change list &lt;refcodes-logger-alt&gt; (version 1.2.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.2.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.2.8/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.2.8/refcodes-logger-alt-async/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.2.8/refcodes-logger-alt-async/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.2.8/refcodes-logger-alt-console/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.2.8/refcodes-logger-alt-console/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.2.8/refcodes-logger-alt-io/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.2.8/refcodes-logger-alt-io/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.2.8/refcodes-logger-alt-simpledb/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.2.8/refcodes-logger-alt-slf4j/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.2.8/refcodes-logger-alt-spring/pom.xml) 

## Change list &lt;refcodes-graphical-ext&gt; (version 1.2.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-1.2.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-1.2.8/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-1.2.8/refcodes-graphical-ext-javafx/pom.xml) 

## Change list &lt;refcodes-matcher&gt; (version 1.2.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-matcher/src/refcodes-matcher-1.2.8/pom.xml) 

## Change list &lt;refcodes-observer&gt; (version 1.2.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-observer/src/refcodes-observer-1.2.8/pom.xml) 

## Change list &lt;refcodes-checkerboard&gt; (version 1.2.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-1.2.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-1.2.8/README.md) 

## Change list &lt;refcodes-checkerboard-alt&gt; (version 1.2.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-alt/src/refcodes-checkerboard-alt-1.2.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-alt/src/refcodes-checkerboard-alt-1.2.8/refcodes-checkerboard-alt-javafx/pom.xml) 

## Change list &lt;refcodes-checkerboard-ext&gt; (version 1.2.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-ext/src/refcodes-checkerboard-ext-1.2.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-checkerboard-ext/src/refcodes-checkerboard-ext-1.2.8/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-ext/src/refcodes-checkerboard-ext-1.2.8/refcodes-checkerboard-ext-javafx-boulderdash/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-ext/src/refcodes-checkerboard-ext-1.2.8/refcodes-checkerboard-ext-javafx-chess/pom.xml) 

## Change list &lt;refcodes-command&gt; (version 1.2.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-command/src/refcodes-command-1.2.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-command/src/refcodes-command-1.2.8/README.md) 

## Change list &lt;refcodes-cli&gt; (version 1.2.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-1.2.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-1.2.8/README.md) 

## Change list &lt;refcodes-boulderdash&gt; (version 1.2.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-boulderdash/src/refcodes-boulderdash-1.2.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-boulderdash/src/refcodes-boulderdash-1.2.8/README.md) 

## Change list &lt;refcodes-io&gt; (version 1.2.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-io/src/refcodes-io-1.2.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-io/src/refcodes-io-1.2.8/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`org.refcodes.io.receiver`](https://bitbucket.org/refcodes/refcodes-io/src/refcodes-io-1.2.8/src/main/java/org/refcodes/io/org.refcodes.io.receiver) 
* \[<span style="color:green">MODIFIED</span>\] [`org.refcodes.io.receiver`](https://bitbucket.org/refcodes/refcodes-io/src/refcodes-io-1.2.8/src/main/java/org/refcodes/io/org.refcodes.io.receiver) 
* \[<span style="color:green">MODIFIED</span>\] [`org.refcodes.io.sender`](https://bitbucket.org/refcodes/refcodes-io/src/refcodes-io-1.2.8/src/main/java/org/refcodes/io/org.refcodes.io.sender) 
* \[<span style="color:green">MODIFIED</span>\] [`org.refcodes.io.sender`](https://bitbucket.org/refcodes/refcodes-io/src/refcodes-io-1.2.8/src/main/java/org/refcodes/io/org.refcodes.io.sender) 

## Change list &lt;refcodes-codec&gt; (version 1.2.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-1.2.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-1.2.8/README.md) 

## Change list &lt;refcodes-component-ext&gt; (version 1.2.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.2.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.2.8/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.2.8/refcodes-component-ext-observer/pom.xml) 

## Change list &lt;refcodes-properties&gt; (version 1.2.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.2.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.2.8/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`package-info.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.2.8/src/main/java/org/refcodes/configuration/package-info.java) 

## Change list &lt;refcodes-security&gt; (version 1.2.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security/src/refcodes-security-1.2.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security/src/refcodes-security-1.2.8/README.md) 

## Change list &lt;refcodes-net&gt; (version 1.2.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.2.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`ApplicationFormFactory.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.2.8/src/main/java/org/refcodes/net/ApplicationFormFactory.java) (see Javadoc at [`ApplicationFormFactory.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/1.2.8/org/refcodes/net/ApplicationFormFactory.html))
* \[<span style="color:green">MODIFIED</span>\] [`ApplicationJsonFactory.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.2.8/src/main/java/org/refcodes/net/ApplicationJsonFactory.java) (see Javadoc at [`ApplicationJsonFactory.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/1.2.8/org/refcodes/net/ApplicationJsonFactory.html))
* \[<span style="color:green">MODIFIED</span>\] [`ApplicationXmlFactory.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.2.8/src/main/java/org/refcodes/net/ApplicationXmlFactory.java) (see Javadoc at [`ApplicationXmlFactory.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/1.2.8/org/refcodes/net/ApplicationXmlFactory.html))
* \[<span style="color:green">MODIFIED</span>\] [`ApplicationYamlFactory.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.2.8/src/main/java/org/refcodes/net/ApplicationYamlFactory.java) (see Javadoc at [`ApplicationYamlFactory.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/1.2.8/org/refcodes/net/ApplicationYamlFactory.html))
* \[<span style="color:green">MODIFIED</span>\] [`BasicAuthObservable.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.2.8/src/main/java/org/refcodes/net/BasicAuthObservable.java) (see Javadoc at [`BasicAuthObservable.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/1.2.8/org/refcodes/net/BasicAuthObservable.html))
* \[<span style="color:green">MODIFIED</span>\] [`BasicAuthResponse.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.2.8/src/main/java/org/refcodes/net/BasicAuthResponse.java) (see Javadoc at [`BasicAuthResponse.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/1.2.8/org/refcodes/net/BasicAuthResponse.html))
* \[<span style="color:green">MODIFIED</span>\] [`FormFields.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.2.8/src/main/java/org/refcodes/net/FormFields.java) (see Javadoc at [`FormFields.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/1.2.8/org/refcodes/net/FormFields.html))
* \[<span style="color:green">MODIFIED</span>\] [`HttpsConnectionRequestObservable.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.2.8/src/main/java/org/refcodes/net/HttpsConnectionRequestObservable.java) (see Javadoc at [`HttpsConnectionRequestObservable.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/1.2.8/org/refcodes/net/HttpsConnectionRequestObservable.html))
* \[<span style="color:green">MODIFIED</span>\] [`HttpServerRequestImpl.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.2.8/src/main/java/org/refcodes/net/HttpServerRequestImpl.java) (see Javadoc at [`HttpServerRequestImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/1.2.8/org/refcodes/net/HttpServerRequestImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`refcodes-net`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.2.8/src/main/java/org/refcodes/net/refcodes-net) 
* \[<span style="color:green">MODIFIED</span>\] [`TextPlainFactory.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.2.8/src/main/java/org/refcodes/net/TextPlainFactory.java) (see Javadoc at [`TextPlainFactory.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/1.2.8/org/refcodes/net/TextPlainFactory.html))

## Change list &lt;refcodes-rest&gt; (version 1.2.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.2.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.2.8/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`HttpRestServerImpl.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.2.8/src/main/java/org/refcodes/rest/HttpRestServerImpl.java) (see Javadoc at [`HttpRestServerImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/1.2.8/org/refcodes/rest/HttpRestServerImpl.html))

## Change list &lt;refcodes-logger-ext&gt; (version 1.2.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-1.2.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-1.2.8/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-1.2.8/refcodes-logger-ext-slf4j/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-1.2.8/refcodes-logger-ext-slf4j/README.md) 

## Change list &lt;refcodes-daemon&gt; (version 1.2.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-daemon/src/refcodes-daemon-1.2.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-daemon/src/refcodes-daemon-1.2.8/README.md) 

## Change list &lt;refcodes-eventbus&gt; (version 1.2.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-eventbus/src/refcodes-eventbus-1.2.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-eventbus/src/refcodes-eventbus-1.2.8/README.md) 

## Change list &lt;refcodes-eventbus-ext&gt; (version 1.2.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-1.2.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-1.2.8/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-1.2.8/refcodes-eventbus-ext-application/pom.xml) 

## Change list &lt;refcodes-filesystem&gt; (version 1.2.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-filesystem/src/refcodes-filesystem-1.2.8/pom.xml) 

## Change list &lt;refcodes-filesystem-alt&gt; (version 1.2.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-filesystem-alt/src/refcodes-filesystem-alt-1.2.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-filesystem-alt/src/refcodes-filesystem-alt-1.2.8/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-filesystem-alt/src/refcodes-filesystem-alt-1.2.8/refcodes-filesystem-alt-s3/pom.xml) 

## Change list &lt;refcodes-forwardsecrecy&gt; (version 1.2.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy/src/refcodes-forwardsecrecy-1.2.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy/src/refcodes-forwardsecrecy-1.2.8/README.md) 

## Change list &lt;refcodes-forwardsecrecy-alt&gt; (version 1.2.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-1.2.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-1.2.8/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-1.2.8/refcodes-forwardsecrecy-alt-filesystem/pom.xml) 

## Change list &lt;refcodes-io-ext&gt; (version 1.2.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-1.2.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-1.2.8/refcodes-io-ext-observable/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-1.2.8/refcodes-io-ext-observable/README.md) 

## Change list &lt;refcodes-interceptor&gt; (version 1.2.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-interceptor/src/refcodes-interceptor-1.2.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-interceptor/src/refcodes-interceptor-1.2.8/README.md) 

## Change list &lt;refcodes-jobbus&gt; (version 1.2.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-jobbus/src/refcodes-jobbus-1.2.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-jobbus/src/refcodes-jobbus-1.2.8/README.md) 

## Change list &lt;refcodes-rest-ext&gt; (version 1.2.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-1.2.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-1.2.8/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-1.2.8/refcodes-rest-ext-eureka/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-1.2.8/refcodes-rest-ext-eureka/README.md) 

## Change list &lt;refcodes-remoting&gt; (version 1.2.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-remoting/src/refcodes-remoting-1.2.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-remoting/src/refcodes-remoting-1.2.8/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`Reply.java`](https://bitbucket.org/refcodes/refcodes-remoting/src/refcodes-remoting-1.2.8/src/main/java/org/refcodes/remoting/Reply.java) (see Javadoc at [`Reply.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-remoting/1.2.8/org/refcodes/remoting/Reply.html))

## Change list &lt;refcodes-remoting-ext&gt; (version 1.2.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-1.2.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-1.2.8/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-1.2.8/refcodes-remoting-ext-observer/pom.xml) 

## Change list &lt;refcodes-security-alt&gt; (version 1.2.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-1.2.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-1.2.8/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-1.2.8/refcodes-security-alt-chaos/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-1.2.8/refcodes-security-alt-chaos/README.md) 

## Change list &lt;refcodes-security-ext&gt; (version 1.2.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-1.2.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-1.2.8/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-1.2.8/refcodes-security-ext-chaos/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-1.2.8/refcodes-security-ext-chaos/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-1.2.8/refcodes-security-ext-spring/pom.xml) 

## Change list &lt;refcodes-properties-ext&gt; (version 1.2.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.2.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.2.8/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.2.8/refcodes-properties-ext-cli/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.2.8/refcodes-properties-ext-cli/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.2.8/refcodes-properties-ext-obfuscation/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.2.8/refcodes-properties-ext-obfuscation/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.2.8/refcodes-properties-ext-observer/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.2.8/refcodes-properties-ext-observer/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.2.8/refcodes-properties-ext-runtime/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.2.8/refcodes-properties-ext-runtime/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`RuntimePropertiesImpl.java`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.2.8/refcodes-properties-ext-runtime/src/main/java/org/refcodes/configuration/ext/runtime/RuntimePropertiesImpl.java) (see Javadoc at [`RuntimePropertiesImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties-ext-runtime/1.2.8/org/refcodes/configuration/ext/runtime/RuntimePropertiesImpl.html))

## Change list &lt;refcodes-servicebus&gt; (version 1.2.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-servicebus/src/refcodes-servicebus-1.2.8/pom.xml) 

## Change list &lt;refcodes-servicebus-alt&gt; (version 1.2.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-servicebus-alt/src/refcodes-servicebus-alt-1.2.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-servicebus-alt/src/refcodes-servicebus-alt-1.2.8/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-servicebus-alt/src/refcodes-servicebus-alt-1.2.8/refcodes-servicebus-alt-spring/pom.xml) 

## Change list &lt;refcodes-tabular-alt&gt; (version 1.2.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-tabular-alt/src/refcodes-tabular-alt-1.2.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-tabular-alt/src/refcodes-tabular-alt-1.2.8/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-tabular-alt/src/refcodes-tabular-alt-1.2.8/refcodes-tabular-alt-forwardsecrecy/pom.xml) 
