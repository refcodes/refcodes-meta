# Change list for REFCODES.ORG artifacts' version 1.10.1

> This change list has been auto-generated on `triton` by `steiner` with with `changelist-all.sh` on the 2018-12-24 at 19:16:03.

## Change list &lt;refcodes-licensing&gt; (version 1.10.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-licensing/src/refcodes-licensing-1.10.1/pom.xml) 

## Change list &lt;refcodes-parent&gt; (version 1.10.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-parent/src/refcodes-parent-1.10.1/pom.xml) 

## Change list &lt;refcodes-time&gt; (version 1.10.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-time/src/refcodes-time-1.10.1/pom.xml) 

## Change list &lt;refcodes-mixin&gt; (version 1.10.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-mixin/src/refcodes-mixin-1.10.1/pom.xml) 

## Change list &lt;refcodes-data&gt; (version 1.10.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data/src/refcodes-data-1.10.1/pom.xml) 

## Change list &lt;refcodes-exception&gt; (version 1.10.1)

* \[<span style="color:blue">ADDED</span>\] [`BreakOutException.java`](https://bitbucket.org/refcodes/refcodes-exception/src/refcodes-exception-1.10.1/src/main/java/org/refcodes/exception/BreakOutException.java) (see Javadoc at [`BreakOutException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-exception/1.10.1/org/refcodes/exception/BreakOutException.html))
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-exception/src/refcodes-exception-1.10.1/pom.xml) 

## Change list &lt;refcodes-factory&gt; (version 1.10.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-factory/src/refcodes-factory-1.10.1/pom.xml) 

## Change list &lt;refcodes-factory-alt&gt; (version 1.10.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-factory-alt/src/refcodes-factory-alt-1.10.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-factory-alt/src/refcodes-factory-alt-1.10.1/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-factory-alt/src/refcodes-factory-alt-1.10.1/refcodes-factory-alt-spring/pom.xml) 

## Change list &lt;refcodes-controlflow&gt; (version 1.10.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-controlflow/src/refcodes-controlflow-1.10.1/pom.xml) 

## Change list &lt;refcodes-numerical&gt; (version 1.10.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-numerical/src/refcodes-numerical-1.10.1/pom.xml) 

## Change list &lt;refcodes-generator&gt; (version 1.10.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-generator/src/refcodes-generator-1.10.1/pom.xml) 

## Change list &lt;refcodes-matcher&gt; (version 1.10.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-matcher/src/refcodes-matcher-1.10.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`WildcardSubstitutesImpl.java`](https://bitbucket.org/refcodes/refcodes-matcher/src/refcodes-matcher-1.10.1/src/main/java/org/refcodes/matcher/WildcardSubstitutesImpl.java) (see Javadoc at [`WildcardSubstitutesImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-matcher/1.10.1/org/refcodes/matcher/WildcardSubstitutesImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`WildcardSubstitutes.java`](https://bitbucket.org/refcodes/refcodes-matcher/src/refcodes-matcher-1.10.1/src/main/java/org/refcodes/matcher/WildcardSubstitutes.java) (see Javadoc at [`WildcardSubstitutes.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-matcher/1.10.1/org/refcodes/matcher/WildcardSubstitutes.html))
* \[<span style="color:green">MODIFIED</span>\] [`PathMatcherTest.java`](https://bitbucket.org/refcodes/refcodes-matcher/src/refcodes-matcher-1.10.1/src/test/java/org/refcodes/matcher/PathMatcherTest.java) 

## Change list &lt;refcodes-structure&gt; (version 1.10.1)

* \[<span style="color:blue">ADDED</span>\] [`CanonicalMapTestFixures.java`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-1.10.1/src/test/java/org/refcodes/structure/CanonicalMapTestFixures.java) 
* \[<span style="color:blue">ADDED</span>\] [`PathMapArrayTest.java`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-1.10.1/src/test/java/org/refcodes/structure/PathMapArrayTest.java) 
* \[<span style="color:blue">ADDED</span>\] [`PathMapDirTest.java`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-1.10.1/src/test/java/org/refcodes/structure/PathMapDirTest.java) 
* \[<span style="color:blue">ADDED</span>\] [`PathMapPathTest.java`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-1.10.1/src/test/java/org/refcodes/structure/PathMapPathTest.java) 
* \[<span style="color:blue">ADDED</span>\] [`PathMapQueryTest.java`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-1.10.1/src/test/java/org/refcodes/structure/PathMapQueryTest.java) 
* \[<span style="color:blue">ADDED</span>\] [`PathMapTestFixures.java`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-1.10.1/src/test/java/org/refcodes/structure/PathMapTestFixures.java) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-1.10.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-1.10.1/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`CanonicalMap.java`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-1.10.1/src/main/java/org/refcodes/structure/CanonicalMap.java) (see Javadoc at [`CanonicalMap.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-structure/1.10.1/org/refcodes/structure/CanonicalMap.html))
* \[<span style="color:green">MODIFIED</span>\] [`PathMapBuilderImpl.java`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-1.10.1/src/main/java/org/refcodes/structure/PathMapBuilderImpl.java) (see Javadoc at [`PathMapBuilderImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-structure/1.10.1/org/refcodes/structure/PathMapBuilderImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`PathMap.java`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-1.10.1/src/main/java/org/refcodes/structure/PathMap.java) (see Javadoc at [`PathMap.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-structure/1.10.1/org/refcodes/structure/PathMap.html))
* \[<span style="color:green">MODIFIED</span>\] [`PropertiesAccessorMixin.java`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-1.10.1/src/main/java/org/refcodes/structure/PropertiesAccessorMixin.java) (see Javadoc at [`PropertiesAccessorMixin.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-structure/1.10.1/org/refcodes/structure/PropertiesAccessorMixin.html))
* \[<span style="color:green">MODIFIED</span>\] [`RangeImpl.java`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-1.10.1/src/main/java/org/refcodes/structure/RangeImpl.java) (see Javadoc at [`RangeImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-structure/1.10.1/org/refcodes/structure/RangeImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`StructureUtility.java`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-1.10.1/src/main/java/org/refcodes/structure/StructureUtility.java) (see Javadoc at [`StructureUtility.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-structure/1.10.1/org/refcodes/structure/StructureUtility.html))
* \[<span style="color:green">MODIFIED</span>\] [`CanonicalMapTest.java`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-1.10.1/src/test/java/org/refcodes/structure/CanonicalMapTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`PathMapTest.java`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-1.10.1/src/test/java/org/refcodes/structure/PathMapTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`PropertyTest.java`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-1.10.1/src/test/java/org/refcodes/structure/PropertyTest.java) 

## Change list &lt;refcodes-runtime&gt; (version 1.10.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-1.10.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-1.10.1/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`application.sh`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-1.10.1/src/main/scripts/application.sh) 

## Change list &lt;refcodes-component&gt; (version 1.10.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-1.10.1/pom.xml) 

## Change list &lt;refcodes-data-ext&gt; (version 1.10.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.10.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.10.1/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.10.1/refcodes-data-ext-boulderdash/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.10.1/refcodes-data-ext-checkers/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.10.1/refcodes-data-ext-checkers/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.10.1/refcodes-data-ext-chess/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.10.1/refcodes-data-ext-chess/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.10.1/refcodes-data-ext-corporate/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.10.1/refcodes-data-ext-corporate/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.10.1/refcodes-data-ext-symbols/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.10.1/refcodes-data-ext-symbols/README.md) 

## Change list &lt;refcodes-graphical&gt; (version 1.10.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-graphical/src/refcodes-graphical-1.10.1/pom.xml) 

## Change list &lt;refcodes-textual&gt; (version 1.10.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-1.10.1/pom.xml) 

## Change list &lt;refcodes-criteria&gt; (version 1.10.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-1.10.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-1.10.1/README.md) 

## Change list &lt;refcodes-tabular&gt; (version 1.10.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-tabular/src/refcodes-tabular-1.10.1/pom.xml) 

## Change list &lt;refcodes-observer&gt; (version 1.10.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-observer/src/refcodes-observer-1.10.1/pom.xml) 

## Change list &lt;refcodes-command&gt; (version 1.10.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-command/src/refcodes-command-1.10.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-command/src/refcodes-command-1.10.1/README.md) 

## Change list &lt;refcodes-cli&gt; (version 1.10.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-1.10.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-1.10.1/README.md) 

## Change list &lt;refcodes-io&gt; (version 1.10.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-io/src/refcodes-io-1.10.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-io/src/refcodes-io-1.10.1/README.md) 

## Change list &lt;refcodes-codec&gt; (version 1.10.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-1.10.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-1.10.1/README.md) 

## Change list &lt;refcodes-component-ext&gt; (version 1.10.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.10.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.10.1/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.10.1/refcodes-component-ext-observer/pom.xml) 

## Change list &lt;refcodes-properties&gt; (version 1.10.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.10.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.10.1/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`JavaPropertiesBuilder.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.10.1/src/main/java/org/refcodes/configuration/JavaPropertiesBuilder.java) (see Javadoc at [`JavaPropertiesBuilder.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties/1.10.1/org/refcodes/configuration/JavaPropertiesBuilder.html))
* \[<span style="color:green">MODIFIED</span>\] [`package-info.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.10.1/src/main/java/org/refcodes/configuration/package-info.java) 
* \[<span style="color:green">MODIFIED</span>\] [`Properties.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.10.1/src/main/java/org/refcodes/configuration/Properties.java) (see Javadoc at [`Properties.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties/1.10.1/org/refcodes/configuration/Properties.html))
* \[<span style="color:green">MODIFIED</span>\] [`TomlPropertiesBuilder.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.10.1/src/main/java/org/refcodes/configuration/TomlPropertiesBuilder.java) (see Javadoc at [`TomlPropertiesBuilder.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties/1.10.1/org/refcodes/configuration/TomlPropertiesBuilder.html))
* \[<span style="color:green">MODIFIED</span>\] [`XmlPropertiesBuilder.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.10.1/src/main/java/org/refcodes/configuration/XmlPropertiesBuilder.java) (see Javadoc at [`XmlPropertiesBuilder.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties/1.10.1/org/refcodes/configuration/XmlPropertiesBuilder.html))
* \[<span style="color:green">MODIFIED</span>\] [`IniPropertiesTest.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.10.1/src/test/java/org/refcodes/configuration/IniPropertiesTest.java) 

## Change list &lt;refcodes-security&gt; (version 1.10.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security/src/refcodes-security-1.10.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security/src/refcodes-security-1.10.1/README.md) 

## Change list &lt;refcodes-security-alt&gt; (version 1.10.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-1.10.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-1.10.1/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-1.10.1/refcodes-security-alt-chaos/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-1.10.1/refcodes-security-alt-chaos/README.md) 

## Change list &lt;refcodes-security-ext&gt; (version 1.10.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-1.10.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-1.10.1/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-1.10.1/refcodes-security-ext-chaos/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-1.10.1/refcodes-security-ext-chaos/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-1.10.1/refcodes-security-ext-spring/pom.xml) 

## Change list &lt;refcodes-properties-ext&gt; (version 1.10.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.10.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.10.1/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.10.1/refcodes-properties-ext-cli/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.10.1/refcodes-properties-ext-cli/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.10.1/refcodes-properties-ext-obfuscation/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.10.1/refcodes-properties-ext-obfuscation/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.10.1/refcodes-properties-ext-observer/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.10.1/refcodes-properties-ext-observer/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.10.1/refcodes-properties-ext-runtime/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.10.1/refcodes-properties-ext-runtime/README.md) 

## Change list &lt;refcodes-logger&gt; (version 1.10.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger/src/refcodes-logger-1.10.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger/src/refcodes-logger-1.10.1/README.md) 

## Change list &lt;refcodes-logger-alt&gt; (version 1.10.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.10.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.10.1/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.10.1/refcodes-logger-alt-async/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.10.1/refcodes-logger-alt-async/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.10.1/refcodes-logger-alt-console/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.10.1/refcodes-logger-alt-console/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.10.1/refcodes-logger-alt-io/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.10.1/refcodes-logger-alt-io/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.10.1/refcodes-logger-alt-simpledb/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.10.1/refcodes-logger-alt-slf4j/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.10.1/refcodes-logger-alt-spring/pom.xml) 

## Change list &lt;refcodes-logger-ext&gt; (version 1.10.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-1.10.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-1.10.1/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-1.10.1/refcodes-logger-ext-slf4j/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-1.10.1/refcodes-logger-ext-slf4j/README.md) 

## Change list &lt;refcodes-graphical-ext&gt; (version 1.10.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-1.10.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-1.10.1/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-1.10.1/refcodes-graphical-ext-javafx/pom.xml) 

## Change list &lt;refcodes-checkerboard&gt; (version 1.10.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-1.10.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-1.10.1/README.md) 

## Change list &lt;refcodes-checkerboard-alt&gt; (version 1.10.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-alt/src/refcodes-checkerboard-alt-1.10.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-alt/src/refcodes-checkerboard-alt-1.10.1/refcodes-checkerboard-alt-javafx/pom.xml) 

## Change list &lt;refcodes-checkerboard-ext&gt; (version 1.10.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-ext/src/refcodes-checkerboard-ext-1.10.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-checkerboard-ext/src/refcodes-checkerboard-ext-1.10.1/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-ext/src/refcodes-checkerboard-ext-1.10.1/refcodes-checkerboard-ext-javafx-boulderdash/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-ext/src/refcodes-checkerboard-ext-1.10.1/refcodes-checkerboard-ext-javafx-chess/pom.xml) 

## Change list &lt;refcodes-boulderdash&gt; (version 1.10.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-boulderdash/src/refcodes-boulderdash-1.10.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-boulderdash/src/refcodes-boulderdash-1.10.1/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`BoulderDashAutomaton.java`](https://bitbucket.org/refcodes/refcodes-boulderdash/src/refcodes-boulderdash-1.10.1/src/main/java/org/refcodes/boulderdash/BoulderDashAutomaton.java) (see Javadoc at [`BoulderDashAutomaton.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-boulderdash/1.10.1/org/refcodes/boulderdash/BoulderDashAutomaton.html))

## Change list &lt;refcodes-net&gt; (version 1.10.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.10.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`HttpBodyMap.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.10.1/src/main/java/org/refcodes/net/HttpBodyMap.java) (see Javadoc at [`HttpBodyMap.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/1.10.1/org/refcodes/net/HttpBodyMap.html))

## Change list &lt;refcodes-rest&gt; (version 1.10.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.10.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.10.1/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`BasicAuthEventImpl.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.10.1/src/main/java/org/refcodes/rest/BasicAuthEventImpl.java) (see Javadoc at [`BasicAuthEventImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/1.10.1/org/refcodes/rest/BasicAuthEventImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`HttpRestClientImpl.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.10.1/src/main/java/org/refcodes/rest/HttpRestClientImpl.java) (see Javadoc at [`HttpRestClientImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/1.10.1/org/refcodes/rest/HttpRestClientImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`RestRequestEventImpl.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.10.1/src/main/java/org/refcodes/rest/RestRequestEventImpl.java) (see Javadoc at [`RestRequestEventImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/1.10.1/org/refcodes/rest/RestRequestEventImpl.html))

## Change list &lt;refcodes-daemon&gt; (version 1.10.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-daemon/src/refcodes-daemon-1.10.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-daemon/src/refcodes-daemon-1.10.1/README.md) 

## Change list &lt;refcodes-eventbus&gt; (version 1.10.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-eventbus/src/refcodes-eventbus-1.10.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-eventbus/src/refcodes-eventbus-1.10.1/README.md) 

## Change list &lt;refcodes-eventbus-ext&gt; (version 1.10.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-1.10.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-1.10.1/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-1.10.1/refcodes-eventbus-ext-application/pom.xml) 

## Change list &lt;refcodes-filesystem&gt; (version 1.10.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-filesystem/src/refcodes-filesystem-1.10.1/pom.xml) 

## Change list &lt;refcodes-filesystem-alt&gt; (version 1.10.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-filesystem-alt/src/refcodes-filesystem-alt-1.10.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-filesystem-alt/src/refcodes-filesystem-alt-1.10.1/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-filesystem-alt/src/refcodes-filesystem-alt-1.10.1/refcodes-filesystem-alt-s3/pom.xml) 

## Change list &lt;refcodes-forwardsecrecy&gt; (version 1.10.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy/src/refcodes-forwardsecrecy-1.10.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy/src/refcodes-forwardsecrecy-1.10.1/README.md) 

## Change list &lt;refcodes-forwardsecrecy-alt&gt; (version 1.10.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-1.10.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-1.10.1/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-1.10.1/refcodes-forwardsecrecy-alt-filesystem/pom.xml) 

## Change list &lt;refcodes-io-ext&gt; (version 1.10.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-1.10.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-1.10.1/refcodes-io-ext-observable/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-1.10.1/refcodes-io-ext-observable/README.md) 

## Change list &lt;refcodes-interceptor&gt; (version 1.10.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-interceptor/src/refcodes-interceptor-1.10.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-interceptor/src/refcodes-interceptor-1.10.1/README.md) 

## Change list &lt;refcodes-jobbus&gt; (version 1.10.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-jobbus/src/refcodes-jobbus-1.10.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-jobbus/src/refcodes-jobbus-1.10.1/README.md) 

## Change list &lt;refcodes-rest-ext&gt; (version 1.10.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-1.10.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-1.10.1/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-1.10.1/refcodes-rest-ext-eureka/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-1.10.1/refcodes-rest-ext-eureka/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`EurekaDiscoverySidecarImpl.java`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-1.10.1/refcodes-rest-ext-eureka/src/main/java/org/refcodes/rest/ext/eureka/EurekaDiscoverySidecarImpl.java) (see Javadoc at [`EurekaDiscoverySidecarImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest-ext-eureka/1.10.1/org/refcodes/rest/ext/eureka/EurekaDiscoverySidecarImpl.html))

## Change list &lt;refcodes-remoting&gt; (version 1.10.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-remoting/src/refcodes-remoting-1.10.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-remoting/src/refcodes-remoting-1.10.1/README.md) 

## Change list &lt;refcodes-remoting-ext&gt; (version 1.10.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-1.10.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-1.10.1/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-1.10.1/refcodes-remoting-ext-observer/pom.xml) 

## Change list &lt;refcodes-servicebus&gt; (version 1.10.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-servicebus/src/refcodes-servicebus-1.10.1/pom.xml) 

## Change list &lt;refcodes-servicebus-alt&gt; (version 1.10.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-servicebus-alt/src/refcodes-servicebus-alt-1.10.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-servicebus-alt/src/refcodes-servicebus-alt-1.10.1/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-servicebus-alt/src/refcodes-servicebus-alt-1.10.1/refcodes-servicebus-alt-spring/pom.xml) 

## Change list &lt;refcodes-tabular-alt&gt; (version 1.10.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-tabular-alt/src/refcodes-tabular-alt-1.10.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-tabular-alt/src/refcodes-tabular-alt-1.10.1/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-tabular-alt/src/refcodes-tabular-alt-1.10.1/refcodes-tabular-alt-forwardsecrecy/pom.xml) 
