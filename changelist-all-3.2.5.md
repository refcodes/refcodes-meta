> This change list has been auto-generated on `triton` by `steiner` with `changelist-all.sh` on the 2023-06-16 at 13:50:55.

## Change list &lt;refcodes-licensing&gt; (version 3.2.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-licensing/src/refcodes-licensing-3.2.5/pom.xml) 

## Change list &lt;refcodes-parent&gt; (version 3.2.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-parent/src/refcodes-parent-3.2.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-parent/src/refcodes-parent-3.2.5/README.md) 

## Change list &lt;refcodes-time&gt; (version 3.2.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-time/src/refcodes-time-3.2.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-time/src/refcodes-time-3.2.5/README.md) 

## Change list &lt;refcodes-mixin&gt; (version 3.2.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-mixin/src/refcodes-mixin-3.2.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-mixin/src/refcodes-mixin-3.2.5/README.md) 

## Change list &lt;refcodes-data&gt; (version 3.2.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data/src/refcodes-data-3.2.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data/src/refcodes-data-3.2.5/README.md) 

## Change list &lt;refcodes-exception&gt; (version 3.2.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-exception/src/refcodes-exception-3.2.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-exception/src/refcodes-exception-3.2.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`MessageDetails.java`](https://bitbucket.org/refcodes/refcodes-exception/src/refcodes-exception-3.2.5/src/main/java/org/refcodes/exception/MessageDetails.java) (see Javadoc at [`MessageDetails.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-exception/3.2.5/org.refcodes.exception/org/refcodes/exception/MessageDetails.html))

## Change list &lt;refcodes-factory&gt; (version 3.2.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-factory/src/refcodes-factory-3.2.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-factory/src/refcodes-factory-3.2.5/README.md) 

## Change list &lt;refcodes-factory-alt&gt; (version 3.2.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-factory-alt/src/refcodes-factory-alt-3.2.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-factory-alt/src/refcodes-factory-alt-3.2.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-factory-alt/src/refcodes-factory-alt-3.2.5/refcodes-factory-alt-spring/pom.xml) 

## Change list &lt;refcodes-controlflow&gt; (version 3.2.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-controlflow/src/refcodes-controlflow-3.2.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-controlflow/src/refcodes-controlflow-3.2.5/README.md) 

## Change list &lt;refcodes-numerical&gt; (version 3.2.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-numerical/src/refcodes-numerical-3.2.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-numerical/src/refcodes-numerical-3.2.5/README.md) 

## Change list &lt;refcodes-generator&gt; (version 3.2.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-generator/src/refcodes-generator-3.2.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-generator/src/refcodes-generator-3.2.5/README.md) 

## Change list &lt;refcodes-matcher&gt; (version 3.2.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-matcher/src/refcodes-matcher-3.2.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-matcher/src/refcodes-matcher-3.2.5/README.md) 

## Change list &lt;refcodes-struct&gt; (version 3.2.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-struct/src/refcodes-struct-3.2.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-struct/src/refcodes-struct-3.2.5/README.md) 

## Change list &lt;refcodes-struct-ext&gt; (version 3.2.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-struct-ext/src/refcodes-struct-ext-3.2.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-struct-ext/src/refcodes-struct-ext-3.2.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-struct-ext/src/refcodes-struct-ext-3.2.5/refcodes-struct-ext-factory/pom.xml) 

## Change list &lt;refcodes-runtime&gt; (version 3.2.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-3.2.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-3.2.5/README.md) 

## Change list &lt;refcodes-data-ext&gt; (version 3.2.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-3.2.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-3.2.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-3.2.5/refcodes-data-ext-checkers/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-3.2.5/refcodes-data-ext-checkers/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-3.2.5/refcodes-data-ext-chess/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-3.2.5/refcodes-data-ext-chess/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-3.2.5/refcodes-data-ext-corporate/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-3.2.5/refcodes-data-ext-corporate/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-3.2.5/refcodes-data-ext-symbols/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-3.2.5/refcodes-data-ext-symbols/README.md) 

## Change list &lt;refcodes-graphical&gt; (version 3.2.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-graphical/src/refcodes-graphical-3.2.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-graphical/src/refcodes-graphical-3.2.5/README.md) 

## Change list &lt;refcodes-criteria&gt; (version 3.2.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-3.2.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-3.2.5/README.md) 

## Change list &lt;refcodes-io&gt; (version 3.2.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-io/src/refcodes-io-3.2.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-io/src/refcodes-io-3.2.5/README.md) 

## Change list &lt;refcodes-tabular&gt; (version 3.2.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-tabular/src/refcodes-tabular-3.2.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-tabular/src/refcodes-tabular-3.2.5/README.md) 

## Change list &lt;refcodes-command&gt; (version 3.2.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-command/src/refcodes-command-3.2.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-command/src/refcodes-command-3.2.5/README.md) 

## Change list &lt;refcodes-cli&gt; (version 3.2.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.2.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.2.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`AbstractCondition.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.2.5/src/main/java/org/refcodes/cli/AbstractCondition.java) (see Javadoc at [`AbstractCondition.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/3.2.5/org.refcodes.cli/org/refcodes/cli/AbstractCondition.html))
* \[<span style="color:green">MODIFIED</span>\] [`AbstractOperand.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.2.5/src/main/java/org/refcodes/cli/AbstractOperand.java) (see Javadoc at [`AbstractOperand.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/3.2.5/org.refcodes.cli/org/refcodes/cli/AbstractOperand.html))
* \[<span style="color:green">MODIFIED</span>\] [`AllCondition.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.2.5/src/main/java/org/refcodes/cli/AllCondition.java) (see Javadoc at [`AllCondition.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/3.2.5/org.refcodes.cli/org/refcodes/cli/AllCondition.html))
* \[<span style="color:green">MODIFIED</span>\] [`AnyCondition.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.2.5/src/main/java/org/refcodes/cli/AnyCondition.java) (see Javadoc at [`AnyCondition.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/3.2.5/org.refcodes.cli/org/refcodes/cli/AnyCondition.html))
* \[<span style="color:green">MODIFIED</span>\] [`OrCondition.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.2.5/src/main/java/org/refcodes/cli/OrCondition.java) (see Javadoc at [`OrCondition.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/3.2.5/org.refcodes.cli/org/refcodes/cli/OrCondition.html))
* \[<span style="color:green">MODIFIED</span>\] [`StackOverflowTest.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.2.5/src/test/java/org/refcodes/cli/StackOverflowTest.java) 

## Change list &lt;refcodes-audio&gt; (version 3.2.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-audio/src/refcodes-audio-3.2.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-audio/src/refcodes-audio-3.2.5/README.md) 

## Change list &lt;refcodes-codec&gt; (version 3.2.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-3.2.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-3.2.5/README.md) 

## Change list &lt;refcodes-component-ext&gt; (version 3.2.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-3.2.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-3.2.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-3.2.5/refcodes-component-ext-observer/pom.xml) 

## Change list &lt;refcodes-properties&gt; (version 3.2.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-3.2.5/pom.xml) 

## Change list &lt;refcodes-security&gt; (version 3.2.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security/src/refcodes-security-3.2.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security/src/refcodes-security-3.2.5/README.md) 

## Change list &lt;refcodes-security-alt&gt; (version 3.2.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-3.2.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-3.2.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-3.2.5/refcodes-security-alt-chaos/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-3.2.5/refcodes-security-alt-chaos/README.md) 

## Change list &lt;refcodes-security-ext&gt; (version 3.2.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-3.2.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-3.2.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-3.2.5/refcodes-security-ext-chaos/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-3.2.5/refcodes-security-ext-chaos/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-3.2.5/refcodes-security-ext-spring/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-3.2.5/refcodes-security-ext-spring/README.md) 

## Change list &lt;refcodes-properties-ext&gt; (version 3.2.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.2.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.2.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.2.5/refcodes-properties-ext-application/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.2.5/refcodes-properties-ext-application/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.2.5/refcodes-properties-ext-cli/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.2.5/refcodes-properties-ext-cli/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.2.5/refcodes-properties-ext-obfuscation/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.2.5/refcodes-properties-ext-obfuscation/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.2.5/refcodes-properties-ext-observer/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.2.5/refcodes-properties-ext-observer/README.md) 

## Change list &lt;refcodes-logger&gt; (version 3.2.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger/src/refcodes-logger-3.2.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger/src/refcodes-logger-3.2.5/README.md) 

## Change list &lt;refcodes-logger-alt&gt; (version 3.2.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.2.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.2.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.2.5/refcodes-logger-alt-async/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.2.5/refcodes-logger-alt-async/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.2.5/refcodes-logger-alt-console/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.2.5/refcodes-logger-alt-console/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.2.5/refcodes-logger-alt-io/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.2.5/refcodes-logger-alt-io/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.2.5/refcodes-logger-alt-simpledb/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.2.5/refcodes-logger-alt-simpledb/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.2.5/refcodes-logger-alt-slf4j-legacy/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.2.5/refcodes-logger-alt-slf4j/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.2.5/refcodes-logger-alt-spring/pom.xml) 

## Change list &lt;refcodes-logger-ext&gt; (version 3.2.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-3.2.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-3.2.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-3.2.5/refcodes-logger-ext-slf4j-legacy/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-3.2.5/refcodes-logger-ext-slf4j-legacy/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-3.2.5/refcodes-logger-ext-slf4j/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-3.2.5/refcodes-logger-ext-slf4j/README.md) 

## Change list &lt;refcodes-graphical-ext&gt; (version 3.2.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-3.2.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-3.2.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-3.2.5/refcodes-graphical-ext-javafx/pom.xml) 

## Change list &lt;refcodes-checkerboard&gt; (version 3.2.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-3.2.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-3.2.5/README.md) 

## Change list &lt;refcodes-checkerboard-alt&gt; (version 3.2.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-alt/src/refcodes-checkerboard-alt-3.2.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-alt/src/refcodes-checkerboard-alt-3.2.5/refcodes-checkerboard-alt-javafx/pom.xml) 

## Change list &lt;refcodes-net&gt; (version 3.2.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-3.2.5/pom.xml) 

## Change list &lt;refcodes-web&gt; (version 3.2.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-web/src/refcodes-web-3.2.5/pom.xml) 

## Change list &lt;refcodes-rest&gt; (version 3.2.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-3.2.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-3.2.5/README.md) 

## Change list &lt;refcodes-hal&gt; (version 3.2.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-3.2.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-3.2.5/README.md) 

## Change list &lt;refcodes-eventbus&gt; (version 3.2.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-eventbus/src/refcodes-eventbus-3.2.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-eventbus/src/refcodes-eventbus-3.2.5/README.md) 

## Change list &lt;refcodes-eventbus-ext&gt; (version 3.2.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-3.2.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-3.2.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-3.2.5/refcodes-eventbus-ext-application/pom.xml) 

## Change list &lt;refcodes-decoupling&gt; (version 3.2.5)

* \[<span style="color:blue">ADDED</span>\] [`.classpath`](https://bitbucket.org/refcodes/refcodes-decoupling/src/refcodes-decoupling-3.2.5/.classpath) 
* \[<span style="color:blue">ADDED</span>\] [`.gitignore`](https://bitbucket.org/refcodes/refcodes-decoupling/src/refcodes-decoupling-3.2.5/.gitignore) 
* \[<span style="color:blue">ADDED</span>\] [`.project`](https://bitbucket.org/refcodes/refcodes-decoupling/src/refcodes-decoupling-3.2.5/.project) 
* \[<span style="color:blue">ADDED</span>\] [`representations.aird`](https://bitbucket.org/refcodes/refcodes-decoupling/src/refcodes-decoupling-3.2.5/representations.aird) 
* \[<span style="color:blue">ADDED</span>\] [`org.eclipse.core.resources.prefs`](https://bitbucket.org/refcodes/refcodes-decoupling/src/refcodes-decoupling-3.2.5/.settings/org.eclipse.core.resources.prefs) 
* \[<span style="color:blue">ADDED</span>\] [`org.eclipse.jdt.core.prefs`](https://bitbucket.org/refcodes/refcodes-decoupling/src/refcodes-decoupling-3.2.5/.settings/org.eclipse.jdt.core.prefs) 
* \[<span style="color:blue">ADDED</span>\] [`org.eclipse.m2e.core.prefs`](https://bitbucket.org/refcodes/refcodes-decoupling/src/refcodes-decoupling-3.2.5/.settings/org.eclipse.m2e.core.prefs) 
* \[<span style="color:blue">ADDED</span>\] [`Alias.java`](https://bitbucket.org/refcodes/refcodes-decoupling/src/refcodes-decoupling-3.2.5/src/main/java/org/refcodes/decoupling/Alias.java) (see Javadoc at [`Alias.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-decoupling/3.2.5/org.refcodes.decoupling/org/refcodes/decoupling/Alias.html))
* \[<span style="color:blue">ADDED</span>\] [`AmbigousClaimException.java`](https://bitbucket.org/refcodes/refcodes-decoupling/src/refcodes-decoupling-3.2.5/src/main/java/org/refcodes/decoupling/AmbigousClaimException.java) (see Javadoc at [`AmbigousClaimException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-decoupling/3.2.5/org.refcodes.decoupling/org/refcodes/decoupling/AmbigousClaimException.html))
* \[<span style="color:blue">ADDED</span>\] [`AmbigousDependencyException.java`](https://bitbucket.org/refcodes/refcodes-decoupling/src/refcodes-decoupling-3.2.5/src/main/java/org/refcodes/decoupling/AmbigousDependencyException.java) (see Javadoc at [`AmbigousDependencyException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-decoupling/3.2.5/org.refcodes.decoupling/org/refcodes/decoupling/AmbigousDependencyException.html))
* \[<span style="color:blue">ADDED</span>\] [`AmbigousFactoryException.java`](https://bitbucket.org/refcodes/refcodes-decoupling/src/refcodes-decoupling-3.2.5/src/main/java/org/refcodes/decoupling/AmbigousFactoryException.java) (see Javadoc at [`AmbigousFactoryException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-decoupling/3.2.5/org.refcodes.decoupling/org/refcodes/decoupling/AmbigousFactoryException.html))
* \[<span style="color:blue">ADDED</span>\] [`AmbigousInitializerException.java`](https://bitbucket.org/refcodes/refcodes-decoupling/src/refcodes-decoupling-3.2.5/src/main/java/org/refcodes/decoupling/AmbigousInitializerException.java) (see Javadoc at [`AmbigousInitializerException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-decoupling/3.2.5/org.refcodes.decoupling/org/refcodes/decoupling/AmbigousInitializerException.html))
* \[<span style="color:blue">ADDED</span>\] [`CircularDependencyException.java`](https://bitbucket.org/refcodes/refcodes-decoupling/src/refcodes-decoupling-3.2.5/src/main/java/org/refcodes/decoupling/CircularDependencyException.java) (see Javadoc at [`CircularDependencyException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-decoupling/3.2.5/org.refcodes.decoupling/org/refcodes/decoupling/CircularDependencyException.html))
* \[<span style="color:blue">ADDED</span>\] [`ClaimAccessor.java`](https://bitbucket.org/refcodes/refcodes-decoupling/src/refcodes-decoupling-3.2.5/src/main/java/org/refcodes/decoupling/ClaimAccessor.java) (see Javadoc at [`ClaimAccessor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-decoupling/3.2.5/org.refcodes.decoupling/org/refcodes/decoupling/ClaimAccessor.html))
* \[<span style="color:blue">ADDED</span>\] [`Claim.java`](https://bitbucket.org/refcodes/refcodes-decoupling/src/refcodes-decoupling-3.2.5/src/main/java/org/refcodes/decoupling/Claim.java) (see Javadoc at [`Claim.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-decoupling/3.2.5/org.refcodes.decoupling/org/refcodes/decoupling/Claim.html))
* \[<span style="color:blue">ADDED</span>\] [`ClaimsAccessor.java`](https://bitbucket.org/refcodes/refcodes-decoupling/src/refcodes-decoupling-3.2.5/src/main/java/org/refcodes/decoupling/ClaimsAccessor.java) (see Javadoc at [`ClaimsAccessor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-decoupling/3.2.5/org.refcodes.decoupling/org/refcodes/decoupling/ClaimsAccessor.html))
* \[<span style="color:blue">ADDED</span>\] [`Context.java`](https://bitbucket.org/refcodes/refcodes-decoupling/src/refcodes-decoupling-3.2.5/src/main/java/org/refcodes/decoupling/Context.java) (see Javadoc at [`Context.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-decoupling/3.2.5/org.refcodes.decoupling/org/refcodes/decoupling/Context.html))
* \[<span style="color:blue">ADDED</span>\] [`DependenciesAccessor.java`](https://bitbucket.org/refcodes/refcodes-decoupling/src/refcodes-decoupling-3.2.5/src/main/java/org/refcodes/decoupling/DependenciesAccessor.java) (see Javadoc at [`DependenciesAccessor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-decoupling/3.2.5/org.refcodes.decoupling/org/refcodes/decoupling/DependenciesAccessor.html))
* \[<span style="color:blue">ADDED</span>\] [`DependencyAccessor.java`](https://bitbucket.org/refcodes/refcodes-decoupling/src/refcodes-decoupling-3.2.5/src/main/java/org/refcodes/decoupling/DependencyAccessor.java) (see Javadoc at [`DependencyAccessor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-decoupling/3.2.5/org.refcodes.decoupling/org/refcodes/decoupling/DependencyAccessor.html))
* \[<span style="color:blue">ADDED</span>\] [`DependencyBuilder.java`](https://bitbucket.org/refcodes/refcodes-decoupling/src/refcodes-decoupling-3.2.5/src/main/java/org/refcodes/decoupling/DependencyBuilder.java) (see Javadoc at [`DependencyBuilder.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-decoupling/3.2.5/org.refcodes.decoupling/org/refcodes/decoupling/DependencyBuilder.html))
* \[<span style="color:blue">ADDED</span>\] [`DependencyException.java`](https://bitbucket.org/refcodes/refcodes-decoupling/src/refcodes-decoupling-3.2.5/src/main/java/org/refcodes/decoupling/DependencyException.java) (see Javadoc at [`DependencyException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-decoupling/3.2.5/org.refcodes.decoupling/org/refcodes/decoupling/DependencyException.html))
* \[<span style="color:blue">ADDED</span>\] [`DependencyInstanciationException.java`](https://bitbucket.org/refcodes/refcodes-decoupling/src/refcodes-decoupling-3.2.5/src/main/java/org/refcodes/decoupling/DependencyInstanciationException.java) (see Javadoc at [`DependencyInstanciationException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-decoupling/3.2.5/org.refcodes.decoupling/org/refcodes/decoupling/DependencyInstanciationException.html))
* \[<span style="color:blue">ADDED</span>\] [`DependencyInterceptor.java`](https://bitbucket.org/refcodes/refcodes-decoupling/src/refcodes-decoupling-3.2.5/src/main/java/org/refcodes/decoupling/DependencyInterceptor.java) (see Javadoc at [`DependencyInterceptor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-decoupling/3.2.5/org.refcodes.decoupling/org/refcodes/decoupling/DependencyInterceptor.html))
* \[<span style="color:blue">ADDED</span>\] [`Dependency.java`](https://bitbucket.org/refcodes/refcodes-decoupling/src/refcodes-decoupling-3.2.5/src/main/java/org/refcodes/decoupling/Dependency.java) (see Javadoc at [`Dependency.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-decoupling/3.2.5/org.refcodes.decoupling/org/refcodes/decoupling/Dependency.html))
* \[<span style="color:blue">ADDED</span>\] [`DependencySchema.java`](https://bitbucket.org/refcodes/refcodes-decoupling/src/refcodes-decoupling-3.2.5/src/main/java/org/refcodes/decoupling/DependencySchema.java) (see Javadoc at [`DependencySchema.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-decoupling/3.2.5/org.refcodes.decoupling/org/refcodes/decoupling/DependencySchema.html))
* \[<span style="color:blue">ADDED</span>\] [`DuplicateClaimException.java`](https://bitbucket.org/refcodes/refcodes-decoupling/src/refcodes-decoupling-3.2.5/src/main/java/org/refcodes/decoupling/DuplicateClaimException.java) (see Javadoc at [`DuplicateClaimException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-decoupling/3.2.5/org.refcodes.decoupling/org/refcodes/decoupling/DuplicateClaimException.html))
* \[<span style="color:blue">ADDED</span>\] [`DuplicateDependencyException.java`](https://bitbucket.org/refcodes/refcodes-decoupling/src/refcodes-decoupling-3.2.5/src/main/java/org/refcodes/decoupling/DuplicateDependencyException.java) (see Javadoc at [`DuplicateDependencyException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-decoupling/3.2.5/org.refcodes.decoupling/org/refcodes/decoupling/DuplicateDependencyException.html))
* \[<span style="color:blue">ADDED</span>\] [`FactoryClaim.java`](https://bitbucket.org/refcodes/refcodes-decoupling/src/refcodes-decoupling-3.2.5/src/main/java/org/refcodes/decoupling/FactoryClaim.java) (see Javadoc at [`FactoryClaim.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-decoupling/3.2.5/org.refcodes.decoupling/org/refcodes/decoupling/FactoryClaim.html))
* \[<span style="color:blue">ADDED</span>\] [`InitializerClaim.java`](https://bitbucket.org/refcodes/refcodes-decoupling/src/refcodes-decoupling-3.2.5/src/main/java/org/refcodes/decoupling/InitializerClaim.java) (see Javadoc at [`InitializerClaim.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-decoupling/3.2.5/org.refcodes.decoupling/org/refcodes/decoupling/InitializerClaim.html))
* \[<span style="color:blue">ADDED</span>\] [`InstallDependencyException.java`](https://bitbucket.org/refcodes/refcodes-decoupling/src/refcodes-decoupling-3.2.5/src/main/java/org/refcodes/decoupling/InstallDependencyException.java) (see Javadoc at [`InstallDependencyException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-decoupling/3.2.5/org.refcodes.decoupling/org/refcodes/decoupling/InstallDependencyException.html))
* \[<span style="color:blue">ADDED</span>\] [`InstanceMetricsAccessor.java`](https://bitbucket.org/refcodes/refcodes-decoupling/src/refcodes-decoupling-3.2.5/src/main/java/org/refcodes/decoupling/InstanceMetricsAccessor.java) (see Javadoc at [`InstanceMetricsAccessor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-decoupling/3.2.5/org.refcodes.decoupling/org/refcodes/decoupling/InstanceMetricsAccessor.html))
* \[<span style="color:blue">ADDED</span>\] [`InstanceMetrics.java`](https://bitbucket.org/refcodes/refcodes-decoupling/src/refcodes-decoupling-3.2.5/src/main/java/org/refcodes/decoupling/InstanceMetrics.java) (see Javadoc at [`InstanceMetrics.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-decoupling/3.2.5/org.refcodes.decoupling/org/refcodes/decoupling/InstanceMetrics.html))
* \[<span style="color:blue">ADDED</span>\] [`InstanceMode.java`](https://bitbucket.org/refcodes/refcodes-decoupling/src/refcodes-decoupling-3.2.5/src/main/java/org/refcodes/decoupling/InstanceMode.java) (see Javadoc at [`InstanceMode.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-decoupling/3.2.5/org.refcodes.decoupling/org/refcodes/decoupling/InstanceMode.html))
* \[<span style="color:blue">ADDED</span>\] [`ProfilesAccessor.java`](https://bitbucket.org/refcodes/refcodes-decoupling/src/refcodes-decoupling-3.2.5/src/main/java/org/refcodes/decoupling/ProfilesAccessor.java) (see Javadoc at [`ProfilesAccessor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-decoupling/3.2.5/org.refcodes.decoupling/org/refcodes/decoupling/ProfilesAccessor.html))
* \[<span style="color:blue">ADDED</span>\] [`Reactor.java`](https://bitbucket.org/refcodes/refcodes-decoupling/src/refcodes-decoupling-3.2.5/src/main/java/org/refcodes/decoupling/Reactor.java) (see Javadoc at [`Reactor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-decoupling/3.2.5/org.refcodes.decoupling/org/refcodes/decoupling/Reactor.html))
* \[<span style="color:blue">ADDED</span>\] [`TagsAccessor.java`](https://bitbucket.org/refcodes/refcodes-decoupling/src/refcodes-decoupling-3.2.5/src/main/java/org/refcodes/decoupling/TagsAccessor.java) (see Javadoc at [`TagsAccessor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-decoupling/3.2.5/org.refcodes.decoupling/org/refcodes/decoupling/TagsAccessor.html))
* \[<span style="color:blue">ADDED</span>\] [`UnsatisfiedClaimException.java`](https://bitbucket.org/refcodes/refcodes-decoupling/src/refcodes-decoupling-3.2.5/src/main/java/org/refcodes/decoupling/UnsatisfiedClaimException.java) (see Javadoc at [`UnsatisfiedClaimException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-decoupling/3.2.5/org.refcodes.decoupling/org/refcodes/decoupling/UnsatisfiedClaimException.html))
* \[<span style="color:blue">ADDED</span>\] [`UnsatisfiedDependencyException.java`](https://bitbucket.org/refcodes/refcodes-decoupling/src/refcodes-decoupling-3.2.5/src/main/java/org/refcodes/decoupling/UnsatisfiedDependencyException.java) (see Javadoc at [`UnsatisfiedDependencyException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-decoupling/3.2.5/org.refcodes.decoupling/org/refcodes/decoupling/UnsatisfiedDependencyException.html))
* \[<span style="color:blue">ADDED</span>\] [`UnsatisfiedFactoryException.java`](https://bitbucket.org/refcodes/refcodes-decoupling/src/refcodes-decoupling-3.2.5/src/main/java/org/refcodes/decoupling/UnsatisfiedFactoryException.java) (see Javadoc at [`UnsatisfiedFactoryException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-decoupling/3.2.5/org.refcodes.decoupling/org/refcodes/decoupling/UnsatisfiedFactoryException.html))
* \[<span style="color:blue">ADDED</span>\] [`UnsatisfiedInitializerException.java`](https://bitbucket.org/refcodes/refcodes-decoupling/src/refcodes-decoupling-3.2.5/src/main/java/org/refcodes/decoupling/UnsatisfiedInitializerException.java) (see Javadoc at [`UnsatisfiedInitializerException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-decoupling/3.2.5/org.refcodes.decoupling/org/refcodes/decoupling/UnsatisfiedInitializerException.html))
* \[<span style="color:blue">ADDED</span>\] [`ClaimTest.java`](https://bitbucket.org/refcodes/refcodes-decoupling/src/refcodes-decoupling-3.2.5/src/test/java/org/refcodes/decoupling/ClaimTest.java) 
* \[<span style="color:blue">ADDED</span>\] [`ComponentA1.java`](https://bitbucket.org/refcodes/refcodes-decoupling/src/refcodes-decoupling-3.2.5/src/test/java/org/refcodes/decoupling/ComponentA1.java) 
* \[<span style="color:blue">ADDED</span>\] [`ComponentA2.java`](https://bitbucket.org/refcodes/refcodes-decoupling/src/refcodes-decoupling-3.2.5/src/test/java/org/refcodes/decoupling/ComponentA2.java) 
* \[<span style="color:blue">ADDED</span>\] [`ComponentA.java`](https://bitbucket.org/refcodes/refcodes-decoupling/src/refcodes-decoupling-3.2.5/src/test/java/org/refcodes/decoupling/ComponentA.java) 
* \[<span style="color:blue">ADDED</span>\] [`ComponentB1.java`](https://bitbucket.org/refcodes/refcodes-decoupling/src/refcodes-decoupling-3.2.5/src/test/java/org/refcodes/decoupling/ComponentB1.java) 
* \[<span style="color:blue">ADDED</span>\] [`ComponentB2.java`](https://bitbucket.org/refcodes/refcodes-decoupling/src/refcodes-decoupling-3.2.5/src/test/java/org/refcodes/decoupling/ComponentB2.java) 
* \[<span style="color:blue">ADDED</span>\] [`ComponentB.java`](https://bitbucket.org/refcodes/refcodes-decoupling/src/refcodes-decoupling-3.2.5/src/test/java/org/refcodes/decoupling/ComponentB.java) 
* \[<span style="color:blue">ADDED</span>\] [`ComponentExtendsA2.java`](https://bitbucket.org/refcodes/refcodes-decoupling/src/refcodes-decoupling-3.2.5/src/test/java/org/refcodes/decoupling/ComponentExtendsA2.java) 
* \[<span style="color:blue">ADDED</span>\] [`ComponentQ1.java`](https://bitbucket.org/refcodes/refcodes-decoupling/src/refcodes-decoupling-3.2.5/src/test/java/org/refcodes/decoupling/ComponentQ1.java) 
* \[<span style="color:blue">ADDED</span>\] [`ComponentQ2.java`](https://bitbucket.org/refcodes/refcodes-decoupling/src/refcodes-decoupling-3.2.5/src/test/java/org/refcodes/decoupling/ComponentQ2.java) 
* \[<span style="color:blue">ADDED</span>\] [`ComponentQ.java`](https://bitbucket.org/refcodes/refcodes-decoupling/src/refcodes-decoupling-3.2.5/src/test/java/org/refcodes/decoupling/ComponentQ.java) 
* \[<span style="color:blue">ADDED</span>\] [`ComponentX.java`](https://bitbucket.org/refcodes/refcodes-decoupling/src/refcodes-decoupling-3.2.5/src/test/java/org/refcodes/decoupling/ComponentX.java) 
* \[<span style="color:blue">ADDED</span>\] [`ComponentY.java`](https://bitbucket.org/refcodes/refcodes-decoupling/src/refcodes-decoupling-3.2.5/src/test/java/org/refcodes/decoupling/ComponentY.java) 
* \[<span style="color:blue">ADDED</span>\] [`ContextTest.java`](https://bitbucket.org/refcodes/refcodes-decoupling/src/refcodes-decoupling-3.2.5/src/test/java/org/refcodes/decoupling/ContextTest.java) 
* \[<span style="color:blue">ADDED</span>\] [`FactoryTest.java`](https://bitbucket.org/refcodes/refcodes-decoupling/src/refcodes-decoupling-3.2.5/src/test/java/org/refcodes/decoupling/FactoryTest.java) 
* \[<span style="color:blue">ADDED</span>\] [`From1To2.java`](https://bitbucket.org/refcodes/refcodes-decoupling/src/refcodes-decoupling-3.2.5/src/test/java/org/refcodes/decoupling/From1To2.java) 
* \[<span style="color:blue">ADDED</span>\] [`From1To2To3.java`](https://bitbucket.org/refcodes/refcodes-decoupling/src/refcodes-decoupling-3.2.5/src/test/java/org/refcodes/decoupling/From1To2To3.java) 
* \[<span style="color:blue">ADDED</span>\] [`From2To1.java`](https://bitbucket.org/refcodes/refcodes-decoupling/src/refcodes-decoupling-3.2.5/src/test/java/org/refcodes/decoupling/From2To1.java) 
* \[<span style="color:blue">ADDED</span>\] [`From2To3To1.java`](https://bitbucket.org/refcodes/refcodes-decoupling/src/refcodes-decoupling-3.2.5/src/test/java/org/refcodes/decoupling/From2To3To1.java) 
* \[<span style="color:blue">ADDED</span>\] [`From3To1To2.java`](https://bitbucket.org/refcodes/refcodes-decoupling/src/refcodes-decoupling-3.2.5/src/test/java/org/refcodes/decoupling/From3To1To2.java) 
* \[<span style="color:blue">ADDED</span>\] [`InitializerTest.java`](https://bitbucket.org/refcodes/refcodes-decoupling/src/refcodes-decoupling-3.2.5/src/test/java/org/refcodes/decoupling/InitializerTest.java) 
* \[<span style="color:blue">ADDED</span>\] [`InterceptorTest.java`](https://bitbucket.org/refcodes/refcodes-decoupling/src/refcodes-decoupling-3.2.5/src/test/java/org/refcodes/decoupling/InterceptorTest.java) 
* \[<span style="color:blue">ADDED</span>\] [`QFactory.java`](https://bitbucket.org/refcodes/refcodes-decoupling/src/refcodes-decoupling-3.2.5/src/test/java/org/refcodes/decoupling/QFactory.java) 
* \[<span style="color:blue">ADDED</span>\] [`ReactorTest.java`](https://bitbucket.org/refcodes/refcodes-decoupling/src/refcodes-decoupling-3.2.5/src/test/java/org/refcodes/decoupling/ReactorTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-decoupling/src/refcodes-decoupling-3.2.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-decoupling/src/refcodes-decoupling-3.2.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`module-info.java`](https://bitbucket.org/refcodes/refcodes-decoupling/src/refcodes-decoupling-3.2.5/src/main/java/module-info.java) (see Javadoc at [`module-info.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-decoupling/3.2.5/./module-info.html))
* \[<span style="color:green">MODIFIED</span>\] [`package-info.java`](https://bitbucket.org/refcodes/refcodes-decoupling/src/refcodes-decoupling-3.2.5/src/main/java/org/refcodes/decoupling/package-info.java) 
* \[<span style="color:green">MODIFIED</span>\] [`createdFiles.lst`](https://bitbucket.org/refcodes/refcodes-decoupling/src/refcodes-decoupling-3.2.5/target/maven-status/maven-compiler-plugin/compile/default-compile/createdFiles.lst) 
* \[<span style="color:green">MODIFIED</span>\] [`inputFiles.lst`](https://bitbucket.org/refcodes/refcodes-decoupling/src/refcodes-decoupling-3.2.5/target/maven-status/maven-compiler-plugin/compile/default-compile/inputFiles.lst) 

## Change list &lt;refcodes-decoupling-ext&gt; (version 3.2.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-decoupling-ext/src/refcodes-decoupling-ext-3.2.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-decoupling-ext/src/refcodes-decoupling-ext-3.2.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-decoupling-ext/src/refcodes-decoupling-ext-3.2.5/refcodes-decoupling-ext-application/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-decoupling-ext/src/refcodes-decoupling-ext-3.2.5/refcodes-decoupling-ext-application/README.md) 

## Change list &lt;refcodes-filesystem&gt; (version 3.2.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-filesystem/src/refcodes-filesystem-3.2.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-filesystem/src/refcodes-filesystem-3.2.5/README.md) 

## Change list &lt;refcodes-filesystem-alt&gt; (version 3.2.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-filesystem-alt/src/refcodes-filesystem-alt-3.2.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-filesystem-alt/src/refcodes-filesystem-alt-3.2.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-filesystem-alt/src/refcodes-filesystem-alt-3.2.5/refcodes-filesystem-alt-s3/pom.xml) 

## Change list &lt;refcodes-forwardsecrecy&gt; (version 3.2.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy/src/refcodes-forwardsecrecy-3.2.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy/src/refcodes-forwardsecrecy-3.2.5/README.md) 

## Change list &lt;refcodes-forwardsecrecy-alt&gt; (version 3.2.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-3.2.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-3.2.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-3.2.5/refcodes-forwardsecrecy-alt-filesystem/pom.xml) 

## Change list &lt;refcodes-io-ext&gt; (version 3.2.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-3.2.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-3.2.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-3.2.5/refcodes-io-ext-observer/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-3.2.5/refcodes-io-ext-observer/README.md) 

## Change list &lt;refcodes-jobbus&gt; (version 3.2.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-jobbus/src/refcodes-jobbus-3.2.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-jobbus/src/refcodes-jobbus-3.2.5/README.md) 

## Change list &lt;refcodes-rest-ext&gt; (version 3.2.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-3.2.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-3.2.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-3.2.5/refcodes-rest-ext-eureka/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-3.2.5/refcodes-rest-ext-eureka/README.md) 

## Change list &lt;refcodes-remoting&gt; (version 3.2.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-remoting/src/refcodes-remoting-3.2.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-remoting/src/refcodes-remoting-3.2.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`RemoteServer.java`](https://bitbucket.org/refcodes/refcodes-remoting/src/refcodes-remoting-3.2.5/src/main/java/org/refcodes/remoting/RemoteServer.java) (see Javadoc at [`RemoteServer.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-remoting/3.2.5/org.refcodes.remoting/org/refcodes/remoting/RemoteServer.html))

## Change list &lt;refcodes-remoting-ext&gt; (version 3.2.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-3.2.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-3.2.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-3.2.5/refcodes-remoting-ext-observer/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-3.2.5/refcodes-remoting-ext-observer/README.md) 

## Change list &lt;refcodes-serial&gt; (version 3.2.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial/src/refcodes-serial-3.2.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-serial/src/refcodes-serial-3.2.5/README.md) 

## Change list &lt;refcodes-serial-alt&gt; (version 3.2.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial-alt/src/refcodes-serial-alt-3.2.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-serial-alt/src/refcodes-serial-alt-3.2.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial-alt/src/refcodes-serial-alt-3.2.5/refcodes-serial-alt-net/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-serial-alt/src/refcodes-serial-alt-3.2.5/refcodes-serial-alt-net/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial-alt/src/refcodes-serial-alt-3.2.5/refcodes-serial-alt-tty/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-serial-alt/src/refcodes-serial-alt-3.2.5/refcodes-serial-alt-tty/README.md) 

## Change list &lt;refcodes-serial-ext&gt; (version 3.2.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-3.2.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-3.2.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-3.2.5/refcodes-serial-ext-handshake/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-3.2.5/refcodes-serial-ext-handshake/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-3.2.5/refcodes-serial-ext-observer/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-3.2.5/refcodes-serial-ext-observer/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-3.2.5/refcodes-serial-ext-security/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-3.2.5/refcodes-serial-ext-security/README.md) 

## Change list &lt;refcodes-p2p&gt; (version 3.2.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-p2p/src/refcodes-p2p-3.2.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-p2p/src/refcodes-p2p-3.2.5/README.md) 

## Change list &lt;refcodes-p2p-alt&gt; (version 3.2.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-p2p-alt/src/refcodes-p2p-alt-3.2.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-p2p-alt/src/refcodes-p2p-alt-3.2.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-p2p-alt/src/refcodes-p2p-alt-3.2.5/refcodes-p2p-alt-rest/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-p2p-alt/src/refcodes-p2p-alt-3.2.5/refcodes-p2p-alt-serial/pom.xml) 

## Change list &lt;refcodes-p2p-ext&gt; (version 3.2.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-p2p-ext/src/refcodes-p2p-ext-3.2.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-p2p-ext/src/refcodes-p2p-ext-3.2.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-p2p-ext/src/refcodes-p2p-ext-3.2.5/refcodes-p2p-ext-observer/pom.xml) 

## Change list &lt;refcodes-tabular-alt&gt; (version 3.2.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-tabular-alt/src/refcodes-tabular-alt-3.2.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-tabular-alt/src/refcodes-tabular-alt-3.2.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-tabular-alt/src/refcodes-tabular-alt-3.2.5/refcodes-tabular-alt-forwardsecrecy/pom.xml) 

## Change list &lt;refcodes-archetype&gt; (version 3.2.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype/src/refcodes-archetype-3.2.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype/src/refcodes-archetype-3.2.5/README.md) 

## Change list &lt;refcodes-archetype-alt&gt; (version 3.2.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.2.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.2.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.2.5/refcodes-archetype-alt-c2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.2.5/refcodes-archetype-alt-c2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.2.5/refcodes-archetype-alt-cli/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.2.5/refcodes-archetype-alt-cli/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.2.5/refcodes-archetype-alt-csv/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.2.5/refcodes-archetype-alt-csv/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.2.5/refcodes-archetype-alt-decoupling/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.2.5/refcodes-archetype-alt-decoupling/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.2.5/refcodes-archetype-alt-eventbus/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.2.5/refcodes-archetype-alt-eventbus/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.2.5/refcodes-archetype-alt-filter/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.2.5/refcodes-archetype-alt-filter/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.2.5/refcodes-archetype-alt-rest/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.2.5/refcodes-archetype-alt-rest/README.md) 
