> This change list has been auto-generated on `triton` by `steiner` with `changelist-all.sh` on the 2023-09-17 at 17:49:48.

## Change list &lt;refcodes-licensing&gt; (version 3.3.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-licensing/src/refcodes-licensing-3.3.1/pom.xml) 

## Change list &lt;refcodes-parent&gt; (version 3.3.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-parent/src/refcodes-parent-3.3.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-parent/src/refcodes-parent-3.3.1/README.md) 

## Change list &lt;refcodes-time&gt; (version 3.3.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-time/src/refcodes-time-3.3.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-time/src/refcodes-time-3.3.1/README.md) 

## Change list &lt;refcodes-mixin&gt; (version 3.3.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-mixin/src/refcodes-mixin-3.3.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-mixin/src/refcodes-mixin-3.3.1/README.md) 

## Change list &lt;refcodes-exception&gt; (version 3.3.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-exception/src/refcodes-exception-3.3.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-exception/src/refcodes-exception-3.3.1/README.md) 

## Change list &lt;refcodes-factory&gt; (version 3.3.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-factory/src/refcodes-factory-3.3.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-factory/src/refcodes-factory-3.3.1/README.md) 

## Change list &lt;refcodes-factory-alt&gt; (version 3.3.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-factory-alt/src/refcodes-factory-alt-3.3.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-factory-alt/src/refcodes-factory-alt-3.3.1/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-factory-alt/src/refcodes-factory-alt-3.3.1/refcodes-factory-alt-spring/pom.xml) 

## Change list &lt;refcodes-controlflow&gt; (version 3.3.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-controlflow/src/refcodes-controlflow-3.3.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-controlflow/src/refcodes-controlflow-3.3.1/README.md) 

## Change list &lt;refcodes-numerical&gt; (version 3.3.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-numerical/src/refcodes-numerical-3.3.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-numerical/src/refcodes-numerical-3.3.1/README.md) 

## Change list &lt;refcodes-generator&gt; (version 3.3.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-generator/src/refcodes-generator-3.3.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-generator/src/refcodes-generator-3.3.1/README.md) 

## Change list &lt;refcodes-matcher&gt; (version 3.3.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-matcher/src/refcodes-matcher-3.3.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-matcher/src/refcodes-matcher-3.3.1/README.md) 

## Change list &lt;refcodes-struct&gt; (version 3.3.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-struct/src/refcodes-struct-3.3.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-struct/src/refcodes-struct-3.3.1/README.md) 

## Change list &lt;refcodes-runtime&gt; (version 3.3.1)

* \[<span style="color:blue">ADDED</span>\] [`AnsiWrapper.java`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-3.3.1/src/main/java/org/refcodes/runtime/AnsiWrapper.java) (see Javadoc at [`AnsiWrapper.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-runtime/3.3.1/org.refcodes.runtime/org/refcodes/runtime/AnsiWrapper.html))
* \[<span style="color:blue">ADDED</span>\] [`AnsiWrapperTest.java`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-3.3.1/src/test/java/org/refcodes/runtime/AnsiWrapperTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-3.3.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-3.3.1/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`Execution.java`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-3.3.1/src/main/java/org/refcodes/runtime/Execution.java) (see Javadoc at [`Execution.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-runtime/3.3.1/org.refcodes.runtime/org/refcodes/runtime/Execution.html))
* \[<span style="color:green">MODIFIED</span>\] [`Terminal.java`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-3.3.1/src/main/java/org/refcodes/runtime/Terminal.java) (see Javadoc at [`Terminal.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-runtime/3.3.1/org.refcodes.runtime/org/refcodes/runtime/Terminal.html))
* \[<span style="color:green">MODIFIED</span>\] [`TerminalTest.java`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-3.3.1/src/test/java/org/refcodes/runtime/TerminalTest.java) 

## Change list &lt;refcodes-struct-ext&gt; (version 3.3.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-struct-ext/src/refcodes-struct-ext-3.3.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-struct-ext/src/refcodes-struct-ext-3.3.1/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-struct-ext/src/refcodes-struct-ext-3.3.1/refcodes-struct-ext-factory/pom.xml) 

## Change list &lt;refcodes-component&gt; (version 3.3.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.3.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.3.1/README.md) 

## Change list &lt;refcodes-data-ext&gt; (version 3.3.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-3.3.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-3.3.1/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-3.3.1/refcodes-data-ext-checkers/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-3.3.1/refcodes-data-ext-checkers/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-3.3.1/refcodes-data-ext-chess/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-3.3.1/refcodes-data-ext-chess/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-3.3.1/refcodes-data-ext-corporate/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-3.3.1/refcodes-data-ext-corporate/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-3.3.1/refcodes-data-ext-symbols/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-3.3.1/refcodes-data-ext-symbols/README.md) 

## Change list &lt;refcodes-textual&gt; (version 3.3.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-3.3.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-3.3.1/README.md) 

## Change list &lt;refcodes-criteria&gt; (version 3.3.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-3.3.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-3.3.1/README.md) 

## Change list &lt;refcodes-io&gt; (version 3.3.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-io/src/refcodes-io-3.3.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-io/src/refcodes-io-3.3.1/README.md) 

## Change list &lt;refcodes-tabular&gt; (version 3.3.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-tabular/src/refcodes-tabular-3.3.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-tabular/src/refcodes-tabular-3.3.1/README.md) 

## Change list &lt;refcodes-observer&gt; (version 3.3.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-observer/src/refcodes-observer-3.3.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-observer/src/refcodes-observer-3.3.1/README.md) 

## Change list &lt;refcodes-command&gt; (version 3.3.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-command/src/refcodes-command-3.3.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-command/src/refcodes-command-3.3.1/README.md) 

## Change list &lt;refcodes-cli&gt; (version 3.3.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.3.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.3.1/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`module-info.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.3.1/src/main/java/module-info.java) (see Javadoc at [`module-info.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/3.3.1/./module-info.html))
* \[<span style="color:green">MODIFIED</span>\] [`NoneOperand.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.3.1/src/main/java/org/refcodes/cli/NoneOperand.java) (see Javadoc at [`NoneOperand.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/3.3.1/org.refcodes.cli/org/refcodes/cli/NoneOperand.html))

## Change list &lt;refcodes-audio&gt; (version 3.3.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-audio/src/refcodes-audio-3.3.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-audio/src/refcodes-audio-3.3.1/README.md) 

## Change list &lt;refcodes-codec&gt; (version 3.3.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-3.3.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-3.3.1/README.md) 

## Change list &lt;refcodes-component-ext&gt; (version 3.3.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-3.3.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-3.3.1/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-3.3.1/refcodes-component-ext-observer/pom.xml) 

## Change list &lt;refcodes-properties&gt; (version 3.3.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-3.3.1/pom.xml) 

## Change list &lt;refcodes-security&gt; (version 3.3.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security/src/refcodes-security-3.3.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security/src/refcodes-security-3.3.1/README.md) 

## Change list &lt;refcodes-security-alt&gt; (version 3.3.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-3.3.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-3.3.1/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-3.3.1/refcodes-security-alt-chaos/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-3.3.1/refcodes-security-alt-chaos/README.md) 

## Change list &lt;refcodes-security-ext&gt; (version 3.3.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-3.3.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-3.3.1/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-3.3.1/refcodes-security-ext-chaos/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-3.3.1/refcodes-security-ext-chaos/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-3.3.1/refcodes-security-ext-spring/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-3.3.1/refcodes-security-ext-spring/README.md) 

## Change list &lt;refcodes-logger&gt; (version 3.3.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger/src/refcodes-logger-3.3.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger/src/refcodes-logger-3.3.1/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`RuntimeLoggerFactoryImpl.java`](https://bitbucket.org/refcodes/refcodes-logger/src/refcodes-logger-3.3.1/src/main/java/org/refcodes/logger/RuntimeLoggerFactoryImpl.java) (see Javadoc at [`RuntimeLoggerFactoryImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-logger/3.3.1/org.refcodes.logger/org/refcodes/logger/RuntimeLoggerFactoryImpl.html))

## Change list &lt;refcodes-logger-alt&gt; (version 3.3.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.3.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.3.1/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.3.1/refcodes-logger-alt-async/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.3.1/refcodes-logger-alt-async/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.3.1/refcodes-logger-alt-console/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.3.1/refcodes-logger-alt-console/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`module-info.java`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.3.1/refcodes-logger-alt-console/src/main/java/module-info.java) (see Javadoc at [`module-info.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-logger-alt-console/3.3.1//module-info.html))
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.3.1/refcodes-logger-alt-io/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.3.1/refcodes-logger-alt-io/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.3.1/refcodes-logger-alt-simpledb/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.3.1/refcodes-logger-alt-simpledb/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.3.1/refcodes-logger-alt-slf4j-legacy/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.3.1/refcodes-logger-alt-slf4j/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.3.1/refcodes-logger-alt-spring/pom.xml) 

## Change list &lt;refcodes-logger-ext&gt; (version 3.3.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-3.3.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-3.3.1/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-3.3.1/refcodes-logger-ext-slf4j-legacy/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-3.3.1/refcodes-logger-ext-slf4j-legacy/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-3.3.1/refcodes-logger-ext-slf4j/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-3.3.1/refcodes-logger-ext-slf4j/README.md) 

## Change list &lt;refcodes-graphical-ext&gt; (version 3.3.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-3.3.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-3.3.1/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-3.3.1/refcodes-graphical-ext-javafx/pom.xml) 

## Change list &lt;refcodes-checkerboard&gt; (version 3.3.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-3.3.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-3.3.1/README.md) 

## Change list &lt;refcodes-checkerboard-alt&gt; (version 3.3.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-alt/src/refcodes-checkerboard-alt-3.3.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-alt/src/refcodes-checkerboard-alt-3.3.1/refcodes-checkerboard-alt-javafx/pom.xml) 

## Change list &lt;refcodes-net&gt; (version 3.3.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-3.3.1/pom.xml) 

## Change list &lt;refcodes-web&gt; (version 3.3.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-web/src/refcodes-web-3.3.1/pom.xml) 

## Change list &lt;refcodes-rest&gt; (version 3.3.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-3.3.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-3.3.1/README.md) 

## Change list &lt;refcodes-hal&gt; (version 3.3.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-3.3.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-3.3.1/README.md) 

## Change list &lt;refcodes-eventbus&gt; (version 3.3.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-eventbus/src/refcodes-eventbus-3.3.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-eventbus/src/refcodes-eventbus-3.3.1/README.md) 

## Change list &lt;refcodes-eventbus-ext&gt; (version 3.3.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-3.3.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-3.3.1/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-3.3.1/refcodes-eventbus-ext-application/pom.xml) 

## Change list &lt;refcodes-decoupling&gt; (version 3.3.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-decoupling/src/refcodes-decoupling-3.3.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-decoupling/src/refcodes-decoupling-3.3.1/README.md) 

## Change list &lt;refcodes-decoupling-ext&gt; (version 3.3.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-decoupling-ext/src/refcodes-decoupling-ext-3.3.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-decoupling-ext/src/refcodes-decoupling-ext-3.3.1/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-decoupling-ext/src/refcodes-decoupling-ext-3.3.1/refcodes-decoupling-ext-application/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-decoupling-ext/src/refcodes-decoupling-ext-3.3.1/refcodes-decoupling-ext-application/README.md) 

## Change list &lt;refcodes-filesystem&gt; (version 3.3.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-filesystem/src/refcodes-filesystem-3.3.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-filesystem/src/refcodes-filesystem-3.3.1/README.md) 

## Change list &lt;refcodes-filesystem-alt&gt; (version 3.3.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-filesystem-alt/src/refcodes-filesystem-alt-3.3.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-filesystem-alt/src/refcodes-filesystem-alt-3.3.1/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-filesystem-alt/src/refcodes-filesystem-alt-3.3.1/refcodes-filesystem-alt-s3/pom.xml) 

## Change list &lt;refcodes-forwardsecrecy&gt; (version 3.3.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy/src/refcodes-forwardsecrecy-3.3.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy/src/refcodes-forwardsecrecy-3.3.1/README.md) 

## Change list &lt;refcodes-forwardsecrecy-alt&gt; (version 3.3.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-3.3.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-3.3.1/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-3.3.1/refcodes-forwardsecrecy-alt-filesystem/pom.xml) 

## Change list &lt;refcodes-io-ext&gt; (version 3.3.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-3.3.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-3.3.1/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-3.3.1/refcodes-io-ext-observer/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-3.3.1/refcodes-io-ext-observer/README.md) 

## Change list &lt;refcodes-jobbus&gt; (version 3.3.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-jobbus/src/refcodes-jobbus-3.3.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-jobbus/src/refcodes-jobbus-3.3.1/README.md) 

## Change list &lt;refcodes-rest-ext&gt; (version 3.3.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-3.3.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-3.3.1/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-3.3.1/refcodes-rest-ext-eureka/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-3.3.1/refcodes-rest-ext-eureka/README.md) 

## Change list &lt;refcodes-remoting&gt; (version 3.3.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-remoting/src/refcodes-remoting-3.3.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-remoting/src/refcodes-remoting-3.3.1/README.md) 

## Change list &lt;refcodes-serial&gt; (version 3.3.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial/src/refcodes-serial-3.3.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-serial/src/refcodes-serial-3.3.1/README.md) 

## Change list &lt;refcodes-serial-alt&gt; (version 3.3.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial-alt/src/refcodes-serial-alt-3.3.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-serial-alt/src/refcodes-serial-alt-3.3.1/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial-alt/src/refcodes-serial-alt-3.3.1/refcodes-serial-alt-tty/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-serial-alt/src/refcodes-serial-alt-3.3.1/refcodes-serial-alt-tty/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`TtyPortTestBench.java`](https://bitbucket.org/refcodes/refcodes-serial-alt/src/refcodes-serial-alt-3.3.1/refcodes-serial-alt-tty/src/main/java/org/refcodes/serial/alt/tty/TtyPortTestBench.java) (see Javadoc at [`TtyPortTestBench.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-serial-alt-tty/3.3.1/org.refcodes.serial.alt.tty/org/refcodes/serial/alt/tty/TtyPortTestBench.html))

## Change list &lt;refcodes-serial-ext&gt; (version 3.3.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-3.3.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-3.3.1/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-3.3.1/refcodes-serial-ext-handshake/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-3.3.1/refcodes-serial-ext-handshake/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-3.3.1/refcodes-serial-ext-observer/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-3.3.1/refcodes-serial-ext-observer/README.md) 

## Change list &lt;refcodes-p2p&gt; (version 3.3.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-p2p/src/refcodes-p2p-3.3.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-p2p/src/refcodes-p2p-3.3.1/README.md) 

## Change list &lt;refcodes-p2p-alt&gt; (version 3.3.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-p2p-alt/src/refcodes-p2p-alt-3.3.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-p2p-alt/src/refcodes-p2p-alt-3.3.1/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-p2p-alt/src/refcodes-p2p-alt-3.3.1/refcodes-p2p-alt-serial/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`AbstractTtyPortTest.java`](https://bitbucket.org/refcodes/refcodes-p2p-alt/src/refcodes-p2p-alt-3.3.1/refcodes-p2p-alt-serial/src/test/java/org/refcodes/p2p/alt/serial/AbstractTtyPortTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`SerialPeerTest.java`](https://bitbucket.org/refcodes/refcodes-p2p-alt/src/refcodes-p2p-alt-3.3.1/refcodes-p2p-alt-serial/src/test/java/org/refcodes/p2p/alt/serial/SerialPeerTest.java) 

## Change list &lt;refcodes-p2p-ext&gt; (version 3.3.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-p2p-ext/src/refcodes-p2p-ext-3.3.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-p2p-ext/src/refcodes-p2p-ext-3.3.1/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-p2p-ext/src/refcodes-p2p-ext-3.3.1/refcodes-p2p-ext-observer/pom.xml) 

## Change list &lt;refcodes-tabular-alt&gt; (version 3.3.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-tabular-alt/src/refcodes-tabular-alt-3.3.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-tabular-alt/src/refcodes-tabular-alt-3.3.1/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-tabular-alt/src/refcodes-tabular-alt-3.3.1/refcodes-tabular-alt-forwardsecrecy/pom.xml) 

## Change list &lt;refcodes-archetype&gt; (version 3.3.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype/src/refcodes-archetype-3.3.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype/src/refcodes-archetype-3.3.1/README.md) 

## Change list &lt;refcodes-archetype-alt&gt; (version 3.3.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.1/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.1/refcodes-archetype-alt-c2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.1/refcodes-archetype-alt-c2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.1/refcodes-archetype-alt-c2/src/main/resources/archetype-resources/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`reflect-config.json`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.1/refcodes-archetype-alt-c2/src/main/resources/archetype-resources/src/main/resources/META-INF/native-image/__groupId__/__artifactId__/reflect-config.json) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.1/refcodes-archetype-alt-cli/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.1/refcodes-archetype-alt-cli/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.1/refcodes-archetype-alt-cli/src/main/resources/archetype-resources/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`reflect-config.json`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.1/refcodes-archetype-alt-cli/src/main/resources/archetype-resources/src/main/resources/META-INF/native-image/__groupId__/__artifactId__/reflect-config.json) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.1/refcodes-archetype-alt-csv/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.1/refcodes-archetype-alt-csv/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.1/refcodes-archetype-alt-csv/src/main/resources/archetype-resources/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`reflect-config.json`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.1/refcodes-archetype-alt-csv/src/main/resources/archetype-resources/src/main/resources/META-INF/native-image/__groupId__/__artifactId__/reflect-config.json) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.1/refcodes-archetype-alt-decoupling/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.1/refcodes-archetype-alt-decoupling/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.1/refcodes-archetype-alt-decoupling/src/main/resources/archetype-resources/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`reflect-config.json`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.1/refcodes-archetype-alt-decoupling/src/main/resources/archetype-resources/src/main/resources/META-INF/native-image/__groupId__/__artifactId__/reflect-config.json) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.1/refcodes-archetype-alt-eventbus/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.1/refcodes-archetype-alt-eventbus/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.1/refcodes-archetype-alt-eventbus/src/main/resources/archetype-resources/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`reflect-config.json`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.1/refcodes-archetype-alt-eventbus/src/main/resources/archetype-resources/src/main/resources/META-INF/native-image/__groupId__/__artifactId__/reflect-config.json) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.1/refcodes-archetype-alt-filter/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.1/refcodes-archetype-alt-filter/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.1/refcodes-archetype-alt-filter/src/main/resources/archetype-resources/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`reflect-config.json`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.1/refcodes-archetype-alt-filter/src/main/resources/archetype-resources/src/main/resources/META-INF/native-image/__groupId__/__artifactId__/reflect-config.json) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.1/refcodes-archetype-alt-rest/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.1/refcodes-archetype-alt-rest/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.1/refcodes-archetype-alt-rest/src/main/resources/archetype-resources/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`reflect-config.json`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.1/refcodes-archetype-alt-rest/src/main/resources/archetype-resources/src/main/resources/META-INF/native-image/__groupId__/__artifactId__/reflect-config.json) 
