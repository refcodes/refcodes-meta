> This change list has been auto-generated on `triton` by `steiner` with `changelist-all.sh` on the 2023-09-28 at 09:25:32.

## Change list &lt;refcodes-licensing&gt; (version 3.3.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-licensing/src/refcodes-licensing-3.3.2/pom.xml) 

## Change list &lt;refcodes-parent&gt; (version 3.3.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-parent/src/refcodes-parent-3.3.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-parent/src/refcodes-parent-3.3.2/README.md) 

## Change list &lt;refcodes-time&gt; (version 3.3.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-time/src/refcodes-time-3.3.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-time/src/refcodes-time-3.3.2/README.md) 

## Change list &lt;refcodes-mixin&gt; (version 3.3.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-mixin/src/refcodes-mixin-3.3.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-mixin/src/refcodes-mixin-3.3.2/README.md) 

## Change list &lt;refcodes-data&gt; (version 3.3.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data/src/refcodes-data-3.3.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data/src/refcodes-data-3.3.2/README.md) 

## Change list &lt;refcodes-exception&gt; (version 3.3.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-exception/src/refcodes-exception-3.3.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-exception/src/refcodes-exception-3.3.2/README.md) 

## Change list &lt;refcodes-factory&gt; (version 3.3.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-factory/src/refcodes-factory-3.3.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-factory/src/refcodes-factory-3.3.2/README.md) 

## Change list &lt;refcodes-factory-alt&gt; (version 3.3.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-factory-alt/src/refcodes-factory-alt-3.3.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-factory-alt/src/refcodes-factory-alt-3.3.2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-factory-alt/src/refcodes-factory-alt-3.3.2/refcodes-factory-alt-spring/pom.xml) 

## Change list &lt;refcodes-controlflow&gt; (version 3.3.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-controlflow/src/refcodes-controlflow-3.3.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-controlflow/src/refcodes-controlflow-3.3.2/README.md) 

## Change list &lt;refcodes-numerical&gt; (version 3.3.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-numerical/src/refcodes-numerical-3.3.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-numerical/src/refcodes-numerical-3.3.2/README.md) 

## Change list &lt;refcodes-generator&gt; (version 3.3.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-generator/src/refcodes-generator-3.3.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-generator/src/refcodes-generator-3.3.2/README.md) 

## Change list &lt;refcodes-matcher&gt; (version 3.3.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-matcher/src/refcodes-matcher-3.3.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-matcher/src/refcodes-matcher-3.3.2/README.md) 

## Change list &lt;refcodes-struct&gt; (version 3.3.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-struct/src/refcodes-struct-3.3.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-struct/src/refcodes-struct-3.3.2/README.md) 

## Change list &lt;refcodes-runtime&gt; (version 3.3.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-3.3.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-3.3.2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`Host.java`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-3.3.2/src/main/java/org/refcodes/runtime/Host.java) (see Javadoc at [`Host.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-runtime/3.3.2/org.refcodes.runtime/org/refcodes/runtime/Host.html))
* \[<span style="color:green">MODIFIED</span>\] [`SystemProperty.java`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-3.3.2/src/main/java/org/refcodes/runtime/SystemProperty.java) (see Javadoc at [`SystemProperty.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-runtime/3.3.2/org.refcodes.runtime/org/refcodes/runtime/SystemProperty.html))

## Change list &lt;refcodes-struct-ext&gt; (version 3.3.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-struct-ext/src/refcodes-struct-ext-3.3.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-struct-ext/src/refcodes-struct-ext-3.3.2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-struct-ext/src/refcodes-struct-ext-3.3.2/refcodes-struct-ext-factory/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`CanonicalMapFactory.java`](https://bitbucket.org/refcodes/refcodes-struct-ext/src/refcodes-struct-ext-3.3.2/refcodes-struct-ext-factory/src/main/java/org/refcodes/struct/ext/factory/CanonicalMapFactory.java) (see Javadoc at [`CanonicalMapFactory.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-struct-ext-factory/3.3.2/org.refcodes.struct.ext.factory/org/refcodes/struct/ext/factory/CanonicalMapFactory.html))

## Change list &lt;refcodes-component&gt; (version 3.3.2)

