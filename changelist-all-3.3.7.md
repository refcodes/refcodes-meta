> This change list has been auto-generated on `triton` by `steiner` with `changelist-all.sh` on the 2024-05-30 at 20:01:00.

## Change list &lt;refcodes-licensing&gt; (version 3.3.7)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-licensing/src/refcodes-licensing-3.3.7/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-licensing/src/refcodes-licensing-3.3.7/README.md) 

## Change list &lt;refcodes-parent&gt; (version 3.3.7)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-parent/src/refcodes-parent-3.3.7/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-parent/src/refcodes-parent-3.3.7/README.md) 

## Change list &lt;refcodes-time&gt; (version 3.3.7)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-time/src/refcodes-time-3.3.7/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-time/src/refcodes-time-3.3.7/README.md) 

## Change list &lt;refcodes-mixin&gt; (version 3.3.7)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-mixin/src/refcodes-mixin-3.3.7/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-mixin/src/refcodes-mixin-3.3.7/README.md) 

## Change list &lt;refcodes-data&gt; (version 3.3.7)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data/src/refcodes-data-3.3.7/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data/src/refcodes-data-3.3.7/README.md) 

## Change list &lt;refcodes-exception&gt; (version 3.3.7)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-exception/src/refcodes-exception-3.3.7/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-exception/src/refcodes-exception-3.3.7/README.md) 

## Change list &lt;refcodes-factory&gt; (version 3.3.7)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-factory/src/refcodes-factory-3.3.7/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-factory/src/refcodes-factory-3.3.7/README.md) 

## Change list &lt;refcodes-factory-alt&gt; (version 3.3.7)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-factory-alt/src/refcodes-factory-alt-3.3.7/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-factory-alt/src/refcodes-factory-alt-3.3.7/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-factory-alt/src/refcodes-factory-alt-3.3.7/refcodes-factory-alt-spring/pom.xml) 

## Change list &lt;refcodes-controlflow&gt; (version 3.3.7)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-controlflow/src/refcodes-controlflow-3.3.7/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-controlflow/src/refcodes-controlflow-3.3.7/README.md) 

## Change list &lt;refcodes-numerical&gt; (version 3.3.7)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-numerical/src/refcodes-numerical-3.3.7/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-numerical/src/refcodes-numerical-3.3.7/README.md) 

## Change list &lt;refcodes-generator&gt; (version 3.3.7)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-generator/src/refcodes-generator-3.3.7/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-generator/src/refcodes-generator-3.3.7/README.md) 

## Change list &lt;refcodes-schema&gt; (version 3.3.7)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-schema/src/refcodes-schema-3.3.7/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-schema/src/refcodes-schema-3.3.7/README.md) 

## Change list &lt;refcodes-matcher&gt; (version 3.3.7)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-matcher/src/refcodes-matcher-3.3.7/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-matcher/src/refcodes-matcher-3.3.7/README.md) 

## Change list &lt;refcodes-struct&gt; (version 3.3.7)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-struct/src/refcodes-struct-3.3.7/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-struct/src/refcodes-struct-3.3.7/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`PathMapBuilderImpl.java`](https://bitbucket.org/refcodes/refcodes-struct/src/refcodes-struct-3.3.7/src/main/java/org/refcodes/struct/PathMapBuilderImpl.java) (see Javadoc at [`PathMapBuilderImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-struct/3.3.7/org.refcodes.struct/org/refcodes/struct/PathMapBuilderImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`PathMap.java`](https://bitbucket.org/refcodes/refcodes-struct/src/refcodes-struct-3.3.7/src/main/java/org/refcodes/struct/PathMap.java) (see Javadoc at [`PathMap.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-struct/3.3.7/org.refcodes.struct/org/refcodes/struct/PathMap.html))
* \[<span style="color:green">MODIFIED</span>\] [`StructureUtility.java`](https://bitbucket.org/refcodes/refcodes-struct/src/refcodes-struct-3.3.7/src/main/java/org/refcodes/struct/StructureUtility.java) (see Javadoc at [`StructureUtility.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-struct/3.3.7/org.refcodes.struct/org/refcodes/struct/StructureUtility.html))
* \[<span style="color:green">MODIFIED</span>\] [`TypeUtility.java`](https://bitbucket.org/refcodes/refcodes-struct/src/refcodes-struct-3.3.7/src/main/java/org/refcodes/struct/TypeUtility.java) (see Javadoc at [`TypeUtility.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-struct/3.3.7/org.refcodes.struct/org/refcodes/struct/TypeUtility.html))

## Change list &lt;refcodes-runtime&gt; (version 3.3.7)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-3.3.7/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-3.3.7/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`EnvironmentVariable.java`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-3.3.7/src/main/java/org/refcodes/runtime/EnvironmentVariable.java) (see Javadoc at [`EnvironmentVariable.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-runtime/3.3.7/org.refcodes.runtime/org/refcodes/runtime/EnvironmentVariable.html))
* \[<span style="color:green">MODIFIED</span>\] [`SystemProperty.java`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-3.3.7/src/main/java/org/refcodes/runtime/SystemProperty.java) (see Javadoc at [`SystemProperty.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-runtime/3.3.7/org.refcodes.runtime/org/refcodes/runtime/SystemProperty.html))

## Change list &lt;refcodes-struct-ext&gt; (version 3.3.7)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-struct-ext/src/refcodes-struct-ext-3.3.7/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-struct-ext/src/refcodes-struct-ext-3.3.7/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-struct-ext/src/refcodes-struct-ext-3.3.7/refcodes-struct-ext-factory/pom.xml) 

## Change list &lt;refcodes-component&gt; (version 3.3.7)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.3.7/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.3.7/README.md) 

## Change list &lt;refcodes-data-ext&gt; (version 3.3.7)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-3.3.7/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-3.3.7/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-3.3.7/refcodes-data-ext-checkers/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-3.3.7/refcodes-data-ext-checkers/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-3.3.7/refcodes-data-ext-chess/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-3.3.7/refcodes-data-ext-chess/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-3.3.7/refcodes-data-ext-corporate/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-3.3.7/refcodes-data-ext-corporate/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-3.3.7/refcodes-data-ext-symbols/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-3.3.7/refcodes-data-ext-symbols/README.md) 

## Change list &lt;refcodes-graphical&gt; (version 3.3.7)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-graphical/src/refcodes-graphical-3.3.7/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-graphical/src/refcodes-graphical-3.3.7/README.md) 

## Change list &lt;refcodes-textual&gt; (version 3.3.7)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-3.3.7/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-3.3.7/README.md) 

## Change list &lt;refcodes-criteria&gt; (version 3.3.7)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-3.3.7/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-3.3.7/README.md) 

## Change list &lt;refcodes-io&gt; (version 3.3.7)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-io/src/refcodes-io-3.3.7/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-io/src/refcodes-io-3.3.7/README.md) 

## Change list &lt;refcodes-tabular&gt; (version 3.3.7)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-tabular/src/refcodes-tabular-3.3.7/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-tabular/src/refcodes-tabular-3.3.7/README.md) 

## Change list &lt;refcodes-observer&gt; (version 3.3.7)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-observer/src/refcodes-observer-3.3.7/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-observer/src/refcodes-observer-3.3.7/README.md) 

## Change list &lt;refcodes-command&gt; (version 3.3.7)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-command/src/refcodes-command-3.3.7/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-command/src/refcodes-command-3.3.7/README.md) 

## Change list &lt;refcodes-cli&gt; (version 3.3.7)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.3.7/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.3.7/README.md) 

## Change list &lt;refcodes-audio&gt; (version 3.3.7)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-audio/src/refcodes-audio-3.3.7/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-audio/src/refcodes-audio-3.3.7/README.md) 

## Change list &lt;refcodes-codec&gt; (version 3.3.7)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-3.3.7/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-3.3.7/README.md) 

## Change list &lt;refcodes-component-ext&gt; (version 3.3.7)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-3.3.7/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-3.3.7/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-3.3.7/refcodes-component-ext-observer/pom.xml) 

## Change list &lt;refcodes-properties&gt; (version 3.3.7)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-3.3.7/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-3.3.7/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`PropertiesTest.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-3.3.7/src/test/java/org/refcodes/properties/PropertiesTest.java) 

## Change list &lt;refcodes-security&gt; (version 3.3.7)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security/src/refcodes-security-3.3.7/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security/src/refcodes-security-3.3.7/README.md) 

## Change list &lt;refcodes-security-alt&gt; (version 3.3.7)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-3.3.7/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-3.3.7/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-3.3.7/refcodes-security-alt-chaos/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-3.3.7/refcodes-security-alt-chaos/README.md) 

## Change list &lt;refcodes-security-ext&gt; (version 3.3.7)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-3.3.7/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-3.3.7/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-3.3.7/refcodes-security-ext-chaos/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-3.3.7/refcodes-security-ext-chaos/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-3.3.7/refcodes-security-ext-spring/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-3.3.7/refcodes-security-ext-spring/README.md) 

## Change list &lt;refcodes-properties-ext&gt; (version 3.3.7)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.3.7/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.3.7/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.3.7/refcodes-properties-ext-application/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.3.7/refcodes-properties-ext-application/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.3.7/refcodes-properties-ext-cli/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.3.7/refcodes-properties-ext-cli/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.3.7/refcodes-properties-ext-obfuscation/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.3.7/refcodes-properties-ext-obfuscation/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.3.7/refcodes-properties-ext-observer/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.3.7/refcodes-properties-ext-observer/README.md) 

## Change list &lt;refcodes-logger&gt; (version 3.3.7)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger/src/refcodes-logger-3.3.7/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger/src/refcodes-logger-3.3.7/README.md) 

## Change list &lt;refcodes-logger-alt&gt; (version 3.3.7)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.3.7/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.3.7/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.3.7/refcodes-logger-alt-async/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.3.7/refcodes-logger-alt-async/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.3.7/refcodes-logger-alt-console/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.3.7/refcodes-logger-alt-console/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.3.7/refcodes-logger-alt-io/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.3.7/refcodes-logger-alt-io/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.3.7/refcodes-logger-alt-jul/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.3.7/refcodes-logger-alt-jul/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.3.7/refcodes-logger-alt-simpledb/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.3.7/refcodes-logger-alt-simpledb/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.3.7/refcodes-logger-alt-slf4j-legacy/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.3.7/refcodes-logger-alt-slf4j-legacy/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.3.7/refcodes-logger-alt-slf4j/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.3.7/refcodes-logger-alt-slf4j/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.3.7/refcodes-logger-alt-spring/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.3.7/refcodes-logger-alt-spring/README.md) 

## Change list &lt;refcodes-logger-ext&gt; (version 3.3.7)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-3.3.7/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-3.3.7/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-3.3.7/refcodes-logger-ext-slf4j-legacy/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-3.3.7/refcodes-logger-ext-slf4j-legacy/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-3.3.7/refcodes-logger-ext-slf4j/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-3.3.7/refcodes-logger-ext-slf4j/README.md) 

## Change list &lt;refcodes-graphical-ext&gt; (version 3.3.7)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-3.3.7/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-3.3.7/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-3.3.7/refcodes-graphical-ext-javafx/pom.xml) 

## Change list &lt;refcodes-checkerboard&gt; (version 3.3.7)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-3.3.7/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-3.3.7/README.md) 

## Change list &lt;refcodes-checkerboard-alt&gt; (version 3.3.7)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-alt/src/refcodes-checkerboard-alt-3.3.7/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-checkerboard-alt/src/refcodes-checkerboard-alt-3.3.7/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-alt/src/refcodes-checkerboard-alt-3.3.7/refcodes-checkerboard-alt-javafx/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`FxCheckerboardViewer.java`](https://bitbucket.org/refcodes/refcodes-checkerboard-alt/src/refcodes-checkerboard-alt-3.3.7/refcodes-checkerboard-alt-javafx/src/main/java/org/refcodes/checkerboard/alt/javafx/FxCheckerboardViewer.java) (see Javadoc at [`FxCheckerboardViewer.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-checkerboard-alt-javafx/3.3.7/org.refcodes.checkerboard.alt.javafx/org/refcodes/checkerboard/alt/javafx/FxCheckerboardViewer.html))

## Change list &lt;refcodes-net&gt; (version 3.3.7)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-3.3.7/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-3.3.7/README.md) 

## Change list &lt;refcodes-web&gt; (version 3.3.7)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-web/src/refcodes-web-3.3.7/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-web/src/refcodes-web-3.3.7/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`HtmlMediaTypeFactory.java`](https://bitbucket.org/refcodes/refcodes-web/src/refcodes-web-3.3.7/src/main/java/org/refcodes/web/HtmlMediaTypeFactory.java) (see Javadoc at [`HtmlMediaTypeFactory.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-web/3.3.7/org.refcodes.web/org/refcodes/web/HtmlMediaTypeFactory.html))
* \[<span style="color:green">MODIFIED</span>\] [`HttpBodyMap.java`](https://bitbucket.org/refcodes/refcodes-web/src/refcodes-web-3.3.7/src/main/java/org/refcodes/web/HttpBodyMap.java) (see Javadoc at [`HttpBodyMap.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-web/3.3.7/org.refcodes.web/org/refcodes/web/HttpBodyMap.html))
* \[<span style="color:green">MODIFIED</span>\] [`JsonMediaTypeFactory.java`](https://bitbucket.org/refcodes/refcodes-web/src/refcodes-web-3.3.7/src/main/java/org/refcodes/web/JsonMediaTypeFactory.java) (see Javadoc at [`JsonMediaTypeFactory.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-web/3.3.7/org.refcodes.web/org/refcodes/web/JsonMediaTypeFactory.html))
* \[<span style="color:green">MODIFIED</span>\] [`MediaTypeFactory.java`](https://bitbucket.org/refcodes/refcodes-web/src/refcodes-web-3.3.7/src/main/java/org/refcodes/web/MediaTypeFactory.java) (see Javadoc at [`MediaTypeFactory.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-web/3.3.7/org.refcodes.web/org/refcodes/web/MediaTypeFactory.html))
* \[<span style="color:green">MODIFIED</span>\] [`XmlMediaTypeFactory.java`](https://bitbucket.org/refcodes/refcodes-web/src/refcodes-web-3.3.7/src/main/java/org/refcodes/web/XmlMediaTypeFactory.java) (see Javadoc at [`XmlMediaTypeFactory.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-web/3.3.7/org.refcodes.web/org/refcodes/web/XmlMediaTypeFactory.html))
* \[<span style="color:green">MODIFIED</span>\] [`YamlMediaTypeFactory.java`](https://bitbucket.org/refcodes/refcodes-web/src/refcodes-web-3.3.7/src/main/java/org/refcodes/web/YamlMediaTypeFactory.java) (see Javadoc at [`YamlMediaTypeFactory.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-web/3.3.7/org.refcodes.web/org/refcodes/web/YamlMediaTypeFactory.html))
* \[<span style="color:green">MODIFIED</span>\] [`AbstractMediaFactoryTest.java`](https://bitbucket.org/refcodes/refcodes-web/src/refcodes-web-3.3.7/src/test/java/org/refcodes/web/AbstractMediaFactoryTest.java) 

## Change list &lt;refcodes-rest&gt; (version 3.3.7)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-3.3.7/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-3.3.7/README.md) 

## Change list &lt;refcodes-hal&gt; (version 3.3.7)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-3.3.7/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-3.3.7/README.md) 

## Change list &lt;refcodes-eventbus&gt; (version 3.3.7)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-eventbus/src/refcodes-eventbus-3.3.7/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-eventbus/src/refcodes-eventbus-3.3.7/README.md) 

## Change list &lt;refcodes-eventbus-ext&gt; (version 3.3.7)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-3.3.7/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-3.3.7/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-3.3.7/refcodes-eventbus-ext-application/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-3.3.7/refcodes-eventbus-ext-application/README.md) 

## Change list &lt;refcodes-decoupling&gt; (version 3.3.7)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-decoupling/src/refcodes-decoupling-3.3.7/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-decoupling/src/refcodes-decoupling-3.3.7/README.md) 

## Change list &lt;refcodes-decoupling-ext&gt; (version 3.3.7)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-decoupling-ext/src/refcodes-decoupling-ext-3.3.7/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-decoupling-ext/src/refcodes-decoupling-ext-3.3.7/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-decoupling-ext/src/refcodes-decoupling-ext-3.3.7/refcodes-decoupling-ext-application/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-decoupling-ext/src/refcodes-decoupling-ext-3.3.7/refcodes-decoupling-ext-application/README.md) 

## Change list &lt;refcodes-filesystem&gt; (version 3.3.7)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-filesystem/src/refcodes-filesystem-3.3.7/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-filesystem/src/refcodes-filesystem-3.3.7/README.md) 

## Change list &lt;refcodes-filesystem-alt&gt; (version 3.3.7)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-filesystem-alt/src/refcodes-filesystem-alt-3.3.7/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-filesystem-alt/src/refcodes-filesystem-alt-3.3.7/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-filesystem-alt/src/refcodes-filesystem-alt-3.3.7/refcodes-filesystem-alt-s3/pom.xml) 

## Change list &lt;refcodes-forwardsecrecy&gt; (version 3.3.7)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy/src/refcodes-forwardsecrecy-3.3.7/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy/src/refcodes-forwardsecrecy-3.3.7/README.md) 

## Change list &lt;refcodes-forwardsecrecy-alt&gt; (version 3.3.7)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-3.3.7/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-3.3.7/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-3.3.7/refcodes-forwardsecrecy-alt-filesystem/pom.xml) 

## Change list &lt;refcodes-io-ext&gt; (version 3.3.7)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-3.3.7/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-3.3.7/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-3.3.7/refcodes-io-ext-observer/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-3.3.7/refcodes-io-ext-observer/README.md) 

## Change list &lt;refcodes-jobbus&gt; (version 3.3.7)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-jobbus/src/refcodes-jobbus-3.3.7/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-jobbus/src/refcodes-jobbus-3.3.7/README.md) 

## Change list &lt;refcodes-rest-ext&gt; (version 3.3.7)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-3.3.7/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-3.3.7/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-3.3.7/refcodes-rest-ext-eureka/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-3.3.7/refcodes-rest-ext-eureka/README.md) 

## Change list &lt;refcodes-remoting&gt; (version 3.3.7)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-remoting/src/refcodes-remoting-3.3.7/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-remoting/src/refcodes-remoting-3.3.7/README.md) 

## Change list &lt;refcodes-remoting-ext&gt; (version 3.3.7)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-3.3.7/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-3.3.7/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-3.3.7/refcodes-remoting-ext-observer/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-3.3.7/refcodes-remoting-ext-observer/README.md) 

## Change list &lt;refcodes-serial&gt; (version 3.3.7)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial/src/refcodes-serial-3.3.7/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-serial/src/refcodes-serial-3.3.7/README.md) 

## Change list &lt;refcodes-serial-alt&gt; (version 3.3.7)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial-alt/src/refcodes-serial-alt-3.3.7/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-serial-alt/src/refcodes-serial-alt-3.3.7/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial-alt/src/refcodes-serial-alt-3.3.7/refcodes-serial-alt-tty/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-serial-alt/src/refcodes-serial-alt-3.3.7/refcodes-serial-alt-tty/README.md) 

## Change list &lt;refcodes-serial-ext&gt; (version 3.3.7)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-3.3.7/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-3.3.7/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-3.3.7/refcodes-serial-ext-handshake/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-3.3.7/refcodes-serial-ext-handshake/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-3.3.7/refcodes-serial-ext-observer/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-3.3.7/refcodes-serial-ext-observer/README.md) 

## Change list &lt;refcodes-p2p&gt; (version 3.3.7)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-p2p/src/refcodes-p2p-3.3.7/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-p2p/src/refcodes-p2p-3.3.7/README.md) 

## Change list &lt;refcodes-p2p-alt&gt; (version 3.3.7)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-p2p-alt/src/refcodes-p2p-alt-3.3.7/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-p2p-alt/src/refcodes-p2p-alt-3.3.7/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-p2p-alt/src/refcodes-p2p-alt-3.3.7/refcodes-p2p-alt-serial/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`AbstractTtyPortTest.java`](https://bitbucket.org/refcodes/refcodes-p2p-alt/src/refcodes-p2p-alt-3.3.7/refcodes-p2p-alt-serial/src/test/java/org/refcodes/p2p/alt/serial/AbstractTtyPortTest.java) 

## Change list &lt;refcodes-p2p-ext&gt; (version 3.3.7)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-p2p-ext/src/refcodes-p2p-ext-3.3.7/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-p2p-ext/src/refcodes-p2p-ext-3.3.7/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-p2p-ext/src/refcodes-p2p-ext-3.3.7/refcodes-p2p-ext-observer/pom.xml) 

## Change list &lt;refcodes-tabular-alt&gt; (version 3.3.7)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-tabular-alt/src/refcodes-tabular-alt-3.3.7/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-tabular-alt/src/refcodes-tabular-alt-3.3.7/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-tabular-alt/src/refcodes-tabular-alt-3.3.7/refcodes-tabular-alt-forwardsecrecy/pom.xml) 

## Change list &lt;refcodes-archetype&gt; (version 3.3.7)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype/src/refcodes-archetype-3.3.7/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype/src/refcodes-archetype-3.3.7/README.md) 

## Change list &lt;refcodes-archetype-alt&gt; (version 3.3.7)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.7/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.7/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.7/refcodes-archetype-alt-c2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.7/refcodes-archetype-alt-c2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.7/refcodes-archetype-alt-c2/src/main/resources/archetype-resources/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.7/refcodes-archetype-alt-cli/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.7/refcodes-archetype-alt-cli/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.7/refcodes-archetype-alt-cli/src/main/resources/archetype-resources/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.7/refcodes-archetype-alt-csv/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.7/refcodes-archetype-alt-csv/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.7/refcodes-archetype-alt-csv/src/main/resources/archetype-resources/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.7/refcodes-archetype-alt-decoupling/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.7/refcodes-archetype-alt-decoupling/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.7/refcodes-archetype-alt-decoupling/src/main/resources/archetype-resources/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.7/refcodes-archetype-alt-eventbus/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.7/refcodes-archetype-alt-eventbus/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.7/refcodes-archetype-alt-eventbus/src/main/resources/archetype-resources/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.7/refcodes-archetype-alt-filter/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.7/refcodes-archetype-alt-filter/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.7/refcodes-archetype-alt-filter/src/main/resources/archetype-resources/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.7/refcodes-archetype-alt-rest/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.7/refcodes-archetype-alt-rest/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.7/refcodes-archetype-alt-rest/src/main/resources/archetype-resources/pom.xml) 
