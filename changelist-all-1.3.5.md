# Change list for REFCODES.ORG artifacts' version 1.3.5

> This change list has been auto-generated on `triton` by `steiner` with with `changelist-all.sh` on the 2018-10-28 at 17:44:48.

## Change list &lt;refcodes-licensing&gt; (version 1.3.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-licensing/src/refcodes-licensing-1.3.5/pom.xml) 

## Change list &lt;refcodes-parent&gt; (version 1.3.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-parent/src/refcodes-parent-1.3.5/pom.xml) 

## Change list &lt;refcodes-time&gt; (version 1.3.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-time/src/refcodes-time-1.3.5/pom.xml) 

## Change list &lt;refcodes-mixin&gt; (version 1.3.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-mixin/src/refcodes-mixin-1.3.5/pom.xml) 

## Change list &lt;refcodes-data&gt; (version 1.3.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data/src/refcodes-data-1.3.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`SchemeTest.java`](https://bitbucket.org/refcodes/refcodes-data/src/refcodes-data-1.3.5/src/test/java/org/refcodes/data/SchemeTest.java) 

## Change list &lt;refcodes-exception&gt; (version 1.3.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-exception/src/refcodes-exception-1.3.5/pom.xml) 

## Change list &lt;refcodes-factory&gt; (version 1.3.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-factory/src/refcodes-factory-1.3.5/pom.xml) 

## Change list &lt;refcodes-factory-alt&gt; (version 1.3.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-factory-alt/src/refcodes-factory-alt-1.3.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-factory-alt/src/refcodes-factory-alt-1.3.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-factory-alt/src/refcodes-factory-alt-1.3.5/refcodes-factory-alt-spring/pom.xml) 

## Change list &lt;refcodes-controlflow&gt; (version 1.3.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-controlflow/src/refcodes-controlflow-1.3.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`ExceptionWatchdogTest.java`](https://bitbucket.org/refcodes/refcodes-controlflow/src/refcodes-controlflow-1.3.5/src/test/java/org/refcodes/controlflow/ExceptionWatchdogTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`RetryCounterTest.java`](https://bitbucket.org/refcodes/refcodes-controlflow/src/refcodes-controlflow-1.3.5/src/test/java/org/refcodes/controlflow/RetryCounterTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`RetryTimeoutTest.java`](https://bitbucket.org/refcodes/refcodes-controlflow/src/refcodes-controlflow-1.3.5/src/test/java/org/refcodes/controlflow/RetryTimeoutTest.java) 

## Change list &lt;refcodes-numerical&gt; (version 1.3.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-numerical/src/refcodes-numerical-1.3.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`NumberBaseBuilderTest.java`](https://bitbucket.org/refcodes/refcodes-numerical/src/refcodes-numerical-1.3.5/src/test/java/org/refcodes/numerical/NumberBaseBuilderTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`NumericalUtilityTest.java`](https://bitbucket.org/refcodes/refcodes-numerical/src/refcodes-numerical-1.3.5/src/test/java/org/refcodes/numerical/NumericalUtilityTest.java) 

## Change list &lt;refcodes-generator&gt; (version 1.3.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-generator/src/refcodes-generator-1.3.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`UniqueIdGeneratorTest.java`](https://bitbucket.org/refcodes/refcodes-generator/src/refcodes-generator-1.3.5/src/test/java/org/refcodes/generator/UniqueIdGeneratorTest.java) 

## Change list &lt;refcodes-matcher&gt; (version 1.3.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-matcher/src/refcodes-matcher-1.3.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`PathMatcherImpl.java`](https://bitbucket.org/refcodes/refcodes-matcher/src/refcodes-matcher-1.3.5/src/main/java/org/refcodes/matcher/PathMatcherImpl.java) (see Javadoc at [`PathMatcherImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-matcher/1.3.5/org/refcodes/matcher/PathMatcherImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`PathMatcher.java`](https://bitbucket.org/refcodes/refcodes-matcher/src/refcodes-matcher-1.3.5/src/main/java/org/refcodes/matcher/PathMatcher.java) (see Javadoc at [`PathMatcher.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-matcher/1.3.5/org/refcodes/matcher/PathMatcher.html))
* \[<span style="color:green">MODIFIED</span>\] [`PathMatcherTest.java`](https://bitbucket.org/refcodes/refcodes-matcher/src/refcodes-matcher-1.3.5/src/test/java/org/refcodes/matcher/PathMatcherTest.java) 

## Change list &lt;refcodes-structure&gt; (version 1.3.5)

* \[<span style="color:blue">ADDED</span>\] [`StructureUtility.java`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-1.3.5/src/main/java/org/refcodes/structure/StructureUtility.java) (see Javadoc at [`StructureUtility.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-structure/1.3.5/org/refcodes/structure/StructureUtility.html))
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-1.3.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-1.3.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`CanonicalMap.java`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-1.3.5/src/main/java/org/refcodes/structure/CanonicalMap.java) (see Javadoc at [`CanonicalMap.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-structure/1.3.5/org/refcodes/structure/CanonicalMap.html))
* \[<span style="color:green">MODIFIED</span>\] [`PathMapBuilderImpl.java`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-1.3.5/src/main/java/org/refcodes/structure/PathMapBuilderImpl.java) (see Javadoc at [`PathMapBuilderImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-structure/1.3.5/org/refcodes/structure/PathMapBuilderImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`PathMapImpl.java`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-1.3.5/src/main/java/org/refcodes/structure/PathMapImpl.java) (see Javadoc at [`PathMapImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-structure/1.3.5/org/refcodes/structure/PathMapImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`PathMap.java`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-1.3.5/src/main/java/org/refcodes/structure/PathMap.java) (see Javadoc at [`PathMap.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-structure/1.3.5/org/refcodes/structure/PathMap.html))
* \[<span style="color:green">MODIFIED</span>\] [`PropertiesAccessorMixin.java`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-1.3.5/src/main/java/org/refcodes/structure/PropertiesAccessorMixin.java) (see Javadoc at [`PropertiesAccessorMixin.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-structure/1.3.5/org/refcodes/structure/PropertiesAccessorMixin.html))
* \[<span style="color:green">MODIFIED</span>\] [`CanonicalMapTest.java`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-1.3.5/src/test/java/org/refcodes/structure/CanonicalMapTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`PathMapTest.java`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-1.3.5/src/test/java/org/refcodes/structure/PathMapTest.java) 

## Change list &lt;refcodes-runtime&gt; (version 1.3.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-1.3.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-1.3.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`SystemUtility.java`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-1.3.5/src/main/java/org/refcodes/runtime/SystemUtility.java) (see Javadoc at [`SystemUtility.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-runtime/1.3.5/org/refcodes/runtime/SystemUtility.html))
* \[<span style="color:green">MODIFIED</span>\] [`ConfigLocatorTest.java`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-1.3.5/src/test/java/org/refcodes/runtime/ConfigLocatorTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`PropertyBuilderTest.java`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-1.3.5/src/test/java/org/refcodes/runtime/PropertyBuilderTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`RuntimeUtilityTest.java`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-1.3.5/src/test/java/org/refcodes/runtime/RuntimeUtilityTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`SystemContextTest.java`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-1.3.5/src/test/java/org/refcodes/runtime/SystemContextTest.java) 

## Change list &lt;refcodes-component&gt; (version 1.3.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-1.3.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`ConfigurableLifeCycleAutomatonTest.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-1.3.5/src/test/java/org/refcodes/component/ConfigurableLifeCycleAutomatonTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`ConnectionAutomatonTest.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-1.3.5/src/test/java/org/refcodes/component/ConnectionAutomatonTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`DeviceAutomatonTest.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-1.3.5/src/test/java/org/refcodes/component/DeviceAutomatonTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`LifeCycleAutomatonTest.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-1.3.5/src/test/java/org/refcodes/component/LifeCycleAutomatonTest.java) 

## Change list &lt;refcodes-data-ext&gt; (version 1.3.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.3.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.3.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.3.5/refcodes-data-ext-boulderdash/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`BoulderDashCaveMapTest.java`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.3.5/refcodes-data-ext-boulderdash/src/test/java/org/refcodes/data/ext/boulderdash/BoulderDashCaveMapTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.3.5/refcodes-data-ext-checkers/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.3.5/refcodes-data-ext-checkers/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.3.5/refcodes-data-ext-chess/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.3.5/refcodes-data-ext-chess/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.3.5/refcodes-data-ext-corporate/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.3.5/refcodes-data-ext-symbols/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.3.5/refcodes-data-ext-symbols/README.md) 

## Change list &lt;refcodes-graphical&gt; (version 1.3.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-graphical/src/refcodes-graphical-1.3.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`BoxBorderModeTest.java`](https://bitbucket.org/refcodes/refcodes-graphical/src/refcodes-graphical-1.3.5/src/test/java/org/refcodes/graphical/BoxBorderModeTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`GraphicalUtilityTest.java`](https://bitbucket.org/refcodes/refcodes-graphical/src/refcodes-graphical-1.3.5/src/test/java/org/refcodes/graphical/GraphicalUtilityTest.java) 

## Change list &lt;refcodes-textual&gt; (version 1.3.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-1.3.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`AlignTextBuilderTest.java`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-1.3.5/src/test/java/org/refcodes/textual/AlignTextBuilderTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`AsciiArtBuilderTest.java`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-1.3.5/src/test/java/org/refcodes/textual/AsciiArtBuilderTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`CaseStyleBuilderTest.java`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-1.3.5/src/test/java/org/refcodes/textual/CaseStyleBuilderTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`CaseStyleTest.java`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-1.3.5/src/test/java/org/refcodes/textual/CaseStyleTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`CsvBuilderTest.java`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-1.3.5/src/test/java/org/refcodes/textual/CsvBuilderTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`EscapeTextBuilderTest.java`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-1.3.5/src/test/java/org/refcodes/textual/EscapeTextBuilderTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`MoreTextBuilderTest.java`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-1.3.5/src/test/java/org/refcodes/textual/MoreTextBuilderTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`OverwriteTextBuilderTest.java`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-1.3.5/src/test/java/org/refcodes/textual/OverwriteTextBuilderTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`ReplaceTextBuilderTest.java`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-1.3.5/src/test/java/org/refcodes/textual/ReplaceTextBuilderTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`SecretHintBuilderTest.java`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-1.3.5/src/test/java/org/refcodes/textual/SecretHintBuilderTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`TableBuilderTest.java`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-1.3.5/src/test/java/org/refcodes/textual/TableBuilderTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`TextBlockBuilderTest.java`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-1.3.5/src/test/java/org/refcodes/textual/TextBlockBuilderTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`TextBorderBuilderTest.java`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-1.3.5/src/test/java/org/refcodes/textual/TextBorderBuilderTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`TruncateTextBuilderTest.java`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-1.3.5/src/test/java/org/refcodes/textual/TruncateTextBuilderTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`VerboseTextBuilderTest.java`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-1.3.5/src/test/java/org/refcodes/textual/VerboseTextBuilderTest.java) 

## Change list &lt;refcodes-criteria&gt; (version 1.3.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-1.3.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-1.3.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`ExpressionCriteriaFactoryImplTest.java`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-1.3.5/src/test/java/org/refcodes/criteria/ExpressionCriteriaFactoryImplTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`ExpressionQueryFactoryTest.java`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-1.3.5/src/test/java/org/refcodes/criteria/ExpressionQueryFactoryTest.java) 

## Change list &lt;refcodes-tabular&gt; (version 1.3.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-tabular/src/refcodes-tabular-1.3.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`ColumnTest.java`](https://bitbucket.org/refcodes/refcodes-tabular/src/refcodes-tabular-1.3.5/src/test/java/org/refcodes/tabular/ColumnTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`CsvReaderTest.java`](https://bitbucket.org/refcodes/refcodes-tabular/src/refcodes-tabular-1.3.5/src/test/java/org/refcodes/tabular/CsvReaderTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`HeaderTest.java`](https://bitbucket.org/refcodes/refcodes-tabular/src/refcodes-tabular-1.3.5/src/test/java/org/refcodes/tabular/HeaderTest.java) 

## Change list &lt;refcodes-observer&gt; (version 1.3.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-observer/src/refcodes-observer-1.3.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`ObservableTest.java`](https://bitbucket.org/refcodes/refcodes-observer/src/refcodes-observer-1.3.5/src/test/java/org/refcodes/observer/ObservableTest.java) 

## Change list &lt;refcodes-command&gt; (version 1.3.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-command/src/refcodes-command-1.3.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-command/src/refcodes-command-1.3.5/README.md) 

## Change list &lt;refcodes-cli&gt; (version 1.3.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-1.3.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-1.3.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`ArgsParserImpl.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-1.3.5/src/main/java/org/refcodes/console/ArgsParserImpl.java) (see Javadoc at [`ArgsParserImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/1.3.5/org/refcodes/console/ArgsParserImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`ArgsParserTest.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-1.3.5/src/test/java/org/refcodes/console/ArgsParserTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`OptionalConditionTest.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-1.3.5/src/test/java/org/refcodes/console/OptionalConditionTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`StackOverflowTest.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-1.3.5/src/test/java/org/refcodes/console/StackOverflowTest.java) 

## Change list &lt;refcodes-io&gt; (version 1.3.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-io/src/refcodes-io-1.3.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-io/src/refcodes-io-1.3.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`FileUtilityTest.java`](https://bitbucket.org/refcodes/refcodes-io/src/refcodes-io-1.3.5/src/test/java/org/refcodes/io/FileUtilityTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`IoStreamConnectionTest.java`](https://bitbucket.org/refcodes/refcodes-io/src/refcodes-io-1.3.5/src/test/java/org/refcodes/io/IoStreamConnectionTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`LoopbackConnectionTest.java`](https://bitbucket.org/refcodes/refcodes-io/src/refcodes-io-1.3.5/src/test/java/org/refcodes/io/LoopbackConnectionTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`ZipFileStreamTest.java`](https://bitbucket.org/refcodes/refcodes-io/src/refcodes-io-1.3.5/src/test/java/org/refcodes/io/ZipFileStreamTest.java) 

## Change list &lt;refcodes-codec&gt; (version 1.3.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-1.3.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-1.3.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`BaseBuilderTest.java`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-1.3.5/src/test/java/org/refcodes/codec/BaseBuilderTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`BaseInputStreamDecoderTest.java`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-1.3.5/src/test/java/org/refcodes/codec/BaseInputStreamDecoderTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`BaseOutputStreamEncoderTest.java`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-1.3.5/src/test/java/org/refcodes/codec/BaseOutputStreamEncoderTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`ModemTest.java`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-1.3.5/src/test/java/org/refcodes/codec/ModemTest.java) 

## Change list &lt;refcodes-component-ext&gt; (version 1.3.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.3.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.3.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.3.5/refcodes-component-ext-observer/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`ObservableLifeCycleAutomatonTest.java`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.3.5/refcodes-component-ext-observer/src/test/java/org/refcodes/component/ext/observer/ObservableLifeCycleAutomatonTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`ObservableLifeCycleRequestAutomatonTest.java`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.3.5/refcodes-component-ext-observer/src/test/java/org/refcodes/component/ext/observer/ObservableLifeCycleRequestAutomatonTest.java) 

## Change list &lt;refcodes-properties&gt; (version 1.3.5)

* \[<span style="color:blue">ADDED</span>\] [`AbstractResourceProperties.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.3.5/src/main/java/org/refcodes/configuration/AbstractResourceProperties.java) (see Javadoc at [`AbstractResourceProperties.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties/1.3.5/org/refcodes/configuration/AbstractResourceProperties.html))
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.3.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.3.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`AbstractResourcePropertiesDecorator.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.3.5/src/main/java/org/refcodes/configuration/AbstractResourcePropertiesDecorator.java) (see Javadoc at [`AbstractResourcePropertiesDecorator.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties/1.3.5/org/refcodes/configuration/AbstractResourcePropertiesDecorator.html))
* \[<span style="color:green">MODIFIED</span>\] [`EnvironmentProperties.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.3.5/src/main/java/org/refcodes/configuration/EnvironmentProperties.java) (see Javadoc at [`EnvironmentProperties.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties/1.3.5/org/refcodes/configuration/EnvironmentProperties.html))
* \[<span style="color:green">MODIFIED</span>\] [`JavaProperties.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.3.5/src/main/java/org/refcodes/configuration/JavaProperties.java) (see Javadoc at [`JavaProperties.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties/1.3.5/org/refcodes/configuration/JavaProperties.html))
* \[<span style="color:green">MODIFIED</span>\] [`JsonProperties.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.3.5/src/main/java/org/refcodes/configuration/JsonProperties.java) (see Javadoc at [`JsonProperties.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties/1.3.5/org/refcodes/configuration/JsonProperties.html))
* \[<span style="color:green">MODIFIED</span>\] [`package-info.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.3.5/src/main/java/org/refcodes/configuration/package-info.java) 
* \[<span style="color:green">MODIFIED</span>\] [`ProfileProperties.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.3.5/src/main/java/org/refcodes/configuration/ProfileProperties.java) (see Javadoc at [`ProfileProperties.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties/1.3.5/org/refcodes/configuration/ProfileProperties.html))
* \[<span style="color:green">MODIFIED</span>\] [`Properties.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.3.5/src/main/java/org/refcodes/configuration/Properties.java) (see Javadoc at [`Properties.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties/1.3.5/org/refcodes/configuration/Properties.html))
* \[<span style="color:green">MODIFIED</span>\] [`ResourceProperties.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.3.5/src/main/java/org/refcodes/configuration/ResourceProperties.java) (see Javadoc at [`ResourceProperties.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties/1.3.5/org/refcodes/configuration/ResourceProperties.html))
* \[<span style="color:green">MODIFIED</span>\] [`StrictProperties.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.3.5/src/main/java/org/refcodes/configuration/StrictProperties.java) (see Javadoc at [`StrictProperties.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties/1.3.5/org/refcodes/configuration/StrictProperties.html))
* \[<span style="color:green">MODIFIED</span>\] [`SystemProperties.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.3.5/src/main/java/org/refcodes/configuration/SystemProperties.java) (see Javadoc at [`SystemProperties.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties/1.3.5/org/refcodes/configuration/SystemProperties.html))
* \[<span style="color:green">MODIFIED</span>\] [`TomlProperties.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.3.5/src/main/java/org/refcodes/configuration/TomlProperties.java) (see Javadoc at [`TomlProperties.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties/1.3.5/org/refcodes/configuration/TomlProperties.html))
* \[<span style="color:green">MODIFIED</span>\] [`XmlProperties.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.3.5/src/main/java/org/refcodes/configuration/XmlProperties.java) (see Javadoc at [`XmlProperties.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties/1.3.5/org/refcodes/configuration/XmlProperties.html))
* \[<span style="color:green">MODIFIED</span>\] [`YamlProperties.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.3.5/src/main/java/org/refcodes/configuration/YamlProperties.java) (see Javadoc at [`YamlProperties.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties/1.3.5/org/refcodes/configuration/YamlProperties.html))
* \[<span style="color:green">MODIFIED</span>\] [`AbstractResourcePropertiesTest.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.3.5/src/test/java/org/refcodes/configuration/AbstractResourcePropertiesTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`ArgsPropertiesTest.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.3.5/src/test/java/org/refcodes/configuration/ArgsPropertiesTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`EnvironmentPropertiesTest.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.3.5/src/test/java/org/refcodes/configuration/EnvironmentPropertiesTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`IniPropertiesTest.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.3.5/src/test/java/org/refcodes/configuration/IniPropertiesTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`NormalizedPathPropertiesTest.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.3.5/src/test/java/org/refcodes/configuration/NormalizedPathPropertiesTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`PolyglotPropertiesBuilderTest.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.3.5/src/test/java/org/refcodes/configuration/PolyglotPropertiesBuilderTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`PolyglotPropertiesTest.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.3.5/src/test/java/org/refcodes/configuration/PolyglotPropertiesTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`ProfilePropertiesTest.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.3.5/src/test/java/org/refcodes/configuration/ProfilePropertiesTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`PropertiesSugarTest.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.3.5/src/test/java/org/refcodes/configuration/PropertiesSugarTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`PropertiesTest.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.3.5/src/test/java/org/refcodes/configuration/PropertiesTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`ResourcePropertiesTest.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.3.5/src/test/java/org/refcodes/configuration/ResourcePropertiesTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`SystemPropertiesTest.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.3.5/src/test/java/org/refcodes/configuration/SystemPropertiesTest.java) 

## Change list &lt;refcodes-security&gt; (version 1.3.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security/src/refcodes-security-1.3.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security/src/refcodes-security-1.3.5/README.md) 

## Change list &lt;refcodes-security-alt&gt; (version 1.3.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-1.3.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-1.3.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-1.3.5/refcodes-security-alt-chaos/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-1.3.5/refcodes-security-alt-chaos/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`ChaosKeyTest.java`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-1.3.5/refcodes-security-alt-chaos/src/test/java/org/refcodes/security/alt/chaos/ChaosKeyTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`ChaosTest.java`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-1.3.5/refcodes-security-alt-chaos/src/test/java/org/refcodes/security/alt/chaos/ChaosTest.java) 

## Change list &lt;refcodes-security-ext&gt; (version 1.3.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-1.3.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-1.3.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-1.3.5/refcodes-security-ext-chaos/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-1.3.5/refcodes-security-ext-chaos/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`ChaosProviderTest.java`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-1.3.5/refcodes-security-ext-chaos/src/test/java/org/refcodes/security/ext/chaos/ChaosProviderTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-1.3.5/refcodes-security-ext-spring/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`TextDecrypterBeanTest.java`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-1.3.5/refcodes-security-ext-spring/src/test/java/org/refcodes/security/ext/spring/TextDecrypterBeanTest.java) 

## Change list &lt;refcodes-properties-ext&gt; (version 1.3.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.3.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.3.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.3.5/refcodes-properties-ext-cli/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.3.5/refcodes-properties-ext-cli/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`ArgsParserPropertiesTest.java`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.3.5/refcodes-properties-ext-cli/src/test/java/org/refcodes/configuration/ext/console/ArgsParserPropertiesTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.3.5/refcodes-properties-ext-obfuscation/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.3.5/refcodes-properties-ext-obfuscation/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`ObfuscationProperties.java`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.3.5/refcodes-properties-ext-obfuscation/src/main/java/org/refcodes/configuration/ext/obfuscation/ObfuscationProperties.java) (see Javadoc at [`ObfuscationProperties.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties-ext-obfuscation/1.3.5/org/refcodes/configuration/ext/obfuscation/ObfuscationProperties.html))
* \[<span style="color:green">MODIFIED</span>\] [`ObfuscationResourceProperties.java`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.3.5/refcodes-properties-ext-obfuscation/src/main/java/org/refcodes/configuration/ext/obfuscation/ObfuscationResourceProperties.java) (see Javadoc at [`ObfuscationResourceProperties.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties-ext-obfuscation/1.3.5/org/refcodes/configuration/ext/obfuscation/ObfuscationResourceProperties.html))
* \[<span style="color:green">MODIFIED</span>\] [`ObfuscationPropertiesBuilderTest.java`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.3.5/refcodes-properties-ext-obfuscation/src/test/java/org/refcodes/configuration/ext/obfuscation/ObfuscationPropertiesBuilderTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`ObfuscationPropertiesTest.java`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.3.5/refcodes-properties-ext-obfuscation/src/test/java/org/refcodes/configuration/ext/obfuscation/ObfuscationPropertiesTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.3.5/refcodes-properties-ext-observer/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.3.5/refcodes-properties-ext-observer/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`AbstractObservableResourcePropertiesTest.java`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.3.5/refcodes-properties-ext-observer/src/test/java/org/refcodes/configuration/ext/observer/AbstractObservableResourcePropertiesTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`ObservablePropertiesSugarTest.java`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.3.5/refcodes-properties-ext-observer/src/test/java/org/refcodes/configuration/ext/observer/ObservablePropertiesSugarTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.3.5/refcodes-properties-ext-runtime/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.3.5/refcodes-properties-ext-runtime/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`RuntimePropertiesImpl.java`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.3.5/refcodes-properties-ext-runtime/src/main/java/org/refcodes/configuration/ext/runtime/RuntimePropertiesImpl.java) (see Javadoc at [`RuntimePropertiesImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties-ext-runtime/1.3.5/org/refcodes/configuration/ext/runtime/RuntimePropertiesImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`ApplicationProperties.java`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.3.5/refcodes-properties-ext-runtime/src/main/java/org/refcodes/configuration/ext/runtime/ApplicationProperties.java) (see Javadoc at [`ApplicationProperties.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties-ext-runtime/1.3.5/org/refcodes/configuration/ext/runtime/ApplicationProperties.html))
* \[<span style="color:green">MODIFIED</span>\] [`RuntimePropertiesSugarTest.java`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.3.5/refcodes-properties-ext-runtime/src/test/java/org/refcodes/configuration/ext/runtime/RuntimePropertiesSugarTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`RuntimePropertiesTest.java`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.3.5/refcodes-properties-ext-runtime/src/test/java/org/refcodes/configuration/ext/runtime/RuntimePropertiesTest.java) 

## Change list &lt;refcodes-logger&gt; (version 1.3.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger/src/refcodes-logger-1.3.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger/src/refcodes-logger-1.3.5/README.md) 

## Change list &lt;refcodes-logger-alt&gt; (version 1.3.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.3.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.3.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.3.5/refcodes-logger-alt-async/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.3.5/refcodes-logger-alt-async/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.3.5/refcodes-logger-alt-console/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.3.5/refcodes-logger-alt-console/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.3.5/refcodes-logger-alt-io/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.3.5/refcodes-logger-alt-io/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.3.5/refcodes-logger-alt-simpledb/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.3.5/refcodes-logger-alt-slf4j/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.3.5/refcodes-logger-alt-spring/pom.xml) 

## Change list &lt;refcodes-logger-ext&gt; (version 1.3.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-1.3.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-1.3.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-1.3.5/refcodes-logger-ext-slf4j/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-1.3.5/refcodes-logger-ext-slf4j/README.md) 

## Change list &lt;refcodes-graphical-ext&gt; (version 1.3.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-1.3.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-1.3.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-1.3.5/refcodes-graphical-ext-javafx/pom.xml) 

## Change list &lt;refcodes-checkerboard&gt; (version 1.3.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-1.3.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-1.3.5/README.md) 

## Change list &lt;refcodes-checkerboard-alt&gt; (version 1.3.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-alt/src/refcodes-checkerboard-alt-1.3.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-alt/src/refcodes-checkerboard-alt-1.3.5/refcodes-checkerboard-alt-javafx/pom.xml) 

## Change list &lt;refcodes-checkerboard-ext&gt; (version 1.3.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-ext/src/refcodes-checkerboard-ext-1.3.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-checkerboard-ext/src/refcodes-checkerboard-ext-1.3.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-ext/src/refcodes-checkerboard-ext-1.3.5/refcodes-checkerboard-ext-javafx-boulderdash/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-ext/src/refcodes-checkerboard-ext-1.3.5/refcodes-checkerboard-ext-javafx-chess/pom.xml) 

## Change list &lt;refcodes-boulderdash&gt; (version 1.3.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-boulderdash/src/refcodes-boulderdash-1.3.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-boulderdash/src/refcodes-boulderdash-1.3.5/README.md) 

## Change list &lt;refcodes-net&gt; (version 1.3.5)

* \[<span style="color:blue">ADDED</span>\] [`OauthTokenImpl.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.3.5/src/main/java/org/refcodes/net/OauthTokenImpl.java) (see Javadoc at [`OauthTokenImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/1.3.5/org/refcodes/net/OauthTokenImpl.html))
* \[<span style="color:red">DELETED</span>\] `OauthRequestField.java`
* \[<span style="color:red">DELETED</span>\] `OauthRequest.java`
* \[<span style="color:red">DELETED</span>\] `OAuthTokenImpl.java`
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.3.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`GrantType.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.3.5/src/main/java/org/refcodes/net/GrantType.java) (see Javadoc at [`GrantType.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/1.3.5/org/refcodes/net/GrantType.html))
* \[<span style="color:green">MODIFIED</span>\] [`HeaderField.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.3.5/src/main/java/org/refcodes/net/HeaderField.java) (see Javadoc at [`HeaderField.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/1.3.5/org/refcodes/net/HeaderField.html))
* \[<span style="color:green">MODIFIED</span>\] [`HttpBodyMap.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.3.5/src/main/java/org/refcodes/net/HttpBodyMap.java) (see Javadoc at [`HttpBodyMap.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/1.3.5/org/refcodes/net/HttpBodyMap.html))
* \[<span style="color:green">MODIFIED</span>\] [`HttpFields.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.3.5/src/main/java/org/refcodes/net/HttpFields.java) (see Javadoc at [`HttpFields.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/1.3.5/org/refcodes/net/HttpFields.html))
* \[<span style="color:green">MODIFIED</span>\] [`JsonMediaTypeFactory.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.3.5/src/main/java/org/refcodes/net/JsonMediaTypeFactory.java) (see Javadoc at [`JsonMediaTypeFactory.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/1.3.5/org/refcodes/net/JsonMediaTypeFactory.html))
* \[<span style="color:green">MODIFIED</span>\] [`MediaType.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.3.5/src/main/java/org/refcodes/net/MediaType.java) (see Javadoc at [`MediaType.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/1.3.5/org/refcodes/net/MediaType.html))
* \[<span style="color:green">MODIFIED</span>\] [`OauthField.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.3.5/src/main/java/org/refcodes/net/OauthField.java) (see Javadoc at [`OauthField.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/1.3.5/org/refcodes/net/OauthField.html))
* \[<span style="color:green">MODIFIED</span>\] [`Url.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.3.5/src/main/java/org/refcodes/net/Url.java) (see Javadoc at [`Url.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/1.3.5/org/refcodes/net/Url.html))
* \[<span style="color:green">MODIFIED</span>\] [`XmlMediaTypeFactory.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.3.5/src/main/java/org/refcodes/net/XmlMediaTypeFactory.java) (see Javadoc at [`XmlMediaTypeFactory.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/1.3.5/org/refcodes/net/XmlMediaTypeFactory.html))
* \[<span style="color:green">MODIFIED</span>\] [`AbstractMediaFactoryTest.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.3.5/src/test/java/org/refcodes/net/AbstractMediaFactoryTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`ContentTypeTest.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.3.5/src/test/java/org/refcodes/net/ContentTypeTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`FormFieldsTest.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.3.5/src/test/java/org/refcodes/net/FormFieldsTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`HeaderFieldsTest.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.3.5/src/test/java/org/refcodes/net/HeaderFieldsTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`HttpStatusCodeTest.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.3.5/src/test/java/org/refcodes/net/HttpStatusCodeTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`IpAddressTest.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.3.5/src/test/java/org/refcodes/net/IpAddressTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`MediaFormFactoryTest.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.3.5/src/test/java/org/refcodes/net/MediaFormFactoryTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`MediaJsonFactoryTest.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.3.5/src/test/java/org/refcodes/net/MediaJsonFactoryTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`OauthTokenTest.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.3.5/src/test/java/org/refcodes/net/OauthTokenTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`PortManagerTest.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.3.5/src/test/java/org/refcodes/net/PortManagerTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`RequestHeaderFieldsTest.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.3.5/src/test/java/org/refcodes/net/RequestHeaderFieldsTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`ResponseHeaderFieldsTest.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.3.5/src/test/java/org/refcodes/net/ResponseHeaderFieldsTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`UrlTest.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.3.5/src/test/java/org/refcodes/net/UrlTest.java) 

## Change list &lt;refcodes-rest&gt; (version 1.3.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.3.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.3.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`HttpRestClientImpl.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.3.5/src/main/java/org/refcodes/rest/HttpRestClientImpl.java) (see Javadoc at [`HttpRestClientImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/1.3.5/org/refcodes/rest/HttpRestClientImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`OauthTokenHandler.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.3.5/src/main/java/org/refcodes/rest/OauthTokenHandler.java) (see Javadoc at [`OauthTokenHandler.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/1.3.5/org/refcodes/rest/OauthTokenHandler.html))
* \[<span style="color:green">MODIFIED</span>\] [`HttpRestClientTest.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.3.5/src/test/java/org/refcodes/rest/HttpRestClientTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`HttpRestServerTest.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.3.5/src/test/java/org/refcodes/rest/HttpRestServerTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`HttpRestSugarTest.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.3.5/src/test/java/org/refcodes/rest/HttpRestSugarTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`HttpsRestServerTest.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.3.5/src/test/java/org/refcodes/rest/HttpsRestServerTest.java) 

## Change list &lt;refcodes-daemon&gt; (version 1.3.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-daemon/src/refcodes-daemon-1.3.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-daemon/src/refcodes-daemon-1.3.5/README.md) 

## Change list &lt;refcodes-eventbus&gt; (version 1.3.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-eventbus/src/refcodes-eventbus-1.3.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-eventbus/src/refcodes-eventbus-1.3.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`DisptachStrategyTest.java`](https://bitbucket.org/refcodes/refcodes-eventbus/src/refcodes-eventbus-1.3.5/src/test/java/org/refcodes/eventbus/DisptachStrategyTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`EventBusTest.java`](https://bitbucket.org/refcodes/refcodes-eventbus/src/refcodes-eventbus-1.3.5/src/test/java/org/refcodes/eventbus/EventBusTest.java) 

## Change list &lt;refcodes-eventbus-ext&gt; (version 1.3.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-1.3.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-1.3.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-1.3.5/refcodes-eventbus-ext-application/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`ApplicationBusTest.java`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-1.3.5/refcodes-eventbus-ext-application/src/test/java/org/refcodes/eventbus/ext/application/ApplicationBusTest.java) 

## Change list &lt;refcodes-filesystem&gt; (version 1.3.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-filesystem/src/refcodes-filesystem-1.3.5/pom.xml) 

## Change list &lt;refcodes-filesystem-alt&gt; (version 1.3.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-filesystem-alt/src/refcodes-filesystem-alt-1.3.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-filesystem-alt/src/refcodes-filesystem-alt-1.3.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-filesystem-alt/src/refcodes-filesystem-alt-1.3.5/refcodes-filesystem-alt-s3/pom.xml) 

## Change list &lt;refcodes-forwardsecrecy&gt; (version 1.3.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy/src/refcodes-forwardsecrecy-1.3.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy/src/refcodes-forwardsecrecy-1.3.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`ForwardSecrecyTest.java`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy/src/refcodes-forwardsecrecy-1.3.5/src/test/java/org/refcodes/forwardsecrecy/ForwardSecrecyTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`ForwardSecrecyUtilityTest.java`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy/src/refcodes-forwardsecrecy-1.3.5/src/test/java/org/refcodes/forwardsecrecy/ForwardSecrecyUtilityTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`ForwardSecrecyWorkshopTest.java`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy/src/refcodes-forwardsecrecy-1.3.5/src/test/java/org/refcodes/forwardsecrecy/ForwardSecrecyWorkshopTest.java) 

## Change list &lt;refcodes-forwardsecrecy-alt&gt; (version 1.3.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-1.3.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-1.3.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-1.3.5/refcodes-forwardsecrecy-alt-filesystem/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`BasicForwardSecrecyFileSystemTest.java`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-1.3.5/refcodes-forwardsecrecy-alt-filesystem/src/test/java/org/refcodes/forwardsecrecy/alt/filesystem/BasicForwardSecrecyFileSystemTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`ForwardSecrecyFileSystemTest.java`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-1.3.5/refcodes-forwardsecrecy-alt-filesystem/src/test/java/org/refcodes/forwardsecrecy/alt/filesystem/ForwardSecrecyFileSystemTest.java) 

## Change list &lt;refcodes-io-ext&gt; (version 1.3.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-1.3.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-1.3.5/refcodes-io-ext-observable/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-1.3.5/refcodes-io-ext-observable/README.md) 

## Change list &lt;refcodes-interceptor&gt; (version 1.3.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-interceptor/src/refcodes-interceptor-1.3.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-interceptor/src/refcodes-interceptor-1.3.5/README.md) 

## Change list &lt;refcodes-jobbus&gt; (version 1.3.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-jobbus/src/refcodes-jobbus-1.3.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-jobbus/src/refcodes-jobbus-1.3.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`JobBusImplTest.java`](https://bitbucket.org/refcodes/refcodes-jobbus/src/refcodes-jobbus-1.3.5/src/test/java/org/refcodes/jobbus/JobBusImplTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`JobBusProxyImplTest.java`](https://bitbucket.org/refcodes/refcodes-jobbus/src/refcodes-jobbus-1.3.5/src/test/java/org/refcodes/jobbus/JobBusProxyImplTest.java) 

## Change list &lt;refcodes-rest-ext&gt; (version 1.3.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-1.3.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-1.3.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-1.3.5/refcodes-rest-ext-eureka/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-1.3.5/refcodes-rest-ext-eureka/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`EurekaDiscoverySidecarTest.java`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-1.3.5/refcodes-rest-ext-eureka/src/test/java/org/refcodes/rest/ext/eureka/EurekaDiscoverySidecarTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`EurekaRestClientTest.java`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-1.3.5/refcodes-rest-ext-eureka/src/test/java/org/refcodes/rest/ext/eureka/EurekaRestClientTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`EurekaRestServerTest.java`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-1.3.5/refcodes-rest-ext-eureka/src/test/java/org/refcodes/rest/ext/eureka/EurekaRestServerTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`EurekaServerDescriptorTest.java`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-1.3.5/refcodes-rest-ext-eureka/src/test/java/org/refcodes/rest/ext/eureka/EurekaServerDescriptorTest.java) 

## Change list &lt;refcodes-remoting&gt; (version 1.3.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-remoting/src/refcodes-remoting-1.3.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-remoting/src/refcodes-remoting-1.3.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`AbstractRemote.java`](https://bitbucket.org/refcodes/refcodes-remoting/src/refcodes-remoting-1.3.5/src/main/java/org/refcodes/remoting/AbstractRemote.java) (see Javadoc at [`AbstractRemote.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-remoting/1.3.5/org/refcodes/remoting/AbstractRemote.html))
* \[<span style="color:green">MODIFIED</span>\] [`RemoteClientImpl.java`](https://bitbucket.org/refcodes/refcodes-remoting/src/refcodes-remoting-1.3.5/src/main/java/org/refcodes/remoting/RemoteClientImpl.java) (see Javadoc at [`RemoteClientImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-remoting/1.3.5/org/refcodes/remoting/RemoteClientImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`IoStreamRemoteTest.java`](https://bitbucket.org/refcodes/refcodes-remoting/src/refcodes-remoting-1.3.5/src/test/java/org/refcodes/remoting/IoStreamRemoteTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`LoopbackRemoteTest.java`](https://bitbucket.org/refcodes/refcodes-remoting/src/refcodes-remoting-1.3.5/src/test/java/org/refcodes/remoting/LoopbackRemoteTest.java) 

## Change list &lt;refcodes-remoting-ext&gt; (version 1.3.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-1.3.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-1.3.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-1.3.5/refcodes-remoting-ext-observer/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`ObservableLoopbackRemoteTest.java`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-1.3.5/refcodes-remoting-ext-observer/src/test/java/org/refcodes/remoting/ext/observer/ObservableLoopbackRemoteTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`ObservableSocketRemoteTest.java`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-1.3.5/refcodes-remoting-ext-observer/src/test/java/org/refcodes/remoting/ext/observer/ObservableSocketRemoteTest.java) 

## Change list &lt;refcodes-servicebus&gt; (version 1.3.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-servicebus/src/refcodes-servicebus-1.3.5/pom.xml) 

## Change list &lt;refcodes-servicebus-alt&gt; (version 1.3.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-servicebus-alt/src/refcodes-servicebus-alt-1.3.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-servicebus-alt/src/refcodes-servicebus-alt-1.3.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-servicebus-alt/src/refcodes-servicebus-alt-1.3.5/refcodes-servicebus-alt-spring/pom.xml) 

## Change list &lt;refcodes-tabular-alt&gt; (version 1.3.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-tabular-alt/src/refcodes-tabular-alt-1.3.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-tabular-alt/src/refcodes-tabular-alt-1.3.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-tabular-alt/src/refcodes-tabular-alt-1.3.5/refcodes-tabular-alt-forwardsecrecy/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`ForwardsSecrecyColumnTest.java`](https://bitbucket.org/refcodes/refcodes-tabular-alt/src/refcodes-tabular-alt-1.3.5/refcodes-tabular-alt-forwardsecrecy/src/test/java/org/refcodes/tabular/alt/forwardsecrecy/ForwardsSecrecyColumnTest.java) 
