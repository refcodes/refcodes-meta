> This change list has been auto-generated on `triton` by `steiner` with `changelist-all.sh` on the 2024-02-29 at 20:01:11.

## Change list &lt;refcodes-licensing&gt; (version 3.3.5)

* \[<span style="color:green">MODIFIED</span>\] [`.gitignore`](https://bitbucket.org/refcodes/refcodes-licensing/src/refcodes-licensing-3.3.5/.gitignore) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-licensing/src/refcodes-licensing-3.3.5/pom.xml) 

## Change list &lt;refcodes-parent&gt; (version 3.3.5)

* \[<span style="color:green">MODIFIED</span>\] [`.gitignore`](https://bitbucket.org/refcodes/refcodes-parent/src/refcodes-parent-3.3.5/.gitignore) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-parent/src/refcodes-parent-3.3.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-parent/src/refcodes-parent-3.3.5/README.md) 

## Change list &lt;refcodes-time&gt; (version 3.3.5)

* \[<span style="color:green">MODIFIED</span>\] [`.gitignore`](https://bitbucket.org/refcodes/refcodes-time/src/refcodes-time-3.3.5/.gitignore) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-time/src/refcodes-time-3.3.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-time/src/refcodes-time-3.3.5/README.md) 

## Change list &lt;refcodes-mixin&gt; (version 3.3.5)

* \[<span style="color:blue">ADDED</span>\] [`Reconcilable.java`](https://bitbucket.org/refcodes/refcodes-mixin/src/refcodes-mixin-3.3.5/src/main/java/org/refcodes/mixin/Reconcilable.java) (see Javadoc at [`Reconcilable.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-mixin/3.3.5/org.refcodes.mixin/org/refcodes/mixin/Reconcilable.html))
* \[<span style="color:green">MODIFIED</span>\] [`.gitignore`](https://bitbucket.org/refcodes/refcodes-mixin/src/refcodes-mixin-3.3.5/.gitignore) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-mixin/src/refcodes-mixin-3.3.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-mixin/src/refcodes-mixin-3.3.5/README.md) 

## Change list &lt;refcodes-data&gt; (version 3.3.5)

* \[<span style="color:green">MODIFIED</span>\] [`.gitignore`](https://bitbucket.org/refcodes/refcodes-data/src/refcodes-data-3.3.5/.gitignore) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data/src/refcodes-data-3.3.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data/src/refcodes-data-3.3.5/README.md) 

## Change list &lt;refcodes-exception&gt; (version 3.3.5)

* \[<span style="color:green">MODIFIED</span>\] [`.gitignore`](https://bitbucket.org/refcodes/refcodes-exception/src/refcodes-exception-3.3.5/.gitignore) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-exception/src/refcodes-exception-3.3.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-exception/src/refcodes-exception-3.3.5/README.md) 

## Change list &lt;refcodes-factory&gt; (version 3.3.5)

* \[<span style="color:green">MODIFIED</span>\] [`.gitignore`](https://bitbucket.org/refcodes/refcodes-factory/src/refcodes-factory-3.3.5/.gitignore) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-factory/src/refcodes-factory-3.3.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-factory/src/refcodes-factory-3.3.5/README.md) 

## Change list &lt;refcodes-factory-alt&gt; (version 3.3.5)

* \[<span style="color:green">MODIFIED</span>\] [`.gitignore`](https://bitbucket.org/refcodes/refcodes-factory-alt/src/refcodes-factory-alt-3.3.5/.gitignore) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-factory-alt/src/refcodes-factory-alt-3.3.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-factory-alt/src/refcodes-factory-alt-3.3.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-factory-alt/src/refcodes-factory-alt-3.3.5/refcodes-factory-alt-spring/pom.xml) 

## Change list &lt;refcodes-controlflow&gt; (version 3.3.5)

* \[<span style="color:green">MODIFIED</span>\] [`.gitignore`](https://bitbucket.org/refcodes/refcodes-controlflow/src/refcodes-controlflow-3.3.5/.gitignore) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-controlflow/src/refcodes-controlflow-3.3.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-controlflow/src/refcodes-controlflow-3.3.5/README.md) 

## Change list &lt;refcodes-numerical&gt; (version 3.3.5)

* \[<span style="color:green">MODIFIED</span>\] [`.gitignore`](https://bitbucket.org/refcodes/refcodes-numerical/src/refcodes-numerical-3.3.5/.gitignore) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-numerical/src/refcodes-numerical-3.3.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-numerical/src/refcodes-numerical-3.3.5/README.md) 

## Change list &lt;refcodes-generator&gt; (version 3.3.5)

* \[<span style="color:green">MODIFIED</span>\] [`.gitignore`](https://bitbucket.org/refcodes/refcodes-generator/src/refcodes-generator-3.3.5/.gitignore) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-generator/src/refcodes-generator-3.3.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-generator/src/refcodes-generator-3.3.5/README.md) 

## Change list &lt;refcodes-schema&gt; (version 3.3.5)

* \[<span style="color:green">MODIFIED</span>\] [`.gitignore`](https://bitbucket.org/refcodes/refcodes-schema/src/refcodes-schema-3.3.5/.gitignore) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-schema/src/refcodes-schema-3.3.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-schema/src/refcodes-schema-3.3.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`JsonVisitor.java`](https://bitbucket.org/refcodes/refcodes-schema/src/refcodes-schema-3.3.5/src/main/java/org/refcodes/schema/JsonVisitor.java) (see Javadoc at [`JsonVisitor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-schema/3.3.5/org.refcodes.schema/org/refcodes/schema/JsonVisitor.html))
* \[<span style="color:green">MODIFIED</span>\] [`XmlVisitor.java`](https://bitbucket.org/refcodes/refcodes-schema/src/refcodes-schema-3.3.5/src/main/java/org/refcodes/schema/XmlVisitor.java) (see Javadoc at [`XmlVisitor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-schema/3.3.5/org.refcodes.schema/org/refcodes/schema/XmlVisitor.html))

## Change list &lt;refcodes-matcher&gt; (version 3.3.5)

* \[<span style="color:green">MODIFIED</span>\] [`.gitignore`](https://bitbucket.org/refcodes/refcodes-matcher/src/refcodes-matcher-3.3.5/.gitignore) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-matcher/src/refcodes-matcher-3.3.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-matcher/src/refcodes-matcher-3.3.5/README.md) 

## Change list &lt;refcodes-struct&gt; (version 3.3.5)

* \[<span style="color:green">MODIFIED</span>\] [`.gitignore`](https://bitbucket.org/refcodes/refcodes-struct/src/refcodes-struct-3.3.5/.gitignore) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-struct/src/refcodes-struct-3.3.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-struct/src/refcodes-struct-3.3.5/README.md) 

## Change list &lt;refcodes-runtime&gt; (version 3.3.5)

* \[<span style="color:green">MODIFIED</span>\] [`.gitignore`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-3.3.5/.gitignore) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-3.3.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-3.3.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`Execution.java`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-3.3.5/src/main/java/org/refcodes/runtime/Execution.java) (see Javadoc at [`Execution.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-runtime/3.3.5/org.refcodes.runtime/org/refcodes/runtime/Execution.html))

## Change list &lt;refcodes-struct-ext&gt; (version 3.3.5)

* \[<span style="color:green">MODIFIED</span>\] [`.gitignore`](https://bitbucket.org/refcodes/refcodes-struct-ext/src/refcodes-struct-ext-3.3.5/.gitignore) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-struct-ext/src/refcodes-struct-ext-3.3.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-struct-ext/src/refcodes-struct-ext-3.3.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-struct-ext/src/refcodes-struct-ext-3.3.5/refcodes-struct-ext-factory/pom.xml) 

## Change list &lt;refcodes-component&gt; (version 3.3.5)

* \[<span style="color:green">MODIFIED</span>\] [`.gitignore`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.3.5/.gitignore) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.3.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.3.5/README.md) 

## Change list &lt;refcodes-data-ext&gt; (version 3.3.5)

* \[<span style="color:green">MODIFIED</span>\] [`.gitignore`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-3.3.5/.gitignore) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-3.3.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-3.3.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-3.3.5/refcodes-data-ext-checkers/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-3.3.5/refcodes-data-ext-checkers/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-3.3.5/refcodes-data-ext-chess/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-3.3.5/refcodes-data-ext-chess/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-3.3.5/refcodes-data-ext-corporate/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-3.3.5/refcodes-data-ext-corporate/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-3.3.5/refcodes-data-ext-symbols/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-3.3.5/refcodes-data-ext-symbols/README.md) 

## Change list &lt;refcodes-graphical&gt; (version 3.3.5)

* \[<span style="color:green">MODIFIED</span>\] [`.gitignore`](https://bitbucket.org/refcodes/refcodes-graphical/src/refcodes-graphical-3.3.5/.gitignore) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-graphical/src/refcodes-graphical-3.3.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-graphical/src/refcodes-graphical-3.3.5/README.md) 

## Change list &lt;refcodes-textual&gt; (version 3.3.5)

* \[<span style="color:green">MODIFIED</span>\] [`.gitignore`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-3.3.5/.gitignore) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-3.3.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-3.3.5/README.md) 

## Change list &lt;refcodes-criteria&gt; (version 3.3.5)

* \[<span style="color:green">MODIFIED</span>\] [`.gitignore`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-3.3.5/.gitignore) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-3.3.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-3.3.5/README.md) 

## Change list &lt;refcodes-io&gt; (version 3.3.5)

* \[<span style="color:green">MODIFIED</span>\] [`.gitignore`](https://bitbucket.org/refcodes/refcodes-io/src/refcodes-io-3.3.5/.gitignore) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-io/src/refcodes-io-3.3.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-io/src/refcodes-io-3.3.5/README.md) 

## Change list &lt;refcodes-tabular&gt; (version 3.3.5)

* \[<span style="color:green">MODIFIED</span>\] [`.gitignore`](https://bitbucket.org/refcodes/refcodes-tabular/src/refcodes-tabular-3.3.5/.gitignore) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-tabular/src/refcodes-tabular-3.3.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-tabular/src/refcodes-tabular-3.3.5/README.md) 

## Change list &lt;refcodes-observer&gt; (version 3.3.5)

* \[<span style="color:green">MODIFIED</span>\] [`.gitignore`](https://bitbucket.org/refcodes/refcodes-observer/src/refcodes-observer-3.3.5/.gitignore) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-observer/src/refcodes-observer-3.3.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-observer/src/refcodes-observer-3.3.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`ObservableObserver.java`](https://bitbucket.org/refcodes/refcodes-observer/src/refcodes-observer-3.3.5/src/main/java/org/refcodes/observer/ObservableObserver.java) (see Javadoc at [`ObservableObserver.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-observer/3.3.5/org.refcodes.observer/org/refcodes/observer/ObservableObserver.html))

## Change list &lt;refcodes-command&gt; (version 3.3.5)

* \[<span style="color:green">MODIFIED</span>\] [`.gitignore`](https://bitbucket.org/refcodes/refcodes-command/src/refcodes-command-3.3.5/.gitignore) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-command/src/refcodes-command-3.3.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-command/src/refcodes-command-3.3.5/README.md) 

## Change list &lt;refcodes-cli&gt; (version 3.3.5)

* \[<span style="color:green">MODIFIED</span>\] [`.gitignore`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.3.5/.gitignore) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.3.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.3.5/README.md) 

## Change list &lt;refcodes-audio&gt; (version 3.3.5)

* \[<span style="color:green">MODIFIED</span>\] [`.gitignore`](https://bitbucket.org/refcodes/refcodes-audio/src/refcodes-audio-3.3.5/.gitignore) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-audio/src/refcodes-audio-3.3.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-audio/src/refcodes-audio-3.3.5/README.md) 

## Change list &lt;refcodes-codec&gt; (version 3.3.5)

* \[<span style="color:green">MODIFIED</span>\] [`.gitignore`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-3.3.5/.gitignore) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-3.3.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-3.3.5/README.md) 

## Change list &lt;refcodes-component-ext&gt; (version 3.3.5)

* \[<span style="color:green">MODIFIED</span>\] [`.gitignore`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-3.3.5/.gitignore) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-3.3.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-3.3.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-3.3.5/refcodes-component-ext-observer/pom.xml) 

## Change list &lt;refcodes-properties&gt; (version 3.3.5)

* \[<span style="color:green">MODIFIED</span>\] [`.gitignore`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-3.3.5/.gitignore) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-3.3.5/pom.xml) 

## Change list &lt;refcodes-security&gt; (version 3.3.5)

* \[<span style="color:green">MODIFIED</span>\] [`.gitignore`](https://bitbucket.org/refcodes/refcodes-security/src/refcodes-security-3.3.5/.gitignore) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security/src/refcodes-security-3.3.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security/src/refcodes-security-3.3.5/README.md) 

## Change list &lt;refcodes-security-alt&gt; (version 3.3.5)

* \[<span style="color:green">MODIFIED</span>\] [`.gitignore`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-3.3.5/.gitignore) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-3.3.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-3.3.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-3.3.5/refcodes-security-alt-chaos/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-3.3.5/refcodes-security-alt-chaos/README.md) 

## Change list &lt;refcodes-security-ext&gt; (version 3.3.5)

* \[<span style="color:green">MODIFIED</span>\] [`.gitignore`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-3.3.5/.gitignore) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-3.3.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-3.3.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-3.3.5/refcodes-security-ext-chaos/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-3.3.5/refcodes-security-ext-chaos/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-3.3.5/refcodes-security-ext-spring/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-3.3.5/refcodes-security-ext-spring/README.md) 

## Change list &lt;refcodes-properties-ext&gt; (version 3.3.5)

* \[<span style="color:green">MODIFIED</span>\] [`.gitignore`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.3.5/.gitignore) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.3.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.3.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.3.5/refcodes-properties-ext-application/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.3.5/refcodes-properties-ext-application/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.3.5/refcodes-properties-ext-cli/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.3.5/refcodes-properties-ext-cli/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.3.5/refcodes-properties-ext-obfuscation/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.3.5/refcodes-properties-ext-obfuscation/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.3.5/refcodes-properties-ext-observer/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.3.5/refcodes-properties-ext-observer/README.md) 

## Change list &lt;refcodes-logger&gt; (version 3.3.5)

* \[<span style="color:green">MODIFIED</span>\] [`.gitignore`](https://bitbucket.org/refcodes/refcodes-logger/src/refcodes-logger-3.3.5/.gitignore) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger/src/refcodes-logger-3.3.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger/src/refcodes-logger-3.3.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`RuntimeLoggerFactorySingleton.java`](https://bitbucket.org/refcodes/refcodes-logger/src/refcodes-logger-3.3.5/src/main/java/org/refcodes/logger/RuntimeLoggerFactorySingleton.java) (see Javadoc at [`RuntimeLoggerFactorySingleton.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-logger/3.3.5/org.refcodes.logger/org/refcodes/logger/RuntimeLoggerFactorySingleton.html))
* \[<span style="color:green">MODIFIED</span>\] [`RuntimeLoggerImpl.java`](https://bitbucket.org/refcodes/refcodes-logger/src/refcodes-logger-3.3.5/src/main/java/org/refcodes/logger/RuntimeLoggerImpl.java) (see Javadoc at [`RuntimeLoggerImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-logger/3.3.5/org.refcodes.logger/org/refcodes/logger/RuntimeLoggerImpl.html))

## Change list &lt;refcodes-logger-alt&gt; (version 3.3.5)

* \[<span style="color:blue">ADDED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.3.5/refcodes-logger-alt-jul/pom.xml) 
* \[<span style="color:blue">ADDED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.3.5/refcodes-logger-alt-jul/README.md) 
* \[<span style="color:blue">ADDED</span>\] [`module-info.java`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.3.5/refcodes-logger-alt-jul/src/main/java/module-info.java) (see Javadoc at [`module-info.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-logger-alt-jul/3.3.5//module-info.html))
* \[<span style="color:blue">ADDED</span>\] [`JulLogger.java`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.3.5/refcodes-logger-alt-jul/src/main/java/org/refcodes/logger/alt/jul/JulLogger.java) (see Javadoc at [`JulLogger.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-logger-alt-jul/3.3.5/org.refcodes.logger.alt.jul/org/refcodes/logger/alt/jul/JulLogger.html))
* \[<span style="color:blue">ADDED</span>\] [`package-info.java`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.3.5/refcodes-logger-alt-jul/src/main/java/org/refcodes/logger/alt/jul/package-info.java) 
* \[<span style="color:blue">ADDED</span>\] [`runtimelogger.ini`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.3.5/refcodes-logger-alt-jul/src/main/resources/runtimelogger.ini) 
* \[<span style="color:blue">ADDED</span>\] [`JulRuntimeLoggerTest.java`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.3.5/refcodes-logger-alt-jul/src/test/java/org/refcodes/logger/alt/jul/JulRuntimeLoggerTest.java) 
* \[<span style="color:blue">ADDED</span>\] [`RuntimeLoggerSingletonTest.java`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.3.5/refcodes-logger-alt-jul/src/test/java/org/refcodes/logger/alt/jul/RuntimeLoggerSingletonTest.java) 
* \[<span style="color:blue">ADDED</span>\] [`log4j.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.3.5/refcodes-logger-alt-jul/src/test/resources/log4j.xml) 
* \[<span style="color:blue">ADDED</span>\] [`runtimelogger.ini`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.3.5/refcodes-logger-alt-jul/src/test/resources/runtimelogger.ini) 
* \[<span style="color:green">MODIFIED</span>\] [`.gitignore`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.3.5/.gitignore) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.3.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.3.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.3.5/refcodes-logger-alt-async/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.3.5/refcodes-logger-alt-async/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.3.5/refcodes-logger-alt-console/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.3.5/refcodes-logger-alt-console/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`ConsoleLogger.java`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.3.5/refcodes-logger-alt-console/src/main/java/org/refcodes/logger/alt/console/ConsoleLogger.java) (see Javadoc at [`ConsoleLogger.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-logger-alt-console/3.3.5/org.refcodes.logger.alt.console/org/refcodes/logger/alt/console/ConsoleLogger.html))
* \[<span style="color:green">MODIFIED</span>\] [`JulLoggerHandlerTest.java`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.3.5/refcodes-logger-alt-console/src/test/java/org/refcodes/logger/alt/console/JulLoggerHandlerTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.3.5/refcodes-logger-alt-io/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.3.5/refcodes-logger-alt-io/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`IoLogger.java`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.3.5/refcodes-logger-alt-io/src/main/java/org/refcodes/logger/alt/io/IoLogger.java) (see Javadoc at [`IoLogger.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-logger-alt-io/3.3.5/org.refcodes.logger.alt.io/org/refcodes/logger/alt/io/IoLogger.html))
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.3.5/refcodes-logger-alt-simpledb/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.3.5/refcodes-logger-alt-simpledb/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.3.5/refcodes-logger-alt-slf4j-legacy/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`Slf4jLogger.java`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.3.5/refcodes-logger-alt-slf4j-legacy/src/main/java/org/refcodes/logger/alt/slf4j/legacy/Slf4jLogger.java) (see Javadoc at [`Slf4jLogger.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-logger-alt-slf4j-legacy/3.3.5/org.refcodes.logger.alt.slf4j.legacy/org/refcodes/logger/alt/slf4j/legacy/Slf4jLogger.html))
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.3.5/refcodes-logger-alt-slf4j/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`Slf4jLogger.java`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.3.5/refcodes-logger-alt-slf4j/src/main/java/org/refcodes/logger/alt/slf4j/Slf4jLogger.java) (see Javadoc at [`Slf4jLogger.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-logger-alt-slf4j/3.3.5/org.refcodes.logger.alt.slf4j/org/refcodes/logger/alt/slf4j/Slf4jLogger.html))
* \[<span style="color:green">MODIFIED</span>\] [`Slf4jRuntimeLogger.java`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.3.5/refcodes-logger-alt-slf4j/src/main/java/org/refcodes/logger/alt/slf4j/Slf4jRuntimeLogger.java) (see Javadoc at [`Slf4jRuntimeLogger.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-logger-alt-slf4j/3.3.5/org.refcodes.logger.alt.slf4j/org/refcodes/logger/alt/slf4j/Slf4jRuntimeLogger.html))
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.3.5/refcodes-logger-alt-spring/pom.xml) 

## Change list &lt;refcodes-logger-ext&gt; (version 3.3.5)

* \[<span style="color:green">MODIFIED</span>\] [`.gitignore`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-3.3.5/.gitignore) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-3.3.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-3.3.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-3.3.5/refcodes-logger-ext-slf4j-legacy/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-3.3.5/refcodes-logger-ext-slf4j-legacy/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-3.3.5/refcodes-logger-ext-slf4j/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-3.3.5/refcodes-logger-ext-slf4j/README.md) 

## Change list &lt;refcodes-graphical-ext&gt; (version 3.3.5)

* \[<span style="color:green">MODIFIED</span>\] [`.gitignore`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-3.3.5/.gitignore) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-3.3.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-3.3.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-3.3.5/refcodes-graphical-ext-javafx/pom.xml) 

## Change list &lt;refcodes-checkerboard&gt; (version 3.3.5)

* \[<span style="color:blue">ADDED</span>\] [`DirectionAccessor.java`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-3.3.5/src/main/java/org/refcodes/checkerboard/DirectionAccessor.java) (see Javadoc at [`DirectionAccessor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-checkerboard/3.3.5/org.refcodes.checkerboard/org/refcodes/checkerboard/DirectionAccessor.html))
* \[<span style="color:green">MODIFIED</span>\] [`.gitignore`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-3.3.5/.gitignore) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-3.3.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-3.3.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`AbstractCheckerboard.java`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-3.3.5/src/main/java/org/refcodes/checkerboard/AbstractCheckerboard.java) (see Javadoc at [`AbstractCheckerboard.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-checkerboard/3.3.5/org.refcodes.checkerboard/org/refcodes/checkerboard/AbstractCheckerboard.html))
* \[<span style="color:green">MODIFIED</span>\] [`CheckerboardObserver.java`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-3.3.5/src/main/java/org/refcodes/checkerboard/CheckerboardObserver.java) (see Javadoc at [`CheckerboardObserver.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-checkerboard/3.3.5/org.refcodes.checkerboard/org/refcodes/checkerboard/CheckerboardObserver.html))
* \[<span style="color:green">MODIFIED</span>\] [`CheckerboardViewer.java`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-3.3.5/src/main/java/org/refcodes/checkerboard/CheckerboardViewer.java) (see Javadoc at [`CheckerboardViewer.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-checkerboard/3.3.5/org.refcodes.checkerboard/org/refcodes/checkerboard/CheckerboardViewer.html))
* \[<span style="color:green">MODIFIED</span>\] [`ConsoleCheckerboardViewer.java`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-3.3.5/src/main/java/org/refcodes/checkerboard/ConsoleCheckerboardViewer.java) (see Javadoc at [`ConsoleCheckerboardViewer.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-checkerboard/3.3.5/org.refcodes.checkerboard/org/refcodes/checkerboard/ConsoleCheckerboardViewer.html))
* \[<span style="color:green">MODIFIED</span>\] [`Direction.java`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-3.3.5/src/main/java/org/refcodes/checkerboard/Direction.java) (see Javadoc at [`Direction.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-checkerboard/3.3.5/org.refcodes.checkerboard/org/refcodes/checkerboard/Direction.html))

## Change list &lt;refcodes-checkerboard-alt&gt; (version 3.3.5)

* \[<span style="color:green">MODIFIED</span>\] [`.gitignore`](https://bitbucket.org/refcodes/refcodes-checkerboard-alt/src/refcodes-checkerboard-alt-3.3.5/.gitignore) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-alt/src/refcodes-checkerboard-alt-3.3.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-alt/src/refcodes-checkerboard-alt-3.3.5/refcodes-checkerboard-alt-javafx/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`FxCheckerboardViewer.java`](https://bitbucket.org/refcodes/refcodes-checkerboard-alt/src/refcodes-checkerboard-alt-3.3.5/refcodes-checkerboard-alt-javafx/src/main/java/org/refcodes/checkerboard/alt/javafx/FxCheckerboardViewer.java) (see Javadoc at [`FxCheckerboardViewer.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-checkerboard-alt-javafx/3.3.5/org.refcodes.checkerboard.alt.javafx/org/refcodes/checkerboard/alt/javafx/FxCheckerboardViewer.html))

## Change list &lt;refcodes-net&gt; (version 3.3.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-3.3.5/pom.xml) 

## Change list &lt;refcodes-web&gt; (version 3.3.5)

* \[<span style="color:green">MODIFIED</span>\] [`.gitignore`](https://bitbucket.org/refcodes/refcodes-web/src/refcodes-web-3.3.5/.gitignore) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-web/src/refcodes-web-3.3.5/pom.xml) 

## Change list &lt;refcodes-rest&gt; (version 3.3.5)

* \[<span style="color:green">MODIFIED</span>\] [`.gitignore`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-3.3.5/.gitignore) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-3.3.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-3.3.5/README.md) 

## Change list &lt;refcodes-hal&gt; (version 3.3.5)

* \[<span style="color:green">MODIFIED</span>\] [`.gitignore`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-3.3.5/.gitignore) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-3.3.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-3.3.5/README.md) 

## Change list &lt;refcodes-eventbus&gt; (version 3.3.5)

* \[<span style="color:green">MODIFIED</span>\] [`.gitignore`](https://bitbucket.org/refcodes/refcodes-eventbus/src/refcodes-eventbus-3.3.5/.gitignore) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-eventbus/src/refcodes-eventbus-3.3.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-eventbus/src/refcodes-eventbus-3.3.5/README.md) 

## Change list &lt;refcodes-eventbus-ext&gt; (version 3.3.5)

* \[<span style="color:green">MODIFIED</span>\] [`.gitignore`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-3.3.5/.gitignore) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-3.3.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-3.3.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-3.3.5/refcodes-eventbus-ext-application/pom.xml) 

## Change list &lt;refcodes-decoupling&gt; (version 3.3.5)

* \[<span style="color:green">MODIFIED</span>\] [`.gitignore`](https://bitbucket.org/refcodes/refcodes-decoupling/src/refcodes-decoupling-3.3.5/.gitignore) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-decoupling/src/refcodes-decoupling-3.3.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-decoupling/src/refcodes-decoupling-3.3.5/README.md) 

## Change list &lt;refcodes-decoupling-ext&gt; (version 3.3.5)

* \[<span style="color:green">MODIFIED</span>\] [`.gitignore`](https://bitbucket.org/refcodes/refcodes-decoupling-ext/src/refcodes-decoupling-ext-3.3.5/.gitignore) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-decoupling-ext/src/refcodes-decoupling-ext-3.3.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-decoupling-ext/src/refcodes-decoupling-ext-3.3.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-decoupling-ext/src/refcodes-decoupling-ext-3.3.5/refcodes-decoupling-ext-application/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-decoupling-ext/src/refcodes-decoupling-ext-3.3.5/refcodes-decoupling-ext-application/README.md) 

## Change list &lt;refcodes-filesystem&gt; (version 3.3.5)

* \[<span style="color:green">MODIFIED</span>\] [`.gitignore`](https://bitbucket.org/refcodes/refcodes-filesystem/src/refcodes-filesystem-3.3.5/.gitignore) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-filesystem/src/refcodes-filesystem-3.3.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-filesystem/src/refcodes-filesystem-3.3.5/README.md) 

## Change list &lt;refcodes-filesystem-alt&gt; (version 3.3.5)

* \[<span style="color:green">MODIFIED</span>\] [`.gitignore`](https://bitbucket.org/refcodes/refcodes-filesystem-alt/src/refcodes-filesystem-alt-3.3.5/.gitignore) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-filesystem-alt/src/refcodes-filesystem-alt-3.3.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-filesystem-alt/src/refcodes-filesystem-alt-3.3.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-filesystem-alt/src/refcodes-filesystem-alt-3.3.5/refcodes-filesystem-alt-s3/pom.xml) 

## Change list &lt;refcodes-forwardsecrecy&gt; (version 3.3.5)

* \[<span style="color:green">MODIFIED</span>\] [`.gitignore`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy/src/refcodes-forwardsecrecy-3.3.5/.gitignore) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy/src/refcodes-forwardsecrecy-3.3.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy/src/refcodes-forwardsecrecy-3.3.5/README.md) 

## Change list &lt;refcodes-forwardsecrecy-alt&gt; (version 3.3.5)

* \[<span style="color:green">MODIFIED</span>\] [`.gitignore`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-3.3.5/.gitignore) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-3.3.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-3.3.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-3.3.5/refcodes-forwardsecrecy-alt-filesystem/pom.xml) 

## Change list &lt;refcodes-io-ext&gt; (version 3.3.5)

* \[<span style="color:green">MODIFIED</span>\] [`.gitignore`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-3.3.5/.gitignore) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-3.3.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-3.3.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-3.3.5/refcodes-io-ext-observer/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-3.3.5/refcodes-io-ext-observer/README.md) 

## Change list &lt;refcodes-jobbus&gt; (version 3.3.5)

* \[<span style="color:green">MODIFIED</span>\] [`.gitignore`](https://bitbucket.org/refcodes/refcodes-jobbus/src/refcodes-jobbus-3.3.5/.gitignore) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-jobbus/src/refcodes-jobbus-3.3.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-jobbus/src/refcodes-jobbus-3.3.5/README.md) 

## Change list &lt;refcodes-rest-ext&gt; (version 3.3.5)

* \[<span style="color:green">MODIFIED</span>\] [`.gitignore`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-3.3.5/.gitignore) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-3.3.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-3.3.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-3.3.5/refcodes-rest-ext-eureka/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-3.3.5/refcodes-rest-ext-eureka/README.md) 

## Change list &lt;refcodes-remoting&gt; (version 3.3.5)

* \[<span style="color:green">MODIFIED</span>\] [`.gitignore`](https://bitbucket.org/refcodes/refcodes-remoting/src/refcodes-remoting-3.3.5/.gitignore) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-remoting/src/refcodes-remoting-3.3.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-remoting/src/refcodes-remoting-3.3.5/README.md) 

## Change list &lt;refcodes-remoting-ext&gt; (version 3.3.5)

* \[<span style="color:green">MODIFIED</span>\] [`.gitignore`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-3.3.5/.gitignore) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-3.3.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-3.3.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-3.3.5/refcodes-remoting-ext-observer/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-3.3.5/refcodes-remoting-ext-observer/README.md) 

## Change list &lt;refcodes-serial&gt; (version 3.3.5)

* \[<span style="color:green">MODIFIED</span>\] [`.gitignore`](https://bitbucket.org/refcodes/refcodes-serial/src/refcodes-serial-3.3.5/.gitignore) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial/src/refcodes-serial-3.3.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-serial/src/refcodes-serial-3.3.5/README.md) 

## Change list &lt;refcodes-serial-alt&gt; (version 3.3.5)

* \[<span style="color:green">MODIFIED</span>\] [`.gitignore`](https://bitbucket.org/refcodes/refcodes-serial-alt/src/refcodes-serial-alt-3.3.5/.gitignore) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial-alt/src/refcodes-serial-alt-3.3.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-serial-alt/src/refcodes-serial-alt-3.3.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial-alt/src/refcodes-serial-alt-3.3.5/refcodes-serial-alt-tty/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-serial-alt/src/refcodes-serial-alt-3.3.5/refcodes-serial-alt-tty/README.md) 

## Change list &lt;refcodes-serial-ext&gt; (version 3.3.5)

* \[<span style="color:green">MODIFIED</span>\] [`.gitignore`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-3.3.5/.gitignore) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-3.3.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-3.3.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-3.3.5/refcodes-serial-ext-handshake/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-3.3.5/refcodes-serial-ext-handshake/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-3.3.5/refcodes-serial-ext-observer/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-3.3.5/refcodes-serial-ext-observer/README.md) 

## Change list &lt;refcodes-p2p&gt; (version 3.3.5)

* \[<span style="color:green">MODIFIED</span>\] [`.gitignore`](https://bitbucket.org/refcodes/refcodes-p2p/src/refcodes-p2p-3.3.5/.gitignore) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-p2p/src/refcodes-p2p-3.3.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-p2p/src/refcodes-p2p-3.3.5/README.md) 

## Change list &lt;refcodes-p2p-alt&gt; (version 3.3.5)

* \[<span style="color:green">MODIFIED</span>\] [`.gitignore`](https://bitbucket.org/refcodes/refcodes-p2p-alt/src/refcodes-p2p-alt-3.3.5/.gitignore) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-p2p-alt/src/refcodes-p2p-alt-3.3.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-p2p-alt/src/refcodes-p2p-alt-3.3.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-p2p-alt/src/refcodes-p2p-alt-3.3.5/refcodes-p2p-alt-serial/pom.xml) 

## Change list &lt;refcodes-p2p-ext&gt; (version 3.3.5)

* \[<span style="color:green">MODIFIED</span>\] [`.gitignore`](https://bitbucket.org/refcodes/refcodes-p2p-ext/src/refcodes-p2p-ext-3.3.5/.gitignore) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-p2p-ext/src/refcodes-p2p-ext-3.3.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-p2p-ext/src/refcodes-p2p-ext-3.3.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-p2p-ext/src/refcodes-p2p-ext-3.3.5/refcodes-p2p-ext-observer/pom.xml) 

## Change list &lt;refcodes-tabular-alt&gt; (version 3.3.5)

* \[<span style="color:green">MODIFIED</span>\] [`.gitignore`](https://bitbucket.org/refcodes/refcodes-tabular-alt/src/refcodes-tabular-alt-3.3.5/.gitignore) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-tabular-alt/src/refcodes-tabular-alt-3.3.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-tabular-alt/src/refcodes-tabular-alt-3.3.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-tabular-alt/src/refcodes-tabular-alt-3.3.5/refcodes-tabular-alt-forwardsecrecy/pom.xml) 

## Change list &lt;refcodes-archetype&gt; (version 3.3.5)

* \[<span style="color:blue">ADDED</span>\] [`CtxAppHelper.java`](https://bitbucket.org/refcodes/refcodes-archetype/src/refcodes-archetype-3.3.5/src/main/java/org/refcodes/archetype/CtxAppHelper.java) (see Javadoc at [`CtxAppHelper.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-archetype/3.3.5/org.refcodes.archetype/org/refcodes/archetype/CtxAppHelper.html))
* \[<span style="color:green">MODIFIED</span>\] [`.gitignore`](https://bitbucket.org/refcodes/refcodes-archetype/src/refcodes-archetype-3.3.5/.gitignore) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype/src/refcodes-archetype-3.3.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype/src/refcodes-archetype-3.3.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`AppHelper.java`](https://bitbucket.org/refcodes/refcodes-archetype/src/refcodes-archetype-3.3.5/src/main/java/org/refcodes/archetype/AppHelper.java) (see Javadoc at [`AppHelper.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-archetype/3.3.5/org.refcodes.archetype/org/refcodes/archetype/AppHelper.html))
* \[<span style="color:green">MODIFIED</span>\] [`CliHelper.java`](https://bitbucket.org/refcodes/refcodes-archetype/src/refcodes-archetype-3.3.5/src/main/java/org/refcodes/archetype/CliHelper.java) (see Javadoc at [`CliHelper.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-archetype/3.3.5/org.refcodes.archetype/org/refcodes/archetype/CliHelper.html))
* \[<span style="color:green">MODIFIED</span>\] [`CtxHelper.java`](https://bitbucket.org/refcodes/refcodes-archetype/src/refcodes-archetype-3.3.5/src/main/java/org/refcodes/archetype/CtxHelper.java) (see Javadoc at [`CtxHelper.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-archetype/3.3.5/org.refcodes.archetype/org/refcodes/archetype/CtxHelper.html))

## Change list &lt;refcodes-archetype-alt&gt; (version 3.3.5)

* \[<span style="color:green">MODIFIED</span>\] [`.gitignore`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.5/.gitignore) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.5/refcodes-archetype-alt-c2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.5/refcodes-archetype-alt-c2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`gitignore`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.5/refcodes-archetype-alt-c2/src/main/resources/archetype-resources/gitignore) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.5/refcodes-archetype-alt-c2/src/main/resources/archetype-resources/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.5/refcodes-archetype-alt-cli/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.5/refcodes-archetype-alt-cli/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`gitignore`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.5/refcodes-archetype-alt-cli/src/main/resources/archetype-resources/gitignore) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.5/refcodes-archetype-alt-cli/src/main/resources/archetype-resources/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.5/refcodes-archetype-alt-csv/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.5/refcodes-archetype-alt-csv/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`gitignore`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.5/refcodes-archetype-alt-csv/src/main/resources/archetype-resources/gitignore) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.5/refcodes-archetype-alt-csv/src/main/resources/archetype-resources/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.5/refcodes-archetype-alt-decoupling/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.5/refcodes-archetype-alt-decoupling/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`gitignore`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.5/refcodes-archetype-alt-decoupling/src/main/resources/archetype-resources/gitignore) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.5/refcodes-archetype-alt-decoupling/src/main/resources/archetype-resources/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.5/refcodes-archetype-alt-eventbus/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.5/refcodes-archetype-alt-eventbus/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`gitignore`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.5/refcodes-archetype-alt-eventbus/src/main/resources/archetype-resources/gitignore) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.5/refcodes-archetype-alt-eventbus/src/main/resources/archetype-resources/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.5/refcodes-archetype-alt-filter/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.5/refcodes-archetype-alt-filter/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`gitignore`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.5/refcodes-archetype-alt-filter/src/main/resources/archetype-resources/gitignore) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.5/refcodes-archetype-alt-filter/src/main/resources/archetype-resources/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`gitignore`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.5/refcodes-archetype-alt-rest/gitignore) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.5/refcodes-archetype-alt-rest/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.5/refcodes-archetype-alt-rest/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`gitignore`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.5/refcodes-archetype-alt-rest/src/main/resources/archetype-resources/gitignore) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.3.5/refcodes-archetype-alt-rest/src/main/resources/archetype-resources/pom.xml) 
