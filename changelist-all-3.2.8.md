> This change list has been auto-generated on `triton` by `steiner` with `changelist-all.sh` on the 2023-08-20 at 14:36:14.

## Change list &lt;refcodes-licensing&gt; (version 3.2.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-licensing/src/refcodes-licensing-3.2.8/pom.xml) 

## Change list &lt;refcodes-parent&gt; (version 3.2.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-parent/src/refcodes-parent-3.2.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-parent/src/refcodes-parent-3.2.8/README.md) 

## Change list &lt;refcodes-time&gt; (version 3.2.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-time/src/refcodes-time-3.2.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-time/src/refcodes-time-3.2.8/README.md) 

## Change list &lt;refcodes-mixin&gt; (version 3.2.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-mixin/src/refcodes-mixin-3.2.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-mixin/src/refcodes-mixin-3.2.8/README.md) 

## Change list &lt;refcodes-data&gt; (version 3.2.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data/src/refcodes-data-3.2.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data/src/refcodes-data-3.2.8/README.md) 

## Change list &lt;refcodes-exception&gt; (version 3.2.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-exception/src/refcodes-exception-3.2.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-exception/src/refcodes-exception-3.2.8/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`Trap.java`](https://bitbucket.org/refcodes/refcodes-exception/src/refcodes-exception-3.2.8/src/main/java/org/refcodes/exception/Trap.java) (see Javadoc at [`Trap.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-exception/3.2.8/org.refcodes.exception/org/refcodes/exception/Trap.html))
* \[<span style="color:green">MODIFIED</span>\] [`MessageTest.java`](https://bitbucket.org/refcodes/refcodes-exception/src/refcodes-exception-3.2.8/src/test/java/org/refcodes/exception/MessageTest.java) 

## Change list &lt;refcodes-factory&gt; (version 3.2.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-factory/src/refcodes-factory-3.2.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-factory/src/refcodes-factory-3.2.8/README.md) 

## Change list &lt;refcodes-factory-alt&gt; (version 3.2.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-factory-alt/src/refcodes-factory-alt-3.2.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-factory-alt/src/refcodes-factory-alt-3.2.8/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-factory-alt/src/refcodes-factory-alt-3.2.8/refcodes-factory-alt-spring/pom.xml) 

## Change list &lt;refcodes-controlflow&gt; (version 3.2.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-controlflow/src/refcodes-controlflow-3.2.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-controlflow/src/refcodes-controlflow-3.2.8/README.md) 

## Change list &lt;refcodes-numerical&gt; (version 3.2.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-numerical/src/refcodes-numerical-3.2.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-numerical/src/refcodes-numerical-3.2.8/README.md) 

## Change list &lt;refcodes-generator&gt; (version 3.2.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-generator/src/refcodes-generator-3.2.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-generator/src/refcodes-generator-3.2.8/README.md) 

## Change list &lt;refcodes-matcher&gt; (version 3.2.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-matcher/src/refcodes-matcher-3.2.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-matcher/src/refcodes-matcher-3.2.8/README.md) 

## Change list &lt;refcodes-struct&gt; (version 3.2.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-struct/src/refcodes-struct-3.2.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-struct/src/refcodes-struct-3.2.8/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`TypeUtility.java`](https://bitbucket.org/refcodes/refcodes-struct/src/refcodes-struct-3.2.8/src/main/java/org/refcodes/struct/TypeUtility.java) (see Javadoc at [`TypeUtility.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-struct/3.2.8/org.refcodes.struct/org/refcodes/struct/TypeUtility.html))

## Change list &lt;refcodes-runtime&gt; (version 3.2.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-3.2.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-3.2.8/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`SystemContext.java`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-3.2.8/src/main/java/org/refcodes/runtime/SystemContext.java) (see Javadoc at [`SystemContext.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-runtime/3.2.8/org.refcodes.runtime/org/refcodes/runtime/SystemContext.html))
* \[<span style="color:green">MODIFIED</span>\] [`SystemContextTest.java`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-3.2.8/src/test/java/org/refcodes/runtime/SystemContextTest.java) 

## Change list &lt;refcodes-struct-ext&gt; (version 3.2.8)

* \[<span style="color:blue">ADDED</span>\] [`module-info.java`](https://bitbucket.org/refcodes/refcodes-struct-ext/src/refcodes-struct-ext-3.2.8/refcodes-struct-ext-factory/src/main/java/module-info.java) (see Javadoc at [`module-info.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-struct-ext-factory/3.2.8//module-info.html))
* \[<span style="color:blue">ADDED</span>\] [`AbstractCanonicalMapFactory.java`](https://bitbucket.org/refcodes/refcodes-struct-ext/src/refcodes-struct-ext-3.2.8/refcodes-struct-ext-factory/src/main/java/org/refcodes/struct/ext/factory/AbstractCanonicalMapFactory.java) (see Javadoc at [`AbstractCanonicalMapFactory.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-struct-ext-factory/3.2.8/org.refcodes.struct.ext.factory/org/refcodes/struct/ext/factory/AbstractCanonicalMapFactory.html))
* \[<span style="color:blue">ADDED</span>\] [`CanonicalMapFactory.java`](https://bitbucket.org/refcodes/refcodes-struct-ext/src/refcodes-struct-ext-3.2.8/refcodes-struct-ext-factory/src/main/java/org/refcodes/struct/ext/factory/CanonicalMapFactory.java) (see Javadoc at [`CanonicalMapFactory.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-struct-ext-factory/3.2.8/org.refcodes.struct.ext.factory/org/refcodes/struct/ext/factory/CanonicalMapFactory.html))
* \[<span style="color:blue">ADDED</span>\] [`HtmlCanonicalMapFactory.java`](https://bitbucket.org/refcodes/refcodes-struct-ext/src/refcodes-struct-ext-3.2.8/refcodes-struct-ext-factory/src/main/java/org/refcodes/struct/ext/factory/HtmlCanonicalMapFactory.java) (see Javadoc at [`HtmlCanonicalMapFactory.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-struct-ext-factory/3.2.8/org.refcodes.struct.ext.factory/org/refcodes/struct/ext/factory/HtmlCanonicalMapFactory.html))
* \[<span style="color:blue">ADDED</span>\] [`HtmlCanonicalMapFactorySingleton.java`](https://bitbucket.org/refcodes/refcodes-struct-ext/src/refcodes-struct-ext-3.2.8/refcodes-struct-ext-factory/src/main/java/org/refcodes/struct/ext/factory/HtmlCanonicalMapFactorySingleton.java) (see Javadoc at [`HtmlCanonicalMapFactorySingleton.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-struct-ext-factory/3.2.8/org.refcodes.struct.ext.factory/org/refcodes/struct/ext/factory/HtmlCanonicalMapFactorySingleton.html))
* \[<span style="color:blue">ADDED</span>\] [`JavaCanonicalMapFactory.java`](https://bitbucket.org/refcodes/refcodes-struct-ext/src/refcodes-struct-ext-3.2.8/refcodes-struct-ext-factory/src/main/java/org/refcodes/struct/ext/factory/JavaCanonicalMapFactory.java) (see Javadoc at [`JavaCanonicalMapFactory.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-struct-ext-factory/3.2.8/org.refcodes.struct.ext.factory/org/refcodes/struct/ext/factory/JavaCanonicalMapFactory.html))
* \[<span style="color:blue">ADDED</span>\] [`JavaCanonicalMapFactorySingleton.java`](https://bitbucket.org/refcodes/refcodes-struct-ext/src/refcodes-struct-ext-3.2.8/refcodes-struct-ext-factory/src/main/java/org/refcodes/struct/ext/factory/JavaCanonicalMapFactorySingleton.java) (see Javadoc at [`JavaCanonicalMapFactorySingleton.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-struct-ext-factory/3.2.8/org.refcodes.struct.ext.factory/org/refcodes/struct/ext/factory/JavaCanonicalMapFactorySingleton.html))
* \[<span style="color:blue">ADDED</span>\] [`JsonCanonicalMapFactory.java`](https://bitbucket.org/refcodes/refcodes-struct-ext/src/refcodes-struct-ext-3.2.8/refcodes-struct-ext-factory/src/main/java/org/refcodes/struct/ext/factory/JsonCanonicalMapFactory.java) (see Javadoc at [`JsonCanonicalMapFactory.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-struct-ext-factory/3.2.8/org.refcodes.struct.ext.factory/org/refcodes/struct/ext/factory/JsonCanonicalMapFactory.html))
* \[<span style="color:blue">ADDED</span>\] [`JsonCanonicalMapFactorySingleton.java`](https://bitbucket.org/refcodes/refcodes-struct-ext/src/refcodes-struct-ext-3.2.8/refcodes-struct-ext-factory/src/main/java/org/refcodes/struct/ext/factory/JsonCanonicalMapFactorySingleton.java) (see Javadoc at [`JsonCanonicalMapFactorySingleton.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-struct-ext-factory/3.2.8/org.refcodes.struct.ext.factory/org/refcodes/struct/ext/factory/JsonCanonicalMapFactorySingleton.html))
* \[<span style="color:blue">ADDED</span>\] [`TomlCanonicalMapFactory.java`](https://bitbucket.org/refcodes/refcodes-struct-ext/src/refcodes-struct-ext-3.2.8/refcodes-struct-ext-factory/src/main/java/org/refcodes/struct/ext/factory/TomlCanonicalMapFactory.java) (see Javadoc at [`TomlCanonicalMapFactory.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-struct-ext-factory/3.2.8/org.refcodes.struct.ext.factory/org/refcodes/struct/ext/factory/TomlCanonicalMapFactory.html))
* \[<span style="color:blue">ADDED</span>\] [`TomlCanonicalMapFactorySingleton.java`](https://bitbucket.org/refcodes/refcodes-struct-ext/src/refcodes-struct-ext-3.2.8/refcodes-struct-ext-factory/src/main/java/org/refcodes/struct/ext/factory/TomlCanonicalMapFactorySingleton.java) (see Javadoc at [`TomlCanonicalMapFactorySingleton.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-struct-ext-factory/3.2.8/org.refcodes.struct.ext.factory/org/refcodes/struct/ext/factory/TomlCanonicalMapFactorySingleton.html))
* \[<span style="color:blue">ADDED</span>\] [`XmlCanonicalMapFactory.java`](https://bitbucket.org/refcodes/refcodes-struct-ext/src/refcodes-struct-ext-3.2.8/refcodes-struct-ext-factory/src/main/java/org/refcodes/struct/ext/factory/XmlCanonicalMapFactory.java) (see Javadoc at [`XmlCanonicalMapFactory.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-struct-ext-factory/3.2.8/org.refcodes.struct.ext.factory/org/refcodes/struct/ext/factory/XmlCanonicalMapFactory.html))
* \[<span style="color:blue">ADDED</span>\] [`XmlCanonicalMapFactorySingleton.java`](https://bitbucket.org/refcodes/refcodes-struct-ext/src/refcodes-struct-ext-3.2.8/refcodes-struct-ext-factory/src/main/java/org/refcodes/struct/ext/factory/XmlCanonicalMapFactorySingleton.java) (see Javadoc at [`XmlCanonicalMapFactorySingleton.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-struct-ext-factory/3.2.8/org.refcodes.struct.ext.factory/org/refcodes/struct/ext/factory/XmlCanonicalMapFactorySingleton.html))
* \[<span style="color:blue">ADDED</span>\] [`YamlCanonicalMapFactory.java`](https://bitbucket.org/refcodes/refcodes-struct-ext/src/refcodes-struct-ext-3.2.8/refcodes-struct-ext-factory/src/main/java/org/refcodes/struct/ext/factory/YamlCanonicalMapFactory.java) (see Javadoc at [`YamlCanonicalMapFactory.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-struct-ext-factory/3.2.8/org.refcodes.struct.ext.factory/org/refcodes/struct/ext/factory/YamlCanonicalMapFactory.html))
* \[<span style="color:blue">ADDED</span>\] [`YamlCanonicalMapFactorySingleton.java`](https://bitbucket.org/refcodes/refcodes-struct-ext/src/refcodes-struct-ext-3.2.8/refcodes-struct-ext-factory/src/main/java/org/refcodes/struct/ext/factory/YamlCanonicalMapFactorySingleton.java) (see Javadoc at [`YamlCanonicalMapFactorySingleton.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-struct-ext-factory/3.2.8/org.refcodes.struct.ext.factory/org/refcodes/struct/ext/factory/YamlCanonicalMapFactorySingleton.html))
* \[<span style="color:blue">ADDED</span>\] [`AbstractCanonicalMapFactoryTest.java`](https://bitbucket.org/refcodes/refcodes-struct-ext/src/refcodes-struct-ext-3.2.8/refcodes-struct-ext-factory/src/test/java/org/refcodes/struct/ext/factory/AbstractCanonicalMapFactoryTest.java) 
* \[<span style="color:blue">ADDED</span>\] [`HtmlCanonicalMapFactoryTest.java`](https://bitbucket.org/refcodes/refcodes-struct-ext/src/refcodes-struct-ext-3.2.8/refcodes-struct-ext-factory/src/test/java/org/refcodes/struct/ext/factory/HtmlCanonicalMapFactoryTest.java) 
* \[<span style="color:blue">ADDED</span>\] [`JavaCanonicalMapFactoryTest.java`](https://bitbucket.org/refcodes/refcodes-struct-ext/src/refcodes-struct-ext-3.2.8/refcodes-struct-ext-factory/src/test/java/org/refcodes/struct/ext/factory/JavaCanonicalMapFactoryTest.java) 
* \[<span style="color:blue">ADDED</span>\] [`JsonCanonicalMapFactoryTest.java`](https://bitbucket.org/refcodes/refcodes-struct-ext/src/refcodes-struct-ext-3.2.8/refcodes-struct-ext-factory/src/test/java/org/refcodes/struct/ext/factory/JsonCanonicalMapFactoryTest.java) 
* \[<span style="color:blue">ADDED</span>\] [`TomlCanonicalMapFactoryTest.java`](https://bitbucket.org/refcodes/refcodes-struct-ext/src/refcodes-struct-ext-3.2.8/refcodes-struct-ext-factory/src/test/java/org/refcodes/struct/ext/factory/TomlCanonicalMapFactoryTest.java) 
* \[<span style="color:blue">ADDED</span>\] [`XmlCanonicalMapFactoryTest.java`](https://bitbucket.org/refcodes/refcodes-struct-ext/src/refcodes-struct-ext-3.2.8/refcodes-struct-ext-factory/src/test/java/org/refcodes/struct/ext/factory/XmlCanonicalMapFactoryTest.java) 
* \[<span style="color:blue">ADDED</span>\] [`YamlCanonicalMapFactoryTest.java`](https://bitbucket.org/refcodes/refcodes-struct-ext/src/refcodes-struct-ext-3.2.8/refcodes-struct-ext-factory/src/test/java/org/refcodes/struct/ext/factory/YamlCanonicalMapFactoryTest.java) 
* \[<span style="color:blue">ADDED</span>\] [`test.ini`](https://bitbucket.org/refcodes/refcodes-struct-ext/src/refcodes-struct-ext-3.2.8/refcodes-struct-ext-factory/src/test/resources/test.ini) 
* \[<span style="color:blue">ADDED</span>\] [`test.json`](https://bitbucket.org/refcodes/refcodes-struct-ext/src/refcodes-struct-ext-3.2.8/refcodes-struct-ext-factory/src/test/resources/test.json) 
* \[<span style="color:blue">ADDED</span>\] [`test.txt`](https://bitbucket.org/refcodes/refcodes-struct-ext/src/refcodes-struct-ext-3.2.8/refcodes-struct-ext-factory/src/test/resources/test.txt) 
* \[<span style="color:blue">ADDED</span>\] [`test.xml`](https://bitbucket.org/refcodes/refcodes-struct-ext/src/refcodes-struct-ext-3.2.8/refcodes-struct-ext-factory/src/test/resources/test.xml) 
* \[<span style="color:red">DELETED</span>\] `module-info.java.off`
* \[<span style="color:red">DELETED</span>\] `AbstractCanonicalMapFactory.java`
* \[<span style="color:red">DELETED</span>\] `CanonicalMapFactory.java`
* \[<span style="color:red">DELETED</span>\] `JavaCanonicalMapFactory.java`
* \[<span style="color:red">DELETED</span>\] `JsonCanonicalMapFactory.java`
* \[<span style="color:red">DELETED</span>\] `TomlCanonicalMapFactory.java`
* \[<span style="color:red">DELETED</span>\] `XmlCanonicalMapFactory.java`
* \[<span style="color:red">DELETED</span>\] `YamlCanonicalMapFactory.java`
* \[<span style="color:red">DELETED</span>\] `JavaCanonicalMapFactoryTest.java`
* \[<span style="color:red">DELETED</span>\] `JsonCanonicalMapFactoryTest.java`
* \[<span style="color:red">DELETED</span>\] `XmlCanonicalMapFactoryTest.java`
* \[<span style="color:red">DELETED</span>\] `YamlCanonicalMapFactoryTest.java`
* \[<span style="color:green">MODIFIED</span>\] [`.gitignore`](https://bitbucket.org/refcodes/refcodes-struct-ext/src/refcodes-struct-ext-3.2.8/.gitignore) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-struct-ext/src/refcodes-struct-ext-3.2.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-struct-ext/src/refcodes-struct-ext-3.2.8/README.md) 

## Change list &lt;refcodes-component&gt; (version 3.2.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.2.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.2.8/README.md) 

## Change list &lt;refcodes-data-ext&gt; (version 3.2.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-3.2.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-3.2.8/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-3.2.8/refcodes-data-ext-checkers/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-3.2.8/refcodes-data-ext-checkers/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-3.2.8/refcodes-data-ext-chess/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-3.2.8/refcodes-data-ext-chess/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-3.2.8/refcodes-data-ext-corporate/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-3.2.8/refcodes-data-ext-corporate/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-3.2.8/refcodes-data-ext-symbols/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-3.2.8/refcodes-data-ext-symbols/README.md) 

## Change list &lt;refcodes-graphical&gt; (version 3.2.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-graphical/src/refcodes-graphical-3.2.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-graphical/src/refcodes-graphical-3.2.8/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`Viewport.java`](https://bitbucket.org/refcodes/refcodes-graphical/src/refcodes-graphical-3.2.8/src/main/java/org/refcodes/graphical/Viewport.java) (see Javadoc at [`Viewport.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-graphical/3.2.8/org.refcodes.graphical/org/refcodes/graphical/Viewport.html))

## Change list &lt;refcodes-textual&gt; (version 3.2.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-3.2.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-3.2.8/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`RandomTextGenerartor.java`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-3.2.8/src/main/java/org/refcodes/textual/RandomTextGenerartor.java) (see Javadoc at [`RandomTextGenerartor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-textual/3.2.8/org.refcodes.textual/org/refcodes/textual/RandomTextGenerartor.html))

## Change list &lt;refcodes-criteria&gt; (version 3.2.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-3.2.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-3.2.8/README.md) 

## Change list &lt;refcodes-io&gt; (version 3.2.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-io/src/refcodes-io-3.2.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-io/src/refcodes-io-3.2.8/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`FileUtility.java`](https://bitbucket.org/refcodes/refcodes-io/src/refcodes-io-3.2.8/src/main/java/org/refcodes/io/FileUtility.java) (see Javadoc at [`FileUtility.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-io/3.2.8/org.refcodes.io/org/refcodes/io/FileUtility.html))
* \[<span style="color:green">MODIFIED</span>\] [`AvailableInputStreamTest.java`](https://bitbucket.org/refcodes/refcodes-io/src/refcodes-io-3.2.8/src/test/java/org/refcodes/io/AvailableInputStreamTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`TimeoutInputStreamTest.java`](https://bitbucket.org/refcodes/refcodes-io/src/refcodes-io-3.2.8/src/test/java/org/refcodes/io/TimeoutInputStreamTest.java) 

## Change list &lt;refcodes-tabular&gt; (version 3.2.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-tabular/src/refcodes-tabular-3.2.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-tabular/src/refcodes-tabular-3.2.8/README.md) 

## Change list &lt;refcodes-observer&gt; (version 3.2.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-observer/src/refcodes-observer-3.2.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-observer/src/refcodes-observer-3.2.8/README.md) 

## Change list &lt;refcodes-command&gt; (version 3.2.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-command/src/refcodes-command-3.2.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-command/src/refcodes-command-3.2.8/README.md) 

## Change list &lt;refcodes-cli&gt; (version 3.2.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.2.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.2.8/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`ArgsParser.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.2.8/src/main/java/org/refcodes/cli/ArgsParser.java) (see Javadoc at [`ArgsParser.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/3.2.8/org.refcodes.cli/org/refcodes/cli/ArgsParser.html))
* \[<span style="color:green">MODIFIED</span>\] [`ArgsSyntaxException.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.2.8/src/main/java/org/refcodes/cli/ArgsSyntaxException.java) (see Javadoc at [`ArgsSyntaxException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/3.2.8/org.refcodes.cli/org/refcodes/cli/ArgsSyntaxException.html))
* \[<span style="color:green">MODIFIED</span>\] [`CliContext.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.2.8/src/main/java/org/refcodes/cli/CliContext.java) (see Javadoc at [`CliContext.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/3.2.8/org.refcodes.cli/org/refcodes/cli/CliContext.html))
* \[<span style="color:green">MODIFIED</span>\] [`Operand.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.2.8/src/main/java/org/refcodes/cli/Operand.java) (see Javadoc at [`Operand.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/3.2.8/org.refcodes.cli/org/refcodes/cli/Operand.html))
* \[<span style="color:green">MODIFIED</span>\] [`Synopsisable.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.2.8/src/main/java/org/refcodes/cli/Synopsisable.java) (see Javadoc at [`Synopsisable.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/3.2.8/org.refcodes.cli/org/refcodes/cli/Synopsisable.html))
* \[<span style="color:green">MODIFIED</span>\] [`ArgsParserTest.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.2.8/src/test/java/org/refcodes/cli/ArgsParserTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`ArgsSyntaxTest.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.2.8/src/test/java/org/refcodes/cli/ArgsSyntaxTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`LambdaTest.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.2.8/src/test/java/org/refcodes/cli/LambdaTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`MessageTest.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.2.8/src/test/java/org/refcodes/cli/MessageTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`SchemaTest.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.2.8/src/test/java/org/refcodes/cli/SchemaTest.java) 

## Change list &lt;refcodes-audio&gt; (version 3.2.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-audio/src/refcodes-audio-3.2.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-audio/src/refcodes-audio-3.2.8/README.md) 

## Change list &lt;refcodes-codec&gt; (version 3.2.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-3.2.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-3.2.8/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`BaseBuilderTest.java`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-3.2.8/src/test/java/org/refcodes/codec/BaseBuilderTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`BaseEncoderDecodeStreamTest.java`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-3.2.8/src/test/java/org/refcodes/codec/BaseEncoderDecodeStreamTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`InputStreamBaseDecoderTest.java`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-3.2.8/src/test/java/org/refcodes/codec/InputStreamBaseDecoderTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`OutputStreamBaseEncoderTest.java`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-3.2.8/src/test/java/org/refcodes/codec/OutputStreamBaseEncoderTest.java) 

## Change list &lt;refcodes-component-ext&gt; (version 3.2.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-3.2.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-3.2.8/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-3.2.8/refcodes-component-ext-observer/pom.xml) 

## Change list &lt;refcodes-properties&gt; (version 3.2.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-3.2.8/pom.xml) 

## Change list &lt;refcodes-security&gt; (version 3.2.8)

* \[<span style="color:blue">ADDED</span>\] [`PasswordTextDecrypter.java`](https://bitbucket.org/refcodes/refcodes-security/src/refcodes-security-3.2.8/src/main/java/org/refcodes/security/PasswordTextDecrypter.java) (see Javadoc at [`PasswordTextDecrypter.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-security/3.2.8/org.refcodes.security/org/refcodes/security/PasswordTextDecrypter.html))
* \[<span style="color:blue">ADDED</span>\] [`PasswordTextEncrypter.java`](https://bitbucket.org/refcodes/refcodes-security/src/refcodes-security-3.2.8/src/main/java/org/refcodes/security/PasswordTextEncrypter.java) (see Javadoc at [`PasswordTextEncrypter.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-security/3.2.8/org.refcodes.security/org/refcodes/security/PasswordTextEncrypter.html))
* \[<span style="color:blue">ADDED</span>\] [`PasswordTextEncryptionTest.java`](https://bitbucket.org/refcodes/refcodes-security/src/refcodes-security-3.2.8/src/test/java/org/refcodes/security/PasswordTextEncryptionTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security/src/refcodes-security-3.2.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security/src/refcodes-security-3.2.8/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`module-info.java`](https://bitbucket.org/refcodes/refcodes-security/src/refcodes-security-3.2.8/src/main/java/module-info.java) (see Javadoc at [`module-info.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-security/3.2.8/./module-info.html))
* \[<span style="color:green">MODIFIED</span>\] [`Decrypter.java`](https://bitbucket.org/refcodes/refcodes-security/src/refcodes-security-3.2.8/src/main/java/org/refcodes/security/Decrypter.java) (see Javadoc at [`Decrypter.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-security/3.2.8/org.refcodes.security/org/refcodes/security/Decrypter.html))
* \[<span style="color:green">MODIFIED</span>\] [`Encrypter.java`](https://bitbucket.org/refcodes/refcodes-security/src/refcodes-security-3.2.8/src/main/java/org/refcodes/security/Encrypter.java) (see Javadoc at [`Encrypter.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-security/3.2.8/org.refcodes.security/org/refcodes/security/Encrypter.html))
* \[<span style="color:green">MODIFIED</span>\] [`SecurityRuntimeException.java`](https://bitbucket.org/refcodes/refcodes-security/src/refcodes-security-3.2.8/src/main/java/org/refcodes/security/SecurityRuntimeException.java) (see Javadoc at [`SecurityRuntimeException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-security/3.2.8/org.refcodes.security/org/refcodes/security/SecurityRuntimeException.html))
* \[<span style="color:green">MODIFIED</span>\] [`StoreType.java`](https://bitbucket.org/refcodes/refcodes-security/src/refcodes-security-3.2.8/src/main/java/org/refcodes/security/StoreType.java) (see Javadoc at [`StoreType.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-security/3.2.8/org.refcodes.security/org/refcodes/security/StoreType.html))

## Change list &lt;refcodes-security-alt&gt; (version 3.2.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-3.2.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-3.2.8/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-3.2.8/refcodes-security-alt-chaos/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-3.2.8/refcodes-security-alt-chaos/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`ChaosEncryptionOutputStream.java`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-3.2.8/refcodes-security-alt-chaos/src/main/java/org/refcodes/security/alt/chaos/ChaosEncryptionOutputStream.java) (see Javadoc at [`ChaosEncryptionOutputStream.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-security-alt-chaos/3.2.8/org.refcodes.security.alt.chaos/org/refcodes/security/alt/chaos/ChaosEncryptionOutputStream.html))
* \[<span style="color:green">MODIFIED</span>\] [`ChaosKey.java`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-3.2.8/refcodes-security-alt-chaos/src/main/java/org/refcodes/security/alt/chaos/ChaosKey.java) (see Javadoc at [`ChaosKey.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-security-alt-chaos/3.2.8/org.refcodes.security.alt.chaos/org/refcodes/security/alt/chaos/ChaosKey.html))
* \[<span style="color:green">MODIFIED</span>\] [`ChaosEncryptionDecryptionStreamTest.java`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-3.2.8/refcodes-security-alt-chaos/src/test/java/org/refcodes/security/alt/chaos/ChaosEncryptionDecryptionStreamTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`ChaosKeyTest.java`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-3.2.8/refcodes-security-alt-chaos/src/test/java/org/refcodes/security/alt/chaos/ChaosKeyTest.java) 

## Change list &lt;refcodes-security-ext&gt; (version 3.2.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-3.2.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-3.2.8/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-3.2.8/refcodes-security-ext-chaos/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-3.2.8/refcodes-security-ext-chaos/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-3.2.8/refcodes-security-ext-spring/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-3.2.8/refcodes-security-ext-spring/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`module-info.java`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-3.2.8/refcodes-security-ext-spring/src/main/java/module-info.java) (see Javadoc at [`module-info.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-security-ext-spring/3.2.8//module-info.html))
* \[<span style="color:green">MODIFIED</span>\] [`TextObfuscaterUtility.java`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-3.2.8/refcodes-security-ext-spring/src/main/java/org/refcodes/security/ext/spring/TextObfuscaterUtility.java) (see Javadoc at [`TextObfuscaterUtility.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-security-ext-spring/3.2.8/org.refcodes.security.ext.spring/org/refcodes/security/ext/spring/TextObfuscaterUtility.html))
* \[<span style="color:green">MODIFIED</span>\] [`TextDecrypterBeanTest.java`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-3.2.8/refcodes-security-ext-spring/src/test/java/org/refcodes/security/ext/spring/TextDecrypterBeanTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`TextObfuscaterUtilityTest.java`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-3.2.8/refcodes-security-ext-spring/src/test/java/org/refcodes/security/ext/spring/TextObfuscaterUtilityTest.java) 

## Change list &lt;refcodes-properties-ext&gt; (version 3.2.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.2.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.2.8/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.2.8/refcodes-properties-ext-application/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.2.8/refcodes-properties-ext-application/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.2.8/refcodes-properties-ext-cli/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.2.8/refcodes-properties-ext-cli/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.2.8/refcodes-properties-ext-obfuscation/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.2.8/refcodes-properties-ext-obfuscation/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.2.8/refcodes-properties-ext-observer/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.2.8/refcodes-properties-ext-observer/README.md) 

## Change list &lt;refcodes-logger&gt; (version 3.2.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger/src/refcodes-logger-3.2.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger/src/refcodes-logger-3.2.8/README.md) 

## Change list &lt;refcodes-logger-alt&gt; (version 3.2.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.2.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.2.8/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.2.8/refcodes-logger-alt-async/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.2.8/refcodes-logger-alt-async/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.2.8/refcodes-logger-alt-console/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.2.8/refcodes-logger-alt-console/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.2.8/refcodes-logger-alt-io/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.2.8/refcodes-logger-alt-io/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.2.8/refcodes-logger-alt-simpledb/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.2.8/refcodes-logger-alt-simpledb/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.2.8/refcodes-logger-alt-slf4j-legacy/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.2.8/refcodes-logger-alt-slf4j/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.2.8/refcodes-logger-alt-spring/pom.xml) 

## Change list &lt;refcodes-logger-ext&gt; (version 3.2.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-3.2.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-3.2.8/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-3.2.8/refcodes-logger-ext-slf4j-legacy/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-3.2.8/refcodes-logger-ext-slf4j-legacy/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-3.2.8/refcodes-logger-ext-slf4j/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-3.2.8/refcodes-logger-ext-slf4j/README.md) 

## Change list &lt;refcodes-graphical-ext&gt; (version 3.2.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-3.2.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-3.2.8/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-3.2.8/refcodes-graphical-ext-javafx/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`FxPixGridBannerPanel.java`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-3.2.8/refcodes-graphical-ext-javafx/src/main/java/org/refcodes/graphical/ext/javafx/FxPixGridBannerPanel.java) (see Javadoc at [`FxPixGridBannerPanel.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-graphical-ext-javafx/3.2.8/org.refcodes.graphical.ext.javafx/org/refcodes/graphical/ext/javafx/FxPixGridBannerPanel.html))

## Change list &lt;refcodes-checkerboard&gt; (version 3.2.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-3.2.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-3.2.8/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`AbstractCheckerboardViewer.java`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-3.2.8/src/main/java/org/refcodes/checkerboard/AbstractCheckerboardViewer.java) (see Javadoc at [`AbstractCheckerboardViewer.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-checkerboard/3.2.8/org.refcodes.checkerboard/org/refcodes/checkerboard/AbstractCheckerboardViewer.html))
* \[<span style="color:green">MODIFIED</span>\] [`CheckerboardViewer.java`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-3.2.8/src/main/java/org/refcodes/checkerboard/CheckerboardViewer.java) (see Javadoc at [`CheckerboardViewer.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-checkerboard/3.2.8/org.refcodes.checkerboard/org/refcodes/checkerboard/CheckerboardViewer.html))

## Change list &lt;refcodes-checkerboard-alt&gt; (version 3.2.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-alt/src/refcodes-checkerboard-alt-3.2.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-alt/src/refcodes-checkerboard-alt-3.2.8/refcodes-checkerboard-alt-javafx/pom.xml) 

## Change list &lt;refcodes-net&gt; (version 3.2.8)

* \[<span style="color:green">MODIFIED</span>\] [`.classpath`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-3.2.8/.classpath) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-3.2.8/pom.xml) 

## Change list &lt;refcodes-web&gt; (version 3.2.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-web/src/refcodes-web-3.2.8/pom.xml) 

## Change list &lt;refcodes-rest&gt; (version 3.2.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-3.2.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-3.2.8/README.md) 

## Change list &lt;refcodes-hal&gt; (version 3.2.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-3.2.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-3.2.8/README.md) 

## Change list &lt;refcodes-eventbus&gt; (version 3.2.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-eventbus/src/refcodes-eventbus-3.2.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-eventbus/src/refcodes-eventbus-3.2.8/README.md) 

## Change list &lt;refcodes-eventbus-ext&gt; (version 3.2.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-3.2.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-3.2.8/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-3.2.8/refcodes-eventbus-ext-application/pom.xml) 

## Change list &lt;refcodes-decoupling&gt; (version 3.2.8)

* \[<span style="color:green">MODIFIED</span>\] [`.classpath`](https://bitbucket.org/refcodes/refcodes-decoupling/src/refcodes-decoupling-3.2.8/.classpath) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-decoupling/src/refcodes-decoupling-3.2.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-decoupling/src/refcodes-decoupling-3.2.8/README.md) 

## Change list &lt;refcodes-decoupling-ext&gt; (version 3.2.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-decoupling-ext/src/refcodes-decoupling-ext-3.2.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-decoupling-ext/src/refcodes-decoupling-ext-3.2.8/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-decoupling-ext/src/refcodes-decoupling-ext-3.2.8/refcodes-decoupling-ext-application/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-decoupling-ext/src/refcodes-decoupling-ext-3.2.8/refcodes-decoupling-ext-application/README.md) 

## Change list &lt;refcodes-filesystem&gt; (version 3.2.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-filesystem/src/refcodes-filesystem-3.2.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-filesystem/src/refcodes-filesystem-3.2.8/README.md) 

## Change list &lt;refcodes-filesystem-alt&gt; (version 3.2.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-filesystem-alt/src/refcodes-filesystem-alt-3.2.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-filesystem-alt/src/refcodes-filesystem-alt-3.2.8/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-filesystem-alt/src/refcodes-filesystem-alt-3.2.8/refcodes-filesystem-alt-s3/pom.xml) 

## Change list &lt;refcodes-forwardsecrecy&gt; (version 3.2.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy/src/refcodes-forwardsecrecy-3.2.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy/src/refcodes-forwardsecrecy-3.2.8/README.md) 

## Change list &lt;refcodes-forwardsecrecy-alt&gt; (version 3.2.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-3.2.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-3.2.8/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-3.2.8/refcodes-forwardsecrecy-alt-filesystem/pom.xml) 

## Change list &lt;refcodes-io-ext&gt; (version 3.2.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-3.2.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-3.2.8/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-3.2.8/refcodes-io-ext-observer/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-3.2.8/refcodes-io-ext-observer/README.md) 

## Change list &lt;refcodes-jobbus&gt; (version 3.2.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-jobbus/src/refcodes-jobbus-3.2.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-jobbus/src/refcodes-jobbus-3.2.8/README.md) 

## Change list &lt;refcodes-rest-ext&gt; (version 3.2.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-3.2.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-3.2.8/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-3.2.8/refcodes-rest-ext-eureka/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-3.2.8/refcodes-rest-ext-eureka/README.md) 

## Change list &lt;refcodes-remoting&gt; (version 3.2.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-remoting/src/refcodes-remoting-3.2.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-remoting/src/refcodes-remoting-3.2.8/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`AbstractRemote.java`](https://bitbucket.org/refcodes/refcodes-remoting/src/refcodes-remoting-3.2.8/src/main/java/org/refcodes/remoting/AbstractRemote.java) (see Javadoc at [`AbstractRemote.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-remoting/3.2.8/org.refcodes.remoting/org/refcodes/remoting/AbstractRemote.html))

## Change list &lt;refcodes-remoting-ext&gt; (version 3.2.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-3.2.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-3.2.8/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-3.2.8/refcodes-remoting-ext-observer/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-3.2.8/refcodes-remoting-ext-observer/README.md) 

## Change list &lt;refcodes-serial&gt; (version 3.2.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial/src/refcodes-serial-3.2.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-serial/src/refcodes-serial-3.2.8/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`DataTypesTest.java`](https://bitbucket.org/refcodes/refcodes-serial/src/refcodes-serial-3.2.8/src/test/java/org/refcodes/serial/DataTypesTest.java) 

## Change list &lt;refcodes-serial-alt&gt; (version 3.2.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial-alt/src/refcodes-serial-alt-3.2.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-serial-alt/src/refcodes-serial-alt-3.2.8/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial-alt/src/refcodes-serial-alt-3.2.8/refcodes-serial-alt-tty/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-serial-alt/src/refcodes-serial-alt-3.2.8/refcodes-serial-alt-tty/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`TtyPortHubTest.java`](https://bitbucket.org/refcodes/refcodes-serial-alt/src/refcodes-serial-alt-3.2.8/refcodes-serial-alt-tty/src/test/java/org/refcodes/serial/alt/tty/TtyPortHubTest.java) 

## Change list &lt;refcodes-serial-ext&gt; (version 3.2.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-3.2.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-3.2.8/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-3.2.8/refcodes-serial-ext-handshake/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-3.2.8/refcodes-serial-ext-handshake/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-3.2.8/refcodes-serial-ext-observer/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-3.2.8/refcodes-serial-ext-observer/README.md) 

## Change list &lt;refcodes-p2p&gt; (version 3.2.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-p2p/src/refcodes-p2p-3.2.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-p2p/src/refcodes-p2p-3.2.8/README.md) 

## Change list &lt;refcodes-p2p-alt&gt; (version 3.2.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-p2p-alt/src/refcodes-p2p-alt-3.2.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-p2p-alt/src/refcodes-p2p-alt-3.2.8/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-p2p-alt/src/refcodes-p2p-alt-3.2.8/refcodes-p2p-alt-serial/pom.xml) 

## Change list &lt;refcodes-p2p-ext&gt; (version 3.2.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-p2p-ext/src/refcodes-p2p-ext-3.2.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-p2p-ext/src/refcodes-p2p-ext-3.2.8/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-p2p-ext/src/refcodes-p2p-ext-3.2.8/refcodes-p2p-ext-observer/pom.xml) 

## Change list &lt;refcodes-tabular-alt&gt; (version 3.2.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-tabular-alt/src/refcodes-tabular-alt-3.2.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-tabular-alt/src/refcodes-tabular-alt-3.2.8/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-tabular-alt/src/refcodes-tabular-alt-3.2.8/refcodes-tabular-alt-forwardsecrecy/pom.xml) 

## Change list &lt;refcodes-archetype&gt; (version 3.2.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype/src/refcodes-archetype-3.2.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype/src/refcodes-archetype-3.2.8/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`C2Helper.java`](https://bitbucket.org/refcodes/refcodes-archetype/src/refcodes-archetype-3.2.8/src/main/java/org/refcodes/archetype/C2Helper.java) (see Javadoc at [`C2Helper.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-archetype/3.2.8/org.refcodes.archetype/org/refcodes/archetype/C2Helper.html))
* \[<span style="color:green">MODIFIED</span>\] [`CliHelper.java`](https://bitbucket.org/refcodes/refcodes-archetype/src/refcodes-archetype-3.2.8/src/main/java/org/refcodes/archetype/CliHelper.java) (see Javadoc at [`CliHelper.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-archetype/3.2.8/org.refcodes.archetype/org/refcodes/archetype/CliHelper.html))
* \[<span style="color:green">MODIFIED</span>\] [`CtxHelper.java`](https://bitbucket.org/refcodes/refcodes-archetype/src/refcodes-archetype-3.2.8/src/main/java/org/refcodes/archetype/CtxHelper.java) (see Javadoc at [`CtxHelper.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-archetype/3.2.8/org.refcodes.archetype/org/refcodes/archetype/CtxHelper.html))

## Change list &lt;refcodes-archetype-alt&gt; (version 3.2.8)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.2.8/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.2.8/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.2.8/refcodes-archetype-alt-c2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.2.8/refcodes-archetype-alt-c2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`bundle.conf`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.2.8/refcodes-archetype-alt-c2/src/main/resources/archetype-resources/bundle.conf) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.2.8/refcodes-archetype-alt-c2/src/main/resources/archetype-resources/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`Main.java`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.2.8/refcodes-archetype-alt-c2/src/main/resources/archetype-resources/src/main/java/Main.java) (see Javadoc at [`Main.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-archetype-alt-c2/src/main/resources/archetype-resources/3.2.8/src.main.resources.archetype-resources/Main.html))
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.2.8/refcodes-archetype-alt-cli/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.2.8/refcodes-archetype-alt-cli/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`bundle.conf`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.2.8/refcodes-archetype-alt-cli/src/main/resources/archetype-resources/bundle.conf) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.2.8/refcodes-archetype-alt-cli/src/main/resources/archetype-resources/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`Main.java`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.2.8/refcodes-archetype-alt-cli/src/main/resources/archetype-resources/src/main/java/Main.java) (see Javadoc at [`Main.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-archetype-alt-cli/src/main/resources/archetype-resources/3.2.8/src.main.resources.archetype-resources/Main.html))
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.2.8/refcodes-archetype-alt-csv/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.2.8/refcodes-archetype-alt-csv/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`bundle.conf`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.2.8/refcodes-archetype-alt-csv/src/main/resources/archetype-resources/bundle.conf) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.2.8/refcodes-archetype-alt-csv/src/main/resources/archetype-resources/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`Main.java`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.2.8/refcodes-archetype-alt-csv/src/main/resources/archetype-resources/src/main/java/Main.java) (see Javadoc at [`Main.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-archetype-alt-csv/src/main/resources/archetype-resources/3.2.8/src.main.resources.archetype-resources/Main.html))
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.2.8/refcodes-archetype-alt-decoupling/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.2.8/refcodes-archetype-alt-decoupling/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`bundle.conf`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.2.8/refcodes-archetype-alt-decoupling/src/main/resources/archetype-resources/bundle.conf) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.2.8/refcodes-archetype-alt-decoupling/src/main/resources/archetype-resources/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`Main.java`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.2.8/refcodes-archetype-alt-decoupling/src/main/resources/archetype-resources/src/main/java/Main.java) (see Javadoc at [`Main.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-archetype-alt-decoupling/src/main/resources/archetype-resources/3.2.8/src.main.resources.archetype-resources/Main.html))
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.2.8/refcodes-archetype-alt-eventbus/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.2.8/refcodes-archetype-alt-eventbus/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`bundle.conf`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.2.8/refcodes-archetype-alt-eventbus/src/main/resources/archetype-resources/bundle.conf) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.2.8/refcodes-archetype-alt-eventbus/src/main/resources/archetype-resources/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`Main.java`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.2.8/refcodes-archetype-alt-eventbus/src/main/resources/archetype-resources/src/main/java/Main.java) (see Javadoc at [`Main.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-archetype-alt-eventbus/src/main/resources/archetype-resources/3.2.8/src.main.resources.archetype-resources/Main.html))
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.2.8/refcodes-archetype-alt-filter/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.2.8/refcodes-archetype-alt-filter/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`bundle.conf`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.2.8/refcodes-archetype-alt-filter/src/main/resources/archetype-resources/bundle.conf) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.2.8/refcodes-archetype-alt-filter/src/main/resources/archetype-resources/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`Main.java`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.2.8/refcodes-archetype-alt-filter/src/main/resources/archetype-resources/src/main/java/Main.java) (see Javadoc at [`Main.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-archetype-alt-filter/src/main/resources/archetype-resources/3.2.8/src.main.resources.archetype-resources/Main.html))
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.2.8/refcodes-archetype-alt-rest/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.2.8/refcodes-archetype-alt-rest/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`bundle.conf`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.2.8/refcodes-archetype-alt-rest/src/main/resources/archetype-resources/bundle.conf) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.2.8/refcodes-archetype-alt-rest/src/main/resources/archetype-resources/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`Main.java`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.2.8/refcodes-archetype-alt-rest/src/main/resources/archetype-resources/src/main/java/Main.java) (see Javadoc at [`Main.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-archetype-alt-rest/src/main/resources/archetype-resources/3.2.8/src.main.resources.archetype-resources/Main.html))
