> This change list has been auto-generated on `triton` by `steiner` with `changelist-all.sh` on the 2021-12-01 at 20:27:50.

## Change list &lt;refcodes-licensing&gt; (version 2.2.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-licensing/src/refcodes-licensing-2.2.0/pom.xml) 

## Change list &lt;refcodes-parent&gt; (version 2.2.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-parent/src/refcodes-parent-2.2.0/pom.xml) 

## Change list &lt;refcodes-time&gt; (version 2.2.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-time/src/refcodes-time-2.2.0/pom.xml) 

## Change list &lt;refcodes-mixin&gt; (version 2.2.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-mixin/src/refcodes-mixin-2.2.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`ReadTimeoutInMsAccessor.java`](https://bitbucket.org/refcodes/refcodes-mixin/src/refcodes-mixin-2.2.0/src/main/java/org/refcodes/mixin/ReadTimeoutInMsAccessor.java) (see Javadoc at [`ReadTimeoutInMsAccessor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-mixin/2.2.0/org.refcodes.mixin/org/refcodes/mixin/ReadTimeoutInMsAccessor.html))
* \[<span style="color:green">MODIFIED</span>\] [`TimeoutInMsAccessor.java`](https://bitbucket.org/refcodes/refcodes-mixin/src/refcodes-mixin-2.2.0/src/main/java/org/refcodes/mixin/TimeoutInMsAccessor.java) (see Javadoc at [`TimeoutInMsAccessor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-mixin/2.2.0/org.refcodes.mixin/org/refcodes/mixin/TimeoutInMsAccessor.html))
* \[<span style="color:green">MODIFIED</span>\] [`WriteTimeoutInMsAccessor.java`](https://bitbucket.org/refcodes/refcodes-mixin/src/refcodes-mixin-2.2.0/src/main/java/org/refcodes/mixin/WriteTimeoutInMsAccessor.java) (see Javadoc at [`WriteTimeoutInMsAccessor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-mixin/2.2.0/org.refcodes.mixin/org/refcodes/mixin/WriteTimeoutInMsAccessor.html))

## Change list &lt;refcodes-data&gt; (version 2.2.0)

* \[<span style="color:blue">ADDED</span>\] [`IoHeurisitcsTimeToLive.java`](https://bitbucket.org/refcodes/refcodes-data/src/refcodes-data-2.2.0/src/main/java/org/refcodes/data/IoHeurisitcsTimeToLive.java) (see Javadoc at [`IoHeurisitcsTimeToLive.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-data/2.2.0/org.refcodes.data/org/refcodes/data/IoHeurisitcsTimeToLive.html))
* \[<span style="color:blue">ADDED</span>\] [`IoReconnectLoopTime.java`](https://bitbucket.org/refcodes/refcodes-data/src/refcodes-data-2.2.0/src/main/java/org/refcodes/data/IoReconnectLoopTime.java) (see Javadoc at [`IoReconnectLoopTime.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-data/2.2.0/org.refcodes.data/org/refcodes/data/IoReconnectLoopTime.html))
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data/src/refcodes-data-2.2.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`Ascii.java`](https://bitbucket.org/refcodes/refcodes-data/src/refcodes-data-2.2.0/src/main/java/org/refcodes/data/Ascii.java) (see Javadoc at [`Ascii.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-data/2.2.0/org.refcodes.data/org/refcodes/data/Ascii.html))
* \[<span style="color:green">MODIFIED</span>\] [`IoPollLoopTime.java`](https://bitbucket.org/refcodes/refcodes-data/src/refcodes-data-2.2.0/src/main/java/org/refcodes/data/IoPollLoopTime.java) (see Javadoc at [`IoPollLoopTime.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-data/2.2.0/org.refcodes.data/org/refcodes/data/IoPollLoopTime.html))
* \[<span style="color:green">MODIFIED</span>\] [`IoRetryCount.java`](https://bitbucket.org/refcodes/refcodes-data/src/refcodes-data-2.2.0/src/main/java/org/refcodes/data/IoRetryCount.java) (see Javadoc at [`IoRetryCount.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-data/2.2.0/org.refcodes.data/org/refcodes/data/IoRetryCount.html))
* \[<span style="color:green">MODIFIED</span>\] [`IoSleepLoopTime.java`](https://bitbucket.org/refcodes/refcodes-data/src/refcodes-data-2.2.0/src/main/java/org/refcodes/data/IoSleepLoopTime.java) (see Javadoc at [`IoSleepLoopTime.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-data/2.2.0/org.refcodes.data/org/refcodes/data/IoSleepLoopTime.html))

## Change list &lt;refcodes-exception&gt; (version 2.2.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-exception/src/refcodes-exception-2.2.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`TimeoutIOException.java`](https://bitbucket.org/refcodes/refcodes-exception/src/refcodes-exception-2.2.0/src/main/java/org/refcodes/exception/TimeoutIOException.java) (see Javadoc at [`TimeoutIOException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-exception/2.2.0/org.refcodes.exception/org/refcodes/exception/TimeoutIOException.html))

## Change list &lt;refcodes-factory&gt; (version 2.2.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-factory/src/refcodes-factory-2.2.0/pom.xml) 

## Change list &lt;refcodes-factory-alt&gt; (version 2.2.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-factory-alt/src/refcodes-factory-alt-2.2.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-factory-alt/src/refcodes-factory-alt-2.2.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-factory-alt/src/refcodes-factory-alt-2.2.0/refcodes-factory-alt-spring/pom.xml) 

## Change list &lt;refcodes-controlflow&gt; (version 2.2.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-controlflow/src/refcodes-controlflow-2.2.0/pom.xml) 

## Change list &lt;refcodes-numerical&gt; (version 2.2.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-numerical/src/refcodes-numerical-2.2.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`AbstractCrcMixin.java`](https://bitbucket.org/refcodes/refcodes-numerical/src/refcodes-numerical-2.2.0/src/main/java/org/refcodes/numerical/AbstractCrcMixin.java) (see Javadoc at [`AbstractCrcMixin.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-numerical/2.2.0/org.refcodes.numerical/org/refcodes/numerical/AbstractCrcMixin.html))
* \[<span style="color:green">MODIFIED</span>\] [`CrcMixin.java`](https://bitbucket.org/refcodes/refcodes-numerical/src/refcodes-numerical-2.2.0/src/main/java/org/refcodes/numerical/CrcMixin.java) (see Javadoc at [`CrcMixin.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-numerical/2.2.0/org.refcodes.numerical/org/refcodes/numerical/CrcMixin.html))

## Change list &lt;refcodes-generator&gt; (version 2.2.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-generator/src/refcodes-generator-2.2.0/pom.xml) 

## Change list &lt;refcodes-matcher&gt; (version 2.2.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-matcher/src/refcodes-matcher-2.2.0/pom.xml) 

## Change list &lt;refcodes-struct&gt; (version 2.2.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-struct/src/refcodes-struct-2.2.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-struct/src/refcodes-struct-2.2.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`PathMap.java`](https://bitbucket.org/refcodes/refcodes-struct/src/refcodes-struct-2.2.0/src/main/java/org/refcodes/struct/PathMap.java) (see Javadoc at [`PathMap.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-struct/2.2.0/org.refcodes.struct/org/refcodes/struct/PathMap.html))

## Change list &lt;refcodes-runtime&gt; (version 2.2.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-2.2.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-2.2.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`module-info.java`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-2.2.0/src/main/java/module-info.java) (see Javadoc at [`module-info.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-runtime/2.2.0/./module-info.html))
* \[<span style="color:green">MODIFIED</span>\] [`RuntimeUtility.java`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-2.2.0/src/main/java/org/refcodes/runtime/RuntimeUtility.java) (see Javadoc at [`RuntimeUtility.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-runtime/2.2.0/org.refcodes.runtime/org/refcodes/runtime/RuntimeUtility.html))

## Change list &lt;refcodes-component&gt; (version 2.2.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-2.2.0/pom.xml) 

## Change list &lt;refcodes-data-ext&gt; (version 2.2.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-2.2.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-2.2.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-2.2.0/refcodes-data-ext-boulderdash/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-2.2.0/refcodes-data-ext-checkers/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-2.2.0/refcodes-data-ext-checkers/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-2.2.0/refcodes-data-ext-chess/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-2.2.0/refcodes-data-ext-chess/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-2.2.0/refcodes-data-ext-corporate/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-2.2.0/refcodes-data-ext-corporate/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-2.2.0/refcodes-data-ext-symbols/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-2.2.0/refcodes-data-ext-symbols/README.md) 

## Change list &lt;refcodes-graphical&gt; (version 2.2.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-graphical/src/refcodes-graphical-2.2.0/pom.xml) 

## Change list &lt;refcodes-textual&gt; (version 2.2.0)

* \[<span style="color:blue">ADDED</span>\] [`NumberSuffixComparator.java`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-2.2.0/src/main/java/org/refcodes/textual/NumberSuffixComparator.java) (see Javadoc at [`NumberSuffixComparator.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-textual/2.2.0/org.refcodes.textual/org/refcodes/textual/NumberSuffixComparator.html))
* \[<span style="color:blue">ADDED</span>\] [`NumberSuffixComparatorSingleton.java`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-2.2.0/src/main/java/org/refcodes/textual/NumberSuffixComparatorSingleton.java) (see Javadoc at [`NumberSuffixComparatorSingleton.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-textual/2.2.0/org.refcodes.textual/org/refcodes/textual/NumberSuffixComparatorSingleton.html))
* \[<span style="color:blue">ADDED</span>\] [`NumberSuffixComparatorTest.java`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-2.2.0/src/test/java/org/refcodes/textual/NumberSuffixComparatorTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-2.2.0/pom.xml) 

## Change list &lt;refcodes-criteria&gt; (version 2.2.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-2.2.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-2.2.0/README.md) 

## Change list &lt;refcodes-io&gt; (version 2.2.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-io/src/refcodes-io-2.2.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-io/src/refcodes-io-2.2.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`Skippable.java`](https://bitbucket.org/refcodes/refcodes-io/src/refcodes-io-2.2.0/src/main/java/org/refcodes/io/Skippable.java) (see Javadoc at [`Skippable.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-io/2.2.0/org.refcodes.io/org/refcodes/io/Skippable.html))

## Change list &lt;refcodes-tabular&gt; (version 2.2.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-tabular/src/refcodes-tabular-2.2.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-tabular/src/refcodes-tabular-2.2.0/README.md) 

## Change list &lt;refcodes-observer&gt; (version 2.2.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-observer/src/refcodes-observer-2.2.0/pom.xml) 

## Change list &lt;refcodes-command&gt; (version 2.2.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-command/src/refcodes-command-2.2.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-command/src/refcodes-command-2.2.0/README.md) 

## Change list &lt;refcodes-cli&gt; (version 2.2.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-2.2.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-2.2.0/README.md) 

## Change list &lt;refcodes-audio&gt; (version 2.2.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-audio/src/refcodes-audio-2.2.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-audio/src/refcodes-audio-2.2.0/README.md) 

## Change list &lt;refcodes-codec&gt; (version 2.2.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-2.2.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-2.2.0/README.md) 

## Change list &lt;refcodes-component-ext&gt; (version 2.2.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-2.2.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-2.2.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-2.2.0/refcodes-component-ext-observer/pom.xml) 

## Change list &lt;refcodes-properties&gt; (version 2.2.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-2.2.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-2.2.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`package-info.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-2.2.0/src/main/java/org/refcodes/properties/package-info.java) 

## Change list &lt;refcodes-security&gt; (version 2.2.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security/src/refcodes-security-2.2.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security/src/refcodes-security-2.2.0/README.md) 

## Change list &lt;refcodes-security-alt&gt; (version 2.2.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-2.2.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-2.2.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-2.2.0/refcodes-security-alt-chaos/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-2.2.0/refcodes-security-alt-chaos/README.md) 

## Change list &lt;refcodes-security-ext&gt; (version 2.2.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-2.2.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-2.2.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-2.2.0/refcodes-security-ext-chaos/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-2.2.0/refcodes-security-ext-chaos/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-2.2.0/refcodes-security-ext-spring/pom.xml) 

## Change list &lt;refcodes-properties-ext&gt; (version 2.2.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-2.2.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-2.2.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-2.2.0/refcodes-properties-ext-cli/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-2.2.0/refcodes-properties-ext-cli/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-2.2.0/refcodes-properties-ext-obfuscation/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-2.2.0/refcodes-properties-ext-obfuscation/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-2.2.0/refcodes-properties-ext-observer/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-2.2.0/refcodes-properties-ext-observer/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-2.2.0/refcodes-properties-ext-runtime/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-2.2.0/refcodes-properties-ext-runtime/README.md) 

## Change list &lt;refcodes-logger&gt; (version 2.2.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger/src/refcodes-logger-2.2.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger/src/refcodes-logger-2.2.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`ColumnLayout.java`](https://bitbucket.org/refcodes/refcodes-logger/src/refcodes-logger-2.2.0/src/main/java/org/refcodes/logger/ColumnLayout.java) (see Javadoc at [`ColumnLayout.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-logger/2.2.0/org.refcodes.logger/org/refcodes/logger/ColumnLayout.html))
* \[<span style="color:green">MODIFIED</span>\] [`RuntimeLoggerFactorySingleton.java`](https://bitbucket.org/refcodes/refcodes-logger/src/refcodes-logger-2.2.0/src/main/java/org/refcodes/logger/RuntimeLoggerFactorySingleton.java) (see Javadoc at [`RuntimeLoggerFactorySingleton.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-logger/2.2.0/org.refcodes.logger/org/refcodes/logger/RuntimeLoggerFactorySingleton.html))
* \[<span style="color:green">MODIFIED</span>\] [`RuntimeLoggerImpl.java`](https://bitbucket.org/refcodes/refcodes-logger/src/refcodes-logger-2.2.0/src/main/java/org/refcodes/logger/RuntimeLoggerImpl.java) (see Javadoc at [`RuntimeLoggerImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-logger/2.2.0/org.refcodes.logger/org/refcodes/logger/RuntimeLoggerImpl.html))

## Change list &lt;refcodes-logger-alt&gt; (version 2.2.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-2.2.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-2.2.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-2.2.0/refcodes-logger-alt-async/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-2.2.0/refcodes-logger-alt-async/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-2.2.0/refcodes-logger-alt-console/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-2.2.0/refcodes-logger-alt-console/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-2.2.0/refcodes-logger-alt-io/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-2.2.0/refcodes-logger-alt-io/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-2.2.0/refcodes-logger-alt-simpledb/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-2.2.0/refcodes-logger-alt-slf4j/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-2.2.0/refcodes-logger-alt-spring/pom.xml) 

## Change list &lt;refcodes-logger-ext&gt; (version 2.2.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-2.2.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-2.2.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-2.2.0/refcodes-logger-ext-slf4j/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-2.2.0/refcodes-logger-ext-slf4j/README.md) 

## Change list &lt;refcodes-graphical-ext&gt; (version 2.2.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-2.2.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-2.2.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-2.2.0/refcodes-graphical-ext-javafx/pom.xml) 

## Change list &lt;refcodes-checkerboard&gt; (version 2.2.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-2.2.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-2.2.0/README.md) 

## Change list &lt;refcodes-checkerboard-alt&gt; (version 2.2.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-alt/src/refcodes-checkerboard-alt-2.2.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-alt/src/refcodes-checkerboard-alt-2.2.0/refcodes-checkerboard-alt-javafx/pom.xml) 

## Change list &lt;refcodes-checkerboard-ext&gt; (version 2.2.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-ext/src/refcodes-checkerboard-ext-2.2.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-checkerboard-ext/src/refcodes-checkerboard-ext-2.2.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-ext/src/refcodes-checkerboard-ext-2.2.0/refcodes-checkerboard-ext-javafx-boulderdash/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-ext/src/refcodes-checkerboard-ext-2.2.0/refcodes-checkerboard-ext-javafx-chess/pom.xml) 

## Change list &lt;refcodes-boulderdash&gt; (version 2.2.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-boulderdash/src/refcodes-boulderdash-2.2.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-boulderdash/src/refcodes-boulderdash-2.2.0/README.md) 

## Change list &lt;refcodes-net&gt; (version 2.2.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-2.2.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`PortManagerSingleton.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-2.2.0/src/main/java/org/refcodes/net/PortManagerSingleton.java) (see Javadoc at [`PortManagerSingleton.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/2.2.0/org.refcodes.net/org/refcodes/net/PortManagerSingleton.html))

## Change list &lt;refcodes-web&gt; (version 2.2.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-web/src/refcodes-web-2.2.0/pom.xml) 

## Change list &lt;refcodes-rest&gt; (version 2.2.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-2.2.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-2.2.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`OauthTokenHandler.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-2.2.0/src/main/java/org/refcodes/rest/OauthTokenHandler.java) (see Javadoc at [`OauthTokenHandler.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/2.2.0/org.refcodes.rest/org/refcodes/rest/OauthTokenHandler.html))

## Change list &lt;refcodes-hal&gt; (version 2.2.0)

* \[<span style="color:blue">ADDED</span>\] [`module-info.java`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-2.2.0/src/main/java/module-info.java) (see Javadoc at [`module-info.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-hal/2.2.0/./module-info.html))
* \[<span style="color:blue">ADDED</span>\] [`HalDataImpl.java`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-2.2.0/src/main/java/org/refcodes/hal/HalDataImpl.java) (see Javadoc at [`HalDataImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-hal/2.2.0/org.refcodes.hal/org/refcodes/hal/HalDataImpl.html))
* \[<span style="color:blue">ADDED</span>\] [`HalData.java`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-2.2.0/src/main/java/org/refcodes/hal/HalData.java) (see Javadoc at [`HalData.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-hal/2.2.0/org.refcodes.hal/org/refcodes/hal/HalData.html))
* \[<span style="color:blue">ADDED</span>\] [`HalDataPageImpl.java`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-2.2.0/src/main/java/org/refcodes/hal/HalDataPageImpl.java) (see Javadoc at [`HalDataPageImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-hal/2.2.0/org.refcodes.hal/org/refcodes/hal/HalDataPageImpl.html))
* \[<span style="color:blue">ADDED</span>\] [`HalDataPage.java`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-2.2.0/src/main/java/org/refcodes/hal/HalDataPage.java) (see Javadoc at [`HalDataPage.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-hal/2.2.0/org.refcodes.hal/org/refcodes/hal/HalDataPage.html))
* \[<span style="color:blue">ADDED</span>\] [`HalMapImpl.java`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-2.2.0/src/main/java/org/refcodes/hal/HalMapImpl.java) (see Javadoc at [`HalMapImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-hal/2.2.0/org.refcodes.hal/org/refcodes/hal/HalMapImpl.html))
* \[<span style="color:blue">ADDED</span>\] [`HalMap.java`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-2.2.0/src/main/java/org/refcodes/hal/HalMap.java) (see Javadoc at [`HalMap.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-hal/2.2.0/org.refcodes.hal/org/refcodes/hal/HalMap.html))
* \[<span style="color:blue">ADDED</span>\] [`HalStructImpl.java`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-2.2.0/src/main/java/org/refcodes/hal/HalStructImpl.java) (see Javadoc at [`HalStructImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-hal/2.2.0/org.refcodes.hal/org/refcodes/hal/HalStructImpl.html))
* \[<span style="color:blue">ADDED</span>\] [`HalStruct.java`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-2.2.0/src/main/java/org/refcodes/hal/HalStruct.java) (see Javadoc at [`HalStruct.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-hal/2.2.0/org.refcodes.hal/org/refcodes/hal/HalStruct.html))
* \[<span style="color:blue">ADDED</span>\] [`TraversalMode.java`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-2.2.0/src/main/java/org/refcodes/hal/TraversalMode.java) (see Javadoc at [`TraversalMode.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-hal/2.2.0/org.refcodes.hal/org/refcodes/hal/TraversalMode.html))
* \[<span style="color:blue">ADDED</span>\] [`AbstractHalClientTest.java`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-2.2.0/src/test/java/org/refcodes/hal/AbstractHalClientTest.java) 
* \[<span style="color:blue">ADDED</span>\] [`Address.java`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-2.2.0/src/test/java/org/refcodes/hal/Address.java) 
* \[<span style="color:blue">ADDED</span>\] [`AddressRepository.java`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-2.2.0/src/test/java/org/refcodes/hal/AddressRepository.java) 
* \[<span style="color:blue">ADDED</span>\] [`Author.java`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-2.2.0/src/test/java/org/refcodes/hal/Author.java) 
* \[<span style="color:blue">ADDED</span>\] [`AuthorRepository.java`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-2.2.0/src/test/java/org/refcodes/hal/AuthorRepository.java) 
* \[<span style="color:blue">ADDED</span>\] [`Book.java`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-2.2.0/src/test/java/org/refcodes/hal/Book.java) 
* \[<span style="color:blue">ADDED</span>\] [`BookRepository.java`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-2.2.0/src/test/java/org/refcodes/hal/BookRepository.java) 
* \[<span style="color:blue">ADDED</span>\] [`HalClientTest.java`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-2.2.0/src/test/java/org/refcodes/hal/HalClientTest.java) 
* \[<span style="color:blue">ADDED</span>\] [`HalServer.java`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-2.2.0/src/test/java/org/refcodes/hal/HalServer.java) 
* \[<span style="color:blue">ADDED</span>\] [`Library.java`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-2.2.0/src/test/java/org/refcodes/hal/Library.java) 
* \[<span style="color:blue">ADDED</span>\] [`LibraryRepository.java`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-2.2.0/src/test/java/org/refcodes/hal/LibraryRepository.java) 
* \[<span style="color:blue">ADDED</span>\] [`Location.java`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-2.2.0/src/test/java/org/refcodes/hal/Location.java) 
* \[<span style="color:blue">ADDED</span>\] [`Name.java`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-2.2.0/src/test/java/org/refcodes/hal/Name.java) 
* \[<span style="color:blue">ADDED</span>\] [`Person.java`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-2.2.0/src/test/java/org/refcodes/hal/Person.java) 
* \[<span style="color:blue">ADDED</span>\] [`PersonRepository.java`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-2.2.0/src/test/java/org/refcodes/hal/PersonRepository.java) 
* \[<span style="color:blue">ADDED</span>\] [`Subject.java`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-2.2.0/src/test/java/org/refcodes/hal/Subject.java) 
* \[<span style="color:blue">ADDED</span>\] [`SubjectRepository.java`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-2.2.0/src/test/java/org/refcodes/hal/SubjectRepository.java) 
* \[<span style="color:blue">ADDED</span>\] [`TraversalModeTest.java`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-2.2.0/src/test/java/org/refcodes/hal/TraversalModeTest.java) 
* \[<span style="color:blue">ADDED</span>\] [`User.java`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-2.2.0/src/test/java/org/refcodes/hal/User.java) 
* \[<span style="color:blue">ADDED</span>\] [`UserRepository.java`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-2.2.0/src/test/java/org/refcodes/hal/UserRepository.java) 
* \[<span style="color:blue">ADDED</span>\] [`application.properties`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-2.2.0/src/test/resources/application.properties) 
* \[<span style="color:blue">ADDED</span>\] [`log4j.xml`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-2.2.0/src/test/resources/log4j.xml) 
* \[<span style="color:red">DELETED</span>\] `module-info.java.off`
* \[<span style="color:red">DELETED</span>\] `NoHalEndpointException.java`
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-2.2.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-2.2.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`HalClientImpl.java`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-2.2.0/src/main/java/org/refcodes/hal/HalClientImpl.java) (see Javadoc at [`HalClientImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-hal/2.2.0/org.refcodes.hal/org/refcodes/hal/HalClientImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`HalClient.java`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-2.2.0/src/main/java/org/refcodes/hal/HalClient.java) (see Javadoc at [`HalClient.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-hal/2.2.0/org.refcodes.hal/org/refcodes/hal/HalClient.html))
* \[<span style="color:green">MODIFIED</span>\] [`HalException.java`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-2.2.0/src/main/java/org/refcodes/hal/HalException.java) (see Javadoc at [`HalException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-hal/2.2.0/org.refcodes.hal/org/refcodes/hal/HalException.html))
* \[<span style="color:green">MODIFIED</span>\] [`HalRuntimeException.java`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-2.2.0/src/main/java/org/refcodes/hal/HalRuntimeException.java) (see Javadoc at [`HalRuntimeException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-hal/2.2.0/org.refcodes.hal/org/refcodes/hal/HalRuntimeException.html))

## Change list &lt;refcodes-eventbus&gt; (version 2.2.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-eventbus/src/refcodes-eventbus-2.2.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-eventbus/src/refcodes-eventbus-2.2.0/README.md) 

## Change list &lt;refcodes-eventbus-ext&gt; (version 2.2.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-2.2.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-2.2.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-2.2.0/refcodes-eventbus-ext-application/pom.xml) 

## Change list &lt;refcodes-filesystem&gt; (version 2.2.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-filesystem/src/refcodes-filesystem-2.2.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-filesystem/src/refcodes-filesystem-2.2.0/README.md) 

## Change list &lt;refcodes-filesystem-alt&gt; (version 2.2.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-filesystem-alt/src/refcodes-filesystem-alt-2.2.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-filesystem-alt/src/refcodes-filesystem-alt-2.2.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-filesystem-alt/src/refcodes-filesystem-alt-2.2.0/refcodes-filesystem-alt-s3/pom.xml) 

## Change list &lt;refcodes-forwardsecrecy&gt; (version 2.2.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy/src/refcodes-forwardsecrecy-2.2.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy/src/refcodes-forwardsecrecy-2.2.0/README.md) 

## Change list &lt;refcodes-forwardsecrecy-alt&gt; (version 2.2.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-2.2.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-2.2.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-2.2.0/refcodes-forwardsecrecy-alt-filesystem/pom.xml) 

## Change list &lt;refcodes-io-ext&gt; (version 2.2.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-2.2.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-2.2.0/refcodes-io-ext-observer/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-2.2.0/refcodes-io-ext-observer/README.md) 

## Change list &lt;refcodes-interceptor&gt; (version 2.2.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-interceptor/src/refcodes-interceptor-2.2.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-interceptor/src/refcodes-interceptor-2.2.0/README.md) 

## Change list &lt;refcodes-jobbus&gt; (version 2.2.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-jobbus/src/refcodes-jobbus-2.2.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-jobbus/src/refcodes-jobbus-2.2.0/README.md) 

## Change list &lt;refcodes-rest-ext&gt; (version 2.2.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-2.2.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-2.2.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-2.2.0/refcodes-rest-ext-eureka/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-2.2.0/refcodes-rest-ext-eureka/README.md) 

## Change list &lt;refcodes-remoting&gt; (version 2.2.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-remoting/src/refcodes-remoting-2.2.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-remoting/src/refcodes-remoting-2.2.0/README.md) 

## Change list &lt;refcodes-remoting-ext&gt; (version 2.2.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-2.2.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-2.2.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-2.2.0/refcodes-remoting-ext-observer/pom.xml) 

## Change list &lt;refcodes-serial&gt; (version 2.2.0)

* \[<span style="color:blue">ADDED</span>\] [`IoHeuristicsTimeToLiveInMsAccessor.java`](https://bitbucket.org/refcodes/refcodes-serial/src/refcodes-serial-2.2.0/src/main/java/org/refcodes/serial/IoHeuristicsTimeToLiveInMsAccessor.java) (see Javadoc at [`IoHeuristicsTimeToLiveInMsAccessor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-serial/2.2.0/org.refcodes.serial/org/refcodes/serial/IoHeuristicsTimeToLiveInMsAccessor.html))
* \[<span style="color:blue">ADDED</span>\] [`PingMagicBytesAccessor.java`](https://bitbucket.org/refcodes/refcodes-serial/src/refcodes-serial-2.2.0/src/main/java/org/refcodes/serial/PingMagicBytesAccessor.java) (see Javadoc at [`PingMagicBytesAccessor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-serial/2.2.0/org.refcodes.serial/org/refcodes/serial/PingMagicBytesAccessor.html))
* \[<span style="color:blue">ADDED</span>\] [`PingRetryNumberAccessor.java`](https://bitbucket.org/refcodes/refcodes-serial/src/refcodes-serial-2.2.0/src/main/java/org/refcodes/serial/PingRetryNumberAccessor.java) (see Javadoc at [`PingRetryNumberAccessor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-serial/2.2.0/org.refcodes.serial/org/refcodes/serial/PingRetryNumberAccessor.html))
* \[<span style="color:blue">ADDED</span>\] [`PingTimeoutInMsAccessor.java`](https://bitbucket.org/refcodes/refcodes-serial/src/refcodes-serial-2.2.0/src/main/java/org/refcodes/serial/PingTimeoutInMsAccessor.java) (see Javadoc at [`PingTimeoutInMsAccessor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-serial/2.2.0/org.refcodes.serial/org/refcodes/serial/PingTimeoutInMsAccessor.html))
* \[<span style="color:blue">ADDED</span>\] [`PongMagicBytesAccessor.java`](https://bitbucket.org/refcodes/refcodes-serial/src/refcodes-serial-2.2.0/src/main/java/org/refcodes/serial/PongMagicBytesAccessor.java) (see Javadoc at [`PongMagicBytesAccessor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-serial/2.2.0/org.refcodes.serial/org/refcodes/serial/PongMagicBytesAccessor.html))
* \[<span style="color:red">DELETED</span>\] `TransmissionMetricsBuilder.java`
* \[<span style="color:red">DELETED</span>\] `TransmissionMetricsImpl.java`
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial/src/refcodes-serial-2.2.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-serial/src/refcodes-serial-2.2.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`AbstractCrcTransmissionDecorator.java`](https://bitbucket.org/refcodes/refcodes-serial/src/refcodes-serial-2.2.0/src/main/java/org/refcodes/serial/AbstractCrcTransmissionDecorator.java) (see Javadoc at [`AbstractCrcTransmissionDecorator.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-serial/2.2.0/org.refcodes.serial/org/refcodes/serial/AbstractCrcTransmissionDecorator.html))
* \[<span style="color:green">MODIFIED</span>\] [`AbstractStopAndWaitTransmissionDecorator.java`](https://bitbucket.org/refcodes/refcodes-serial/src/refcodes-serial-2.2.0/src/main/java/org/refcodes/serial/AbstractStopAndWaitTransmissionDecorator.java) (see Javadoc at [`AbstractStopAndWaitTransmissionDecorator.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-serial/2.2.0/org.refcodes.serial/org/refcodes/serial/AbstractStopAndWaitTransmissionDecorator.html))
* \[<span style="color:green">MODIFIED</span>\] [`AcknowledgeTimeoutInMsAccessor.java`](https://bitbucket.org/refcodes/refcodes-serial/src/refcodes-serial-2.2.0/src/main/java/org/refcodes/serial/AcknowledgeTimeoutInMsAccessor.java) (see Javadoc at [`AcknowledgeTimeoutInMsAccessor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-serial/2.2.0/org.refcodes.serial/org/refcodes/serial/AcknowledgeTimeoutInMsAccessor.html))
* \[<span style="color:green">MODIFIED</span>\] [`CrcSectionDecorator.java`](https://bitbucket.org/refcodes/refcodes-serial/src/refcodes-serial-2.2.0/src/main/java/org/refcodes/serial/CrcSectionDecorator.java) (see Javadoc at [`CrcSectionDecorator.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-serial/2.2.0/org.refcodes.serial/org/refcodes/serial/CrcSectionDecorator.html))
* \[<span style="color:green">MODIFIED</span>\] [`CrcSegmentDecorator.java`](https://bitbucket.org/refcodes/refcodes-serial/src/refcodes-serial-2.2.0/src/main/java/org/refcodes/serial/CrcSegmentDecorator.java) (see Javadoc at [`CrcSegmentDecorator.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-serial/2.2.0/org.refcodes.serial/org/refcodes/serial/CrcSegmentDecorator.html))
* \[<span style="color:green">MODIFIED</span>\] [`CrcSegmentPackager.java`](https://bitbucket.org/refcodes/refcodes-serial/src/refcodes-serial-2.2.0/src/main/java/org/refcodes/serial/CrcSegmentPackager.java) (see Javadoc at [`CrcSegmentPackager.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-serial/2.2.0/org.refcodes.serial/org/refcodes/serial/CrcSegmentPackager.html))
* \[<span style="color:green">MODIFIED</span>\] [`FlowControlRetryException.java`](https://bitbucket.org/refcodes/refcodes-serial/src/refcodes-serial-2.2.0/src/main/java/org/refcodes/serial/FlowControlRetryException.java) (see Javadoc at [`FlowControlRetryException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-serial/2.2.0/org.refcodes.serial/org/refcodes/serial/FlowControlRetryException.html))
* \[<span style="color:green">MODIFIED</span>\] [`FlowControlTimeoutException.java`](https://bitbucket.org/refcodes/refcodes-serial/src/refcodes-serial-2.2.0/src/main/java/org/refcodes/serial/FlowControlTimeoutException.java) (see Javadoc at [`FlowControlTimeoutException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-serial/2.2.0/org.refcodes.serial/org/refcodes/serial/FlowControlTimeoutException.html))
* \[<span style="color:green">MODIFIED</span>\] [`MagicBytes.java`](https://bitbucket.org/refcodes/refcodes-serial/src/refcodes-serial-2.2.0/src/main/java/org/refcodes/serial/MagicBytes.java) (see Javadoc at [`MagicBytes.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-serial/2.2.0/org.refcodes.serial/org/refcodes/serial/MagicBytes.html))
* \[<span style="color:green">MODIFIED</span>\] [`MagicBytesSectionMultiplexer.java`](https://bitbucket.org/refcodes/refcodes-serial/src/refcodes-serial-2.2.0/src/main/java/org/refcodes/serial/MagicBytesSectionMultiplexer.java) (see Javadoc at [`MagicBytesSectionMultiplexer.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-serial/2.2.0/org.refcodes.serial/org/refcodes/serial/MagicBytesSectionMultiplexer.html))
* \[<span style="color:green">MODIFIED</span>\] [`MagicBytesSegmentMultiplexer.java`](https://bitbucket.org/refcodes/refcodes-serial/src/refcodes-serial-2.2.0/src/main/java/org/refcodes/serial/MagicBytesSegmentMultiplexer.java) (see Javadoc at [`MagicBytesSegmentMultiplexer.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-serial/2.2.0/org.refcodes.serial/org/refcodes/serial/MagicBytesSegmentMultiplexer.html))
* \[<span style="color:green">MODIFIED</span>\] [`PacketInputStream.java`](https://bitbucket.org/refcodes/refcodes-serial/src/refcodes-serial-2.2.0/src/main/java/org/refcodes/serial/PacketInputStream.java) (see Javadoc at [`PacketInputStream.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-serial/2.2.0/org.refcodes.serial/org/refcodes/serial/PacketInputStream.html))
* \[<span style="color:green">MODIFIED</span>\] [`PacketOutputStream.java`](https://bitbucket.org/refcodes/refcodes-serial/src/refcodes-serial-2.2.0/src/main/java/org/refcodes/serial/PacketOutputStream.java) (see Javadoc at [`PacketOutputStream.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-serial/2.2.0/org.refcodes.serial/org/refcodes/serial/PacketOutputStream.html))
* \[<span style="color:green">MODIFIED</span>\] [`PortHub.java`](https://bitbucket.org/refcodes/refcodes-serial/src/refcodes-serial-2.2.0/src/main/java/org/refcodes/serial/PortHub.java) (see Javadoc at [`PortHub.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-serial/2.2.0/org.refcodes.serial/org/refcodes/serial/PortHub.html))
* \[<span style="color:green">MODIFIED</span>\] [`Port.java`](https://bitbucket.org/refcodes/refcodes-serial/src/refcodes-serial-2.2.0/src/main/java/org/refcodes/serial/Port.java) (see Javadoc at [`Port.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-serial/2.2.0/org.refcodes.serial/org/refcodes/serial/Port.html))
* \[<span style="color:green">MODIFIED</span>\] [`ReceiveSegmentConsumerDaemon.java`](https://bitbucket.org/refcodes/refcodes-serial/src/refcodes-serial-2.2.0/src/main/java/org/refcodes/serial/ReceiveSegmentConsumerDaemon.java) (see Javadoc at [`ReceiveSegmentConsumerDaemon.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-serial/2.2.0/org.refcodes.serial/org/refcodes/serial/ReceiveSegmentConsumerDaemon.html))
* \[<span style="color:green">MODIFIED</span>\] [`ReceiveSegmentResultDaemon.java`](https://bitbucket.org/refcodes/refcodes-serial/src/refcodes-serial-2.2.0/src/main/java/org/refcodes/serial/ReceiveSegmentResultDaemon.java) (see Javadoc at [`ReceiveSegmentResultDaemon.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-serial/2.2.0/org.refcodes.serial/org/refcodes/serial/ReceiveSegmentResultDaemon.html))
* \[<span style="color:green">MODIFIED</span>\] [`SerialSugar.java`](https://bitbucket.org/refcodes/refcodes-serial/src/refcodes-serial-2.2.0/src/main/java/org/refcodes/serial/SerialSugar.java) (see Javadoc at [`SerialSugar.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-serial/2.2.0/org.refcodes.serial/org/refcodes/serial/SerialSugar.html))
* \[<span style="color:green">MODIFIED</span>\] [`StopAndWaitPacketInputStream.java`](https://bitbucket.org/refcodes/refcodes-serial/src/refcodes-serial-2.2.0/src/main/java/org/refcodes/serial/StopAndWaitPacketInputStream.java) (see Javadoc at [`StopAndWaitPacketInputStream.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-serial/2.2.0/org.refcodes.serial/org/refcodes/serial/StopAndWaitPacketInputStream.html))
* \[<span style="color:green">MODIFIED</span>\] [`StopAndWaitPacketOutputStream.java`](https://bitbucket.org/refcodes/refcodes-serial/src/refcodes-serial-2.2.0/src/main/java/org/refcodes/serial/StopAndWaitPacketOutputStream.java) (see Javadoc at [`StopAndWaitPacketOutputStream.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-serial/2.2.0/org.refcodes.serial/org/refcodes/serial/StopAndWaitPacketOutputStream.html))
* \[<span style="color:green">MODIFIED</span>\] [`StopAndWaitPacketStreamSectionDecorator.java`](https://bitbucket.org/refcodes/refcodes-serial/src/refcodes-serial-2.2.0/src/main/java/org/refcodes/serial/StopAndWaitPacketStreamSectionDecorator.java) (see Javadoc at [`StopAndWaitPacketStreamSectionDecorator.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-serial/2.2.0/org.refcodes.serial/org/refcodes/serial/StopAndWaitPacketStreamSectionDecorator.html))
* \[<span style="color:green">MODIFIED</span>\] [`StopAndWaitPacketStreamSegmentDecorator.java`](https://bitbucket.org/refcodes/refcodes-serial/src/refcodes-serial-2.2.0/src/main/java/org/refcodes/serial/StopAndWaitPacketStreamSegmentDecorator.java) (see Javadoc at [`StopAndWaitPacketStreamSegmentDecorator.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-serial/2.2.0/org.refcodes.serial/org/refcodes/serial/StopAndWaitPacketStreamSegmentDecorator.html))
* \[<span style="color:green">MODIFIED</span>\] [`StopAndWaitSectionDecorator.java`](https://bitbucket.org/refcodes/refcodes-serial/src/refcodes-serial-2.2.0/src/main/java/org/refcodes/serial/StopAndWaitSectionDecorator.java) (see Javadoc at [`StopAndWaitSectionDecorator.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-serial/2.2.0/org.refcodes.serial/org/refcodes/serial/StopAndWaitSectionDecorator.html))
* \[<span style="color:green">MODIFIED</span>\] [`StopAndWaitSegmentDecorator.java`](https://bitbucket.org/refcodes/refcodes-serial/src/refcodes-serial-2.2.0/src/main/java/org/refcodes/serial/StopAndWaitSegmentDecorator.java) (see Javadoc at [`StopAndWaitSegmentDecorator.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-serial/2.2.0/org.refcodes.serial/org/refcodes/serial/StopAndWaitSegmentDecorator.html))
* \[<span style="color:green">MODIFIED</span>\] [`TransmissionMetrics.java`](https://bitbucket.org/refcodes/refcodes-serial/src/refcodes-serial-2.2.0/src/main/java/org/refcodes/serial/TransmissionMetrics.java) (see Javadoc at [`TransmissionMetrics.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-serial/2.2.0/org.refcodes.serial/org/refcodes/serial/TransmissionMetrics.html))
* \[<span style="color:green">MODIFIED</span>\] [`TransmissionTimeoutInMsAccessor.java`](https://bitbucket.org/refcodes/refcodes-serial/src/refcodes-serial-2.2.0/src/main/java/org/refcodes/serial/TransmissionTimeoutInMsAccessor.java) (see Javadoc at [`TransmissionTimeoutInMsAccessor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-serial/2.2.0/org.refcodes.serial/org/refcodes/serial/TransmissionTimeoutInMsAccessor.html))
* \[<span style="color:green">MODIFIED</span>\] [`TransmitSegmentConsumerDaemon.java`](https://bitbucket.org/refcodes/refcodes-serial/src/refcodes-serial-2.2.0/src/main/java/org/refcodes/serial/TransmitSegmentConsumerDaemon.java) (see Javadoc at [`TransmitSegmentConsumerDaemon.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-serial/2.2.0/org.refcodes.serial/org/refcodes/serial/TransmitSegmentConsumerDaemon.html))
* \[<span style="color:green">MODIFIED</span>\] [`TransmitSegmentResultDaemon.java`](https://bitbucket.org/refcodes/refcodes-serial/src/refcodes-serial-2.2.0/src/main/java/org/refcodes/serial/TransmitSegmentResultDaemon.java) (see Javadoc at [`TransmitSegmentResultDaemon.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-serial/2.2.0/org.refcodes.serial/org/refcodes/serial/TransmitSegmentResultDaemon.html))
* \[<span style="color:green">MODIFIED</span>\] [`ComplexTypeSegmentTest.java`](https://bitbucket.org/refcodes/refcodes-serial/src/refcodes-serial-2.2.0/src/test/java/org/refcodes/serial/ComplexTypeSegmentTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`FileTest.java`](https://bitbucket.org/refcodes/refcodes-serial/src/refcodes-serial-2.2.0/src/test/java/org/refcodes/serial/FileTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`PacketStreamTest.java`](https://bitbucket.org/refcodes/refcodes-serial/src/refcodes-serial-2.2.0/src/test/java/org/refcodes/serial/PacketStreamTest.java) 

## Change list &lt;refcodes-serial-alt&gt; (version 2.2.0)

* \[<span style="color:blue">ADDED</span>\] [`TtyPortHubSingleton.java`](https://bitbucket.org/refcodes/refcodes-serial-alt/src/refcodes-serial-alt-2.2.0/refcodes-serial-alt-tty/src/main/java/org/refcodes/serial/alt/tty/TtyPortHubSingleton.java) (see Javadoc at [`TtyPortHubSingleton.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-serial-alt-tty/2.2.0/org.refcodes.serial.alt.tty/org/refcodes/serial/alt/tty/TtyPortHubSingleton.html))
* \[<span style="color:blue">ADDED</span>\] [`TtyStopAndWaitTest.java`](https://bitbucket.org/refcodes/refcodes-serial-alt/src/refcodes-serial-alt-2.2.0/refcodes-serial-alt-tty/src/test/java/org/refcodes/serial/alt/tty/TtyStopAndWaitTest.java) 
* \[<span style="color:red">DELETED</span>\] `TtyPortMetricsBuilder.java`
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial-alt/src/refcodes-serial-alt-2.2.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-serial-alt/src/refcodes-serial-alt-2.2.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial-alt/src/refcodes-serial-alt-2.2.0/refcodes-serial-alt-net/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-serial-alt/src/refcodes-serial-alt-2.2.0/refcodes-serial-alt-net/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial-alt/src/refcodes-serial-alt-2.2.0/refcodes-serial-alt-tty/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-serial-alt/src/refcodes-serial-alt-2.2.0/refcodes-serial-alt-tty/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`TtyPortHub.java`](https://bitbucket.org/refcodes/refcodes-serial-alt/src/refcodes-serial-alt-2.2.0/refcodes-serial-alt-tty/src/main/java/org/refcodes/serial/alt/tty/TtyPortHub.java) (see Javadoc at [`TtyPortHub.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-serial-alt-tty/2.2.0/org.refcodes.serial.alt.tty/org/refcodes/serial/alt/tty/TtyPortHub.html))
* \[<span style="color:green">MODIFIED</span>\] [`TtyPortMetrics.java`](https://bitbucket.org/refcodes/refcodes-serial-alt/src/refcodes-serial-alt-2.2.0/refcodes-serial-alt-tty/src/main/java/org/refcodes/serial/alt/tty/TtyPortMetrics.java) (see Javadoc at [`TtyPortMetrics.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-serial-alt-tty/2.2.0/org.refcodes.serial.alt.tty/org/refcodes/serial/alt/tty/TtyPortMetrics.html))
* \[<span style="color:green">MODIFIED</span>\] [`AbstractTtyPortTest.java`](https://bitbucket.org/refcodes/refcodes-serial-alt/src/refcodes-serial-alt-2.2.0/refcodes-serial-alt-tty/src/test/java/org/refcodes/serial/alt/tty/AbstractTtyPortTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`TtyFileTest.java`](https://bitbucket.org/refcodes/refcodes-serial-alt/src/refcodes-serial-alt-2.2.0/refcodes-serial-alt-tty/src/test/java/org/refcodes/serial/alt/tty/TtyFileTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`TtyPacketStreamTest.java`](https://bitbucket.org/refcodes/refcodes-serial-alt/src/refcodes-serial-alt-2.2.0/refcodes-serial-alt-tty/src/test/java/org/refcodes/serial/alt/tty/TtyPacketStreamTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`TtyPortHubTest.java`](https://bitbucket.org/refcodes/refcodes-serial-alt/src/refcodes-serial-alt-2.2.0/refcodes-serial-alt-tty/src/test/java/org/refcodes/serial/alt/tty/TtyPortHubTest.java) 

## Change list &lt;refcodes-serial-ext&gt; (version 2.2.0)

* \[<span style="color:blue">ADDED</span>\] [`HandshakeTransmissionMetrics.java`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-2.2.0/refcodes-serial-ext-handshake/src/main/java/org/refcodes/serial/ext/handshake/HandshakeTransmissionMetrics.java) (see Javadoc at [`HandshakeTransmissionMetrics.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-serial-ext-handshake/2.2.0/org.refcodes.serial.ext.handshake/org/refcodes/serial/ext/handshake/HandshakeTransmissionMetrics.html))
* \[<span style="color:blue">ADDED</span>\] [`ReplyRetryNumberAccessor.java`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-2.2.0/refcodes-serial-ext-handshake/src/main/java/org/refcodes/serial/ext/handshake/ReplyRetryNumberAccessor.java) (see Javadoc at [`ReplyRetryNumberAccessor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-serial-ext-handshake/2.2.0/org.refcodes.serial.ext.handshake/org/refcodes/serial/ext/handshake/ReplyRetryNumberAccessor.html))
* \[<span style="color:blue">ADDED</span>\] [`ReplyTimeoutInMsAccessor.java`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-2.2.0/refcodes-serial-ext-handshake/src/main/java/org/refcodes/serial/ext/handshake/ReplyTimeoutInMsAccessor.java) (see Javadoc at [`ReplyTimeoutInMsAccessor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-serial-ext-handshake/2.2.0/org.refcodes.serial.ext.handshake/org/refcodes/serial/ext/handshake/ReplyTimeoutInMsAccessor.html))
* \[<span style="color:blue">ADDED</span>\] [`LoopbackHandshakePortControllerTest.java`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-2.2.0/refcodes-serial-ext-handshake/src/test/java/org/refcodes/serial/ext/handshake/LoopbackHandshakePortControllerTest.java) 
* \[<span style="color:red">DELETED</span>\] `TtyStopAndWaitTest.java`
* \[<span style="color:red">DELETED</span>\] `#Untitled-1#`
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-2.2.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-2.2.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-2.2.0/refcodes-serial-ext-handshake/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-2.2.0/refcodes-serial-ext-handshake/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`AcknowledgeMessage.java`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-2.2.0/refcodes-serial-ext-handshake/src/main/java/org/refcodes/serial/ext/handshake/AcknowledgeMessage.java) (see Javadoc at [`AcknowledgeMessage.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-serial-ext-handshake/2.2.0/org.refcodes.serial.ext.handshake/org/refcodes/serial/ext/handshake/AcknowledgeMessage.html))
* \[<span style="color:green">MODIFIED</span>\] [`AcknowledgeType.java`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-2.2.0/refcodes-serial-ext-handshake/src/main/java/org/refcodes/serial/ext/handshake/AcknowledgeType.java) (see Javadoc at [`AcknowledgeType.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-serial-ext-handshake/2.2.0/org.refcodes.serial.ext.handshake/org/refcodes/serial/ext/handshake/AcknowledgeType.html))
* \[<span style="color:green">MODIFIED</span>\] [`HandshakePortController.java`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-2.2.0/refcodes-serial-ext-handshake/src/main/java/org/refcodes/serial/ext/handshake/HandshakePortController.java) (see Javadoc at [`HandshakePortController.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-serial-ext-handshake/2.2.0/org.refcodes.serial.ext.handshake/org/refcodes/serial/ext/handshake/HandshakePortController.html))
* \[<span style="color:green">MODIFIED</span>\] [`SerialHandshakeSugar.java`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-2.2.0/refcodes-serial-ext-handshake/src/main/java/org/refcodes/serial/ext/handshake/SerialHandshakeSugar.java) (see Javadoc at [`SerialHandshakeSugar.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-serial-ext-handshake/2.2.0/org.refcodes.serial.ext.handshake/org/refcodes/serial/ext/handshake/SerialHandshakeSugar.html))
* \[<span style="color:green">MODIFIED</span>\] [`TransmissionMessage.java`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-2.2.0/refcodes-serial-ext-handshake/src/main/java/org/refcodes/serial/ext/handshake/TransmissionMessage.java) (see Javadoc at [`TransmissionMessage.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-serial-ext-handshake/2.2.0/org.refcodes.serial.ext.handshake/org/refcodes/serial/ext/handshake/TransmissionMessage.html))
* \[<span style="color:green">MODIFIED</span>\] [`TransmissionType.java`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-2.2.0/refcodes-serial-ext-handshake/src/main/java/org/refcodes/serial/ext/handshake/TransmissionType.java) (see Javadoc at [`TransmissionType.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-serial-ext-handshake/2.2.0/org.refcodes.serial.ext.handshake/org/refcodes/serial/ext/handshake/TransmissionType.html))
* \[<span style="color:green">MODIFIED</span>\] [`AbstractTtyPortTest.java`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-2.2.0/refcodes-serial-ext-handshake/src/test/java/org/refcodes/serial/ext/handshake/AbstractTtyPortTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`HandshakePortControllerTest.java`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-2.2.0/refcodes-serial-ext-handshake/src/test/java/org/refcodes/serial/ext/handshake/HandshakePortControllerTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`TtyHandshakePortControllerTest.java`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-2.2.0/refcodes-serial-ext-handshake/src/test/java/org/refcodes/serial/ext/handshake/TtyHandshakePortControllerTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-2.2.0/refcodes-serial-ext-observer/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-2.2.0/refcodes-serial-ext-observer/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-2.2.0/refcodes-serial-ext-security/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-2.2.0/refcodes-serial-ext-security/README.md) 

## Change list &lt;refcodes-p2p&gt; (version 2.2.0)

* \[<span style="color:blue">ADDED</span>\] [`AbstractP2PTail.java`](https://bitbucket.org/refcodes/refcodes-p2p/src/refcodes-p2p-2.2.0/src/main/java/org/refcodes/p2p/AbstractP2PTail.java) (see Javadoc at [`AbstractP2PTail.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-p2p/2.2.0/org.refcodes.p2p/org/refcodes/p2p/AbstractP2PTail.html))
* \[<span style="color:blue">ADDED</span>\] [`P2PTail.java`](https://bitbucket.org/refcodes/refcodes-p2p/src/refcodes-p2p-2.2.0/src/main/java/org/refcodes/p2p/P2PTail.java) (see Javadoc at [`P2PTail.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-p2p/2.2.0/org.refcodes.p2p/org/refcodes/p2p/P2PTail.html))
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-p2p/src/refcodes-p2p-2.2.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-p2p/src/refcodes-p2p-2.2.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`AbstractP2PHeader.java`](https://bitbucket.org/refcodes/refcodes-p2p/src/refcodes-p2p-2.2.0/src/main/java/org/refcodes/p2p/AbstractP2PHeader.java) (see Javadoc at [`AbstractP2PHeader.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-p2p/2.2.0/org.refcodes.p2p/org/refcodes/p2p/AbstractP2PHeader.html))
* \[<span style="color:green">MODIFIED</span>\] [`AbstractP2PMessage.java`](https://bitbucket.org/refcodes/refcodes-p2p/src/refcodes-p2p-2.2.0/src/main/java/org/refcodes/p2p/AbstractP2PMessage.java) (see Javadoc at [`AbstractP2PMessage.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-p2p/2.2.0/org.refcodes.p2p/org/refcodes/p2p/AbstractP2PMessage.html))
* \[<span style="color:green">MODIFIED</span>\] [`AbstractPeer.java`](https://bitbucket.org/refcodes/refcodes-p2p/src/refcodes-p2p-2.2.0/src/main/java/org/refcodes/p2p/AbstractPeer.java) (see Javadoc at [`AbstractPeer.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-p2p/2.2.0/org.refcodes.p2p/org/refcodes/p2p/AbstractPeer.html))
* \[<span style="color:green">MODIFIED</span>\] [`P2PHeader.java`](https://bitbucket.org/refcodes/refcodes-p2p/src/refcodes-p2p-2.2.0/src/main/java/org/refcodes/p2p/P2PHeader.java) (see Javadoc at [`P2PHeader.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-p2p/2.2.0/org.refcodes.p2p/org/refcodes/p2p/P2PHeader.html))
* \[<span style="color:green">MODIFIED</span>\] [`P2PMessageConsumer.java`](https://bitbucket.org/refcodes/refcodes-p2p/src/refcodes-p2p-2.2.0/src/main/java/org/refcodes/p2p/P2PMessageConsumer.java) (see Javadoc at [`P2PMessageConsumer.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-p2p/2.2.0/org.refcodes.p2p/org/refcodes/p2p/P2PMessageConsumer.html))
* \[<span style="color:green">MODIFIED</span>\] [`P2PMessage.java`](https://bitbucket.org/refcodes/refcodes-p2p/src/refcodes-p2p-2.2.0/src/main/java/org/refcodes/p2p/P2PMessage.java) (see Javadoc at [`P2PMessage.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-p2p/2.2.0/org.refcodes.p2p/org/refcodes/p2p/P2PMessage.html))
* \[<span style="color:green">MODIFIED</span>\] [`Peer.java`](https://bitbucket.org/refcodes/refcodes-p2p/src/refcodes-p2p-2.2.0/src/main/java/org/refcodes/p2p/Peer.java) (see Javadoc at [`Peer.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-p2p/2.2.0/org.refcodes.p2p/org/refcodes/p2p/Peer.html))
* \[<span style="color:green">MODIFIED</span>\] [`PeerProxy.java`](https://bitbucket.org/refcodes/refcodes-p2p/src/refcodes-p2p-2.2.0/src/main/java/org/refcodes/p2p/PeerProxy.java) (see Javadoc at [`PeerProxy.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-p2p/2.2.0/org.refcodes.p2p/org/refcodes/p2p/PeerProxy.html))
* \[<span style="color:green">MODIFIED</span>\] [`PeerRouter.java`](https://bitbucket.org/refcodes/refcodes-p2p/src/refcodes-p2p-2.2.0/src/main/java/org/refcodes/p2p/PeerRouter.java) (see Javadoc at [`PeerRouter.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-p2p/2.2.0/org.refcodes.p2p/org/refcodes/p2p/PeerRouter.html))

## Change list &lt;refcodes-p2p-alt&gt; (version 2.2.0)

* \[<span style="color:blue">ADDED</span>\] [`AcknowledgeMode.java`](https://bitbucket.org/refcodes/refcodes-p2p-alt/src/refcodes-p2p-alt-2.2.0/refcodes-p2p-alt-serial/src/main/java/org/refcodes/p2p/alt/serial/AcknowledgeMode.java) (see Javadoc at [`AcknowledgeMode.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-p2p-alt-serial/2.2.0/org.refcodes.p2p.alt.serial/org/refcodes/p2p/alt/serial/AcknowledgeMode.html))
* \[<span style="color:blue">ADDED</span>\] [`SerialP2PTransmissionMetrics.java`](https://bitbucket.org/refcodes/refcodes-p2p-alt/src/refcodes-p2p-alt-2.2.0/refcodes-p2p-alt-serial/src/main/java/org/refcodes/p2p/alt/serial/SerialP2PTransmissionMetrics.java) (see Javadoc at [`SerialP2PTransmissionMetrics.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-p2p-alt-serial/2.2.0/org.refcodes.p2p.alt.serial/org/refcodes/p2p/alt/serial/SerialP2PTransmissionMetrics.html))
* \[<span style="color:red">DELETED</span>\] `HopCountResponse.java`
* \[<span style="color:red">DELETED</span>\] `SerialP2PResponse.java`
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-p2p-alt/src/refcodes-p2p-alt-2.2.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-p2p-alt/src/refcodes-p2p-alt-2.2.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-p2p-alt/src/refcodes-p2p-alt-2.2.0/refcodes-p2p-alt-rest/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-p2p-alt/src/refcodes-p2p-alt-2.2.0/refcodes-p2p-alt-serial/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`module-info.java`](https://bitbucket.org/refcodes/refcodes-p2p-alt/src/refcodes-p2p-alt-2.2.0/refcodes-p2p-alt-serial/src/main/java/module-info.java) (see Javadoc at [`module-info.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-p2p-alt-serial/2.2.0//module-info.html))
* \[<span style="color:green">MODIFIED</span>\] [`SerialP2PHeader.java`](https://bitbucket.org/refcodes/refcodes-p2p-alt/src/refcodes-p2p-alt-2.2.0/refcodes-p2p-alt-serial/src/main/java/org/refcodes/p2p/alt/serial/SerialP2PHeader.java) (see Javadoc at [`SerialP2PHeader.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-p2p-alt-serial/2.2.0/org.refcodes.p2p.alt.serial/org/refcodes/p2p/alt/serial/SerialP2PHeader.html))
* \[<span style="color:green">MODIFIED</span>\] [`SerialP2PMessage.java`](https://bitbucket.org/refcodes/refcodes-p2p-alt/src/refcodes-p2p-alt-2.2.0/refcodes-p2p-alt-serial/src/main/java/org/refcodes/p2p/alt/serial/SerialP2PMessage.java) (see Javadoc at [`SerialP2PMessage.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-p2p-alt-serial/2.2.0/org.refcodes.p2p.alt.serial/org/refcodes/p2p/alt/serial/SerialP2PMessage.html))
* \[<span style="color:green">MODIFIED</span>\] [`SerialPeer.java`](https://bitbucket.org/refcodes/refcodes-p2p-alt/src/refcodes-p2p-alt-2.2.0/refcodes-p2p-alt-serial/src/main/java/org/refcodes/p2p/alt/serial/SerialPeer.java) (see Javadoc at [`SerialPeer.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-p2p-alt-serial/2.2.0/org.refcodes.p2p.alt.serial/org/refcodes/p2p/alt/serial/SerialPeer.html))
* \[<span style="color:green">MODIFIED</span>\] [`SerialPeerProxy.java`](https://bitbucket.org/refcodes/refcodes-p2p-alt/src/refcodes-p2p-alt-2.2.0/refcodes-p2p-alt-serial/src/main/java/org/refcodes/p2p/alt/serial/SerialPeerProxy.java) (see Javadoc at [`SerialPeerProxy.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-p2p-alt-serial/2.2.0/org.refcodes.p2p.alt.serial/org/refcodes/p2p/alt/serial/SerialPeerProxy.html))
* \[<span style="color:green">MODIFIED</span>\] [`SerialPeerRouter.java`](https://bitbucket.org/refcodes/refcodes-p2p-alt/src/refcodes-p2p-alt-2.2.0/refcodes-p2p-alt-serial/src/main/java/org/refcodes/p2p/alt/serial/SerialPeerRouter.java) (see Javadoc at [`SerialPeerRouter.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-p2p-alt-serial/2.2.0/org.refcodes.p2p.alt.serial/org/refcodes/p2p/alt/serial/SerialPeerRouter.html))
* \[<span style="color:green">MODIFIED</span>\] [`AbstractTtyPortTest.java`](https://bitbucket.org/refcodes/refcodes-p2p-alt/src/refcodes-p2p-alt-2.2.0/refcodes-p2p-alt-serial/src/test/java/org/refcodes/p2p/alt/serial/AbstractTtyPortTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`LoopbackPeerTest.java`](https://bitbucket.org/refcodes/refcodes-p2p-alt/src/refcodes-p2p-alt-2.2.0/refcodes-p2p-alt-serial/src/test/java/org/refcodes/p2p/alt/serial/LoopbackPeerTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`TtyPeerTest.java`](https://bitbucket.org/refcodes/refcodes-p2p-alt/src/refcodes-p2p-alt-2.2.0/refcodes-p2p-alt-serial/src/test/java/org/refcodes/p2p/alt/serial/TtyPeerTest.java) 

## Change list &lt;refcodes-p2p-ext&gt; (version 2.2.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-p2p-ext/src/refcodes-p2p-ext-2.2.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-p2p-ext/src/refcodes-p2p-ext-2.2.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-p2p-ext/src/refcodes-p2p-ext-2.2.0/refcodes-p2p-ext-observer/pom.xml) 

## Change list &lt;refcodes-servicebus&gt; (version 2.2.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-servicebus/src/refcodes-servicebus-2.2.0/pom.xml) 

## Change list &lt;refcodes-servicebus-alt&gt; (version 2.2.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-servicebus-alt/src/refcodes-servicebus-alt-2.2.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-servicebus-alt/src/refcodes-servicebus-alt-2.2.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-servicebus-alt/src/refcodes-servicebus-alt-2.2.0/refcodes-servicebus-alt-spring/pom.xml) 

## Change list &lt;refcodes-tabular-alt&gt; (version 2.2.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-tabular-alt/src/refcodes-tabular-alt-2.2.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-tabular-alt/src/refcodes-tabular-alt-2.2.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-tabular-alt/src/refcodes-tabular-alt-2.2.0/refcodes-tabular-alt-forwardsecrecy/pom.xml) 

## Change list &lt;refcodes-archetype-alt&gt; (version 2.2.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-2.2.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-2.2.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-2.2.0/refcodes-archetype-alt-c2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-2.2.0/refcodes-archetype-alt-c2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-2.2.0/refcodes-archetype-alt-c2/src/main/resources/archetype-resources/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`Main.java`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-2.2.0/refcodes-archetype-alt-c2/src/main/resources/archetype-resources/src/main/java/Main.java) (see Javadoc at [`Main.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-archetype-alt-c2/src/main/resources/archetype-resources/2.2.0/src.main.resources.archetype-resources/Main.html))
* \[<span style="color:green">MODIFIED</span>\] [`runtimelogger.ini`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-2.2.0/refcodes-archetype-alt-c2/src/main/resources/archetype-resources/src/main/resources/runtimelogger.ini) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-2.2.0/refcodes-archetype-alt-cli/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-2.2.0/refcodes-archetype-alt-cli/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-2.2.0/refcodes-archetype-alt-cli/src/main/resources/archetype-resources/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`Main.java`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-2.2.0/refcodes-archetype-alt-cli/src/main/resources/archetype-resources/src/main/java/Main.java) (see Javadoc at [`Main.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-archetype-alt-cli/src/main/resources/archetype-resources/2.2.0/src.main.resources.archetype-resources/Main.html))
* \[<span style="color:green">MODIFIED</span>\] [`runtimelogger.ini`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-2.2.0/refcodes-archetype-alt-cli/src/main/resources/archetype-resources/src/main/resources/runtimelogger.ini) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-2.2.0/refcodes-archetype-alt-csv/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-2.2.0/refcodes-archetype-alt-csv/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-2.2.0/refcodes-archetype-alt-csv/src/main/resources/archetype-resources/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`Main.java`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-2.2.0/refcodes-archetype-alt-csv/src/main/resources/archetype-resources/src/main/java/Main.java) (see Javadoc at [`Main.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-archetype-alt-csv/src/main/resources/archetype-resources/2.2.0/src.main.resources.archetype-resources/Main.html))
* \[<span style="color:green">MODIFIED</span>\] [`runtimelogger.ini`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-2.2.0/refcodes-archetype-alt-csv/src/main/resources/archetype-resources/src/main/resources/runtimelogger.ini) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-2.2.0/refcodes-archetype-alt-eventbus/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-2.2.0/refcodes-archetype-alt-eventbus/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-2.2.0/refcodes-archetype-alt-eventbus/src/main/resources/archetype-resources/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`Main.java`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-2.2.0/refcodes-archetype-alt-eventbus/src/main/resources/archetype-resources/src/main/java/Main.java) (see Javadoc at [`Main.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-archetype-alt-eventbus/src/main/resources/archetype-resources/2.2.0/src.main.resources.archetype-resources/Main.html))
* \[<span style="color:green">MODIFIED</span>\] [`runtimelogger.ini`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-2.2.0/refcodes-archetype-alt-eventbus/src/main/resources/archetype-resources/src/main/resources/runtimelogger.ini) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-2.2.0/refcodes-archetype-alt-rest/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-2.2.0/refcodes-archetype-alt-rest/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-2.2.0/refcodes-archetype-alt-rest/src/main/resources/archetype-resources/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`Main.java`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-2.2.0/refcodes-archetype-alt-rest/src/main/resources/archetype-resources/src/main/java/Main.java) (see Javadoc at [`Main.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-archetype-alt-rest/src/main/resources/archetype-resources/2.2.0/src.main.resources.archetype-resources/Main.html))
* \[<span style="color:green">MODIFIED</span>\] [`runtimelogger.ini`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-2.2.0/refcodes-archetype-alt-rest/src/main/resources/archetype-resources/src/main/resources/runtimelogger.ini) 
