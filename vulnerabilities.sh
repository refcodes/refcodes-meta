# /////////////////////////////////////////////////////////////////////////////
# REFCODES.ORG
# =============================================================================
# This code is copyright (c) by Siegfried Steiner, Munich, Germany and licensed
# under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
# licenses:
# =============================================================================
# GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
# =============================================================================
# Apache License, v2.0 ("http://www.apache.org/licenses/LICENSE-2.0")
# =============================================================================
# Please contact the copyright holding author(s) of the software artifacts in
# question for licensing issues not being coveFG_RED by the above listed licenses,
# also regarding commercial licensing models or regarding the compatibility
# with other open source licenses.
# /////////////////////////////////////////////////////////////////////////////
#!/bin/bash

# ------------------------------------------------------------------------------
# INIT:
# ------------------------------------------------------------------------------

CURRENT_PATH="$(pwd)"
SCRIPT_PATH="$(dirname $0)"
cd "${SCRIPT_PATH}"
SCRIPT_PATH="$(pwd)"
cd "${CURRENT_PATH}"
SCRIPT_DIR="${SCRIPT_PATH##*/}"
SCRIPT_NAME="$(basename $0 .sh)"
PARENT_PATH="$(realpath $(dirname $0)/..)"
PARENT_DIR="${PARENT_PATH##*/}"
MODULE_NAME="$(echo -e "${SCRIPT_DIR}" | cut -d- -f3- )"
if [ -z "${MODULE_NAME}" ]; then
	MODULE_NAME="$(echo -e "${SCRIPT_DIR}" | cut -d- -f2- )"
	if [ -z "${MODULE_NAME}" ]; then
		MODULE_NAME="${SCRIPT_DIR}"
	fi
fi
if [ -z ${COLUMNS} ] ; then
	export COLUMNS=$(tput cols)
fi

# ------------------------------------------------------------------------------
# ANSI ESCAPE CODES:
# ------------------------------------------------------------------------------

ESC_BOLD="\E[1m"
ESC_FAINT="\E[2m"
ESC_ITALIC="\E[3m"
ESC_UNDERLINE="\E[4m"
ESC_FG_RED="\E[31m"
ESC_FG_GREEN="\E[32m"
ESC_FG_YELLOW="\E[33m"
ESC_FG_BLUE="\E[34m"
ESC_FG_MAGENTA="\E[35m"
ESC_FG_CYAN="\E[36m"
ESC_FG_WHITE="\E[37m"
ESC_RESET="\E[0m"

# ------------------------------------------------------------------------------
# PRINTLN:
# ------------------------------------------------------------------------------

function printLn {
	char="-"
	if [[ $# == 1 ]] ; then
		char="$1"
	fi
	echo -en "${ESC_FAINT}"
	for (( i=0; i< ${COLUMNS}; i++ )) ; do
		echo -en "${char}"
	done
	echo -e "${ESC_RESET}"
}

# ------------------------------------------------------------------------------
# QUIT:
# ------------------------------------------------------------------------------

function quit {
	input=""
	while ([ "$input" != "q" ] && [ "$input" != "y" ]); do
		echo -ne "> Continue? Enter [${ESC_BOLD}q${ESC_RESET}] to quit, [${ESC_BOLD}y${ESC_RESET}] to continue: ";
		read input;
	done
	# printf '%*s\n' "${COLUMNS:-$(tput cols)}" '' | tr ' ' -
	if [ "$input" == "q" ] ; then
		printLn
		echo -e "> ${ESC_BOLD}Aborting due to user input.${ESC_RESET}"
		exit
	fi
}

# ------------------------------------------------------------------------------
# BANNER:
# ------------------------------------------------------------------------------

function printBanner {
	banner=$( figlet -w 999 "/${MODULE_NAME}:>>>${SCRIPT_NAME}..." 2> /dev/null )
	if [ $? -eq 0 ]; then
		echo "${banner}" | cut -c -${COLUMNS}
	else
		banner "${SCRIPT_NAME}..." 2> /dev/null
		if [ $? -ne 0 ]; then
			echo -e "> ${SCRIPT_NAME}:" | tr a-z A-Z 
		fi
	fi
}

printBanner

# ------------------------------------------------------------------------------
# HELP:
# ------------------------------------------------------------------------------

function printHelp {
	printLn
	echo -e "Usage: ${ESC_BOLD}${SCRIPT_NAME}${ESC_RESET}.sh [-h] | [-o] [-s]"
	printLn
	echo -e "Creates and analyzes the dependency vulnerability reports for all artifacts."
	printLn
	echo -e "${ESC_BOLD}-s${ESC_RESET}: Skips running the OWASP vulnerability scan (must have been run before)"
	echo -e "${ESC_BOLD}-o${ESC_RESET}: Opens reports with vulnerability in firefox"
	echo -e "${ESC_BOLD}-h${ESC_RESET}: Print this help"
	printLn
}

# ------------------------------------------------------------------------------
# ARGS:
# ------------------------------------------------------------------------------

POSITIONAL_ARGS=()
while [[ $# -gt 0 ]]; do
  case $1 in
    -h|--help)
      printHelp
			exit 0
      ;;
    -o|--open) 
      open=1
      shift # Skip $1
      # shift # Skip $2 if $1 has an argument
      ;;
    -s|--skip)
      skip=1
      shift # Skip $1
      # shift # Skip $2 if $1 has an argument
      ;;
    -*|--*)
			printHelp
			echo -e "> Unknown argument <${ESC_BOLD}${ESC_FG_RED}$1${ESC_RESET}>!"
      exit 1
      ;;
    *)
      POSITIONAL_ARGS+=("$1") # Save positional arg
      shift # Past argument
      ;;
  esac
done
set -- "${POSITIONAL_ARGS[@]}"

# ------------------------------------------------------------------------------
# MAIN:
# ------------------------------------------------------------------------------

vendor="${SCRIPT_DIR/-meta/}"
echo -e "> Checking ${vendor} artifacts' dependencies for vulnerabilities..."
cd ${SCRIPT_PATH}
printLn
if [ ! "$skip" = "1" ]; then
	mvn -DskipTests verify -P owasp
fi
#printLn
cd ..
reports=$(find . -type f  -name dependency-check-report.html)
NEWLINE=$'\n'
while IFS= read -r path; do
	artifact=$(echo "${path}" | cut -d/ -f3)
	if [ "${artifact}" = "target" ] ; then	
		artifact=$(echo "${path}" | cut -d/ -f2)
	fi
	artifacts="${artifacts}${NEWLINE}${artifact}"
done <<< "${reports}"
tabsize=$(echo "${artifacts}" | awk '{ print length }' | sort -n | tail -1)
tabsize=$(($tabsize + 4))
count=0
overall=0
newWindow="1"
while IFS= read -r path; do
	overall=$(($overall + 1))
	artifact=$(echo "${path}" | cut -d/ -f3)
	if [ "${artifact}" = "target" ] ; then	
		artifact=$(echo "${path}" | cut -d/ -f2)
	fi
	vulnerabilities=$(cat "${path}" | grep -oP '(?<=\<li\>\<i\>Vulnerabilities Found\<\/i\>\:&nbsp;)[0-9]+'  )
	if [ ! "${vulnerabilities}" = "0" ] ; then
		count=$(($count + 1))
		number="${ESC_BOLD}${ESC_FG_RED}${vulnerabilities}${ESC_RESET}"
		if [[ "$open" == "1" ]]; then
			if [ "${newWindow}" = "1" ] ; then
				firefox --new-window "${path}"
				newWindow="0"
			else
				firefox --new-tab "${path}"
			fi
		fi
	else
		number="${ESC_BOLD}${ESC_FG_GREEN}${vulnerabilities}${ESC_RESET}"
	fi
	echo -e "> ${artifact}:\t[${number}] vulnerabilities found (${ESC_BOLD}${ESC_ITALIC}file://${ESC_RESET}${ESC_ITALIC}$(realpath ${path})${ESC_RESET})" | expand -t ${tabsize}
done <<< "${reports}"
printLn
if [ ! "${count}" = "0" ] ; then
	number="${ESC_BOLD}${ESC_FG_RED}${count}${ESC_RESET}"
else
	number="${ESC_BOLD}${ESC_FG_GREEN}${count}${ESC_RESET}"
fi
echo -e "> Found <${number}> of a total of <${ESC_BOLD}${overall}${ESC_RESET}> artifacts with vulnerabilities!"

printLn
if [ ! "${count}" = "0" ] ; then
	echo -e "${ESC_FG_RED}!${ESC_RESET} Check for dependnecy updates with <${ESC_BOLD}${ESC_ITALIC}mvn versions:display-dependency-updates${ESC_RESET}> in parent artifact to fix any vulnerabilities!"
else
	echo -e "> Check for dependnecy updates with <${ESC_BOLD}${ESC_ITALIC}mvn versions:display-dependency-updates${ESC_RESET}> in parent artifact ..."
fi
echo -e "> Check for plugin updates with <${ESC_BOLD}${ESC_ITALIC}mvn versions:display-plugin-updates${ESC_RESET}> in parent artifact ..."

cd "${CURRENT_PATH}"
# echo -e "> Done."
