# Change list for REFCODES.ORG artifacts' version 2.0.1

> This change list has been auto-generated on `triton.local` by `steiner` with `changelist-all.sh` on the 2019-04-29 at 06:56:43.

## Change list &lt;refcodes-licensing&gt; (version 2.0.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-licensing/src/refcodes-licensing-2.0.1/pom.xml) 

## Change list &lt;refcodes-parent&gt; (version 2.0.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-parent/src/refcodes-parent-2.0.1/pom.xml) 

## Change list &lt;refcodes-time&gt; (version 2.0.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-time/src/refcodes-time-2.0.1/pom.xml) 

## Change list &lt;refcodes-mixin&gt; (version 2.0.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-mixin/src/refcodes-mixin-2.0.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`Dumpable.java`](https://bitbucket.org/refcodes/refcodes-mixin/src/refcodes-mixin-2.0.1/src/main/java/org/refcodes/mixin/Dumpable.java) (see Javadoc at [`Dumpable.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-mixin/2.0.1/org/refcodes/mixin/Dumpable.html))
* \[<span style="color:green">MODIFIED</span>\] [`MillisecondsAccessor.java`](https://bitbucket.org/refcodes/refcodes-mixin/src/refcodes-mixin-2.0.1/src/main/java/org/refcodes/mixin/MillisecondsAccessor.java) (see Javadoc at [`MillisecondsAccessor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-mixin/2.0.1/org/refcodes/mixin/MillisecondsAccessor.html))

## Change list &lt;refcodes-data&gt; (version 2.0.1)

* \[<span style="color:blue">ADDED</span>\] [`IoLoopSleepTime.java`](https://bitbucket.org/refcodes/refcodes-data/src/refcodes-data-2.0.1/src/main/java/org/refcodes/data/IoLoopSleepTime.java) (see Javadoc at [`IoLoopSleepTime.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-data/2.0.1/org/refcodes/data/IoLoopSleepTime.html))
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data/src/refcodes-data-2.0.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`AsciiColorPalette.java`](https://bitbucket.org/refcodes/refcodes-data/src/refcodes-data-2.0.1/src/main/java/org/refcodes/data/AsciiColorPalette.java) (see Javadoc at [`AsciiColorPalette.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-data/2.0.1/org/refcodes/data/AsciiColorPalette.html))
* \[<span style="color:green">MODIFIED</span>\] [`CharSet.java`](https://bitbucket.org/refcodes/refcodes-data/src/refcodes-data-2.0.1/src/main/java/org/refcodes/data/CharSet.java) (see Javadoc at [`CharSet.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-data/2.0.1/org/refcodes/data/CharSet.html))
* \[<span style="color:green">MODIFIED</span>\] [`DaemonLoopSleepTime.java`](https://bitbucket.org/refcodes/refcodes-data/src/refcodes-data-2.0.1/src/main/java/org/refcodes/data/DaemonLoopSleepTime.java) (see Javadoc at [`DaemonLoopSleepTime.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-data/2.0.1/org/refcodes/data/DaemonLoopSleepTime.html))
* \[<span style="color:green">MODIFIED</span>\] [`IoRetryCount.java`](https://bitbucket.org/refcodes/refcodes-data/src/refcodes-data-2.0.1/src/main/java/org/refcodes/data/IoRetryCount.java) (see Javadoc at [`IoRetryCount.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-data/2.0.1/org/refcodes/data/IoRetryCount.html))
* \[<span style="color:green">MODIFIED</span>\] [`IoTimeout.java`](https://bitbucket.org/refcodes/refcodes-data/src/refcodes-data-2.0.1/src/main/java/org/refcodes/data/IoTimeout.java) (see Javadoc at [`IoTimeout.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-data/2.0.1/org/refcodes/data/IoTimeout.html))
* \[<span style="color:green">MODIFIED</span>\] [`LatencySleepTime.java`](https://bitbucket.org/refcodes/refcodes-data/src/refcodes-data-2.0.1/src/main/java/org/refcodes/data/LatencySleepTime.java) (see Javadoc at [`LatencySleepTime.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-data/2.0.1/org/refcodes/data/LatencySleepTime.html))
* \[<span style="color:green">MODIFIED</span>\] [`LoopExtensionTime.java`](https://bitbucket.org/refcodes/refcodes-data/src/refcodes-data-2.0.1/src/main/java/org/refcodes/data/LoopExtensionTime.java) (see Javadoc at [`LoopExtensionTime.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-data/2.0.1/org/refcodes/data/LoopExtensionTime.html))
* \[<span style="color:green">MODIFIED</span>\] [`LoopSleepTime.java`](https://bitbucket.org/refcodes/refcodes-data/src/refcodes-data-2.0.1/src/main/java/org/refcodes/data/LoopSleepTime.java) (see Javadoc at [`LoopSleepTime.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-data/2.0.1/org/refcodes/data/LoopSleepTime.html))
* \[<span style="color:green">MODIFIED</span>\] [`PollLoopTime.java`](https://bitbucket.org/refcodes/refcodes-data/src/refcodes-data-2.0.1/src/main/java/org/refcodes/data/PollLoopTime.java) (see Javadoc at [`PollLoopTime.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-data/2.0.1/org/refcodes/data/PollLoopTime.html))
* \[<span style="color:green">MODIFIED</span>\] [`Prefix.java`](https://bitbucket.org/refcodes/refcodes-data/src/refcodes-data-2.0.1/src/main/java/org/refcodes/data/Prefix.java) (see Javadoc at [`Prefix.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-data/2.0.1/org/refcodes/data/Prefix.html))
* \[<span style="color:green">MODIFIED</span>\] [`RetryCount.java`](https://bitbucket.org/refcodes/refcodes-data/src/refcodes-data-2.0.1/src/main/java/org/refcodes/data/RetryCount.java) (see Javadoc at [`RetryCount.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-data/2.0.1/org/refcodes/data/RetryCount.html))
* \[<span style="color:green">MODIFIED</span>\] [`RetryLoopCount.java`](https://bitbucket.org/refcodes/refcodes-data/src/refcodes-data-2.0.1/src/main/java/org/refcodes/data/RetryLoopCount.java) (see Javadoc at [`RetryLoopCount.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-data/2.0.1/org/refcodes/data/RetryLoopCount.html))
* \[<span style="color:green">MODIFIED</span>\] [`ScheduleSleepTime.java`](https://bitbucket.org/refcodes/refcodes-data/src/refcodes-data-2.0.1/src/main/java/org/refcodes/data/ScheduleSleepTime.java) (see Javadoc at [`ScheduleSleepTime.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-data/2.0.1/org/refcodes/data/ScheduleSleepTime.html))
* \[<span style="color:green">MODIFIED</span>\] [`SystemProperty.java`](https://bitbucket.org/refcodes/refcodes-data/src/refcodes-data-2.0.1/src/main/java/org/refcodes/data/SystemProperty.java) (see Javadoc at [`SystemProperty.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-data/2.0.1/org/refcodes/data/SystemProperty.html))

## Change list &lt;refcodes-exception&gt; (version 2.0.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-exception/src/refcodes-exception-2.0.1/pom.xml) 

## Change list &lt;refcodes-factory&gt; (version 2.0.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-factory/src/refcodes-factory-2.0.1/pom.xml) 

## Change list &lt;refcodes-factory-alt&gt; (version 2.0.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-factory-alt/src/refcodes-factory-alt-2.0.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-factory-alt/src/refcodes-factory-alt-2.0.1/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-factory-alt/src/refcodes-factory-alt-2.0.1/refcodes-factory-alt-spring/pom.xml) 

## Change list &lt;refcodes-controlflow&gt; (version 2.0.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-controlflow/src/refcodes-controlflow-2.0.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`ControlFlowUtility.java`](https://bitbucket.org/refcodes/refcodes-controlflow/src/refcodes-controlflow-2.0.1/src/main/java/org/refcodes/controlflow/ControlFlowUtility.java) (see Javadoc at [`ControlFlowUtility.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-controlflow/2.0.1/org/refcodes/controlflow/ControlFlowUtility.html))
* \[<span style="color:green">MODIFIED</span>\] [`ExceptionWatchdogImpl.java`](https://bitbucket.org/refcodes/refcodes-controlflow/src/refcodes-controlflow-2.0.1/src/main/java/org/refcodes/controlflow/ExceptionWatchdogImpl.java) (see Javadoc at [`ExceptionWatchdogImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-controlflow/2.0.1/org/refcodes/controlflow/ExceptionWatchdogImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`RetryCounterTest.java`](https://bitbucket.org/refcodes/refcodes-controlflow/src/refcodes-controlflow-2.0.1/src/test/java/org/refcodes/controlflow/RetryCounterTest.java) 

## Change list &lt;refcodes-numerical&gt; (version 2.0.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-numerical/src/refcodes-numerical-2.0.1/pom.xml) 

## Change list &lt;refcodes-generator&gt; (version 2.0.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-generator/src/refcodes-generator-2.0.1/pom.xml) 

## Change list &lt;refcodes-matcher&gt; (version 2.0.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-matcher/src/refcodes-matcher-2.0.1/pom.xml) 

## Change list &lt;refcodes-structure&gt; (version 2.0.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-2.0.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-2.0.1/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`CanonicalMap.java`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-2.0.1/src/main/java/org/refcodes/structure/CanonicalMap.java) (see Javadoc at [`CanonicalMap.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-structure/2.0.1/org/refcodes/structure/CanonicalMap.html))
* \[<span style="color:green">MODIFIED</span>\] [`PathComparator.java`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-2.0.1/src/main/java/org/refcodes/structure/PathComparator.java) (see Javadoc at [`PathComparator.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-structure/2.0.1/org/refcodes/structure/PathComparator.html))
* \[<span style="color:green">MODIFIED</span>\] [`PathMap.java`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-2.0.1/src/main/java/org/refcodes/structure/PathMap.java) (see Javadoc at [`PathMap.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-structure/2.0.1/org/refcodes/structure/PathMap.html))
* \[<span style="color:green">MODIFIED</span>\] [`TypeUtility.java`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-2.0.1/src/main/java/org/refcodes/structure/TypeUtility.java) (see Javadoc at [`TypeUtility.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-structure/2.0.1/org/refcodes/structure/TypeUtility.html))

## Change list &lt;refcodes-structure-ext&gt; (version 2.0.1)

* \[<span style="color:blue">ADDED</span>\] [`JavaCanonicalMapFactorySingleton.java`](https://bitbucket.org/refcodes/refcodes-structure-ext/src/refcodes-structure-ext-2.0.1/refcodes-structure-ext-factory/src/main/java/org/refcodes/structure/ext/factory/JavaCanonicalMapFactorySingleton.java) (see Javadoc at [`JavaCanonicalMapFactorySingleton.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-structure-ext-factory/2.0.1/org/refcodes/structure/ext/factory/JavaCanonicalMapFactorySingleton.html))
* \[<span style="color:blue">ADDED</span>\] [`JsonCanonicalMapFactorySingleton.java`](https://bitbucket.org/refcodes/refcodes-structure-ext/src/refcodes-structure-ext-2.0.1/refcodes-structure-ext-factory/src/main/java/org/refcodes/structure/ext/factory/JsonCanonicalMapFactorySingleton.java) (see Javadoc at [`JsonCanonicalMapFactorySingleton.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-structure-ext-factory/2.0.1/org/refcodes/structure/ext/factory/JsonCanonicalMapFactorySingleton.html))
* \[<span style="color:blue">ADDED</span>\] [`TomlCanonicalMapFactorySingleton.java`](https://bitbucket.org/refcodes/refcodes-structure-ext/src/refcodes-structure-ext-2.0.1/refcodes-structure-ext-factory/src/main/java/org/refcodes/structure/ext/factory/TomlCanonicalMapFactorySingleton.java) (see Javadoc at [`TomlCanonicalMapFactorySingleton.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-structure-ext-factory/2.0.1/org/refcodes/structure/ext/factory/TomlCanonicalMapFactorySingleton.html))
* \[<span style="color:blue">ADDED</span>\] [`XmlCanonicalMapFactorySingleton.java`](https://bitbucket.org/refcodes/refcodes-structure-ext/src/refcodes-structure-ext-2.0.1/refcodes-structure-ext-factory/src/main/java/org/refcodes/structure/ext/factory/XmlCanonicalMapFactorySingleton.java) (see Javadoc at [`XmlCanonicalMapFactorySingleton.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-structure-ext-factory/2.0.1/org/refcodes/structure/ext/factory/XmlCanonicalMapFactorySingleton.html))
* \[<span style="color:blue">ADDED</span>\] [`YamlCanonicalMapFactorySingleton.java`](https://bitbucket.org/refcodes/refcodes-structure-ext/src/refcodes-structure-ext-2.0.1/refcodes-structure-ext-factory/src/main/java/org/refcodes/structure/ext/factory/YamlCanonicalMapFactorySingleton.java) (see Javadoc at [`YamlCanonicalMapFactorySingleton.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-structure-ext-factory/2.0.1/org/refcodes/structure/ext/factory/YamlCanonicalMapFactorySingleton.html))
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-structure-ext/src/refcodes-structure-ext-2.0.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-structure-ext/src/refcodes-structure-ext-2.0.1/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-structure-ext/src/refcodes-structure-ext-2.0.1/refcodes-structure-ext-factory/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`CanonicalMapFactory.java`](https://bitbucket.org/refcodes/refcodes-structure-ext/src/refcodes-structure-ext-2.0.1/refcodes-structure-ext-factory/src/main/java/org/refcodes/structure/ext/factory/CanonicalMapFactory.java) (see Javadoc at [`CanonicalMapFactory.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-structure-ext-factory/2.0.1/org/refcodes/structure/ext/factory/CanonicalMapFactory.html))
* \[<span style="color:green">MODIFIED</span>\] [`JavaCanonicalMapFactory.java`](https://bitbucket.org/refcodes/refcodes-structure-ext/src/refcodes-structure-ext-2.0.1/refcodes-structure-ext-factory/src/main/java/org/refcodes/structure/ext/factory/JavaCanonicalMapFactory.java) (see Javadoc at [`JavaCanonicalMapFactory.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-structure-ext-factory/2.0.1/org/refcodes/structure/ext/factory/JavaCanonicalMapFactory.html))

## Change list &lt;refcodes-component&gt; (version 2.0.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-2.0.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`Closable.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-2.0.1/src/main/java/org/refcodes/component/Closable.java) (see Javadoc at [`Closable.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/2.0.1/org/refcodes/component/Closable.html))

## Change list &lt;refcodes-graphical&gt; (version 2.0.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-graphical/src/refcodes-graphical-2.0.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`GraphicalUtility.java`](https://bitbucket.org/refcodes/refcodes-graphical/src/refcodes-graphical-2.0.1/src/main/java/org/refcodes/graphical/GraphicalUtility.java) (see Javadoc at [`GraphicalUtility.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-graphical/2.0.1/org/refcodes/graphical/GraphicalUtility.html))

## Change list &lt;refcodes-textual&gt; (version 2.0.1)

* \[<span style="color:blue">ADDED</span>\] [`FillCharAccessor.java`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-2.0.1/src/main/java/org/refcodes/textual/FillCharAccessor.java) (see Javadoc at [`FillCharAccessor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-textual/2.0.1/org/refcodes/textual/FillCharAccessor.html))
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-2.0.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`CaseStyleBuilderImpl.java`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-2.0.1/src/main/java/org/refcodes/textual/CaseStyleBuilderImpl.java) (see Javadoc at [`CaseStyleBuilderImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-textual/2.0.1/org/refcodes/textual/CaseStyleBuilderImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`CaseStyleBuilder.java`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-2.0.1/src/main/java/org/refcodes/textual/CaseStyleBuilder.java) (see Javadoc at [`CaseStyleBuilder.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-textual/2.0.1/org/refcodes/textual/CaseStyleBuilder.html))
* \[<span style="color:green">MODIFIED</span>\] [`CsvBuilder.java`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-2.0.1/src/main/java/org/refcodes/textual/CsvBuilder.java) (see Javadoc at [`CsvBuilder.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-textual/2.0.1/org/refcodes/textual/CsvBuilder.html))
* \[<span style="color:green">MODIFIED</span>\] [`CsvMixin.java`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-2.0.1/src/main/java/org/refcodes/textual/CsvMixin.java) (see Javadoc at [`CsvMixin.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-textual/2.0.1/org/refcodes/textual/CsvMixin.html))
* \[<span style="color:green">MODIFIED</span>\] [`HorizAlignTextBuilder.java`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-2.0.1/src/main/java/org/refcodes/textual/HorizAlignTextBuilder.java) (see Javadoc at [`HorizAlignTextBuilder.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-textual/2.0.1/org/refcodes/textual/HorizAlignTextBuilder.html))
* \[<span style="color:green">MODIFIED</span>\] [`TextBlockBuilderImpl.java`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-2.0.1/src/main/java/org/refcodes/textual/TextBlockBuilderImpl.java) (see Javadoc at [`TextBlockBuilderImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-textual/2.0.1/org/refcodes/textual/TextBlockBuilderImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`TextBlockBuilder.java`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-2.0.1/src/main/java/org/refcodes/textual/TextBlockBuilder.java) (see Javadoc at [`TextBlockBuilder.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-textual/2.0.1/org/refcodes/textual/TextBlockBuilder.html))
* \[<span style="color:green">MODIFIED</span>\] [`VerboseTextBuilderImpl.java`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-2.0.1/src/main/java/org/refcodes/textual/VerboseTextBuilderImpl.java) (see Javadoc at [`VerboseTextBuilderImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-textual/2.0.1/org/refcodes/textual/VerboseTextBuilderImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`VerboseTextBuilder.java`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-2.0.1/src/main/java/org/refcodes/textual/VerboseTextBuilder.java) (see Javadoc at [`VerboseTextBuilder.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-textual/2.0.1/org/refcodes/textual/VerboseTextBuilder.html))

## Change list &lt;refcodes-criteria&gt; (version 2.0.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-2.0.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-2.0.1/README.md) 

## Change list &lt;refcodes-tabular&gt; (version 2.0.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-tabular/src/refcodes-tabular-2.0.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-tabular/src/refcodes-tabular-2.0.1/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`AbstractHeader.java`](https://bitbucket.org/refcodes/refcodes-tabular/src/refcodes-tabular-2.0.1/src/main/java/org/refcodes/tabular/AbstractHeader.java) (see Javadoc at [`AbstractHeader.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-tabular/2.0.1/org/refcodes/tabular/AbstractHeader.html))
* \[<span style="color:green">MODIFIED</span>\] [`Column.java`](https://bitbucket.org/refcodes/refcodes-tabular/src/refcodes-tabular-2.0.1/src/main/java/org/refcodes/tabular/Column.java) (see Javadoc at [`Column.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-tabular/2.0.1/org/refcodes/tabular/Column.html))
* \[<span style="color:green">MODIFIED</span>\] [`CsvRecordsWriter.java`](https://bitbucket.org/refcodes/refcodes-tabular/src/refcodes-tabular-2.0.1/src/main/java/org/refcodes/tabular/CsvRecordsWriter.java) (see Javadoc at [`CsvRecordsWriter.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-tabular/2.0.1/org/refcodes/tabular/CsvRecordsWriter.html))

## Change list &lt;refcodes-observer&gt; (version 2.0.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-observer/src/refcodes-observer-2.0.1/pom.xml) 

## Change list &lt;refcodes-command&gt; (version 2.0.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-command/src/refcodes-command-2.0.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-command/src/refcodes-command-2.0.1/README.md) 

## Change list &lt;refcodes-cli&gt; (version 2.0.1)

* \[<span style="color:blue">ADDED</span>\] [`DebugFlag.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-2.0.1/src/main/java/org/refcodes/console/DebugFlag.java) (see Javadoc at [`DebugFlag.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/2.0.1/org/refcodes/console/DebugFlag.html))
* \[<span style="color:blue">ADDED</span>\] [`LongOption.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-2.0.1/src/main/java/org/refcodes/console/LongOption.java) (see Javadoc at [`LongOption.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/2.0.1/org/refcodes/console/LongOption.html))
* \[<span style="color:red">DELETED</span>\] `FlagImpl.java`
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-2.0.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-2.0.1/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`ArgsParserImpl.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-2.0.1/src/main/java/org/refcodes/console/ArgsParserImpl.java) (see Javadoc at [`ArgsParserImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/2.0.1/org/refcodes/console/ArgsParserImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`ArgsParser.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-2.0.1/src/main/java/org/refcodes/console/ArgsParser.java) (see Javadoc at [`ArgsParser.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/2.0.1/org/refcodes/console/ArgsParser.html))
* \[<span style="color:green">MODIFIED</span>\] [`CliSugar.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-2.0.1/src/main/java/org/refcodes/console/CliSugar.java) (see Javadoc at [`CliSugar.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/2.0.1/org/refcodes/console/CliSugar.html))
* \[<span style="color:green">MODIFIED</span>\] [`Flag.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-2.0.1/src/main/java/org/refcodes/console/Flag.java) (see Javadoc at [`Flag.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/2.0.1/org/refcodes/console/Flag.html))
* \[<span style="color:green">MODIFIED</span>\] [`package-info.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-2.0.1/src/main/java/org/refcodes/console/package-info.java) 
* \[<span style="color:green">MODIFIED</span>\] [`ArgsParserTest.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-2.0.1/src/test/java/org/refcodes/console/ArgsParserTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`OptionalConditionTest.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-2.0.1/src/test/java/org/refcodes/console/OptionalConditionTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`StackOverflowTest.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-2.0.1/src/test/java/org/refcodes/console/StackOverflowTest.java) 

## Change list &lt;refcodes-cli-ext&gt; (version 2.0.1)

* \[<span style="color:blue">ADDED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-cli-ext/src/refcodes-cli-ext-2.0.1/refcodes-cli-ext-archetype/src/main/resources/archetype-resources/README.md) 
* \[<span style="color:blue">ADDED</span>\] [`__artifactId__.ini`](https://bitbucket.org/refcodes/refcodes-cli-ext/src/refcodes-cli-ext-2.0.1/refcodes-cli-ext-archetype/src/main/resources/archetype-resources/src/main/resources/__artifactId__.ini) 
* \[<span style="color:blue">ADDED</span>\] [`runtimelogger.ini`](https://bitbucket.org/refcodes/refcodes-cli-ext/src/refcodes-cli-ext-2.0.1/refcodes-cli-ext-archetype/src/main/resources/archetype-resources/src/main/resources/runtimelogger.ini) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-cli-ext/src/refcodes-cli-ext-2.0.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-cli-ext/src/refcodes-cli-ext-2.0.1/refcodes-cli-ext-archetype/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-cli-ext/src/refcodes-cli-ext-2.0.1/refcodes-cli-ext-archetype/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-cli-ext/src/refcodes-cli-ext-2.0.1/refcodes-cli-ext-archetype/src/main/resources/archetype-resources/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`Main.java`](https://bitbucket.org/refcodes/refcodes-cli-ext/src/refcodes-cli-ext-2.0.1/refcodes-cli-ext-archetype/src/main/resources/archetype-resources/src/main/java/Main.java) (see Javadoc at [`Main.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli-ext-archetype/src/main/resources/archetype-resources/2.0.1/Main.html))
* \[<span style="color:green">MODIFIED</span>\] [`archetype-metadata.xml`](https://bitbucket.org/refcodes/refcodes-cli-ext/src/refcodes-cli-ext-2.0.1/refcodes-cli-ext-archetype/src/main/resources/META-INF/maven/archetype-metadata.xml) 

## Change list &lt;refcodes-io&gt; (version 2.0.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-io/src/refcodes-io-2.0.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-io/src/refcodes-io-2.0.1/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`AbstractByteProvider.java`](https://bitbucket.org/refcodes/refcodes-io/src/refcodes-io-2.0.1/src/main/java/org/refcodes/io/AbstractByteProvider.java) (see Javadoc at [`AbstractByteProvider.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-io/2.0.1/org/refcodes/io/AbstractByteProvider.html))
* \[<span style="color:green">MODIFIED</span>\] [`AbstractPrefetchInputStreamByteReceiver.java`](https://bitbucket.org/refcodes/refcodes-io/src/refcodes-io-2.0.1/src/main/java/org/refcodes/io/AbstractPrefetchInputStreamByteReceiver.java) (see Javadoc at [`AbstractPrefetchInputStreamByteReceiver.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-io/2.0.1/org/refcodes/io/AbstractPrefetchInputStreamByteReceiver.html))
* \[<span style="color:green">MODIFIED</span>\] [`AbstractPrefetchInputStreamReceiver.java`](https://bitbucket.org/refcodes/refcodes-io/src/refcodes-io-2.0.1/src/main/java/org/refcodes/io/AbstractPrefetchInputStreamReceiver.java) (see Javadoc at [`AbstractPrefetchInputStreamReceiver.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-io/2.0.1/org/refcodes/io/AbstractPrefetchInputStreamReceiver.html))
* \[<span style="color:green">MODIFIED</span>\] [`AbstractReceiver.java`](https://bitbucket.org/refcodes/refcodes-io/src/refcodes-io-2.0.1/src/main/java/org/refcodes/io/AbstractReceiver.java) (see Javadoc at [`AbstractReceiver.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-io/2.0.1/org/refcodes/io/AbstractReceiver.html))
* \[<span style="color:green">MODIFIED</span>\] [`AbstractShortReceiver.java`](https://bitbucket.org/refcodes/refcodes-io/src/refcodes-io-2.0.1/src/main/java/org/refcodes/io/AbstractShortReceiver.java) (see Javadoc at [`AbstractShortReceiver.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-io/2.0.1/org/refcodes/io/AbstractShortReceiver.html))
* \[<span style="color:green">MODIFIED</span>\] [`ByteReceiver.java`](https://bitbucket.org/refcodes/refcodes-io/src/refcodes-io-2.0.1/src/main/java/org/refcodes/io/ByteReceiver.java) (see Javadoc at [`ByteReceiver.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-io/2.0.1/org/refcodes/io/ByteReceiver.html))
* \[<span style="color:green">MODIFIED</span>\] [`Receiver.java`](https://bitbucket.org/refcodes/refcodes-io/src/refcodes-io-2.0.1/src/main/java/org/refcodes/io/Receiver.java) (see Javadoc at [`Receiver.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-io/2.0.1/org/refcodes/io/Receiver.html))
* \[<span style="color:green">MODIFIED</span>\] [`ShortReceiver.java`](https://bitbucket.org/refcodes/refcodes-io/src/refcodes-io-2.0.1/src/main/java/org/refcodes/io/ShortReceiver.java) (see Javadoc at [`ShortReceiver.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-io/2.0.1/org/refcodes/io/ShortReceiver.html))

## Change list &lt;refcodes-codec&gt; (version 2.0.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-2.0.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-2.0.1/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`ModemDecoderImpl.java`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-2.0.1/src/main/java/org/refcodes/codec/ModemDecoderImpl.java) (see Javadoc at [`ModemDecoderImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-codec/2.0.1/org/refcodes/codec/ModemDecoderImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`ModemEncoderImpl.java`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-2.0.1/src/main/java/org/refcodes/codec/ModemEncoderImpl.java) (see Javadoc at [`ModemEncoderImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-codec/2.0.1/org/refcodes/codec/ModemEncoderImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`BaseBuilderTest.java`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-2.0.1/src/test/java/org/refcodes/codec/BaseBuilderTest.java) 

## Change list &lt;refcodes-component-ext&gt; (version 2.0.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-2.0.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-2.0.1/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-2.0.1/refcodes-component-ext-observer/pom.xml) 

## Change list &lt;refcodes-properties&gt; (version 2.0.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-2.0.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-2.0.1/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`package-info.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-2.0.1/src/main/java/org/refcodes/configuration/package-info.java) 
* \[<span style="color:green">MODIFIED</span>\] [`Properties.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-2.0.1/src/main/java/org/refcodes/configuration/Properties.java) (see Javadoc at [`Properties.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties/2.0.1/org/refcodes/configuration/Properties.html))
* \[<span style="color:green">MODIFIED</span>\] [`ScheduledResourcePropertiesBuilderDecorator.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-2.0.1/src/main/java/org/refcodes/configuration/ScheduledResourcePropertiesBuilderDecorator.java) (see Javadoc at [`ScheduledResourcePropertiesBuilderDecorator.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties/2.0.1/org/refcodes/configuration/ScheduledResourcePropertiesBuilderDecorator.html))
* \[<span style="color:green">MODIFIED</span>\] [`ScheduledResourcePropertiesDecorator.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-2.0.1/src/main/java/org/refcodes/configuration/ScheduledResourcePropertiesDecorator.java) (see Javadoc at [`ScheduledResourcePropertiesDecorator.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties/2.0.1/org/refcodes/configuration/ScheduledResourcePropertiesDecorator.html))

## Change list &lt;refcodes-security&gt; (version 2.0.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security/src/refcodes-security-2.0.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security/src/refcodes-security-2.0.1/README.md) 

## Change list &lt;refcodes-security-alt&gt; (version 2.0.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-2.0.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-2.0.1/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-2.0.1/refcodes-security-alt-chaos/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-2.0.1/refcodes-security-alt-chaos/README.md) 

## Change list &lt;refcodes-security-ext&gt; (version 2.0.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-2.0.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-2.0.1/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-2.0.1/refcodes-security-ext-chaos/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-2.0.1/refcodes-security-ext-chaos/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-2.0.1/refcodes-security-ext-spring/pom.xml) 

## Change list &lt;refcodes-properties-ext&gt; (version 2.0.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-2.0.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-2.0.1/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-2.0.1/refcodes-properties-ext-cli/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-2.0.1/refcodes-properties-ext-cli/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`ArgsParserPropertiesTest.java`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-2.0.1/refcodes-properties-ext-cli/src/test/java/org/refcodes/configuration/ext/console/ArgsParserPropertiesTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-2.0.1/refcodes-properties-ext-obfuscation/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-2.0.1/refcodes-properties-ext-obfuscation/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-2.0.1/refcodes-properties-ext-observer/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-2.0.1/refcodes-properties-ext-observer/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-2.0.1/refcodes-properties-ext-runtime/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-2.0.1/refcodes-properties-ext-runtime/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`RuntimePropertiesImpl.java`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-2.0.1/refcodes-properties-ext-runtime/src/main/java/org/refcodes/configuration/ext/runtime/RuntimePropertiesImpl.java) (see Javadoc at [`RuntimePropertiesImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties-ext-runtime/2.0.1/org/refcodes/configuration/ext/runtime/RuntimePropertiesImpl.html))

## Change list &lt;refcodes-logger&gt; (version 2.0.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger/src/refcodes-logger-2.0.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger/src/refcodes-logger-2.0.1/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`AbstractCompositeLogger.java`](https://bitbucket.org/refcodes/refcodes-logger/src/refcodes-logger-2.0.1/src/main/java/org/refcodes/logger/AbstractCompositeLogger.java) (see Javadoc at [`AbstractCompositeLogger.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-logger/2.0.1/org/refcodes/logger/AbstractCompositeLogger.html))
* \[<span style="color:green">MODIFIED</span>\] [`RuntimeLoggerFactoryImpl.java`](https://bitbucket.org/refcodes/refcodes-logger/src/refcodes-logger-2.0.1/src/main/java/org/refcodes/logger/RuntimeLoggerFactoryImpl.java) (see Javadoc at [`RuntimeLoggerFactoryImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-logger/2.0.1/org/refcodes/logger/RuntimeLoggerFactoryImpl.html))

## Change list &lt;refcodes-logger-alt&gt; (version 2.0.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-2.0.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-2.0.1/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-2.0.1/refcodes-logger-alt-async/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-2.0.1/refcodes-logger-alt-async/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`AsyncLogger.java`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-2.0.1/refcodes-logger-alt-async/src/main/java/org/refcodes/logger/alt/async/AsyncLogger.java) (see Javadoc at [`AsyncLogger.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-logger-alt-async/2.0.1/org/refcodes/logger/alt/async/AsyncLogger.html))
* \[<span style="color:green">MODIFIED</span>\] [`AsyncRuntimeLoggerTest.java`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-2.0.1/refcodes-logger-alt-async/src/test/java/org/refcodes/logger/alt/async/AsyncRuntimeLoggerTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`RuntimeLoggerSingletonTest.java`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-2.0.1/refcodes-logger-alt-async/src/test/java/org/refcodes/logger/alt/async/RuntimeLoggerSingletonTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-2.0.1/refcodes-logger-alt-console/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-2.0.1/refcodes-logger-alt-console/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`ConsoleLogger.java`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-2.0.1/refcodes-logger-alt-console/src/main/java/org/refcodes/logger/alt/console/ConsoleLogger.java) (see Javadoc at [`ConsoleLogger.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-logger-alt-console/2.0.1/org/refcodes/logger/alt/console/ConsoleLogger.html))
* \[<span style="color:green">MODIFIED</span>\] [`FormattedLoggerImpl.java`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-2.0.1/refcodes-logger-alt-console/src/main/java/org/refcodes/logger/alt/console/FormattedLoggerImpl.java) (see Javadoc at [`FormattedLoggerImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-logger-alt-console/2.0.1/org/refcodes/logger/alt/console/FormattedLoggerImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-2.0.1/refcodes-logger-alt-io/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-2.0.1/refcodes-logger-alt-io/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-2.0.1/refcodes-logger-alt-simpledb/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`SimpleDbLogger.java`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-2.0.1/refcodes-logger-alt-simpledb/src/main/java/org/refcodes/logger/alt/simpledb/SimpleDbLogger.java) (see Javadoc at [`SimpleDbLogger.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-logger-alt-simpledb/2.0.1/org/refcodes/logger/alt/simpledb/SimpleDbLogger.html))
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-2.0.1/refcodes-logger-alt-slf4j/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-2.0.1/refcodes-logger-alt-spring/pom.xml) 

## Change list &lt;refcodes-logger-ext&gt; (version 2.0.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-2.0.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-2.0.1/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-2.0.1/refcodes-logger-ext-slf4j/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-2.0.1/refcodes-logger-ext-slf4j/README.md) 

## Change list &lt;refcodes-graphical-ext&gt; (version 2.0.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-2.0.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-2.0.1/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-2.0.1/refcodes-graphical-ext-javafx/pom.xml) 

## Change list &lt;refcodes-checkerboard&gt; (version 2.0.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-2.0.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-2.0.1/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`ConsoleCheckerboardViewerImpl.java`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-2.0.1/src/main/java/org/refcodes/checkerboard/ConsoleCheckerboardViewerImpl.java) (see Javadoc at [`ConsoleCheckerboardViewerImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-checkerboard/2.0.1/org/refcodes/checkerboard/ConsoleCheckerboardViewerImpl.html))

## Change list &lt;refcodes-checkerboard-alt&gt; (version 2.0.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-alt/src/refcodes-checkerboard-alt-2.0.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-alt/src/refcodes-checkerboard-alt-2.0.1/refcodes-checkerboard-alt-javafx/pom.xml) 

## Change list &lt;refcodes-checkerboard-ext&gt; (version 2.0.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-ext/src/refcodes-checkerboard-ext-2.0.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-checkerboard-ext/src/refcodes-checkerboard-ext-2.0.1/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-ext/src/refcodes-checkerboard-ext-2.0.1/refcodes-checkerboard-ext-javafx-boulderdash/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-ext/src/refcodes-checkerboard-ext-2.0.1/refcodes-checkerboard-ext-javafx-chess/pom.xml) 

## Change list &lt;refcodes-boulderdash&gt; (version 2.0.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-boulderdash/src/refcodes-boulderdash-2.0.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-boulderdash/src/refcodes-boulderdash-2.0.1/README.md) 

## Change list &lt;refcodes-net&gt; (version 2.0.1)

* \[<span style="color:blue">ADDED</span>\] [`BadInvocationException.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-2.0.1/src/main/java/org/refcodes/net/BadInvocationException.java) (see Javadoc at [`BadInvocationException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/2.0.1/org/refcodes/net/BadInvocationException.html))
* \[<span style="color:blue">ADDED</span>\] [`BadInvocationRuntimeException.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-2.0.1/src/main/java/org/refcodes/net/BadInvocationRuntimeException.java) (see Javadoc at [`BadInvocationRuntimeException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/2.0.1/org/refcodes/net/BadInvocationRuntimeException.html))
* \[<span style="color:blue">ADDED</span>\] [`OauthTokenAccessor.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-2.0.1/src/main/java/org/refcodes/net/OauthTokenAccessor.java) (see Javadoc at [`OauthTokenAccessor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/2.0.1/org/refcodes/net/OauthTokenAccessor.html))
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-2.0.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`BadResponseException.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-2.0.1/src/main/java/org/refcodes/net/BadResponseException.java) (see Javadoc at [`BadResponseException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/2.0.1/org/refcodes/net/BadResponseException.html))
* \[<span style="color:green">MODIFIED</span>\] [`BadResponseRuntimeException.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-2.0.1/src/main/java/org/refcodes/net/BadResponseRuntimeException.java) (see Javadoc at [`BadResponseRuntimeException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/2.0.1/org/refcodes/net/BadResponseRuntimeException.html))
* \[<span style="color:green">MODIFIED</span>\] [`FormFields.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-2.0.1/src/main/java/org/refcodes/net/FormFields.java) (see Javadoc at [`FormFields.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/2.0.1/org/refcodes/net/FormFields.html))
* \[<span style="color:green">MODIFIED</span>\] [`HeaderFields.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-2.0.1/src/main/java/org/refcodes/net/HeaderFields.java) (see Javadoc at [`HeaderFields.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/2.0.1/org/refcodes/net/HeaderFields.html))
* \[<span style="color:green">MODIFIED</span>\] [`HttpBodyMap.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-2.0.1/src/main/java/org/refcodes/net/HttpBodyMap.java) (see Javadoc at [`HttpBodyMap.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/2.0.1/org/refcodes/net/HttpBodyMap.html))
* \[<span style="color:green">MODIFIED</span>\] [`HttpClientRequestImpl.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-2.0.1/src/main/java/org/refcodes/net/HttpClientRequestImpl.java) (see Javadoc at [`HttpClientRequestImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/2.0.1/org/refcodes/net/HttpClientRequestImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`HttpClientRequest.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-2.0.1/src/main/java/org/refcodes/net/HttpClientRequest.java) (see Javadoc at [`HttpClientRequest.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/2.0.1/org/refcodes/net/HttpClientRequest.html))
* \[<span style="color:green">MODIFIED</span>\] [`HttpClientResponseImpl.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-2.0.1/src/main/java/org/refcodes/net/HttpClientResponseImpl.java) (see Javadoc at [`HttpClientResponseImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/2.0.1/org/refcodes/net/HttpClientResponseImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`HttpException.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-2.0.1/src/main/java/org/refcodes/net/HttpException.java) (see Javadoc at [`HttpException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/2.0.1/org/refcodes/net/HttpException.html))
* \[<span style="color:green">MODIFIED</span>\] [`HttpStatusCode.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-2.0.1/src/main/java/org/refcodes/net/HttpStatusCode.java) (see Javadoc at [`HttpStatusCode.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/2.0.1/org/refcodes/net/HttpStatusCode.html))
* \[<span style="color:green">MODIFIED</span>\] [`InternalClientErrorException.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-2.0.1/src/main/java/org/refcodes/net/InternalClientErrorException.java) (see Javadoc at [`InternalClientErrorException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/2.0.1/org/refcodes/net/InternalClientErrorException.html))
* \[<span style="color:green">MODIFIED</span>\] [`InternalClientErrorRuntimeException.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-2.0.1/src/main/java/org/refcodes/net/InternalClientErrorRuntimeException.java) (see Javadoc at [`InternalClientErrorRuntimeException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/2.0.1/org/refcodes/net/InternalClientErrorRuntimeException.html))
* \[<span style="color:green">MODIFIED</span>\] [`NetException.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-2.0.1/src/main/java/org/refcodes/net/NetException.java) (see Javadoc at [`NetException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/2.0.1/org/refcodes/net/NetException.html))
* \[<span style="color:green">MODIFIED</span>\] [`NetRuntimeException.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-2.0.1/src/main/java/org/refcodes/net/NetRuntimeException.java) (see Javadoc at [`NetRuntimeException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/2.0.1/org/refcodes/net/NetRuntimeException.html))
* \[<span style="color:green">MODIFIED</span>\] [`Url.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-2.0.1/src/main/java/org/refcodes/net/Url.java) (see Javadoc at [`Url.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/2.0.1/org/refcodes/net/Url.html))

## Change list &lt;refcodes-rest&gt; (version 2.0.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-2.0.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-2.0.1/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`AbstractHttpRestClientDecorator.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-2.0.1/src/main/java/org/refcodes/rest/AbstractHttpRestClientDecorator.java) (see Javadoc at [`AbstractHttpRestClientDecorator.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/2.0.1/org/refcodes/rest/AbstractHttpRestClientDecorator.html))
* \[<span style="color:green">MODIFIED</span>\] [`AbstractRestClient.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-2.0.1/src/main/java/org/refcodes/rest/AbstractRestClient.java) (see Javadoc at [`AbstractRestClient.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/2.0.1/org/refcodes/rest/AbstractRestClient.html))
* \[<span style="color:green">MODIFIED</span>\] [`HttpRestClientImpl.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-2.0.1/src/main/java/org/refcodes/rest/HttpRestClientImpl.java) (see Javadoc at [`HttpRestClientImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/2.0.1/org/refcodes/rest/HttpRestClientImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`HttpRestServerImpl.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-2.0.1/src/main/java/org/refcodes/rest/HttpRestServerImpl.java) (see Javadoc at [`HttpRestServerImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/2.0.1/org/refcodes/rest/HttpRestServerImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`LoopbackRestClientImpl.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-2.0.1/src/main/java/org/refcodes/rest/LoopbackRestClientImpl.java) (see Javadoc at [`LoopbackRestClientImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/2.0.1/org/refcodes/rest/LoopbackRestClientImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`OauthTokenHandler.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-2.0.1/src/main/java/org/refcodes/rest/OauthTokenHandler.java) (see Javadoc at [`OauthTokenHandler.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/2.0.1/org/refcodes/rest/OauthTokenHandler.html))
* \[<span style="color:green">MODIFIED</span>\] [`RestClient.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-2.0.1/src/main/java/org/refcodes/rest/RestClient.java) (see Javadoc at [`RestClient.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/2.0.1/org/refcodes/rest/RestClient.html))

## Change list &lt;refcodes-hal&gt; (version 2.0.1)

* \[<span style="color:blue">ADDED</span>\] [`HalDataImpl.java`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-2.0.1/src/main/java/org/refcodes/hal/HalDataImpl.java) (see Javadoc at [`HalDataImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-hal/2.0.1/org/refcodes/hal/HalDataImpl.html))
* \[<span style="color:blue">ADDED</span>\] [`HalData.java`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-2.0.1/src/main/java/org/refcodes/hal/HalData.java) (see Javadoc at [`HalData.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-hal/2.0.1/org/refcodes/hal/HalData.html))
* \[<span style="color:blue">ADDED</span>\] [`HalMapImpl.java`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-2.0.1/src/main/java/org/refcodes/hal/HalMapImpl.java) (see Javadoc at [`HalMapImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-hal/2.0.1/org/refcodes/hal/HalMapImpl.html))
* \[<span style="color:blue">ADDED</span>\] [`HalMap.java`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-2.0.1/src/main/java/org/refcodes/hal/HalMap.java) (see Javadoc at [`HalMap.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-hal/2.0.1/org/refcodes/hal/HalMap.html))
* \[<span style="color:blue">ADDED</span>\] [`HalStructImpl.java`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-2.0.1/src/main/java/org/refcodes/hal/HalStructImpl.java) (see Javadoc at [`HalStructImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-hal/2.0.1/org/refcodes/hal/HalStructImpl.html))
* \[<span style="color:blue">ADDED</span>\] [`HalStruct.java`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-2.0.1/src/main/java/org/refcodes/hal/HalStruct.java) (see Javadoc at [`HalStruct.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-hal/2.0.1/org/refcodes/hal/HalStruct.html))
* \[<span style="color:blue">ADDED</span>\] [`TraversalMode.java`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-2.0.1/src/main/java/org/refcodes/hal/TraversalMode.java) (see Javadoc at [`TraversalMode.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-hal/2.0.1/org/refcodes/hal/TraversalMode.html))
* \[<span style="color:blue">ADDED</span>\] [`AbstractHalClientTest.java`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-2.0.1/src/test/java/org/refcodes/hal/AbstractHalClientTest.java) 
* \[<span style="color:blue">ADDED</span>\] [`Address.java`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-2.0.1/src/test/java/org/refcodes/hal/Address.java) 
* \[<span style="color:blue">ADDED</span>\] [`AddressRepository.java`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-2.0.1/src/test/java/org/refcodes/hal/AddressRepository.java) 
* \[<span style="color:blue">ADDED</span>\] [`Author.java`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-2.0.1/src/test/java/org/refcodes/hal/Author.java) 
* \[<span style="color:blue">ADDED</span>\] [`AuthorRepository.java`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-2.0.1/src/test/java/org/refcodes/hal/AuthorRepository.java) 
* \[<span style="color:blue">ADDED</span>\] [`Book.java`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-2.0.1/src/test/java/org/refcodes/hal/Book.java) 
* \[<span style="color:blue">ADDED</span>\] [`BookRepository.java`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-2.0.1/src/test/java/org/refcodes/hal/BookRepository.java) 
* \[<span style="color:blue">ADDED</span>\] [`HalClientTest.java`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-2.0.1/src/test/java/org/refcodes/hal/HalClientTest.java) 
* \[<span style="color:blue">ADDED</span>\] [`HalServer.java`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-2.0.1/src/test/java/org/refcodes/hal/HalServer.java) 
* \[<span style="color:blue">ADDED</span>\] [`Library.java`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-2.0.1/src/test/java/org/refcodes/hal/Library.java) 
* \[<span style="color:blue">ADDED</span>\] [`LibraryRepository.java`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-2.0.1/src/test/java/org/refcodes/hal/LibraryRepository.java) 
* \[<span style="color:blue">ADDED</span>\] [`Location.java`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-2.0.1/src/test/java/org/refcodes/hal/Location.java) 
* \[<span style="color:blue">ADDED</span>\] [`Name.java`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-2.0.1/src/test/java/org/refcodes/hal/Name.java) 
* \[<span style="color:blue">ADDED</span>\] [`Person.java`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-2.0.1/src/test/java/org/refcodes/hal/Person.java) 
* \[<span style="color:blue">ADDED</span>\] [`PersonRepository.java`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-2.0.1/src/test/java/org/refcodes/hal/PersonRepository.java) 
* \[<span style="color:blue">ADDED</span>\] [`Subject.java`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-2.0.1/src/test/java/org/refcodes/hal/Subject.java) 
* \[<span style="color:blue">ADDED</span>\] [`SubjectRepository.java`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-2.0.1/src/test/java/org/refcodes/hal/SubjectRepository.java) 
* \[<span style="color:blue">ADDED</span>\] [`TraversalModeTest.java`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-2.0.1/src/test/java/org/refcodes/hal/TraversalModeTest.java) 
* \[<span style="color:blue">ADDED</span>\] [`User.java`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-2.0.1/src/test/java/org/refcodes/hal/User.java) 
* \[<span style="color:blue">ADDED</span>\] [`UserRepository.java`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-2.0.1/src/test/java/org/refcodes/hal/UserRepository.java) 
* \[<span style="color:blue">ADDED</span>\] [`application.properties`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-2.0.1/src/test/resources/application.properties) 
* \[<span style="color:blue">ADDED</span>\] [`log4j.xml`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-2.0.1/src/test/resources/log4j.xml) 
* \[<span style="color:red">DELETED</span>\] `NoHalEndpointException.java`
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-2.0.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`HalClientImpl.java`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-2.0.1/src/main/java/org/refcodes/hal/HalClientImpl.java) (see Javadoc at [`HalClientImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-hal/2.0.1/org/refcodes/hal/HalClientImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`HalClient.java`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-2.0.1/src/main/java/org/refcodes/hal/HalClient.java) (see Javadoc at [`HalClient.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-hal/2.0.1/org/refcodes/hal/HalClient.html))

## Change list &lt;refcodes-daemon&gt; (version 2.0.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-daemon/src/refcodes-daemon-2.0.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-daemon/src/refcodes-daemon-2.0.1/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`AbstractDaemon.java`](https://bitbucket.org/refcodes/refcodes-daemon/src/refcodes-daemon-2.0.1/src/main/java/org/refcodes/daemon/AbstractDaemon.java) (see Javadoc at [`AbstractDaemon.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-daemon/2.0.1/org/refcodes/daemon/AbstractDaemon.html))

## Change list &lt;refcodes-eventbus&gt; (version 2.0.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-eventbus/src/refcodes-eventbus-2.0.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-eventbus/src/refcodes-eventbus-2.0.1/README.md) 

## Change list &lt;refcodes-eventbus-ext&gt; (version 2.0.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-2.0.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-2.0.1/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-2.0.1/refcodes-eventbus-ext-application/pom.xml) 

## Change list &lt;refcodes-filesystem&gt; (version 2.0.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-filesystem/src/refcodes-filesystem-2.0.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-filesystem/src/refcodes-filesystem-2.0.1/README.md) 

## Change list &lt;refcodes-filesystem-alt&gt; (version 2.0.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-filesystem-alt/src/refcodes-filesystem-alt-2.0.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-filesystem-alt/src/refcodes-filesystem-alt-2.0.1/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-filesystem-alt/src/refcodes-filesystem-alt-2.0.1/refcodes-filesystem-alt-s3/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`S3FileSystemImpl.java`](https://bitbucket.org/refcodes/refcodes-filesystem-alt/src/refcodes-filesystem-alt-2.0.1/refcodes-filesystem-alt-s3/src/main/java/org/refcodes/filesystem/alt/s3/S3FileSystemImpl.java) (see Javadoc at [`S3FileSystemImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-filesystem-alt-s3/2.0.1/org/refcodes/filesystem/alt/s3/S3FileSystemImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`S3FileSystemTest.java`](https://bitbucket.org/refcodes/refcodes-filesystem-alt/src/refcodes-filesystem-alt-2.0.1/refcodes-filesystem-alt-s3/src/test/java/org/refcodes/filesystem/alt/s3/S3FileSystemTest.java) 

## Change list &lt;refcodes-forwardsecrecy&gt; (version 2.0.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy/src/refcodes-forwardsecrecy-2.0.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy/src/refcodes-forwardsecrecy-2.0.1/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`AbstractEncryptionService.java`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy/src/refcodes-forwardsecrecy-2.0.1/src/main/java/org/refcodes/forwardsecrecy/AbstractEncryptionService.java) (see Javadoc at [`AbstractEncryptionService.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-forwardsecrecy/2.0.1/org/refcodes/forwardsecrecy/AbstractEncryptionService.html))
* \[<span style="color:green">MODIFIED</span>\] [`DecryptionProviderImpl.java`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy/src/refcodes-forwardsecrecy-2.0.1/src/main/java/org/refcodes/forwardsecrecy/DecryptionProviderImpl.java) (see Javadoc at [`DecryptionProviderImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-forwardsecrecy/2.0.1/org/refcodes/forwardsecrecy/DecryptionProviderImpl.html))

## Change list &lt;refcodes-forwardsecrecy-alt&gt; (version 2.0.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-2.0.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-2.0.1/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-2.0.1/refcodes-forwardsecrecy-alt-filesystem/pom.xml) 

## Change list &lt;refcodes-io-ext&gt; (version 2.0.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-2.0.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-2.0.1/refcodes-io-ext-observable/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-2.0.1/refcodes-io-ext-observable/README.md) 

## Change list &lt;refcodes-interceptor&gt; (version 2.0.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-interceptor/src/refcodes-interceptor-2.0.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-interceptor/src/refcodes-interceptor-2.0.1/README.md) 

## Change list &lt;refcodes-jobbus&gt; (version 2.0.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-jobbus/src/refcodes-jobbus-2.0.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-jobbus/src/refcodes-jobbus-2.0.1/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`AbstractJobBus.java`](https://bitbucket.org/refcodes/refcodes-jobbus/src/refcodes-jobbus-2.0.1/src/main/java/org/refcodes/jobbus/AbstractJobBus.java) (see Javadoc at [`AbstractJobBus.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-jobbus/2.0.1/org/refcodes/jobbus/AbstractJobBus.html))
* \[<span style="color:green">MODIFIED</span>\] [`JobBusProxyImpl.java`](https://bitbucket.org/refcodes/refcodes-jobbus/src/refcodes-jobbus-2.0.1/src/main/java/org/refcodes/jobbus/JobBusProxyImpl.java) (see Javadoc at [`JobBusProxyImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-jobbus/2.0.1/org/refcodes/jobbus/JobBusProxyImpl.html))

## Change list &lt;refcodes-rest-ext&gt; (version 2.0.1)

* \[<span style="color:blue">ADDED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-2.0.1/refcodes-rest-ext-archetype/pom.xml) 
* \[<span style="color:blue">ADDED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-2.0.1/refcodes-rest-ext-archetype/README.md) 
* \[<span style="color:blue">ADDED</span>\] [`build.sh`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-2.0.1/refcodes-rest-ext-archetype/src/main/resources/archetype-resources/build.sh) 
* \[<span style="color:blue">ADDED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-2.0.1/refcodes-rest-ext-archetype/src/main/resources/archetype-resources/pom.xml) 
* \[<span style="color:blue">ADDED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-2.0.1/refcodes-rest-ext-archetype/src/main/resources/archetype-resources/README.md) 
* \[<span style="color:blue">ADDED</span>\] [`shell-exec.inc`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-2.0.1/refcodes-rest-ext-archetype/src/main/resources/archetype-resources/shell-exec.inc) 
* \[<span style="color:blue">ADDED</span>\] [`scriptify.sh`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-2.0.1/refcodes-rest-ext-archetype/src/main/resources/archetype-resources/scriptify.sh) 
* \[<span style="color:blue">ADDED</span>\] [`Main.java`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-2.0.1/refcodes-rest-ext-archetype/src/main/resources/archetype-resources/src/main/java/Main.java) (see Javadoc at [`Main.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest-ext-archetype/src/main/resources/archetype-resources/2.0.1/Main.html))
* \[<span style="color:blue">ADDED</span>\] [`__artifactId__.ini`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-2.0.1/refcodes-rest-ext-archetype/src/main/resources/archetype-resources/src/main/resources/__artifactId__.ini) 
* \[<span style="color:blue">ADDED</span>\] [`runtimelogger.ini`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-2.0.1/refcodes-rest-ext-archetype/src/main/resources/archetype-resources/src/main/resources/runtimelogger.ini) 
* \[<span style="color:blue">ADDED</span>\] [`archetype-metadata.xml`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-2.0.1/refcodes-rest-ext-archetype/src/main/resources/META-INF/maven/archetype-metadata.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-2.0.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-2.0.1/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-2.0.1/refcodes-rest-ext-eureka/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-2.0.1/refcodes-rest-ext-eureka/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`EurekaDiscoverySidecarImpl.java`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-2.0.1/refcodes-rest-ext-eureka/src/main/java/org/refcodes/rest/ext/eureka/EurekaDiscoverySidecarImpl.java) (see Javadoc at [`EurekaDiscoverySidecarImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest-ext-eureka/2.0.1/org/refcodes/rest/ext/eureka/EurekaDiscoverySidecarImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`EurekaLoopSleepTime.java`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-2.0.1/refcodes-rest-ext-eureka/src/main/java/org/refcodes/rest/ext/eureka/EurekaLoopSleepTime.java) (see Javadoc at [`EurekaLoopSleepTime.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest-ext-eureka/2.0.1/org/refcodes/rest/ext/eureka/EurekaLoopSleepTime.html))
* \[<span style="color:green">MODIFIED</span>\] [`EurekaRegistrySidecarImpl.java`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-2.0.1/refcodes-rest-ext-eureka/src/main/java/org/refcodes/rest/ext/eureka/EurekaRegistrySidecarImpl.java) (see Javadoc at [`EurekaRegistrySidecarImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest-ext-eureka/2.0.1/org/refcodes/rest/ext/eureka/EurekaRegistrySidecarImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`EurekaRestClientDecorator.java`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-2.0.1/refcodes-rest-ext-eureka/src/main/java/org/refcodes/rest/ext/eureka/EurekaRestClientDecorator.java) (see Javadoc at [`EurekaRestClientDecorator.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest-ext-eureka/2.0.1/org/refcodes/rest/ext/eureka/EurekaRestClientDecorator.html))
* \[<span style="color:green">MODIFIED</span>\] [`EurekaRestServerDecorator.java`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-2.0.1/refcodes-rest-ext-eureka/src/main/java/org/refcodes/rest/ext/eureka/EurekaRestServerDecorator.java) (see Javadoc at [`EurekaRestServerDecorator.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest-ext-eureka/2.0.1/org/refcodes/rest/ext/eureka/EurekaRestServerDecorator.html))

## Change list &lt;refcodes-remoting&gt; (version 2.0.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-remoting/src/refcodes-remoting-2.0.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-remoting/src/refcodes-remoting-2.0.1/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`AbstractRemote.java`](https://bitbucket.org/refcodes/refcodes-remoting/src/refcodes-remoting-2.0.1/src/main/java/org/refcodes/remoting/AbstractRemote.java) (see Javadoc at [`AbstractRemote.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-remoting/2.0.1/org/refcodes/remoting/AbstractRemote.html))
* \[<span style="color:green">MODIFIED</span>\] [`RemoteClientImpl.java`](https://bitbucket.org/refcodes/refcodes-remoting/src/refcodes-remoting-2.0.1/src/main/java/org/refcodes/remoting/RemoteClientImpl.java) (see Javadoc at [`RemoteClientImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-remoting/2.0.1/org/refcodes/remoting/RemoteClientImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`RemoteServerImpl.java`](https://bitbucket.org/refcodes/refcodes-remoting/src/refcodes-remoting-2.0.1/src/main/java/org/refcodes/remoting/RemoteServerImpl.java) (see Javadoc at [`RemoteServerImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-remoting/2.0.1/org/refcodes/remoting/RemoteServerImpl.html))

## Change list &lt;refcodes-remoting-ext&gt; (version 2.0.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-2.0.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-2.0.1/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-2.0.1/refcodes-remoting-ext-observer/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`ObservableLoopbackRemoteTest.java`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-2.0.1/refcodes-remoting-ext-observer/src/test/java/org/refcodes/remoting/ext/observer/ObservableLoopbackRemoteTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`ObservableSocketRemoteTest.java`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-2.0.1/refcodes-remoting-ext-observer/src/test/java/org/refcodes/remoting/ext/observer/ObservableSocketRemoteTest.java) 

## Change list &lt;refcodes-servicebus&gt; (version 2.0.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-servicebus/src/refcodes-servicebus-2.0.1/pom.xml) 

## Change list &lt;refcodes-servicebus-alt&gt; (version 2.0.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-servicebus-alt/src/refcodes-servicebus-alt-2.0.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-servicebus-alt/src/refcodes-servicebus-alt-2.0.1/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-servicebus-alt/src/refcodes-servicebus-alt-2.0.1/refcodes-servicebus-alt-spring/pom.xml) 

## Change list &lt;refcodes-tabular-alt&gt; (version 2.0.1)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-tabular-alt/src/refcodes-tabular-alt-2.0.1/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-tabular-alt/src/refcodes-tabular-alt-2.0.1/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-tabular-alt/src/refcodes-tabular-alt-2.0.1/refcodes-tabular-alt-forwardsecrecy/pom.xml) 
