# Change list for REFCODES.ORG artifacts' version 1.2.0

> This change list has been auto-generated on `triton` by `steiner` with with `changelist-all.sh` on the 2017-12-23 at 17:05:31.

## Change list &lt;refcodes-licensing&gt; (version 1.2.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-licensing/src/refcodes-licensing-1.2.0/pom.xml) 

## Change list &lt;refcodes-parent&gt; (version 1.2.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-parent/src/refcodes-parent-1.2.0/pom.xml) 

## Change list &lt;refcodes-time&gt; (version 1.2.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-time/src/refcodes-time-1.2.0/pom.xml) 

## Change list &lt;refcodes-exception&gt; (version 1.2.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-exception/src/refcodes-exception-1.2.0/pom.xml) 

## Change list &lt;refcodes-mixin&gt; (version 1.2.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-mixin/src/refcodes-mixin-1.2.0/pom.xml) 

## Change list &lt;refcodes-data&gt; (version 1.2.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data/src/refcodes-data-1.2.0/pom.xml) 

## Change list &lt;refcodes-structure&gt; (version 1.2.0)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-1.2.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-1.2.0/pom.xml) 

## Change list &lt;refcodes-controlflow&gt; (version 1.2.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-controlflow/src/refcodes-controlflow-1.2.0/pom.xml) 

## Change list &lt;refcodes-numerical&gt; (version 1.2.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-numerical/src/refcodes-numerical-1.2.0/pom.xml) 

## Change list &lt;refcodes-generator&gt; (version 1.2.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-generator/src/refcodes-generator-1.2.0/pom.xml) 

## Change list &lt;refcodes-runtime&gt; (version 1.2.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-1.2.0/pom.xml) 

## Change list &lt;refcodes-component&gt; (version 1.2.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-1.2.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`Flushable.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-1.2.0/src/main/java/org/refcodes/component/Flushable.java) (see Javadoc at [`Flushable.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/1.2.0/org/refcodes/component/Flushable.html))

## Change list &lt;refcodes-properties&gt; (version 1.2.0)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.2.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.2.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`AbstractResourcePropertiesBuilder.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.2.0/src/main/java/org/refcodes/configuration/AbstractResourcePropertiesBuilder.java) (see Javadoc at [`AbstractResourcePropertiesBuilder.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties/1.2.0/org/refcodes/configuration/AbstractResourcePropertiesBuilder.html))
* \[<span style="color:green">MODIFIED</span>\] [`AbstractResourcePropertiesBuilderDecorator.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.2.0/src/main/java/org/refcodes/configuration/AbstractResourcePropertiesBuilderDecorator.java) (see Javadoc at [`AbstractResourcePropertiesBuilderDecorator.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties/1.2.0/org/refcodes/configuration/AbstractResourcePropertiesBuilderDecorator.html))
* \[<span style="color:green">MODIFIED</span>\] [`AbstractResourcePropertiesDecorator.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.2.0/src/main/java/org/refcodes/configuration/AbstractResourcePropertiesDecorator.java) (see Javadoc at [`AbstractResourcePropertiesDecorator.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties/1.2.0/org/refcodes/configuration/AbstractResourcePropertiesDecorator.html))
* \[<span style="color:green">MODIFIED</span>\] [`ProfilePropertiesProjection.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.2.0/src/main/java/org/refcodes/configuration/ProfilePropertiesProjection.java) (see Javadoc at [`ProfilePropertiesProjection.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties/1.2.0/org/refcodes/configuration/ProfilePropertiesProjection.html))
* \[<span style="color:green">MODIFIED</span>\] [`Properties.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.2.0/src/main/java/org/refcodes/configuration/Properties.java) (see Javadoc at [`Properties.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties/1.2.0/org/refcodes/configuration/Properties.html))
* \[<span style="color:green">MODIFIED</span>\] [`PropertiesSugar.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.2.0/src/main/java/org/refcodes/configuration/PropertiesSugar.java) (see Javadoc at [`PropertiesSugar.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties/1.2.0/org/refcodes/configuration/PropertiesSugar.html))
* \[<span style="color:green">MODIFIED</span>\] [`ResourceProperties.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.2.0/src/main/java/org/refcodes/configuration/ResourceProperties.java) (see Javadoc at [`ResourceProperties.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties/1.2.0/org/refcodes/configuration/ResourceProperties.html))
* \[<span style="color:green">MODIFIED</span>\] [`RuntimePropertiesComposite.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.2.0/src/main/java/org/refcodes/configuration/RuntimePropertiesComposite.java) (see Javadoc at [`RuntimePropertiesComposite.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties/1.2.0/org/refcodes/configuration/RuntimePropertiesComposite.html))
* \[<span style="color:green">MODIFIED</span>\] [`package-info.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.2.0/src/main/java/org/refcodes/configuration/package-info.java) 
* \[<span style="color:green">MODIFIED</span>\] [`EnvironmentPropertiesTest.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.2.0/src/test/java/org/refcodes/configuration/EnvironmentPropertiesTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`JavaPropertiesTest.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.2.0/src/test/java/org/refcodes/configuration/JavaPropertiesTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`JsonPropertiesTest.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.2.0/src/test/java/org/refcodes/configuration/JsonPropertiesTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`ProfilePropertiesTest.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.2.0/src/test/java/org/refcodes/configuration/ProfilePropertiesTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`PropertiesSugarTest.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.2.0/src/test/java/org/refcodes/configuration/PropertiesSugarTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`ResourcePropertiesTest.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.2.0/src/test/java/org/refcodes/configuration/ResourcePropertiesTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`SystemPropertiesTest.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.2.0/src/test/java/org/refcodes/configuration/SystemPropertiesTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`TomlPropertiesTest.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.2.0/src/test/java/org/refcodes/configuration/TomlPropertiesTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`XmlPropertiesTest.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.2.0/src/test/java/org/refcodes/configuration/XmlPropertiesTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`YamlPropertiesTest.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.2.0/src/test/java/org/refcodes/configuration/YamlPropertiesTest.java) 

## Change list &lt;refcodes-interceptor&gt; (version 1.2.0)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-interceptor/src/refcodes-interceptor-1.2.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-interceptor/src/refcodes-interceptor-1.2.0/pom.xml) 

## Change list &lt;refcodes-factory&gt; (version 1.2.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-factory/src/refcodes-factory-1.2.0/pom.xml) 

## Change list &lt;refcodes-data-ext&gt; (version 1.2.0)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.2.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.2.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.2.0/refcodes-data-ext-boulderdash/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.2.0/refcodes-data-ext-checkers/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.2.0/refcodes-data-ext-checkers/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.2.0/refcodes-data-ext-chess/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.2.0/refcodes-data-ext-chess/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.2.0/refcodes-data-ext-corporate/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.2.0/refcodes-data-ext-symbols/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.2.0/refcodes-data-ext-symbols/pom.xml) 

## Change list &lt;refcodes-graphical&gt; (version 1.2.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-graphical/src/refcodes-graphical-1.2.0/pom.xml) 

## Change list &lt;refcodes-textual&gt; (version 1.2.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-1.2.0/pom.xml) 

## Change list &lt;refcodes-criteria&gt; (version 1.2.0)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-1.2.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-1.2.0/pom.xml) 

## Change list &lt;refcodes-tabular&gt; (version 1.2.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-tabular/src/refcodes-tabular-1.2.0/pom.xml) 

## Change list &lt;refcodes-logger&gt; (version 1.2.0)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger/src/refcodes-logger-1.2.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger/src/refcodes-logger-1.2.0/pom.xml) 

## Change list &lt;refcodes-factory-alt&gt; (version 1.2.0)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-factory-alt/src/refcodes-factory-alt-1.2.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-factory-alt/src/refcodes-factory-alt-1.2.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-factory-alt/src/refcodes-factory-alt-1.2.0/refcodes-factory-alt-spring/pom.xml) 

## Change list &lt;refcodes-logger-alt&gt; (version 1.2.0)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.2.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.2.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.2.0/refcodes-logger-alt-async/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.2.0/refcodes-logger-alt-async/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.2.0/refcodes-logger-alt-console/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.2.0/refcodes-logger-alt-console/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.2.0/refcodes-logger-alt-io/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.2.0/refcodes-logger-alt-io/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.2.0/refcodes-logger-alt-simpledb/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.2.0/refcodes-logger-alt-slf4j/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.2.0/refcodes-logger-alt-spring/pom.xml) 

## Change list &lt;refcodes-matcher&gt; (version 1.2.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-matcher/src/refcodes-matcher-1.2.0/pom.xml) 

## Change list &lt;refcodes-observer&gt; (version 1.2.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-observer/src/refcodes-observer-1.2.0/pom.xml) 

## Change list &lt;refcodes-properties-ext&gt; (version 1.2.0)

* \[<span style="color:blue">ADDED</span>\] [`ObservableTomlPropertiesBuilderImpl.java`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.2.0/refcodes-properties-ext-observer/src/main/java/org/refcodes/configuration/ext/observer/ObservableTomlPropertiesBuilderImpl.java) (see Javadoc at [`ObservableTomlPropertiesBuilderImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties-ext-observer/1.2.0/org/refcodes/configuration/ext/observer/ObservableTomlPropertiesBuilderImpl.html))
* \[<span style="color:blue">ADDED</span>\] [`ObservablePropertiesSugarTest.java`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.2.0/refcodes-properties-ext-observer/src/test/java/org/refcodes/configuration/ext/observer/ObservablePropertiesSugarTest.java) 
* \[<span style="color:blue">ADDED</span>\] [`ObservableTomlPropertiesTest.java`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.2.0/refcodes-properties-ext-observer/src/test/java/org/refcodes/configuration/ext/observer/ObservableTomlPropertiesTest.java) 
* \[<span style="color:blue">ADDED</span>\] [`application.toml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.2.0/refcodes-properties-ext-observer/src/test/resources/application.toml) 
* \[<span style="color:blue">ADDED</span>\] [`vi.exe.stackdump`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.2.0/vi.exe.stackdump) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.2.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.2.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.2.0/refcodes-properties-ext-observer/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.2.0/refcodes-properties-ext-observer/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`AbstractObservableResourcePropertiesBuilderDecorator.java`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.2.0/refcodes-properties-ext-observer/src/main/java/org/refcodes/configuration/ext/observer/AbstractObservableResourcePropertiesBuilderDecorator.java) (see Javadoc at [`AbstractObservableResourcePropertiesBuilderDecorator.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties-ext-observer/1.2.0/org/refcodes/configuration/ext/observer/AbstractObservableResourcePropertiesBuilderDecorator.html))
* \[<span style="color:green">MODIFIED</span>\] [`ObservableJavaPropertiesBuilderImpl.java`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.2.0/refcodes-properties-ext-observer/src/main/java/org/refcodes/configuration/ext/observer/ObservableJavaPropertiesBuilderImpl.java) (see Javadoc at [`ObservableJavaPropertiesBuilderImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties-ext-observer/1.2.0/org/refcodes/configuration/ext/observer/ObservableJavaPropertiesBuilderImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`ObservableJsonPropertiesBuilderImpl.java`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.2.0/refcodes-properties-ext-observer/src/main/java/org/refcodes/configuration/ext/observer/ObservableJsonPropertiesBuilderImpl.java) (see Javadoc at [`ObservableJsonPropertiesBuilderImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties-ext-observer/1.2.0/org/refcodes/configuration/ext/observer/ObservableJsonPropertiesBuilderImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`ObservableProperties.java`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.2.0/refcodes-properties-ext-observer/src/main/java/org/refcodes/configuration/ext/observer/ObservableProperties.java) (see Javadoc at [`ObservableProperties.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties-ext-observer/1.2.0/org/refcodes/configuration/ext/observer/ObservableProperties.html))
* \[<span style="color:green">MODIFIED</span>\] [`ObservablePropertiesSugar.java`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.2.0/refcodes-properties-ext-observer/src/main/java/org/refcodes/configuration/ext/observer/ObservablePropertiesSugar.java) (see Javadoc at [`ObservablePropertiesSugar.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties-ext-observer/1.2.0/org/refcodes/configuration/ext/observer/ObservablePropertiesSugar.html))
* \[<span style="color:green">MODIFIED</span>\] [`ObservableResouceProperties.java`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.2.0/refcodes-properties-ext-observer/src/main/java/org/refcodes/configuration/ext/observer/ObservableResouceProperties.java) (see Javadoc at [`ObservableResouceProperties.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties-ext-observer/1.2.0/org/refcodes/configuration/ext/observer/ObservableResouceProperties.html))
* \[<span style="color:green">MODIFIED</span>\] [`ObservableXmlPropertiesBuilderImpl.java`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.2.0/refcodes-properties-ext-observer/src/main/java/org/refcodes/configuration/ext/observer/ObservableXmlPropertiesBuilderImpl.java) (see Javadoc at [`ObservableXmlPropertiesBuilderImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties-ext-observer/1.2.0/org/refcodes/configuration/ext/observer/ObservableXmlPropertiesBuilderImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`ObservableYamlPropertiesBuilderImpl.java`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.2.0/refcodes-properties-ext-observer/src/main/java/org/refcodes/configuration/ext/observer/ObservableYamlPropertiesBuilderImpl.java) (see Javadoc at [`ObservableYamlPropertiesBuilderImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties-ext-observer/1.2.0/org/refcodes/configuration/ext/observer/ObservableYamlPropertiesBuilderImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`PropertiesObserver.java`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.2.0/refcodes-properties-ext-observer/src/main/java/org/refcodes/configuration/ext/observer/PropertiesObserver.java) (see Javadoc at [`PropertiesObserver.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties-ext-observer/1.2.0/org/refcodes/configuration/ext/observer/PropertiesObserver.html))
* \[<span style="color:green">MODIFIED</span>\] [`AbstractObservableResourcePropertiesTest.java`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.2.0/refcodes-properties-ext-observer/src/test/java/org/refcodes/configuration/ext/observer/AbstractObservableResourcePropertiesTest.java) 

## Change list &lt;refcodes-checkerboard&gt; (version 1.2.0)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-1.2.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-1.2.0/pom.xml) 

## Change list &lt;refcodes-graphical-ext&gt; (version 1.2.0)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-1.2.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-1.2.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-1.2.0/refcodes-graphical-ext-javafx/pom.xml) 

## Change list &lt;refcodes-checkerboard-alt&gt; (version 1.2.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-alt/src/refcodes-checkerboard-alt-1.2.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-alt/src/refcodes-checkerboard-alt-1.2.0/refcodes-checkerboard-alt-javafx/pom.xml) 

## Change list &lt;refcodes-checkerboard-ext&gt; (version 1.2.0)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-checkerboard-ext/src/refcodes-checkerboard-ext-1.2.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-ext/src/refcodes-checkerboard-ext-1.2.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-ext/src/refcodes-checkerboard-ext-1.2.0/refcodes-checkerboard-ext-javafx-boulderdash/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-ext/src/refcodes-checkerboard-ext-1.2.0/refcodes-checkerboard-ext-javafx-chess/pom.xml) 

## Change list &lt;refcodes-io&gt; (version 1.2.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-io/src/refcodes-io-1.2.0/pom.xml) 

## Change list &lt;refcodes-codec&gt; (version 1.2.0)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-1.2.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-1.2.0/pom.xml) 

## Change list &lt;refcodes-command&gt; (version 1.2.0)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-command/src/refcodes-command-1.2.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-command/src/refcodes-command-1.2.0/pom.xml) 

## Change list &lt;refcodes-net&gt; (version 1.2.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.2.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`HttpStatusCode.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.2.0/src/main/java/org/refcodes/net/HttpStatusCode.java) (see Javadoc at [`HttpStatusCode.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/1.2.0/org/refcodes/net/HttpStatusCode.html))

## Change list &lt;refcodes-rest&gt; (version 1.2.0)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.2.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.2.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`package-info.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.2.0/src/main/java/org/refcodes/rest/package-info.java) 

## Change list &lt;refcodes-component-ext&gt; (version 1.2.0)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.2.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.2.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.2.0/refcodes-component-ext-observer/pom.xml) 

## Change list &lt;refcodes-cli&gt; (version 1.2.0)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-1.2.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-1.2.0/pom.xml) 

## Change list &lt;refcodes-security&gt; (version 1.2.0)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security/src/refcodes-security-1.2.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security/src/refcodes-security-1.2.0/pom.xml) 

## Change list &lt;refcodes-forwardsecrecy&gt; (version 1.2.0)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy/src/refcodes-forwardsecrecy-1.2.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy/src/refcodes-forwardsecrecy-1.2.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`EncryptionService.java`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy/src/refcodes-forwardsecrecy-1.2.0/src/main/java/org/refcodes/forwardsecrecy/EncryptionService.java) (see Javadoc at [`EncryptionService.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-forwardsecrecy/1.2.0/org/refcodes/forwardsecrecy/EncryptionService.html))
* \[<span style="color:green">MODIFIED</span>\] [`package-info.java`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy/src/refcodes-forwardsecrecy-1.2.0/src/main/java/org/refcodes/forwardsecrecy/package-info.java) 

## Change list &lt;refcodes-filesystem&gt; (version 1.2.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-filesystem/src/refcodes-filesystem-1.2.0/pom.xml) 

## Change list &lt;refcodes-forwardsecrecy-alt&gt; (version 1.2.0)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-1.2.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-1.2.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-1.2.0/refcodes-forwardsecrecy-alt-filesystem/pom.xml) 

## Change list &lt;refcodes-eventbus&gt; (version 1.2.0)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-eventbus/src/refcodes-eventbus-1.2.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-eventbus/src/refcodes-eventbus-1.2.0/pom.xml) 

## Change list &lt;refcodes-filesystem-alt&gt; (version 1.2.0)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-filesystem-alt/src/refcodes-filesystem-alt-1.2.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-filesystem-alt/src/refcodes-filesystem-alt-1.2.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-filesystem-alt/src/refcodes-filesystem-alt-1.2.0/refcodes-filesystem-alt-s3/pom.xml) 

## Change list &lt;refcodes-io-ext&gt; (version 1.2.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-1.2.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-1.2.0/refcodes-io-ext-observable/pom.xml) 

## Change list &lt;refcodes-jobbus&gt; (version 1.2.0)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-jobbus/src/refcodes-jobbus-1.2.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-jobbus/src/refcodes-jobbus-1.2.0/pom.xml) 

## Change list &lt;refcodes-logger-ext&gt; (version 1.2.0)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-1.2.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-1.2.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-1.2.0/refcodes-logger-ext-slf4j/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-1.2.0/refcodes-logger-ext-slf4j/pom.xml) 

## Change list &lt;refcodes-daemon&gt; (version 1.2.0)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-daemon/src/refcodes-daemon-1.2.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-daemon/src/refcodes-daemon-1.2.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`AbstractDaemon.java`](https://bitbucket.org/refcodes/refcodes-daemon/src/refcodes-daemon-1.2.0/src/main/java/org/refcodes/daemon/AbstractDaemon.java) (see Javadoc at [`AbstractDaemon.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-daemon/1.2.0/org/refcodes/daemon/AbstractDaemon.html))

## Change list &lt;refcodes-remoting&gt; (version 1.2.0)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-remoting/src/refcodes-remoting-1.2.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-remoting/src/refcodes-remoting-1.2.0/pom.xml) 

## Change list &lt;refcodes-remoting-ext&gt; (version 1.2.0)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-1.2.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-1.2.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-1.2.0/refcodes-remoting-ext-observer/pom.xml) 

## Change list &lt;refcodes-security-alt&gt; (version 1.2.0)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-1.2.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-1.2.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-1.2.0/refcodes-security-alt-chaos/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-1.2.0/refcodes-security-alt-chaos/pom.xml) 

## Change list &lt;refcodes-security-ext&gt; (version 1.2.0)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-1.2.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-1.2.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-1.2.0/refcodes-security-ext-chaos/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-1.2.0/refcodes-security-ext-chaos/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-1.2.0/refcodes-security-ext-spring/pom.xml) 

## Change list &lt;refcodes-servicebus&gt; (version 1.2.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-servicebus/src/refcodes-servicebus-1.2.0/pom.xml) 

## Change list &lt;refcodes-servicebus-alt&gt; (version 1.2.0)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-servicebus-alt/src/refcodes-servicebus-alt-1.2.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-servicebus-alt/src/refcodes-servicebus-alt-1.2.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-servicebus-alt/src/refcodes-servicebus-alt-1.2.0/refcodes-servicebus-alt-spring/pom.xml) 

## Change list &lt;refcodes-tabular-alt&gt; (version 1.2.0)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-tabular-alt/src/refcodes-tabular-alt-1.2.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-tabular-alt/src/refcodes-tabular-alt-1.2.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-tabular-alt/src/refcodes-tabular-alt-1.2.0/refcodes-tabular-alt-forwardsecrecy/pom.xml) 

## Change list &lt;refcodes-boulderdash&gt; (version 1.2.0)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-boulderdash/src/refcodes-boulderdash-1.2.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-boulderdash/src/refcodes-boulderdash-1.2.0/pom.xml) 
