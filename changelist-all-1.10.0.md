# Change list for REFCODES.ORG artifacts' version 1.10.0

> This change list has been auto-generated on `triton` by `steiner` with with `changelist-all.sh` on the 2018-11-25 at 07:52:58.

## Change list &lt;refcodes-licensing&gt; (version 1.10.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-licensing/src/refcodes-licensing-1.10.0/pom.xml) 

## Change list &lt;refcodes-parent&gt; (version 1.10.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-parent/src/refcodes-parent-1.10.0/pom.xml) 

## Change list &lt;refcodes-time&gt; (version 1.10.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-time/src/refcodes-time-1.10.0/pom.xml) 

## Change list &lt;refcodes-mixin&gt; (version 1.10.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-mixin/src/refcodes-mixin-1.10.0/pom.xml) 

## Change list &lt;refcodes-data&gt; (version 1.10.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data/src/refcodes-data-1.10.0/pom.xml) 

## Change list &lt;refcodes-exception&gt; (version 1.10.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-exception/src/refcodes-exception-1.10.0/pom.xml) 

## Change list &lt;refcodes-factory&gt; (version 1.10.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-factory/src/refcodes-factory-1.10.0/pom.xml) 

## Change list &lt;refcodes-factory-alt&gt; (version 1.10.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-factory-alt/src/refcodes-factory-alt-1.10.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-factory-alt/src/refcodes-factory-alt-1.10.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-factory-alt/src/refcodes-factory-alt-1.10.0/refcodes-factory-alt-spring/pom.xml) 

## Change list &lt;refcodes-controlflow&gt; (version 1.10.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-controlflow/src/refcodes-controlflow-1.10.0/pom.xml) 

## Change list &lt;refcodes-numerical&gt; (version 1.10.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-numerical/src/refcodes-numerical-1.10.0/pom.xml) 

## Change list &lt;refcodes-generator&gt; (version 1.10.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-generator/src/refcodes-generator-1.10.0/pom.xml) 

## Change list &lt;refcodes-matcher&gt; (version 1.10.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-matcher/src/refcodes-matcher-1.10.0/pom.xml) 

## Change list &lt;refcodes-structure&gt; (version 1.10.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-1.10.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-1.10.0/README.md) 

## Change list &lt;refcodes-runtime&gt; (version 1.10.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-1.10.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-1.10.0/README.md) 

## Change list &lt;refcodes-component&gt; (version 1.10.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-1.10.0/pom.xml) 

## Change list &lt;refcodes-data-ext&gt; (version 1.10.0)

* \[<span style="color:red">DELETED</span>\] `BoulderDashAnimationInputStreamsFactoryImpl.java`
* \[<span style="color:red">DELETED</span>\] `BoulderDashAnimationUrlsFactoryImpl.java`
* \[<span style="color:red">DELETED</span>\] `BoulderDashCaveMapFactoryImpl.java`
* \[<span style="color:red">DELETED</span>\] `BoulderDashPixmapInputStreamFactoryImpl.java`
* \[<span style="color:red">DELETED</span>\] `BoulderDashPixmapUrlFactoryImpl.java`
* \[<span style="color:red">DELETED</span>\] `CheckerVectorGraphicsInputStreamFactoryImpl.java`
* \[<span style="color:red">DELETED</span>\] `CheckerVectorGraphicsUrlFactoryImpl.java`
* \[<span style="color:red">DELETED</span>\] `ChessVectorGraphicsInputStreamFactoryImpl.java`
* \[<span style="color:red">DELETED</span>\] `ChessVectorGraphicsUrlFactoryImpl.java`
* \[<span style="color:red">DELETED</span>\] `LogoPixmapInputStreamFactoryImpl.java`
* \[<span style="color:red">DELETED</span>\] `LogoPixmapUrlFactoryImpl.java`
* \[<span style="color:red">DELETED</span>\] `SymbolPixmapInputStreamFactoryImpl.java`
* \[<span style="color:red">DELETED</span>\] `SymbolPixmapUrlFactoryImpl.java`
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.10.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.10.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.10.0/refcodes-data-ext-boulderdash/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.10.0/refcodes-data-ext-boulderdash/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`BoulderDashAnimationInputStreamsFactory.java`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.10.0/refcodes-data-ext-boulderdash/src/main/java/org/refcodes/data/ext/boulderdash/BoulderDashAnimationInputStreamsFactory.java) (see Javadoc at [`BoulderDashAnimationInputStreamsFactory.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-data-ext-boulderdash/1.10.0/org/refcodes/data/ext/boulderdash/BoulderDashAnimationInputStreamsFactory.html))
* \[<span style="color:green">MODIFIED</span>\] [`BoulderDashAnimationUrlsFactory.java`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.10.0/refcodes-data-ext-boulderdash/src/main/java/org/refcodes/data/ext/boulderdash/BoulderDashAnimationUrlsFactory.java) (see Javadoc at [`BoulderDashAnimationUrlsFactory.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-data-ext-boulderdash/1.10.0/org/refcodes/data/ext/boulderdash/BoulderDashAnimationUrlsFactory.html))
* \[<span style="color:green">MODIFIED</span>\] [`BoulderDashCaveMapFactory.java`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.10.0/refcodes-data-ext-boulderdash/src/main/java/org/refcodes/data/ext/boulderdash/BoulderDashCaveMapFactory.java) (see Javadoc at [`BoulderDashCaveMapFactory.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-data-ext-boulderdash/1.10.0/org/refcodes/data/ext/boulderdash/BoulderDashCaveMapFactory.html))
* \[<span style="color:green">MODIFIED</span>\] [`BoulderDashPixmapInputStreamFactory.java`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.10.0/refcodes-data-ext-boulderdash/src/main/java/org/refcodes/data/ext/boulderdash/BoulderDashPixmapInputStreamFactory.java) (see Javadoc at [`BoulderDashPixmapInputStreamFactory.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-data-ext-boulderdash/1.10.0/org/refcodes/data/ext/boulderdash/BoulderDashPixmapInputStreamFactory.html))
* \[<span style="color:green">MODIFIED</span>\] [`BoulderDashPixmapUrlFactory.java`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.10.0/refcodes-data-ext-boulderdash/src/main/java/org/refcodes/data/ext/boulderdash/BoulderDashPixmapUrlFactory.java) (see Javadoc at [`BoulderDashPixmapUrlFactory.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-data-ext-boulderdash/1.10.0/org/refcodes/data/ext/boulderdash/BoulderDashPixmapUrlFactory.html))
* \[<span style="color:green">MODIFIED</span>\] [`BoulderDashCaveMapTest.java`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.10.0/refcodes-data-ext-boulderdash/src/test/java/org/refcodes/data/ext/boulderdash/BoulderDashCaveMapTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`BoulderDashPixmapDataLocatorTest.java`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.10.0/refcodes-data-ext-boulderdash/src/test/java/org/refcodes/data/ext/boulderdash/BoulderDashPixmapDataLocatorTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.10.0/refcodes-data-ext-checkers/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.10.0/refcodes-data-ext-checkers/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`CheckerVectorGraphicsInputStreamFactory.java`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.10.0/refcodes-data-ext-checkers/src/main/java/org/refcodes/data/ext/checkers/CheckerVectorGraphicsInputStreamFactory.java) (see Javadoc at [`CheckerVectorGraphicsInputStreamFactory.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-data-ext-checkers/1.10.0/org/refcodes/data/ext/checkers/CheckerVectorGraphicsInputStreamFactory.html))
* \[<span style="color:green">MODIFIED</span>\] [`CheckerVectorGraphicsUrlFactory.java`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.10.0/refcodes-data-ext-checkers/src/main/java/org/refcodes/data/ext/checkers/CheckerVectorGraphicsUrlFactory.java) (see Javadoc at [`CheckerVectorGraphicsUrlFactory.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-data-ext-checkers/1.10.0/org/refcodes/data/ext/checkers/CheckerVectorGraphicsUrlFactory.html))
* \[<span style="color:green">MODIFIED</span>\] [`CheckerVectorGraphicsDataLocatorTest.java`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.10.0/refcodes-data-ext-checkers/src/test/java/org/refcodes/data/ext/checkers/CheckerVectorGraphicsDataLocatorTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.10.0/refcodes-data-ext-chess/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.10.0/refcodes-data-ext-chess/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`ChessVectorGraphicsInputStreamFactory.java`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.10.0/refcodes-data-ext-chess/src/main/java/org/refcodes/data/ext/chess/ChessVectorGraphicsInputStreamFactory.java) (see Javadoc at [`ChessVectorGraphicsInputStreamFactory.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-data-ext-chess/1.10.0/org/refcodes/data/ext/chess/ChessVectorGraphicsInputStreamFactory.html))
* \[<span style="color:green">MODIFIED</span>\] [`ChessVectorGraphicsUrlFactory.java`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.10.0/refcodes-data-ext-chess/src/main/java/org/refcodes/data/ext/chess/ChessVectorGraphicsUrlFactory.java) (see Javadoc at [`ChessVectorGraphicsUrlFactory.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-data-ext-chess/1.10.0/org/refcodes/data/ext/chess/ChessVectorGraphicsUrlFactory.html))
* \[<span style="color:green">MODIFIED</span>\] [`ChessVectorGraphicsDataLocatorTest.java`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.10.0/refcodes-data-ext-chess/src/test/java/org/refcodes/data/ext/chess/ChessVectorGraphicsDataLocatorTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.10.0/refcodes-data-ext-corporate/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.10.0/refcodes-data-ext-corporate/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`LogoPixmapInputStreamFactory.java`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.10.0/refcodes-data-ext-corporate/src/main/java/org/refcodes/data/ext/corporate/LogoPixmapInputStreamFactory.java) (see Javadoc at [`LogoPixmapInputStreamFactory.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-data-ext-corporate/1.10.0/org/refcodes/data/ext/corporate/LogoPixmapInputStreamFactory.html))
* \[<span style="color:green">MODIFIED</span>\] [`LogoPixmapUrlFactory.java`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.10.0/refcodes-data-ext-corporate/src/main/java/org/refcodes/data/ext/corporate/LogoPixmapUrlFactory.java) (see Javadoc at [`LogoPixmapUrlFactory.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-data-ext-corporate/1.10.0/org/refcodes/data/ext/corporate/LogoPixmapUrlFactory.html))
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.10.0/refcodes-data-ext-symbols/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.10.0/refcodes-data-ext-symbols/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`SymbolPixmapInputStreamFactory.java`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.10.0/refcodes-data-ext-symbols/src/main/java/org/refcodes/data/ext/symbols/SymbolPixmapInputStreamFactory.java) (see Javadoc at [`SymbolPixmapInputStreamFactory.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-data-ext-symbols/1.10.0/org/refcodes/data/ext/symbols/SymbolPixmapInputStreamFactory.html))
* \[<span style="color:green">MODIFIED</span>\] [`SymbolPixmapUrlFactory.java`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.10.0/refcodes-data-ext-symbols/src/main/java/org/refcodes/data/ext/symbols/SymbolPixmapUrlFactory.java) (see Javadoc at [`SymbolPixmapUrlFactory.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-data-ext-symbols/1.10.0/org/refcodes/data/ext/symbols/SymbolPixmapUrlFactory.html))
* \[<span style="color:green">MODIFIED</span>\] [`SymbolsPixmapDataLocatorTest.java`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.10.0/refcodes-data-ext-symbols/src/test/java/org/refcodes/data/ext/symbols/SymbolsPixmapDataLocatorTest.java) 

## Change list &lt;refcodes-graphical&gt; (version 1.10.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-graphical/src/refcodes-graphical-1.10.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`RgbPixmapImageBuilderTest.java`](https://bitbucket.org/refcodes/refcodes-graphical/src/refcodes-graphical-1.10.0/src/test/java/org/refcodes/graphical/RgbPixmapImageBuilderTest.java) 

## Change list &lt;refcodes-textual&gt; (version 1.10.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-1.10.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`AsciiArtBuilderTest.java`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-1.10.0/src/test/java/org/refcodes/textual/AsciiArtBuilderTest.java) 

## Change list &lt;refcodes-criteria&gt; (version 1.10.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-1.10.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-1.10.0/README.md) 

## Change list &lt;refcodes-tabular&gt; (version 1.10.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-tabular/src/refcodes-tabular-1.10.0/pom.xml) 

## Change list &lt;refcodes-observer&gt; (version 1.10.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-observer/src/refcodes-observer-1.10.0/pom.xml) 

## Change list &lt;refcodes-command&gt; (version 1.10.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-command/src/refcodes-command-1.10.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-command/src/refcodes-command-1.10.0/README.md) 

## Change list &lt;refcodes-cli&gt; (version 1.10.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-1.10.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-1.10.0/README.md) 

## Change list &lt;refcodes-io&gt; (version 1.10.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-io/src/refcodes-io-1.10.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-io/src/refcodes-io-1.10.0/README.md) 

## Change list &lt;refcodes-codec&gt; (version 1.10.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-1.10.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-1.10.0/README.md) 

## Change list &lt;refcodes-component-ext&gt; (version 1.10.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.10.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.10.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.10.0/refcodes-component-ext-observer/pom.xml) 

## Change list &lt;refcodes-properties&gt; (version 1.10.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.10.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.10.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`package-info.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.10.0/src/main/java/org/refcodes/configuration/package-info.java) 
* \[<span style="color:green">MODIFIED</span>\] [`PropertiesPrecedenceBuilderComposite.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.10.0/src/main/java/org/refcodes/configuration/PropertiesPrecedenceBuilderComposite.java) (see Javadoc at [`PropertiesPrecedenceBuilderComposite.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties/1.10.0/org/refcodes/configuration/PropertiesPrecedenceBuilderComposite.html))
* \[<span style="color:green">MODIFIED</span>\] [`PropertiesPrecedenceComposite.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.10.0/src/main/java/org/refcodes/configuration/PropertiesPrecedenceComposite.java) (see Javadoc at [`PropertiesPrecedenceComposite.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties/1.10.0/org/refcodes/configuration/PropertiesPrecedenceComposite.html))
* \[<span style="color:green">MODIFIED</span>\] [`PropertiesPrecedence.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.10.0/src/main/java/org/refcodes/configuration/PropertiesPrecedence.java) (see Javadoc at [`PropertiesPrecedence.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties/1.10.0/org/refcodes/configuration/PropertiesPrecedence.html))

## Change list &lt;refcodes-security&gt; (version 1.10.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security/src/refcodes-security-1.10.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security/src/refcodes-security-1.10.0/README.md) 

## Change list &lt;refcodes-security-alt&gt; (version 1.10.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-1.10.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-1.10.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-1.10.0/refcodes-security-alt-chaos/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-1.10.0/refcodes-security-alt-chaos/README.md) 

## Change list &lt;refcodes-security-ext&gt; (version 1.10.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-1.10.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-1.10.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-1.10.0/refcodes-security-ext-chaos/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-1.10.0/refcodes-security-ext-chaos/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-1.10.0/refcodes-security-ext-spring/pom.xml) 

## Change list &lt;refcodes-properties-ext&gt; (version 1.10.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.10.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.10.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.10.0/refcodes-properties-ext-cli/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.10.0/refcodes-properties-ext-cli/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.10.0/refcodes-properties-ext-obfuscation/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.10.0/refcodes-properties-ext-obfuscation/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.10.0/refcodes-properties-ext-observer/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.10.0/refcodes-properties-ext-observer/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.10.0/refcodes-properties-ext-runtime/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.10.0/refcodes-properties-ext-runtime/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`RuntimePropertiesImpl.java`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.10.0/refcodes-properties-ext-runtime/src/main/java/org/refcodes/configuration/ext/runtime/RuntimePropertiesImpl.java) (see Javadoc at [`RuntimePropertiesImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties-ext-runtime/1.10.0/org/refcodes/configuration/ext/runtime/RuntimePropertiesImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`RuntimeProperties.java`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.10.0/refcodes-properties-ext-runtime/src/main/java/org/refcodes/configuration/ext/runtime/RuntimeProperties.java) (see Javadoc at [`RuntimeProperties.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties-ext-runtime/1.10.0/org/refcodes/configuration/ext/runtime/RuntimeProperties.html))

## Change list &lt;refcodes-logger&gt; (version 1.10.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger/src/refcodes-logger-1.10.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger/src/refcodes-logger-1.10.0/README.md) 

## Change list &lt;refcodes-logger-alt&gt; (version 1.10.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.10.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.10.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.10.0/refcodes-logger-alt-async/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.10.0/refcodes-logger-alt-async/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.10.0/refcodes-logger-alt-console/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.10.0/refcodes-logger-alt-console/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.10.0/refcodes-logger-alt-io/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.10.0/refcodes-logger-alt-io/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.10.0/refcodes-logger-alt-simpledb/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.10.0/refcodes-logger-alt-slf4j/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.10.0/refcodes-logger-alt-spring/pom.xml) 

## Change list &lt;refcodes-logger-ext&gt; (version 1.10.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-1.10.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-1.10.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-1.10.0/refcodes-logger-ext-slf4j/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-1.10.0/refcodes-logger-ext-slf4j/README.md) 

## Change list &lt;refcodes-graphical-ext&gt; (version 1.10.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-1.10.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-1.10.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-1.10.0/refcodes-graphical-ext-javafx/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`FxCellBuilderImpl.java`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-1.10.0/refcodes-graphical-ext-javafx/src/main/java/org/refcodes/graphical/ext/javafx/FxCellBuilderImpl.java) (see Javadoc at [`FxCellBuilderImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-graphical-ext-javafx/1.10.0/org/refcodes/graphical/ext/javafx/FxCellBuilderImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`FxGraphicalUtility.java`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-1.10.0/refcodes-graphical-ext-javafx/src/main/java/org/refcodes/graphical/ext/javafx/FxGraphicalUtility.java) (see Javadoc at [`FxGraphicalUtility.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-graphical-ext-javafx/1.10.0/org/refcodes/graphical/ext/javafx/FxGraphicalUtility.html))

## Change list &lt;refcodes-checkerboard&gt; (version 1.10.0)

* \[<span style="color:blue">ADDED</span>\] [`NeighbourhoodPositions.java`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-1.10.0/src/main/java/org/refcodes/checkerboard/NeighbourhoodPositions.java) (see Javadoc at [`NeighbourhoodPositions.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-checkerboard/1.10.0/org/refcodes/checkerboard/NeighbourhoodPositions.html))
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-1.10.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-1.10.0/README.md) 

## Change list &lt;refcodes-checkerboard-alt&gt; (version 1.10.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-alt/src/refcodes-checkerboard-alt-1.10.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-alt/src/refcodes-checkerboard-alt-1.10.0/refcodes-checkerboard-alt-javafx/pom.xml) 

## Change list &lt;refcodes-checkerboard-ext&gt; (version 1.10.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-ext/src/refcodes-checkerboard-ext-1.10.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-checkerboard-ext/src/refcodes-checkerboard-ext-1.10.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-ext/src/refcodes-checkerboard-ext-1.10.0/refcodes-checkerboard-ext-javafx-boulderdash/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`FxBoulderDashSpriteFactoryImpl.java`](https://bitbucket.org/refcodes/refcodes-checkerboard-ext/src/refcodes-checkerboard-ext-1.10.0/refcodes-checkerboard-ext-javafx-boulderdash/src/main/java/org/refcodes/checkerboard/ext/javafx/boulderdash/FxBoulderDashSpriteFactoryImpl.java) (see Javadoc at [`FxBoulderDashSpriteFactoryImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-checkerboard-ext-javafx-boulderdash/1.10.0/org/refcodes/checkerboard/ext/javafx/boulderdash/FxBoulderDashSpriteFactoryImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-ext/src/refcodes-checkerboard-ext-1.10.0/refcodes-checkerboard-ext-javafx-chess/pom.xml) 

## Change list &lt;refcodes-boulderdash&gt; (version 1.10.0)

* \[<span style="color:blue">ADDED</span>\] [`BoulderDashConsoleSpriteFactory.java`](https://bitbucket.org/refcodes/refcodes-boulderdash/src/refcodes-boulderdash-1.10.0/src/main/java/org/refcodes/boulderdash/BoulderDashConsoleSpriteFactory.java) (see Javadoc at [`BoulderDashConsoleSpriteFactory.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-boulderdash/1.10.0/org/refcodes/boulderdash/BoulderDashConsoleSpriteFactory.html))
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-boulderdash/src/refcodes-boulderdash-1.10.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-boulderdash/src/refcodes-boulderdash-1.10.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`BoulderDashAutomaton.java`](https://bitbucket.org/refcodes/refcodes-boulderdash/src/refcodes-boulderdash-1.10.0/src/main/java/org/refcodes/boulderdash/BoulderDashAutomaton.java) (see Javadoc at [`BoulderDashAutomaton.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-boulderdash/1.10.0/org/refcodes/boulderdash/BoulderDashAutomaton.html))
* \[<span style="color:green">MODIFIED</span>\] [`package-info.java`](https://bitbucket.org/refcodes/refcodes-boulderdash/src/refcodes-boulderdash-1.10.0/src/main/java/org/refcodes/boulderdash/package-info.java) 

## Change list &lt;refcodes-net&gt; (version 1.10.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.10.0/pom.xml) 

## Change list &lt;refcodes-rest&gt; (version 1.10.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.10.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.10.0/README.md) 

## Change list &lt;refcodes-daemon&gt; (version 1.10.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-daemon/src/refcodes-daemon-1.10.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-daemon/src/refcodes-daemon-1.10.0/README.md) 

## Change list &lt;refcodes-eventbus&gt; (version 1.10.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-eventbus/src/refcodes-eventbus-1.10.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-eventbus/src/refcodes-eventbus-1.10.0/README.md) 

## Change list &lt;refcodes-eventbus-ext&gt; (version 1.10.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-1.10.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-1.10.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-1.10.0/refcodes-eventbus-ext-application/pom.xml) 

## Change list &lt;refcodes-filesystem&gt; (version 1.10.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-filesystem/src/refcodes-filesystem-1.10.0/pom.xml) 

## Change list &lt;refcodes-filesystem-alt&gt; (version 1.10.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-filesystem-alt/src/refcodes-filesystem-alt-1.10.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-filesystem-alt/src/refcodes-filesystem-alt-1.10.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-filesystem-alt/src/refcodes-filesystem-alt-1.10.0/refcodes-filesystem-alt-s3/pom.xml) 

## Change list &lt;refcodes-forwardsecrecy&gt; (version 1.10.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy/src/refcodes-forwardsecrecy-1.10.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy/src/refcodes-forwardsecrecy-1.10.0/README.md) 

## Change list &lt;refcodes-forwardsecrecy-alt&gt; (version 1.10.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-1.10.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-1.10.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-1.10.0/refcodes-forwardsecrecy-alt-filesystem/pom.xml) 

## Change list &lt;refcodes-io-ext&gt; (version 1.10.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-1.10.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-1.10.0/refcodes-io-ext-observable/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-1.10.0/refcodes-io-ext-observable/README.md) 

## Change list &lt;refcodes-interceptor&gt; (version 1.10.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-interceptor/src/refcodes-interceptor-1.10.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-interceptor/src/refcodes-interceptor-1.10.0/README.md) 

## Change list &lt;refcodes-jobbus&gt; (version 1.10.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-jobbus/src/refcodes-jobbus-1.10.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-jobbus/src/refcodes-jobbus-1.10.0/README.md) 

## Change list &lt;refcodes-rest-ext&gt; (version 1.10.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-1.10.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-1.10.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-1.10.0/refcodes-rest-ext-eureka/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-1.10.0/refcodes-rest-ext-eureka/README.md) 

## Change list &lt;refcodes-remoting&gt; (version 1.10.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-remoting/src/refcodes-remoting-1.10.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-remoting/src/refcodes-remoting-1.10.0/README.md) 

## Change list &lt;refcodes-remoting-ext&gt; (version 1.10.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-1.10.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-1.10.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-1.10.0/refcodes-remoting-ext-observer/pom.xml) 

## Change list &lt;refcodes-servicebus&gt; (version 1.10.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-servicebus/src/refcodes-servicebus-1.10.0/pom.xml) 

## Change list &lt;refcodes-servicebus-alt&gt; (version 1.10.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-servicebus-alt/src/refcodes-servicebus-alt-1.10.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-servicebus-alt/src/refcodes-servicebus-alt-1.10.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-servicebus-alt/src/refcodes-servicebus-alt-1.10.0/refcodes-servicebus-alt-spring/pom.xml) 

## Change list &lt;refcodes-tabular-alt&gt; (version 1.10.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-tabular-alt/src/refcodes-tabular-alt-1.10.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-tabular-alt/src/refcodes-tabular-alt-1.10.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-tabular-alt/src/refcodes-tabular-alt-1.10.0/refcodes-tabular-alt-forwardsecrecy/pom.xml) 
