> This change list has been auto-generated on `triton` by `steiner` with `changelist-all.sh` on the 2022-01-01 at 02:02:02.

## Change list &lt;refcodes-licensing&gt; (version 2.2.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-licensing/src/refcodes-licensing-2.2.2/pom.xml) 

## Change list &lt;refcodes-parent&gt; (version 2.2.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-parent/src/refcodes-parent-2.2.2/pom.xml) 

## Change list &lt;refcodes-time&gt; (version 2.2.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-time/src/refcodes-time-2.2.2/pom.xml) 

## Change list &lt;refcodes-mixin&gt; (version 2.2.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-mixin/src/refcodes-mixin-2.2.2/pom.xml) 

## Change list &lt;refcodes-data&gt; (version 2.2.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data/src/refcodes-data-2.2.2/pom.xml) 

## Change list &lt;refcodes-exception&gt; (version 2.2.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-exception/src/refcodes-exception-2.2.2/pom.xml) 

## Change list &lt;refcodes-factory&gt; (version 2.2.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-factory/src/refcodes-factory-2.2.2/pom.xml) 

## Change list &lt;refcodes-factory-alt&gt; (version 2.2.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-factory-alt/src/refcodes-factory-alt-2.2.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-factory-alt/src/refcodes-factory-alt-2.2.2/refcodes-factory-alt-spring/pom.xml) 

## Change list &lt;refcodes-controlflow&gt; (version 2.2.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-controlflow/src/refcodes-controlflow-2.2.2/pom.xml) 

## Change list &lt;refcodes-numerical&gt; (version 2.2.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-numerical/src/refcodes-numerical-2.2.2/pom.xml) 

## Change list &lt;refcodes-generator&gt; (version 2.2.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-generator/src/refcodes-generator-2.2.2/pom.xml) 

## Change list &lt;refcodes-matcher&gt; (version 2.2.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-matcher/src/refcodes-matcher-2.2.2/pom.xml) 

## Change list &lt;refcodes-struct&gt; (version 2.2.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-struct/src/refcodes-struct-2.2.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`ClassStructMapBuilderImpl.java`](https://bitbucket.org/refcodes/refcodes-struct/src/refcodes-struct-2.2.2/src/main/java/org/refcodes/struct/ClassStructMapBuilderImpl.java) (see Javadoc at [`ClassStructMapBuilderImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-struct/2.2.2/org.refcodes.struct/org/refcodes/struct/ClassStructMapBuilderImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`ClassStructMap.java`](https://bitbucket.org/refcodes/refcodes-struct/src/refcodes-struct-2.2.2/src/main/java/org/refcodes/struct/ClassStructMap.java) (see Javadoc at [`ClassStructMap.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-struct/2.2.2/org.refcodes.struct/org/refcodes/struct/ClassStructMap.html))

## Change list &lt;refcodes-struct-ext&gt; (version 2.2.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-struct-ext/src/refcodes-struct-ext-2.2.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-struct-ext/src/refcodes-struct-ext-2.2.2/refcodes-struct-ext-factory/pom.xml) 

## Change list &lt;refcodes-runtime&gt; (version 2.2.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-2.2.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`Terminal.java`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-2.2.2/src/main/java/org/refcodes/runtime/Terminal.java) (see Javadoc at [`Terminal.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-runtime/2.2.2/org.refcodes.runtime/org/refcodes/runtime/Terminal.html))

## Change list &lt;refcodes-component&gt; (version 2.2.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-2.2.2/pom.xml) 

## Change list &lt;refcodes-data-ext&gt; (version 2.2.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-2.2.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-2.2.2/refcodes-data-ext-boulderdash/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-2.2.2/refcodes-data-ext-checkers/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-2.2.2/refcodes-data-ext-chess/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-2.2.2/refcodes-data-ext-corporate/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-2.2.2/refcodes-data-ext-symbols/pom.xml) 

## Change list &lt;refcodes-graphical&gt; (version 2.2.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-graphical/src/refcodes-graphical-2.2.2/pom.xml) 

## Change list &lt;refcodes-textual&gt; (version 2.2.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-2.2.2/pom.xml) 

## Change list &lt;refcodes-criteria&gt; (version 2.2.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-2.2.2/pom.xml) 

## Change list &lt;refcodes-io&gt; (version 2.2.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-io/src/refcodes-io-2.2.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`BytesReceiver.java`](https://bitbucket.org/refcodes/refcodes-io/src/refcodes-io-2.2.2/src/main/java/org/refcodes/io/BytesReceiver.java) (see Javadoc at [`BytesReceiver.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-io/2.2.2/org.refcodes.io/org/refcodes/io/BytesReceiver.html))
* \[<span style="color:green">MODIFIED</span>\] [`Skippable.java`](https://bitbucket.org/refcodes/refcodes-io/src/refcodes-io-2.2.2/src/main/java/org/refcodes/io/Skippable.java) (see Javadoc at [`Skippable.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-io/2.2.2/org.refcodes.io/org/refcodes/io/Skippable.html))

## Change list &lt;refcodes-tabular&gt; (version 2.2.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-tabular/src/refcodes-tabular-2.2.2/pom.xml) 

## Change list &lt;refcodes-observer&gt; (version 2.2.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-observer/src/refcodes-observer-2.2.2/pom.xml) 

## Change list &lt;refcodes-command&gt; (version 2.2.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-command/src/refcodes-command-2.2.2/pom.xml) 

## Change list &lt;refcodes-cli&gt; (version 2.2.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-2.2.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`ArgsParserTest.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-2.2.2/src/test/java/org/refcodes/cli/ArgsParserTest.java) 

## Change list &lt;refcodes-audio&gt; (version 2.2.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-audio/src/refcodes-audio-2.2.2/pom.xml) 

## Change list &lt;refcodes-codec&gt; (version 2.2.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-2.2.2/pom.xml) 

## Change list &lt;refcodes-component-ext&gt; (version 2.2.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-2.2.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-2.2.2/refcodes-component-ext-observer/pom.xml) 

## Change list &lt;refcodes-properties&gt; (version 2.2.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-2.2.2/pom.xml) 

## Change list &lt;refcodes-security&gt; (version 2.2.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security/src/refcodes-security-2.2.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`EncryptionOutputStream.java`](https://bitbucket.org/refcodes/refcodes-security/src/refcodes-security-2.2.2/src/main/java/org/refcodes/security/EncryptionOutputStream.java) (see Javadoc at [`EncryptionOutputStream.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-security/2.2.2/org.refcodes.security/org/refcodes/security/EncryptionOutputStream.html))

## Change list &lt;refcodes-security-alt&gt; (version 2.2.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-2.2.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-2.2.2/refcodes-security-alt-chaos/pom.xml) 

## Change list &lt;refcodes-security-ext&gt; (version 2.2.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-2.2.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-2.2.2/refcodes-security-ext-chaos/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-2.2.2/refcodes-security-ext-spring/pom.xml) 

## Change list &lt;refcodes-properties-ext&gt; (version 2.2.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-2.2.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-2.2.2/refcodes-properties-ext-cli/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-2.2.2/refcodes-properties-ext-obfuscation/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-2.2.2/refcodes-properties-ext-observer/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-2.2.2/refcodes-properties-ext-runtime/pom.xml) 

## Change list &lt;refcodes-logger&gt; (version 2.2.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger/src/refcodes-logger-2.2.2/pom.xml) 

## Change list &lt;refcodes-logger-alt&gt; (version 2.2.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-2.2.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-2.2.2/refcodes-logger-alt-async/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-2.2.2/refcodes-logger-alt-console/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-2.2.2/refcodes-logger-alt-io/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-2.2.2/refcodes-logger-alt-simpledb/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-2.2.2/refcodes-logger-alt-slf4j/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-2.2.2/refcodes-logger-alt-spring/pom.xml) 

## Change list &lt;refcodes-logger-ext&gt; (version 2.2.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-2.2.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-2.2.2/refcodes-logger-ext-slf4j/pom.xml) 

## Change list &lt;refcodes-graphical-ext&gt; (version 2.2.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-2.2.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-2.2.2/refcodes-graphical-ext-javafx/pom.xml) 

## Change list &lt;refcodes-checkerboard&gt; (version 2.2.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-2.2.2/pom.xml) 

## Change list &lt;refcodes-checkerboard-alt&gt; (version 2.2.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-alt/src/refcodes-checkerboard-alt-2.2.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-alt/src/refcodes-checkerboard-alt-2.2.2/refcodes-checkerboard-alt-javafx/pom.xml) 

## Change list &lt;refcodes-checkerboard-ext&gt; (version 2.2.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-ext/src/refcodes-checkerboard-ext-2.2.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-ext/src/refcodes-checkerboard-ext-2.2.2/refcodes-checkerboard-ext-javafx-boulderdash/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-ext/src/refcodes-checkerboard-ext-2.2.2/refcodes-checkerboard-ext-javafx-chess/pom.xml) 

## Change list &lt;refcodes-boulderdash&gt; (version 2.2.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-boulderdash/src/refcodes-boulderdash-2.2.2/pom.xml) 

## Change list &lt;refcodes-net&gt; (version 2.2.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-2.2.2/pom.xml) 

## Change list &lt;refcodes-web&gt; (version 2.2.2)

* \[<span style="color:blue">ADDED</span>\] [`AbstractMediaFactoryTest.java`](https://bitbucket.org/refcodes/refcodes-web/src/refcodes-web-2.2.2/src/test/java/org/refcodes/web/AbstractMediaFactoryTest.java) 
* \[<span style="color:blue">ADDED</span>\] [`ContentTypeTest.java`](https://bitbucket.org/refcodes/refcodes-web/src/refcodes-web-2.2.2/src/test/java/org/refcodes/web/ContentTypeTest.java) 
* \[<span style="color:blue">ADDED</span>\] [`CookieTest.java`](https://bitbucket.org/refcodes/refcodes-web/src/refcodes-web-2.2.2/src/test/java/org/refcodes/web/CookieTest.java) 
* \[<span style="color:blue">ADDED</span>\] [`FormFieldsTest.java`](https://bitbucket.org/refcodes/refcodes-web/src/refcodes-web-2.2.2/src/test/java/org/refcodes/web/FormFieldsTest.java) 
* \[<span style="color:blue">ADDED</span>\] [`FormMediaTypeFactoryTest.java`](https://bitbucket.org/refcodes/refcodes-web/src/refcodes-web-2.2.2/src/test/java/org/refcodes/web/FormMediaTypeFactoryTest.java) 
* \[<span style="color:blue">ADDED</span>\] [`HeaderFieldsTest.java`](https://bitbucket.org/refcodes/refcodes-web/src/refcodes-web-2.2.2/src/test/java/org/refcodes/web/HeaderFieldsTest.java) 
* \[<span style="color:blue">ADDED</span>\] [`HtmlMediaTypeFactoryTest.java`](https://bitbucket.org/refcodes/refcodes-web/src/refcodes-web-2.2.2/src/test/java/org/refcodes/web/HtmlMediaTypeFactoryTest.java) 
* \[<span style="color:blue">ADDED</span>\] [`HttpStatusCodeTest.java`](https://bitbucket.org/refcodes/refcodes-web/src/refcodes-web-2.2.2/src/test/java/org/refcodes/web/HttpStatusCodeTest.java) 
* \[<span style="color:blue">ADDED</span>\] [`JsonMediaTypeFactoryTest.java`](https://bitbucket.org/refcodes/refcodes-web/src/refcodes-web-2.2.2/src/test/java/org/refcodes/web/JsonMediaTypeFactoryTest.java) 
* \[<span style="color:blue">ADDED</span>\] [`MediaTypeFactoriesTest.java`](https://bitbucket.org/refcodes/refcodes-web/src/refcodes-web-2.2.2/src/test/java/org/refcodes/web/MediaTypeFactoriesTest.java) 
* \[<span style="color:blue">ADDED</span>\] [`MediaTypeTest.java`](https://bitbucket.org/refcodes/refcodes-web/src/refcodes-web-2.2.2/src/test/java/org/refcodes/web/MediaTypeTest.java) 
* \[<span style="color:blue">ADDED</span>\] [`OauthTokenTest.java`](https://bitbucket.org/refcodes/refcodes-web/src/refcodes-web-2.2.2/src/test/java/org/refcodes/web/OauthTokenTest.java) 
* \[<span style="color:blue">ADDED</span>\] [`RequestHeaderFieldsTest.java`](https://bitbucket.org/refcodes/refcodes-web/src/refcodes-web-2.2.2/src/test/java/org/refcodes/web/RequestHeaderFieldsTest.java) 
* \[<span style="color:blue">ADDED</span>\] [`ResponseHeaderFieldsTest.java`](https://bitbucket.org/refcodes/refcodes-web/src/refcodes-web-2.2.2/src/test/java/org/refcodes/web/ResponseHeaderFieldsTest.java) 
* \[<span style="color:blue">ADDED</span>\] [`UrlTest.java`](https://bitbucket.org/refcodes/refcodes-web/src/refcodes-web-2.2.2/src/test/java/org/refcodes/web/UrlTest.java) 
* \[<span style="color:blue">ADDED</span>\] [`XmlMediaTypeFactoryTest.java`](https://bitbucket.org/refcodes/refcodes-web/src/refcodes-web-2.2.2/src/test/java/org/refcodes/web/XmlMediaTypeFactoryTest.java) 
* \[<span style="color:blue">ADDED</span>\] [`YamlMediaTypeFactoryTest.java`](https://bitbucket.org/refcodes/refcodes-web/src/refcodes-web-2.2.2/src/test/java/org/refcodes/web/YamlMediaTypeFactoryTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-web/src/refcodes-web-2.2.2/pom.xml) 

## Change list &lt;refcodes-rest&gt; (version 2.2.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-2.2.2/pom.xml) 

## Change list &lt;refcodes-hal&gt; (version 2.2.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-2.2.2/pom.xml) 

## Change list &lt;refcodes-eventbus&gt; (version 2.2.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-eventbus/src/refcodes-eventbus-2.2.2/pom.xml) 

## Change list &lt;refcodes-eventbus-ext&gt; (version 2.2.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-2.2.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-2.2.2/refcodes-eventbus-ext-application/pom.xml) 

## Change list &lt;refcodes-filesystem&gt; (version 2.2.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-filesystem/src/refcodes-filesystem-2.2.2/pom.xml) 

## Change list &lt;refcodes-filesystem-alt&gt; (version 2.2.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-filesystem-alt/src/refcodes-filesystem-alt-2.2.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-filesystem-alt/src/refcodes-filesystem-alt-2.2.2/refcodes-filesystem-alt-s3/pom.xml) 

## Change list &lt;refcodes-forwardsecrecy&gt; (version 2.2.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy/src/refcodes-forwardsecrecy-2.2.2/pom.xml) 

## Change list &lt;refcodes-forwardsecrecy-alt&gt; (version 2.2.2)

* \[<span style="color:blue">ADDED</span>\] [`BasicForwardSecrecyFileSystemTest.java`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-2.2.2/refcodes-forwardsecrecy-alt-filesystem/src/test/java/org/refcodes/forwardsecrecy/alt/filesystem/BasicForwardSecrecyFileSystemTest.java) 
* \[<span style="color:blue">ADDED</span>\] [`ForwardSecrecyFileSystemTest.java`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-2.2.2/refcodes-forwardsecrecy-alt-filesystem/src/test/java/org/refcodes/forwardsecrecy/alt/filesystem/ForwardSecrecyFileSystemTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-2.2.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-2.2.2/refcodes-forwardsecrecy-alt-filesystem/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`FileSystemDecryptionServer.java`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-2.2.2/refcodes-forwardsecrecy-alt-filesystem/src/main/java/org/refcodes/forwardsecrecy/alt/filesystem/FileSystemDecryptionServer.java) (see Javadoc at [`FileSystemDecryptionServer.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-forwardsecrecy-alt-filesystem/2.2.2/org.refcodes.forwardsecrecy.alt.filesystem/org/refcodes/forwardsecrecy/alt/filesystem/FileSystemDecryptionServer.html))
* \[<span style="color:green">MODIFIED</span>\] [`FileSystemEncryptionServer.java`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-2.2.2/refcodes-forwardsecrecy-alt-filesystem/src/main/java/org/refcodes/forwardsecrecy/alt/filesystem/FileSystemEncryptionServer.java) (see Javadoc at [`FileSystemEncryptionServer.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-forwardsecrecy-alt-filesystem/2.2.2/org.refcodes.forwardsecrecy.alt.filesystem/org/refcodes/forwardsecrecy/alt/filesystem/FileSystemEncryptionServer.html))

## Change list &lt;refcodes-io-ext&gt; (version 2.2.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-2.2.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-2.2.2/refcodes-io-ext-observer/pom.xml) 

## Change list &lt;refcodes-interceptor&gt; (version 2.2.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-interceptor/src/refcodes-interceptor-2.2.2/pom.xml) 

## Change list &lt;refcodes-jobbus&gt; (version 2.2.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-jobbus/src/refcodes-jobbus-2.2.2/pom.xml) 

## Change list &lt;refcodes-rest-ext&gt; (version 2.2.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-2.2.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-2.2.2/refcodes-rest-ext-eureka/pom.xml) 

## Change list &lt;refcodes-remoting&gt; (version 2.2.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-remoting/src/refcodes-remoting-2.2.2/pom.xml) 

## Change list &lt;refcodes-remoting-ext&gt; (version 2.2.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-2.2.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-2.2.2/refcodes-remoting-ext-observer/pom.xml) 

## Change list &lt;refcodes-serial&gt; (version 2.2.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial/src/refcodes-serial-2.2.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`AbstractReadyToSendTransmissionDecorator.java`](https://bitbucket.org/refcodes/refcodes-serial/src/refcodes-serial-2.2.2/src/main/java/org/refcodes/serial/AbstractReadyToSendTransmissionDecorator.java) (see Javadoc at [`AbstractReadyToSendTransmissionDecorator.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-serial/2.2.2/org.refcodes.serial/org/refcodes/serial/AbstractReadyToSendTransmissionDecorator.html))
* \[<span style="color:green">MODIFIED</span>\] [`ComplexTypeSegment.java`](https://bitbucket.org/refcodes/refcodes-serial/src/refcodes-serial-2.2.2/src/main/java/org/refcodes/serial/ComplexTypeSegment.java) (see Javadoc at [`ComplexTypeSegment.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-serial/2.2.2/org.refcodes.serial/org/refcodes/serial/ComplexTypeSegment.html))
* \[<span style="color:green">MODIFIED</span>\] [`EnumSegment.java`](https://bitbucket.org/refcodes/refcodes-serial/src/refcodes-serial-2.2.2/src/main/java/org/refcodes/serial/EnumSegment.java) (see Javadoc at [`EnumSegment.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-serial/2.2.2/org.refcodes.serial/org/refcodes/serial/EnumSegment.html))
* \[<span style="color:green">MODIFIED</span>\] [`ReadyToSendSegmentDecorator.java`](https://bitbucket.org/refcodes/refcodes-serial/src/refcodes-serial-2.2.2/src/main/java/org/refcodes/serial/ReadyToSendSegmentDecorator.java) (see Javadoc at [`ReadyToSendSegmentDecorator.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-serial/2.2.2/org.refcodes.serial/org/refcodes/serial/ReadyToSendSegmentDecorator.html))
* \[<span style="color:green">MODIFIED</span>\] [`ComplexTypeSegmentTest.java`](https://bitbucket.org/refcodes/refcodes-serial/src/refcodes-serial-2.2.2/src/test/java/org/refcodes/serial/ComplexTypeSegmentTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`StopAndWaitTest.java`](https://bitbucket.org/refcodes/refcodes-serial/src/refcodes-serial-2.2.2/src/test/java/org/refcodes/serial/StopAndWaitTest.java) 

## Change list &lt;refcodes-serial-alt&gt; (version 2.2.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial-alt/src/refcodes-serial-alt-2.2.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial-alt/src/refcodes-serial-alt-2.2.2/refcodes-serial-alt-net/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial-alt/src/refcodes-serial-alt-2.2.2/refcodes-serial-alt-tty/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`TtyStopAndWaitTest.java`](https://bitbucket.org/refcodes/refcodes-serial-alt/src/refcodes-serial-alt-2.2.2/refcodes-serial-alt-tty/src/test/java/org/refcodes/serial/alt/tty/TtyStopAndWaitTest.java) 

## Change list &lt;refcodes-serial-ext&gt; (version 2.2.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-2.2.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-2.2.2/refcodes-serial-ext-handshake/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-2.2.2/refcodes-serial-ext-observer/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-2.2.2/refcodes-serial-ext-security/pom.xml) 

## Change list &lt;refcodes-p2p&gt; (version 2.2.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-p2p/src/refcodes-p2p-2.2.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`PeerRouter.java`](https://bitbucket.org/refcodes/refcodes-p2p/src/refcodes-p2p-2.2.2/src/main/java/org/refcodes/p2p/PeerRouter.java) (see Javadoc at [`PeerRouter.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-p2p/2.2.2/org.refcodes.p2p/org/refcodes/p2p/PeerRouter.html))

## Change list &lt;refcodes-p2p-alt&gt; (version 2.2.2)

* \[<span style="color:blue">ADDED</span>\] [`SerialP2PMessageTest.java`](https://bitbucket.org/refcodes/refcodes-p2p-alt/src/refcodes-p2p-alt-2.2.2/refcodes-p2p-alt-serial/src/test/java/org/refcodes/p2p/alt/serial/SerialP2PMessageTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-p2p-alt/src/refcodes-p2p-alt-2.2.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-p2p-alt/src/refcodes-p2p-alt-2.2.2/refcodes-p2p-alt-rest/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-p2p-alt/src/refcodes-p2p-alt-2.2.2/refcodes-p2p-alt-serial/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`HopCountTest.java`](https://bitbucket.org/refcodes/refcodes-p2p-alt/src/refcodes-p2p-alt-2.2.2/refcodes-p2p-alt-serial/src/test/java/org/refcodes/p2p/alt/serial/HopCountTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`SerialPeerTest.java`](https://bitbucket.org/refcodes/refcodes-p2p-alt/src/refcodes-p2p-alt-2.2.2/refcodes-p2p-alt-serial/src/test/java/org/refcodes/p2p/alt/serial/SerialPeerTest.java) 

## Change list &lt;refcodes-p2p-ext&gt; (version 2.2.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-p2p-ext/src/refcodes-p2p-ext-2.2.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-p2p-ext/src/refcodes-p2p-ext-2.2.2/refcodes-p2p-ext-observer/pom.xml) 

## Change list &lt;refcodes-servicebus&gt; (version 2.2.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-servicebus/src/refcodes-servicebus-2.2.2/pom.xml) 

## Change list &lt;refcodes-servicebus-alt&gt; (version 2.2.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-servicebus-alt/src/refcodes-servicebus-alt-2.2.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-servicebus-alt/src/refcodes-servicebus-alt-2.2.2/refcodes-servicebus-alt-spring/pom.xml) 

## Change list &lt;refcodes-tabular-alt&gt; (version 2.2.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-tabular-alt/src/refcodes-tabular-alt-2.2.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-tabular-alt/src/refcodes-tabular-alt-2.2.2/refcodes-tabular-alt-forwardsecrecy/pom.xml) 

## Change list &lt;refcodes-archetype&gt; (version 2.2.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype/src/refcodes-archetype-2.2.2/pom.xml) 

## Change list &lt;refcodes-archetype-alt&gt; (version 2.2.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-2.2.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-2.2.2/refcodes-archetype-alt-c2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-2.2.2/refcodes-archetype-alt-c2/src/main/resources/archetype-resources/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-2.2.2/refcodes-archetype-alt-cli/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-2.2.2/refcodes-archetype-alt-cli/src/main/resources/archetype-resources/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-2.2.2/refcodes-archetype-alt-csv/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-2.2.2/refcodes-archetype-alt-csv/src/main/resources/archetype-resources/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-2.2.2/refcodes-archetype-alt-eventbus/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-2.2.2/refcodes-archetype-alt-eventbus/src/main/resources/archetype-resources/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-2.2.2/refcodes-archetype-alt-rest/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-2.2.2/refcodes-archetype-alt-rest/src/main/resources/archetype-resources/pom.xml) 
