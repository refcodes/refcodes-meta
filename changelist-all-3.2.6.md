> This change list has been auto-generated on `triton` by `steiner` with `changelist-all.sh` on the 2023-06-23 at 16:58:05.

## Change list &lt;refcodes-licensing&gt; (version 3.2.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-licensing/src/refcodes-licensing-3.2.6/pom.xml) 

## Change list &lt;refcodes-parent&gt; (version 3.2.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-parent/src/refcodes-parent-3.2.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-parent/src/refcodes-parent-3.2.6/README.md) 

## Change list &lt;refcodes-time&gt; (version 3.2.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-time/src/refcodes-time-3.2.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-time/src/refcodes-time-3.2.6/README.md) 

## Change list &lt;refcodes-mixin&gt; (version 3.2.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-mixin/src/refcodes-mixin-3.2.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-mixin/src/refcodes-mixin-3.2.6/README.md) 

## Change list &lt;refcodes-data&gt; (version 3.2.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data/src/refcodes-data-3.2.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data/src/refcodes-data-3.2.6/README.md) 

## Change list &lt;refcodes-exception&gt; (version 3.2.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-exception/src/refcodes-exception-3.2.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-exception/src/refcodes-exception-3.2.6/README.md) 

## Change list &lt;refcodes-factory&gt; (version 3.2.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-factory/src/refcodes-factory-3.2.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-factory/src/refcodes-factory-3.2.6/README.md) 

## Change list &lt;refcodes-factory-alt&gt; (version 3.2.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-factory-alt/src/refcodes-factory-alt-3.2.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-factory-alt/src/refcodes-factory-alt-3.2.6/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-factory-alt/src/refcodes-factory-alt-3.2.6/refcodes-factory-alt-spring/pom.xml) 

## Change list &lt;refcodes-controlflow&gt; (version 3.2.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-controlflow/src/refcodes-controlflow-3.2.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-controlflow/src/refcodes-controlflow-3.2.6/README.md) 

## Change list &lt;refcodes-numerical&gt; (version 3.2.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-numerical/src/refcodes-numerical-3.2.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-numerical/src/refcodes-numerical-3.2.6/README.md) 

## Change list &lt;refcodes-generator&gt; (version 3.2.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-generator/src/refcodes-generator-3.2.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-generator/src/refcodes-generator-3.2.6/README.md) 

## Change list &lt;refcodes-matcher&gt; (version 3.2.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-matcher/src/refcodes-matcher-3.2.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-matcher/src/refcodes-matcher-3.2.6/README.md) 

## Change list &lt;refcodes-struct&gt; (version 3.2.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-struct/src/refcodes-struct-3.2.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-struct/src/refcodes-struct-3.2.6/README.md) 

## Change list &lt;refcodes-struct-ext&gt; (version 3.2.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-struct-ext/src/refcodes-struct-ext-3.2.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-struct-ext/src/refcodes-struct-ext-3.2.6/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-struct-ext/src/refcodes-struct-ext-3.2.6/refcodes-struct-ext-factory/pom.xml) 

## Change list &lt;refcodes-runtime&gt; (version 3.2.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-3.2.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-3.2.6/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`Execution.java`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-3.2.6/src/main/java/org/refcodes/runtime/Execution.java) (see Javadoc at [`Execution.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-runtime/3.2.6/org.refcodes.runtime/org/refcodes/runtime/Execution.html))
* \[<span style="color:green">MODIFIED</span>\] [`Host.java`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-3.2.6/src/main/java/org/refcodes/runtime/Host.java) (see Javadoc at [`Host.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-runtime/3.2.6/org.refcodes.runtime/org/refcodes/runtime/Host.html))
* \[<span style="color:green">MODIFIED</span>\] [`Terminal.java`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-3.2.6/src/main/java/org/refcodes/runtime/Terminal.java) (see Javadoc at [`Terminal.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-runtime/3.2.6/org.refcodes.runtime/org/refcodes/runtime/Terminal.html))

## Change list &lt;refcodes-component&gt; (version 3.2.6)

* \[<span style="color:blue">ADDED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.2.6/README.md) 
* \[<span style="color:blue">ADDED</span>\] [`module-info.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.2.6/src/main/java/module-info.java) (see Javadoc at [`module-info.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/3.2.6/./module-info.html))
* \[<span style="color:blue">ADDED</span>\] [`AbstractComponentComposite.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.2.6/src/main/java/org/refcodes/component/AbstractComponentComposite.java) (see Javadoc at [`AbstractComponentComposite.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/3.2.6/org.refcodes.component/org/refcodes/component/AbstractComponentComposite.html))
* \[<span style="color:blue">ADDED</span>\] [`AbstractConnectableAutomaton.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.2.6/src/main/java/org/refcodes/component/AbstractConnectableAutomaton.java) (see Javadoc at [`AbstractConnectableAutomaton.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/3.2.6/org.refcodes.component/org/refcodes/component/AbstractConnectableAutomaton.html))
* \[<span style="color:blue">ADDED</span>\] [`AbstractConnectable.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.2.6/src/main/java/org/refcodes/component/AbstractConnectable.java) (see Javadoc at [`AbstractConnectable.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/3.2.6/org.refcodes.component/org/refcodes/component/AbstractConnectable.html))
* \[<span style="color:blue">ADDED</span>\] [`AbstractDeviceAutomaton.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.2.6/src/main/java/org/refcodes/component/AbstractDeviceAutomaton.java) (see Javadoc at [`AbstractDeviceAutomaton.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/3.2.6/org.refcodes.component/org/refcodes/component/AbstractDeviceAutomaton.html))
* \[<span style="color:blue">ADDED</span>\] [`BidirectionalConnectionAccessor.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.2.6/src/main/java/org/refcodes/component/BidirectionalConnectionAccessor.java) (see Javadoc at [`BidirectionalConnectionAccessor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/3.2.6/org.refcodes.component/org/refcodes/component/BidirectionalConnectionAccessor.html))
* \[<span style="color:blue">ADDED</span>\] [`BidirectionalConnectionComponent.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.2.6/src/main/java/org/refcodes/component/BidirectionalConnectionComponent.java) (see Javadoc at [`BidirectionalConnectionComponent.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/3.2.6/org.refcodes.component/org/refcodes/component/BidirectionalConnectionComponent.html))
* \[<span style="color:blue">ADDED</span>\] [`BidirectionalConnectionOpenableHandle.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.2.6/src/main/java/org/refcodes/component/BidirectionalConnectionOpenableHandle.java) (see Javadoc at [`BidirectionalConnectionOpenableHandle.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/3.2.6/org.refcodes.component/org/refcodes/component/BidirectionalConnectionOpenableHandle.html))
* \[<span style="color:blue">ADDED</span>\] [`BidirectionalConnectionOpenable.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.2.6/src/main/java/org/refcodes/component/BidirectionalConnectionOpenable.java) (see Javadoc at [`BidirectionalConnectionOpenable.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/3.2.6/org.refcodes.component/org/refcodes/component/BidirectionalConnectionOpenable.html))
* \[<span style="color:blue">ADDED</span>\] [`CeasableHandle.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.2.6/src/main/java/org/refcodes/component/CeasableHandle.java) (see Javadoc at [`CeasableHandle.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/3.2.6/org.refcodes.component/org/refcodes/component/CeasableHandle.html))
* \[<span style="color:blue">ADDED</span>\] [`Ceasable.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.2.6/src/main/java/org/refcodes/component/Ceasable.java) (see Javadoc at [`Ceasable.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/3.2.6/org.refcodes.component/org/refcodes/component/Ceasable.html))
* \[<span style="color:blue">ADDED</span>\] [`CeaseException.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.2.6/src/main/java/org/refcodes/component/CeaseException.java) (see Javadoc at [`CeaseException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/3.2.6/org.refcodes.component/org/refcodes/component/CeaseException.html))
* \[<span style="color:blue">ADDED</span>\] [`ClosableHandle.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.2.6/src/main/java/org/refcodes/component/ClosableHandle.java) (see Javadoc at [`ClosableHandle.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/3.2.6/org.refcodes.component/org/refcodes/component/ClosableHandle.html))
* \[<span style="color:blue">ADDED</span>\] [`Closable.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.2.6/src/main/java/org/refcodes/component/Closable.java) (see Javadoc at [`Closable.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/3.2.6/org.refcodes.component/org/refcodes/component/Closable.html))
* \[<span style="color:blue">ADDED</span>\] [`ClosedAccessor.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.2.6/src/main/java/org/refcodes/component/ClosedAccessor.java) (see Javadoc at [`ClosedAccessor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/3.2.6/org.refcodes.component/org/refcodes/component/ClosedAccessor.html))
* \[<span style="color:blue">ADDED</span>\] [`CloseException.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.2.6/src/main/java/org/refcodes/component/CloseException.java) (see Javadoc at [`CloseException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/3.2.6/org.refcodes.component/org/refcodes/component/CloseException.html))
* \[<span style="color:blue">ADDED</span>\] [`ComponentComposite.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.2.6/src/main/java/org/refcodes/component/ComponentComposite.java) (see Javadoc at [`ComponentComposite.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/3.2.6/org.refcodes.component/org/refcodes/component/ComponentComposite.html))
* \[<span style="color:blue">ADDED</span>\] [`ComponentException.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.2.6/src/main/java/org/refcodes/component/ComponentException.java) (see Javadoc at [`ComponentException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/3.2.6/org.refcodes.component/org/refcodes/component/ComponentException.html))
* \[<span style="color:blue">ADDED</span>\] [`ComponentHandleComposite.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.2.6/src/main/java/org/refcodes/component/ComponentHandleComposite.java) (see Javadoc at [`ComponentHandleComposite.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/3.2.6/org.refcodes.component/org/refcodes/component/ComponentHandleComposite.html))
* \[<span style="color:blue">ADDED</span>\] [`ComponentRuntimeException.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.2.6/src/main/java/org/refcodes/component/ComponentRuntimeException.java) (see Javadoc at [`ComponentRuntimeException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/3.2.6/org.refcodes.component/org/refcodes/component/ComponentRuntimeException.html))
* \[<span style="color:blue">ADDED</span>\] [`ComponentUtility.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.2.6/src/main/java/org/refcodes/component/ComponentUtility.java) (see Javadoc at [`ComponentUtility.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/3.2.6/org.refcodes.component/org/refcodes/component/ComponentUtility.html))
* \[<span style="color:blue">ADDED</span>\] [`ConfigurableHandle.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.2.6/src/main/java/org/refcodes/component/ConfigurableHandle.java) (see Javadoc at [`ConfigurableHandle.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/3.2.6/org.refcodes.component/org/refcodes/component/ConfigurableHandle.html))
* \[<span style="color:blue">ADDED</span>\] [`Configurable.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.2.6/src/main/java/org/refcodes/component/Configurable.java) (see Javadoc at [`Configurable.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/3.2.6/org.refcodes.component/org/refcodes/component/Configurable.html))
* \[<span style="color:blue">ADDED</span>\] [`ConfigurableLifecycleAutomatonImpl.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.2.6/src/main/java/org/refcodes/component/ConfigurableLifecycleAutomatonImpl.java) (see Javadoc at [`ConfigurableLifecycleAutomatonImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/3.2.6/org.refcodes.component/org/refcodes/component/ConfigurableLifecycleAutomatonImpl.html))
* \[<span style="color:blue">ADDED</span>\] [`ConfigurableLifecycleComponentHandle.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.2.6/src/main/java/org/refcodes/component/ConfigurableLifecycleComponentHandle.java) (see Javadoc at [`ConfigurableLifecycleComponentHandle.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/3.2.6/org.refcodes.component/org/refcodes/component/ConfigurableLifecycleComponentHandle.html))
* \[<span style="color:blue">ADDED</span>\] [`ConfigurableLifecycleComponent.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.2.6/src/main/java/org/refcodes/component/ConfigurableLifecycleComponent.java) (see Javadoc at [`ConfigurableLifecycleComponent.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/3.2.6/org.refcodes.component/org/refcodes/component/ConfigurableLifecycleComponent.html))
* \[<span style="color:blue">ADDED</span>\] [`ConfigureException.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.2.6/src/main/java/org/refcodes/component/ConfigureException.java) (see Javadoc at [`ConfigureException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/3.2.6/org.refcodes.component/org/refcodes/component/ConfigureException.html))
* \[<span style="color:blue">ADDED</span>\] [`ConnectableComponent.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.2.6/src/main/java/org/refcodes/component/ConnectableComponent.java) (see Javadoc at [`ConnectableComponent.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/3.2.6/org.refcodes.component/org/refcodes/component/ConnectableComponent.html))
* \[<span style="color:blue">ADDED</span>\] [`ConnectionAccessor.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.2.6/src/main/java/org/refcodes/component/ConnectionAccessor.java) (see Javadoc at [`ConnectionAccessor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/3.2.6/org.refcodes.component/org/refcodes/component/ConnectionAccessor.html))
* \[<span style="color:blue">ADDED</span>\] [`ConnectionAutomatonImpl.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.2.6/src/main/java/org/refcodes/component/ConnectionAutomatonImpl.java) (see Javadoc at [`ConnectionAutomatonImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/3.2.6/org.refcodes.component/org/refcodes/component/ConnectionAutomatonImpl.html))
* \[<span style="color:blue">ADDED</span>\] [`ConnectionComponentHandle.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.2.6/src/main/java/org/refcodes/component/ConnectionComponentHandle.java) (see Javadoc at [`ConnectionComponentHandle.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/3.2.6/org.refcodes.component/org/refcodes/component/ConnectionComponentHandle.html))
* \[<span style="color:blue">ADDED</span>\] [`ConnectionComponent.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.2.6/src/main/java/org/refcodes/component/ConnectionComponent.java) (see Javadoc at [`ConnectionComponent.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/3.2.6/org.refcodes.component/org/refcodes/component/ConnectionComponent.html))
* \[<span style="color:blue">ADDED</span>\] [`ConnectionOpenableHandle.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.2.6/src/main/java/org/refcodes/component/ConnectionOpenableHandle.java) (see Javadoc at [`ConnectionOpenableHandle.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/3.2.6/org.refcodes.component/org/refcodes/component/ConnectionOpenableHandle.html))
* \[<span style="color:blue">ADDED</span>\] [`ConnectionOpenable.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.2.6/src/main/java/org/refcodes/component/ConnectionOpenable.java) (see Javadoc at [`ConnectionOpenable.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/3.2.6/org.refcodes.component/org/refcodes/component/ConnectionOpenable.html))
* \[<span style="color:blue">ADDED</span>\] [`ConnectionRequest.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.2.6/src/main/java/org/refcodes/component/ConnectionRequest.java) (see Javadoc at [`ConnectionRequest.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/3.2.6/org.refcodes.component/org/refcodes/component/ConnectionRequest.html))
* \[<span style="color:blue">ADDED</span>\] [`ConnectionStatusAccessor.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.2.6/src/main/java/org/refcodes/component/ConnectionStatusAccessor.java) (see Javadoc at [`ConnectionStatusAccessor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/3.2.6/org.refcodes.component/org/refcodes/component/ConnectionStatusAccessor.html))
* \[<span style="color:blue">ADDED</span>\] [`ConnectionStatusHandle.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.2.6/src/main/java/org/refcodes/component/ConnectionStatusHandle.java) (see Javadoc at [`ConnectionStatusHandle.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/3.2.6/org.refcodes.component/org/refcodes/component/ConnectionStatusHandle.html))
* \[<span style="color:blue">ADDED</span>\] [`ConnectionStatus.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.2.6/src/main/java/org/refcodes/component/ConnectionStatus.java) (see Javadoc at [`ConnectionStatus.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/3.2.6/org.refcodes.component/org/refcodes/component/ConnectionStatus.html))
* \[<span style="color:blue">ADDED</span>\] [`ContextAccessor.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.2.6/src/main/java/org/refcodes/component/ContextAccessor.java) (see Javadoc at [`ContextAccessor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/3.2.6/org.refcodes.component/org/refcodes/component/ContextAccessor.html))
* \[<span style="color:blue">ADDED</span>\] [`DecomposableHandle.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.2.6/src/main/java/org/refcodes/component/DecomposableHandle.java) (see Javadoc at [`DecomposableHandle.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/3.2.6/org.refcodes.component/org/refcodes/component/DecomposableHandle.html))
* \[<span style="color:blue">ADDED</span>\] [`Decomposable.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.2.6/src/main/java/org/refcodes/component/Decomposable.java) (see Javadoc at [`Decomposable.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/3.2.6/org.refcodes.component/org/refcodes/component/Decomposable.html))
* \[<span style="color:blue">ADDED</span>\] [`DecomposeException.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.2.6/src/main/java/org/refcodes/component/DecomposeException.java) (see Javadoc at [`DecomposeException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/3.2.6/org.refcodes.component/org/refcodes/component/DecomposeException.html))
* \[<span style="color:blue">ADDED</span>\] [`DestroyableHandle.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.2.6/src/main/java/org/refcodes/component/DestroyableHandle.java) (see Javadoc at [`DestroyableHandle.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/3.2.6/org.refcodes.component/org/refcodes/component/DestroyableHandle.html))
* \[<span style="color:blue">ADDED</span>\] [`Destroyable.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.2.6/src/main/java/org/refcodes/component/Destroyable.java) (see Javadoc at [`Destroyable.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/3.2.6/org.refcodes.component/org/refcodes/component/Destroyable.html))
* \[<span style="color:blue">ADDED</span>\] [`DestroyException.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.2.6/src/main/java/org/refcodes/component/DestroyException.java) (see Javadoc at [`DestroyException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/3.2.6/org.refcodes.component/org/refcodes/component/DestroyException.html))
* \[<span style="color:blue">ADDED</span>\] [`DigesterComponent.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.2.6/src/main/java/org/refcodes/component/DigesterComponent.java) (see Javadoc at [`DigesterComponent.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/3.2.6/org.refcodes.component/org/refcodes/component/DigesterComponent.html))
* \[<span style="color:blue">ADDED</span>\] [`Digester.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.2.6/src/main/java/org/refcodes/component/Digester.java) (see Javadoc at [`Digester.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/3.2.6/org.refcodes.component/org/refcodes/component/Digester.html))
* \[<span style="color:blue">ADDED</span>\] [`DigestException.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.2.6/src/main/java/org/refcodes/component/DigestException.java) (see Javadoc at [`DigestException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/3.2.6/org.refcodes.component/org/refcodes/component/DigestException.html))
* \[<span style="color:blue">ADDED</span>\] [`DisposeException.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.2.6/src/main/java/org/refcodes/component/DisposeException.java) (see Javadoc at [`DisposeException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/3.2.6/org.refcodes.component/org/refcodes/component/DisposeException.html))
* \[<span style="color:blue">ADDED</span>\] [`Flushable.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.2.6/src/main/java/org/refcodes/component/Flushable.java) (see Javadoc at [`Flushable.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/3.2.6/org.refcodes.component/org/refcodes/component/Flushable.html))
* \[<span style="color:blue">ADDED</span>\] [`FlushHandle.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.2.6/src/main/java/org/refcodes/component/FlushHandle.java) (see Javadoc at [`FlushHandle.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/3.2.6/org.refcodes.component/org/refcodes/component/FlushHandle.html))
* \[<span style="color:blue">ADDED</span>\] [`HandleAccessor.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.2.6/src/main/java/org/refcodes/component/HandleAccessor.java) (see Javadoc at [`HandleAccessor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/3.2.6/org.refcodes.component/org/refcodes/component/HandleAccessor.html))
* \[<span style="color:blue">ADDED</span>\] [`HandleDirectory.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.2.6/src/main/java/org/refcodes/component/HandleDirectory.java) (see Javadoc at [`HandleDirectory.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/3.2.6/org.refcodes.component/org/refcodes/component/HandleDirectory.html))
* \[<span style="color:blue">ADDED</span>\] [`HandleGeneratorImpl.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.2.6/src/main/java/org/refcodes/component/HandleGeneratorImpl.java) (see Javadoc at [`HandleGeneratorImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/3.2.6/org.refcodes.component/org/refcodes/component/HandleGeneratorImpl.html))
* \[<span style="color:blue">ADDED</span>\] [`HandleGenerator.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.2.6/src/main/java/org/refcodes/component/HandleGenerator.java) (see Javadoc at [`HandleGenerator.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/3.2.6/org.refcodes.component/org/refcodes/component/HandleGenerator.html))
* \[<span style="color:blue">ADDED</span>\] [`HandleLookup.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.2.6/src/main/java/org/refcodes/component/HandleLookup.java) (see Javadoc at [`HandleLookup.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/3.2.6/org.refcodes.component/org/refcodes/component/HandleLookup.html))
* \[<span style="color:blue">ADDED</span>\] [`HandleTimeoutRuntimeException.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.2.6/src/main/java/org/refcodes/component/HandleTimeoutRuntimeException.java) (see Javadoc at [`HandleTimeoutRuntimeException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/3.2.6/org.refcodes.component/org/refcodes/component/HandleTimeoutRuntimeException.html))
* \[<span style="color:blue">ADDED</span>\] [`IllegalHandleStateChangeRuntimeException.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.2.6/src/main/java/org/refcodes/component/IllegalHandleStateChangeRuntimeException.java) (see Javadoc at [`IllegalHandleStateChangeRuntimeException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/3.2.6/org.refcodes.component/org/refcodes/component/IllegalHandleStateChangeRuntimeException.html))
* \[<span style="color:blue">ADDED</span>\] [`InitializableComponent.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.2.6/src/main/java/org/refcodes/component/InitializableComponent.java) (see Javadoc at [`InitializableComponent.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/3.2.6/org.refcodes.component/org/refcodes/component/InitializableComponent.html))
* \[<span style="color:blue">ADDED</span>\] [`InitializableHandle.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.2.6/src/main/java/org/refcodes/component/InitializableHandle.java) (see Javadoc at [`InitializableHandle.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/3.2.6/org.refcodes.component/org/refcodes/component/InitializableHandle.html))
* \[<span style="color:blue">ADDED</span>\] [`Initializable.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.2.6/src/main/java/org/refcodes/component/Initializable.java) (see Javadoc at [`Initializable.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/3.2.6/org.refcodes.component/org/refcodes/component/Initializable.html))
* \[<span style="color:blue">ADDED</span>\] [`InitializedAccessor.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.2.6/src/main/java/org/refcodes/component/InitializedAccessor.java) (see Javadoc at [`InitializedAccessor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/3.2.6/org.refcodes.component/org/refcodes/component/InitializedAccessor.html))
* \[<span style="color:blue">ADDED</span>\] [`InitializedHandle.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.2.6/src/main/java/org/refcodes/component/InitializedHandle.java) (see Javadoc at [`InitializedHandle.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/3.2.6/org.refcodes.component/org/refcodes/component/InitializedHandle.html))
* \[<span style="color:blue">ADDED</span>\] [`InitializeException.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.2.6/src/main/java/org/refcodes/component/InitializeException.java) (see Javadoc at [`InitializeException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/3.2.6/org.refcodes.component/org/refcodes/component/InitializeException.html))
* \[<span style="color:blue">ADDED</span>\] [`LifecycleComponentHandle.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.2.6/src/main/java/org/refcodes/component/LifecycleComponentHandle.java) (see Javadoc at [`LifecycleComponentHandle.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/3.2.6/org.refcodes.component/org/refcodes/component/LifecycleComponentHandle.html))
* \[<span style="color:blue">ADDED</span>\] [`LifecycleComponent.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.2.6/src/main/java/org/refcodes/component/LifecycleComponent.java) (see Javadoc at [`LifecycleComponent.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/3.2.6/org.refcodes.component/org/refcodes/component/LifecycleComponent.html))
* \[<span style="color:blue">ADDED</span>\] [`LifecycleException.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.2.6/src/main/java/org/refcodes/component/LifecycleException.java) (see Javadoc at [`LifecycleException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/3.2.6/org.refcodes.component/org/refcodes/component/LifecycleException.html))
* \[<span style="color:blue">ADDED</span>\] [`LifecycleMachine.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.2.6/src/main/java/org/refcodes/component/LifecycleMachine.java) (see Javadoc at [`LifecycleMachine.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/3.2.6/org.refcodes.component/org/refcodes/component/LifecycleMachine.html))
* \[<span style="color:blue">ADDED</span>\] [`LifecycleRequestAccessor.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.2.6/src/main/java/org/refcodes/component/LifecycleRequestAccessor.java) (see Javadoc at [`LifecycleRequestAccessor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/3.2.6/org.refcodes.component/org/refcodes/component/LifecycleRequestAccessor.html))
* \[<span style="color:blue">ADDED</span>\] [`LifecycleRequest.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.2.6/src/main/java/org/refcodes/component/LifecycleRequest.java) (see Javadoc at [`LifecycleRequest.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/3.2.6/org.refcodes.component/org/refcodes/component/LifecycleRequest.html))
* \[<span style="color:blue">ADDED</span>\] [`LifecycleStatusAccessor.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.2.6/src/main/java/org/refcodes/component/LifecycleStatusAccessor.java) (see Javadoc at [`LifecycleStatusAccessor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/3.2.6/org.refcodes.component/org/refcodes/component/LifecycleStatusAccessor.html))
* \[<span style="color:blue">ADDED</span>\] [`LifecycleStatusHandle.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.2.6/src/main/java/org/refcodes/component/LifecycleStatusHandle.java) (see Javadoc at [`LifecycleStatusHandle.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/3.2.6/org.refcodes.component/org/refcodes/component/LifecycleStatusHandle.html))
* \[<span style="color:blue">ADDED</span>\] [`LifecycleStatus.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.2.6/src/main/java/org/refcodes/component/LifecycleStatus.java) (see Javadoc at [`LifecycleStatus.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/3.2.6/org.refcodes.component/org/refcodes/component/LifecycleStatus.html))
* \[<span style="color:blue">ADDED</span>\] [`LinkAutomatonImpl.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.2.6/src/main/java/org/refcodes/component/LinkAutomatonImpl.java) (see Javadoc at [`LinkAutomatonImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/3.2.6/org.refcodes.component/org/refcodes/component/LinkAutomatonImpl.html))
* \[<span style="color:blue">ADDED</span>\] [`LinkComponentHandle.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.2.6/src/main/java/org/refcodes/component/LinkComponentHandle.java) (see Javadoc at [`LinkComponentHandle.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/3.2.6/org.refcodes.component/org/refcodes/component/LinkComponentHandle.html))
* \[<span style="color:blue">ADDED</span>\] [`LinkComponent.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.2.6/src/main/java/org/refcodes/component/LinkComponent.java) (see Javadoc at [`LinkComponent.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/3.2.6/org.refcodes.component/org/refcodes/component/LinkComponent.html))
* \[<span style="color:blue">ADDED</span>\] [`OpenableHandle.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.2.6/src/main/java/org/refcodes/component/OpenableHandle.java) (see Javadoc at [`OpenableHandle.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/3.2.6/org.refcodes.component/org/refcodes/component/OpenableHandle.html))
* \[<span style="color:blue">ADDED</span>\] [`Openable.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.2.6/src/main/java/org/refcodes/component/Openable.java) (see Javadoc at [`Openable.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/3.2.6/org.refcodes.component/org/refcodes/component/Openable.html))
* \[<span style="color:blue">ADDED</span>\] [`OpenedAccessor.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.2.6/src/main/java/org/refcodes/component/OpenedAccessor.java) (see Javadoc at [`OpenedAccessor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/3.2.6/org.refcodes.component/org/refcodes/component/OpenedAccessor.html))
* \[<span style="color:blue">ADDED</span>\] [`OpenedHandle.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.2.6/src/main/java/org/refcodes/component/OpenedHandle.java) (see Javadoc at [`OpenedHandle.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/3.2.6/org.refcodes.component/org/refcodes/component/OpenedHandle.html))
* \[<span style="color:blue">ADDED</span>\] [`PausableHandle.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.2.6/src/main/java/org/refcodes/component/PausableHandle.java) (see Javadoc at [`PausableHandle.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/3.2.6/org.refcodes.component/org/refcodes/component/PausableHandle.html))
* \[<span style="color:blue">ADDED</span>\] [`Pausable.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.2.6/src/main/java/org/refcodes/component/Pausable.java) (see Javadoc at [`Pausable.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/3.2.6/org.refcodes.component/org/refcodes/component/Pausable.html))
* \[<span style="color:blue">ADDED</span>\] [`PauseException.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.2.6/src/main/java/org/refcodes/component/PauseException.java) (see Javadoc at [`PauseException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/3.2.6/org.refcodes.component/org/refcodes/component/PauseException.html))
* \[<span style="color:blue">ADDED</span>\] [`ProgressAccessor.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.2.6/src/main/java/org/refcodes/component/ProgressAccessor.java) (see Javadoc at [`ProgressAccessor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/3.2.6/org.refcodes.component/org/refcodes/component/ProgressAccessor.html))
* \[<span style="color:blue">ADDED</span>\] [`ProgressHandle.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.2.6/src/main/java/org/refcodes/component/ProgressHandle.java) (see Javadoc at [`ProgressHandle.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/3.2.6/org.refcodes.component/org/refcodes/component/ProgressHandle.html))
* \[<span style="color:blue">ADDED</span>\] [`Reloadable.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.2.6/src/main/java/org/refcodes/component/Reloadable.java) (see Javadoc at [`Reloadable.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/3.2.6/org.refcodes.component/org/refcodes/component/Reloadable.html))
* \[<span style="color:blue">ADDED</span>\] [`ReloadHandle.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.2.6/src/main/java/org/refcodes/component/ReloadHandle.java) (see Javadoc at [`ReloadHandle.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/3.2.6/org.refcodes.component/org/refcodes/component/ReloadHandle.html))
* \[<span style="color:blue">ADDED</span>\] [`ResetException.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.2.6/src/main/java/org/refcodes/component/ResetException.java) (see Javadoc at [`ResetException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/3.2.6/org.refcodes.component/org/refcodes/component/ResetException.html))
* \[<span style="color:blue">ADDED</span>\] [`ResetHandle.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.2.6/src/main/java/org/refcodes/component/ResetHandle.java) (see Javadoc at [`ResetHandle.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/3.2.6/org.refcodes.component/org/refcodes/component/ResetHandle.html))
* \[<span style="color:blue">ADDED</span>\] [`ResumableHandle.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.2.6/src/main/java/org/refcodes/component/ResumableHandle.java) (see Javadoc at [`ResumableHandle.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/3.2.6/org.refcodes.component/org/refcodes/component/ResumableHandle.html))
* \[<span style="color:blue">ADDED</span>\] [`Resumable.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.2.6/src/main/java/org/refcodes/component/Resumable.java) (see Javadoc at [`Resumable.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/3.2.6/org.refcodes.component/org/refcodes/component/Resumable.html))
* \[<span style="color:blue">ADDED</span>\] [`ResumeException.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.2.6/src/main/java/org/refcodes/component/ResumeException.java) (see Javadoc at [`ResumeException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/3.2.6/org.refcodes.component/org/refcodes/component/ResumeException.html))
* \[<span style="color:blue">ADDED</span>\] [`RunningAccessor.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.2.6/src/main/java/org/refcodes/component/RunningAccessor.java) (see Javadoc at [`RunningAccessor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/3.2.6/org.refcodes.component/org/refcodes/component/RunningAccessor.html))
* \[<span style="color:blue">ADDED</span>\] [`RunningHandle.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.2.6/src/main/java/org/refcodes/component/RunningHandle.java) (see Javadoc at [`RunningHandle.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/3.2.6/org.refcodes.component/org/refcodes/component/RunningHandle.html))
* \[<span style="color:blue">ADDED</span>\] [`StartableHandle.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.2.6/src/main/java/org/refcodes/component/StartableHandle.java) (see Javadoc at [`StartableHandle.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/3.2.6/org.refcodes.component/org/refcodes/component/StartableHandle.html))
* \[<span style="color:blue">ADDED</span>\] [`Startable.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.2.6/src/main/java/org/refcodes/component/Startable.java) (see Javadoc at [`Startable.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/3.2.6/org.refcodes.component/org/refcodes/component/Startable.html))
* \[<span style="color:blue">ADDED</span>\] [`StartException.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.2.6/src/main/java/org/refcodes/component/StartException.java) (see Javadoc at [`StartException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/3.2.6/org.refcodes.component/org/refcodes/component/StartException.html))
* \[<span style="color:blue">ADDED</span>\] [`StopException.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.2.6/src/main/java/org/refcodes/component/StopException.java) (see Javadoc at [`StopException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/3.2.6/org.refcodes.component/org/refcodes/component/StopException.html))
* \[<span style="color:blue">ADDED</span>\] [`StoppableHandle.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.2.6/src/main/java/org/refcodes/component/StoppableHandle.java) (see Javadoc at [`StoppableHandle.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/3.2.6/org.refcodes.component/org/refcodes/component/StoppableHandle.html))
* \[<span style="color:blue">ADDED</span>\] [`Stoppable.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.2.6/src/main/java/org/refcodes/component/Stoppable.java) (see Javadoc at [`Stoppable.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/3.2.6/org.refcodes.component/org/refcodes/component/Stoppable.html))
* \[<span style="color:blue">ADDED</span>\] [`UnknownHandleRuntimeException.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.2.6/src/main/java/org/refcodes/component/UnknownHandleRuntimeException.java) (see Javadoc at [`UnknownHandleRuntimeException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/3.2.6/org.refcodes.component/org/refcodes/component/UnknownHandleRuntimeException.html))
* \[<span style="color:blue">ADDED</span>\] [`UnsupportedHandleOperationRuntimeException.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.2.6/src/main/java/org/refcodes/component/UnsupportedHandleOperationRuntimeException.java) (see Javadoc at [`UnsupportedHandleOperationRuntimeException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/3.2.6/org.refcodes.component/org/refcodes/component/UnsupportedHandleOperationRuntimeException.html))
* \[<span style="color:blue">ADDED</span>\] [`ConfigurableLifecycleAutomatonTest.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.2.6/src/test/java/org/refcodes/component/ConfigurableLifecycleAutomatonTest.java) 
* \[<span style="color:blue">ADDED</span>\] [`ConnectionAutomatonTest.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.2.6/src/test/java/org/refcodes/component/ConnectionAutomatonTest.java) 
* \[<span style="color:blue">ADDED</span>\] [`LifecycleAutomatonTest.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.2.6/src/test/java/org/refcodes/component/LifecycleAutomatonTest.java) 
* \[<span style="color:blue">ADDED</span>\] [`LinkAutomatonTest.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.2.6/src/test/java/org/refcodes/component/LinkAutomatonTest.java) 
* \[<span style="color:blue">ADDED</span>\] [`log4j.xml`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.2.6/src/test/resources/log4j.xml) 
* \[<span style="color:red">DELETED</span>\] `ComponentException.java`
* \[<span style="color:red">DELETED</span>\] `InitializeException.java`
* \[<span style="color:red">DELETED</span>\] `StartException.java`
* \[<span style="color:red">DELETED</span>\] `StopException.java`
* \[<span style="color:red">DELETED</span>\] `Configurable.java`
* \[<span style="color:red">DELETED</span>\] `Decomposeable.java`
* \[<span style="color:red">DELETED</span>\] `Destroyable.java`
* \[<span style="color:red">DELETED</span>\] `Startable.java`
* \[<span style="color:green">MODIFIED</span>\] [`.gitignore`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.2.6/.gitignore) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.2.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`Component.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.2.6/src/main/java/org/refcodes/component/Component.java) (see Javadoc at [`Component.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/3.2.6/org.refcodes.component/org/refcodes/component/Component.html))
* \[<span style="color:green">MODIFIED</span>\] [`ConfigurableComponent.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-3.2.6/src/main/java/org/refcodes/component/ConfigurableComponent.java) (see Javadoc at [`ConfigurableComponent.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/3.2.6/org.refcodes.component/org/refcodes/component/ConfigurableComponent.html))

## Change list &lt;refcodes-data-ext&gt; (version 3.2.6)

* \[<span style="color:red">DELETED</span>\] `.gitignore`
* \[<span style="color:red">DELETED</span>\] `pom.xml`
* \[<span style="color:red">DELETED</span>\] `README.md`
* \[<span style="color:red">DELETED</span>\] `BoulderDashAnimation.class`
* \[<span style="color:red">DELETED</span>\] `BoulderDashAnimationInputStreamsFactory.class`
* \[<span style="color:red">DELETED</span>\] `BoulderDashAnimationUrlsFactory.class`
* \[<span style="color:red">DELETED</span>\] `BoulderDashCaveMap.class`
* \[<span style="color:red">DELETED</span>\] `BoulderDashCaveMapFactory.class`
* \[<span style="color:red">DELETED</span>\] `BoulderDashPixmap.class`
* \[<span style="color:red">DELETED</span>\] `BoulderDashPixmapInputStreamFactory.class`
* \[<span style="color:red">DELETED</span>\] `BoulderDashPixmapUrlFactory.class`
* \[<span style="color:red">DELETED</span>\] `BoulderDashStatus.class`
* \[<span style="color:red">DELETED</span>\] `package-info.class`
* \[<span style="color:red">DELETED</span>\] `BDCFF_0000_boulder.png`
* \[<span style="color:red">DELETED</span>\] `BDCFF_0001_diamond_01.png`
* \[<span style="color:red">DELETED</span>\] `BDCFF_0001_diamond_02.png`
* \[<span style="color:red">DELETED</span>\] `BDCFF_0001_diamond_03.png`
* \[<span style="color:red">DELETED</span>\] `BDCFF_0001_diamond_04.png`
* \[<span style="color:red">DELETED</span>\] `BDCFF_0001_diamond_05.png`
* \[<span style="color:red">DELETED</span>\] `BDCFF_0001_diamond_06.png`
* \[<span style="color:red">DELETED</span>\] `BDCFF_0001_diamond_07.png`
* \[<span style="color:red">DELETED</span>\] `BDCFF_0001_diamond_08.png`
* \[<span style="color:red">DELETED</span>\] `BDCFF_0001_diamond_birth_01.png`
* \[<span style="color:red">DELETED</span>\] `BDCFF_0001_diamond_birth_02.png`
* \[<span style="color:red">DELETED</span>\] `BDCFF_0001_diamond_birth_03.png`
* \[<span style="color:red">DELETED</span>\] `BDCFF_0001_diamond_birth_04.png`
* \[<span style="color:red">DELETED</span>\] `BDCFF_0001_diamond_birth_05.png`
* \[<span style="color:red">DELETED</span>\] `BDCFF_0002_magic_wall_01.png`
* \[<span style="color:red">DELETED</span>\] `BDCFF_0002_magic_wall_02.png`
* \[<span style="color:red">DELETED</span>\] `BDCFF_0002_magic_wall_03.png`
* \[<span style="color:red">DELETED</span>\] `BDCFF_0002_magic_wall_04.png`
* \[<span style="color:red">DELETED</span>\] `BDCFF_0002_magic_wall_expired.png`
* \[<span style="color:red">DELETED</span>\] `BDCFF_0003_brick_wall.png`
* \[<span style="color:red">DELETED</span>\] `BDCFF_0004_steel_wall_open.png`
* \[<span style="color:red">DELETED</span>\] `BDCFF_0004_steel_wall.png`
* \[<span style="color:red">DELETED</span>\] `BDCFF_0005_expanding_wall.png`
* \[<span style="color:red">DELETED</span>\] `BDCFF_0006_rockford_birth_01.png`
* \[<span style="color:red">DELETED</span>\] `BDCFF_0006_rockford_birth_02.png`
* \[<span style="color:red">DELETED</span>\] `BDCFF_0006_rockford_birth_03.png`
* \[<span style="color:red">DELETED</span>\] `BDCFF_0006_rockford_facing_forward_blinking_01.png`
* \[<span style="color:red">DELETED</span>\] `BDCFF_0006_rockford_facing_forward_blinking_02.png`
* \[<span style="color:red">DELETED</span>\] `BDCFF_0006_rockford_facing_forward_blinking_03.png`
* \[<span style="color:red">DELETED</span>\] `BDCFF_0006_rockford_facing_forward_blinking_04.png`
* \[<span style="color:red">DELETED</span>\] `BDCFF_0006_rockford_facing_forward_blinking_05.png`
* \[<span style="color:red">DELETED</span>\] `BDCFF_0006_rockford_facing_forward_blinking_06.png`
* \[<span style="color:red">DELETED</span>\] `BDCFF_0006_rockford_facing_forward_blinking_07.png`
* \[<span style="color:red">DELETED</span>\] `BDCFF_0006_rockford_facing_forward_blinking_08.png`
* \[<span style="color:red">DELETED</span>\] `BDCFF_0006_rockford_facing_forward_blinking_tapping_foot_01.png`
* \[<span style="color:red">DELETED</span>\] `BDCFF_0006_rockford_facing_forward_blinking_tapping_foot_02.png`
* \[<span style="color:red">DELETED</span>\] `BDCFF_0006_rockford_facing_forward_blinking_tapping_foot_03.png`
* \[<span style="color:red">DELETED</span>\] `BDCFF_0006_rockford_facing_forward_blinking_tapping_foot_04.png`
* \[<span style="color:red">DELETED</span>\] `BDCFF_0006_rockford_facing_forward_blinking_tapping_foot_05.png`
* \[<span style="color:red">DELETED</span>\] `BDCFF_0006_rockford_facing_forward_blinking_tapping_foot_06.png`
* \[<span style="color:red">DELETED</span>\] `BDCFF_0006_rockford_facing_forward_blinking_tapping_foot_07.png`
* \[<span style="color:red">DELETED</span>\] `BDCFF_0006_rockford_facing_forward_blinking_tapping_foot_08.png`
* \[<span style="color:red">DELETED</span>\] `BDCFF_0006_rockford_facing_forward.png`
* \[<span style="color:red">DELETED</span>\] `BDCFF_0006_rockford_facing_forward_tapping_foot_01.png`
* \[<span style="color:red">DELETED</span>\] `BDCFF_0006_rockford_facing_forward_tapping_foot_02.png`
* \[<span style="color:red">DELETED</span>\] `BDCFF_0006_rockford_facing_forward_tapping_foot_03.png`
* \[<span style="color:red">DELETED</span>\] `BDCFF_0006_rockford_facing_forward_tapping_foot_04.png`
* \[<span style="color:red">DELETED</span>\] `BDCFF_0006_rockford_facing_forward_tapping_foot_05.png`
* \[<span style="color:red">DELETED</span>\] `BDCFF_0006_rockford_facing_forward_tapping_foot_06.png`
* \[<span style="color:red">DELETED</span>\] `BDCFF_0006_rockford_facing_forward_tapping_foot_07.png`
* \[<span style="color:red">DELETED</span>\] `BDCFF_0006_rockford_facing_forward_tapping_foot_08.png`
* \[<span style="color:red">DELETED</span>\] `BDCFF_0006_rockford_facing_left_01.png`
* \[<span style="color:red">DELETED</span>\] `BDCFF_0006_rockford_facing_left_02.png`
* \[<span style="color:red">DELETED</span>\] `BDCFF_0006_rockford_facing_left_03.png`
* \[<span style="color:red">DELETED</span>\] `BDCFF_0006_rockford_facing_left_04.png`
* \[<span style="color:red">DELETED</span>\] `BDCFF_0006_rockford_facing_left_05.png`
* \[<span style="color:red">DELETED</span>\] `BDCFF_0006_rockford_facing_left_06.png`
* \[<span style="color:red">DELETED</span>\] `BDCFF_0006_rockford_facing_left_07.png`
* \[<span style="color:red">DELETED</span>\] `BDCFF_0006_rockford_facing_left_08.png`
* \[<span style="color:red">DELETED</span>\] `BDCFF_0006_rockford_facing_right_01.png`
* \[<span style="color:red">DELETED</span>\] `BDCFF_0006_rockford_facing_right_02.png`
* \[<span style="color:red">DELETED</span>\] `BDCFF_0006_rockford_facing_right_03.png`
* \[<span style="color:red">DELETED</span>\] `BDCFF_0006_rockford_facing_right_04.png`
* \[<span style="color:red">DELETED</span>\] `BDCFF_0006_rockford_facing_right_05.png`
* \[<span style="color:red">DELETED</span>\] `BDCFF_0006_rockford_facing_right_06.png`
* \[<span style="color:red">DELETED</span>\] `BDCFF_0006_rockford_facing_right_07.png`
* \[<span style="color:red">DELETED</span>\] `BDCFF_0006_rockford_facing_right_08.png`
* \[<span style="color:red">DELETED</span>\] `BDCFF_0007_dirt.png`
* \[<span style="color:red">DELETED</span>\] `BDCFF_0008_firefly_01.png`
* \[<span style="color:red">DELETED</span>\] `BDCFF_0008_firefly_02.png`
* \[<span style="color:red">DELETED</span>\] `BDCFF_0008_firefly_03.png`
* \[<span style="color:red">DELETED</span>\] `BDCFF_0008_firefly_04.png`
* \[<span style="color:red">DELETED</span>\] `BDCFF_0008_firefly_05.png`
* \[<span style="color:red">DELETED</span>\] `BDCFF_0008_firefly_06.png`
* \[<span style="color:red">DELETED</span>\] `BDCFF_0008_firefly_07.png`
* \[<span style="color:red">DELETED</span>\] `BDCFF_0008_firefly_08.png`
* \[<span style="color:red">DELETED</span>\] `BDCFF_0009_butterfly_01.png`
* \[<span style="color:red">DELETED</span>\] `BDCFF_0009_butterfly_02.png`
* \[<span style="color:red">DELETED</span>\] `BDCFF_0009_butterfly_03.png`
* \[<span style="color:red">DELETED</span>\] `BDCFF_0009_butterfly_04.png`
* \[<span style="color:red">DELETED</span>\] `BDCFF_0009_butterfly_05.png`
* \[<span style="color:red">DELETED</span>\] `BDCFF_0009_butterfly_06.png`
* \[<span style="color:red">DELETED</span>\] `BDCFF_0009_butterfly_07.png`
* \[<span style="color:red">DELETED</span>\] `BDCFF_0009_butterfly_08.png`
* \[<span style="color:red">DELETED</span>\] `BDCFF_000A_amoeba_01.png`
* \[<span style="color:red">DELETED</span>\] `BDCFF_000A_amoeba_02.png`
* \[<span style="color:red">DELETED</span>\] `BDCFF_000A_amoeba_03.png`
* \[<span style="color:red">DELETED</span>\] `BDCFF_000A_amoeba_04.png`
* \[<span style="color:red">DELETED</span>\] `BDCFF_000A_amoeba_05.png`
* \[<span style="color:red">DELETED</span>\] `BDCFF_000A_amoeba_06.png`
* \[<span style="color:red">DELETED</span>\] `BDCFF_000A_amoeba_07.png`
* \[<span style="color:red">DELETED</span>\] `BDCFF_000A_amoeba_08.png`
* \[<span style="color:red">DELETED</span>\] `Cave_01_Intro.txt`
* \[<span style="color:red">DELETED</span>\] `Cave_02_Rooms.txt`
* \[<span style="color:red">DELETED</span>\] `Cave_03_Maze.txt`
* \[<span style="color:red">DELETED</span>\] `Cave_04_Butterflies.txt`
* \[<span style="color:red">DELETED</span>\] `Cave_05_Guards.txt`
* \[<span style="color:red">DELETED</span>\] `Cave_06_Firefly_dens.txt`
* \[<span style="color:red">DELETED</span>\] `Cave_07_Amoeba.txt`
* \[<span style="color:red">DELETED</span>\] `Cave_08_Enchanted_wall.txt`
* \[<span style="color:red">DELETED</span>\] `Cave_09_Greed.txt`
* \[<span style="color:red">DELETED</span>\] `Cave_10_Tracks.txt`
* \[<span style="color:red">DELETED</span>\] `Cave_11_Crowd.txt`
* \[<span style="color:red">DELETED</span>\] `Cave_12_Walls.txt`
* \[<span style="color:red">DELETED</span>\] `Cave_13_Apocalypse.txt`
* \[<span style="color:red">DELETED</span>\] `Cave_14_Zigzag.txt`
* \[<span style="color:red">DELETED</span>\] `Cave_15_Funnel.txt`
* \[<span style="color:red">DELETED</span>\] `Cave_16_Enchanted_boxes.txt`
* \[<span style="color:red">DELETED</span>\] `Cave_17_Intermission_1.txt`
* \[<span style="color:red">DELETED</span>\] `Cave_18_Intermission_2.txt`
* \[<span style="color:red">DELETED</span>\] `Cave_19_Intermission_3.txt`
* \[<span style="color:red">DELETED</span>\] `Cave_20_Intermission_4.txt`
* \[<span style="color:red">DELETED</span>\] `Cave_99_Testrun.txt`
* \[<span style="color:red">DELETED</span>\] `BoulderDashCaveMapTest.class`
* \[<span style="color:red">DELETED</span>\] `BoulderDashPixmapDataLocatorTest.class`
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-3.2.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-3.2.6/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-3.2.6/refcodes-data-ext-checkers/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-3.2.6/refcodes-data-ext-checkers/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-3.2.6/refcodes-data-ext-chess/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-3.2.6/refcodes-data-ext-chess/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-3.2.6/refcodes-data-ext-corporate/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-3.2.6/refcodes-data-ext-corporate/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-3.2.6/refcodes-data-ext-symbols/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-3.2.6/refcodes-data-ext-symbols/README.md) 

## Change list &lt;refcodes-graphical&gt; (version 3.2.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-graphical/src/refcodes-graphical-3.2.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-graphical/src/refcodes-graphical-3.2.6/README.md) 

## Change list &lt;refcodes-textual&gt; (version 3.2.6)

* \[<span style="color:blue">ADDED</span>\] [`MessageBuilder.java`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-3.2.6/src/main/java/org/refcodes/textual/MessageBuilder.java) (see Javadoc at [`MessageBuilder.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-textual/3.2.6/org.refcodes.textual/org/refcodes/textual/MessageBuilder.html))
* \[<span style="color:red">DELETED</span>\] `TextUtility.java`
* \[<span style="color:red">DELETED</span>\] `TextUtilityTest.java`
* \[<span style="color:green">MODIFIED</span>\] [`.gitignore`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-3.2.6/.gitignore) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-3.2.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`log4j.xml`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-3.2.6/src/test/resources/log4j.xml) 

## Change list &lt;refcodes-criteria&gt; (version 3.2.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-3.2.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-3.2.6/README.md) 

## Change list &lt;refcodes-io&gt; (version 3.2.6)

* \[<span style="color:blue">ADDED</span>\] [`InputStreamTap.java`](https://bitbucket.org/refcodes/refcodes-io/src/refcodes-io-3.2.6/src/main/java/org/refcodes/io/InputStreamTap.java) (see Javadoc at [`InputStreamTap.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-io/3.2.6/org.refcodes.io/org/refcodes/io/InputStreamTap.html))
* \[<span style="color:blue">ADDED</span>\] [`OutputStreamComposite.java`](https://bitbucket.org/refcodes/refcodes-io/src/refcodes-io-3.2.6/src/main/java/org/refcodes/io/OutputStreamComposite.java) (see Javadoc at [`OutputStreamComposite.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-io/3.2.6/org.refcodes.io/org/refcodes/io/OutputStreamComposite.html))
* \[<span style="color:blue">ADDED</span>\] [`InputStreamTapTest.java`](https://bitbucket.org/refcodes/refcodes-io/src/refcodes-io-3.2.6/src/test/java/org/refcodes/io/InputStreamTapTest.java) 
* \[<span style="color:blue">ADDED</span>\] [`OutputStreamCompositeTest.java`](https://bitbucket.org/refcodes/refcodes-io/src/refcodes-io-3.2.6/src/test/java/org/refcodes/io/OutputStreamCompositeTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-io/src/refcodes-io-3.2.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-io/src/refcodes-io-3.2.6/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`HexOutputStream.java`](https://bitbucket.org/refcodes/refcodes-io/src/refcodes-io-3.2.6/src/main/java/org/refcodes/io/HexOutputStream.java) (see Javadoc at [`HexOutputStream.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-io/3.2.6/org.refcodes.io/org/refcodes/io/HexOutputStream.html))
* \[<span style="color:green">MODIFIED</span>\] [`FileUtilityTest.java`](https://bitbucket.org/refcodes/refcodes-io/src/refcodes-io-3.2.6/src/test/java/org/refcodes/io/FileUtilityTest.java) 

## Change list &lt;refcodes-tabular&gt; (version 3.2.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-tabular/src/refcodes-tabular-3.2.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-tabular/src/refcodes-tabular-3.2.6/README.md) 

## Change list &lt;refcodes-observer&gt; (version 3.2.6)

* \[<span style="color:green">MODIFIED</span>\] [`.gitignore`](https://bitbucket.org/refcodes/refcodes-observer/src/refcodes-observer-3.2.6/.gitignore) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-observer/src/refcodes-observer-3.2.6/pom.xml) 

## Change list &lt;refcodes-command&gt; (version 3.2.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-command/src/refcodes-command-3.2.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-command/src/refcodes-command-3.2.6/README.md) 

## Change list &lt;refcodes-cli&gt; (version 3.2.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.2.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.2.6/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`ArgsParser.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.2.6/src/main/java/org/refcodes/cli/ArgsParser.java) (see Javadoc at [`ArgsParser.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/3.2.6/org.refcodes.cli/org/refcodes/cli/ArgsParser.html))
* \[<span style="color:green">MODIFIED</span>\] [`CliContext.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.2.6/src/main/java/org/refcodes/cli/CliContext.java) (see Javadoc at [`CliContext.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/3.2.6/org.refcodes.cli/org/refcodes/cli/CliContext.html))
* \[<span style="color:green">MODIFIED</span>\] [`StackOverflowTest.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-3.2.6/src/test/java/org/refcodes/cli/StackOverflowTest.java) 

## Change list &lt;refcodes-audio&gt; (version 3.2.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-audio/src/refcodes-audio-3.2.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-audio/src/refcodes-audio-3.2.6/README.md) 

## Change list &lt;refcodes-codec&gt; (version 3.2.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-3.2.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-3.2.6/README.md) 

## Change list &lt;refcodes-component-ext&gt; (version 3.2.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-3.2.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-3.2.6/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-3.2.6/refcodes-component-ext-observer/pom.xml) 

## Change list &lt;refcodes-properties&gt; (version 3.2.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-3.2.6/pom.xml) 

## Change list &lt;refcodes-security&gt; (version 3.2.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security/src/refcodes-security-3.2.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security/src/refcodes-security-3.2.6/README.md) 

## Change list &lt;refcodes-security-alt&gt; (version 3.2.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-3.2.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-3.2.6/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-3.2.6/refcodes-security-alt-chaos/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-3.2.6/refcodes-security-alt-chaos/README.md) 

## Change list &lt;refcodes-security-ext&gt; (version 3.2.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-3.2.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-3.2.6/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-3.2.6/refcodes-security-ext-chaos/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-3.2.6/refcodes-security-ext-chaos/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-3.2.6/refcodes-security-ext-spring/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-3.2.6/refcodes-security-ext-spring/README.md) 

## Change list &lt;refcodes-properties-ext&gt; (version 3.2.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.2.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.2.6/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.2.6/refcodes-properties-ext-application/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.2.6/refcodes-properties-ext-application/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.2.6/refcodes-properties-ext-cli/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.2.6/refcodes-properties-ext-cli/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.2.6/refcodes-properties-ext-obfuscation/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.2.6/refcodes-properties-ext-obfuscation/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.2.6/refcodes-properties-ext-observer/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-3.2.6/refcodes-properties-ext-observer/README.md) 

## Change list &lt;refcodes-logger&gt; (version 3.2.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger/src/refcodes-logger-3.2.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger/src/refcodes-logger-3.2.6/README.md) 

## Change list &lt;refcodes-logger-alt&gt; (version 3.2.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.2.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.2.6/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.2.6/refcodes-logger-alt-async/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.2.6/refcodes-logger-alt-async/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.2.6/refcodes-logger-alt-console/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.2.6/refcodes-logger-alt-console/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`AbstractConsoleLogger.java`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.2.6/refcodes-logger-alt-console/src/main/java/org/refcodes/logger/alt/console/AbstractConsoleLogger.java) (see Javadoc at [`AbstractConsoleLogger.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-logger-alt-console/3.2.6/org.refcodes.logger.alt.console/org/refcodes/logger/alt/console/AbstractConsoleLogger.html))
* \[<span style="color:green">MODIFIED</span>\] [`ConsoleLogger.java`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.2.6/refcodes-logger-alt-console/src/main/java/org/refcodes/logger/alt/console/ConsoleLogger.java) (see Javadoc at [`ConsoleLogger.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-logger-alt-console/3.2.6/org.refcodes.logger.alt.console/org/refcodes/logger/alt/console/ConsoleLogger.html))
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.2.6/refcodes-logger-alt-io/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.2.6/refcodes-logger-alt-io/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.2.6/refcodes-logger-alt-simpledb/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.2.6/refcodes-logger-alt-simpledb/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.2.6/refcodes-logger-alt-slf4j-legacy/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.2.6/refcodes-logger-alt-slf4j/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-3.2.6/refcodes-logger-alt-spring/pom.xml) 

## Change list &lt;refcodes-logger-ext&gt; (version 3.2.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-3.2.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-3.2.6/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-3.2.6/refcodes-logger-ext-slf4j-legacy/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-3.2.6/refcodes-logger-ext-slf4j-legacy/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-3.2.6/refcodes-logger-ext-slf4j/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-3.2.6/refcodes-logger-ext-slf4j/README.md) 

## Change list &lt;refcodes-graphical-ext&gt; (version 3.2.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-3.2.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-3.2.6/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-3.2.6/refcodes-graphical-ext-javafx/pom.xml) 

## Change list &lt;refcodes-checkerboard&gt; (version 3.2.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-3.2.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-3.2.6/README.md) 

## Change list &lt;refcodes-checkerboard-alt&gt; (version 3.2.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-alt/src/refcodes-checkerboard-alt-3.2.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-alt/src/refcodes-checkerboard-alt-3.2.6/refcodes-checkerboard-alt-javafx/pom.xml) 

## Change list &lt;refcodes-net&gt; (version 3.2.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-3.2.6/pom.xml) 

## Change list &lt;refcodes-web&gt; (version 3.2.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-web/src/refcodes-web-3.2.6/pom.xml) 

## Change list &lt;refcodes-rest&gt; (version 3.2.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-3.2.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-3.2.6/README.md) 

## Change list &lt;refcodes-hal&gt; (version 3.2.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-3.2.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-3.2.6/README.md) 

## Change list &lt;refcodes-eventbus&gt; (version 3.2.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-eventbus/src/refcodes-eventbus-3.2.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-eventbus/src/refcodes-eventbus-3.2.6/README.md) 

## Change list &lt;refcodes-eventbus-ext&gt; (version 3.2.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-3.2.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-3.2.6/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-3.2.6/refcodes-eventbus-ext-application/pom.xml) 

## Change list &lt;refcodes-decoupling&gt; (version 3.2.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-decoupling/src/refcodes-decoupling-3.2.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-decoupling/src/refcodes-decoupling-3.2.6/README.md) 

## Change list &lt;refcodes-decoupling-ext&gt; (version 3.2.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-decoupling-ext/src/refcodes-decoupling-ext-3.2.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-decoupling-ext/src/refcodes-decoupling-ext-3.2.6/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-decoupling-ext/src/refcodes-decoupling-ext-3.2.6/refcodes-decoupling-ext-application/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-decoupling-ext/src/refcodes-decoupling-ext-3.2.6/refcodes-decoupling-ext-application/README.md) 

## Change list &lt;refcodes-filesystem&gt; (version 3.2.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-filesystem/src/refcodes-filesystem-3.2.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-filesystem/src/refcodes-filesystem-3.2.6/README.md) 

## Change list &lt;refcodes-forwardsecrecy&gt; (version 3.2.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy/src/refcodes-forwardsecrecy-3.2.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy/src/refcodes-forwardsecrecy-3.2.6/README.md) 

## Change list &lt;refcodes-forwardsecrecy-alt&gt; (version 3.2.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-3.2.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-3.2.6/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-3.2.6/refcodes-forwardsecrecy-alt-filesystem/pom.xml) 

## Change list &lt;refcodes-io-ext&gt; (version 3.2.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-3.2.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-3.2.6/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-3.2.6/refcodes-io-ext-observer/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-3.2.6/refcodes-io-ext-observer/README.md) 

## Change list &lt;refcodes-jobbus&gt; (version 3.2.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-jobbus/src/refcodes-jobbus-3.2.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-jobbus/src/refcodes-jobbus-3.2.6/README.md) 

## Change list &lt;refcodes-rest-ext&gt; (version 3.2.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-3.2.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-3.2.6/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-3.2.6/refcodes-rest-ext-eureka/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-3.2.6/refcodes-rest-ext-eureka/README.md) 

## Change list &lt;refcodes-remoting&gt; (version 3.2.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-remoting/src/refcodes-remoting-3.2.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-remoting/src/refcodes-remoting-3.2.6/README.md) 

## Change list &lt;refcodes-remoting-ext&gt; (version 3.2.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-3.2.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-3.2.6/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-3.2.6/refcodes-remoting-ext-observer/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-3.2.6/refcodes-remoting-ext-observer/README.md) 

## Change list &lt;refcodes-serial&gt; (version 3.2.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial/src/refcodes-serial-3.2.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-serial/src/refcodes-serial-3.2.6/README.md) 

## Change list &lt;refcodes-serial-alt&gt; (version 3.2.6)

* \[<span style="color:red">DELETED</span>\] `pom.xml`
* \[<span style="color:red">DELETED</span>\] `README.md`
* \[<span style="color:red">DELETED</span>\] `module-info.java`
* \[<span style="color:red">DELETED</span>\] `SocketPortHub.java`
* \[<span style="color:red">DELETED</span>\] `SocketPort.java`
* \[<span style="color:red">DELETED</span>\] `SocketPortMetricsBuilder.java`
* \[<span style="color:red">DELETED</span>\] `SocketPortMetrics.java`
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial-alt/src/refcodes-serial-alt-3.2.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-serial-alt/src/refcodes-serial-alt-3.2.6/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial-alt/src/refcodes-serial-alt-3.2.6/refcodes-serial-alt-tty/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-serial-alt/src/refcodes-serial-alt-3.2.6/refcodes-serial-alt-tty/README.md) 

## Change list &lt;refcodes-serial-ext&gt; (version 3.2.6)

* \[<span style="color:red">DELETED</span>\] `pom.xml`
* \[<span style="color:red">DELETED</span>\] `README.md`
* \[<span style="color:red">DELETED</span>\] `module-info.java`
* \[<span style="color:red">DELETED</span>\] `CipherTransmission.java`
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-3.2.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-3.2.6/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-3.2.6/refcodes-serial-ext-handshake/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-3.2.6/refcodes-serial-ext-handshake/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-3.2.6/refcodes-serial-ext-observer/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-3.2.6/refcodes-serial-ext-observer/README.md) 

## Change list &lt;refcodes-p2p&gt; (version 3.2.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-p2p/src/refcodes-p2p-3.2.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-p2p/src/refcodes-p2p-3.2.6/README.md) 

## Change list &lt;refcodes-p2p-alt&gt; (version 3.2.6)

* \[<span style="color:red">DELETED</span>\] `pom.xml`
* \[<span style="color:red">DELETED</span>\] `module-info.java`
* \[<span style="color:red">DELETED</span>\] `RestPeer.java`
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-p2p-alt/src/refcodes-p2p-alt-3.2.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-p2p-alt/src/refcodes-p2p-alt-3.2.6/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-p2p-alt/src/refcodes-p2p-alt-3.2.6/refcodes-p2p-alt-serial/pom.xml) 

## Change list &lt;refcodes-p2p-ext&gt; (version 3.2.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-p2p-ext/src/refcodes-p2p-ext-3.2.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-p2p-ext/src/refcodes-p2p-ext-3.2.6/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-p2p-ext/src/refcodes-p2p-ext-3.2.6/refcodes-p2p-ext-observer/pom.xml) 

## Change list &lt;refcodes-tabular-alt&gt; (version 3.2.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-tabular-alt/src/refcodes-tabular-alt-3.2.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-tabular-alt/src/refcodes-tabular-alt-3.2.6/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-tabular-alt/src/refcodes-tabular-alt-3.2.6/refcodes-tabular-alt-forwardsecrecy/pom.xml) 

## Change list &lt;refcodes-archetype&gt; (version 3.2.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype/src/refcodes-archetype-3.2.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype/src/refcodes-archetype-3.2.6/README.md) 

## Change list &lt;refcodes-archetype-alt&gt; (version 3.2.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.2.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.2.6/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.2.6/refcodes-archetype-alt-c2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.2.6/refcodes-archetype-alt-c2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.2.6/refcodes-archetype-alt-cli/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.2.6/refcodes-archetype-alt-cli/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.2.6/refcodes-archetype-alt-csv/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.2.6/refcodes-archetype-alt-csv/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.2.6/refcodes-archetype-alt-decoupling/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.2.6/refcodes-archetype-alt-decoupling/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.2.6/refcodes-archetype-alt-eventbus/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.2.6/refcodes-archetype-alt-eventbus/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.2.6/refcodes-archetype-alt-filter/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.2.6/refcodes-archetype-alt-filter/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.2.6/refcodes-archetype-alt-rest/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.2.6/refcodes-archetype-alt-rest/README.md) 
