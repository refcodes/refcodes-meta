# Change list for REFCODES.ORG artifacts' version 1.2.6

> This change list has been auto-generated on `triton` by `steiner` with with `changelist-all.sh` on the 2018-05-05 at 17:55:11.

## Change list &lt;refcodes-licensing&gt; (version 1.2.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-licensing/src/refcodes-licensing-1.2.6/pom.xml) 

## Change list &lt;refcodes-parent&gt; (version 1.2.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-parent/src/refcodes-parent-1.2.6/pom.xml) 

## Change list &lt;refcodes-time&gt; (version 1.2.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-time/src/refcodes-time-1.2.6/pom.xml) 

## Change list &lt;refcodes-mixin&gt; (version 1.2.6)

* \[<span style="color:red">DELETED</span>\] `ThreadMode.java`
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-mixin/src/refcodes-mixin-1.2.6/pom.xml) 

## Change list &lt;refcodes-data&gt; (version 1.2.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data/src/refcodes-data-1.2.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`EnvironmentProperty.java`](https://bitbucket.org/refcodes/refcodes-data/src/refcodes-data-1.2.6/src/main/java/org/refcodes/data/EnvironmentProperty.java) (see Javadoc at [`EnvironmentProperty.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-data/1.2.6/org/refcodes/data/EnvironmentProperty.html))

## Change list &lt;refcodes-exception&gt; (version 1.2.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-exception/src/refcodes-exception-1.2.6/pom.xml) 

## Change list &lt;refcodes-factory&gt; (version 1.2.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-factory/src/refcodes-factory-1.2.6/pom.xml) 

## Change list &lt;refcodes-factory-alt&gt; (version 1.2.6)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-factory-alt/src/refcodes-factory-alt-1.2.6/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-factory-alt/src/refcodes-factory-alt-1.2.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-factory-alt/src/refcodes-factory-alt-1.2.6/refcodes-factory-alt-spring/pom.xml) 

## Change list &lt;refcodes-controlflow&gt; (version 1.2.6)

* \[<span style="color:blue">ADDED</span>\] [`ThreadMode.java`](https://bitbucket.org/refcodes/refcodes-controlflow/src/refcodes-controlflow-1.2.6/src/main/java/org/refcodes/controlflow/ThreadMode.java) (see Javadoc at [`ThreadMode.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-controlflow/1.2.6/org/refcodes/controlflow/ThreadMode.html))
* \[<span style="color:blue">ADDED</span>\] [`ThreadingModel.java`](https://bitbucket.org/refcodes/refcodes-controlflow/src/refcodes-controlflow-1.2.6/src/main/java/org/refcodes/controlflow/ThreadingModel.java) (see Javadoc at [`ThreadingModel.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-controlflow/1.2.6/org/refcodes/controlflow/ThreadingModel.html))
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-controlflow/src/refcodes-controlflow-1.2.6/pom.xml) 

## Change list &lt;refcodes-numerical&gt; (version 1.2.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-numerical/src/refcodes-numerical-1.2.6/pom.xml) 

## Change list &lt;refcodes-generator&gt; (version 1.2.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-generator/src/refcodes-generator-1.2.6/pom.xml) 

## Change list &lt;refcodes-structure&gt; (version 1.2.6)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-1.2.6/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-1.2.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`Keys.java`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-1.2.6/src/main/java/org/refcodes/structure/Keys.java) (see Javadoc at [`Keys.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-structure/1.2.6/org/refcodes/structure/Keys.html))

## Change list &lt;refcodes-runtime&gt; (version 1.2.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-1.2.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`SystemUtility.java`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-1.2.6/src/main/java/org/refcodes/runtime/SystemUtility.java) (see Javadoc at [`SystemUtility.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-runtime/1.2.6/org/refcodes/runtime/SystemUtility.html))
* \[<span style="color:green">MODIFIED</span>\] [`RuntimeUtilityTest.java`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-1.2.6/src/test/java/org/refcodes/runtime/RuntimeUtilityTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`SystemUtilityTest.java`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-1.2.6/src/test/java/org/refcodes/runtime/SystemUtilityTest.java) 

## Change list &lt;refcodes-component&gt; (version 1.2.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-1.2.6/pom.xml) 

## Change list &lt;refcodes-data-ext&gt; (version 1.2.6)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.2.6/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.2.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.2.6/refcodes-data-ext-boulderdash/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.2.6/refcodes-data-ext-checkers/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.2.6/refcodes-data-ext-checkers/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.2.6/refcodes-data-ext-chess/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.2.6/refcodes-data-ext-chess/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.2.6/refcodes-data-ext-corporate/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.2.6/refcodes-data-ext-symbols/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.2.6/refcodes-data-ext-symbols/pom.xml) 

## Change list &lt;refcodes-graphical&gt; (version 1.2.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-graphical/src/refcodes-graphical-1.2.6/pom.xml) 

## Change list &lt;refcodes-textual&gt; (version 1.2.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-1.2.6/pom.xml) 

## Change list &lt;refcodes-criteria&gt; (version 1.2.6)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-1.2.6/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-1.2.6/pom.xml) 

## Change list &lt;refcodes-tabular&gt; (version 1.2.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-tabular/src/refcodes-tabular-1.2.6/pom.xml) 

## Change list &lt;refcodes-logger&gt; (version 1.2.6)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger/src/refcodes-logger-1.2.6/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger/src/refcodes-logger-1.2.6/pom.xml) 

## Change list &lt;refcodes-logger-alt&gt; (version 1.2.6)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.2.6/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.2.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.2.6/refcodes-logger-alt-async/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.2.6/refcodes-logger-alt-async/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.2.6/refcodes-logger-alt-console/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.2.6/refcodes-logger-alt-console/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`ConsoleLoggerImpl.java`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.2.6/refcodes-logger-alt-console/src/main/java/org/refcodes/logger/alt/console/ConsoleLoggerImpl.java) (see Javadoc at [`ConsoleLoggerImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-logger-alt-console/1.2.6/org/refcodes/logger/alt/console/ConsoleLoggerImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`FormattedLoggerImpl.java`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.2.6/refcodes-logger-alt-console/src/main/java/org/refcodes/logger/alt/console/FormattedLoggerImpl.java) (see Javadoc at [`FormattedLoggerImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-logger-alt-console/1.2.6/org/refcodes/logger/alt/console/FormattedLoggerImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.2.6/refcodes-logger-alt-io/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.2.6/refcodes-logger-alt-io/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.2.6/refcodes-logger-alt-simpledb/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.2.6/refcodes-logger-alt-slf4j/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.2.6/refcodes-logger-alt-spring/pom.xml) 

## Change list &lt;refcodes-graphical-ext&gt; (version 1.2.6)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-1.2.6/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-1.2.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-1.2.6/refcodes-graphical-ext-javafx/pom.xml) 

## Change list &lt;refcodes-matcher&gt; (version 1.2.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-matcher/src/refcodes-matcher-1.2.6/pom.xml) 

## Change list &lt;refcodes-observer&gt; (version 1.2.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-observer/src/refcodes-observer-1.2.6/pom.xml) 

## Change list &lt;refcodes-checkerboard&gt; (version 1.2.6)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-1.2.6/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-1.2.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`MooreNeighbourhood.java`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-1.2.6/src/main/java/org/refcodes/checkerboard/MooreNeighbourhood.java) (see Javadoc at [`MooreNeighbourhood.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-checkerboard/1.2.6/org/refcodes/checkerboard/MooreNeighbourhood.html))
* \[<span style="color:green">MODIFIED</span>\] [`Neighbourhood.java`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-1.2.6/src/main/java/org/refcodes/checkerboard/Neighbourhood.java) (see Javadoc at [`Neighbourhood.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-checkerboard/1.2.6/org/refcodes/checkerboard/Neighbourhood.html))
* \[<span style="color:green">MODIFIED</span>\] [`Rotation.java`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-1.2.6/src/main/java/org/refcodes/checkerboard/Rotation.java) (see Javadoc at [`Rotation.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-checkerboard/1.2.6/org/refcodes/checkerboard/Rotation.html))
* \[<span style="color:green">MODIFIED</span>\] [`VonNeumannNeighbourhood.java`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-1.2.6/src/main/java/org/refcodes/checkerboard/VonNeumannNeighbourhood.java) (see Javadoc at [`VonNeumannNeighbourhood.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-checkerboard/1.2.6/org/refcodes/checkerboard/VonNeumannNeighbourhood.html))

## Change list &lt;refcodes-checkerboard-alt&gt; (version 1.2.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-alt/src/refcodes-checkerboard-alt-1.2.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-alt/src/refcodes-checkerboard-alt-1.2.6/refcodes-checkerboard-alt-javafx/pom.xml) 

## Change list &lt;refcodes-checkerboard-ext&gt; (version 1.2.6)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-checkerboard-ext/src/refcodes-checkerboard-ext-1.2.6/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-ext/src/refcodes-checkerboard-ext-1.2.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-ext/src/refcodes-checkerboard-ext-1.2.6/refcodes-checkerboard-ext-javafx-boulderdash/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-ext/src/refcodes-checkerboard-ext-1.2.6/refcodes-checkerboard-ext-javafx-chess/pom.xml) 

## Change list &lt;refcodes-command&gt; (version 1.2.6)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-command/src/refcodes-command-1.2.6/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-command/src/refcodes-command-1.2.6/pom.xml) 

## Change list &lt;refcodes-cli&gt; (version 1.2.6)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-1.2.6/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-1.2.6/pom.xml) 

## Change list &lt;refcodes-boulderdash&gt; (version 1.2.6)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-boulderdash/src/refcodes-boulderdash-1.2.6/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-boulderdash/src/refcodes-boulderdash-1.2.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`BoulderDashPlayerFactoryImpl.java`](https://bitbucket.org/refcodes/refcodes-boulderdash/src/refcodes-boulderdash-1.2.6/src/main/java/org/refcodes/boulderdash/BoulderDashPlayerFactoryImpl.java) (see Javadoc at [`BoulderDashPlayerFactoryImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-boulderdash/1.2.6/org/refcodes/boulderdash/BoulderDashPlayerFactoryImpl.html))

## Change list &lt;refcodes-io&gt; (version 1.2.6)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-io/src/refcodes-io-1.2.6/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-io/src/refcodes-io-1.2.6/pom.xml) 

## Change list &lt;refcodes-codec&gt; (version 1.2.6)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-1.2.6/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-1.2.6/pom.xml) 

## Change list &lt;refcodes-component-ext&gt; (version 1.2.6)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.2.6/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.2.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.2.6/refcodes-component-ext-observer/pom.xml) 

## Change list &lt;refcodes-properties&gt; (version 1.2.6)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.2.6/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.2.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`PropertiesSugar.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.2.6/src/main/java/org/refcodes/configuration/PropertiesSugar.java) (see Javadoc at [`PropertiesSugar.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties/1.2.6/org/refcodes/configuration/PropertiesSugar.html))
* \[<span style="color:green">MODIFIED</span>\] [`ScheduledResourcePropertiesBuilderDecorator.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.2.6/src/main/java/org/refcodes/configuration/ScheduledResourcePropertiesBuilderDecorator.java) (see Javadoc at [`ScheduledResourcePropertiesBuilderDecorator.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties/1.2.6/org/refcodes/configuration/ScheduledResourcePropertiesBuilderDecorator.html))
* \[<span style="color:green">MODIFIED</span>\] [`ScheduledResourcePropertiesDecorator.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.2.6/src/main/java/org/refcodes/configuration/ScheduledResourcePropertiesDecorator.java) (see Javadoc at [`ScheduledResourcePropertiesDecorator.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties/1.2.6/org/refcodes/configuration/ScheduledResourcePropertiesDecorator.html))
* \[<span style="color:green">MODIFIED</span>\] [`package-info.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.2.6/src/main/java/org/refcodes/configuration/package-info.java) 

## Change list &lt;refcodes-properties-ext&gt; (version 1.2.6)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.2.6/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.2.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.2.6/refcodes-properties-ext-cli/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.2.6/refcodes-properties-ext-cli/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.2.6/refcodes-properties-ext-observer/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.2.6/refcodes-properties-ext-observer/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.2.6/refcodes-properties-ext-runtime/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.2.6/refcodes-properties-ext-runtime/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`RuntimeProperties.java`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.2.6/refcodes-properties-ext-runtime/src/main/java/org/refcodes/configuration/ext/runtime/RuntimeProperties.java) (see Javadoc at [`RuntimeProperties.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties-ext-runtime/1.2.6/org/refcodes/configuration/ext/runtime/RuntimeProperties.html))
* \[<span style="color:green">MODIFIED</span>\] [`RuntimePropertiesImpl.java`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.2.6/refcodes-properties-ext-runtime/src/main/java/org/refcodes/configuration/ext/runtime/RuntimePropertiesImpl.java) (see Javadoc at [`RuntimePropertiesImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties-ext-runtime/1.2.6/org/refcodes/configuration/ext/runtime/RuntimePropertiesImpl.html))

## Change list &lt;refcodes-security&gt; (version 1.2.6)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security/src/refcodes-security-1.2.6/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security/src/refcodes-security-1.2.6/pom.xml) 

## Change list &lt;refcodes-net&gt; (version 1.2.6)

* \[<span style="color:blue">ADDED</span>\] [`UrlBuilderImpl.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.2.6/src/main/java/org/refcodes/net/UrlBuilderImpl.java) (see Javadoc at [`UrlBuilderImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/1.2.6/org/refcodes/net/UrlBuilderImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.2.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`ContentTypeImpl.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.2.6/src/main/java/org/refcodes/net/ContentTypeImpl.java) (see Javadoc at [`ContentTypeImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/1.2.6/org/refcodes/net/ContentTypeImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`HttpBodyMap.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.2.6/src/main/java/org/refcodes/net/HttpBodyMap.java) (see Javadoc at [`HttpBodyMap.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/1.2.6/org/refcodes/net/HttpBodyMap.html))
* \[<span style="color:green">MODIFIED</span>\] [`HttpStatusCode.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.2.6/src/main/java/org/refcodes/net/HttpStatusCode.java) (see Javadoc at [`HttpStatusCode.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/1.2.6/org/refcodes/net/HttpStatusCode.html))
* \[<span style="color:green">MODIFIED</span>\] [`IpAddress.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.2.6/src/main/java/org/refcodes/net/IpAddress.java) (see Javadoc at [`IpAddress.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/1.2.6/org/refcodes/net/IpAddress.html))
* \[<span style="color:green">MODIFIED</span>\] [`UnsupportedMediaTypeException.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.2.6/src/main/java/org/refcodes/net/UnsupportedMediaTypeException.java) (see Javadoc at [`UnsupportedMediaTypeException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/1.2.6/org/refcodes/net/UnsupportedMediaTypeException.html))
* \[<span style="color:green">MODIFIED</span>\] [`UrlImpl.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.2.6/src/main/java/org/refcodes/net/UrlImpl.java) (see Javadoc at [`UrlImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/1.2.6/org/refcodes/net/UrlImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`UrlSugar.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.2.6/src/main/java/org/refcodes/net/UrlSugar.java) (see Javadoc at [`UrlSugar.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/1.2.6/org/refcodes/net/UrlSugar.html))

## Change list &lt;refcodes-rest&gt; (version 1.2.6)

* \[<span style="color:blue">ADDED</span>\] [`HttpExceptionHandler.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.2.6/src/main/java/org/refcodes/rest/HttpExceptionHandler.java) (see Javadoc at [`HttpExceptionHandler.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/1.2.6/org/refcodes/rest/HttpExceptionHandler.html))
* \[<span style="color:blue">ADDED</span>\] [`HttpExceptionHandlerAccessor.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.2.6/src/main/java/org/refcodes/rest/HttpExceptionHandlerAccessor.java) (see Javadoc at [`HttpExceptionHandlerAccessor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/1.2.6/org/refcodes/rest/HttpExceptionHandlerAccessor.html))
* \[<span style="color:blue">ADDED</span>\] [`HttpExceptionHandling.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.2.6/src/main/java/org/refcodes/rest/HttpExceptionHandling.java) (see Javadoc at [`HttpExceptionHandling.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/1.2.6/org/refcodes/rest/HttpExceptionHandling.html))
* \[<span style="color:blue">ADDED</span>\] [`HttpExceptionHandlingAccessor.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.2.6/src/main/java/org/refcodes/rest/HttpExceptionHandlingAccessor.java) (see Javadoc at [`HttpExceptionHandlingAccessor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/1.2.6/org/refcodes/rest/HttpExceptionHandlingAccessor.html))
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.2.6/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.2.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`AbstractRestServer.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.2.6/src/main/java/org/refcodes/rest/AbstractRestServer.java) (see Javadoc at [`AbstractRestServer.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/1.2.6/org/refcodes/rest/AbstractRestServer.html))
* \[<span style="color:green">MODIFIED</span>\] [`HttpDiscovery.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.2.6/src/main/java/org/refcodes/rest/HttpDiscovery.java) (see Javadoc at [`HttpDiscovery.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/1.2.6/org/refcodes/rest/HttpDiscovery.html))
* \[<span style="color:green">MODIFIED</span>\] [`HttpRestServer.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.2.6/src/main/java/org/refcodes/rest/HttpRestServer.java) (see Javadoc at [`HttpRestServer.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/1.2.6/org/refcodes/rest/HttpRestServer.html))
* \[<span style="color:green">MODIFIED</span>\] [`HttpRestServerImpl.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.2.6/src/main/java/org/refcodes/rest/HttpRestServerImpl.java) (see Javadoc at [`HttpRestServerImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/1.2.6/org/refcodes/rest/HttpRestServerImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`HttpRestServerSingleton.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.2.6/src/main/java/org/refcodes/rest/HttpRestServerSingleton.java) (see Javadoc at [`HttpRestServerSingleton.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/1.2.6/org/refcodes/rest/HttpRestServerSingleton.html))
* \[<span style="color:green">MODIFIED</span>\] [`LoopbackRestServer.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.2.6/src/main/java/org/refcodes/rest/LoopbackRestServer.java) (see Javadoc at [`LoopbackRestServer.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/1.2.6/org/refcodes/rest/LoopbackRestServer.html))
* \[<span style="color:green">MODIFIED</span>\] [`LoopbackRestServerImpl.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.2.6/src/main/java/org/refcodes/rest/LoopbackRestServerImpl.java) (see Javadoc at [`LoopbackRestServerImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/1.2.6/org/refcodes/rest/LoopbackRestServerImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`RestCallerBuilder.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.2.6/src/main/java/org/refcodes/rest/RestCallerBuilder.java) (see Javadoc at [`RestCallerBuilder.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/1.2.6/org/refcodes/rest/RestCallerBuilder.html))
* \[<span style="color:green">MODIFIED</span>\] [`RestCallerBuilderImpl.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.2.6/src/main/java/org/refcodes/rest/RestCallerBuilderImpl.java) (see Javadoc at [`RestCallerBuilderImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/1.2.6/org/refcodes/rest/RestCallerBuilderImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`RestDeleteClientSugar.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.2.6/src/main/java/org/refcodes/rest/RestDeleteClientSugar.java) (see Javadoc at [`RestDeleteClientSugar.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/1.2.6/org/refcodes/rest/RestDeleteClientSugar.html))
* \[<span style="color:green">MODIFIED</span>\] [`RestEndpointBuilder.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.2.6/src/main/java/org/refcodes/rest/RestEndpointBuilder.java) (see Javadoc at [`RestEndpointBuilder.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/1.2.6/org/refcodes/rest/RestEndpointBuilder.html))
* \[<span style="color:green">MODIFIED</span>\] [`RestGetClientSugar.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.2.6/src/main/java/org/refcodes/rest/RestGetClientSugar.java) (see Javadoc at [`RestGetClientSugar.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/1.2.6/org/refcodes/rest/RestGetClientSugar.html))
* \[<span style="color:green">MODIFIED</span>\] [`RestPostClientSugar.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.2.6/src/main/java/org/refcodes/rest/RestPostClientSugar.java) (see Javadoc at [`RestPostClientSugar.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/1.2.6/org/refcodes/rest/RestPostClientSugar.html))
* \[<span style="color:green">MODIFIED</span>\] [`RestPutClientSugar.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.2.6/src/main/java/org/refcodes/rest/RestPutClientSugar.java) (see Javadoc at [`RestPutClientSugar.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/1.2.6/org/refcodes/rest/RestPutClientSugar.html))
* \[<span style="color:green">MODIFIED</span>\] [`RestRequestBuilderImpl.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.2.6/src/main/java/org/refcodes/rest/RestRequestBuilderImpl.java) (see Javadoc at [`RestRequestBuilderImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/1.2.6/org/refcodes/rest/RestRequestBuilderImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`RestServer.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.2.6/src/main/java/org/refcodes/rest/RestServer.java) (see Javadoc at [`RestServer.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/1.2.6/org/refcodes/rest/RestServer.html))
* \[<span style="color:green">MODIFIED</span>\] [`LoopbackRestServerTest.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.2.6/src/test/java/org/refcodes/rest/LoopbackRestServerTest.java) 

## Change list &lt;refcodes-logger-ext&gt; (version 1.2.6)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-1.2.6/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-1.2.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-1.2.6/refcodes-logger-ext-slf4j/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-1.2.6/refcodes-logger-ext-slf4j/pom.xml) 

## Change list &lt;refcodes-daemon&gt; (version 1.2.6)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-daemon/src/refcodes-daemon-1.2.6/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-daemon/src/refcodes-daemon-1.2.6/pom.xml) 

## Change list &lt;refcodes-eventbus&gt; (version 1.2.6)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-eventbus/src/refcodes-eventbus-1.2.6/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-eventbus/src/refcodes-eventbus-1.2.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`AbstractEventBus.java`](https://bitbucket.org/refcodes/refcodes-eventbus/src/refcodes-eventbus-1.2.6/src/main/java/org/refcodes/eventbus/AbstractEventBus.java) (see Javadoc at [`AbstractEventBus.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-eventbus/1.2.6/org/refcodes/eventbus/AbstractEventBus.html))

## Change list &lt;refcodes-eventbus-ext&gt; (version 1.2.6)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-1.2.6/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-1.2.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-1.2.6/refcodes-eventbus-ext-application/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`ExceptionBus.java`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-1.2.6/refcodes-eventbus-ext-application/src/main/java/org/refcodes/eventbus/ext/application/ExceptionBus.java) (see Javadoc at [`ExceptionBus.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-eventbus-ext-application/1.2.6/org/refcodes/eventbus/ext/application/ExceptionBus.html))
* \[<span style="color:green">MODIFIED</span>\] [`MessageBus.java`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-1.2.6/refcodes-eventbus-ext-application/src/main/java/org/refcodes/eventbus/ext/application/MessageBus.java) (see Javadoc at [`MessageBus.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-eventbus-ext-application/1.2.6/org/refcodes/eventbus/ext/application/MessageBus.html))
* \[<span style="color:green">MODIFIED</span>\] [`MessagePropertiesBus.java`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-1.2.6/refcodes-eventbus-ext-application/src/main/java/org/refcodes/eventbus/ext/application/MessagePropertiesBus.java) (see Javadoc at [`MessagePropertiesBus.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-eventbus-ext-application/1.2.6/org/refcodes/eventbus/ext/application/MessagePropertiesBus.html))
* \[<span style="color:green">MODIFIED</span>\] [`PayloadBus.java`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-1.2.6/refcodes-eventbus-ext-application/src/main/java/org/refcodes/eventbus/ext/application/PayloadBus.java) (see Javadoc at [`PayloadBus.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-eventbus-ext-application/1.2.6/org/refcodes/eventbus/ext/application/PayloadBus.html))
* \[<span style="color:green">MODIFIED</span>\] [`PayloadBusEventImpl.java`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-1.2.6/refcodes-eventbus-ext-application/src/main/java/org/refcodes/eventbus/ext/application/PayloadBusEventImpl.java) (see Javadoc at [`PayloadBusEventImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-eventbus-ext-application/1.2.6/org/refcodes/eventbus/ext/application/PayloadBusEventImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`PropertiesBus.java`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-1.2.6/refcodes-eventbus-ext-application/src/main/java/org/refcodes/eventbus/ext/application/PropertiesBus.java) (see Javadoc at [`PropertiesBus.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-eventbus-ext-application/1.2.6/org/refcodes/eventbus/ext/application/PropertiesBus.html))

## Change list &lt;refcodes-filesystem&gt; (version 1.2.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-filesystem/src/refcodes-filesystem-1.2.6/pom.xml) 

## Change list &lt;refcodes-filesystem-alt&gt; (version 1.2.6)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-filesystem-alt/src/refcodes-filesystem-alt-1.2.6/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-filesystem-alt/src/refcodes-filesystem-alt-1.2.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-filesystem-alt/src/refcodes-filesystem-alt-1.2.6/refcodes-filesystem-alt-s3/pom.xml) 

## Change list &lt;refcodes-forwardsecrecy&gt; (version 1.2.6)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy/src/refcodes-forwardsecrecy-1.2.6/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy/src/refcodes-forwardsecrecy-1.2.6/pom.xml) 

## Change list &lt;refcodes-forwardsecrecy-alt&gt; (version 1.2.6)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-1.2.6/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-1.2.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-1.2.6/refcodes-forwardsecrecy-alt-filesystem/pom.xml) 

## Change list &lt;refcodes-io-ext&gt; (version 1.2.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-1.2.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-1.2.6/refcodes-io-ext-observable/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-1.2.6/refcodes-io-ext-observable/pom.xml) 

## Change list &lt;refcodes-interceptor&gt; (version 1.2.6)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-interceptor/src/refcodes-interceptor-1.2.6/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-interceptor/src/refcodes-interceptor-1.2.6/pom.xml) 

## Change list &lt;refcodes-jobbus&gt; (version 1.2.6)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-jobbus/src/refcodes-jobbus-1.2.6/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-jobbus/src/refcodes-jobbus-1.2.6/pom.xml) 

## Change list &lt;refcodes-rest-ext&gt; (version 1.2.6)

* \[<span style="color:red">DELETED</span>\] `vi.exe.stackdump`
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-1.2.6/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-1.2.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-1.2.6/refcodes-rest-ext-eureka/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-1.2.6/refcodes-rest-ext-eureka/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`EurekaDiscoverySidecarImpl.java`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-1.2.6/refcodes-rest-ext-eureka/src/main/java/org/refcodes/rest/ext/eureka/EurekaDiscoverySidecarImpl.java) (see Javadoc at [`EurekaDiscoverySidecarImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest-ext-eureka/1.2.6/org/refcodes/rest/ext/eureka/EurekaDiscoverySidecarImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`EurekaRegistrySidecarImpl.java`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-1.2.6/refcodes-rest-ext-eureka/src/main/java/org/refcodes/rest/ext/eureka/EurekaRegistrySidecarImpl.java) (see Javadoc at [`EurekaRegistrySidecarImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest-ext-eureka/1.2.6/org/refcodes/rest/ext/eureka/EurekaRegistrySidecarImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`EurekaRestServer.java`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-1.2.6/refcodes-rest-ext-eureka/src/main/java/org/refcodes/rest/ext/eureka/EurekaRestServer.java) (see Javadoc at [`EurekaRestServer.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest-ext-eureka/1.2.6/org/refcodes/rest/ext/eureka/EurekaRestServer.html))
* \[<span style="color:green">MODIFIED</span>\] [`EurekaRestServerDecorator.java`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-1.2.6/refcodes-rest-ext-eureka/src/main/java/org/refcodes/rest/ext/eureka/EurekaRestServerDecorator.java) (see Javadoc at [`EurekaRestServerDecorator.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest-ext-eureka/1.2.6/org/refcodes/rest/ext/eureka/EurekaRestServerDecorator.html))

## Change list &lt;refcodes-remoting&gt; (version 1.2.6)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-remoting/src/refcodes-remoting-1.2.6/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-remoting/src/refcodes-remoting-1.2.6/pom.xml) 

## Change list &lt;refcodes-remoting-ext&gt; (version 1.2.6)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-1.2.6/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-1.2.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-1.2.6/refcodes-remoting-ext-observer/pom.xml) 

## Change list &lt;refcodes-security-alt&gt; (version 1.2.6)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-1.2.6/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-1.2.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-1.2.6/refcodes-security-alt-chaos/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-1.2.6/refcodes-security-alt-chaos/pom.xml) 

## Change list &lt;refcodes-security-ext&gt; (version 1.2.6)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-1.2.6/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-1.2.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-1.2.6/refcodes-security-ext-chaos/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-1.2.6/refcodes-security-ext-chaos/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`ChaosProviderImpl.java`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-1.2.6/refcodes-security-ext-chaos/src/main/java/org/refcodes/security/ext/chaos/ChaosProviderImpl.java) (see Javadoc at [`ChaosProviderImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-security-ext-chaos/1.2.6/org/refcodes/security/ext/chaos/ChaosProviderImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-1.2.6/refcodes-security-ext-spring/pom.xml) 

## Change list &lt;refcodes-servicebus&gt; (version 1.2.6)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-servicebus/src/refcodes-servicebus-1.2.6/pom.xml) 

## Change list &lt;refcodes-servicebus-alt&gt; (version 1.2.6)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-servicebus-alt/src/refcodes-servicebus-alt-1.2.6/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-servicebus-alt/src/refcodes-servicebus-alt-1.2.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-servicebus-alt/src/refcodes-servicebus-alt-1.2.6/refcodes-servicebus-alt-spring/pom.xml) 

## Change list &lt;refcodes-tabular-alt&gt; (version 1.2.6)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-tabular-alt/src/refcodes-tabular-alt-1.2.6/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-tabular-alt/src/refcodes-tabular-alt-1.2.6/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-tabular-alt/src/refcodes-tabular-alt-1.2.6/refcodes-tabular-alt-forwardsecrecy/pom.xml) 
