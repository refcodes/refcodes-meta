# Change list for REFCODES.ORG artifacts' version 1.3.2

> This change list has been auto-generated on `triton` by `steiner` with with `changelist-all.sh` on the 2018-09-21 at 06:47:39.

## Change list &lt;refcodes-licensing&gt; (version 1.3.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-licensing/src/refcodes-licensing-1.3.2/pom.xml) 

## Change list &lt;refcodes-parent&gt; (version 1.3.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-parent/src/refcodes-parent-1.3.2/pom.xml) 

## Change list &lt;refcodes-time&gt; (version 1.3.2)

* \[<span style="color:blue">ADDED</span>\] [`CookieDateFormatTest.java`](https://bitbucket.org/refcodes/refcodes-time/src/refcodes-time-1.3.2/src/test/java/org/refcodes/time/CookieDateFormatTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-time/src/refcodes-time-1.3.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`DateFormat.java`](https://bitbucket.org/refcodes/refcodes-time/src/refcodes-time-1.3.2/src/main/java/org/refcodes/time/DateFormat.java) (see Javadoc at [`DateFormat.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-time/1.3.2/org/refcodes/time/DateFormat.html))
* \[<span style="color:green">MODIFIED</span>\] [`DateFormats.java`](https://bitbucket.org/refcodes/refcodes-time/src/refcodes-time-1.3.2/src/main/java/org/refcodes/time/DateFormats.java) (see Javadoc at [`DateFormats.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-time/1.3.2/org/refcodes/time/DateFormats.html))

## Change list &lt;refcodes-mixin&gt; (version 1.3.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-mixin/src/refcodes-mixin-1.3.2/pom.xml) 

## Change list &lt;refcodes-data&gt; (version 1.3.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data/src/refcodes-data-1.3.2/pom.xml) 

## Change list &lt;refcodes-exception&gt; (version 1.3.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-exception/src/refcodes-exception-1.3.2/pom.xml) 

## Change list &lt;refcodes-factory&gt; (version 1.3.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-factory/src/refcodes-factory-1.3.2/pom.xml) 

## Change list &lt;refcodes-factory-alt&gt; (version 1.3.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-factory-alt/src/refcodes-factory-alt-1.3.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-factory-alt/src/refcodes-factory-alt-1.3.2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-factory-alt/src/refcodes-factory-alt-1.3.2/refcodes-factory-alt-spring/pom.xml) 

## Change list &lt;refcodes-controlflow&gt; (version 1.3.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-controlflow/src/refcodes-controlflow-1.3.2/pom.xml) 

## Change list &lt;refcodes-numerical&gt; (version 1.3.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-numerical/src/refcodes-numerical-1.3.2/pom.xml) 

## Change list &lt;refcodes-generator&gt; (version 1.3.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-generator/src/refcodes-generator-1.3.2/pom.xml) 

## Change list &lt;refcodes-structure&gt; (version 1.3.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-1.3.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-1.3.2/README.md) 

## Change list &lt;refcodes-runtime&gt; (version 1.3.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-1.3.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-1.3.2/README.md) 

## Change list &lt;refcodes-component&gt; (version 1.3.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-1.3.2/pom.xml) 

## Change list &lt;refcodes-data-ext&gt; (version 1.3.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.3.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.3.2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.3.2/refcodes-data-ext-boulderdash/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.3.2/refcodes-data-ext-boulderdash/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.3.2/refcodes-data-ext-checkers/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.3.2/refcodes-data-ext-checkers/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.3.2/refcodes-data-ext-chess/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.3.2/refcodes-data-ext-chess/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.3.2/refcodes-data-ext-corporate/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.3.2/refcodes-data-ext-symbols/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.3.2/refcodes-data-ext-symbols/README.md) 

## Change list &lt;refcodes-graphical&gt; (version 1.3.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-graphical/src/refcodes-graphical-1.3.2/pom.xml) 

## Change list &lt;refcodes-textual&gt; (version 1.3.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-1.3.2/pom.xml) 

## Change list &lt;refcodes-criteria&gt; (version 1.3.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-1.3.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-1.3.2/README.md) 

## Change list &lt;refcodes-tabular&gt; (version 1.3.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-tabular/src/refcodes-tabular-1.3.2/pom.xml) 

## Change list &lt;refcodes-matcher&gt; (version 1.3.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-matcher/src/refcodes-matcher-1.3.2/pom.xml) 

## Change list &lt;refcodes-observer&gt; (version 1.3.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-observer/src/refcodes-observer-1.3.2/pom.xml) 

## Change list &lt;refcodes-command&gt; (version 1.3.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-command/src/refcodes-command-1.3.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-command/src/refcodes-command-1.3.2/README.md) 

## Change list &lt;refcodes-cli&gt; (version 1.3.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-1.3.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-1.3.2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`AbstractCondition.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-1.3.2/src/main/java/org/refcodes/console/AbstractCondition.java) (see Javadoc at [`AbstractCondition.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/1.3.2/org/refcodes/console/AbstractCondition.html))
* \[<span style="color:green">MODIFIED</span>\] [`ArgsParserImpl.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-1.3.2/src/main/java/org/refcodes/console/ArgsParserImpl.java) (see Javadoc at [`ArgsParserImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/1.3.2/org/refcodes/console/ArgsParserImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`ArgsParser.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-1.3.2/src/main/java/org/refcodes/console/ArgsParser.java) (see Javadoc at [`ArgsParser.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/1.3.2/org/refcodes/console/ArgsParser.html))
* \[<span style="color:green">MODIFIED</span>\] [`ArgsSyntax.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-1.3.2/src/main/java/org/refcodes/console/ArgsSyntax.java) (see Javadoc at [`ArgsSyntax.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/1.3.2/org/refcodes/console/ArgsSyntax.html))
* \[<span style="color:green">MODIFIED</span>\] [`CliSugar.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-1.3.2/src/main/java/org/refcodes/console/CliSugar.java) (see Javadoc at [`CliSugar.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/1.3.2/org/refcodes/console/CliSugar.html))
* \[<span style="color:green">MODIFIED</span>\] [`ConsoleUtility.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-1.3.2/src/main/java/org/refcodes/console/ConsoleUtility.java) (see Javadoc at [`ConsoleUtility.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/1.3.2/org/refcodes/console/ConsoleUtility.html))
* \[<span style="color:green">MODIFIED</span>\] [`package-info.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-1.3.2/src/main/java/org/refcodes/console/package-info.java) 
* \[<span style="color:green">MODIFIED</span>\] [`Syntaxable.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-1.3.2/src/main/java/org/refcodes/console/Syntaxable.java) (see Javadoc at [`Syntaxable.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/1.3.2/org/refcodes/console/Syntaxable.html))
* \[<span style="color:green">MODIFIED</span>\] [`XorConditionImpl.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-1.3.2/src/main/java/org/refcodes/console/XorConditionImpl.java) (see Javadoc at [`XorConditionImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/1.3.2/org/refcodes/console/XorConditionImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`ArgsParserTest.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-1.3.2/src/test/java/org/refcodes/console/ArgsParserTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`OptionalConditionTest.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-1.3.2/src/test/java/org/refcodes/console/OptionalConditionTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`StackOverflowTest.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-1.3.2/src/test/java/org/refcodes/console/StackOverflowTest.java) 

## Change list &lt;refcodes-io&gt; (version 1.3.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-io/src/refcodes-io-1.3.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-io/src/refcodes-io-1.3.2/README.md) 

## Change list &lt;refcodes-codec&gt; (version 1.3.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-1.3.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-1.3.2/README.md) 

## Change list &lt;refcodes-component-ext&gt; (version 1.3.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.3.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.3.2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.3.2/refcodes-component-ext-observer/pom.xml) 

## Change list &lt;refcodes-properties&gt; (version 1.3.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.3.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.3.2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`package-info.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.3.2/src/main/java/org/refcodes/configuration/package-info.java) 

## Change list &lt;refcodes-security&gt; (version 1.3.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security/src/refcodes-security-1.3.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security/src/refcodes-security-1.3.2/README.md) 

## Change list &lt;refcodes-security-alt&gt; (version 1.3.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-1.3.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-1.3.2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-1.3.2/refcodes-security-alt-chaos/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-1.3.2/refcodes-security-alt-chaos/README.md) 

## Change list &lt;refcodes-security-ext&gt; (version 1.3.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-1.3.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-1.3.2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-1.3.2/refcodes-security-ext-chaos/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-1.3.2/refcodes-security-ext-chaos/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-1.3.2/refcodes-security-ext-spring/pom.xml) 

## Change list &lt;refcodes-properties-ext&gt; (version 1.3.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.3.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.3.2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.3.2/refcodes-properties-ext-cli/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.3.2/refcodes-properties-ext-cli/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`ArgsParserPropertiesTest.java`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.3.2/refcodes-properties-ext-cli/src/test/java/org/refcodes/configuration/ext/console/ArgsParserPropertiesTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.3.2/refcodes-properties-ext-obfuscation/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.3.2/refcodes-properties-ext-obfuscation/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.3.2/refcodes-properties-ext-observer/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.3.2/refcodes-properties-ext-observer/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.3.2/refcodes-properties-ext-runtime/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.3.2/refcodes-properties-ext-runtime/README.md) 

## Change list &lt;refcodes-logger&gt; (version 1.3.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger/src/refcodes-logger-1.3.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger/src/refcodes-logger-1.3.2/README.md) 

## Change list &lt;refcodes-logger-alt&gt; (version 1.3.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.3.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.3.2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.3.2/refcodes-logger-alt-async/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.3.2/refcodes-logger-alt-async/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.3.2/refcodes-logger-alt-console/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.3.2/refcodes-logger-alt-console/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`ConsoleLoggerImpl.java`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.3.2/refcodes-logger-alt-console/src/main/java/org/refcodes/logger/alt/console/ConsoleLoggerImpl.java) (see Javadoc at [`ConsoleLoggerImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-logger-alt-console/1.3.2/org/refcodes/logger/alt/console/ConsoleLoggerImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`FormattedLoggerImpl.java`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.3.2/refcodes-logger-alt-console/src/main/java/org/refcodes/logger/alt/console/FormattedLoggerImpl.java) (see Javadoc at [`FormattedLoggerImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-logger-alt-console/1.3.2/org/refcodes/logger/alt/console/FormattedLoggerImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.3.2/refcodes-logger-alt-io/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.3.2/refcodes-logger-alt-io/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.3.2/refcodes-logger-alt-simpledb/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.3.2/refcodes-logger-alt-slf4j/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.3.2/refcodes-logger-alt-spring/pom.xml) 

## Change list &lt;refcodes-logger-ext&gt; (version 1.3.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-1.3.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-1.3.2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-1.3.2/refcodes-logger-ext-slf4j/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-1.3.2/refcodes-logger-ext-slf4j/README.md) 

## Change list &lt;refcodes-graphical-ext&gt; (version 1.3.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-1.3.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-1.3.2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-1.3.2/refcodes-graphical-ext-javafx/pom.xml) 

## Change list &lt;refcodes-checkerboard&gt; (version 1.3.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-1.3.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-1.3.2/README.md) 

## Change list &lt;refcodes-checkerboard-alt&gt; (version 1.3.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-alt/src/refcodes-checkerboard-alt-1.3.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-alt/src/refcodes-checkerboard-alt-1.3.2/refcodes-checkerboard-alt-javafx/pom.xml) 

## Change list &lt;refcodes-checkerboard-ext&gt; (version 1.3.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-ext/src/refcodes-checkerboard-ext-1.3.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-checkerboard-ext/src/refcodes-checkerboard-ext-1.3.2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-ext/src/refcodes-checkerboard-ext-1.3.2/refcodes-checkerboard-ext-javafx-boulderdash/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-ext/src/refcodes-checkerboard-ext-1.3.2/refcodes-checkerboard-ext-javafx-chess/pom.xml) 

## Change list &lt;refcodes-boulderdash&gt; (version 1.3.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-boulderdash/src/refcodes-boulderdash-1.3.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-boulderdash/src/refcodes-boulderdash-1.3.2/README.md) 

## Change list &lt;refcodes-net&gt; (version 1.3.2)

* \[<span style="color:blue">ADDED</span>\] [`CookieTest.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.3.2/src/test/java/org/refcodes/net/CookieTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.3.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.3.2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`CookieAttribute.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.3.2/src/main/java/org/refcodes/net/CookieAttribute.java) (see Javadoc at [`CookieAttribute.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/1.3.2/org/refcodes/net/CookieAttribute.html))
* \[<span style="color:green">MODIFIED</span>\] [`ResponseCookie.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.3.2/src/main/java/org/refcodes/net/ResponseCookie.java) (see Javadoc at [`ResponseCookie.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/1.3.2/org/refcodes/net/ResponseCookie.html))

## Change list &lt;refcodes-rest&gt; (version 1.3.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.3.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.3.2/README.md) 

## Change list &lt;refcodes-daemon&gt; (version 1.3.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-daemon/src/refcodes-daemon-1.3.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-daemon/src/refcodes-daemon-1.3.2/README.md) 

## Change list &lt;refcodes-eventbus&gt; (version 1.3.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-eventbus/src/refcodes-eventbus-1.3.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-eventbus/src/refcodes-eventbus-1.3.2/README.md) 

## Change list &lt;refcodes-eventbus-ext&gt; (version 1.3.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-1.3.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-1.3.2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-1.3.2/refcodes-eventbus-ext-application/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-1.3.2/refcodes-eventbus-ext-application/README.md) 

## Change list &lt;refcodes-filesystem&gt; (version 1.3.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-filesystem/src/refcodes-filesystem-1.3.2/pom.xml) 

## Change list &lt;refcodes-filesystem-alt&gt; (version 1.3.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-filesystem-alt/src/refcodes-filesystem-alt-1.3.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-filesystem-alt/src/refcodes-filesystem-alt-1.3.2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-filesystem-alt/src/refcodes-filesystem-alt-1.3.2/refcodes-filesystem-alt-s3/pom.xml) 

## Change list &lt;refcodes-forwardsecrecy&gt; (version 1.3.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy/src/refcodes-forwardsecrecy-1.3.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy/src/refcodes-forwardsecrecy-1.3.2/README.md) 

## Change list &lt;refcodes-forwardsecrecy-alt&gt; (version 1.3.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-1.3.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-1.3.2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-1.3.2/refcodes-forwardsecrecy-alt-filesystem/pom.xml) 

## Change list &lt;refcodes-io-ext&gt; (version 1.3.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-1.3.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-1.3.2/refcodes-io-ext-observable/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-1.3.2/refcodes-io-ext-observable/README.md) 

## Change list &lt;refcodes-interceptor&gt; (version 1.3.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-interceptor/src/refcodes-interceptor-1.3.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-interceptor/src/refcodes-interceptor-1.3.2/README.md) 

## Change list &lt;refcodes-jobbus&gt; (version 1.3.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-jobbus/src/refcodes-jobbus-1.3.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-jobbus/src/refcodes-jobbus-1.3.2/README.md) 

## Change list &lt;refcodes-rest-ext&gt; (version 1.3.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-1.3.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-1.3.2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-1.3.2/refcodes-rest-ext-eureka/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-1.3.2/refcodes-rest-ext-eureka/README.md) 

## Change list &lt;refcodes-remoting&gt; (version 1.3.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-remoting/src/refcodes-remoting-1.3.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-remoting/src/refcodes-remoting-1.3.2/README.md) 

## Change list &lt;refcodes-remoting-ext&gt; (version 1.3.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-1.3.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-1.3.2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-1.3.2/refcodes-remoting-ext-observer/pom.xml) 

## Change list &lt;refcodes-servicebus&gt; (version 1.3.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-servicebus/src/refcodes-servicebus-1.3.2/pom.xml) 

## Change list &lt;refcodes-servicebus-alt&gt; (version 1.3.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-servicebus-alt/src/refcodes-servicebus-alt-1.3.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-servicebus-alt/src/refcodes-servicebus-alt-1.3.2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-servicebus-alt/src/refcodes-servicebus-alt-1.3.2/refcodes-servicebus-alt-spring/pom.xml) 

## Change list &lt;refcodes-tabular-alt&gt; (version 1.3.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-tabular-alt/src/refcodes-tabular-alt-1.3.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-tabular-alt/src/refcodes-tabular-alt-1.3.2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-tabular-alt/src/refcodes-tabular-alt-1.3.2/refcodes-tabular-alt-forwardsecrecy/pom.xml) 
