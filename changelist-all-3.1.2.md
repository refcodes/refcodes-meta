> This change list has been auto-generated on `triton` by `steiner` with `changelist-all.sh` on the 2023-04-21 at 17:55:58.

## Change list &lt;refcodes-rest&gt; (version 3.1.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-3.1.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-3.1.2/README.md) 

## Change list &lt;refcodes-hal&gt; (version 3.1.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-3.1.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-3.1.2/README.md) 

## Change list &lt;refcodes-eventbus&gt; (version 3.1.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-eventbus/src/refcodes-eventbus-3.1.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-eventbus/src/refcodes-eventbus-3.1.2/README.md) 

## Change list &lt;refcodes-eventbus-ext&gt; (version 3.1.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-3.1.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-3.1.2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-3.1.2/refcodes-eventbus-ext-application/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-3.1.2/refcodes-eventbus-ext-application/README.md) 

## Change list &lt;refcodes-decoupling&gt; (version 3.1.2)

* \[<span style="color:blue">ADDED</span>\] [`QFactory.java`](https://bitbucket.org/refcodes/refcodes-decoupling/src/refcodes-decoupling-3.1.2/src/test/java/org/refcodes/decoupling/QFactory.java) 
* \[<span style="color:red">DELETED</span>\] `DependencyBuilderImpl.java`
* \[<span style="color:red">DELETED</span>\] `DependencyImpl.java`
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-decoupling/src/refcodes-decoupling-3.1.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-decoupling/src/refcodes-decoupling-3.1.2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`AmbigousFactoryException.java`](https://bitbucket.org/refcodes/refcodes-decoupling/src/refcodes-decoupling-3.1.2/src/main/java/org/refcodes/decoupling/AmbigousFactoryException.java) (see Javadoc at [`AmbigousFactoryException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-decoupling/3.1.2/org.refcodes.decoupling/org/refcodes/decoupling/AmbigousFactoryException.html))
* \[<span style="color:green">MODIFIED</span>\] [`Claim.java`](https://bitbucket.org/refcodes/refcodes-decoupling/src/refcodes-decoupling-3.1.2/src/main/java/org/refcodes/decoupling/Claim.java) (see Javadoc at [`Claim.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-decoupling/3.1.2/org.refcodes.decoupling/org/refcodes/decoupling/Claim.html))
* \[<span style="color:green">MODIFIED</span>\] [`Context.java`](https://bitbucket.org/refcodes/refcodes-decoupling/src/refcodes-decoupling-3.1.2/src/main/java/org/refcodes/decoupling/Context.java) (see Javadoc at [`Context.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-decoupling/3.1.2/org.refcodes.decoupling/org/refcodes/decoupling/Context.html))
* \[<span style="color:green">MODIFIED</span>\] [`DependencyBuilder.java`](https://bitbucket.org/refcodes/refcodes-decoupling/src/refcodes-decoupling-3.1.2/src/main/java/org/refcodes/decoupling/DependencyBuilder.java) (see Javadoc at [`DependencyBuilder.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-decoupling/3.1.2/org.refcodes.decoupling/org/refcodes/decoupling/DependencyBuilder.html))
* \[<span style="color:green">MODIFIED</span>\] [`DependencyException.java`](https://bitbucket.org/refcodes/refcodes-decoupling/src/refcodes-decoupling-3.1.2/src/main/java/org/refcodes/decoupling/DependencyException.java) (see Javadoc at [`DependencyException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-decoupling/3.1.2/org.refcodes.decoupling/org/refcodes/decoupling/DependencyException.html))
* \[<span style="color:green">MODIFIED</span>\] [`Dependency.java`](https://bitbucket.org/refcodes/refcodes-decoupling/src/refcodes-decoupling-3.1.2/src/main/java/org/refcodes/decoupling/Dependency.java) (see Javadoc at [`Dependency.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-decoupling/3.1.2/org.refcodes.decoupling/org/refcodes/decoupling/Dependency.html))
* \[<span style="color:green">MODIFIED</span>\] [`DependencySchema.java`](https://bitbucket.org/refcodes/refcodes-decoupling/src/refcodes-decoupling-3.1.2/src/main/java/org/refcodes/decoupling/DependencySchema.java) (see Javadoc at [`DependencySchema.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-decoupling/3.1.2/org.refcodes.decoupling/org/refcodes/decoupling/DependencySchema.html))
* \[<span style="color:green">MODIFIED</span>\] [`InstallDependencyException.java`](https://bitbucket.org/refcodes/refcodes-decoupling/src/refcodes-decoupling-3.1.2/src/main/java/org/refcodes/decoupling/InstallDependencyException.java) (see Javadoc at [`InstallDependencyException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-decoupling/3.1.2/org.refcodes.decoupling/org/refcodes/decoupling/InstallDependencyException.html))
* \[<span style="color:green">MODIFIED</span>\] [`Reactor.java`](https://bitbucket.org/refcodes/refcodes-decoupling/src/refcodes-decoupling-3.1.2/src/main/java/org/refcodes/decoupling/Reactor.java) (see Javadoc at [`Reactor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-decoupling/3.1.2/org.refcodes.decoupling/org/refcodes/decoupling/Reactor.html))
* \[<span style="color:green">MODIFIED</span>\] [`UnsatisfiedFactoryException.java`](https://bitbucket.org/refcodes/refcodes-decoupling/src/refcodes-decoupling-3.1.2/src/main/java/org/refcodes/decoupling/UnsatisfiedFactoryException.java) (see Javadoc at [`UnsatisfiedFactoryException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-decoupling/3.1.2/org.refcodes.decoupling/org/refcodes/decoupling/UnsatisfiedFactoryException.html))
* \[<span style="color:green">MODIFIED</span>\] [`ClaimTest.java`](https://bitbucket.org/refcodes/refcodes-decoupling/src/refcodes-decoupling-3.1.2/src/test/java/org/refcodes/decoupling/ClaimTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`ComponentQ2.java`](https://bitbucket.org/refcodes/refcodes-decoupling/src/refcodes-decoupling-3.1.2/src/test/java/org/refcodes/decoupling/ComponentQ2.java) 
* \[<span style="color:green">MODIFIED</span>\] [`ComponentQ.java`](https://bitbucket.org/refcodes/refcodes-decoupling/src/refcodes-decoupling-3.1.2/src/test/java/org/refcodes/decoupling/ComponentQ.java) 
* \[<span style="color:green">MODIFIED</span>\] [`ContextTest.java`](https://bitbucket.org/refcodes/refcodes-decoupling/src/refcodes-decoupling-3.1.2/src/test/java/org/refcodes/decoupling/ContextTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`FactoryTest.java`](https://bitbucket.org/refcodes/refcodes-decoupling/src/refcodes-decoupling-3.1.2/src/test/java/org/refcodes/decoupling/FactoryTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`ReactorTest.java`](https://bitbucket.org/refcodes/refcodes-decoupling/src/refcodes-decoupling-3.1.2/src/test/java/org/refcodes/decoupling/ReactorTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`createdFiles.lst`](https://bitbucket.org/refcodes/refcodes-decoupling/src/refcodes-decoupling-3.1.2/target/maven-status/maven-compiler-plugin/compile/default-compile/createdFiles.lst) 
* \[<span style="color:green">MODIFIED</span>\] [`inputFiles.lst`](https://bitbucket.org/refcodes/refcodes-decoupling/src/refcodes-decoupling-3.1.2/target/maven-status/maven-compiler-plugin/compile/default-compile/inputFiles.lst) 

## Change list &lt;refcodes-decoupling-ext&gt; (version 3.1.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-decoupling-ext/src/refcodes-decoupling-ext-3.1.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-decoupling-ext/src/refcodes-decoupling-ext-3.1.2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-decoupling-ext/src/refcodes-decoupling-ext-3.1.2/refcodes-decoupling-ext-application/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-decoupling-ext/src/refcodes-decoupling-ext-3.1.2/refcodes-decoupling-ext-application/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`module-info.java`](https://bitbucket.org/refcodes/refcodes-decoupling-ext/src/refcodes-decoupling-ext-3.1.2/refcodes-decoupling-ext-application/src/main/java/module-info.java) (see Javadoc at [`module-info.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-decoupling-ext-application/3.1.2//module-info.html))
* \[<span style="color:green">MODIFIED</span>\] [`ApplicationContext.java`](https://bitbucket.org/refcodes/refcodes-decoupling-ext/src/refcodes-decoupling-ext-3.1.2/refcodes-decoupling-ext-application/src/main/java/org/refcodes/decoupling/ext/application/ApplicationContext.java) (see Javadoc at [`ApplicationContext.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-decoupling-ext-application/3.1.2/org.refcodes.decoupling.ext.application/org/refcodes/decoupling/ext/application/ApplicationContext.html))
* \[<span style="color:green">MODIFIED</span>\] [`ApplicationReactor.java`](https://bitbucket.org/refcodes/refcodes-decoupling-ext/src/refcodes-decoupling-ext-3.1.2/refcodes-decoupling-ext-application/src/main/java/org/refcodes/decoupling/ext/application/ApplicationReactor.java) (see Javadoc at [`ApplicationReactor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-decoupling-ext-application/3.1.2/org.refcodes.decoupling.ext.application/org/refcodes/decoupling/ext/application/ApplicationReactor.html))

## Change list &lt;refcodes-filesystem&gt; (version 3.1.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-filesystem/src/refcodes-filesystem-3.1.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-filesystem/src/refcodes-filesystem-3.1.2/README.md) 

## Change list &lt;refcodes-filesystem-alt&gt; (version 3.1.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-filesystem-alt/src/refcodes-filesystem-alt-3.1.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-filesystem-alt/src/refcodes-filesystem-alt-3.1.2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-filesystem-alt/src/refcodes-filesystem-alt-3.1.2/refcodes-filesystem-alt-s3/pom.xml) 

## Change list &lt;refcodes-forwardsecrecy&gt; (version 3.1.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy/src/refcodes-forwardsecrecy-3.1.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy/src/refcodes-forwardsecrecy-3.1.2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`AbstractCipherVersionGenerator.java`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy/src/refcodes-forwardsecrecy-3.1.2/src/main/java/org/refcodes/forwardsecrecy/AbstractCipherVersionGenerator.java) (see Javadoc at [`AbstractCipherVersionGenerator.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-forwardsecrecy/3.1.2/org.refcodes.forwardsecrecy/org/refcodes/forwardsecrecy/AbstractCipherVersionGenerator.html))
* \[<span style="color:green">MODIFIED</span>\] [`CipherVersionFactoryImpl.java`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy/src/refcodes-forwardsecrecy-3.1.2/src/main/java/org/refcodes/forwardsecrecy/CipherVersionFactoryImpl.java) (see Javadoc at [`CipherVersionFactoryImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-forwardsecrecy/3.1.2/org.refcodes.forwardsecrecy/org/refcodes/forwardsecrecy/CipherVersionFactoryImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`CipherVersionFactory.java`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy/src/refcodes-forwardsecrecy-3.1.2/src/main/java/org/refcodes/forwardsecrecy/CipherVersionFactory.java) (see Javadoc at [`CipherVersionFactory.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-forwardsecrecy/3.1.2/org.refcodes.forwardsecrecy/org/refcodes/forwardsecrecy/CipherVersionFactory.html))
* \[<span style="color:green">MODIFIED</span>\] [`InMemoryDecryptionServer.java`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy/src/refcodes-forwardsecrecy-3.1.2/src/main/java/org/refcodes/forwardsecrecy/InMemoryDecryptionServer.java) (see Javadoc at [`InMemoryDecryptionServer.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-forwardsecrecy/3.1.2/org.refcodes.forwardsecrecy/org/refcodes/forwardsecrecy/InMemoryDecryptionServer.html))
* \[<span style="color:green">MODIFIED</span>\] [`PublicKeyDecryptionServerWrapper.java`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy/src/refcodes-forwardsecrecy-3.1.2/src/main/java/org/refcodes/forwardsecrecy/PublicKeyDecryptionServerWrapper.java) (see Javadoc at [`PublicKeyDecryptionServerWrapper.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-forwardsecrecy/3.1.2/org.refcodes.forwardsecrecy/org/refcodes/forwardsecrecy/PublicKeyDecryptionServerWrapper.html))
* \[<span style="color:green">MODIFIED</span>\] [`PublicKeyDecryptionService.java`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy/src/refcodes-forwardsecrecy-3.1.2/src/main/java/org/refcodes/forwardsecrecy/PublicKeyDecryptionService.java) (see Javadoc at [`PublicKeyDecryptionService.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-forwardsecrecy/3.1.2/org.refcodes.forwardsecrecy/org/refcodes/forwardsecrecy/PublicKeyDecryptionService.html))
* \[<span style="color:green">MODIFIED</span>\] [`PublicKeyEncryptionService.java`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy/src/refcodes-forwardsecrecy-3.1.2/src/main/java/org/refcodes/forwardsecrecy/PublicKeyEncryptionService.java) (see Javadoc at [`PublicKeyEncryptionService.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-forwardsecrecy/3.1.2/org.refcodes.forwardsecrecy/org/refcodes/forwardsecrecy/PublicKeyEncryptionService.html))

## Change list &lt;refcodes-forwardsecrecy-alt&gt; (version 3.1.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-3.1.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-3.1.2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-3.1.2/refcodes-forwardsecrecy-alt-filesystem/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`BasicForwardSecrecyFileSystemTest.java`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-3.1.2/refcodes-forwardsecrecy-alt-filesystem/src/test/java/org/refcodes/forwardsecrecy/alt/filesystem/BasicForwardSecrecyFileSystemTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`ForwardSecrecyFileSystemTest.java`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-3.1.2/refcodes-forwardsecrecy-alt-filesystem/src/test/java/org/refcodes/forwardsecrecy/alt/filesystem/ForwardSecrecyFileSystemTest.java) 

## Change list &lt;refcodes-io-ext&gt; (version 3.1.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-3.1.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-3.1.2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-3.1.2/refcodes-io-ext-observer/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-3.1.2/refcodes-io-ext-observer/README.md) 

## Change list &lt;refcodes-interceptor&gt; (version 3.1.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-interceptor/src/refcodes-interceptor-3.1.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-interceptor/src/refcodes-interceptor-3.1.2/README.md) 

## Change list &lt;refcodes-jobbus&gt; (version 3.1.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-jobbus/src/refcodes-jobbus-3.1.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-jobbus/src/refcodes-jobbus-3.1.2/README.md) 

## Change list &lt;refcodes-rest-ext&gt; (version 3.1.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-3.1.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-3.1.2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-3.1.2/refcodes-rest-ext-eureka/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-3.1.2/refcodes-rest-ext-eureka/README.md) 

## Change list &lt;refcodes-remoting&gt; (version 3.1.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-remoting/src/refcodes-remoting-3.1.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-remoting/src/refcodes-remoting-3.1.2/README.md) 

## Change list &lt;refcodes-remoting-ext&gt; (version 3.1.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-3.1.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-3.1.2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-3.1.2/refcodes-remoting-ext-observer/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-3.1.2/refcodes-remoting-ext-observer/README.md) 

## Change list &lt;refcodes-serial&gt; (version 3.1.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial/src/refcodes-serial-3.1.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-serial/src/refcodes-serial-3.1.2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`FixedSegmentArraySection.java`](https://bitbucket.org/refcodes/refcodes-serial/src/refcodes-serial-3.1.2/src/main/java/org/refcodes/serial/FixedSegmentArraySection.java) (see Javadoc at [`FixedSegmentArraySection.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-serial/3.1.2/org.refcodes.serial/org/refcodes/serial/FixedSegmentArraySection.html))
* \[<span style="color:green">MODIFIED</span>\] [`SectionComposite.java`](https://bitbucket.org/refcodes/refcodes-serial/src/refcodes-serial-3.1.2/src/main/java/org/refcodes/serial/SectionComposite.java) (see Javadoc at [`SectionComposite.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-serial/3.1.2/org.refcodes.serial/org/refcodes/serial/SectionComposite.html))
* \[<span style="color:green">MODIFIED</span>\] [`SegmentArraySection.java`](https://bitbucket.org/refcodes/refcodes-serial/src/refcodes-serial-3.1.2/src/main/java/org/refcodes/serial/SegmentArraySection.java) (see Javadoc at [`SegmentArraySection.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-serial/3.1.2/org.refcodes.serial/org/refcodes/serial/SegmentArraySection.html))
* \[<span style="color:green">MODIFIED</span>\] [`StringArraySection.java`](https://bitbucket.org/refcodes/refcodes-serial/src/refcodes-serial-3.1.2/src/main/java/org/refcodes/serial/StringArraySection.java) (see Javadoc at [`StringArraySection.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-serial/3.1.2/org.refcodes.serial/org/refcodes/serial/StringArraySection.html))
* \[<span style="color:green">MODIFIED</span>\] [`Transmission.java`](https://bitbucket.org/refcodes/refcodes-serial/src/refcodes-serial-3.1.2/src/main/java/org/refcodes/serial/Transmission.java) (see Javadoc at [`Transmission.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-serial/3.1.2/org.refcodes.serial/org/refcodes/serial/Transmission.html))

## Change list &lt;refcodes-serial-alt&gt; (version 3.1.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial-alt/src/refcodes-serial-alt-3.1.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-serial-alt/src/refcodes-serial-alt-3.1.2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial-alt/src/refcodes-serial-alt-3.1.2/refcodes-serial-alt-net/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-serial-alt/src/refcodes-serial-alt-3.1.2/refcodes-serial-alt-net/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial-alt/src/refcodes-serial-alt-3.1.2/refcodes-serial-alt-tty/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-serial-alt/src/refcodes-serial-alt-3.1.2/refcodes-serial-alt-tty/README.md) 

## Change list &lt;refcodes-serial-ext&gt; (version 3.1.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-3.1.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-3.1.2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-3.1.2/refcodes-serial-ext-handshake/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-3.1.2/refcodes-serial-ext-handshake/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-3.1.2/refcodes-serial-ext-observer/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-3.1.2/refcodes-serial-ext-observer/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-3.1.2/refcodes-serial-ext-security/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-serial-ext/src/refcodes-serial-ext-3.1.2/refcodes-serial-ext-security/README.md) 

## Change list &lt;refcodes-p2p&gt; (version 3.1.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-p2p/src/refcodes-p2p-3.1.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-p2p/src/refcodes-p2p-3.1.2/README.md) 

## Change list &lt;refcodes-p2p-alt&gt; (version 3.1.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-p2p-alt/src/refcodes-p2p-alt-3.1.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-p2p-alt/src/refcodes-p2p-alt-3.1.2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-p2p-alt/src/refcodes-p2p-alt-3.1.2/refcodes-p2p-alt-rest/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-p2p-alt/src/refcodes-p2p-alt-3.1.2/refcodes-p2p-alt-serial/pom.xml) 

## Change list &lt;refcodes-p2p-ext&gt; (version 3.1.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-p2p-ext/src/refcodes-p2p-ext-3.1.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-p2p-ext/src/refcodes-p2p-ext-3.1.2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-p2p-ext/src/refcodes-p2p-ext-3.1.2/refcodes-p2p-ext-observer/pom.xml) 

## Change list &lt;refcodes-servicebus&gt; (version 3.1.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-servicebus/src/refcodes-servicebus-3.1.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-servicebus/src/refcodes-servicebus-3.1.2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`ServiceLookupImpl.java`](https://bitbucket.org/refcodes/refcodes-servicebus/src/refcodes-servicebus-3.1.2/src/main/java/org/refcodes/servicebus/ServiceLookupImpl.java) (see Javadoc at [`ServiceLookupImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-servicebus/3.1.2/org.refcodes.servicebus/org/refcodes/servicebus/ServiceLookupImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`TestServiceDescriptorFactory.java`](https://bitbucket.org/refcodes/refcodes-servicebus/src/refcodes-servicebus-3.1.2/src/test/java/org/refcodes/servicebus/TestServiceDescriptorFactory.java) 

## Change list &lt;refcodes-servicebus-alt&gt; (version 3.1.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-servicebus-alt/src/refcodes-servicebus-alt-3.1.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-servicebus-alt/src/refcodes-servicebus-alt-3.1.2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-servicebus-alt/src/refcodes-servicebus-alt-3.1.2/refcodes-servicebus-alt-spring/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`SpringServiceLookup.java`](https://bitbucket.org/refcodes/refcodes-servicebus-alt/src/refcodes-servicebus-alt-3.1.2/refcodes-servicebus-alt-spring/src/main/java/org/refcodes/servicebus/alt/spring/SpringServiceLookup.java) (see Javadoc at [`SpringServiceLookup.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-servicebus-alt-spring/3.1.2/org.refcodes.servicebus.alt.spring/org/refcodes/servicebus/alt/spring/SpringServiceLookup.html))

## Change list &lt;refcodes-tabular-alt&gt; (version 3.1.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-tabular-alt/src/refcodes-tabular-alt-3.1.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-tabular-alt/src/refcodes-tabular-alt-3.1.2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-tabular-alt/src/refcodes-tabular-alt-3.1.2/refcodes-tabular-alt-forwardsecrecy/pom.xml) 

## Change list &lt;refcodes-archetype&gt; (version 3.1.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype/src/refcodes-archetype-3.1.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype/src/refcodes-archetype-3.1.2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`module-info.java`](https://bitbucket.org/refcodes/refcodes-archetype/src/refcodes-archetype-3.1.2/src/main/java/module-info.java) (see Javadoc at [`module-info.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-archetype/3.1.2/./module-info.html))
* \[<span style="color:green">MODIFIED</span>\] [`CliHelper.java`](https://bitbucket.org/refcodes/refcodes-archetype/src/refcodes-archetype-3.1.2/src/main/java/org/refcodes/archetype/CliHelper.java) (see Javadoc at [`CliHelper.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-archetype/3.1.2/org.refcodes.archetype/org/refcodes/archetype/CliHelper.html))
* \[<span style="color:green">MODIFIED</span>\] [`CtxHelper.java`](https://bitbucket.org/refcodes/refcodes-archetype/src/refcodes-archetype-3.1.2/src/main/java/org/refcodes/archetype/CtxHelper.java) (see Javadoc at [`CtxHelper.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-archetype/3.1.2/org.refcodes.archetype/org/refcodes/archetype/CtxHelper.html))

## Change list &lt;refcodes-archetype-alt&gt; (version 3.1.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.1.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.1.2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.1.2/refcodes-archetype-alt-c2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.1.2/refcodes-archetype-alt-c2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.1.2/refcodes-archetype-alt-c2/src/main/resources/archetype-resources/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`Main.java`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.1.2/refcodes-archetype-alt-c2/src/main/resources/archetype-resources/src/main/java/Main.java) (see Javadoc at [`Main.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-archetype-alt-c2/src/main/resources/archetype-resources/3.1.2/src.main.resources.archetype-resources/Main.html))
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.1.2/refcodes-archetype-alt-cli/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.1.2/refcodes-archetype-alt-cli/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.1.2/refcodes-archetype-alt-cli/src/main/resources/archetype-resources/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`Main.java`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.1.2/refcodes-archetype-alt-cli/src/main/resources/archetype-resources/src/main/java/Main.java) (see Javadoc at [`Main.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-archetype-alt-cli/src/main/resources/archetype-resources/3.1.2/src.main.resources.archetype-resources/Main.html))
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.1.2/refcodes-archetype-alt-csv/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.1.2/refcodes-archetype-alt-csv/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.1.2/refcodes-archetype-alt-csv/src/main/resources/archetype-resources/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`Main.java`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.1.2/refcodes-archetype-alt-csv/src/main/resources/archetype-resources/src/main/java/Main.java) (see Javadoc at [`Main.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-archetype-alt-csv/src/main/resources/archetype-resources/3.1.2/src.main.resources.archetype-resources/Main.html))
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.1.2/refcodes-archetype-alt-decoupling/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.1.2/refcodes-archetype-alt-decoupling/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.1.2/refcodes-archetype-alt-decoupling/src/main/resources/archetype-resources/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`Main.java`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.1.2/refcodes-archetype-alt-decoupling/src/main/resources/archetype-resources/src/main/java/Main.java) (see Javadoc at [`Main.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-archetype-alt-decoupling/src/main/resources/archetype-resources/3.1.2/src.main.resources.archetype-resources/Main.html))
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.1.2/refcodes-archetype-alt-eventbus/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.1.2/refcodes-archetype-alt-eventbus/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.1.2/refcodes-archetype-alt-eventbus/src/main/resources/archetype-resources/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`Main.java`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.1.2/refcodes-archetype-alt-eventbus/src/main/resources/archetype-resources/src/main/java/Main.java) (see Javadoc at [`Main.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-archetype-alt-eventbus/src/main/resources/archetype-resources/3.1.2/src.main.resources.archetype-resources/Main.html))
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.1.2/refcodes-archetype-alt-filter/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.1.2/refcodes-archetype-alt-filter/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.1.2/refcodes-archetype-alt-filter/src/main/resources/archetype-resources/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`Main.java`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.1.2/refcodes-archetype-alt-filter/src/main/resources/archetype-resources/src/main/java/Main.java) (see Javadoc at [`Main.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-archetype-alt-filter/src/main/resources/archetype-resources/3.1.2/src.main.resources.archetype-resources/Main.html))
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.1.2/refcodes-archetype-alt-rest/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.1.2/refcodes-archetype-alt-rest/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.1.2/refcodes-archetype-alt-rest/src/main/resources/archetype-resources/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`Main.java`](https://bitbucket.org/refcodes/refcodes-archetype-alt/src/refcodes-archetype-alt-3.1.2/refcodes-archetype-alt-rest/src/main/resources/archetype-resources/src/main/java/Main.java) (see Javadoc at [`Main.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-archetype-alt-rest/src/main/resources/archetype-resources/3.1.2/src.main.resources.archetype-resources/Main.html))
