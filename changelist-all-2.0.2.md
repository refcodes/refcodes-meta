# Change list for REFCODES.ORG artifacts' version 2.0.2

> This change list has been auto-generated on `triton.local` by `steiner` with `changelist-all.sh` on the 2019-06-10 at 17:19:28.

## Change list &lt;refcodes-licensing&gt; (version 2.0.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-licensing/src/refcodes-licensing-2.0.2/pom.xml) 

## Change list &lt;refcodes-parent&gt; (version 2.0.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-parent/src/refcodes-parent-2.0.2/pom.xml) 

## Change list &lt;refcodes-time&gt; (version 2.0.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-time/src/refcodes-time-2.0.2/pom.xml) 

## Change list &lt;refcodes-mixin&gt; (version 2.0.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-mixin/src/refcodes-mixin-2.0.2/pom.xml) 

## Change list &lt;refcodes-data&gt; (version 2.0.2)

* \[<span style="color:blue">ADDED</span>\] [`CommonParameter.java`](https://bitbucket.org/refcodes/refcodes-data/src/refcodes-data-2.0.2/src/main/java/org/refcodes/data/CommonParameter.java) (see Javadoc at [`CommonParameter.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-data/2.0.2/org/refcodes/data/CommonParameter.html))
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data/src/refcodes-data-2.0.2/pom.xml) 

## Change list &lt;refcodes-exception&gt; (version 2.0.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-exception/src/refcodes-exception-2.0.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`UnmarshalException.java`](https://bitbucket.org/refcodes/refcodes-exception/src/refcodes-exception-2.0.2/src/main/java/org/refcodes/exception/UnmarshalException.java) (see Javadoc at [`UnmarshalException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-exception/2.0.2/org/refcodes/exception/UnmarshalException.html))

## Change list &lt;refcodes-factory&gt; (version 2.0.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-factory/src/refcodes-factory-2.0.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`MarshalTypeFactory.java`](https://bitbucket.org/refcodes/refcodes-factory/src/refcodes-factory-2.0.2/src/main/java/org/refcodes/factory/MarshalTypeFactory.java) (see Javadoc at [`MarshalTypeFactory.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-factory/2.0.2/org/refcodes/factory/MarshalTypeFactory.html))

## Change list &lt;refcodes-factory-alt&gt; (version 2.0.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-factory-alt/src/refcodes-factory-alt-2.0.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-factory-alt/src/refcodes-factory-alt-2.0.2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-factory-alt/src/refcodes-factory-alt-2.0.2/refcodes-factory-alt-spring/pom.xml) 

## Change list &lt;refcodes-controlflow&gt; (version 2.0.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-controlflow/src/refcodes-controlflow-2.0.2/pom.xml) 

## Change list &lt;refcodes-numerical&gt; (version 2.0.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-numerical/src/refcodes-numerical-2.0.2/pom.xml) 

## Change list &lt;refcodes-generator&gt; (version 2.0.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-generator/src/refcodes-generator-2.0.2/pom.xml) 

## Change list &lt;refcodes-matcher&gt; (version 2.0.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-matcher/src/refcodes-matcher-2.0.2/pom.xml) 

## Change list &lt;refcodes-structure&gt; (version 2.0.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-2.0.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-2.0.2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`CanonicalMap.java`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-2.0.2/src/main/java/org/refcodes/structure/CanonicalMap.java) (see Javadoc at [`CanonicalMap.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-structure/2.0.2/org/refcodes/structure/CanonicalMap.html))
* \[<span style="color:green">MODIFIED</span>\] [`PathMap.java`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-2.0.2/src/main/java/org/refcodes/structure/PathMap.java) (see Javadoc at [`PathMap.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-structure/2.0.2/org/refcodes/structure/PathMap.html))
* \[<span style="color:green">MODIFIED</span>\] [`Property.java`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-2.0.2/src/main/java/org/refcodes/structure/Property.java) (see Javadoc at [`Property.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-structure/2.0.2/org/refcodes/structure/Property.html))
* \[<span style="color:green">MODIFIED</span>\] [`TypeUtility.java`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-2.0.2/src/main/java/org/refcodes/structure/TypeUtility.java) (see Javadoc at [`TypeUtility.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-structure/2.0.2/org/refcodes/structure/TypeUtility.html))

## Change list &lt;refcodes-structure-ext&gt; (version 2.0.2)

* \[<span style="color:blue">ADDED</span>\] [`HtmlCanonicalMapFactory.java`](https://bitbucket.org/refcodes/refcodes-structure-ext/src/refcodes-structure-ext-2.0.2/refcodes-structure-ext-factory/src/main/java/org/refcodes/structure/ext/factory/HtmlCanonicalMapFactory.java) (see Javadoc at [`HtmlCanonicalMapFactory.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-structure-ext-factory/2.0.2/org/refcodes/structure/ext/factory/HtmlCanonicalMapFactory.html))
* \[<span style="color:blue">ADDED</span>\] [`HtmlCanonicalMapFactorySingleton.java`](https://bitbucket.org/refcodes/refcodes-structure-ext/src/refcodes-structure-ext-2.0.2/refcodes-structure-ext-factory/src/main/java/org/refcodes/structure/ext/factory/HtmlCanonicalMapFactorySingleton.java) (see Javadoc at [`HtmlCanonicalMapFactorySingleton.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-structure-ext-factory/2.0.2/org/refcodes/structure/ext/factory/HtmlCanonicalMapFactorySingleton.html))
* \[<span style="color:blue">ADDED</span>\] [`AbstractCanonicalMapFactoryTest.java`](https://bitbucket.org/refcodes/refcodes-structure-ext/src/refcodes-structure-ext-2.0.2/refcodes-structure-ext-factory/src/test/java/org/refcodes/structure/ext/factory/AbstractCanonicalMapFactoryTest.java) 
* \[<span style="color:blue">ADDED</span>\] [`HtmlCanonicalMapFactoryTest.java`](https://bitbucket.org/refcodes/refcodes-structure-ext/src/refcodes-structure-ext-2.0.2/refcodes-structure-ext-factory/src/test/java/org/refcodes/structure/ext/factory/HtmlCanonicalMapFactoryTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-structure-ext/src/refcodes-structure-ext-2.0.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-structure-ext/src/refcodes-structure-ext-2.0.2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-structure-ext/src/refcodes-structure-ext-2.0.2/refcodes-structure-ext-factory/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`module-info.java.off`](https://bitbucket.org/refcodes/refcodes-structure-ext/src/refcodes-structure-ext-2.0.2/refcodes-structure-ext-factory/src/main/java/module-info.java.off) 
* \[<span style="color:green">MODIFIED</span>\] [`AbstractCanonicalMapFactory.java`](https://bitbucket.org/refcodes/refcodes-structure-ext/src/refcodes-structure-ext-2.0.2/refcodes-structure-ext-factory/src/main/java/org/refcodes/structure/ext/factory/AbstractCanonicalMapFactory.java) (see Javadoc at [`AbstractCanonicalMapFactory.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-structure-ext-factory/2.0.2/org/refcodes/structure/ext/factory/AbstractCanonicalMapFactory.html))
* \[<span style="color:green">MODIFIED</span>\] [`CanonicalMapFactory.java`](https://bitbucket.org/refcodes/refcodes-structure-ext/src/refcodes-structure-ext-2.0.2/refcodes-structure-ext-factory/src/main/java/org/refcodes/structure/ext/factory/CanonicalMapFactory.java) (see Javadoc at [`CanonicalMapFactory.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-structure-ext-factory/2.0.2/org/refcodes/structure/ext/factory/CanonicalMapFactory.html))
* \[<span style="color:green">MODIFIED</span>\] [`JavaCanonicalMapFactory.java`](https://bitbucket.org/refcodes/refcodes-structure-ext/src/refcodes-structure-ext-2.0.2/refcodes-structure-ext-factory/src/main/java/org/refcodes/structure/ext/factory/JavaCanonicalMapFactory.java) (see Javadoc at [`JavaCanonicalMapFactory.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-structure-ext-factory/2.0.2/org/refcodes/structure/ext/factory/JavaCanonicalMapFactory.html))
* \[<span style="color:green">MODIFIED</span>\] [`JsonCanonicalMapFactory.java`](https://bitbucket.org/refcodes/refcodes-structure-ext/src/refcodes-structure-ext-2.0.2/refcodes-structure-ext-factory/src/main/java/org/refcodes/structure/ext/factory/JsonCanonicalMapFactory.java) (see Javadoc at [`JsonCanonicalMapFactory.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-structure-ext-factory/2.0.2/org/refcodes/structure/ext/factory/JsonCanonicalMapFactory.html))
* \[<span style="color:green">MODIFIED</span>\] [`TomlCanonicalMapFactory.java`](https://bitbucket.org/refcodes/refcodes-structure-ext/src/refcodes-structure-ext-2.0.2/refcodes-structure-ext-factory/src/main/java/org/refcodes/structure/ext/factory/TomlCanonicalMapFactory.java) (see Javadoc at [`TomlCanonicalMapFactory.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-structure-ext-factory/2.0.2/org/refcodes/structure/ext/factory/TomlCanonicalMapFactory.html))
* \[<span style="color:green">MODIFIED</span>\] [`XmlCanonicalMapFactory.java`](https://bitbucket.org/refcodes/refcodes-structure-ext/src/refcodes-structure-ext-2.0.2/refcodes-structure-ext-factory/src/main/java/org/refcodes/structure/ext/factory/XmlCanonicalMapFactory.java) (see Javadoc at [`XmlCanonicalMapFactory.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-structure-ext-factory/2.0.2/org/refcodes/structure/ext/factory/XmlCanonicalMapFactory.html))
* \[<span style="color:green">MODIFIED</span>\] [`YamlCanonicalMapFactory.java`](https://bitbucket.org/refcodes/refcodes-structure-ext/src/refcodes-structure-ext-2.0.2/refcodes-structure-ext-factory/src/main/java/org/refcodes/structure/ext/factory/YamlCanonicalMapFactory.java) (see Javadoc at [`YamlCanonicalMapFactory.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-structure-ext-factory/2.0.2/org/refcodes/structure/ext/factory/YamlCanonicalMapFactory.html))
* \[<span style="color:green">MODIFIED</span>\] [`JavaCanonicalMapFactoryTest.java`](https://bitbucket.org/refcodes/refcodes-structure-ext/src/refcodes-structure-ext-2.0.2/refcodes-structure-ext-factory/src/test/java/org/refcodes/structure/ext/factory/JavaCanonicalMapFactoryTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`JsonCanonicalMapFactoryTest.java`](https://bitbucket.org/refcodes/refcodes-structure-ext/src/refcodes-structure-ext-2.0.2/refcodes-structure-ext-factory/src/test/java/org/refcodes/structure/ext/factory/JsonCanonicalMapFactoryTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`TomlCanonicalMapFactoryTest.java`](https://bitbucket.org/refcodes/refcodes-structure-ext/src/refcodes-structure-ext-2.0.2/refcodes-structure-ext-factory/src/test/java/org/refcodes/structure/ext/factory/TomlCanonicalMapFactoryTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`XmlCanonicalMapFactoryTest.java`](https://bitbucket.org/refcodes/refcodes-structure-ext/src/refcodes-structure-ext-2.0.2/refcodes-structure-ext-factory/src/test/java/org/refcodes/structure/ext/factory/XmlCanonicalMapFactoryTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`YamlCanonicalMapFactoryTest.java`](https://bitbucket.org/refcodes/refcodes-structure-ext/src/refcodes-structure-ext-2.0.2/refcodes-structure-ext-factory/src/test/java/org/refcodes/structure/ext/factory/YamlCanonicalMapFactoryTest.java) 

## Change list &lt;refcodes-runtime&gt; (version 2.0.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-2.0.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-2.0.2/README.md) 

## Change list &lt;refcodes-component&gt; (version 2.0.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-2.0.2/pom.xml) 

## Change list &lt;refcodes-data-ext&gt; (version 2.0.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-2.0.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-2.0.2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-2.0.2/refcodes-data-ext-boulderdash/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-2.0.2/refcodes-data-ext-checkers/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-2.0.2/refcodes-data-ext-checkers/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-2.0.2/refcodes-data-ext-chess/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-2.0.2/refcodes-data-ext-chess/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-2.0.2/refcodes-data-ext-corporate/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-2.0.2/refcodes-data-ext-corporate/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-2.0.2/refcodes-data-ext-symbols/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-2.0.2/refcodes-data-ext-symbols/README.md) 

## Change list &lt;refcodes-graphical&gt; (version 2.0.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-graphical/src/refcodes-graphical-2.0.2/pom.xml) 

## Change list &lt;refcodes-textual&gt; (version 2.0.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-2.0.2/pom.xml) 

## Change list &lt;refcodes-tabular&gt; (version 2.0.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-tabular/src/refcodes-tabular-2.0.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-tabular/src/refcodes-tabular-2.0.2/README.md) 

## Change list &lt;refcodes-observer&gt; (version 2.0.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-observer/src/refcodes-observer-2.0.2/pom.xml) 

## Change list &lt;refcodes-command&gt; (version 2.0.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-command/src/refcodes-command-2.0.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-command/src/refcodes-command-2.0.2/README.md) 

## Change list &lt;refcodes-cli&gt; (version 2.0.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-2.0.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-2.0.2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`Flag.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-2.0.2/src/main/java/org/refcodes/console/Flag.java) (see Javadoc at [`Flag.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/2.0.2/org/refcodes/console/Flag.html))

## Change list &lt;refcodes-cli-ext&gt; (version 2.0.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-cli-ext/src/refcodes-cli-ext-2.0.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-cli-ext/src/refcodes-cli-ext-2.0.2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-cli-ext/src/refcodes-cli-ext-2.0.2/refcodes-cli-ext-archetype/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-cli-ext/src/refcodes-cli-ext-2.0.2/refcodes-cli-ext-archetype/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-cli-ext/src/refcodes-cli-ext-2.0.2/refcodes-cli-ext-archetype/src/main/resources/archetype-resources/README.md) 

## Change list &lt;refcodes-io&gt; (version 2.0.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-io/src/refcodes-io-2.0.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-io/src/refcodes-io-2.0.2/README.md) 

## Change list &lt;refcodes-codec&gt; (version 2.0.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-2.0.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-2.0.2/README.md) 

## Change list &lt;refcodes-component-ext&gt; (version 2.0.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-2.0.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-2.0.2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-2.0.2/refcodes-component-ext-observer/pom.xml) 

## Change list &lt;refcodes-properties&gt; (version 2.0.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-2.0.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-2.0.2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`AbstractResourcePropertiesBuilder.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-2.0.2/src/main/java/org/refcodes/configuration/AbstractResourcePropertiesBuilder.java) (see Javadoc at [`AbstractResourcePropertiesBuilder.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties/2.0.2/org/refcodes/configuration/AbstractResourcePropertiesBuilder.html))
* \[<span style="color:green">MODIFIED</span>\] [`package-info.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-2.0.2/src/main/java/org/refcodes/configuration/package-info.java) 
* \[<span style="color:green">MODIFIED</span>\] [`JavaPropertiesTest.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-2.0.2/src/test/java/org/refcodes/configuration/JavaPropertiesTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`JsonPropertiesTest.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-2.0.2/src/test/java/org/refcodes/configuration/JsonPropertiesTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`XmlPropertiesTest.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-2.0.2/src/test/java/org/refcodes/configuration/XmlPropertiesTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`YamlPropertiesTest.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-2.0.2/src/test/java/org/refcodes/configuration/YamlPropertiesTest.java) 

## Change list &lt;refcodes-security&gt; (version 2.0.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security/src/refcodes-security-2.0.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security/src/refcodes-security-2.0.2/README.md) 

## Change list &lt;refcodes-security-alt&gt; (version 2.0.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-2.0.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-2.0.2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-2.0.2/refcodes-security-alt-chaos/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-2.0.2/refcodes-security-alt-chaos/README.md) 

## Change list &lt;refcodes-security-ext&gt; (version 2.0.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-2.0.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-2.0.2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-2.0.2/refcodes-security-ext-chaos/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-2.0.2/refcodes-security-ext-chaos/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`ChaosProviderImpl.java`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-2.0.2/refcodes-security-ext-chaos/src/main/java/org/refcodes/security/ext/chaos/ChaosProviderImpl.java) (see Javadoc at [`ChaosProviderImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-security-ext-chaos/2.0.2/org/refcodes/security/ext/chaos/ChaosProviderImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-2.0.2/refcodes-security-ext-spring/pom.xml) 

## Change list &lt;refcodes-properties-ext&gt; (version 2.0.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-2.0.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-2.0.2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-2.0.2/refcodes-properties-ext-cli/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-2.0.2/refcodes-properties-ext-cli/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-2.0.2/refcodes-properties-ext-obfuscation/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-2.0.2/refcodes-properties-ext-obfuscation/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-2.0.2/refcodes-properties-ext-observer/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-2.0.2/refcodes-properties-ext-observer/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`PropertyCreatedEvent.java`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-2.0.2/refcodes-properties-ext-observer/src/main/java/org/refcodes/configuration/ext/observer/PropertyCreatedEvent.java) (see Javadoc at [`PropertyCreatedEvent.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties-ext-observer/2.0.2/org/refcodes/configuration/ext/observer/PropertyCreatedEvent.html))
* \[<span style="color:green">MODIFIED</span>\] [`PropertyDeletedEvent.java`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-2.0.2/refcodes-properties-ext-observer/src/main/java/org/refcodes/configuration/ext/observer/PropertyDeletedEvent.java) (see Javadoc at [`PropertyDeletedEvent.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties-ext-observer/2.0.2/org/refcodes/configuration/ext/observer/PropertyDeletedEvent.html))
* \[<span style="color:green">MODIFIED</span>\] [`PropertyUpdatedEvent.java`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-2.0.2/refcodes-properties-ext-observer/src/main/java/org/refcodes/configuration/ext/observer/PropertyUpdatedEvent.java) (see Javadoc at [`PropertyUpdatedEvent.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties-ext-observer/2.0.2/org/refcodes/configuration/ext/observer/PropertyUpdatedEvent.html))
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-2.0.2/refcodes-properties-ext-runtime/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-2.0.2/refcodes-properties-ext-runtime/README.md) 

## Change list &lt;refcodes-logger&gt; (version 2.0.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger/src/refcodes-logger-2.0.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger/src/refcodes-logger-2.0.2/README.md) 

## Change list &lt;refcodes-logger-alt&gt; (version 2.0.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-2.0.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-2.0.2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-2.0.2/refcodes-logger-alt-async/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-2.0.2/refcodes-logger-alt-async/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-2.0.2/refcodes-logger-alt-console/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-2.0.2/refcodes-logger-alt-console/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-2.0.2/refcodes-logger-alt-io/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-2.0.2/refcodes-logger-alt-io/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-2.0.2/refcodes-logger-alt-simpledb/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-2.0.2/refcodes-logger-alt-slf4j/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-2.0.2/refcodes-logger-alt-spring/pom.xml) 

## Change list &lt;refcodes-logger-ext&gt; (version 2.0.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-2.0.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-2.0.2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-2.0.2/refcodes-logger-ext-slf4j/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-2.0.2/refcodes-logger-ext-slf4j/README.md) 

## Change list &lt;refcodes-graphical-ext&gt; (version 2.0.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-2.0.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-2.0.2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-2.0.2/refcodes-graphical-ext-javafx/pom.xml) 

## Change list &lt;refcodes-checkerboard&gt; (version 2.0.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-2.0.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-2.0.2/README.md) 

## Change list &lt;refcodes-checkerboard-alt&gt; (version 2.0.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-alt/src/refcodes-checkerboard-alt-2.0.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-alt/src/refcodes-checkerboard-alt-2.0.2/refcodes-checkerboard-alt-javafx/pom.xml) 

## Change list &lt;refcodes-checkerboard-ext&gt; (version 2.0.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-ext/src/refcodes-checkerboard-ext-2.0.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-checkerboard-ext/src/refcodes-checkerboard-ext-2.0.2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-ext/src/refcodes-checkerboard-ext-2.0.2/refcodes-checkerboard-ext-javafx-boulderdash/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-ext/src/refcodes-checkerboard-ext-2.0.2/refcodes-checkerboard-ext-javafx-chess/pom.xml) 

## Change list &lt;refcodes-boulderdash&gt; (version 2.0.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-boulderdash/src/refcodes-boulderdash-2.0.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-boulderdash/src/refcodes-boulderdash-2.0.2/README.md) 

## Change list &lt;refcodes-net&gt; (version 2.0.2)

* \[<span style="color:blue">ADDED</span>\] [`HtmlMediaTypeFactory.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-2.0.2/src/main/java/org/refcodes/net/HtmlMediaTypeFactory.java) (see Javadoc at [`HtmlMediaTypeFactory.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/2.0.2/org/refcodes/net/HtmlMediaTypeFactory.html))
* \[<span style="color:blue">ADDED</span>\] [`HtmlMediaTypeFactoryTest.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-2.0.2/src/test/java/org/refcodes/net/HtmlMediaTypeFactoryTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-2.0.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`module-info.java.off`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-2.0.2/src/main/java/module-info.java.off) 
* \[<span style="color:green">MODIFIED</span>\] [`HttpClientResponseImpl.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-2.0.2/src/main/java/org/refcodes/net/HttpClientResponseImpl.java) (see Javadoc at [`HttpClientResponseImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/2.0.2/org/refcodes/net/HttpClientResponseImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`HttpServerRequestImpl.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-2.0.2/src/main/java/org/refcodes/net/HttpServerRequestImpl.java) (see Javadoc at [`HttpServerRequestImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/2.0.2/org/refcodes/net/HttpServerRequestImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`JsonMediaTypeFactory.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-2.0.2/src/main/java/org/refcodes/net/JsonMediaTypeFactory.java) (see Javadoc at [`JsonMediaTypeFactory.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/2.0.2/org/refcodes/net/JsonMediaTypeFactory.html))
* \[<span style="color:green">MODIFIED</span>\] [`MediaTypeParameter.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-2.0.2/src/main/java/org/refcodes/net/MediaTypeParameter.java) (see Javadoc at [`MediaTypeParameter.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/2.0.2/org/refcodes/net/MediaTypeParameter.html))
* \[<span style="color:green">MODIFIED</span>\] [`Url.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-2.0.2/src/main/java/org/refcodes/net/Url.java) (see Javadoc at [`Url.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/2.0.2/org/refcodes/net/Url.html))
* \[<span style="color:green">MODIFIED</span>\] [`XmlMediaTypeFactory.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-2.0.2/src/main/java/org/refcodes/net/XmlMediaTypeFactory.java) (see Javadoc at [`XmlMediaTypeFactory.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/2.0.2/org/refcodes/net/XmlMediaTypeFactory.html))
* \[<span style="color:green">MODIFIED</span>\] [`YamlMediaTypeFactory.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-2.0.2/src/main/java/org/refcodes/net/YamlMediaTypeFactory.java) (see Javadoc at [`YamlMediaTypeFactory.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/2.0.2/org/refcodes/net/YamlMediaTypeFactory.html))
* \[<span style="color:green">MODIFIED</span>\] [`AbstractMediaFactoryTest.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-2.0.2/src/test/java/org/refcodes/net/AbstractMediaFactoryTest.java) 

## Change list &lt;refcodes-rest&gt; (version 2.0.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-2.0.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-2.0.2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`AbstractHttpRegistryContextBuilder.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-2.0.2/src/main/java/org/refcodes/rest/AbstractHttpRegistryContextBuilder.java) (see Javadoc at [`AbstractHttpRegistryContextBuilder.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/2.0.2/org/refcodes/rest/AbstractHttpRegistryContextBuilder.html))
* \[<span style="color:green">MODIFIED</span>\] [`AbstractRestClient.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-2.0.2/src/main/java/org/refcodes/rest/AbstractRestClient.java) (see Javadoc at [`AbstractRestClient.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/2.0.2/org/refcodes/rest/AbstractRestClient.html))
* \[<span style="color:green">MODIFIED</span>\] [`HttpRestClientImpl.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-2.0.2/src/main/java/org/refcodes/rest/HttpRestClientImpl.java) (see Javadoc at [`HttpRestClientImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/2.0.2/org/refcodes/rest/HttpRestClientImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`HttpRestClientTest.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-2.0.2/src/test/java/org/refcodes/rest/HttpRestClientTest.java) 

## Change list &lt;refcodes-hal&gt; (version 2.0.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-2.0.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-2.0.2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`HalClientImpl.java`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-2.0.2/src/main/java/org/refcodes/hal/HalClientImpl.java) (see Javadoc at [`HalClientImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-hal/2.0.2/org/refcodes/hal/HalClientImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`HalData.java`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-2.0.2/src/main/java/org/refcodes/hal/HalData.java) (see Javadoc at [`HalData.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-hal/2.0.2/org/refcodes/hal/HalData.html))
* \[<span style="color:green">MODIFIED</span>\] [`HalStruct.java`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-2.0.2/src/main/java/org/refcodes/hal/HalStruct.java) (see Javadoc at [`HalStruct.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-hal/2.0.2/org/refcodes/hal/HalStruct.html))
* \[<span style="color:green">MODIFIED</span>\] [`HalClientTest.java`](https://bitbucket.org/refcodes/refcodes-hal/src/refcodes-hal-2.0.2/src/test/java/org/refcodes/hal/HalClientTest.java) 

## Change list &lt;refcodes-daemon&gt; (version 2.0.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-daemon/src/refcodes-daemon-2.0.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-daemon/src/refcodes-daemon-2.0.2/README.md) 

## Change list &lt;refcodes-eventbus&gt; (version 2.0.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-eventbus/src/refcodes-eventbus-2.0.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-eventbus/src/refcodes-eventbus-2.0.2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`AbstractEventBus.java`](https://bitbucket.org/refcodes/refcodes-eventbus/src/refcodes-eventbus-2.0.2/src/main/java/org/refcodes/eventbus/AbstractEventBus.java) (see Javadoc at [`AbstractEventBus.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-eventbus/2.0.2/org/refcodes/eventbus/AbstractEventBus.html))
* \[<span style="color:green">MODIFIED</span>\] [`SimpleEventBus.java`](https://bitbucket.org/refcodes/refcodes-eventbus/src/refcodes-eventbus-2.0.2/src/main/java/org/refcodes/eventbus/SimpleEventBus.java) (see Javadoc at [`SimpleEventBus.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-eventbus/2.0.2/org/refcodes/eventbus/SimpleEventBus.html))

## Change list &lt;refcodes-eventbus-ext&gt; (version 2.0.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-2.0.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-2.0.2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-2.0.2/refcodes-eventbus-ext-application/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`ApplicationBusImpl.java`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-2.0.2/refcodes-eventbus-ext-application/src/main/java/org/refcodes/eventbus/ext/application/ApplicationBusImpl.java) (see Javadoc at [`ApplicationBusImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-eventbus-ext-application/2.0.2/org/refcodes/eventbus/ext/application/ApplicationBusImpl.html))

## Change list &lt;refcodes-filesystem&gt; (version 2.0.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-filesystem/src/refcodes-filesystem-2.0.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-filesystem/src/refcodes-filesystem-2.0.2/README.md) 

## Change list &lt;refcodes-filesystem-alt&gt; (version 2.0.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-filesystem-alt/src/refcodes-filesystem-alt-2.0.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-filesystem-alt/src/refcodes-filesystem-alt-2.0.2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-filesystem-alt/src/refcodes-filesystem-alt-2.0.2/refcodes-filesystem-alt-s3/pom.xml) 

## Change list &lt;refcodes-forwardsecrecy&gt; (version 2.0.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy/src/refcodes-forwardsecrecy-2.0.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy/src/refcodes-forwardsecrecy-2.0.2/README.md) 

## Change list &lt;refcodes-forwardsecrecy-alt&gt; (version 2.0.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-2.0.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-2.0.2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-2.0.2/refcodes-forwardsecrecy-alt-filesystem/pom.xml) 

## Change list &lt;refcodes-io-ext&gt; (version 2.0.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-2.0.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-2.0.2/refcodes-io-ext-observable/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-2.0.2/refcodes-io-ext-observable/README.md) 

## Change list &lt;refcodes-interceptor&gt; (version 2.0.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-interceptor/src/refcodes-interceptor-2.0.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-interceptor/src/refcodes-interceptor-2.0.2/README.md) 

## Change list &lt;refcodes-jobbus&gt; (version 2.0.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-jobbus/src/refcodes-jobbus-2.0.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-jobbus/src/refcodes-jobbus-2.0.2/README.md) 

## Change list &lt;refcodes-rest-ext&gt; (version 2.0.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-2.0.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-2.0.2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-2.0.2/refcodes-rest-ext-archetype/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-2.0.2/refcodes-rest-ext-archetype/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`build.sh`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-2.0.2/refcodes-rest-ext-archetype/src/main/resources/archetype-resources/build.sh) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-2.0.2/refcodes-rest-ext-archetype/src/main/resources/archetype-resources/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`scriptify.sh`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-2.0.2/refcodes-rest-ext-archetype/src/main/resources/archetype-resources/scriptify.sh) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-2.0.2/refcodes-rest-ext-eureka/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-2.0.2/refcodes-rest-ext-eureka/README.md) 

## Change list &lt;refcodes-remoting&gt; (version 2.0.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-remoting/src/refcodes-remoting-2.0.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-remoting/src/refcodes-remoting-2.0.2/README.md) 

## Change list &lt;refcodes-remoting-ext&gt; (version 2.0.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-2.0.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-2.0.2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-2.0.2/refcodes-remoting-ext-observer/pom.xml) 

## Change list &lt;refcodes-servicebus&gt; (version 2.0.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-servicebus/src/refcodes-servicebus-2.0.2/pom.xml) 

## Change list &lt;refcodes-servicebus-alt&gt; (version 2.0.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-servicebus-alt/src/refcodes-servicebus-alt-2.0.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-servicebus-alt/src/refcodes-servicebus-alt-2.0.2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-servicebus-alt/src/refcodes-servicebus-alt-2.0.2/refcodes-servicebus-alt-spring/pom.xml) 

## Change list &lt;refcodes-tabular-alt&gt; (version 2.0.2)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-tabular-alt/src/refcodes-tabular-alt-2.0.2/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-tabular-alt/src/refcodes-tabular-alt-2.0.2/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-tabular-alt/src/refcodes-tabular-alt-2.0.2/refcodes-tabular-alt-forwardsecrecy/pom.xml) 
