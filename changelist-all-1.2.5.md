# Change list for REFCODES.ORG artifacts' version 1.2.5

> This change list has been auto-generated on `triton` by `steiner` with with `changelist-all.sh` on the 2018-04-12 at 06:16:50.

## Change list &lt;refcodes-licensing&gt; (version 1.2.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-licensing/src/refcodes-licensing-1.2.5/pom.xml) 

## Change list &lt;refcodes-parent&gt; (version 1.2.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-parent/src/refcodes-parent-1.2.5/pom.xml) 

## Change list &lt;refcodes-time&gt; (version 1.2.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-time/src/refcodes-time-1.2.5/pom.xml) 

## Change list &lt;refcodes-mixin&gt; (version 1.2.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-mixin/src/refcodes-mixin-1.2.5/pom.xml) 

## Change list &lt;refcodes-data&gt; (version 1.2.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data/src/refcodes-data-1.2.5/pom.xml) 

## Change list &lt;refcodes-exception&gt; (version 1.2.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-exception/src/refcodes-exception-1.2.5/pom.xml) 

## Change list &lt;refcodes-factory&gt; (version 1.2.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-factory/src/refcodes-factory-1.2.5/pom.xml) 

## Change list &lt;refcodes-factory-alt&gt; (version 1.2.5)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-factory-alt/src/refcodes-factory-alt-1.2.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-factory-alt/src/refcodes-factory-alt-1.2.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-factory-alt/src/refcodes-factory-alt-1.2.5/refcodes-factory-alt-spring/pom.xml) 

## Change list &lt;refcodes-controlflow&gt; (version 1.2.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-controlflow/src/refcodes-controlflow-1.2.5/pom.xml) 

## Change list &lt;refcodes-numerical&gt; (version 1.2.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-numerical/src/refcodes-numerical-1.2.5/pom.xml) 

## Change list &lt;refcodes-generator&gt; (version 1.2.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-generator/src/refcodes-generator-1.2.5/pom.xml) 

## Change list &lt;refcodes-structure&gt; (version 1.2.5)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-1.2.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-structure/src/refcodes-structure-1.2.5/pom.xml) 

## Change list &lt;refcodes-runtime&gt; (version 1.2.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-1.2.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`SystemUtility.java`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-1.2.5/src/main/java/org/refcodes/runtime/SystemUtility.java) (see Javadoc at [`SystemUtility.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-runtime/1.2.5/org/refcodes/runtime/SystemUtility.html))

## Change list &lt;refcodes-component&gt; (version 1.2.5)

* \[<span style="color:blue">ADDED</span>\] [`ConnectionRequest.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-1.2.5/src/main/java/org/refcodes/component/ConnectionRequest.java) (see Javadoc at [`ConnectionRequest.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/1.2.5/org/refcodes/component/ConnectionRequest.html))
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-1.2.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`ComponentRuntimeException.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-1.2.5/src/main/java/org/refcodes/component/ComponentRuntimeException.java) (see Javadoc at [`ComponentRuntimeException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/1.2.5/org/refcodes/component/ComponentRuntimeException.html))

## Change list &lt;refcodes-data-ext&gt; (version 1.2.5)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.2.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.2.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.2.5/refcodes-data-ext-boulderdash/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.2.5/refcodes-data-ext-checkers/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.2.5/refcodes-data-ext-checkers/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.2.5/refcodes-data-ext-chess/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.2.5/refcodes-data-ext-chess/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.2.5/refcodes-data-ext-corporate/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.2.5/refcodes-data-ext-symbols/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.2.5/refcodes-data-ext-symbols/pom.xml) 

## Change list &lt;refcodes-graphical&gt; (version 1.2.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-graphical/src/refcodes-graphical-1.2.5/pom.xml) 

## Change list &lt;refcodes-textual&gt; (version 1.2.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-1.2.5/pom.xml) 

## Change list &lt;refcodes-criteria&gt; (version 1.2.5)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-1.2.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-1.2.5/pom.xml) 

## Change list &lt;refcodes-tabular&gt; (version 1.2.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-tabular/src/refcodes-tabular-1.2.5/pom.xml) 

## Change list &lt;refcodes-logger&gt; (version 1.2.5)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger/src/refcodes-logger-1.2.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger/src/refcodes-logger-1.2.5/pom.xml) 

## Change list &lt;refcodes-logger-alt&gt; (version 1.2.5)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.2.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.2.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.2.5/refcodes-logger-alt-async/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.2.5/refcodes-logger-alt-async/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.2.5/refcodes-logger-alt-console/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.2.5/refcodes-logger-alt-console/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.2.5/refcodes-logger-alt-io/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.2.5/refcodes-logger-alt-io/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.2.5/refcodes-logger-alt-simpledb/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.2.5/refcodes-logger-alt-slf4j/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.2.5/refcodes-logger-alt-spring/pom.xml) 

## Change list &lt;refcodes-graphical-ext&gt; (version 1.2.5)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-1.2.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-1.2.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-1.2.5/refcodes-graphical-ext-javafx/pom.xml) 

## Change list &lt;refcodes-matcher&gt; (version 1.2.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-matcher/src/refcodes-matcher-1.2.5/pom.xml) 

## Change list &lt;refcodes-observer&gt; (version 1.2.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-observer/src/refcodes-observer-1.2.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`AbstractObservable.java`](https://bitbucket.org/refcodes/refcodes-observer/src/refcodes-observer-1.2.5/src/main/java/org/refcodes/observer/AbstractObservable.java) (see Javadoc at [`AbstractObservable.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-observer/1.2.5/org/refcodes/observer/AbstractObservable.html))
* \[<span style="color:green">MODIFIED</span>\] [`EventMetaDataImpl.java`](https://bitbucket.org/refcodes/refcodes-observer/src/refcodes-observer-1.2.5/src/main/java/org/refcodes/observer/EventMetaDataImpl.java) (see Javadoc at [`EventMetaDataImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-observer/1.2.5/org/refcodes/observer/EventMetaDataImpl.html))

## Change list &lt;refcodes-checkerboard&gt; (version 1.2.5)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-1.2.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-1.2.5/pom.xml) 

## Change list &lt;refcodes-checkerboard-alt&gt; (version 1.2.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-alt/src/refcodes-checkerboard-alt-1.2.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-alt/src/refcodes-checkerboard-alt-1.2.5/refcodes-checkerboard-alt-javafx/pom.xml) 

## Change list &lt;refcodes-checkerboard-ext&gt; (version 1.2.5)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-checkerboard-ext/src/refcodes-checkerboard-ext-1.2.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-ext/src/refcodes-checkerboard-ext-1.2.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-ext/src/refcodes-checkerboard-ext-1.2.5/refcodes-checkerboard-ext-javafx-boulderdash/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-ext/src/refcodes-checkerboard-ext-1.2.5/refcodes-checkerboard-ext-javafx-chess/pom.xml) 

## Change list &lt;refcodes-command&gt; (version 1.2.5)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-command/src/refcodes-command-1.2.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-command/src/refcodes-command-1.2.5/pom.xml) 

## Change list &lt;refcodes-cli&gt; (version 1.2.5)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-1.2.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-1.2.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`package-info.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-1.2.5/src/main/java/org/refcodes/console/package-info.java) 

## Change list &lt;refcodes-boulderdash&gt; (version 1.2.5)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-boulderdash/src/refcodes-boulderdash-1.2.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-boulderdash/src/refcodes-boulderdash-1.2.5/pom.xml) 

## Change list &lt;refcodes-io&gt; (version 1.2.5)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-io/src/refcodes-io-1.2.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-io/src/refcodes-io-1.2.5/pom.xml) 

## Change list &lt;refcodes-codec&gt; (version 1.2.5)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-1.2.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-1.2.5/pom.xml) 

## Change list &lt;refcodes-component-ext&gt; (version 1.2.5)

* \[<span style="color:blue">ADDED</span>\] [`GenericCloseEvent.java`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.2.5/refcodes-component-ext-observer/src/main/java/org/refcodes/component/ext/observer/GenericCloseEvent.java) (see Javadoc at [`GenericCloseEvent.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component-ext-observer/1.2.5/org/refcodes/component/ext/observer/GenericCloseEvent.html))
* \[<span style="color:blue">ADDED</span>\] [`GenericClosedEvent.java`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.2.5/refcodes-component-ext-observer/src/main/java/org/refcodes/component/ext/observer/GenericClosedEvent.java) (see Javadoc at [`GenericClosedEvent.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component-ext-observer/1.2.5/org/refcodes/component/ext/observer/GenericClosedEvent.html))
* \[<span style="color:blue">ADDED</span>\] [`GenericDestroyEvent.java`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.2.5/refcodes-component-ext-observer/src/main/java/org/refcodes/component/ext/observer/GenericDestroyEvent.java) (see Javadoc at [`GenericDestroyEvent.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component-ext-observer/1.2.5/org/refcodes/component/ext/observer/GenericDestroyEvent.html))
* \[<span style="color:blue">ADDED</span>\] [`GenericDestroyedEvent.java`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.2.5/refcodes-component-ext-observer/src/main/java/org/refcodes/component/ext/observer/GenericDestroyedEvent.java) (see Javadoc at [`GenericDestroyedEvent.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component-ext-observer/1.2.5/org/refcodes/component/ext/observer/GenericDestroyedEvent.html))
* \[<span style="color:blue">ADDED</span>\] [`GenericInitializeEvent.java`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.2.5/refcodes-component-ext-observer/src/main/java/org/refcodes/component/ext/observer/GenericInitializeEvent.java) (see Javadoc at [`GenericInitializeEvent.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component-ext-observer/1.2.5/org/refcodes/component/ext/observer/GenericInitializeEvent.html))
* \[<span style="color:blue">ADDED</span>\] [`GenericInitializedEvent.java`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.2.5/refcodes-component-ext-observer/src/main/java/org/refcodes/component/ext/observer/GenericInitializedEvent.java) (see Javadoc at [`GenericInitializedEvent.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component-ext-observer/1.2.5/org/refcodes/component/ext/observer/GenericInitializedEvent.html))
* \[<span style="color:blue">ADDED</span>\] [`GenericLifeCycleRequestObserver.java`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.2.5/refcodes-component-ext-observer/src/main/java/org/refcodes/component/ext/observer/GenericLifeCycleRequestObserver.java) (see Javadoc at [`GenericLifeCycleRequestObserver.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component-ext-observer/1.2.5/org/refcodes/component/ext/observer/GenericLifeCycleRequestObserver.html))
* \[<span style="color:blue">ADDED</span>\] [`GenericLifeCycleStatusObserver.java`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.2.5/refcodes-component-ext-observer/src/main/java/org/refcodes/component/ext/observer/GenericLifeCycleStatusObserver.java) (see Javadoc at [`GenericLifeCycleStatusObserver.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component-ext-observer/1.2.5/org/refcodes/component/ext/observer/GenericLifeCycleStatusObserver.html))
* \[<span style="color:blue">ADDED</span>\] [`GenericOpenEvent.java`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.2.5/refcodes-component-ext-observer/src/main/java/org/refcodes/component/ext/observer/GenericOpenEvent.java) (see Javadoc at [`GenericOpenEvent.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component-ext-observer/1.2.5/org/refcodes/component/ext/observer/GenericOpenEvent.html))
* \[<span style="color:blue">ADDED</span>\] [`GenericOpenedEvent.java`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.2.5/refcodes-component-ext-observer/src/main/java/org/refcodes/component/ext/observer/GenericOpenedEvent.java) (see Javadoc at [`GenericOpenedEvent.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component-ext-observer/1.2.5/org/refcodes/component/ext/observer/GenericOpenedEvent.html))
* \[<span style="color:blue">ADDED</span>\] [`GenericPauseEvent.java`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.2.5/refcodes-component-ext-observer/src/main/java/org/refcodes/component/ext/observer/GenericPauseEvent.java) (see Javadoc at [`GenericPauseEvent.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component-ext-observer/1.2.5/org/refcodes/component/ext/observer/GenericPauseEvent.html))
* \[<span style="color:blue">ADDED</span>\] [`GenericPausedEvent.java`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.2.5/refcodes-component-ext-observer/src/main/java/org/refcodes/component/ext/observer/GenericPausedEvent.java) (see Javadoc at [`GenericPausedEvent.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component-ext-observer/1.2.5/org/refcodes/component/ext/observer/GenericPausedEvent.html))
* \[<span style="color:blue">ADDED</span>\] [`GenericResumeEvent.java`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.2.5/refcodes-component-ext-observer/src/main/java/org/refcodes/component/ext/observer/GenericResumeEvent.java) (see Javadoc at [`GenericResumeEvent.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component-ext-observer/1.2.5/org/refcodes/component/ext/observer/GenericResumeEvent.html))
* \[<span style="color:blue">ADDED</span>\] [`GenericResumedEvent.java`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.2.5/refcodes-component-ext-observer/src/main/java/org/refcodes/component/ext/observer/GenericResumedEvent.java) (see Javadoc at [`GenericResumedEvent.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component-ext-observer/1.2.5/org/refcodes/component/ext/observer/GenericResumedEvent.html))
* \[<span style="color:blue">ADDED</span>\] [`GenericStartEvent.java`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.2.5/refcodes-component-ext-observer/src/main/java/org/refcodes/component/ext/observer/GenericStartEvent.java) (see Javadoc at [`GenericStartEvent.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component-ext-observer/1.2.5/org/refcodes/component/ext/observer/GenericStartEvent.html))
* \[<span style="color:blue">ADDED</span>\] [`GenericStartedEvent.java`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.2.5/refcodes-component-ext-observer/src/main/java/org/refcodes/component/ext/observer/GenericStartedEvent.java) (see Javadoc at [`GenericStartedEvent.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component-ext-observer/1.2.5/org/refcodes/component/ext/observer/GenericStartedEvent.html))
* \[<span style="color:blue">ADDED</span>\] [`GenericStopEvent.java`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.2.5/refcodes-component-ext-observer/src/main/java/org/refcodes/component/ext/observer/GenericStopEvent.java) (see Javadoc at [`GenericStopEvent.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component-ext-observer/1.2.5/org/refcodes/component/ext/observer/GenericStopEvent.html))
* \[<span style="color:blue">ADDED</span>\] [`GenericStoppedEvent.java`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.2.5/refcodes-component-ext-observer/src/main/java/org/refcodes/component/ext/observer/GenericStoppedEvent.java) (see Javadoc at [`GenericStoppedEvent.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component-ext-observer/1.2.5/org/refcodes/component/ext/observer/GenericStoppedEvent.html))
* \[<span style="color:blue">ADDED</span>\] [`LifeCycleRequestObserver.java`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.2.5/refcodes-component-ext-observer/src/main/java/org/refcodes/component/ext/observer/LifeCycleRequestObserver.java) (see Javadoc at [`LifeCycleRequestObserver.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component-ext-observer/1.2.5/org/refcodes/component/ext/observer/LifeCycleRequestObserver.html))
* \[<span style="color:blue">ADDED</span>\] [`ObservableLifeCycleRequestAutomaton.java`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.2.5/refcodes-component-ext-observer/src/main/java/org/refcodes/component/ext/observer/ObservableLifeCycleRequestAutomaton.java) (see Javadoc at [`ObservableLifeCycleRequestAutomaton.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component-ext-observer/1.2.5/org/refcodes/component/ext/observer/ObservableLifeCycleRequestAutomaton.html))
* \[<span style="color:red">DELETED</span>\] `LifeCycleObserver.java`
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.2.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.2.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.2.5/refcodes-component-ext-observer/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`AbstractConnectionRequestEvent.java`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.2.5/refcodes-component-ext-observer/src/main/java/org/refcodes/component/ext/observer/AbstractConnectionRequestEvent.java) (see Javadoc at [`AbstractConnectionRequestEvent.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component-ext-observer/1.2.5/org/refcodes/component/ext/observer/AbstractConnectionRequestEvent.html))
* \[<span style="color:green">MODIFIED</span>\] [`AbstractConnectionStatusEvent.java`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.2.5/refcodes-component-ext-observer/src/main/java/org/refcodes/component/ext/observer/AbstractConnectionStatusEvent.java) (see Javadoc at [`AbstractConnectionStatusEvent.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component-ext-observer/1.2.5/org/refcodes/component/ext/observer/AbstractConnectionStatusEvent.html))
* \[<span style="color:green">MODIFIED</span>\] [`AbstractLifeCycleRequestEvent.java`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.2.5/refcodes-component-ext-observer/src/main/java/org/refcodes/component/ext/observer/AbstractLifeCycleRequestEvent.java) (see Javadoc at [`AbstractLifeCycleRequestEvent.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component-ext-observer/1.2.5/org/refcodes/component/ext/observer/AbstractLifeCycleRequestEvent.html))
* \[<span style="color:green">MODIFIED</span>\] [`AbstractLifeCycleStatusEvent.java`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.2.5/refcodes-component-ext-observer/src/main/java/org/refcodes/component/ext/observer/AbstractLifeCycleStatusEvent.java) (see Javadoc at [`AbstractLifeCycleStatusEvent.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component-ext-observer/1.2.5/org/refcodes/component/ext/observer/AbstractLifeCycleStatusEvent.html))
* \[<span style="color:green">MODIFIED</span>\] [`AbstractNetworkRequestEvent.java`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.2.5/refcodes-component-ext-observer/src/main/java/org/refcodes/component/ext/observer/AbstractNetworkRequestEvent.java) (see Javadoc at [`AbstractNetworkRequestEvent.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component-ext-observer/1.2.5/org/refcodes/component/ext/observer/AbstractNetworkRequestEvent.html))
* \[<span style="color:green">MODIFIED</span>\] [`AbstractNetworkStatusEvent.java`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.2.5/refcodes-component-ext-observer/src/main/java/org/refcodes/component/ext/observer/AbstractNetworkStatusEvent.java) (see Javadoc at [`AbstractNetworkStatusEvent.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component-ext-observer/1.2.5/org/refcodes/component/ext/observer/AbstractNetworkStatusEvent.html))
* \[<span style="color:green">MODIFIED</span>\] [`CloseEvent.java`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.2.5/refcodes-component-ext-observer/src/main/java/org/refcodes/component/ext/observer/CloseEvent.java) (see Javadoc at [`CloseEvent.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component-ext-observer/1.2.5/org/refcodes/component/ext/observer/CloseEvent.html))
* \[<span style="color:green">MODIFIED</span>\] [`ClosedEvent.java`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.2.5/refcodes-component-ext-observer/src/main/java/org/refcodes/component/ext/observer/ClosedEvent.java) (see Javadoc at [`ClosedEvent.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component-ext-observer/1.2.5/org/refcodes/component/ext/observer/ClosedEvent.html))
* \[<span style="color:green">MODIFIED</span>\] [`ConnectionRequestAccessor.java`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.2.5/refcodes-component-ext-observer/src/main/java/org/refcodes/component/ext/observer/ConnectionRequestAccessor.java) (see Javadoc at [`ConnectionRequestAccessor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component-ext-observer/1.2.5/org/refcodes/component/ext/observer/ConnectionRequestAccessor.html))
* \[<span style="color:green">MODIFIED</span>\] [`ConnectionRequestEvent.java`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.2.5/refcodes-component-ext-observer/src/main/java/org/refcodes/component/ext/observer/ConnectionRequestEvent.java) (see Javadoc at [`ConnectionRequestEvent.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component-ext-observer/1.2.5/org/refcodes/component/ext/observer/ConnectionRequestEvent.html))
* \[<span style="color:green">MODIFIED</span>\] [`ConnectionStatusEvent.java`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.2.5/refcodes-component-ext-observer/src/main/java/org/refcodes/component/ext/observer/ConnectionStatusEvent.java) (see Javadoc at [`ConnectionStatusEvent.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component-ext-observer/1.2.5/org/refcodes/component/ext/observer/ConnectionStatusEvent.html))
* \[<span style="color:green">MODIFIED</span>\] [`DestroyEvent.java`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.2.5/refcodes-component-ext-observer/src/main/java/org/refcodes/component/ext/observer/DestroyEvent.java) (see Javadoc at [`DestroyEvent.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component-ext-observer/1.2.5/org/refcodes/component/ext/observer/DestroyEvent.html))
* \[<span style="color:green">MODIFIED</span>\] [`DestroyedEvent.java`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.2.5/refcodes-component-ext-observer/src/main/java/org/refcodes/component/ext/observer/DestroyedEvent.java) (see Javadoc at [`DestroyedEvent.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component-ext-observer/1.2.5/org/refcodes/component/ext/observer/DestroyedEvent.html))
* \[<span style="color:green">MODIFIED</span>\] [`GenericConnectionRequestEvent.java`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.2.5/refcodes-component-ext-observer/src/main/java/org/refcodes/component/ext/observer/GenericConnectionRequestEvent.java) (see Javadoc at [`GenericConnectionRequestEvent.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component-ext-observer/1.2.5/org/refcodes/component/ext/observer/GenericConnectionRequestEvent.html))
* \[<span style="color:green">MODIFIED</span>\] [`GenericConnectionStatusEvent.java`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.2.5/refcodes-component-ext-observer/src/main/java/org/refcodes/component/ext/observer/GenericConnectionStatusEvent.java) (see Javadoc at [`GenericConnectionStatusEvent.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component-ext-observer/1.2.5/org/refcodes/component/ext/observer/GenericConnectionStatusEvent.html))
* \[<span style="color:green">MODIFIED</span>\] [`GenericLifeCycleRequestEvent.java`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.2.5/refcodes-component-ext-observer/src/main/java/org/refcodes/component/ext/observer/GenericLifeCycleRequestEvent.java) (see Javadoc at [`GenericLifeCycleRequestEvent.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component-ext-observer/1.2.5/org/refcodes/component/ext/observer/GenericLifeCycleRequestEvent.html))
* \[<span style="color:green">MODIFIED</span>\] [`GenericLifeCycleStatusEvent.java`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.2.5/refcodes-component-ext-observer/src/main/java/org/refcodes/component/ext/observer/GenericLifeCycleStatusEvent.java) (see Javadoc at [`GenericLifeCycleStatusEvent.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component-ext-observer/1.2.5/org/refcodes/component/ext/observer/GenericLifeCycleStatusEvent.html))
* \[<span style="color:green">MODIFIED</span>\] [`GenericNetworkRequestEvent.java`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.2.5/refcodes-component-ext-observer/src/main/java/org/refcodes/component/ext/observer/GenericNetworkRequestEvent.java) (see Javadoc at [`GenericNetworkRequestEvent.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component-ext-observer/1.2.5/org/refcodes/component/ext/observer/GenericNetworkRequestEvent.html))
* \[<span style="color:green">MODIFIED</span>\] [`GenericNetworkStatusEvent.java`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.2.5/refcodes-component-ext-observer/src/main/java/org/refcodes/component/ext/observer/GenericNetworkStatusEvent.java) (see Javadoc at [`GenericNetworkStatusEvent.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component-ext-observer/1.2.5/org/refcodes/component/ext/observer/GenericNetworkStatusEvent.html))
* \[<span style="color:green">MODIFIED</span>\] [`InitializeEvent.java`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.2.5/refcodes-component-ext-observer/src/main/java/org/refcodes/component/ext/observer/InitializeEvent.java) (see Javadoc at [`InitializeEvent.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component-ext-observer/1.2.5/org/refcodes/component/ext/observer/InitializeEvent.html))
* \[<span style="color:green">MODIFIED</span>\] [`InitializedEvent.java`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.2.5/refcodes-component-ext-observer/src/main/java/org/refcodes/component/ext/observer/InitializedEvent.java) (see Javadoc at [`InitializedEvent.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component-ext-observer/1.2.5/org/refcodes/component/ext/observer/InitializedEvent.html))
* \[<span style="color:green">MODIFIED</span>\] [`NetworkRequestEvent.java`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.2.5/refcodes-component-ext-observer/src/main/java/org/refcodes/component/ext/observer/NetworkRequestEvent.java) (see Javadoc at [`NetworkRequestEvent.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component-ext-observer/1.2.5/org/refcodes/component/ext/observer/NetworkRequestEvent.html))
* \[<span style="color:green">MODIFIED</span>\] [`NetworkStatusEvent.java`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.2.5/refcodes-component-ext-observer/src/main/java/org/refcodes/component/ext/observer/NetworkStatusEvent.java) (see Javadoc at [`NetworkStatusEvent.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component-ext-observer/1.2.5/org/refcodes/component/ext/observer/NetworkStatusEvent.html))
* \[<span style="color:green">MODIFIED</span>\] [`ObservableLifeCycleAutomatonAccessor.java`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.2.5/refcodes-component-ext-observer/src/main/java/org/refcodes/component/ext/observer/ObservableLifeCycleAutomatonAccessor.java) (see Javadoc at [`ObservableLifeCycleAutomatonAccessor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component-ext-observer/1.2.5/org/refcodes/component/ext/observer/ObservableLifeCycleAutomatonAccessor.html))
* \[<span style="color:green">MODIFIED</span>\] [`ObservableLifeCycleRequestAutomatonImpl.java`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.2.5/refcodes-component-ext-observer/src/main/java/org/refcodes/component/ext/observer/ObservableLifeCycleRequestAutomatonImpl.java) (see Javadoc at [`ObservableLifeCycleRequestAutomatonImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component-ext-observer/1.2.5/org/refcodes/component/ext/observer/ObservableLifeCycleRequestAutomatonImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`OpenEvent.java`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.2.5/refcodes-component-ext-observer/src/main/java/org/refcodes/component/ext/observer/OpenEvent.java) (see Javadoc at [`OpenEvent.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component-ext-observer/1.2.5/org/refcodes/component/ext/observer/OpenEvent.html))
* \[<span style="color:green">MODIFIED</span>\] [`OpenedEvent.java`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.2.5/refcodes-component-ext-observer/src/main/java/org/refcodes/component/ext/observer/OpenedEvent.java) (see Javadoc at [`OpenedEvent.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component-ext-observer/1.2.5/org/refcodes/component/ext/observer/OpenedEvent.html))
* \[<span style="color:green">MODIFIED</span>\] [`PauseEvent.java`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.2.5/refcodes-component-ext-observer/src/main/java/org/refcodes/component/ext/observer/PauseEvent.java) (see Javadoc at [`PauseEvent.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component-ext-observer/1.2.5/org/refcodes/component/ext/observer/PauseEvent.html))
* \[<span style="color:green">MODIFIED</span>\] [`PausedEvent.java`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.2.5/refcodes-component-ext-observer/src/main/java/org/refcodes/component/ext/observer/PausedEvent.java) (see Javadoc at [`PausedEvent.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component-ext-observer/1.2.5/org/refcodes/component/ext/observer/PausedEvent.html))
* \[<span style="color:green">MODIFIED</span>\] [`ResumeEvent.java`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.2.5/refcodes-component-ext-observer/src/main/java/org/refcodes/component/ext/observer/ResumeEvent.java) (see Javadoc at [`ResumeEvent.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component-ext-observer/1.2.5/org/refcodes/component/ext/observer/ResumeEvent.html))
* \[<span style="color:green">MODIFIED</span>\] [`ResumedEvent.java`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.2.5/refcodes-component-ext-observer/src/main/java/org/refcodes/component/ext/observer/ResumedEvent.java) (see Javadoc at [`ResumedEvent.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component-ext-observer/1.2.5/org/refcodes/component/ext/observer/ResumedEvent.html))
* \[<span style="color:green">MODIFIED</span>\] [`StartEvent.java`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.2.5/refcodes-component-ext-observer/src/main/java/org/refcodes/component/ext/observer/StartEvent.java) (see Javadoc at [`StartEvent.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component-ext-observer/1.2.5/org/refcodes/component/ext/observer/StartEvent.html))
* \[<span style="color:green">MODIFIED</span>\] [`StartedEvent.java`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.2.5/refcodes-component-ext-observer/src/main/java/org/refcodes/component/ext/observer/StartedEvent.java) (see Javadoc at [`StartedEvent.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component-ext-observer/1.2.5/org/refcodes/component/ext/observer/StartedEvent.html))
* \[<span style="color:green">MODIFIED</span>\] [`StopEvent.java`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.2.5/refcodes-component-ext-observer/src/main/java/org/refcodes/component/ext/observer/StopEvent.java) (see Javadoc at [`StopEvent.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component-ext-observer/1.2.5/org/refcodes/component/ext/observer/StopEvent.html))
* \[<span style="color:green">MODIFIED</span>\] [`StoppedEvent.java`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.2.5/refcodes-component-ext-observer/src/main/java/org/refcodes/component/ext/observer/StoppedEvent.java) (see Javadoc at [`StoppedEvent.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component-ext-observer/1.2.5/org/refcodes/component/ext/observer/StoppedEvent.html))
* \[<span style="color:green">MODIFIED</span>\] [`ObservableLifeCycleAutomatonTest.java`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.2.5/refcodes-component-ext-observer/src/test/java/org/refcodes/component/ext/observer/ObservableLifeCycleAutomatonTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`ObservableLifeCycleRequestAutomatonTest.java`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.2.5/refcodes-component-ext-observer/src/test/java/org/refcodes/component/ext/observer/ObservableLifeCycleRequestAutomatonTest.java) 

## Change list &lt;refcodes-properties&gt; (version 1.2.5)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.2.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.2.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`package-info.java`](https://bitbucket.org/refcodes/refcodes-properties/src/refcodes-properties-1.2.5/src/main/java/org/refcodes/configuration/package-info.java) 

## Change list &lt;refcodes-properties-ext&gt; (version 1.2.5)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.2.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.2.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.2.5/refcodes-properties-ext-cli/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.2.5/refcodes-properties-ext-cli/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.2.5/refcodes-properties-ext-observer/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.2.5/refcodes-properties-ext-observer/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.2.5/refcodes-properties-ext-runtime/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-properties-ext/src/refcodes-properties-ext-1.2.5/refcodes-properties-ext-runtime/pom.xml) 

## Change list &lt;refcodes-security&gt; (version 1.2.5)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security/src/refcodes-security-1.2.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security/src/refcodes-security-1.2.5/pom.xml) 

## Change list &lt;refcodes-net&gt; (version 1.2.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.2.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`IpAddress.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.2.5/src/main/java/org/refcodes/net/IpAddress.java) (see Javadoc at [`IpAddress.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/1.2.5/org/refcodes/net/IpAddress.html))
* \[<span style="color:green">MODIFIED</span>\] [`IpAddressAccessor.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.2.5/src/main/java/org/refcodes/net/IpAddressAccessor.java) (see Javadoc at [`IpAddressAccessor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/1.2.5/org/refcodes/net/IpAddressAccessor.html))
* \[<span style="color:green">MODIFIED</span>\] [`UrlImpl.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.2.5/src/main/java/org/refcodes/net/UrlImpl.java) (see Javadoc at [`UrlImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-net/1.2.5/org/refcodes/net/UrlImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`IpAddressTest.java`](https://bitbucket.org/refcodes/refcodes-net/src/refcodes-net-1.2.5/src/test/java/org/refcodes/net/IpAddressTest.java) 

## Change list &lt;refcodes-rest&gt; (version 1.2.5)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.2.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.2.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`AbstractHttpRegistrySidecar.java`](https://bitbucket.org/refcodes/refcodes-rest/src/refcodes-rest-1.2.5/src/main/java/org/refcodes/rest/AbstractHttpRegistrySidecar.java) (see Javadoc at [`AbstractHttpRegistrySidecar.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest/1.2.5/org/refcodes/rest/AbstractHttpRegistrySidecar.html))

## Change list &lt;refcodes-logger-ext&gt; (version 1.2.5)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-1.2.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-1.2.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-1.2.5/refcodes-logger-ext-slf4j/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-1.2.5/refcodes-logger-ext-slf4j/pom.xml) 

## Change list &lt;refcodes-daemon&gt; (version 1.2.5)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-daemon/src/refcodes-daemon-1.2.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-daemon/src/refcodes-daemon-1.2.5/pom.xml) 

## Change list &lt;refcodes-eventbus&gt; (version 1.2.5)

* \[<span style="color:red">DELETED</span>\] `BusObservableImpl.java`
* \[<span style="color:red">DELETED</span>\] `BusPublisherImpl.java`
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-eventbus/src/refcodes-eventbus-1.2.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-eventbus/src/refcodes-eventbus-1.2.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`AbstractEventBus.java`](https://bitbucket.org/refcodes/refcodes-eventbus/src/refcodes-eventbus-1.2.5/src/main/java/org/refcodes/eventbus/AbstractEventBus.java) (see Javadoc at [`AbstractEventBus.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-eventbus/1.2.5/org/refcodes/eventbus/AbstractEventBus.html))
* \[<span style="color:green">MODIFIED</span>\] [`BusEventBuilderImpl.java`](https://bitbucket.org/refcodes/refcodes-eventbus/src/refcodes-eventbus-1.2.5/src/main/java/org/refcodes/eventbus/BusEventBuilderImpl.java) (see Javadoc at [`BusEventBuilderImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-eventbus/1.2.5/org/refcodes/eventbus/BusEventBuilderImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`BusEventImpl.java`](https://bitbucket.org/refcodes/refcodes-eventbus/src/refcodes-eventbus-1.2.5/src/main/java/org/refcodes/eventbus/BusEventImpl.java) (see Javadoc at [`BusEventImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-eventbus/1.2.5/org/refcodes/eventbus/BusEventImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`BusMatcherSugar.java`](https://bitbucket.org/refcodes/refcodes-eventbus/src/refcodes-eventbus-1.2.5/src/main/java/org/refcodes/eventbus/BusMatcherSugar.java) (see Javadoc at [`BusMatcherSugar.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-eventbus/1.2.5/org/refcodes/eventbus/BusMatcherSugar.html))
* \[<span style="color:green">MODIFIED</span>\] [`EventBus.java`](https://bitbucket.org/refcodes/refcodes-eventbus/src/refcodes-eventbus-1.2.5/src/main/java/org/refcodes/eventbus/EventBus.java) (see Javadoc at [`EventBus.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-eventbus/1.2.5/org/refcodes/eventbus/EventBus.html))
* \[<span style="color:green">MODIFIED</span>\] [`SimpleEventBus.java`](https://bitbucket.org/refcodes/refcodes-eventbus/src/refcodes-eventbus-1.2.5/src/main/java/org/refcodes/eventbus/SimpleEventBus.java) (see Javadoc at [`SimpleEventBus.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-eventbus/1.2.5/org/refcodes/eventbus/SimpleEventBus.html))
* \[<span style="color:green">MODIFIED</span>\] [`GenericBusEvent.java`](https://bitbucket.org/refcodes/refcodes-eventbus/src/refcodes-eventbus-1.2.5/src/main/java/org/refcodes/eventbus/GenericBusEvent.java) (see Javadoc at [`GenericBusEvent.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-eventbus/1.2.5/org/refcodes/eventbus/GenericBusEvent.html))
* \[<span style="color:green">MODIFIED</span>\] [`GenericEventBus.java`](https://bitbucket.org/refcodes/refcodes-eventbus/src/refcodes-eventbus-1.2.5/src/main/java/org/refcodes/eventbus/GenericEventBus.java) (see Javadoc at [`GenericEventBus.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-eventbus/1.2.5/org/refcodes/eventbus/GenericEventBus.html))

## Change list &lt;refcodes-eventbus-ext&gt; (version 1.2.5)

* \[<span style="color:blue">ADDED</span>\] [`ApplicationEvent.java`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-1.2.5/refcodes-eventbus-ext-application/src/main/java/org/refcodes/eventbus/ext/application/ApplicationEvent.java) (see Javadoc at [`ApplicationEvent.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-eventbus-ext-application/1.2.5/org/refcodes/eventbus/ext/application/ApplicationEvent.html))
* \[<span style="color:blue">ADDED</span>\] [`ApplicationEventBuilderImpl.java`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-1.2.5/refcodes-eventbus-ext-application/src/main/java/org/refcodes/eventbus/ext/application/ApplicationEventBuilderImpl.java) (see Javadoc at [`ApplicationEventBuilderImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-eventbus-ext-application/1.2.5/org/refcodes/eventbus/ext/application/ApplicationEventBuilderImpl.html))
* \[<span style="color:blue">ADDED</span>\] [`ApplicationEventImpl.java`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-1.2.5/refcodes-eventbus-ext-application/src/main/java/org/refcodes/eventbus/ext/application/ApplicationEventImpl.java) (see Javadoc at [`ApplicationEventImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-eventbus-ext-application/1.2.5/org/refcodes/eventbus/ext/application/ApplicationEventImpl.html))
* \[<span style="color:blue">ADDED</span>\] [`ApplicationMatcher.java`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-1.2.5/refcodes-eventbus-ext-application/src/main/java/org/refcodes/eventbus/ext/application/ApplicationMatcher.java) (see Javadoc at [`ApplicationMatcher.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-eventbus-ext-application/1.2.5/org/refcodes/eventbus/ext/application/ApplicationMatcher.html))
* \[<span style="color:blue">ADDED</span>\] [`ApplicationMatcherSugar.java`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-1.2.5/refcodes-eventbus-ext-application/src/main/java/org/refcodes/eventbus/ext/application/ApplicationMatcherSugar.java) (see Javadoc at [`ApplicationMatcherSugar.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-eventbus-ext-application/1.2.5/org/refcodes/eventbus/ext/application/ApplicationMatcherSugar.html))
* \[<span style="color:blue">ADDED</span>\] [`ApplicationObserver.java`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-1.2.5/refcodes-eventbus-ext-application/src/main/java/org/refcodes/eventbus/ext/application/ApplicationObserver.java) (see Javadoc at [`ApplicationObserver.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-eventbus-ext-application/1.2.5/org/refcodes/eventbus/ext/application/ApplicationObserver.html))
* \[<span style="color:blue">ADDED</span>\] [`DestroyBus.java`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-1.2.5/refcodes-eventbus-ext-application/src/main/java/org/refcodes/eventbus/ext/application/DestroyBus.java) (see Javadoc at [`DestroyBus.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-eventbus-ext-application/1.2.5/org/refcodes/eventbus/ext/application/DestroyBus.html))
* \[<span style="color:blue">ADDED</span>\] [`DestroyBusEvent.java`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-1.2.5/refcodes-eventbus-ext-application/src/main/java/org/refcodes/eventbus/ext/application/DestroyBusEvent.java) (see Javadoc at [`DestroyBusEvent.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-eventbus-ext-application/1.2.5/org/refcodes/eventbus/ext/application/DestroyBusEvent.html))
* \[<span style="color:blue">ADDED</span>\] [`ExceptionBus.java`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-1.2.5/refcodes-eventbus-ext-application/src/main/java/org/refcodes/eventbus/ext/application/ExceptionBus.java) (see Javadoc at [`ExceptionBus.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-eventbus-ext-application/1.2.5/org/refcodes/eventbus/ext/application/ExceptionBus.html))
* \[<span style="color:blue">ADDED</span>\] [`InitializeBus.java`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-1.2.5/refcodes-eventbus-ext-application/src/main/java/org/refcodes/eventbus/ext/application/InitializeBus.java) (see Javadoc at [`InitializeBus.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-eventbus-ext-application/1.2.5/org/refcodes/eventbus/ext/application/InitializeBus.html))
* \[<span style="color:blue">ADDED</span>\] [`InitializeBusEvent.java`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-1.2.5/refcodes-eventbus-ext-application/src/main/java/org/refcodes/eventbus/ext/application/InitializeBusEvent.java) (see Javadoc at [`InitializeBusEvent.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-eventbus-ext-application/1.2.5/org/refcodes/eventbus/ext/application/InitializeBusEvent.html))
* \[<span style="color:blue">ADDED</span>\] [`LifeCycleBus.java`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-1.2.5/refcodes-eventbus-ext-application/src/main/java/org/refcodes/eventbus/ext/application/LifeCycleBus.java) (see Javadoc at [`LifeCycleBus.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-eventbus-ext-application/1.2.5/org/refcodes/eventbus/ext/application/LifeCycleBus.html))
* \[<span style="color:blue">ADDED</span>\] [`LifeCycleBusEvent.java`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-1.2.5/refcodes-eventbus-ext-application/src/main/java/org/refcodes/eventbus/ext/application/LifeCycleBusEvent.java) (see Javadoc at [`LifeCycleBusEvent.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-eventbus-ext-application/1.2.5/org/refcodes/eventbus/ext/application/LifeCycleBusEvent.html))
* \[<span style="color:blue">ADDED</span>\] [`LifeCycleBusObserver.java`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-1.2.5/refcodes-eventbus-ext-application/src/main/java/org/refcodes/eventbus/ext/application/LifeCycleBusObserver.java) (see Javadoc at [`LifeCycleBusObserver.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-eventbus-ext-application/1.2.5/org/refcodes/eventbus/ext/application/LifeCycleBusObserver.html))
* \[<span style="color:blue">ADDED</span>\] [`MessageBus.java`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-1.2.5/refcodes-eventbus-ext-application/src/main/java/org/refcodes/eventbus/ext/application/MessageBus.java) (see Javadoc at [`MessageBus.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-eventbus-ext-application/1.2.5/org/refcodes/eventbus/ext/application/MessageBus.html))
* \[<span style="color:blue">ADDED</span>\] [`MessagePropertiesBus.java`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-1.2.5/refcodes-eventbus-ext-application/src/main/java/org/refcodes/eventbus/ext/application/MessagePropertiesBus.java) (see Javadoc at [`MessagePropertiesBus.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-eventbus-ext-application/1.2.5/org/refcodes/eventbus/ext/application/MessagePropertiesBus.html))
* \[<span style="color:blue">ADDED</span>\] [`PauseBus.java`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-1.2.5/refcodes-eventbus-ext-application/src/main/java/org/refcodes/eventbus/ext/application/PauseBus.java) (see Javadoc at [`PauseBus.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-eventbus-ext-application/1.2.5/org/refcodes/eventbus/ext/application/PauseBus.html))
* \[<span style="color:blue">ADDED</span>\] [`PauseBusEvent.java`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-1.2.5/refcodes-eventbus-ext-application/src/main/java/org/refcodes/eventbus/ext/application/PauseBusEvent.java) (see Javadoc at [`PauseBusEvent.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-eventbus-ext-application/1.2.5/org/refcodes/eventbus/ext/application/PauseBusEvent.html))
* \[<span style="color:blue">ADDED</span>\] [`PayloadBus.java`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-1.2.5/refcodes-eventbus-ext-application/src/main/java/org/refcodes/eventbus/ext/application/PayloadBus.java) (see Javadoc at [`PayloadBus.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-eventbus-ext-application/1.2.5/org/refcodes/eventbus/ext/application/PayloadBus.html))
* \[<span style="color:blue">ADDED</span>\] [`PropertiesBus.java`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-1.2.5/refcodes-eventbus-ext-application/src/main/java/org/refcodes/eventbus/ext/application/PropertiesBus.java) (see Javadoc at [`PropertiesBus.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-eventbus-ext-application/1.2.5/org/refcodes/eventbus/ext/application/PropertiesBus.html))
* \[<span style="color:blue">ADDED</span>\] [`ResumeBus.java`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-1.2.5/refcodes-eventbus-ext-application/src/main/java/org/refcodes/eventbus/ext/application/ResumeBus.java) (see Javadoc at [`ResumeBus.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-eventbus-ext-application/1.2.5/org/refcodes/eventbus/ext/application/ResumeBus.html))
* \[<span style="color:blue">ADDED</span>\] [`ResumeBusEvent.java`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-1.2.5/refcodes-eventbus-ext-application/src/main/java/org/refcodes/eventbus/ext/application/ResumeBusEvent.java) (see Javadoc at [`ResumeBusEvent.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-eventbus-ext-application/1.2.5/org/refcodes/eventbus/ext/application/ResumeBusEvent.html))
* \[<span style="color:blue">ADDED</span>\] [`StartBus.java`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-1.2.5/refcodes-eventbus-ext-application/src/main/java/org/refcodes/eventbus/ext/application/StartBus.java) (see Javadoc at [`StartBus.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-eventbus-ext-application/1.2.5/org/refcodes/eventbus/ext/application/StartBus.html))
* \[<span style="color:blue">ADDED</span>\] [`StartBusEvent.java`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-1.2.5/refcodes-eventbus-ext-application/src/main/java/org/refcodes/eventbus/ext/application/StartBusEvent.java) (see Javadoc at [`StartBusEvent.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-eventbus-ext-application/1.2.5/org/refcodes/eventbus/ext/application/StartBusEvent.html))
* \[<span style="color:blue">ADDED</span>\] [`StopBus.java`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-1.2.5/refcodes-eventbus-ext-application/src/main/java/org/refcodes/eventbus/ext/application/StopBus.java) (see Javadoc at [`StopBus.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-eventbus-ext-application/1.2.5/org/refcodes/eventbus/ext/application/StopBus.html))
* \[<span style="color:blue">ADDED</span>\] [`StopBusEvent.java`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-1.2.5/refcodes-eventbus-ext-application/src/main/java/org/refcodes/eventbus/ext/application/StopBusEvent.java) (see Javadoc at [`StopBusEvent.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-eventbus-ext-application/1.2.5/org/refcodes/eventbus/ext/application/StopBusEvent.html))
* \[<span style="color:blue">ADDED</span>\] [`ApplicationBusTest.java`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-1.2.5/refcodes-eventbus-ext-application/src/test/java/org/refcodes/eventbus/ext/application/ApplicationBusTest.java) 
* \[<span style="color:blue">ADDED</span>\] [`runtimelogger-config.xml`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-1.2.5/refcodes-eventbus-ext-application/src/test/resources/runtimelogger-config.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-1.2.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-1.2.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-1.2.5/refcodes-eventbus-ext-application/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`ApplicationBus.java`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-1.2.5/refcodes-eventbus-ext-application/src/main/java/org/refcodes/eventbus/ext/application/ApplicationBus.java) (see Javadoc at [`ApplicationBus.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-eventbus-ext-application/1.2.5/org/refcodes/eventbus/ext/application/ApplicationBus.html))
* \[<span style="color:green">MODIFIED</span>\] [`ApplicationBusImpl.java`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-1.2.5/refcodes-eventbus-ext-application/src/main/java/org/refcodes/eventbus/ext/application/ApplicationBusImpl.java) (see Javadoc at [`ApplicationBusImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-eventbus-ext-application/1.2.5/org/refcodes/eventbus/ext/application/ApplicationBusImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`ExceptionBusEvent.java`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-1.2.5/refcodes-eventbus-ext-application/src/main/java/org/refcodes/eventbus/ext/application/ExceptionBusEvent.java) (see Javadoc at [`ExceptionBusEvent.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-eventbus-ext-application/1.2.5/org/refcodes/eventbus/ext/application/ExceptionBusEvent.html))
* \[<span style="color:green">MODIFIED</span>\] [`ExceptionBusEventImpl.java`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-1.2.5/refcodes-eventbus-ext-application/src/main/java/org/refcodes/eventbus/ext/application/ExceptionBusEventImpl.java) (see Javadoc at [`ExceptionBusEventImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-eventbus-ext-application/1.2.5/org/refcodes/eventbus/ext/application/ExceptionBusEventImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`MessageBusEvent.java`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-1.2.5/refcodes-eventbus-ext-application/src/main/java/org/refcodes/eventbus/ext/application/MessageBusEvent.java) (see Javadoc at [`MessageBusEvent.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-eventbus-ext-application/1.2.5/org/refcodes/eventbus/ext/application/MessageBusEvent.html))
* \[<span style="color:green">MODIFIED</span>\] [`MessageBusEventImpl.java`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-1.2.5/refcodes-eventbus-ext-application/src/main/java/org/refcodes/eventbus/ext/application/MessageBusEventImpl.java) (see Javadoc at [`MessageBusEventImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-eventbus-ext-application/1.2.5/org/refcodes/eventbus/ext/application/MessageBusEventImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`PayloadBusEvent.java`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-1.2.5/refcodes-eventbus-ext-application/src/main/java/org/refcodes/eventbus/ext/application/PayloadBusEvent.java) (see Javadoc at [`PayloadBusEvent.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-eventbus-ext-application/1.2.5/org/refcodes/eventbus/ext/application/PayloadBusEvent.html))
* \[<span style="color:green">MODIFIED</span>\] [`PayloadBusEventBuilderImpl.java`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-1.2.5/refcodes-eventbus-ext-application/src/main/java/org/refcodes/eventbus/ext/application/PayloadBusEventBuilderImpl.java) (see Javadoc at [`PayloadBusEventBuilderImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-eventbus-ext-application/1.2.5/org/refcodes/eventbus/ext/application/PayloadBusEventBuilderImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`PayloadBusEventImpl.java`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-1.2.5/refcodes-eventbus-ext-application/src/main/java/org/refcodes/eventbus/ext/application/PayloadBusEventImpl.java) (see Javadoc at [`PayloadBusEventImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-eventbus-ext-application/1.2.5/org/refcodes/eventbus/ext/application/PayloadBusEventImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`PropertiesBusEvent.java`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-1.2.5/refcodes-eventbus-ext-application/src/main/java/org/refcodes/eventbus/ext/application/PropertiesBusEvent.java) (see Javadoc at [`PropertiesBusEvent.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-eventbus-ext-application/1.2.5/org/refcodes/eventbus/ext/application/PropertiesBusEvent.html))
* \[<span style="color:green">MODIFIED</span>\] [`PropertiesBusEventImpl.java`](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/refcodes-eventbus-ext-1.2.5/refcodes-eventbus-ext-application/src/main/java/org/refcodes/eventbus/ext/application/PropertiesBusEventImpl.java) (see Javadoc at [`PropertiesBusEventImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-eventbus-ext-application/1.2.5/org/refcodes/eventbus/ext/application/PropertiesBusEventImpl.html))

## Change list &lt;refcodes-filesystem&gt; (version 1.2.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-filesystem/src/refcodes-filesystem-1.2.5/pom.xml) 

## Change list &lt;refcodes-filesystem-alt&gt; (version 1.2.5)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-filesystem-alt/src/refcodes-filesystem-alt-1.2.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-filesystem-alt/src/refcodes-filesystem-alt-1.2.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-filesystem-alt/src/refcodes-filesystem-alt-1.2.5/refcodes-filesystem-alt-s3/pom.xml) 

## Change list &lt;refcodes-forwardsecrecy&gt; (version 1.2.5)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy/src/refcodes-forwardsecrecy-1.2.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy/src/refcodes-forwardsecrecy-1.2.5/pom.xml) 

## Change list &lt;refcodes-forwardsecrecy-alt&gt; (version 1.2.5)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-1.2.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-1.2.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-1.2.5/refcodes-forwardsecrecy-alt-filesystem/pom.xml) 

## Change list &lt;refcodes-io-ext&gt; (version 1.2.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-1.2.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-1.2.5/refcodes-io-ext-observable/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-1.2.5/refcodes-io-ext-observable/pom.xml) 

## Change list &lt;refcodes-interceptor&gt; (version 1.2.5)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-interceptor/src/refcodes-interceptor-1.2.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-interceptor/src/refcodes-interceptor-1.2.5/pom.xml) 

## Change list &lt;refcodes-jobbus&gt; (version 1.2.5)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-jobbus/src/refcodes-jobbus-1.2.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-jobbus/src/refcodes-jobbus-1.2.5/pom.xml) 

## Change list &lt;refcodes-rest-ext&gt; (version 1.2.5)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-1.2.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-1.2.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-1.2.5/refcodes-rest-ext-eureka/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-1.2.5/refcodes-rest-ext-eureka/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`EurekaInstanceDescriptor.java`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-1.2.5/refcodes-rest-ext-eureka/src/main/java/org/refcodes/rest/ext/eureka/EurekaInstanceDescriptor.java) (see Javadoc at [`EurekaInstanceDescriptor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest-ext-eureka/1.2.5/org/refcodes/rest/ext/eureka/EurekaInstanceDescriptor.html))
* \[<span style="color:green">MODIFIED</span>\] [`EurekaServerDescriptor.java`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/refcodes-rest-ext-1.2.5/refcodes-rest-ext-eureka/src/main/java/org/refcodes/rest/ext/eureka/EurekaServerDescriptor.java) (see Javadoc at [`EurekaServerDescriptor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-rest-ext-eureka/1.2.5/org/refcodes/rest/ext/eureka/EurekaServerDescriptor.html))

## Change list &lt;refcodes-remoting&gt; (version 1.2.5)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-remoting/src/refcodes-remoting-1.2.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-remoting/src/refcodes-remoting-1.2.5/pom.xml) 

## Change list &lt;refcodes-remoting-ext&gt; (version 1.2.5)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-1.2.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-1.2.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-1.2.5/refcodes-remoting-ext-observer/pom.xml) 

## Change list &lt;refcodes-security-alt&gt; (version 1.2.5)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-1.2.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-1.2.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-1.2.5/refcodes-security-alt-chaos/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-1.2.5/refcodes-security-alt-chaos/pom.xml) 

## Change list &lt;refcodes-security-ext&gt; (version 1.2.5)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-1.2.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-1.2.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-1.2.5/refcodes-security-ext-chaos/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-1.2.5/refcodes-security-ext-chaos/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-1.2.5/refcodes-security-ext-spring/pom.xml) 

## Change list &lt;refcodes-servicebus&gt; (version 1.2.5)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-servicebus/src/refcodes-servicebus-1.2.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`ServiceBusImpl.java`](https://bitbucket.org/refcodes/refcodes-servicebus/src/refcodes-servicebus-1.2.5/src/main/java/org/refcodes/servicebus/ServiceBusImpl.java) (see Javadoc at [`ServiceBusImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-servicebus/1.2.5/org/refcodes/servicebus/ServiceBusImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`ServiceContextImpl.java`](https://bitbucket.org/refcodes/refcodes-servicebus/src/refcodes-servicebus-1.2.5/src/main/java/org/refcodes/servicebus/ServiceContextImpl.java) (see Javadoc at [`ServiceContextImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-servicebus/1.2.5/org/refcodes/servicebus/ServiceContextImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`TestServiceAImpl.java`](https://bitbucket.org/refcodes/refcodes-servicebus/src/refcodes-servicebus-1.2.5/src/test/java/org/refcodes/servicebus/TestServiceAImpl.java) 
* \[<span style="color:green">MODIFIED</span>\] [`TestServiceBImpl.java`](https://bitbucket.org/refcodes/refcodes-servicebus/src/refcodes-servicebus-1.2.5/src/test/java/org/refcodes/servicebus/TestServiceBImpl.java) 
* \[<span style="color:green">MODIFIED</span>\] [`TestServiceDescriptorFactoryImpl.java`](https://bitbucket.org/refcodes/refcodes-servicebus/src/refcodes-servicebus-1.2.5/src/test/java/org/refcodes/servicebus/TestServiceDescriptorFactoryImpl.java) 

## Change list &lt;refcodes-servicebus-alt&gt; (version 1.2.5)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-servicebus-alt/src/refcodes-servicebus-alt-1.2.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-servicebus-alt/src/refcodes-servicebus-alt-1.2.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-servicebus-alt/src/refcodes-servicebus-alt-1.2.5/refcodes-servicebus-alt-spring/pom.xml) 

## Change list &lt;refcodes-tabular-alt&gt; (version 1.2.5)

* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-tabular-alt/src/refcodes-tabular-alt-1.2.5/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-tabular-alt/src/refcodes-tabular-alt-1.2.5/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-tabular-alt/src/refcodes-tabular-alt-1.2.5/refcodes-tabular-alt-forwardsecrecy/pom.xml) 
