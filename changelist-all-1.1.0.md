# Change list for REFCODES.ORG artifacts' version 1.1.0

> This change list has been auto-generated on `triton` by `steiner` with with `changelist-all.sh` on the 2017-04-26 at 21:49:13.

## Change list &lt;refcodes-licensing&gt; (version 1.1.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-licensing/src/refcodes-licensing-1.1.0/pom.xml) 

## Change list &lt;refcodes-parent&gt; (version 1.1.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-parent/src/refcodes-parent-1.1.0/pom.xml) 

## Change list &lt;refcodes-time&gt; (version 1.1.0)

* \[<span style="color:blue">ADDED</span>\] [`DateUtility.java`](https://bitbucket.org/refcodes/refcodes-time/src/refcodes-time-1.1.0/src/main/java/org/refcodes/time/DateUtility.java) (see Javadoc at [`DateUtility.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-time/1.1.0/org/refcodes/time/DateUtility.html))
* \[<span style="color:blue">ADDED</span>\] [`ConcurrentDateFormat.java`](https://bitbucket.org/refcodes/refcodes-time/src/refcodes-time-1.1.0/src/main/java/org/refcodes/time/ConcurrentDateFormat.java) (see Javadoc at [`ConcurrentDateFormat.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-time/1.1.0/org/refcodes/time/ConcurrentDateFormat.html))
* \[<span style="color:blue">ADDED</span>\] [`TimeUnit.java`](https://bitbucket.org/refcodes/refcodes-time/src/refcodes-time-1.1.0/src/main/java/org/refcodes/time/TimeUnit.java) (see Javadoc at [`TimeUnit.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-time/1.1.0/org/refcodes/time/TimeUnit.html))
* \[<span style="color:blue">ADDED</span>\] [`TimeUnitTest.java`](https://bitbucket.org/refcodes/refcodes-time/src/refcodes-time-1.1.0/src/test/java/org/refcodes/time/TimeUnitTest.java) 
* \[<span style="color:red">DELETED</span>\] `DateUtility.java`
* \[<span style="color:red">DELETED</span>\] `ConcurrentDateFormat.java`
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-time/src/refcodes-time-1.1.0/pom.xml) 

## Change list &lt;refcodes-exception&gt; (version 1.1.0)

* \[<span style="color:blue">ADDED</span>\] [`AbstractHiddenException.java`](https://bitbucket.org/refcodes/refcodes-exception/src/refcodes-exception-1.1.0/src/main/java/org/refcodes/exception/AbstractHiddenException.java) (see Javadoc at [`AbstractHiddenException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-exception/1.1.0/org/refcodes/exception/AbstractHiddenException.html))
* \[<span style="color:blue">ADDED</span>\] [`AbstractRuntimeException.java`](https://bitbucket.org/refcodes/refcodes-exception/src/refcodes-exception-1.1.0/src/main/java/org/refcodes/exception/AbstractRuntimeException.java) (see Javadoc at [`AbstractRuntimeException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-exception/1.1.0/org/refcodes/exception/AbstractRuntimeException.html))
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-exception/src/refcodes-exception-1.1.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`AbstractException.java`](https://bitbucket.org/refcodes/refcodes-exception/src/refcodes-exception-1.1.0/src/main/java/org/refcodes/exception/AbstractException.java) (see Javadoc at [`AbstractException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-exception/1.1.0/org/refcodes/exception/AbstractException.html))
* \[<span style="color:green">MODIFIED</span>\] [`BugException.java`](https://bitbucket.org/refcodes/refcodes-exception/src/refcodes-exception-1.1.0/src/main/java/org/refcodes/exception/BugException.java) (see Javadoc at [`BugException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-exception/1.1.0/org/refcodes/exception/BugException.html))
* \[<span style="color:green">MODIFIED</span>\] [`HiddenException.java`](https://bitbucket.org/refcodes/refcodes-exception/src/refcodes-exception-1.1.0/src/main/java/org/refcodes/exception/HiddenException.java) (see Javadoc at [`HiddenException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-exception/1.1.0/org/refcodes/exception/HiddenException.html))
* \[<span style="color:green">MODIFIED</span>\] [`VerifyRuntimeException.java`](https://bitbucket.org/refcodes/refcodes-exception/src/refcodes-exception-1.1.0/src/main/java/org/refcodes/exception/VerifyRuntimeException.java) (see Javadoc at [`VerifyRuntimeException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-exception/1.1.0/org/refcodes/exception/VerifyRuntimeException.html))

## Change list &lt;refcodes-mixin&gt; (version 1.1.0)

* \[<span style="color:blue">ADDED</span>\] [`DomainAccessor.java`](https://bitbucket.org/refcodes/refcodes-mixin/src/refcodes-mixin-1.1.0/src/main/java/org/refcodes/mixin/DomainAccessor.java) (see Javadoc at [`DomainAccessor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-mixin/1.1.0/org/refcodes/mixin/DomainAccessor.html))
* \[<span style="color:blue">ADDED</span>\] [`PrintStreamAccessor.java`](https://bitbucket.org/refcodes/refcodes-mixin/src/refcodes-mixin-1.1.0/src/main/java/org/refcodes/mixin/PrintStreamAccessor.java) (see Javadoc at [`PrintStreamAccessor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-mixin/1.1.0/org/refcodes/mixin/PrintStreamAccessor.html))
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-mixin/src/refcodes-mixin-1.1.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`OutputStreamAccessor.java`](https://bitbucket.org/refcodes/refcodes-mixin/src/refcodes-mixin-1.1.0/src/main/java/org/refcodes/mixin/OutputStreamAccessor.java) (see Javadoc at [`OutputStreamAccessor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-mixin/1.1.0/org/refcodes/mixin/OutputStreamAccessor.html))
* \[<span style="color:green">MODIFIED</span>\] [`PathAccessor.java`](https://bitbucket.org/refcodes/refcodes-mixin/src/refcodes-mixin-1.1.0/src/main/java/org/refcodes/mixin/PathAccessor.java) (see Javadoc at [`PathAccessor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-mixin/1.1.0/org/refcodes/mixin/PathAccessor.html))

## Change list &lt;refcodes-data&gt; (version 1.1.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data/src/refcodes-data-1.1.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`DateConsts.java`](https://bitbucket.org/refcodes/refcodes-data/src/refcodes-data-1.1.0/src/main/java/org/refcodes/data/DateConsts.java) (see Javadoc at [`DateConsts.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-data/1.1.0/org/refcodes/data/DateConsts.html))
* \[<span style="color:green">MODIFIED</span>\] [`DelimeterConsts.java`](https://bitbucket.org/refcodes/refcodes-data/src/refcodes-data-1.1.0/src/main/java/org/refcodes/data/DelimeterConsts.java) (see Javadoc at [`DelimeterConsts.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-data/1.1.0/org/refcodes/data/DelimeterConsts.html))
* \[<span style="color:green">MODIFIED</span>\] [`FileSystemConsts.java`](https://bitbucket.org/refcodes/refcodes-data/src/refcodes-data-1.1.0/src/main/java/org/refcodes/data/FileSystemConsts.java) (see Javadoc at [`FileSystemConsts.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-data/1.1.0/org/refcodes/data/FileSystemConsts.html))

## Change list &lt;refcodes-controlflow&gt; (version 1.1.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-controlflow/src/refcodes-controlflow-1.1.0/pom.xml) 

## Change list &lt;refcodes-numerical&gt; (version 1.1.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-numerical/src/refcodes-numerical-1.1.0/pom.xml) 

## Change list &lt;refcodes-generator&gt; (version 1.1.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-generator/src/refcodes-generator-1.1.0/pom.xml) 

## Change list &lt;refcodes-collection&gt; (version 1.1.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-collection/src/refcodes-collection-1.1.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`PropertyImpl.java`](https://bitbucket.org/refcodes/refcodes-collection/src/refcodes-collection-1.1.0/src/main/java/org/refcodes/collection/PropertyImpl.java) (see Javadoc at [`PropertyImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-collection/1.1.0/org/refcodes/collection/PropertyImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`RelationImpl.java`](https://bitbucket.org/refcodes/refcodes-collection/src/refcodes-collection-1.1.0/src/main/java/org/refcodes/collection/RelationImpl.java) (see Javadoc at [`RelationImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-collection/1.1.0/org/refcodes/collection/RelationImpl.html))

## Change list &lt;refcodes-runtime&gt; (version 1.1.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-runtime/src/refcodes-runtime-1.1.0/pom.xml) 

## Change list &lt;refcodes-component&gt; (version 1.1.0)

* \[<span style="color:blue">ADDED</span>\] [`ComponentException.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-1.1.0/src/main/java/org/refcodes/component/ComponentException.java) (see Javadoc at [`ComponentException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/1.1.0/org/refcodes/component/ComponentException.html))
* \[<span style="color:blue">ADDED</span>\] [`ComponentRuntimeException.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-1.1.0/src/main/java/org/refcodes/component/ComponentRuntimeException.java) (see Javadoc at [`ComponentRuntimeException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/1.1.0/org/refcodes/component/ComponentRuntimeException.html))
* \[<span style="color:red">DELETED</span>\] `AbstractComponentException.java`
* \[<span style="color:red">DELETED</span>\] `AbstractComponentRuntimeException.java`
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-1.1.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`CeaseException.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-1.1.0/src/main/java/org/refcodes/component/CeaseException.java) (see Javadoc at [`CeaseException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/1.1.0/org/refcodes/component/CeaseException.html))
* \[<span style="color:green">MODIFIED</span>\] [`DigestException.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-1.1.0/src/main/java/org/refcodes/component/DigestException.java) (see Javadoc at [`DigestException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/1.1.0/org/refcodes/component/DigestException.html))
* \[<span style="color:green">MODIFIED</span>\] [`HandleTimeoutRuntimeException.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-1.1.0/src/main/java/org/refcodes/component/HandleTimeoutRuntimeException.java) (see Javadoc at [`HandleTimeoutRuntimeException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/1.1.0/org/refcodes/component/HandleTimeoutRuntimeException.html))
* \[<span style="color:green">MODIFIED</span>\] [`IllegaleHandleStateChangeRuntimeException.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-1.1.0/src/main/java/org/refcodes/component/IllegaleHandleStateChangeRuntimeException.java) (see Javadoc at [`IllegaleHandleStateChangeRuntimeException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/1.1.0/org/refcodes/component/IllegaleHandleStateChangeRuntimeException.html))
* \[<span style="color:green">MODIFIED</span>\] [`InitializeException.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-1.1.0/src/main/java/org/refcodes/component/InitializeException.java) (see Javadoc at [`InitializeException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/1.1.0/org/refcodes/component/InitializeException.html))
* \[<span style="color:green">MODIFIED</span>\] [`PauseException.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-1.1.0/src/main/java/org/refcodes/component/PauseException.java) (see Javadoc at [`PauseException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/1.1.0/org/refcodes/component/PauseException.html))
* \[<span style="color:green">MODIFIED</span>\] [`ResumeException.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-1.1.0/src/main/java/org/refcodes/component/ResumeException.java) (see Javadoc at [`ResumeException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/1.1.0/org/refcodes/component/ResumeException.html))
* \[<span style="color:green">MODIFIED</span>\] [`StartException.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-1.1.0/src/main/java/org/refcodes/component/StartException.java) (see Javadoc at [`StartException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/1.1.0/org/refcodes/component/StartException.html))
* \[<span style="color:green">MODIFIED</span>\] [`StopException.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-1.1.0/src/main/java/org/refcodes/component/StopException.java) (see Javadoc at [`StopException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/1.1.0/org/refcodes/component/StopException.html))
* \[<span style="color:green">MODIFIED</span>\] [`UnknownHandleRuntimeException.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-1.1.0/src/main/java/org/refcodes/component/UnknownHandleRuntimeException.java) (see Javadoc at [`UnknownHandleRuntimeException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/1.1.0/org/refcodes/component/UnknownHandleRuntimeException.html))
* \[<span style="color:green">MODIFIED</span>\] [`UnsupportedHandleOperationRuntimeException.java`](https://bitbucket.org/refcodes/refcodes-component/src/refcodes-component-1.1.0/src/main/java/org/refcodes/component/UnsupportedHandleOperationRuntimeException.java) (see Javadoc at [`UnsupportedHandleOperationRuntimeException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-component/1.1.0/org/refcodes/component/UnsupportedHandleOperationRuntimeException.html))

## Change list &lt;refcodes-interceptor&gt; (version 1.1.0)

* \[<span style="color:blue">ADDED</span>\] [`InterceptorException.java`](https://bitbucket.org/refcodes/refcodes-interceptor/src/refcodes-interceptor-1.1.0/src/main/java/org/refcodes/interceptor/InterceptorException.java) (see Javadoc at [`InterceptorException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-interceptor/1.1.0/org/refcodes/interceptor/InterceptorException.html))
* \[<span style="color:red">DELETED</span>\] `AbstractInterceptorException.java`
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-interceptor/src/refcodes-interceptor-1.1.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-interceptor/src/refcodes-interceptor-1.1.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`WorkPieceException.java`](https://bitbucket.org/refcodes/refcodes-interceptor/src/refcodes-interceptor-1.1.0/src/main/java/org/refcodes/interceptor/WorkPieceException.java) (see Javadoc at [`WorkPieceException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-interceptor/1.1.0/org/refcodes/interceptor/WorkPieceException.html))

## Change list &lt;refcodes-factory&gt; (version 1.1.0)

* \[<span style="color:blue">ADDED</span>\] [`FactoryRuntimeException.java`](https://bitbucket.org/refcodes/refcodes-factory/src/refcodes-factory-1.1.0/src/main/java/org/refcodes/factory/FactoryRuntimeException.java) (see Javadoc at [`FactoryRuntimeException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-factory/1.1.0/org/refcodes/factory/FactoryRuntimeException.html))
* \[<span style="color:red">DELETED</span>\] `AbstractFactoryRuntimeException.java`
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-factory/src/refcodes-factory-1.1.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`UnexpectedFactoryRuntimeException.java`](https://bitbucket.org/refcodes/refcodes-factory/src/refcodes-factory-1.1.0/src/main/java/org/refcodes/factory/UnexpectedFactoryRuntimeException.java) (see Javadoc at [`UnexpectedFactoryRuntimeException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-factory/1.1.0/org/refcodes/factory/UnexpectedFactoryRuntimeException.html))

## Change list &lt;refcodes-data-ext&gt; (version 1.1.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.1.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.1.0/refcodes-data-ext-boulderdash/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.1.0/refcodes-data-ext-boulderdash/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.1.0/refcodes-data-ext-checkers/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.1.0/refcodes-data-ext-checkers/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.1.0/refcodes-data-ext-chess/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.1.0/refcodes-data-ext-chess/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-data-ext/src/refcodes-data-ext-1.1.0/refcodes-data-ext-corporate/pom.xml) 

## Change list &lt;refcodes-graphical&gt; (version 1.1.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-graphical/src/refcodes-graphical-1.1.0/pom.xml) 

## Change list &lt;refcodes-textual&gt; (version 1.1.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-textual/src/refcodes-textual-1.1.0/pom.xml) 

## Change list &lt;refcodes-criteria&gt; (version 1.1.0)

* \[<span style="color:blue">ADDED</span>\] [`CriteriaException.java`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-1.1.0/src/main/java/org/refcodes/criteria/CriteriaException.java) (see Javadoc at [`CriteriaException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-criteria/1.1.0/org/refcodes/criteria/CriteriaException.html))
* \[<span style="color:blue">ADDED</span>\] [`CriteriaRuntimeException.java`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-1.1.0/src/main/java/org/refcodes/criteria/CriteriaRuntimeException.java) (see Javadoc at [`CriteriaRuntimeException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-criteria/1.1.0/org/refcodes/criteria/CriteriaRuntimeException.html))
* \[<span style="color:red">DELETED</span>\] `AbstractCriteriaException.java`
* \[<span style="color:red">DELETED</span>\] `AbstractCriteriaRuntimeException.java`
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-1.1.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-1.1.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`ComplexCriteriaException.java`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-1.1.0/src/main/java/org/refcodes/criteria/ComplexCriteriaException.java) (see Javadoc at [`ComplexCriteriaException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-criteria/1.1.0/org/refcodes/criteria/ComplexCriteriaException.html))
* \[<span style="color:green">MODIFIED</span>\] [`UnknownCriteriaRuntimeException.java`](https://bitbucket.org/refcodes/refcodes-criteria/src/refcodes-criteria-1.1.0/src/main/java/org/refcodes/criteria/UnknownCriteriaRuntimeException.java) (see Javadoc at [`UnknownCriteriaRuntimeException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-criteria/1.1.0/org/refcodes/criteria/UnknownCriteriaRuntimeException.html))

## Change list &lt;refcodes-tabular&gt; (version 1.1.0)

* \[<span style="color:blue">ADDED</span>\] [`TabularException.java`](https://bitbucket.org/refcodes/refcodes-tabular/src/refcodes-tabular-1.1.0/src/main/java/org/refcodes/tabular/TabularException.java) (see Javadoc at [`TabularException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-tabular/1.1.0/org/refcodes/tabular/TabularException.html))
* \[<span style="color:blue">ADDED</span>\] [`TabularRuntimeException.java`](https://bitbucket.org/refcodes/refcodes-tabular/src/refcodes-tabular-1.1.0/src/main/java/org/refcodes/tabular/TabularRuntimeException.java) (see Javadoc at [`TabularRuntimeException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-tabular/1.1.0/org/refcodes/tabular/TabularRuntimeException.html))
* \[<span style="color:red">DELETED</span>\] `AbstractTabularException.java`
* \[<span style="color:red">DELETED</span>\] `AbstractTabularRuntimeException.java`
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-tabular/src/refcodes-tabular-1.1.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`ColumnMismatchException.java`](https://bitbucket.org/refcodes/refcodes-tabular/src/refcodes-tabular-1.1.0/src/main/java/org/refcodes/tabular/ColumnMismatchException.java) (see Javadoc at [`ColumnMismatchException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-tabular/1.1.0/org/refcodes/tabular/ColumnMismatchException.html))
* \[<span style="color:green">MODIFIED</span>\] [`HeaderAccessor.java`](https://bitbucket.org/refcodes/refcodes-tabular/src/refcodes-tabular-1.1.0/src/main/java/org/refcodes/tabular/HeaderAccessor.java) (see Javadoc at [`HeaderAccessor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-tabular/1.1.0/org/refcodes/tabular/HeaderAccessor.html))
* \[<span style="color:green">MODIFIED</span>\] [`HeaderMismatchException.java`](https://bitbucket.org/refcodes/refcodes-tabular/src/refcodes-tabular-1.1.0/src/main/java/org/refcodes/tabular/HeaderMismatchException.java) (see Javadoc at [`HeaderMismatchException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-tabular/1.1.0/org/refcodes/tabular/HeaderMismatchException.html))
* \[<span style="color:green">MODIFIED</span>\] [`DateColumnImpl.java`](https://bitbucket.org/refcodes/refcodes-tabular/src/refcodes-tabular-1.1.0/src/main/java/org/refcodes/tabular/DateColumnImpl.java) (see Javadoc at [`DateColumnImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-tabular/1.1.0/org/refcodes/tabular/DateColumnImpl.html))

## Change list &lt;refcodes-logger&gt; (version 1.1.0)

* \[<span style="color:blue">ADDED</span>\] [`LoggerRuntimeException.java`](https://bitbucket.org/refcodes/refcodes-logger/src/refcodes-logger-1.1.0/src/main/java/org/refcodes/logger/LoggerRuntimeException.java) (see Javadoc at [`LoggerRuntimeException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-logger/1.1.0/org/refcodes/logger/LoggerRuntimeException.html))
* \[<span style="color:red">DELETED</span>\] `AbstractLoggerRuntimeException.java`
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger/src/refcodes-logger-1.1.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger/src/refcodes-logger-1.1.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`IllegalRecordRuntimeException.java`](https://bitbucket.org/refcodes/refcodes-logger/src/refcodes-logger-1.1.0/src/main/java/org/refcodes/logger/IllegalRecordRuntimeException.java) (see Javadoc at [`IllegalRecordRuntimeException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-logger/1.1.0/org/refcodes/logger/IllegalRecordRuntimeException.html))
* \[<span style="color:green">MODIFIED</span>\] [`LoggerInstantiationRuntimeException.java`](https://bitbucket.org/refcodes/refcodes-logger/src/refcodes-logger-1.1.0/src/main/java/org/refcodes/logger/LoggerInstantiationRuntimeException.java) (see Javadoc at [`LoggerInstantiationRuntimeException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-logger/1.1.0/org/refcodes/logger/LoggerInstantiationRuntimeException.html))
* \[<span style="color:green">MODIFIED</span>\] [`UnexpectedLogRuntimeException.java`](https://bitbucket.org/refcodes/refcodes-logger/src/refcodes-logger-1.1.0/src/main/java/org/refcodes/logger/UnexpectedLogRuntimeException.java) (see Javadoc at [`UnexpectedLogRuntimeException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-logger/1.1.0/org/refcodes/logger/UnexpectedLogRuntimeException.html))

## Change list &lt;refcodes-factory-alt&gt; (version 1.1.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-factory-alt/src/refcodes-factory-alt-1.1.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-factory-alt/src/refcodes-factory-alt-1.1.0/refcodes-factory-alt-spring/pom.xml) 

## Change list &lt;refcodes-logger-alt&gt; (version 1.1.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.1.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.1.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.1.0/refcodes-logger-alt-async/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.1.0/refcodes-logger-alt-async/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`org.eclipse.core.resources.prefs`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.1.0/refcodes-logger-alt-async/.settings/org.eclipse.core.resources.prefs) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.1.0/refcodes-logger-alt-console/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.1.0/refcodes-logger-alt-console/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.1.0/refcodes-logger-alt-io/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.1.0/refcodes-logger-alt-io/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`IoLoggerImpl.java`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.1.0/refcodes-logger-alt-io/src/main/java/org/refcodes/logger/alt/io/IoLoggerImpl.java) (see Javadoc at [`IoLoggerImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-logger-alt-io/1.1.0/org/refcodes/logger/alt/io/IoLoggerImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.1.0/refcodes-logger-alt-simpledb/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.1.0/refcodes-logger-alt-slf4j/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/refcodes-logger-alt-1.1.0/refcodes-logger-alt-spring/pom.xml) 

## Change list &lt;refcodes-matcher&gt; (version 1.1.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-matcher/src/refcodes-matcher-1.1.0/pom.xml) 

## Change list &lt;refcodes-observer&gt; (version 1.1.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-observer/src/refcodes-observer-1.1.0/pom.xml) 

## Change list &lt;refcodes-checkerboard&gt; (version 1.1.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-1.1.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-checkerboard/src/refcodes-checkerboard-1.1.0/README.md) 

## Change list &lt;refcodes-graphical-ext&gt; (version 1.1.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-1.1.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-1.1.0/refcodes-graphical-ext-javafx/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`FxDragSpriteEventHandlerImpl.java`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-1.1.0/refcodes-graphical-ext-javafx/src/main/java/org/refcodes/graphical/ext/javafx/FxDragSpriteEventHandlerImpl.java) (see Javadoc at [`FxDragSpriteEventHandlerImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-graphical-ext-javafx/1.1.0/org/refcodes/graphical/ext/javafx/FxDragSpriteEventHandlerImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`FxViewportPaneImpl.java`](https://bitbucket.org/refcodes/refcodes-graphical-ext/src/refcodes-graphical-ext-1.1.0/refcodes-graphical-ext-javafx/src/main/java/org/refcodes/graphical/ext/javafx/FxViewportPaneImpl.java) (see Javadoc at [`FxViewportPaneImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-graphical-ext-javafx/1.1.0/org/refcodes/graphical/ext/javafx/FxViewportPaneImpl.html))

## Change list &lt;refcodes-checkerboard-alt&gt; (version 1.1.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-alt/src/refcodes-checkerboard-alt-1.1.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-checkerboard-alt/src/refcodes-checkerboard-alt-1.1.0/refcodes-checkerboard-alt-javafx/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`FxCheckerboardViewerImpl.java`](https://bitbucket.org/refcodes/refcodes-checkerboard-alt/src/refcodes-checkerboard-alt-1.1.0/refcodes-checkerboard-alt-javafx/src/main/java/org/refcodes/checkerboard/alt/javafx/FxCheckerboardViewerImpl.java) (see Javadoc at [`FxCheckerboardViewerImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-checkerboard-alt-javafx/1.1.0/org/refcodes/checkerboard/alt/javafx/FxCheckerboardViewerImpl.html))

## Change list &lt;refcodes-io&gt; (version 1.1.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-io/src/refcodes-io-1.1.0/pom.xml) 

## Change list &lt;refcodes-codec&gt; (version 1.1.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-1.1.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-1.1.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`BaseCodecConfig.java`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-1.1.0/src/main/java/org/refcodes/codec/BaseCodecConfig.java) (see Javadoc at [`BaseCodecConfig.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-codec/1.1.0/org/refcodes/codec/BaseCodecConfig.html))
* \[<span style="color:green">MODIFIED</span>\] [`BaseCodecBuilderTest.java`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-1.1.0/src/test/java/org/refcodes/codec/BaseCodecBuilderTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`BaseDecodeInputStreamReceiverTest.java`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-1.1.0/src/test/java/org/refcodes/codec/BaseDecodeInputStreamReceiverTest.java) 
* \[<span style="color:green">MODIFIED</span>\] [`BaseEncodeOutputStreamSenderTest.java`](https://bitbucket.org/refcodes/refcodes-codec/src/refcodes-codec-1.1.0/src/test/java/org/refcodes/codec/BaseEncodeOutputStreamSenderTest.java) 

## Change list &lt;refcodes-command&gt; (version 1.1.0)

* \[<span style="color:blue">ADDED</span>\] [`CommandRuntimeException.java`](https://bitbucket.org/refcodes/refcodes-command/src/refcodes-command-1.1.0/src/main/java/org/refcodes/command/CommandRuntimeException.java) (see Javadoc at [`CommandRuntimeException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-command/1.1.0/org/refcodes/command/CommandRuntimeException.html))
* \[<span style="color:red">DELETED</span>\] `AbstractCommandRuntimeException.java`
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-command/src/refcodes-command-1.1.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-command/src/refcodes-command-1.1.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`Command.java`](https://bitbucket.org/refcodes/refcodes-command/src/refcodes-command-1.1.0/src/main/java/org/refcodes/command/Command.java) (see Javadoc at [`Command.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-command/1.1.0/org/refcodes/command/Command.html))
* \[<span style="color:green">MODIFIED</span>\] [`AbstractCommand.java`](https://bitbucket.org/refcodes/refcodes-command/src/refcodes-command-1.1.0/src/main/java/org/refcodes/command/AbstractCommand.java) (see Javadoc at [`AbstractCommand.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-command/1.1.0/org/refcodes/command/AbstractCommand.html))
* \[<span style="color:green">MODIFIED</span>\] [`AbstractUndoable.java`](https://bitbucket.org/refcodes/refcodes-command/src/refcodes-command-1.1.0/src/main/java/org/refcodes/command/AbstractUndoable.java) (see Javadoc at [`AbstractUndoable.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-command/1.1.0/org/refcodes/command/AbstractUndoable.html))
* \[<span style="color:green">MODIFIED</span>\] [`NoExceptionAvailableRuntimeException.java`](https://bitbucket.org/refcodes/refcodes-command/src/refcodes-command-1.1.0/src/main/java/org/refcodes/command/NoExceptionAvailableRuntimeException.java) (see Javadoc at [`NoExceptionAvailableRuntimeException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-command/1.1.0/org/refcodes/command/NoExceptionAvailableRuntimeException.html))
* \[<span style="color:green">MODIFIED</span>\] [`NoResultAvailableRuntimeException.java`](https://bitbucket.org/refcodes/refcodes-command/src/refcodes-command-1.1.0/src/main/java/org/refcodes/command/NoResultAvailableRuntimeException.java) (see Javadoc at [`NoResultAvailableRuntimeException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-command/1.1.0/org/refcodes/command/NoResultAvailableRuntimeException.html))
* \[<span style="color:green">MODIFIED</span>\] [`NotUndoableRuntimeException.java`](https://bitbucket.org/refcodes/refcodes-command/src/refcodes-command-1.1.0/src/main/java/org/refcodes/command/NotUndoableRuntimeException.java) (see Javadoc at [`NotUndoableRuntimeException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-command/1.1.0/org/refcodes/command/NotUndoableRuntimeException.html))
* \[<span style="color:green">MODIFIED</span>\] [`NotYetExecutedRuntimeException.java`](https://bitbucket.org/refcodes/refcodes-command/src/refcodes-command-1.1.0/src/main/java/org/refcodes/command/NotYetExecutedRuntimeException.java) (see Javadoc at [`NotYetExecutedRuntimeException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-command/1.1.0/org/refcodes/command/NotYetExecutedRuntimeException.html))
* \[<span style="color:green">MODIFIED</span>\] [`Undoable.java`](https://bitbucket.org/refcodes/refcodes-command/src/refcodes-command-1.1.0/src/main/java/org/refcodes/command/Undoable.java) (see Javadoc at [`Undoable.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-command/1.1.0/org/refcodes/command/Undoable.html))
* \[<span style="color:green">MODIFIED</span>\] [`AddCommandImpl.java`](https://bitbucket.org/refcodes/refcodes-command/src/refcodes-command-1.1.0/src/test/java/org/refcodes/command/AddCommandImpl.java) (see Javadoc at [`AddCommandImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-command/1.1.0/src/test/java/org/refcodes/command/AddCommandImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`MulCommandImpl.java`](https://bitbucket.org/refcodes/refcodes-command/src/refcodes-command-1.1.0/src/test/java/org/refcodes/command/MulCommandImpl.java) (see Javadoc at [`MulCommandImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-command/1.1.0/src/test/java/org/refcodes/command/MulCommandImpl.html))

## Change list &lt;refcodes-component-ext&gt; (version 1.1.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.1.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-component-ext/src/refcodes-component-ext-1.1.0/refcodes-component-ext-observer/pom.xml) 

## Change list &lt;refcodes-cli&gt; (version 1.1.0)

* \[<span style="color:blue">ADDED</span>\] [`ConsoleException.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-1.1.0/src/main/java/org/refcodes/console/ConsoleException.java) (see Javadoc at [`ConsoleException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/1.1.0/org/refcodes/console/ConsoleException.html))
* \[<span style="color:red">DELETED</span>\] `AbstractConsoleException.java`
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-1.1.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-1.1.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`ArgsSyntaxException.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-1.1.0/src/main/java/org/refcodes/console/ArgsSyntaxException.java) (see Javadoc at [`ArgsSyntaxException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/1.1.0/org/refcodes/console/ArgsSyntaxException.html))
* \[<span style="color:green">MODIFIED</span>\] [`ArgsParser.java`](https://bitbucket.org/refcodes/refcodes-cli/src/refcodes-cli-1.1.0/src/main/java/org/refcodes/console/ArgsParser.java) (see Javadoc at [`ArgsParser.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-cli/1.1.0/org/refcodes/console/ArgsParser.html))

## Change list &lt;refcodes-security&gt; (version 1.1.0)

* \[<span style="color:blue">ADDED</span>\] [`SecurityException.java`](https://bitbucket.org/refcodes/refcodes-security/src/refcodes-security-1.1.0/src/main/java/org/refcodes/security/SecurityException.java) (see Javadoc at [`SecurityException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-security/1.1.0/org/refcodes/security/SecurityException.html))
* \[<span style="color:red">DELETED</span>\] `AbstractSecurityException.java`
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security/src/refcodes-security-1.1.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security/src/refcodes-security-1.1.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`DecryptionException.java`](https://bitbucket.org/refcodes/refcodes-security/src/refcodes-security-1.1.0/src/main/java/org/refcodes/security/DecryptionException.java) (see Javadoc at [`DecryptionException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-security/1.1.0/org/refcodes/security/DecryptionException.html))
* \[<span style="color:green">MODIFIED</span>\] [`EncryptionException.java`](https://bitbucket.org/refcodes/refcodes-security/src/refcodes-security-1.1.0/src/main/java/org/refcodes/security/EncryptionException.java) (see Javadoc at [`EncryptionException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-security/1.1.0/org/refcodes/security/EncryptionException.html))

## Change list &lt;refcodes-forwardsecrecy&gt; (version 1.1.0)

* \[<span style="color:blue">ADDED</span>\] [`ForwardSecrecyDecryptionException.java`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy/src/refcodes-forwardsecrecy-1.1.0/src/main/java/org/refcodes/forwardsecrecy/ForwardSecrecyDecryptionException.java) (see Javadoc at [`ForwardSecrecyDecryptionException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-forwardsecrecy/1.1.0/org/refcodes/forwardsecrecy/ForwardSecrecyDecryptionException.html))
* \[<span style="color:blue">ADDED</span>\] [`ForwardSecrecyException.java`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy/src/refcodes-forwardsecrecy-1.1.0/src/main/java/org/refcodes/forwardsecrecy/ForwardSecrecyException.java) (see Javadoc at [`ForwardSecrecyException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-forwardsecrecy/1.1.0/org/refcodes/forwardsecrecy/ForwardSecrecyException.html))
* \[<span style="color:blue">ADDED</span>\] [`ForwardSecrecyRuntimeException.java`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy/src/refcodes-forwardsecrecy-1.1.0/src/main/java/org/refcodes/forwardsecrecy/ForwardSecrecyRuntimeException.java) (see Javadoc at [`ForwardSecrecyRuntimeException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-forwardsecrecy/1.1.0/org/refcodes/forwardsecrecy/ForwardSecrecyRuntimeException.html))
* \[<span style="color:red">DELETED</span>\] `AbstractForwardSecrecyDecryptionException.java`
* \[<span style="color:red">DELETED</span>\] `AbstractForwardSecrecyException.java`
* \[<span style="color:red">DELETED</span>\] `AbstractForwardSecrecyRuntimeException.java`
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy/src/refcodes-forwardsecrecy-1.1.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy/src/refcodes-forwardsecrecy-1.1.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`CipherUidAlreadyInUseException.java`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy/src/refcodes-forwardsecrecy-1.1.0/src/main/java/org/refcodes/forwardsecrecy/CipherUidAlreadyInUseException.java) (see Javadoc at [`CipherUidAlreadyInUseException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-forwardsecrecy/1.1.0/org/refcodes/forwardsecrecy/CipherUidAlreadyInUseException.html))
* \[<span style="color:green">MODIFIED</span>\] [`NoCipherUidException.java`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy/src/refcodes-forwardsecrecy-1.1.0/src/main/java/org/refcodes/forwardsecrecy/NoCipherUidException.java) (see Javadoc at [`NoCipherUidException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-forwardsecrecy/1.1.0/org/refcodes/forwardsecrecy/NoCipherUidException.html))
* \[<span style="color:green">MODIFIED</span>\] [`SignatureVerificationException.java`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy/src/refcodes-forwardsecrecy-1.1.0/src/main/java/org/refcodes/forwardsecrecy/SignatureVerificationException.java) (see Javadoc at [`SignatureVerificationException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-forwardsecrecy/1.1.0/org/refcodes/forwardsecrecy/SignatureVerificationException.html))
* \[<span style="color:green">MODIFIED</span>\] [`UnexpectedForwardSecrecyRuntimeException.java`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy/src/refcodes-forwardsecrecy-1.1.0/src/main/java/org/refcodes/forwardsecrecy/UnexpectedForwardSecrecyRuntimeException.java) (see Javadoc at [`UnexpectedForwardSecrecyRuntimeException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-forwardsecrecy/1.1.0/org/refcodes/forwardsecrecy/UnexpectedForwardSecrecyRuntimeException.html))
* \[<span style="color:green">MODIFIED</span>\] [`UnknownCipherUidException.java`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy/src/refcodes-forwardsecrecy-1.1.0/src/main/java/org/refcodes/forwardsecrecy/UnknownCipherUidException.java) (see Javadoc at [`UnknownCipherUidException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-forwardsecrecy/1.1.0/org/refcodes/forwardsecrecy/UnknownCipherUidException.html))

## Change list &lt;refcodes-filesystem&gt; (version 1.1.0)

* \[<span style="color:blue">ADDED</span>\] [`FileSystemException.java`](https://bitbucket.org/refcodes/refcodes-filesystem/src/refcodes-filesystem-1.1.0/src/main/java/org/refcodes/filesystem/FileSystemException.java) (see Javadoc at [`FileSystemException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-filesystem/1.1.0/org/refcodes/filesystem/FileSystemException.html))
* \[<span style="color:blue">ADDED</span>\] [`FileSystemRuntimeException.java`](https://bitbucket.org/refcodes/refcodes-filesystem/src/refcodes-filesystem-1.1.0/src/main/java/org/refcodes/filesystem/FileSystemRuntimeException.java) (see Javadoc at [`FileSystemRuntimeException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-filesystem/1.1.0/org/refcodes/filesystem/FileSystemRuntimeException.html))
* \[<span style="color:red">DELETED</span>\] `AbstractFileSystemException.java`
* \[<span style="color:red">DELETED</span>\] `AbstractFileSystemRuntimeException.java`
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-filesystem/src/refcodes-filesystem-1.1.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`ConcurrentAccessException.java`](https://bitbucket.org/refcodes/refcodes-filesystem/src/refcodes-filesystem-1.1.0/src/main/java/org/refcodes/filesystem/ConcurrentAccessException.java) (see Javadoc at [`ConcurrentAccessException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-filesystem/1.1.0/org/refcodes/filesystem/ConcurrentAccessException.html))
* \[<span style="color:green">MODIFIED</span>\] [`FileAlreadyExistsException.java`](https://bitbucket.org/refcodes/refcodes-filesystem/src/refcodes-filesystem-1.1.0/src/main/java/org/refcodes/filesystem/FileAlreadyExistsException.java) (see Javadoc at [`FileAlreadyExistsException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-filesystem/1.1.0/org/refcodes/filesystem/FileAlreadyExistsException.html))
* \[<span style="color:green">MODIFIED</span>\] [`FileSystemAccessException.java`](https://bitbucket.org/refcodes/refcodes-filesystem/src/refcodes-filesystem-1.1.0/src/main/java/org/refcodes/filesystem/FileSystemAccessException.java) (see Javadoc at [`FileSystemAccessException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-filesystem/1.1.0/org/refcodes/filesystem/FileSystemAccessException.html))
* \[<span style="color:green">MODIFIED</span>\] [`FileSystemAlreadyExistsException.java`](https://bitbucket.org/refcodes/refcodes-filesystem/src/refcodes-filesystem-1.1.0/src/main/java/org/refcodes/filesystem/FileSystemAlreadyExistsException.java) (see Javadoc at [`FileSystemAlreadyExistsException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-filesystem/1.1.0/org/refcodes/filesystem/FileSystemAlreadyExistsException.html))
* \[<span style="color:green">MODIFIED</span>\] [`IllegalFileHandleException.java`](https://bitbucket.org/refcodes/refcodes-filesystem/src/refcodes-filesystem-1.1.0/src/main/java/org/refcodes/filesystem/IllegalFileHandleException.java) (see Javadoc at [`IllegalFileHandleException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-filesystem/1.1.0/org/refcodes/filesystem/IllegalFileHandleException.html))
* \[<span style="color:green">MODIFIED</span>\] [`IllegalKeyException.java`](https://bitbucket.org/refcodes/refcodes-filesystem/src/refcodes-filesystem-1.1.0/src/main/java/org/refcodes/filesystem/IllegalKeyException.java) (see Javadoc at [`IllegalKeyException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-filesystem/1.1.0/org/refcodes/filesystem/IllegalKeyException.html))
* \[<span style="color:green">MODIFIED</span>\] [`IllegalNameException.java`](https://bitbucket.org/refcodes/refcodes-filesystem/src/refcodes-filesystem-1.1.0/src/main/java/org/refcodes/filesystem/IllegalNameException.java) (see Javadoc at [`IllegalNameException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-filesystem/1.1.0/org/refcodes/filesystem/IllegalNameException.html))
* \[<span style="color:green">MODIFIED</span>\] [`IllegalPathException.java`](https://bitbucket.org/refcodes/refcodes-filesystem/src/refcodes-filesystem-1.1.0/src/main/java/org/refcodes/filesystem/IllegalPathException.java) (see Javadoc at [`IllegalPathException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-filesystem/1.1.0/org/refcodes/filesystem/IllegalPathException.html))
* \[<span style="color:green">MODIFIED</span>\] [`NoCreateAccessException.java`](https://bitbucket.org/refcodes/refcodes-filesystem/src/refcodes-filesystem-1.1.0/src/main/java/org/refcodes/filesystem/NoCreateAccessException.java) (see Javadoc at [`NoCreateAccessException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-filesystem/1.1.0/org/refcodes/filesystem/NoCreateAccessException.html))
* \[<span style="color:green">MODIFIED</span>\] [`NoDeleteAccessException.java`](https://bitbucket.org/refcodes/refcodes-filesystem/src/refcodes-filesystem-1.1.0/src/main/java/org/refcodes/filesystem/NoDeleteAccessException.java) (see Javadoc at [`NoDeleteAccessException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-filesystem/1.1.0/org/refcodes/filesystem/NoDeleteAccessException.html))
* \[<span style="color:green">MODIFIED</span>\] [`NoListAccessException.java`](https://bitbucket.org/refcodes/refcodes-filesystem/src/refcodes-filesystem-1.1.0/src/main/java/org/refcodes/filesystem/NoListAccessException.java) (see Javadoc at [`NoListAccessException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-filesystem/1.1.0/org/refcodes/filesystem/NoListAccessException.html))
* \[<span style="color:green">MODIFIED</span>\] [`NoReadAccessException.java`](https://bitbucket.org/refcodes/refcodes-filesystem/src/refcodes-filesystem-1.1.0/src/main/java/org/refcodes/filesystem/NoReadAccessException.java) (see Javadoc at [`NoReadAccessException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-filesystem/1.1.0/org/refcodes/filesystem/NoReadAccessException.html))
* \[<span style="color:green">MODIFIED</span>\] [`NoWriteAccessException.java`](https://bitbucket.org/refcodes/refcodes-filesystem/src/refcodes-filesystem-1.1.0/src/main/java/org/refcodes/filesystem/NoWriteAccessException.java) (see Javadoc at [`NoWriteAccessException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-filesystem/1.1.0/org/refcodes/filesystem/NoWriteAccessException.html))
* \[<span style="color:green">MODIFIED</span>\] [`UnknownFileException.java`](https://bitbucket.org/refcodes/refcodes-filesystem/src/refcodes-filesystem-1.1.0/src/main/java/org/refcodes/filesystem/UnknownFileException.java) (see Javadoc at [`UnknownFileException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-filesystem/1.1.0/org/refcodes/filesystem/UnknownFileException.html))
* \[<span style="color:green">MODIFIED</span>\] [`UnknownFileSystemException.java`](https://bitbucket.org/refcodes/refcodes-filesystem/src/refcodes-filesystem-1.1.0/src/main/java/org/refcodes/filesystem/UnknownFileSystemException.java) (see Javadoc at [`UnknownFileSystemException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-filesystem/1.1.0/org/refcodes/filesystem/UnknownFileSystemException.html))
* \[<span style="color:green">MODIFIED</span>\] [`UnknownKeyException.java`](https://bitbucket.org/refcodes/refcodes-filesystem/src/refcodes-filesystem-1.1.0/src/main/java/org/refcodes/filesystem/UnknownKeyException.java) (see Javadoc at [`UnknownKeyException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-filesystem/1.1.0/org/refcodes/filesystem/UnknownKeyException.html))
* \[<span style="color:green">MODIFIED</span>\] [`UnknownNameException.java`](https://bitbucket.org/refcodes/refcodes-filesystem/src/refcodes-filesystem-1.1.0/src/main/java/org/refcodes/filesystem/UnknownNameException.java) (see Javadoc at [`UnknownNameException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-filesystem/1.1.0/org/refcodes/filesystem/UnknownNameException.html))
* \[<span style="color:green">MODIFIED</span>\] [`UnknownPathException.java`](https://bitbucket.org/refcodes/refcodes-filesystem/src/refcodes-filesystem-1.1.0/src/main/java/org/refcodes/filesystem/UnknownPathException.java) (see Javadoc at [`UnknownPathException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-filesystem/1.1.0/org/refcodes/filesystem/UnknownPathException.html))

## Change list &lt;refcodes-forwardsecrecy-alt&gt; (version 1.1.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-1.1.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-1.1.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-forwardsecrecy-alt/src/refcodes-forwardsecrecy-alt-1.1.0/refcodes-forwardsecrecy-alt-filesystem/pom.xml) 

## Change list &lt;refcodes-eventbus&gt; (version 1.1.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-eventbus/src/refcodes-eventbus-1.1.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-eventbus/src/refcodes-eventbus-1.1.0/README.md) 

## Change list &lt;refcodes-filesystem-alt&gt; (version 1.1.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-filesystem-alt/src/refcodes-filesystem-alt-1.1.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-filesystem-alt/src/refcodes-filesystem-alt-1.1.0/refcodes-filesystem-alt-s3/pom.xml) 

## Change list &lt;refcodes-io-ext&gt; (version 1.1.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-1.1.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-io-ext/src/refcodes-io-ext-1.1.0/refcodes-io-ext-observable/pom.xml) 

## Change list &lt;refcodes-jobbus&gt; (version 1.1.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-jobbus/src/refcodes-jobbus-1.1.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-jobbus/src/refcodes-jobbus-1.1.0/README.md) 

## Change list &lt;refcodes-logger-ext&gt; (version 1.1.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-1.1.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-1.1.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-1.1.0/refcodes-logger-ext-slf4j/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/refcodes-logger-ext-1.1.0/refcodes-logger-ext-slf4j/README.md) 

## Change list &lt;refcodes-remoting&gt; (version 1.1.0)

* \[<span style="color:blue">ADDED</span>\] [`RemotingException.java`](https://bitbucket.org/refcodes/refcodes-remoting/src/refcodes-remoting-1.1.0/src/main/java/org/refcodes/remoting/RemotingException.java) (see Javadoc at [`RemotingException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-remoting/1.1.0/org/refcodes/remoting/RemotingException.html))
* \[<span style="color:blue">ADDED</span>\] [`RemotingRuntimeException.java`](https://bitbucket.org/refcodes/refcodes-remoting/src/refcodes-remoting-1.1.0/src/main/java/org/refcodes/remoting/RemotingRuntimeException.java) (see Javadoc at [`RemotingRuntimeException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-remoting/1.1.0/org/refcodes/remoting/RemotingRuntimeException.html))
* \[<span style="color:blue">ADDED</span>\] [`SubjectDescriptor.java`](https://bitbucket.org/refcodes/refcodes-remoting/src/refcodes-remoting-1.1.0/src/main/java/org/refcodes/remoting/SubjectDescriptor.java) (see Javadoc at [`SubjectDescriptor.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-remoting/1.1.0/org/refcodes/remoting/SubjectDescriptor.html))
* \[<span style="color:red">DELETED</span>\] `AbstractRemotingException.java`
* \[<span style="color:red">DELETED</span>\] `AbstractRemotingRuntimeException.java`
* \[<span style="color:red">DELETED</span>\] `Subject.java`
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-remoting/src/refcodes-remoting-1.1.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-remoting/src/refcodes-remoting-1.1.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`AmbiguousProxyException.java`](https://bitbucket.org/refcodes/refcodes-remoting/src/refcodes-remoting-1.1.0/src/main/java/org/refcodes/remoting/AmbiguousProxyException.java) (see Javadoc at [`AmbiguousProxyException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-remoting/1.1.0/org/refcodes/remoting/AmbiguousProxyException.html))
* \[<span style="color:green">MODIFIED</span>\] [`DuplicateInstanceIdRuntimeException.java`](https://bitbucket.org/refcodes/refcodes-remoting/src/refcodes-remoting-1.1.0/src/main/java/org/refcodes/remoting/DuplicateInstanceIdRuntimeException.java) (see Javadoc at [`DuplicateInstanceIdRuntimeException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-remoting/1.1.0/org/refcodes/remoting/DuplicateInstanceIdRuntimeException.html))
* \[<span style="color:green">MODIFIED</span>\] [`DuplicateSessionIdRuntimeException.java`](https://bitbucket.org/refcodes/refcodes-remoting/src/refcodes-remoting-1.1.0/src/main/java/org/refcodes/remoting/DuplicateSessionIdRuntimeException.java) (see Javadoc at [`DuplicateSessionIdRuntimeException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-remoting/1.1.0/org/refcodes/remoting/DuplicateSessionIdRuntimeException.html))
* \[<span style="color:green">MODIFIED</span>\] [`RemoteServerImpl.java`](https://bitbucket.org/refcodes/refcodes-remoting/src/refcodes-remoting-1.1.0/src/main/java/org/refcodes/remoting/RemoteServerImpl.java) (see Javadoc at [`RemoteServerImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-remoting/1.1.0/org/refcodes/remoting/RemoteServerImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`SubjectDescriptorImpl.java`](https://bitbucket.org/refcodes/refcodes-remoting/src/refcodes-remoting-1.1.0/src/main/java/org/refcodes/remoting/SubjectDescriptorImpl.java) (see Javadoc at [`SubjectDescriptorImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-remoting/1.1.0/org/refcodes/remoting/SubjectDescriptorImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`SubjectInstanceDescriptorImpl.java`](https://bitbucket.org/refcodes/refcodes-remoting/src/refcodes-remoting-1.1.0/src/main/java/org/refcodes/remoting/SubjectInstanceDescriptorImpl.java) (see Javadoc at [`SubjectInstanceDescriptorImpl.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-remoting/1.1.0/org/refcodes/remoting/SubjectInstanceDescriptorImpl.html))
* \[<span style="color:green">MODIFIED</span>\] [`InvalidMethodReplyRuntimeException.java`](https://bitbucket.org/refcodes/refcodes-remoting/src/refcodes-remoting-1.1.0/src/main/java/org/refcodes/remoting/InvalidMethodReplyRuntimeException.java) (see Javadoc at [`InvalidMethodReplyRuntimeException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-remoting/1.1.0/org/refcodes/remoting/InvalidMethodReplyRuntimeException.html))
* \[<span style="color:green">MODIFIED</span>\] [`InvalidMethodRequestRuntimeException.java`](https://bitbucket.org/refcodes/refcodes-remoting/src/refcodes-remoting-1.1.0/src/main/java/org/refcodes/remoting/InvalidMethodRequestRuntimeException.java) (see Javadoc at [`InvalidMethodRequestRuntimeException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-remoting/1.1.0/org/refcodes/remoting/InvalidMethodRequestRuntimeException.html))
* \[<span style="color:green">MODIFIED</span>\] [`NoSuchProxyException.java`](https://bitbucket.org/refcodes/refcodes-remoting/src/refcodes-remoting-1.1.0/src/main/java/org/refcodes/remoting/NoSuchProxyException.java) (see Javadoc at [`NoSuchProxyException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-remoting/1.1.0/org/refcodes/remoting/NoSuchProxyException.html))
* \[<span style="color:green">MODIFIED</span>\] [`ProxyDisposedRuntimeException.java`](https://bitbucket.org/refcodes/refcodes-remoting/src/refcodes-remoting-1.1.0/src/main/java/org/refcodes/remoting/ProxyDisposedRuntimeException.java) (see Javadoc at [`ProxyDisposedRuntimeException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-remoting/1.1.0/org/refcodes/remoting/ProxyDisposedRuntimeException.html))
* \[<span style="color:green">MODIFIED</span>\] [`SubjectInstance.java`](https://bitbucket.org/refcodes/refcodes-remoting/src/refcodes-remoting-1.1.0/src/main/java/org/refcodes/remoting/SubjectInstance.java) (see Javadoc at [`SubjectInstance.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-remoting/1.1.0/org/refcodes/remoting/SubjectInstance.html))
* \[<span style="color:green">MODIFIED</span>\] [`UnknownInstanceIdRuntimeException.java`](https://bitbucket.org/refcodes/refcodes-remoting/src/refcodes-remoting-1.1.0/src/main/java/org/refcodes/remoting/UnknownInstanceIdRuntimeException.java) (see Javadoc at [`UnknownInstanceIdRuntimeException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-remoting/1.1.0/org/refcodes/remoting/UnknownInstanceIdRuntimeException.html))

## Change list &lt;refcodes-remoting-ext&gt; (version 1.1.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-1.1.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-remoting-ext/src/refcodes-remoting-ext-1.1.0/refcodes-remoting-ext-observer/pom.xml) 

## Change list &lt;refcodes-security-alt&gt; (version 1.1.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-1.1.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-1.1.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-1.1.0/refcodes-security-alt-chaos/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-alt/src/refcodes-security-alt-1.1.0/refcodes-security-alt-chaos/README.md) 

## Change list &lt;refcodes-security-ext&gt; (version 1.1.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-1.1.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-1.1.0/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-1.1.0/refcodes-security-ext-chaos/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`README.md`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-1.1.0/refcodes-security-ext-chaos/README.md) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-security-ext/src/refcodes-security-ext-1.1.0/refcodes-security-ext-spring/pom.xml) 

## Change list &lt;refcodes-servicebus&gt; (version 1.1.0)

* \[<span style="color:blue">ADDED</span>\] [`ServiceBusRuntimeException.java`](https://bitbucket.org/refcodes/refcodes-servicebus/src/refcodes-servicebus-1.1.0/src/main/java/org/refcodes/servicebus/ServiceBusRuntimeException.java) (see Javadoc at [`ServiceBusRuntimeException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-servicebus/1.1.0/org/refcodes/servicebus/ServiceBusRuntimeException.html))
* \[<span style="color:red">DELETED</span>\] `AbstractServiceBusRuntimeException.java`
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-servicebus/src/refcodes-servicebus-1.1.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`AmbiguousServiceRuntimeException.java`](https://bitbucket.org/refcodes/refcodes-servicebus/src/refcodes-servicebus-1.1.0/src/main/java/org/refcodes/servicebus/AmbiguousServiceRuntimeException.java) (see Javadoc at [`AmbiguousServiceRuntimeException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-servicebus/1.1.0/org/refcodes/servicebus/AmbiguousServiceRuntimeException.html))
* \[<span style="color:green">MODIFIED</span>\] [`DuplicateServiceRuntimeException.java`](https://bitbucket.org/refcodes/refcodes-servicebus/src/refcodes-servicebus-1.1.0/src/main/java/org/refcodes/servicebus/DuplicateServiceRuntimeException.java) (see Javadoc at [`DuplicateServiceRuntimeException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-servicebus/1.1.0/org/refcodes/servicebus/DuplicateServiceRuntimeException.html))
* \[<span style="color:green">MODIFIED</span>\] [`NoMatchingServiceRuntimeException.java`](https://bitbucket.org/refcodes/refcodes-servicebus/src/refcodes-servicebus-1.1.0/src/main/java/org/refcodes/servicebus/NoMatchingServiceRuntimeException.java) (see Javadoc at [`NoMatchingServiceRuntimeException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-servicebus/1.1.0/org/refcodes/servicebus/NoMatchingServiceRuntimeException.html))
* \[<span style="color:green">MODIFIED</span>\] [`ServiceDirectory.java`](https://bitbucket.org/refcodes/refcodes-servicebus/src/refcodes-servicebus-1.1.0/src/main/java/org/refcodes/servicebus/ServiceDirectory.java) (see Javadoc at [`ServiceDirectory.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-servicebus/1.1.0/org/refcodes/servicebus/ServiceDirectory.html))
* \[<span style="color:green">MODIFIED</span>\] [`UnknownServiceRuntimeException.java`](https://bitbucket.org/refcodes/refcodes-servicebus/src/refcodes-servicebus-1.1.0/src/main/java/org/refcodes/servicebus/UnknownServiceRuntimeException.java) (see Javadoc at [`UnknownServiceRuntimeException.java`](https://www.javadoc.io/doc/org.refcodes/refcodes-servicebus/1.1.0/org/refcodes/servicebus/UnknownServiceRuntimeException.html))

## Change list &lt;refcodes-servicebus-alt&gt; (version 1.1.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-servicebus-alt/src/refcodes-servicebus-alt-1.1.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-servicebus-alt/src/refcodes-servicebus-alt-1.1.0/refcodes-servicebus-alt-spring/pom.xml) 

## Change list &lt;refcodes-tabular-alt&gt; (version 1.1.0)

* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-tabular-alt/src/refcodes-tabular-alt-1.1.0/pom.xml) 
* \[<span style="color:green">MODIFIED</span>\] [`pom.xml`](https://bitbucket.org/refcodes/refcodes-tabular-alt/src/refcodes-tabular-alt-1.1.0/refcodes-tabular-alt-forwardsecrecy/pom.xml) 
